<?php

class clsAmazonAWS
{
    // http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&SubscriptionId=1Y29C4CA4M559W0KD9R2&AssociateTag=ws&Operation=ItemLookup&ItemId=0385504209&IdType=ASIN&ResponseGroup=Request,Medium&Version=2005-02-23
    public $sSearch;
    public $sSubscriptionId;
    public $sResultXML;

    public $sBookISBN;
    public $sBookDetailPageURL;
    public $iBookSalesRank;

    public $sImageURL_Small;
    public $sImageURL_Medium;
    public $sImageURL_Large;

    public $sBookAuthor;
    public $sBookTitle;
    public $sBookBinding;
    public $sBookEAN;
    public $sBookLabel;
    public $dBookPrice;
    public $sBookManufacturer;
    public $iBookPages;
    public $dBookPublicationDate;
    public $sBookPublisher;
    public $dBookReleaseDate;

    public $sBookDescription;
    public $sBookEditorialReview;
    public $sBookDescription2;

    public $aBookSearchByParameters;

    public $oCurl;

    function clsAmazonAWS($sSubscriptionId = "")
    {
        $this->sSubscriptionId = $sSubscriptionId;
        $this->oCurl = curl_init();

        $this->aBookSearchByParameters = array();

    	curl_setopt($this->oCurl, CURLOPT_RETURNTRANSFER, 1);
    }

    function SearchTitle($sBookTitle)
    {
        $this->SearchByParameters("TITLE", $sBookTitle);
    }

    function SearchAuthor($sBookAuthor)
    {
        $this->SearchByParameters("AUTHOR", $sBookAuthor);
    }

    function SearchByParameters($sSearcyBy, $sSearch)
    {
        $this->sSearch = $sSearch;

        $sSearchURL = "http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&SubscriptionId=" . $this->sSubscriptionId . "&AssociateTag=ws&Operation=ItemSearch&Keywords=" . urlencode($this->sSearch) . "&SearchIndex=Books&IdType=" . $sSearcyBy . "&ResponseGroup=Request,Medium&Version=2005-02-23";
    	curl_setopt($this->oCurl, CURLOPT_URL, $sSearchURL);
        // print($sSearchURL . '<HR>');
        $content = curl_exec ($this->oCurl);
        $this->sResultXML = $content;
        $objXML = new clsXML($this->sResultXML, "xml");

        $aISBN = $objXML->evaluate("//Item/ASIN");
        $aBookImages = $objXML->evaluate("//Item/MediumImage/URL");
        $aBookTitles = $objXML->evaluate("//Item/ItemAttributes/Title");

        for ($i=0; $i < count($aISBN); $i++)
        {
            $iISBN = $objXML->get_content($aISBN[$i]);
            $sBookImage = $objXML->get_content($aBookImages[$i]);
            $sBookTitle = $objXML->get_content($aBookTitles[$i]);

            $this->aBookSearchByParameters[$i][0] = $iISBN;
            $this->aBookSearchByParameters[$i][1] = $sBookTitle;
            $this->aBookSearchByParameters[$i][2] = $sBookImage;
        }
    }

    function SearchISBN($sISBN)
    {
        $sISBN = str_replace('-', '', $sISBN);

        if (strlen($sISBN) < 10)
            $sISBN .= 'X';

        $this->sSearch = $sISBN;
        $sSearchURL = "http://webservices.amazon.com/onca/xml?Service=AWSECommerceService&SubscriptionId=" . $this->sSubscriptionId . "&AssociateTag=ws&Operation=ItemLookup&ItemId=" . $this->sSearch . "&IdType=ASIN&ResponseGroup=Request,Medium&Version=2005-02-23";

    	curl_setopt($this->oCurl, CURLOPT_URL, $sSearchURL);
        $content = curl_exec ($this->oCurl);
        $this->sResultXML = $content;
        $aResult = $this->xml2array($this->sResultXML);		
        // MAPPING of all detail fields like this
		/*
        print("BookDetailPageURL = ");
        print($aResult['ItemLookupResponse']['Items']['Item']['DetailPageURL']);
        print("<HR>");

        // kkori work from here
        print_r($aResult);
        die();
		*/
		
        $this->sBookISBN =$aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['ISBN'];
		$this->sBookDetailPageURL = $aResult['ItemLookupResponse']['Items']['Item']['DetailPageURL'];
		$this->iBookSalesRank = $aResult['ItemLookupResponse']['Items']['Item']['SalesRank'];
		$this->sImageURL_Small = $aResult['ItemLookupResponse']['Items']['Item']['SmallImage']['URL'];
		$this->sImageURL_Medium = $aResult['ItemLookupResponse']['Items']['Item']['MediumImage']['URL'];
        $this->sImageURL_Large = $aResult['ItemLookupResponse']['Items']['Item']['LargeImage']['URL'];
		$this->sBookAuthor = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['Author'];
		
		if ($this->sBookAuthor == '')
            $this->sBookAuthor = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['Creator'];
			
		$this->sBookTitle = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['Title'];
		$this->sBookTitle = str_replace('&apos', "'", $this->sBookTitle);
		$this->sBookBinding = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['Binding'];
		$this->sBookEAN = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['EAN'];
		
		 $this->sBookLabel = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['Label'];
        $this->dBookPrice = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['ListPrice']['FormattedPrice'];
        $this->sBookManufacturer = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['Manufacturer'];
        $this->iBookPages = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['NumberOfPages'];
        $this->dBookPublicationDate = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['PublicationDate'];
        $this->sBookPublisher = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['Publisher'];
        $this->dBookReleaseDate = $aResult['ItemLookupResponse']['Items']['Item']['ItemAttributes']['ReleaseDate'];
		$this->sBookDescription = $aResult['ItemLookupResponse']['Items']['Item']['EditorialReviews']['EditorialReview'][0]['Content'];
        $this->sBookEditorialReview = $aResult['ItemLookupResponse']['Items']['Item']['EditorialReviews']['EditorialReview'][1]['Content'];
		
		
        /*
		$this->sBookDescription = html_entity_decode($objXML->get_content($varValue[0]));
        $this->sBookEditorialReview = html_entity_decode($objXML->get_content($varValue[1]));
        $this->sBookDescription2 = html_entity_decode($objXML->get_content($varValue[2]));
		

        $this->sBookISBN = $objXML->GetValue("//Item/ASIN");
        $this->sBookDetailPageURL = $objXML->GetValue("//Item/DetailPageURL");
        $this->iBookSalesRank = $objXML->GetValue("//Item/SalesRank");

        $this->sImageURL_Small = $objXML->GetValue("//Item/SmallImage/URL");
        $this->sImageURL_Medium = $objXML->GetValue("//Item/MediumImage/URL");
        $this->sImageURL_Large = $objXML->GetValue("//Item/LargeImage/URL");

        $this->sBookAuthor = html_entity_decode($objXML->GetValue("//Item/ItemAttributes/Author"));

        if ($this->sBookAuthor == '')
            $this->sBookAuthor = html_entity_decode($objXML->GetValue("//Item/ItemAttributes/Creator"));

        $this->sBookTitle = html_entity_decode($objXML->GetValue("//Item/ItemAttributes/Title"));

        $this->sBookTitle = str_replace('&apos', "'", $this->sBookTitle);

        $this->sBookBinding = $objXML->GetValue("//Item/ItemAttributes/Binding");
        $this->sBookEAN = $objXML->GetValue("//Item/ItemAttributes/EAN");
        $this->sBookLabel = $objXML->GetValue("//Item/ItemAttributes/Label");
        $this->dBookPrice = $objXML->GetValue("//Item/ItemAttributes/ListPrice/FormattedPrice");
        $this->sBookManufacturer = $objXML->GetValue("//Item/ItemAttributes/Manufacturer");
        $this->iBookPages = $objXML->GetValue("//Item/ItemAttributes/NumberOfPages");
        $this->dBookPublicationDate = $objXML->GetValue("//Item/ItemAttributes/PublicationDate");
        $this->sBookPublisher = $objXML->GetValue("//Item/ItemAttributes/Publisher");
        $this->dBookReleaseDate = $objXML->GetValue("//Item/ItemAttributes/ReleaseDate");

        $varValue = $objXML->evaluate("//Item/EditorialReviews/EditorialReview/Content");
        $this->sBookDescription = html_entity_decode($objXML->get_content($varValue[0]));
        $this->sBookEditorialReview = html_entity_decode($objXML->get_content($varValue[1]));
        $this->sBookDescription2 = html_entity_decode($objXML->get_content($varValue[2]));
		*/
		
        curl_close($this->oCurl);
    }

    function SaveImages($sImageName, $sSmallImageLocation, $sMediumImageLocation, $sLargeImageLocation)
    {
        $sExtension = substr($this->sImageURL_Small, -4);
        $this->GrabImage($this->sImageURL_Small, $sSmallImageLocation . $sImageName . $sExtension);
        $this->GrabImage($this->sImageURL_Medium, $sMediumImageLocation . $sImageName . $sExtension);
        $this->GrabImage($this->sImageURL_Large, $sLargeImageLocation . $sImageName . $sExtension);
    }

    function GrabImage($sSource, $sTarget)
    {
        if (($sSource == '') || ($sTarget == ''))
            return('');

    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    	curl_setopt($ch, CURLOPT_URL, $sSource);
    	$content = curl_exec ($ch);
        curl_close($ch);

        if (file_exists($sTarget)) unlink($sTarget);

        $varFileHandle = @fopen($sTarget,'w');
        $varBytes = @fwrite($varFileHandle, $content);
        fclose($varFileHandle);
    }

    function xml2array($contents, $get_attributes = 1, $priority = 'tag')
    {
        //$contents = "";
        if (!function_exists('xml_parser_create'))
        {
            return array ();
        }
        $parser = xml_parser_create('');

        /*
        if (!($fp = @ fopen($url, 'rb')))
        {
            return array ();
        }
        while (!feof($fp))
        {
            $contents .= fread($fp, 8192);
        }
        fclose($fp);
        */

        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);
        if (!$xml_values)
            return; //Hmm...
        $xml_array = array ();
        $parents = array ();
        $opened_tags = array ();
        $arr = array ();
        $current = & $xml_array;
        $repeated_tag_index = array ();
        foreach ($xml_values as $data)
        {
            unset ($attributes, $value);
            extract($data);
            $result = array ();
            $attributes_data = array ();
            if (isset ($value))
            {
                if ($priority == 'tag')
                    $result = $value;
                else
                    $result['value'] = $value;
            }
            if (isset ($attributes) and $get_attributes)
            {
                foreach ($attributes as $attr => $val)
                {
                    if ($priority == 'tag')
                        $attributes_data[$attr] = $val;
                    else
                        $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
                }
            }
            if ($type == "open")
            {
                $parent[$level -1] = & $current;
                if (!is_array($current) or (!in_array($tag, array_keys($current))))
                {
                    $current[$tag] = $result;
                    if ($attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    $current = & $current[$tag];
                }
                else
                {
                    if (isset ($current[$tag][0]))
                    {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level]++;
                    }
                    else
                    {
                        $current[$tag] = array (
                            $current[$tag],
                            $result
                        );
                        $repeated_tag_index[$tag . '_' . $level] = 2;
                        if (isset ($current[$tag . '_attr']))
                        {
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset ($current[$tag . '_attr']);
                        }
                    }
                    $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                    $current = & $current[$tag][$last_item_index];
                }
            }
            elseif ($type == "complete")
            {
                if (!isset ($current[$tag]))
                {
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                }
                else
                {
                    if (isset ($current[$tag][0]) and is_array($current[$tag]))
                    {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        if ($priority == 'tag' and $get_attributes and $attributes_data)
                        {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag . '_' . $level]++;
                    }
                    else
                    {
                        $current[$tag] = array (
                            $current[$tag],
                            $result
                        );
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        if ($priority == 'tag' and $get_attributes)
                        {
                            if (isset ($current[$tag . '_attr']))
                            {
                                $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                unset ($current[$tag . '_attr']);
                            }
                            if ($attributes_data)
                            {
                                $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
                    }
                }
            }
            elseif ($type == 'close')
            {
                $current = & $parent[$level -1];
            }
        }
        return ($xml_array);
    }
}

?>
