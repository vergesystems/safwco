<?php
/**
  * PHP-Class hn_captcha Version 1.2, released 16-Apr-2004
  * Author: Horst Nogajski, horst@nogajski.de
  *
  * License: GNU GPL (http://www.opensource.org/licenses/gpl-license.html)
  * Download: http://hn273.users.phpclasses.org/browse/package/1569.html
  *
  * If you find it useful, you might rate it on http://www.phpclasses.org/rate.html?package=1569
  * If you use this class in a productional environment, you might drop me a note, so I can add a link to the page.
  *
  **/

/* EXAMPLE:

	// Please type in all needed values before run the script!
	require_once("clsCaptcha.php");
	// ConfigArray
	$CAPTCHA_INIT = array(
            'tempfolder'     => 'tmp/',      // string: absolute path (with trailing slash!) to a writeable tempfolder which is also accessible via HTTP!
			'TTF_folder'     => 'fonts/', // string: absolute path (with trailing slash!) to folder which contains your TrueType-Fontfiles.
                                // mixed (array or string): basename(s) of TrueType-Fontfiles
		//	'TTF_RANGE'      => array('COMIC.TTF','JACOBITE.TTF','LYDIAN.TTF','MREARL.TTF','RUBBERSTAMP.TTF','ZINJARON.TTF'),
			'TTF_RANGE'      => 'COMIC.TTF',

            'chars'          => 5,       // integer: number of chars to use for ID
            'minsize'        => 20,      // integer: minimal size of chars
            'maxsize'        => 30,      // integer: maximal size of chars
            'maxrotation'    => 25,      // integer: define the maximal angle for char-rotation, good results are between 0 and 30

            'noise'          => TRUE,    // boolean: TRUE = noisy chars | FALSE = grid
            'websafecolors'  => FALSE,   // boolean
            'refreshlink'    => TRUE,    // boolean
            'lang'           => 'en',    // string:  ['en'|'de']
            'maxtry'         => 3,       // integer: [1-9]

            'badguys_url'    => '/',     // string: URL
            'secretstring'   => 'A very, very secret string which is used to generate a md5-key!',
            'secretposition' => 24,      // integer: [1-32]

            'debug'          => FALSE,


			'counter_filename'		=> '',              // string: absolute filename for textfile which stores current counter-value. Needs read- & write-access!
			'prefix'				=> 'hn_captcha_',   // string: prefix for the captcha-images, is needed to identify the files in shared tempfolders
			'collect_garbage_after'	=> 20,             // integer: the garbage-collector run once after this number of script-calls
			'maxlifetime'			=> 60              // integer: only imagefiles which are older than this amount of seconds will be deleted

	);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<TITLE>PHP-Captcha-Class :: DEMO</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">

<style type="text/css">
<!--
	a:link
	{
		color: #0079C5;
		background: transparent;
		text-decoration: none;
	}
	a:visited
	{
		color: #5DA3ED;
		background: transparent;
		text-decoration: none;
	}
	a:hover,
	a:active,
	a:focus
	{
		color: #cd3021;
		background: transparent;
		text-decoration: underline;
	}

	html,
	body
	{
		margin-top: 20px;
		margin-bottom: 20px;
		margin-left: 20px;
		margin-right: 20px;
		padding-top: 0px;
		padding-bottom: 0px;
		padding-left: 0px;
		padding-right: 0px;
	}

	body
	{
		background-color: #FFFFFF;
		color: #000000;
		font-family: Verdana, Helvetica, Arial, sans-serif;
	}

	h3
	{
		margin-left: 30px;
		margin-right: 20px;
		background: transparent;
		color: #222222;
		font-size: 20px;
		font-style: normal;
		font-weight: bold;
		font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
		line-height: 100%;
		letter-spacing: 1px;
	}

/*********************************
 *
 *	CAPTCHA-Styles
 *
	p.captcha_1,
	p.captcha_2,
	p.captcha_notvalid
	{
		margin-left: 30px;
		margin-right: 20px;
		font-size: 12px;
		font-style: normal;
		font-weight: normal;
		font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
		background: transparent;
		color: #000000;
	}
	p.captcha_2
	{
		font-size: 10px%;
		font-style: italic;
		font-weight: normal;
	}
	p.captcha_notvalid
	{
		font-weight: bold;
		color: #FFAAAA;
	}

	.captchapict
	{
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		border-style: inset;
		border-width: 4px;
		border-color: #C0C0C0;
	}

	#captcha
	{
		margin-left: 30px;
		margin-right: 30px;
		border-style: dashed;
		border-width: 2px;
		border-color: #FFD940;
	}
-->
</style>
</head>
<body>
<h3>This is a demo of hn_captcha.class.php with garbage collector extension</h3>

<?PHP


	$captcha =& new hn_captcha_X1($CAPTCHA_INIT);



	if($captcha->garbage_collector_error)
	{
		// Error! (Counter-file or deleting lost images)
		echo "<p><br><b>An ERROR has occured!</b><br>Here you might send email-notification to webmaster or something like that.</p>";
	}



	switch($captcha->validate_submit())
	{

		// was submitted and has valid keys
		case 1:
			// PUT IN ALL YOUR STUFF HERE //
					echo "<p><br>Congratulation. You will get the resource now.";
					echo "<br><br><a href=\"".$_SERVER['PHP_SELF']."?download=yes&id=1234\">New DEMO</a></p>";
			break;


		// was submitted with no matching keys, but has not reached the maximum try's
		case 2:
			echo $captcha->display_form();
			break;


		// was submitted, has bad keys and also reached the maximum try's
		case 3:
			//if(!headers_sent() && isset($captcha->badguys_url)) header('location: '.$captcha->badguys_url);
					echo "<p><br>Reached the maximum try's of ".$captcha->maxtry." without success!";
					echo "<br><br><a href=\"".$_SERVER['PHP_SELF']."?download=yes&id=1234\">New DEMO</a></p>";
			break;


		// was not submitted, first entry
		default:
			echo $captcha->display_form();
			break;

	}

</body>
</html>


*/

/**
  * changes in version 1.1:
  *  - added a new configuration-variable: maxrotation
  *  - added a new configuration-variable: secretstring
  *  - modified function get_try(): now ever returns a string of 16 chars
  *
  * changes in version 1.2:
  *  - added a new configuration-variable: secretposition
  *  - once more modified the function get_try(): generate a string of 32 chars length,
  *    where at secretposition is the number of current-try.
  *    Hopefully this is enough for hackprevention.
  *
  **/

/**
  * License: GNU GPL (http://www.opensource.org/licenses/gpl-license.html)
  *
  * This program is free software;
  *
  * you can redistribute it and/or modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2 of the License,
  * or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License along with this program;
  * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  *
  **/


/**
  * Tabsize: 4
  *
  **/

/**
  * This class generates a picture to use in forms that perform CAPTCHA test
  * (Completely Automated Public Turing to tell Computers from Humans Apart).
  * After the test form is submitted a key entered by the user in a text field
  * is compared by the class to determine whether it matches the text in the picture.
  *
  * The class is a fork of the original released at www.phpclasses.org
  * by Julien Pachet with the name ocr_captcha.
  *
  * The following enhancements were added:
  *
  * - Support to make it work with GD library before version 2
  * - Hacking prevention
  * - Optional use of Web safe colors
  * - Limit the number of users attempts
  * - Display an optional refresh link to generate a new picture with a different key
  *   without counting to the user attempts limit verification
  * - Support the use of multiple random TrueType fonts
  * - Control the output image by only three parameters: number of text characters
  *   and minimum and maximum size preserving the size proportion
  * - Preserve all request parameters passed to the page via the GET method,
  *   so the CAPTCHA test can be added to existing scripts with minimal changes
  * - Added a debug option for testing the current configuration
  *
  * All the configuration settings are passed to the class in an array when the object instance is initialized.
  *
  * The class only needs two function calls to be used: display_form() and validate_submit().
  *
  * The class comes with an examplefile.
  * If you don't have it: http://hn273.users.phpclasses.org/browse/package/1569.html
  *
  * @shortdesc Class that generate a captcha-image with text and a form to fill in this text
  * @public
  * @author Horst Nogajski, (mail: horst@nogajski.de)
  * @version 1.0
  * @date 2004-April-10
  *
  **/

class clsCaptcha
{
	public $CAPTCHA_INIT;

	function __construct()
	{
        // ConfigArray
        $this->CAPTCHA_INIT = array(
            'tempfolder'     => cDataFolder . '/temp/',      // string: absolute path (with trailing slash!) to a writeable tempfolder which is also accessible via HTTP!
            'tempfolder2'    => cDataFolder_FrontEnd . '/temp/',      // Naveed: Temp2 for mod_rewrite
            'TTF_folder'     => cVSFFolder . '/components/captcha/fonts/', // string: absolute path (with trailing slash!) to folder which contains your TrueType-Fontfiles.
            // mixed (array or string): basename(s) of TrueType-Fontfiles
            //	'TTF_RANGE'      => array('COMIC.TTF','JACOBITE.TTF','LYDIAN.TTF','MREARL.TTF','RUBBERSTAMP.TTF','ZINJARON.TTF'),
            'TTF_RANGE'      => 'comic.ttf',

            'chars'          => 5,       // integer: number of chars to use for ID
            'minsize'        => 20,      // integer: minimal size of chars
            'maxsize'        => 30,      // integer: maximal size of chars
            'maxrotation'    => 25,      // integer: define the maximal angle for char-rotation, good results are between 0 and 30

            'noise'          => TRUE,    // boolean: TRUE = noisy chars | FALSE = grid
            'websafecolors'  => FALSE,   // boolean
            'refreshlink'    => TRUE,    // boolean
            'lang'           => 'en',    // string:  ['en'|'de']
            'maxtry'         => 3,       // integer: [1-9]

            'badguys_url'    => '/',     // string: URL
            'secretstring'   => 'A very, very secret string which is used to generate a md5-key!',
            'secretposition' => 24,      // integer: [1-32]
            'debug'          => FALSE,

            'counter_filename'		=> '',              // string: absolute filename for textfile which stores current counter-value. Needs read- & write-access!
            'prefix'				=> 'hn_captcha_',   // string: prefix for the captcha-images, is needed to identify the files in shared tempfolders
            'collect_garbage_after'	=> 20,             // integer: the garbage-collector run once after this number of script-calls
            'maxlifetime'			=> 60              // integer: only imagefiles which are older than this amount of seconds will be deleted
        );
	}

    function GetCaptchaBox(&$sCaptchaImage, &$sCaptchaPublicKey)
    {
    	$captcha = new hn_captcha_X1($this->CAPTCHA_INIT);
    	if($captcha->garbage_collector_error)
    	{
    		// Error! (Counter-file or deleting lost images)
    		die("<p><br><b>An ERROR has occured!</b><br>Here you might send email-notification to webmaster or something like that.</p>");
    	}

    	$sCaptchaImage = $captcha->display_captcha(true);
    	$sCaptchaPublicKey = $captcha->public_key;
    }

    function ValidateCaptcha($sCaptchaString, $sCaptchaCode)
    {
   		global $objEncryption;

   		// Captcha Verification
        $sPublicKey = $objEncryption->fnDecrypt(cEncryptionKey, $sCaptchaCode);

        $captcha = new hn_captcha_X1($this->CAPTCHA_INIT);
    	if($captcha->garbage_collector_error)
    		die("<p><br><b>An ERROR has occured!</b><br>Here you might send email-notification to webmaster or something like that.</p>");

        if ($captcha->generate_private($sPublicKey) != strtolower($sCaptchaString))
            return(false);
        else
            return(true);
    }
}



class hn_captcha
{

	////////////////////////////////
	//
	//	PUBLIC PARAMS
	//
        public $iRandomNumber;

		/**
		  * @shortdesc Absolute path to a Tempfolder (with trailing slash!). This must be writeable for PHP and also accessible via HTTP, because the image will be stored there.
          * @type string
		  * @public
          *
          **/
		public $tempfolder;
		public $tempfolder2;

		/**
          * @shortdesc Absolute path to folder with TrueTypeFonts (with trailing slash!). This must be readable by PHP.
		  * @type string
		  * @public
          *
          **/
		public $TTF_folder;

		/**
          * @shortdesc A List with available TrueTypeFonts for random char-creation.
		  * @type mixed[array|string]
		  * @public
          *
          **/
		public $TTF_RANGE  = array('COMIC.TTF','JACOBITE.TTF','LYDIAN.TTF','MREARL.TTF','RUBBERSTAMP.TTF','ZINJARON.TTF');

		/**
          * @shortdesc How many chars the generated text should have
		  * @type integer
		  * @public
          *
          **/
		public $chars		= 6;

		/**
          * @shortdesc The minimum size a Char should have
		  * @type integer
		  * @public
          *
          **/
		public $minsize	= 20;

		/**
          * @shortdesc The maximum size a Char can have
		  * @type integer
		  * @public
          *
          **/
		public $maxsize	= 40;

		/**
          * @shortdesc The maximum degrees a Char should be rotated. Set it to 30 means a random rotation between -30 and 30.
		  * @type integer
		  * @public
          *
          **/
		public $maxrotation = 30;

		/**
          * @shortdesc Background noise On/Off (if is Off, a grid will be created)
		  * @type boolean
		  * @public
          *
          **/
		public $noise		= TRUE;

		/**
          * @shortdesc This will only use the 216 websafe color pallette for the image.
		  * @type boolean
		  * @public
          *
          **/
		public $websafecolors = FALSE;

		/**
          * @shortdesc Switches language, available are 'en' and 'de'. You can easily add more. Look in CONSTRUCTOR.
		  * @type string
		  * @public
          *
          **/
		public $lang		= "en";

		/**
          * @shortdesc If a user has reached this number of try's without success, he will moved to the $badguys_url
		  * @type integer
		  * @public
          *
          **/
		public $maxtry		= 3;

		/**
          * @shortdesc Gives the user the possibility to generate a new captcha-image.
		  * @type boolean
		  * @public
          *
          **/
		public $refreshlink = TRUE;

		/**
          * @shortdesc If a user has reached his maximum try's, he will located to this url.
		  * @type boolean
		  * @public
          *
          **/
		public $badguys_url = "/";

		/**
		  * Number between 1 and 32
          *
          * @shortdesc Defines the position of 'current try number' in (32-char-length)-string generated by function get_try()
		  * @type integer
		  * @public
          *
          **/
		public $secretposition = 21;

		/**
          * @shortdesc The string is used to generate the md5-key.
		  * @type string
		  * @public
          *
          **/
		public $secretstring = "This is a very secret string. Nobody should know it, =:)";

		/**
          * @shortdesc Outputs configuration values for testing
		  * @type boolean
		  * @public
          *
          **/
		public $debug = FALSE;



	////////////////////////////////
	//
	//	PRIVATE PARAMS
	//

		/** @private **/
		public $lx;				// width of picture
		/** @private **/
		public $ly;				// height of picture
		/** @private **/
		public $jpegquality = 80;	// image quality
		/** @private **/
		public $noisefactor = 9;	// this will multiplyed with number of chars
		/** @private **/
		public $nb_noise;			// number of background-noise-characters
		/** @private **/
		public $TTF_file;			// holds the current selected TrueTypeFont
		/** @private **/
		public $msg1;
		/** @private **/
		public $msg2;
		/** @private **/
		public $buttontext;
		/** @private **/
		public $refreshbuttontext;
		/** @private **/
		public $public_K;
		/** @private **/
		public $private_K;
		/** @private **/
		public $key;				// md5-key
		/** @private **/
		public $public_key;    	// public key
		/** @private **/
		public $filename;			// filename of captcha picture
		/** @private **/
		public $gd_version;		// holds the Version Number of GD-Library
		/** @private **/
		public $QUERY_STRING;		// keeps the ($_GET) Querystring of the original Request
		/** @private **/
		public $current_try = 0;
		/** @private **/
		public $r;
		/** @private **/
		public $g;
		/** @private **/
		public $b;


	////////////////////////////////
	//
	//	CONSTRUCTOR
	//

		/**
		  * @shortdesc Extracts the config array and generate needed params.
		  * @private
		  * @type void
		  * @return nothing
		  *
		  **/
		function hn_captcha($config,$secure=TRUE)
		{
            $this->iRandomNumber = rand(0, 50000);

			// Test for GD-Library(-Version)
			$this->gd_version = $this->get_gd_version();
			if($this->gd_version == 0) die("There is no GD-Library-Support enabled. The Captcha-Class cannot be used!");
			if($this->debug) echo "\n<br>-Captcha-Debug: The available GD-Library has major version ".$this->gd_version;


			// Hackprevention
			if(
				(isset($_GET['maxtry']) || isset($_POST['maxtry']) || isset($_COOKIE['maxtry']))
				||
				(isset($_GET['debug']) || isset($_POST['debug']) || isset($_COOKIE['debug']))
				||
				(isset($_GET['captcharefresh']) || isset($_COOKIE['captcharefresh']))
				||
				(isset($_POST['captcharefresh']) && isset($_POST['private_key']))
				)
			{
				if($this->debug) echo "\n<br>-Captcha-Debug: Buuh. You are a bad guy!";
				if(isset($this->badguys_url) && !headers_sent()) header('location: '.$this->badguys_url);
				else die('Sorry.');
			}


			// extracts config array
			if(is_array($config))
			{
				if($secure && strcmp('4.2.0', phpversion()) < 0)
				{
					if($this->debug) echo "\n<br>-Captcha-Debug: Extracts Config-Array in secure-mode!";
					$valid = get_class_vars(get_class($this));
					foreach($config as $k=>$v)
					{
						if(array_key_exists($k,$valid)) $this->$k = $v;
					}
				}
				else
				{
					if($this->debug) echo "\n<br>-Captcha-Debug: Extracts Config-Array in unsecure-mode!";
					foreach($config as $k=>$v) $this->$k = $v;
				}
			}


			// check vars for maxtry, secretposition and min-max-size
			$this->maxtry = ($this->maxtry > 9 || $this->maxtry < 1) ? 3 : $this->maxtry;
			$this->secretposition = ($this->secretposition > 32 || $this->secretposition < 1) ? $this->maxtry : $this->secretposition;
			if($this->minsize > $this->maxsize)
			{
				$temp = $this->minsize;
				$this->minsize = $this->maxsize;
				$this->maxsize = $temp;
				if($this->debug) echo "<br>-Captcha-Debug: Arrghh! What do you think I mean with min and max? Switch minsize with maxsize.";
			}


			// check TrueTypeFonts
			if(is_array($this->TTF_RANGE))
			{
				if($this->debug) echo "\n<br>-Captcha-Debug: Check given TrueType-Array! (".count($this->TTF_RANGE).")";
				$temp = array();
				foreach($this->TTF_RANGE as $k=>$v)
				{
					if(is_readable($this->TTF_folder.$v)) $temp[] = $v;
				}
				$this->TTF_RANGE = $temp;
				if($this->debug) echo "\n<br>-Captcha-Debug: Valid TrueType-files: (".count($this->TTF_RANGE).")";
				if(count($this->TTF_RANGE) < 1) die('No Truetypefont available for the CaptchaClass.');
			}
			else
			{
				if($this->debug) echo "\n<br>-Captcha-Debug: Check given TrueType-File! (".$this->TTF_RANGE.")";
				if(!is_readable($this->TTF_folder.$this->TTF_RANGE)) die('No Truetypefont available for the CaptchaClass.');
			}

			// select first TrueTypeFont
			$this->change_TTF();
			if($this->debug) echo "\n<br>-Captcha-Debug: Set current TrueType-File: (".$this->TTF_file.")";


			// get number of noise-chars for background if is enabled
			$this->nb_noise = $this->noise ? ($this->chars * $this->noisefactor) : 0;
			if($this->debug) echo "\n<br>-Captcha-Debug: Set number of noise characters to: (".$this->nb_noise.")";


			// set dimension of image
			$this->lx = ($this->chars + 1) * (int)(($this->maxsize + $this->minsize) / 1.5);
			$this->ly = (int)(2.4 * $this->maxsize);
			if($this->debug) echo "\n<br>-Captcha-Debug: Set image dimension to: (".$this->lx." x ".$this->ly.")";


			// set all messages
			// (if you add a new language, you also want to add a line to the function "notvalid_msg()" at the end of the class!)
			$this->messages = array(
				'de'=>array(
							'msg1'=>'Du mu�t die <b>'.$this->chars.' Zeichen</b> im Bild, (Zahlen&nbsp;von&nbsp;<b>0&nbsp;-&nbsp;9</b> und Buchstaben&nbsp;von&nbsp;<b>A&nbsp;-&nbsp;F</b>),<br>in das Feld eintragen und das Formular abschicken um den Download zu starten.',
							'msg2'=>'Ohje, das kann ich nicht lesen. Bitte, generiere mir eine ',
							'buttontext'=>'abschicken',
							'refreshbuttontext'=>'neue ID'
							),
				'en'=>array(
							'msg1'=>'You must read and type the <b>'.$this->chars.' chars</b> within <b>0..9</b> and <b>A..F</b>, and submit the form.',
							'msg2'=>'Oh no, I cannot read this. Please, generate a ',
							'buttontext'=>'submit',
							'refreshbuttontext'=>'new ID'
							)
			);
			$this->msg1 = $this->messages[$this->lang]['msg1'];
			$this->msg2 = $this->messages[$this->lang]['msg2'];
			$this->buttontext = $this->messages[$this->lang]['buttontext'];
			$this->refreshbuttontext = $this->messages[$this->lang]['refreshbuttontext'];
			if($this->debug) echo "\n<br>-Captcha-Debug: Set messages to language: (".$this->lang.")";


			// keep params from original GET-request
			// (if you use POST or COOKIES, you have to implement it yourself, sorry.)
			$this->QUERY_STRING = strlen(trim($_SERVER['QUERY_STRING'])) > 0 ? '?'.strip_tags($_SERVER['QUERY_STRING']) : '';
			$refresh = $_SERVER['PHP_SELF'].$this->QUERY_STRING;
			if($this->debug) echo "\n<br>-Captcha-Debug: Keep this params from original GET-request: (".$this->QUERY_STRING.")";


			// check Postvars
			if(isset($_POST['public_key']))  $this->public_K = substr(strip_tags($_POST['public_key']),0,$this->chars);
			if(isset($_POST['private_key'])) $this->private_K = substr(strip_tags($_POST['private_key']),0,$this->chars);
			$this->current_try = isset($_POST['hncaptcha']) ? $this->get_try() : 0;
			if(!isset($_POST['captcharefresh'])) $this->current_try++;
			if($this->debug) echo "\n<br>-Captcha-Debug: Check POST-vars, current try is: (".$this->current_try.")";


			// generate Keys
			$this->key = md5($this->secretstring);
			$this->public_key = substr(md5(uniqid(rand(),true)), 0, $this->chars);
			if($this->debug) echo "\n<br>-Captcha-Debug: Generate Keys, public key is: (".$this->public_key.")";

		}



	////////////////////////////////
	//
	//	PUBLIC METHODS
	//

		/**
		  *
		  * @shortdesc displays a complete form with captcha-picture
		  * @public
		  * @type void
		  * @return HTML-Output
		  *
		  **/
		function display_form()
		{
			$try = $this->get_try(FALSE);
			if($this->debug) echo "\n<br>-Captcha-Debug: Generate a string which contains current try: ($try)";
			$s  = '<div id="captcha">';
			$s .= '<form class="captcha" name="captcha1" action="'.$_SERVER['PHP_SELF'].$this->QUERY_STRING.'" method="POST">'."\n";
			$s .= '<input type="hidden" name="hncaptcha" value="'.$try.'">'."\n";
			$s .= '<p class="captcha_notvalid">'.$this->notvalid_msg().'</p>';
			$s .= '<p class="captcha_1">'.$this->display_captcha()."</p>\n";
			$s .= '<p class="captcha_1">'.$this->msg1.'</p>';
			$s .= '<p class="captcha_1"><input class="captcha" type="text" name="private_key" value="" maxlength="'.$this->chars.'" size="'.$this->chars.'">&nbsp;&nbsp;';
			$s .= '<input class="captcha" type="submit" value="'.$this->buttontext.'">'."</p>\n";
			$s .= '</form>'."\n";
			if($this->refreshlink)
			{
				$s .= '<form style="display:inline;" name="captcha2" action="'.$_SERVER['PHP_SELF'].$this->QUERY_STRING.'" method="POST">'."\n";
				$s .= '<input type="hidden" name="captcharefresh" value="1"><input type="hidden" name="hncaptcha" value="'.$try.'">'."\n";
				$s .= '<p class="captcha_2">'.$this->msg2;
				$s .= $this->public_key_input().'<input class="captcha" type="submit" value="'.$this->refreshbuttontext.'">'."</p>\n";
				$s .= '</form>'."\n";
			}
			$s .= '</div>';
			if($this->debug) echo "\n<br>-Captcha-Debug: Output Form with captcha-image.<br><br>";
			return $s;
		}


		/**
		  *
		  * @shortdesc validates POST-vars and return result
		  * @public
		  * @type integer
		  * @return 0 = first call | 1 = valid submit | 2 = not valid | 3 = not valid and has reached maximum try's
		  *
		  **/
		function validate_submit()
		{
			if($this->check_captcha($this->public_K,$this->private_K))
			{
				if($this->debug) echo "\n<br>-Captcha-Debug: Validating submitted form returns: (1)";
				return 1;
			}
			else
			{
				if($this->current_try > $this->maxtry)
				{
					if($this->debug) echo "\n<br>-Captcha-Debug: Validating submitted form returns: (3)";
					return 3;
				}
				elseif($this->current_try > 0)
				{
					if($this->debug) echo "\n<br>-Captcha-Debug: Validating submitted form returns: (2)";
					return 2;
				}
				else
				{
					if($this->debug) echo "\n<br>-Captcha-Debug: Validating submitted form returns: (0)";
					return 0;
				}
			}
		}



	////////////////////////////////
	//
	//	PRIVATE METHODS
	//

		/** @private **/
		function display_captcha($onlyTheImage=FALSE)
		{
            //die($this->get_filename_url()); // Naveed
			$this->make_captcha();
			$is = getimagesize($this->get_filename());
			if($onlyTheImage) return "\n".'<img class="captchapict" src="'.$this->get_filename_url().'" '.$is[3].' alt="This is a captcha-picture. It is used to prevent mass-access by robots. (see: www.captcha.net)" title="">'."\n";
			else return $this->public_key_input()."\n".'<img class="captchapict" src="'.$this->get_filename_url().'" '.$is[3].' alt="This is a captcha-picture. It is used to prevent mass-access by robots. (see: www.captcha.net)" title="">'."\n";
		}

		/** @private **/
		function public_key_input()
		{
			return '<input type="hidden" name="public_key" value="'.$this->public_key.'">';
		}

		/** @private **/
		function make_captcha()
		{
			$private_key = $this->generate_private();
			if($this->debug) echo "\n<br>-Captcha-Debug: Generate private key: ($private_key)";

			// create Image and set the apropriate function depending on GD-Version & websafecolor-value
			if($this->gd_version >= 2 && !$this->websafecolors)
			{
				$func1 = 'imagecreatetruecolor';
				$func2 = 'imagecolorallocate';
			}
			else
			{
				$func1 = 'imageCreate';
				$func2 = 'imagecolorclosest';
			}
			$image = $func1($this->lx,$this->ly);
			if($this->debug) echo "\n<br>-Captcha-Debug: Generate ImageStream with: ($func1())";
			if($this->debug) echo "\n<br>-Captcha-Debug: For colordefinitions we use: ($func2())";


			// Set Backgroundcolor
			$this->random_color(224, 255);
			$back =  @imagecolorallocate($image, $this->r, $this->g, $this->b);
			@ImageFilledRectangle($image,0,0,$this->lx,$this->ly,$back);
			if($this->debug) echo "\n<br>-Captcha-Debug: We allocate one color for Background: (".$this->r."-".$this->g."-".$this->b.")";

			// allocates the 216 websafe color palette to the image
			if($this->gd_version < 2 || $this->websafecolors) $this->makeWebsafeColors($image);

			// fill with noise or grid
			if($this->nb_noise > 0)
			{
				// random characters in background with random position, angle, color
				if($this->debug) echo "\n<br>-Captcha-Debug: Fill background with noise: (".$this->nb_noise.")";
				for($i=0; $i < $this->nb_noise; $i++)
				{
					srand((double)microtime()*1000000);
					$size	= intval(rand((int)($this->minsize / 2.3), (int)($this->maxsize / 1.7)));
					srand((double)microtime()*1000000);
					$angle	= intval(rand(0, 360));
					srand((double)microtime()*1000000);
					$x		= intval(rand(0, $this->lx));
					srand((double)microtime()*1000000);
					$y		= intval(rand(0, (int)($this->ly - ($size / 5))));
					$this->random_color(160, 224);
					$color	= $func2($image, $this->r, $this->g, $this->b);
					srand((double)microtime()*1000000);
					$text	= chr(intval(rand(45,250)));
					@ImageTTFText($image, $size, $angle, $x, $y, $color, $this->change_TTF(), $text);
				}
			}
			else
			{
				// generate grid
				if($this->debug) echo "\n<br>-Captcha-Debug: Fill background with x-gridlines: (".(int)($this->lx / (int)($this->minsize / 1.5)).")";
				for($i=0; $i < $this->lx; $i += (int)($this->minsize / 1.5))
				{
					$this->random_color(160, 224);
					$color	= $func2($image, $this->r, $this->g, $this->b);
					@imageline($image, $i, 0, $i, $this->ly, $color);
				}
				if($this->debug) echo "\n<br>-Captcha-Debug: Fill background with y-gridlines: (".(int)($this->ly / (int)(($this->minsize / 1.8))).")";
				for($i=0 ; $i < $this->ly; $i += (int)($this->minsize / 1.8))
				{
					$this->random_color(160, 224);
					$color	= $func2($image, $this->r, $this->g, $this->b);
					@imageline($image, 0, $i, $this->lx, $i, $color);
				}
			}

			// generate Text
			if($this->debug) echo "\n<br>-Captcha-Debug: Fill forground with chars and shadows: (".$this->chars.")";
			for($i=0, $x = intval(rand($this->minsize,$this->maxsize)); $i < $this->chars; $i++)
			{
				$text	= strtoupper(substr($private_key, $i, 1));
				srand((double)microtime()*1000000);
				$angle	= intval(rand(($this->maxrotation * -1), $this->maxrotation));
				srand((double)microtime()*1000000);
				$size	= intval(rand($this->minsize, $this->maxsize));
				srand((double)microtime()*1000000);
				$y		= intval(rand((int)($size * 1.5), (int)($this->ly - ($size / 7))));
				$this->random_color(0, 127);
				$color	=  $func2($image, $this->r, $this->g, $this->b);
				$this->random_color(0, 127);
				$shadow = $func2($image, $this->r + 127, $this->g + 127, $this->b + 127);
				@ImageTTFText($image, $size, $angle, $x + (int)($size / 15), $y, $shadow, $this->change_TTF(), $text);
				@ImageTTFText($image, $size, $angle, $x, $y - (int)($size / 15), $color, $this->TTF_file, $text);
				$x += (int)($size + ($this->minsize / 5));
			}
			@ImageJPEG($image, $this->get_filename(), $this->jpegquality);
			$res = file_exists($this->get_filename());
			if($this->debug) echo "\n<br>-Captcha-Debug: Safe Image with quality [".$this->jpegquality."] as (".$this->get_filename().") returns: (".($res ? 'TRUE' : 'FALSE').")";
			@ImageDestroy($image);
			if($this->debug) echo "\n<br>-Captcha-Debug: Destroy Imagestream.";
			if(!$res) die('Unable to safe captcha-image.');
		}

		/** @private **/
		function makeWebsafeColors(&$image)
		{
			//$a = array();
			for($r = 0; $r <= 255; $r += 51)
			{
				for($g = 0; $g <= 255; $g += 51)
				{
					for($b = 0; $b <= 255; $b += 51)
					{
						$color = imagecolorallocate($image, $r, $g, $b);
						//$a[$color] = array('r'=>$r,'g'=>$g,'b'=>$b);
					}
				}
			}
			if($this->debug) echo "\n<br>-Captcha-Debug: Allocate 216 websafe colors to image: (".imagecolorstotal($image).")";
			//return $a;
		}

		/** @private **/
		function random_color($min,$max)
		{
			srand((double)microtime() * 1000000);
			$this->r = intval(rand($min,$max));
			srand((double)microtime() * 1000000);
			$this->g = intval(rand($min,$max));
			srand((double)microtime() * 1000000);
			$this->b = intval(rand($min,$max));
			//echo " (".$this->r."-".$this->g."-".$this->b.") ";
		}

		/** @private **/
		function change_TTF()
		{
			if(is_array($this->TTF_RANGE))
			{
				srand((float)microtime() * 10000000);
				$key = array_rand($this->TTF_RANGE);
				$this->TTF_file = $this->TTF_folder.$this->TTF_RANGE[$key];
			}
			else
			{
				$this->TTF_file = $this->TTF_folder.$this->TTF_RANGE;
			}
			return $this->TTF_file;
		}

		/** @private **/
		function check_captcha($public,$private)
		{
			// when check, destroy picture on disk
			if(file_exists($this->get_filename($public)))
			{
				$res = @unlink($this->get_filename($public)) ? 'TRUE' : 'FALSE';
				if($this->debug) echo "\n<br>-Captcha-Debug: Delete image (".$this->get_filename($public).") returns: ($res)";
			}
			$res = (strtolower($private)==strtolower($this->generate_private($public))) ? 'TRUE' : 'FALSE';
			if($this->debug) echo "\n<br>-Captcha-Debug: Comparing public with private key returns: ($res)";
			return $res == 'TRUE' ? TRUE : FALSE;
		}

		/** @private **/
		function get_filename($public="")
		{
			if($public=="") $public=$this->public_key;
            $public = $this->iRandomNumber;
			return $this->tempfolder.$public.".jpg";
		}

		/** @private **/
		function get_filename_url($public="")
		{
			if($public=="") $public = $this->public_key;
            $public = $this->iRandomNumber;
			//return str_replace($_SERVER['DOCUMENT_ROOT'],'',$this->tempfolder).$public.".jpg";
			return str_replace($_SERVER['DOCUMENT_ROOT'],'',$this->tempfolder2).$public.".jpg"; // Naveed
		}

		/** @private **/
		function get_try($in=TRUE)
		{
			$s = array();
			for($i = 1; $i <= $this->maxtry; $i++) $s[$i] = $i;

			if($in)
			{
				return (int)substr(strip_tags($_POST['hncaptcha']),($this->secretposition -1),1);
			}
			else
			{
				$a = "";
				$b = "";
				for($i = 1; $i < $this->secretposition; $i++)
				{
					srand((double)microtime()*1000000);
					$a .= $s[intval(rand(1,$this->maxtry))];
				}
				for($i = 0; $i < (32 - $this->secretposition); $i++)
				{
					srand((double)microtime()*1000000);
					$b .= $s[intval(rand(1,$this->maxtry))];
				}
				return $a.$this->current_try.$b;
			}
		}

		/** @private **/
		function get_gd_version()
		{
			static $gd_version_number = null;
			if($gd_version_number === null)
			{
			   ob_start();
			   phpinfo(8);
			   $module_info = ob_get_contents();
			   ob_end_clean();
			   if(preg_match("/\bgd\s+version\b[^\d\n\r]+?([\d\.]+)/i", $module_info, $matches))
			   {
				   $gd_version_number = $matches[1];
			   }
			   else
			   {
				   $gd_version_number = 0;
			   }
			}
			return $gd_version_number;
		}

		/** @private **/
		function generate_private($public="")
		{
			if($public=="") $public = $this->public_key;
			$key = substr(md5($this->key.$public), 16 - $this->chars / 2, $this->chars);
			return $key;
		}

		/**
		  *
		  * @shortdesc returns a message if the form validation has failed
		  * @private
		  * @type string
		  * @return string message or blankline as placeholder
		  *
		  **/
		function notvalid_msg()
		{
			// blank line for all languages
			if($this->current_try == 1) return '&nbsp;<br>&nbsp;';

			// invalid try's: en
			if($this->lang == "en" && $this->current_try > 2 && $this->refreshlink) return 'No valid entry. Please try again:<br>Tipp: If you cannot identify the chars, you can generate a new image!';
			if($this->lang == "en" && $this->current_try >= 2) return 'No valid entry. Please try again:<br>&nbsp;';

			// invalid try's: de
			if($this->lang == "de" && $this->current_try > 2 && $this->refreshlink) return 'Die Eingabe war nicht korrekt.<br>Tipp: Wenn Du die Zeichen nicht erkennen kannst, generiere neue mit dem Link unten!';
			if($this->lang == "de" && $this->current_try >= 2) return 'Die Eingabe war nicht korrekt. Bitte noch einmal versuchen:<br>&nbsp;';

		}


} // END CLASS hn_CAPTCHA



/**
  * PHP-Class hn_captcha_X1 Version 1.0, released 19-Apr-2004
  * is an extension for PHP-Class hn_captcha.
  * It adds a garbage-collector. (Useful, if you cannot use cronjobs.)
  * Author: Horst Nogajski, horst@nogajski.de
  *
  * License: GNU GPL (http://www.opensource.org/licenses/gpl-license.html)
  * Download: http://hn273.users.phpclasses.org/browse/package/1569.html
  *
  * If you find it useful, you might rate it on http://www.phpclasses.org/rate.html
  * If you use this class in a productional environment, you might drop me a note, so I can add a link to the page.
  *
  **/

/**
  * License: GNU GPL (http://www.opensource.org/licenses/gpl-license.html)
  *
  * This program is free software;
  *
  * you can redistribute it and/or modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2 of the License,
  * or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License along with this program;
  * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  *
  **/

/**
  * Tabsize: 4
  *
  **/



/**
  * This class is an extension for hn_captcha-class. It adds a garbage-collector!
  *
  * Normally all used images will be deleted automatically. But everytime a user
  * doesn't finish a request one image stays as garbage in tempfolder.
  * With this extension you can collect & trash this.
  *
  * You can specify:
  * - when the garbage-collector should run, (default = after 100 calls)
  * - the maxlifetime for images, (default is 600, = 10 minutes)
  * - a filename-prefix for the captcha-images (default = 'hn_captcha_')
  * - absolute filename for a textfile which stores the current counter-value
  *   (default is $tempfolder.'hn_captcha_counter.txt')
  *
  * The classextension needs the filename-prefix to identify lost images
  * also if the tempfolder is shared with other scripts.
  *
  * If an error occures (with counting or trash-file-deleting), the class sets
  * the variable $classhandle->garbage_collector_error to TRUE.
  * You can check this in your scripts and if is TRUE, you might execute
  * an email-notification or something else.
  *
  *
  * @shortdesc Class that adds a garbage-collector to the class hn_captcha
  * @public
  * @author Horst Nogajski, (mail: horst@nogajski.de)
  * @version 1.0
  * @date 2004-April-19
  *
  **/
class hn_captcha_X1 extends hn_captcha
{

	////////////////////////////////
	//
	//	PUBLIC PARAMS
	//

		/**
		  * @shortdesc You optionally can specify an absolute filename for the counter. If is not specified, the class use the tempfolder and the default_basename.
          * @public
          * @type string
          *
          **/
		public $counter_filename		= '';

		/**
		  * @shortdesc This is used as prefix for the picture filenames, so we can identify them also if we share the tempfolder with other programs.
          * @public
          * @type string
          *
          **/
		public $prefix					= 'hn_captcha_';

		/**
		  * @shortdesc The garbage-collector will started once when the class was called that number times.
          * @public
          * @type integer
          *
          **/
		public $collect_garbage_after	= 100;

		/**
		  * @shortdesc Only trash files which are older than this number of seconds.
          * @public
          * @type integer
          *
          **/
		public $maxlifetime			= 600;

		/**
		  * @shortdesc This becomes TRUE if the counter doesn't work or if trashfiles couldn't be deleted.
          * @public
          * @type boolean
          *
          **/
		public $garbage_collector_error	= FALSE;



	////////////////////////////////
	//
	//	PRIVATE PARAMS
	//

		/** @private **/
		public $counter_fn_default_basename = 'hn_captcha_counter.txt';




	////////////////////////////////
	//
	//	CONSTRUCTOR
	//

		/**
		  * @shortdesc This calls the constructor of main-class for extracting the config array and generating all needed params. Additionally it control the garbage-collector.
		  * @public
		  * @type void
		  * @return nothing
		  *
		  **/
		function hn_captcha_X1($config,$secure=TRUE)
		{
			// Call Constructor of main-class
			$this->hn_captcha($config,$secure);


			// specify counter-filename
			if($this->counter_filename == '') $this->counter_filename = $this->tempfolder.$this->counter_fn_default_basename;
			if($this->debug) echo "\n<br>-Captcha-Debug: The counterfilename is (".$this->counter_filename.")";


			// retrieve last counter-value
			$test = $this->txt_counter($this->counter_filename);

			// set and retrieve current counter-value
			$counter = $this->txt_counter($this->counter_filename,TRUE);


			// check if counter works correct
			if(($counter !== FALSE) && ($counter - $test == 1))
			{
				// Counter works perfect, =:)
				if($this->debug) echo "\n<br>-Captcha-Debug: Current counter-value is ($counter). Garbage-collector should start at (".$this->collect_garbage_after.")";

				// check if garbage-collector should run
				if($counter >= $this->collect_garbage_after)
				{
					// Reset counter
					if($this->debug) echo "\n<br>-Captcha-Debug: Reset the counter-value. (0)";
					$this->txt_counter($this->counter_filename,TRUE,0);

					// start garbage-collector
					$this->garbage_collector_error = $this->collect_garbage() ? FALSE : TRUE;
					if($this->debug && $this->garbage_collector_error) echo "\n<br>-Captcha-Debug: ERROR! SOME TRASHFILES COULD NOT BE DELETED! (Set the garbage_collector_error to TRUE)";
				}

			}
			else
			{
				// Counter-ERROR!
				if($this->debug) echo "\n<br>-Captcha-Debug: ERROR! NO COUNTER-VALUE AVAILABLE! (Set the garbage_collector_error to TRUE)";
				$this->garbage_collector_error = TRUE;
			}
		}


	////////////////////////////////
	//
	//	PRIVATE METHODS
	//


		/**
		  * @shortdesc Store/Retrieve a counter-value in/from a textfile. Optionally count it up or store a (as third param) specified value.
		  * @private
		  * @type integer
		  * @return counter-value
		  *
		  **/
		function txt_counter($filename,$add=FALSE,$fixvalue=FALSE)
		{
			if(is_file($filename) ? TRUE : touch($filename))
			{
				if(is_readable($filename) && is_writable($filename))
				{
					$fp = @fopen($filename, "r");
					if($fp)
					{
						$counter = (int)trim(fgets($fp));
						fclose($fp);

						if($add)
						{
							if($fixvalue !== FALSE)
							{
								$counter = (int)$fixvalue;
							}
							else
							{
								$counter++;
							}
							$fp = @fopen($filename, "w");
							if($fp)
							{
								fputs($fp,$counter);
								fclose($fp);
								return $counter;
							}
							else return FALSE;
						}
						else
						{
							return $counter;
						}
					}
					else return FALSE;
				}
				else return FALSE;
			}
			else return FALSE;
		}


		/**
		  * @shortdesc Scanns the tempfolder for jpeg-files with nameprefix used by the class and trash them if they are older than maxlifetime.
		  * @private
		  *
		  **/
		function collect_garbage()
		{
			$OK = FALSE;
			$captchas = 0;
			$trashed = 0;
			if($handle = @opendir($this->tempfolder))
			{
				$OK = TRUE;
				while(false !== ($file = readdir($handle)))
				{
					if(!is_file($this->tempfolder.$file)) continue;
					// check for name-prefix, extension and filetime
					if(substr($file,0,strlen($this->prefix)) == $this->prefix)
					{
						if(strrchr($file, ".") == ".jpg")
						{
							$captchas++;
							if((time() - filemtime($this->tempfolder.$file)) >= $this->maxlifetime)
							{
								$trashed++;
								$res = @unlink($this->tempfolder.$file);
								if(!$res) $OK = FALSE;
							}
						}
					}
				}
				closedir($handle);
			}
			if($this->debug) echo "\n<br>-Captcha-Debug: There are ($captchas) captcha-images in tempfolder, where ($trashed) are seems to be lost.";
			return $OK;
		}


		/** @private **/
		function get_filename($public="")
		{
			if($public=="") $public = $this->public_key;

            // Naveed
            $public = $this->iRandomNumber;
			return $this->tempfolder.$this->prefix.$public.".jpg";
		}


		/** @private **/
		function get_filename_url($public="")
		{
			if($public=="") $public = $this->public_key;
            $public = $this->iRandomNumber;
			//return str_replace($_SERVER['DOCUMENT_ROOT'],'',$this->tempfolder).$this->prefix.$public.".jpg";
			return str_replace($_SERVER['DOCUMENT_ROOT'],'',$this->tempfolder2).$this->prefix.$public.".jpg"; // Naveed
		}


} // END CLASS hn_CAPTCHA_X1

?>
