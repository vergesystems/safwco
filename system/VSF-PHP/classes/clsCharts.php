<?php

class clsCharts
{
    function __construct()
    {
    }

    function Initialize()
    {
   		include_once(cVSFFolder . "/components/FusionCharts/FusionChartsFree/Code/PHP/Includes/FusionCharts.php");

        $sReturn = '<script language="Javascript" src="' . cVSFFolder . '/components/FusionCharts/Code/FusionCharts/FusionCharts.js"></script>';
        return($sReturn);
    }

    function PieChart3D($aChartData, $iChartId, $sChartTitle, $iChartWidth, $iChartHeight, $sPaletteColors)
    {
        $sReturn = $this->Initialize();
        $sChartData = "<chart caption='$sChartTitle' paletteColors='" . $sPaletteColors . "' showPercentValues='1' labelSepChar=' - ' palette='4' decimals='0' enableSmartLabels='0' enableRotation='0' bgColor='FFFFFF' showBorder='0' startingAngle='30'>";
        for ($i=0; $i < count($aChartData); $i++)
            $sChartData .= "<set label='" . $aChartData[$i][0] . "' value='" . $aChartData[$i][1] . "' />";
        $sChartData .= "</chart>";

		//Create the chart - MS Column 3D Chart with data contained in strXML
		$sReturn .= renderChart(cVSFFolder . "/components/FusionCharts/Charts/Pie3D.swf", "", $sChartData, $iChartId, $iChartWidth, $iChartHeight, false, false);

        return($sReturn);
    }

    function MSColumn3D($aCategories, $aSeries, $iChartId, $sChartTitle, $iChartWidth, $iChartHeight, $sNumberPrefix = "$")
    {
        $sReturn = $this->Initialize();

        $sChartData = "<chart caption='" . $sChartTitle . "' shownames='1' showvalues='1' decimals='0' formatNumberScale='0' numberPrefix='" . $sNumberPrefix . "'>";

        // Categories
        $sChartData .= "<categories>";
        for ($i=0; $i < count($aCategories); $i++)
            $sChartData .= "<category label='" . $aCategories[$i] . "' />";
        $sChartData .= "</categories>";

        for ($i=0; $i < count($aSeries); $i++)
        {
            $sChartData .= "<dataset seriesName='" . $aSeries[$i][0] . "' color='" . $aSeries[$i][1] . "' showValues='0'>";
            for ($j=2; $j < count($aSeries[$i]); $j++)
                $sChartData .= "<set value='" . $aSeries[$i][$j] . "' />";
            $sChartData .= "</dataset>";
        }
        $sChartData .= "</chart>";

		//Create the chart - MS Column 3D Chart with data contained in strXML
		$sReturn .= renderChart(cVSFFolder . "/components/FusionCharts/Charts/MSColumn3D.swf", "", $sChartData, $iChartId, $iChartWidth, $iChartHeight, "0", "0");

        return($sReturn);
    }

    function Area2D($aChartData, $iChartId, $sChartTitle, $sChartSubTitle, $iChartWidth, $iChartHeight, $sXAxisName, $sCurrencySign = "$")
    {
        $sReturn = $this->Initialize();

        $sChartData = "<chart palette='2' caption='" . $sChartTitle . "' subcaption='" . $sChartSubTitle . "' formatNumberScale='0' xAxisName='" . $sXAxisName . "' yAxisName='Sales' numberPrefix='" . $sCurrencySign . "' showValues='0'>";

        for ($i=0; $i < count($aChartData); $i++)
            $sChartData .= "<set label='" . $aChartData[$i][0] . "' value='" . $aChartData[$i][1] . "' />";

        $sChartData .= "<styles><definition>";
        $sChartData .= "<style name='Anim1' type='animation' param='_xscale' start='0' duration='1' />";
        $sChartData .= "<style name='Anim2' type='animation' param='_alpha' start='0' duration='1' />";
        $sChartData .= "<style name='DataShadow' type='Shadow' alpha='20'/>";
		$sChartData .= "</definition>";
		$sChartData .= "<application>";
		$sChartData .= "<apply toObject='DIVLINES' styles='Anim1' />";
		$sChartData .= "<apply toObject='HGRID' styles='Anim2' />";
		$sChartData .= "<apply toObject='DATALABELS' styles='DataShadow,Anim2' />";
	    $sChartData .= "</application>";
        $sChartData .= "</styles>";
        $sChartData .= "</chart>";

		//Create the chart - MS Column 3D Chart with data contained in strXML
		$sReturn .= renderChart(cVSFFolder . "/components/FusionCharts/Charts/Area2D.swf", "", $sChartData, $iChartId, $iChartWidth, $iChartHeight, "0", "0");

        return($sReturn);
    }

}

class clsFusionCharts
{
    public $sFusionChartType;

    function __construct($sChartType = "Free")
    {
        $this->sFusionChartType = $sChartType;
    }

    function Initialize()
    {
        include_once(cVSFFolder . '/components/FusionCharts' . (($this->sFusionChartType == "Free") ? '/FusionChartsFree/' : '/')  . 'Code/PHP/Includes/FusionCharts.php');
        $sReturn = '<script language="Javascript" src="' . cVSFFolder . '/components/FusionCharts' . (($this->sFusionChartType == "Free") ? '/FusionChartsFree/' : '/')  . 'Code/FusionCharts/FusionCharts.js"></script>';

        return($sReturn);
    }

    function PieChart3D($aChartData, $iChartId, $sChartTitle, $iChartWidth, $iChartHeight)
    {
        $sReturn = $this->Initialize();

        $sChartData = "<graph pieSliceDepth='15' showhovercap='1' caption='$sChartTitle' showValues='0' showNames='1' formatNumberScale='0' showPercentageValues='0' decimalPrecision='0' bgColor='FFFFFF' showBorder='0'>";
        for ($i=0; $i < count($aChartData); $i++)
            $sChartData .= "<set name='" . str_replace("'", '', $aChartData[$i][0]) . "' value='" . $aChartData[$i][1] . "' color='" . $aChartData[$i][2] . "' />";
        $sChartData .= "</graph>";

		//Create the chart - MS Column 3D Chart with data contained in strXML
        $sReturn .= renderChart(cVSFFolder . '/components/FusionCharts' . (($this->sFusionChartType == "Free") ? '/FusionChartsFree/' : '/') . 'Charts/' . (($this->sFusionChartType == "Free") ? 'FCF_' : '') . 'Pie3D.swf', "", $sChartData, $iChartId, $iChartWidth, $iChartHeight, false, false);

        return($sReturn);
    }

    function PieChart2D($aChartData, $iChartId, $sChartTitle, $iChartWidth, $iChartHeight)
    {
        $sReturn = $this->Initialize();

        $sChartData = "<graph pieSliceDepth='15' showhovercap='1' caption='$sChartTitle' showValues='0' showNames='1' formatNumberScale='0' showPercentageValues='0' decimalPrecision='0' bgColor='FFFFFF' showBorder='0'>";
        for ($i=0; $i < count($aChartData); $i++)
            $sChartData .= "<set name='" . str_replace("'", '', $aChartData[$i][0]) . "' value='" . $aChartData[$i][1] . "' color='" . $aChartData[$i][2] . "' />";
        $sChartData .= "</graph>";

        $sReturn .= renderChart(cVSFFolder . '/components/FusionCharts' . (($this->sFusionChartType == "Free") ? '/FusionChartsFree/' : '/') . 'Charts/' . (($this->sFusionChartType == "Free") ? 'FCF_' : '') . 'Pie2D.swf', "", $sChartData, $iChartId, $iChartWidth, $iChartHeight, false, false);

        return($sReturn);
    }

    function Column3D($aChartData, $iChartId, $sChartTitle, $sYAxis, $iChartWidth, $iChartHeight, $sNumberPrefix = "$")
    {
        $sReturn = $this->Initialize();
        $iMinValue = 0;
        $iMaxValue = 0;

        for ($i=0; $i < count($aChartData); $i++)
        {
            if ($aChartData[$i][1] <= $iMinValue) $iMinValue = $aChartData[$i][1];
            if ($aChartData[$i][1] >= $iMaxValue) $iMaxValue = $aChartData[$i][1];

            $sChartItems .= "<set name='" . $aChartData[$i][0] . "' value='" . $aChartData[$i][1] . "' color='" . $aChartData[$i][2] . "' />";
        }

        if (($iMinValue == 0) && ($iMaxValue == 0)) $iMaxValue = 10;

        $sChartData .= "<graph yAxisMinValue='" . $iMinValue . "' yAxisMaxValue='" . $iMaxValue . "' yAxisName='" . $sYAxis . "' rotateNames='1' caption='" . $sChartTitle . "' shownames='1' showvalues='1' decimals='0' decimalPrecision='0' divlinedecimalPrecision='0' limitsdecimalPrecision='0' formatNumberScale='0' numberPrefix='" . $sNumberPrefix . "'>" . $sChartItems . "</graph>";

		//Create the chart - MS Column 3D Chart with data contained in strXML
        $sReturn .= renderChart(cVSFFolder . '/components/FusionCharts' . (($this->sFusionChartType == "Free") ? '/FusionChartsFree/' : '/') . 'Charts/' . (($this->sFusionChartType == "Free") ? 'FCF_' : '') . 'Column3D.swf', "", $sChartData, $iChartId, $iChartWidth, $iChartHeight, "0", "0");

        return($sReturn);
    }

    function MSColumn3D($aCategories, $aSeries, $aDataSet, $iChartId, $sChartTitle, $sYAxis, $iChartWidth, $iChartHeight, $sNumberPrefix = "$")
    {
        $sReturn = $this->Initialize();
        $iMinValue = 0;
        $iMaxValue = 0;

        // Categories
        $sCategories = '<categories>';
        for ($i=0; $i < count($aCategories); $i++)
            $sCategories .= "<category name='" . $aCategories[$i] . "' />";
        $sCategories .= "</categories>";

        for ($i=0; $i < count($aDataSet); $i++)
        {
            $sDataSet .= "<dataset seriesName='" . $aSeries[$i][0] . "' color='" .  $aSeries[$i][1] . "'>";

            for ($j=0; $j < count($aDataSet[$i]); $j++)
            {
                if ($aChartData[$i][$j] <= $iMinValue) $iMinValue = $aChartData[$i][$j];
                if ($aChartData[$i][$j] >= $iMaxValue) $iMaxValue = $aChartData[$i][$j];

                $sDataSet .= "<set value='" . $aDataSet[$i][$j] . "' />";
            }
            $sDataSet .= "</dataset>";
        }

        if (($iMinValue == 0) && ($iMaxValue == 0)) $iMaxValue = 10;

        $sChartData = "<graph yAxisMinValue='" . $iMinValue . "' yAxisMaxValue='" . $iMaxValue . "' yAxisName='" . $sYAxis . "' rotateNames='1' caption='" . $sChartTitle . "' shownames='1' showvalues='1' decimals='0' formatNumberScale='0' numberPrefix='" . $sNumberPrefix . "'>";
        $sChartData .= $sCategories;
        $sChartData .= $sDataSet;
        $sChartData .= "</graph>";

		//Create the chart - MS Column 3D Chart with data contained in strXML
        $sReturn .= renderChart(cVSFFolder . '/components/FusionCharts' . (($this->sFusionChartType == "Free") ? '/FusionChartsFree/' : '/') . 'Charts/' . (($this->sFusionChartType == "Free") ? 'FCF_' : '') . 'MSColumn3D.swf', "", $sChartData, $iChartId, $iChartWidth, $iChartHeight, "0", "0");

        return($sReturn);
    }

    function Area2D($aChartData, $iChartId, $sChartTitle, $sChartSubTitle, $iChartWidth, $iChartHeight, $sXAxisName, $sCurrencySign = "$")
    {
        $sReturn = $this->Initialize();

        $sChartData = "<chart palette='2' caption='" . $sChartTitle . "' subcaption='" . $sChartSubTitle . "' formatNumberScale='0' xAxisName='" . $sXAxisName . "' yAxisName='Sales' numberPrefix='" . $sCurrencySign . "' showValues='0'>";

        for ($i=0; $i < count($aChartData); $i++)
            $sChartData .= "<set label='" . $aChartData[$i][0] . "' value='" . $aChartData[$i][1] . "' />";

        $sChartData .= "<styles><definition>";
        $sChartData .= "<style name='Anim1' type='animation' param='_xscale' start='0' duration='1' />";
        $sChartData .= "<style name='Anim2' type='animation' param='_alpha' start='0' duration='1' />";
        $sChartData .= "<style name='DataShadow' type='Shadow' alpha='20'/>";
		$sChartData .= "</definition>";
		$sChartData .= "<application>";
		$sChartData .= "<apply toObject='DIVLINES' styles='Anim1' />";
		$sChartData .= "<apply toObject='HGRID' styles='Anim2' />";
		$sChartData .= "<apply toObject='DATALABELS' styles='DataShadow,Anim2' />";
	    $sChartData .= "</application>";
        $sChartData .= "</styles>";
        $sChartData .= "</chart>";

		//Create the chart - MS Column 3D Chart with data contained in strXML
		$sReturn .= renderChart(cVSFFolder . "/components/FusionCharts/Charts/Area2D.swf", "", $sChartData, $iChartId, $iChartWidth, $iChartHeight, "0", "0");

        return($sReturn);
    }

    function CustomChart($sChartType, $sChartData, $iChartWidth, $iChartHeight)
    {
        $sReturn = $this->Initialize();
        $sReturn .= renderChart(cVSFFolder . '/components/FusionCharts' . (($this->sFusionChartType == "Free") ? '/FusionChartsFree/' : '/') . 'Charts/' . (($this->sFusionChartType == "Free") ? 'FCF_' : '') . $sChartType . '.swf', "", $sChartData, rand(0, 1000), $iChartWidth, $iChartHeight, "0", "0");

        return($sReturn);
    }
}

/*
class clsCharts_FusionChartsFree
{
    function Pie3D()
    {
        $sReturn = '<script language="javascript" src="' . cVSFFolder . '/components/FusionCharts.js"></script>

<script type="text/javascript" src="../assets/prettify/prettify.js"></script>
<link rel="stylesheet" type="text/css" href="../assets/prettify/prettify.css" />
</head>
<body>



<div id="free">
  <div id="chart1div" style="width:600px; height: 350px">
    <p>FusionCharts needs Adobe Flash Player to run. If you're unable to see the chart here, it means that your browser does not seem to have the Flash Player Installed. You can downloaded it <a href="http://www.adobe.com/products/flashplayer/" target="_blank"><u>here</u></a> for free.</p>
  </div>
  <h1>Pie 3D Chart</h1>
  <p><a href="show.asp?id=19&view=xml" >View XML</a> | <a href="show.asp?id=19&view=v3">View FusionCharts v3 chart</a></p>
</div>
<script language="javascript" type="text/javascript">
var chart1 = new FusionCharts("charts/FCF_Pie3D.swf", "sampleChart", "600", "350");
chart1.setDataURL("data/Pie3D2.xml");

chart1.render("chart1div");
</script>
<div style="display:none">
<!-- Google Analytics code -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost
	+ "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
	var pageTracker = _gat._getTracker("UA-215295-3");
	pageTracker._initData();
	pageTracker._setDomainName("none");
	pageTracker._setAllowLinker(true);
	pageTracker._trackPageview();
}
catch(err) {}
</script>
<!-- End of Google Analytics code -->


<!-- LeadForce1 -->
<a href="http://www.leadforce1.com" title="Web analytics" onclick="window.open(this.href);return(false);">
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://vlog.leadforce1.com/" : "http://vlog.leadforce1.com/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "bf/bf.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://vlog.leadforce1.com/" : "http://vlog.leadforce1.com/");
<!--
bf_action_name = '';
bf_idsite = 5973;
bf_url = pkBaseURL+'bf/bf.php';
setTimeout('bf_log("' + bf_action_name+'",'+ bf_idsite+',"'+ bf_url +'")',0);
//-->
</script></a>
<!-- /LeadForce1 -->
</div>
</body>
</html>';

        return($sReturn);

    }
}
*/
?>