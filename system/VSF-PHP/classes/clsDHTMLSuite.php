<?php

class clsDHTMLSuite
{
    public $sDHTMLSuiteFolder;

    function __construct($sDHTMLSuiteFolder = "")
    {
        if ($sDHTMLSuiteFolder == '')
            $this->sDHTMLSuiteFolder = cVSFFolder . "/components/dhtmlsuite";
        else
            $this->sDHTMLSuiteFolder = $sDHTMLSuiteFolder;
    }

    function Calendar($sDateFieldName, $bFirst = true, $iFormNumber = 0, $iDisplayTimeBar = 0, $bDisplayDateFormat = true)
    {
    	$sDisplayTimeBar = ($iDisplayTimeBar == 0) ? "false" : "true";

        if ($bFirst)
        {
            $sReturn = '<script type="text/javascript" src="' . $this->sDHTMLSuiteFolder . '/js/separateFiles/dhtmlSuite-common.js"></script>
            <script type="text/javascript" src="' . $this->sDHTMLSuiteFolder . '/js/separateFiles/dhtmlSuite-calendar.js"></script>
            <script type="text/javascript" src="' . $this->sDHTMLSuiteFolder . '/js/separateFiles/dhtmlSuite-dragDropSimple.js"></script>';

            $sReturn .= '<script type="text/javascript">
        	var DHTML_SUITE_THEME_FOLDER = "' . $this->sDHTMLSuiteFolder . '/themes/";
        	var calendarObjForForm = new DHTMLSuite.calendar({minuteDropDownInterval:10,numberOfRowsInHourDropDown:5,callbackFunctionOnDayClick:\'getDateFromCalendar\',isDragable:true,displayTimeBar:' . $sDisplayTimeBar . '});

        	function pickDate(buttonObj,inputObject)
        	{
        		calendarObjForForm.setCalendarPositionByHTMLElement(inputObject,0,inputObject.offsetHeight+2);	// Position the calendar right below the form input
        		calendarObjForForm.setInitialDateFromInput(inputObject,\'yyyy-mm-dd hh:ii\');	// Specify that the calendar should set it\'s initial date from the value of the input field.
        		calendarObjForForm.addHtmlElementReference(\'' . $sDateFieldName . '\',inputObject);	// Adding a reference to this element so that I can pick it up in the getDateFromCalendar below(myInput is a unique key)
        		if(calendarObjForForm.isVisible()){
        			calendarObjForForm.hide();
        		}else{
        			calendarObjForForm.resetViewDisplayedMonth();	// This line resets the view back to the inital display, i.e. it displays the inital month and not the month it displayed the last time it was open.
        			calendarObjForForm.display();
        		}
        	}

        	function getDateFromCalendar(inputArray)
        	{
        		var references = calendarObjForForm.getHtmlElementReferences(); // Get back reference to form field.
        		references.' . $sDateFieldName . '.value = inputArray.year + \'-\' + inputArray.month + \'-\' + inputArray.day ' . (($iDisplayTimeBar == 0) ? ';' : '+ \' \' + inputArray.hour + \':\' + inputArray.minute;') . '
        		calendarObjForForm.hide();
        	}
        	</script>';
        }
        else
            $sReturn = '&nbsp;';

        //$sReturn .= '<a href="#noanchor" onclick="pickDate(this,document.forms[' . $iFormNumber . '].' . $sDateFieldName . ');"><img src="' . $this->sDHTMLSuiteFolder . '/images/calendar.gif" valign="middle" border="0" alt="Pick a Date" title="Pick a Date" /></a>' . (($bDisplayDateFormat) ? '<br />(YYYY-MM-DD)' : '');
        $sReturn .= '<a href="#noanchor" onclick="pickDate(this,document.getElementById(\'' . $sDateFieldName . '\'));"><img src="' . $this->sDHTMLSuiteFolder . '/images/calendar.gif" valign="middle" border="0" alt="Pick a Date" title="Pick a Date" /></a>' . (($bDisplayDateFormat) ? '<br />(YYYY-MM-DD)' : '');

        return($sReturn);
    }


    function TabBar($aTabs, $sMenu = '', $sInternalTableWidth = "98%", $sFrameHeight = "520px")
    {
    	$sReturn = '<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
  <tr>
   <td>' . $sMenu  . '</td>
 </tr>
 <tr>
  <td>
   <table border="0" cellspacing="0" cellpadding="0" width="' . $sInternalTableWidth . '" align="center">
    <tr>
     <td>
      <!-- Start of Tabbed Control -->
	  <link rel="stylesheet" href="' . cVSFFolder . '/components/tabbar/css/tab-view.css" type="text/css" media="screen" />
	  <script type="text/javascript" language="JavaScript">
	   var TabBarFolder = "' . cVSFFolder . '/components/tabbar";
	  </script>
	  <script type="text/javascript" src="' . cVSFFolder . '/components/tabbar/js/ajax.js"></script>
	  <script type="text/javascript" src="' . cVSFFolder . '/components/tabbar/js/tab-view.js"></script>
	  <div id="tabContainer">
	   <div class="dhtmlgoodies_aTab"><iframe src="' . $aTabs[0][1] . '" frameborder="0" width="100%" height="' . $sFrameHeight . '" marginheight="0" marginwidth="0" scrolling="auto">Sorry Your Browser Does not Support Frames...</iframe></div>
	  </div>
	  <script type="text/javascript">
	   initTabs("tabContainer",Array("' . $aTabs[0][0] . '"),0,"100%","100%",Array(false));
	   ';

    	for ($i=1; $i < count($aTabs); $i++)
    		$sReturn .= 'CreateTab("tabContainer", "' . $aTabs[$i][0] . '", "' . $aTabs[$i][1] . '", "' . $sFrameHeight . '", false);';

    	$sReturn .= '
	   showTab("tabContainer", 0);
	  </script>
	  <!-- End of Tabbar -->
     <br />
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>';

    	return($sReturn);
    }

	function DataGrid($aGridColumns, $aGridColumns_Width, $aGridColumns_Alignment, $aGridColumns_CellFormat, $aGridData, $iGridRowCount = 6, $iGridColumnCount = 4, $sGridWidth = "600px", $sGridHeight = "200px", $bGridEditable = true, $bSorting = true)
	{
		// Grid Columns
		for ($i=0; $i < count($aGridColumns); $i++)
		{
			if ($i > 0)
			{
				$sGridColumns .= ', ';
				$sCellFormat .= ', ';
			}

			$sGridColumns_Width .= '#myGrid .aw-column-' . $i . ' {width: ' . $aGridColumns_Width[$i] . ';}' . "\n";
			$sGridColumns_Alignment .= '#myGrid .aw-column-' . $i . ' {text-align: ' . $aGridColumns_Alignment[$i] . ';}' . "\n";
			
			$sCellFormat .= $aGridColumns_CellFormat[$i];
			
			$sGridColumns .= '"' . $aGridColumns[$i] . '"';
		}

		// Grid Data
		for ($i=0; $i < count($aGridData); $i++)
		{
			$sGridData .= '[';
			for ($j=0; $j < count($aGridData[$i]); $j++)
			{
				if ($j > 0) $sGridData .= ", ";
				$sGridData .= '"' . $aGridData[$i][$j] . '"';
			}
			$sGridData .= '],' . "\n";
		}

		$sReturn = '<!-- ActiveWidgets stylesheet and scripts -->
		<link href="' . cVSFFolder . '/components/activewidgets/runtime/styles/system/aw.css" rel="stylesheet" type="text/css" ></link>
		<script src="' . cVSFFolder . '/components/activewidgets/runtime/lib/aw2.js"></script>

		<!-- grid format -->
		<style>
		 #myGrid {height: ' . $sGridHeight . '; width: ' . $sGridWidth . ';}
		 #myGrid.aw-row-selector {text-align: center}

		 ' . $sGridColumns_Width . '
		 ' . $sGridColumns_Alignment . '
		 
		 #myGrid .aw-grid-cell {border-right: 1px solid #ccc;}
		 #myGrid .aw-grid-row {height: 20px; border-bottom: 1px solid #ccc;}
		 // #myGrid .aw-mouseover-row {background: #000000;}

		 /* box model fix for strict doctypes, safari */
		 .aw-strict #myGrid .aw-grid-cell {padding-right: 3px;}
		 .aw-strict #myGrid .aw-grid-row {padding-bottom: 3px;}

		
				
	    </style>

	    <!-- grid data -->
	    <script type="text/javascript" language="JavaScript">

	    // Row Count
	    var iRowCount = ' . $iGridRowCount . ';
	    
	    
		var myData = [
			' . $sGridData . '
		];

		var myColumns = [ ' . $sGridColumns . ' ];

		//	create ActiveWidgets Grid javascript object
		var obj = new AW.UI.Grid;
		obj.setId("myGrid");

		//	define data formats
		var str = new AW.Formats.String;
		var num = new AW.Formats.Number;

		obj.setCellFormat([' . $sCellFormat . ']);

		//	provide cells and headers text
		obj.setCellText(myData);
		obj.setHeaderText(myColumns);

		//	set number of rows/columns
		obj.setRowCount(iRowCount);
		obj.setColumnCount(' . $iGridColumnCount . ');

		//	enable row selectors
		obj.setSelectorVisible(true);
		obj.setSelectorText(function(i){return this.getRowPosition(i)+1});

		//	set headers width/height
		obj.setSelectorWidth(28);
		obj.setHeaderHeight(20);

		//	set row selection
		//obj.setSelectionMode("single-row");

		//	set click action handler
		obj.onCellClicked = function(event, col, row){window.status = this.getCellText(col, row)};
		obj.onRowDeleting = function(row){ return !confirm("Are you sure you want to delete the selected row?"); } 
		obj.onRowDeleted = function(row){ obj.setCellValue(0, 0, row); } 
		//obj.onCellTextChanged = function(){alert("data changed")}
		
		' . (($bSorting == false) ? 'obj.onHeaderClicked = function(event,index) { return "disabled"; }' : '') . '

		obj.setCellEditable(' . (($bGridEditable == true) ? 'true' : 'false') . ');
		
		
		//	write grid html to the page
		document.write(obj);

		function GetGridData()
		{
			//aData = MultiDimensionalArray(obj.getRowCount(), obj.getColumnCount());
			var sReturn = "";

			//for(var i=0; i < obj.getRowCount(); i++)
			for(var i=0; i < iRowCount; i++)
			{
				if ((obj.getCellValue(0, i) > 0) && (obj.getCellValue(0, i) != ""))
				{
	  				for(var j=0; j < obj.getColumnCount(); j++)
  					{
  						//aData[i][j] = obj.getCellValue(j, i);
						sReturn += obj.getCellValue(j, i) + "|";
		  			}

					sReturn += "~";
				}
			}

			return(sReturn);
		}

		function AddNewRow()
		{
			//iRowCount++;
			//obj.setRowProperty("count", iRowCount); 
    		//obj.refresh(); 
			
			obj.addRow(iRowCount++); 
		}
		
		function DeleteRow()
		{ 
			var i = obj.getCurrentRow(); 
			obj.deleteRow(i); 
			
			//obj.setRowProperty("count", iRowCount); 
			obj.refresh(); 
		} 

		</script>';

		return($sReturn);
	}
	
	function ActiveCalendar($aEvents = "", $iDay = "", $iMonth = "", $iYear = "", $iEmployeeId = '')
	{
		$myurl = '?id=' . $iEmployeeId; //$_SERVER['PHP_SELF']; // the links url is this page
		
		if ($iDay == "") $iDay = date("d");
		if ($iMonth == "") $iMonth = date("m");
		if ($iYear == "") $iYear = date("Y");

		include(cVSFFolder . '/components/activecalendar/activecalendar.php');
		$objCalendar = new activeCalendar($iYear, $iMonth, $iDay);

		if ($aEvents)
		{
			for($x=0; $x < count($aEvents); $x++)
				$objCalendar->setEventContent($aEvents[$x][0], $aEvents[$x][1], $aEvents[$x][2], $aEvents[$x][3], $aEvents[$x][4]);
		}
		
		$sReturn .= '<link rel="stylesheet" type="text/css" href="' . cVSFFolder . '/components/activecalendar/css/blue.css" />';
		$objCalendar->enableDayLinks($myurl); // this enables the month's day links
		$objCalendar->enableMonthNav($myurl); // this enables the month's navigation controls
		$sReturn .= $objCalendar->showMonth(); // this displays the month's view

		return($sReturn);
	}

    function FCKEditor($sTextAreaName, $sDefaultValue = "", $iRows = 10, $iColumns = 80, $sWidth = "100%", $sHeight = "200px", $sToolbarSet = "Basic", $bFirst = true)
    {
        if ($bFirst)
            $sReturn = '<script type="text/javascript" src="' . cVSFFolder . '/components/FCKeditor/fckeditor.js" language="JavaScript"></script>';

        $sReturn .= '
        <textarea name="' . $sTextAreaName . '" id="' . $sTextAreaName . '" rows="' . $iRows . '" cols="' . $iColumns . '" style="width: ' . $sWidth . '; height: ' . $sHeight . '">' . $sDefaultValue . '</textarea>
		<script type="text/javascript" language="JavaScript">
        //window.onload = function()
        //{
            var sBasePath = "' . cVSFFolder . '/components/FCKeditor/";
        	var oFCKeditor = new FCKeditor("' . $sTextAreaName . '");
        	oFCKeditor.BasePath	= sBasePath;
            oFCKeditor.ToolbarSet = "' . $sToolbarSet . '";
            oFCKeditor.Height = ' . str_replace('px', '', $sHeight) . ';
        	oFCKeditor.ReplaceTextarea();
        //}
    	</script>';

        return($sReturn);
    }

    function CKEditor($iUserId, $sTextAreaName, $sDefaultValue = "", $iRows = 10, $iColumns = 80, $sWidth = "100%", $sHeight = "200px", $sToolbarSet = "Basic", $bFirst = true)
    {
        if ($bFirst)
	    {
            $sReturn = '<script type="text/javascript" src="' . cVSFFolder . '/components/CKEditor/ckeditor.js" language="JavaScript"></script>
	        <script type="text/javascript" src="' . cVSFFolder . '/components/CKEditor/CKFinder/ckfinder.js"></script>';
	    }

        $sToolbar = '';

        if ($sToolbarSet == "Basic")
        {
            $sToolbar = ',
                toolbar : [ [ \'Source\', \'-\', \'Bold\', \'Italic\', \'Underline\', \'Strike\',\'-\',\'Link\', \'-\' ] ]';
        }

        $sReturn .= '
        <textarea name="' . $sTextAreaName . '" id="' . $sTextAreaName . '" rows="' . $iRows . '" cols="' . $iColumns . '" style="width: ' . $sWidth . '; height: ' . $sHeight . '">' . $sDefaultValue . '</textarea>
		<script type="text/javascript" language="JavaScript">
		//<![CDATA[

		var editor = CKEDITOR.replace(\'' . $sTextAreaName . '\',
		{
			skin : \'office2003\'' . $sToolbar . '
		});
		//]]>';

        $_SESSION["UserFiles"] = cUserFiles . '/' . $iUserId . '/';
        $_SESSION["UserFilesURL"] = '../../system/data/userfiles/' . $iUserId . '/';  // Naveed

        if ($sToolbarSet != "Basic")
        {
            $sReturn .= '
            // This is a check for the CKEditor class. If not defined, the paths must be checked.
            if ( typeof CKEDITOR == \'undefined\' )
            {
                alert("CKEditor not found...");
            }
            else
            {
    	        //var editor = CKEDITOR.replace( \'' . $sTextAreaName . '\' );
	            //editor.setData( \'<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>\' );

            	// Just call CKFinder.SetupCKEditor and pass the CKEditor instance as the first argument.
	            // The second parameter (optional), is the path for the CKFinder installation (default = "/ckfinder/").

                CKFinder.SetupCKEditor( editor, \'' . cVSFFolder . '/components/CKEditor/CKFinder/\' ) ;

            	// It is also possible to pass an object with selected CKFinder properties as a second argument.
	            //  CKFinder.SetupCKEditor( editor, { BasePath : \'' . cVSFFolder . '/components/CKEditor/\', RememberLastFolder : false } ) ;
            }';
        }

        $sReturn .= '</script>';

        return($sReturn);
    }

    function YUI_AutoComplete($sInputName, $sHiddenFieldName, $sDataSource, $sDefaultInputValue = "", $sDefaultHiddenFieldValue = "", $sContainer = "myContainer", $bFirst = true, $sStyleSheet = "")
    {
        $sReturn = '';
        if ($bFirst)
        {
            $sReturn = '<link rel="stylesheet" type="text/css" href="' . cVSFFolder . '/components/YahooUI/build/fonts/fonts-min.css" />
            <link rel="stylesheet" type="text/css" href="' . cVSFFolder . '/components/YahooUI/build/autocomplete/assets/skins/sam/autocomplete.css" />
            <script type="text/javascript" src="' . cVSFFolder . '/components/YahooUI/build/yahoo-dom-event/yahoo-dom-event.js"></script>
            <script type="text/javascript" src="' . cVSFFolder . '/components/YahooUI/build/connection/connection-min.js"></script>
            <script type="text/javascript" src="' . cVSFFolder . '/components/YahooUI/build/animation/animation-min.js"></script>
            <script type="text/javascript" src="' . cVSFFolder . '/components/YahooUI/build/datasource/datasource-min.js"></script>
            <script type="text/javascript" src="' . cVSFFolder . '/components/YahooUI/build/autocomplete/autocomplete-min.js"></script>';
        }

        $sReturn .= '<table border="0" cellspacing="0" cellpadding="0" align="left" class="yui-skin-sam">
         <tr>
          <td>
           <div style="width:15em; padding-bottom:2em;">
            <input id="' . $sInputName . '" name="' . $sInputName . '" value="' . $sDefaultInputValue . '" type="text" ' . (($sStyleSheet != "") ? 'style="' . $sStyleSheet . '"' : 'class="form1"') . ' />
            <input id="' . $sHiddenFieldName . '" name="' . $sHiddenFieldName . '" value="' . $sDefaultHiddenFieldValue . '" type="hidden" />
        	<div id="' . $sContainer . '"></div>
           </div>
          </td>
         </tr>
        </table>

        <script type="text/javascript">
        YAHOO.example.BasicRemote = function()
        {
            // Use an XHRDataSource
            var oDS = new YAHOO.util.XHRDataSource("' . $sDataSource . '");

            // Set the responseType
            oDS.responseType = YAHOO.util.XHRDataSource.TYPE_TEXT;

            // Define the schema of the delimited results
            oDS.responseSchema = {
                recordDelim: "\n",
                fieldDelim: "|"
            };

            // Enable caching
            oDS.maxCacheEntries = 5;

            // Instantiate the AutoComplete
            var oAC = new YAHOO.widget.AutoComplete("' . $sInputName . '", "' . $sContainer . '", oDS);

            // Define an event handler to populate a hidden form field when an item gets selected
            var myHiddenField = YAHOO.util.Dom.get("' . $sHiddenFieldName . '");
            var myHandler = function(sType, aArgs)
            {
                var myAC = aArgs[0]; // reference back to the AC instance
                var elLI = aArgs[1]; // reference to the selected LI element
                var oData = aArgs[2]; // object literal of selected item\'s result data

                // update hidden form field with the selected item ID
                myHiddenField.value = oData[1];
            };
            oAC.itemSelectEvent.subscribe(myHandler);

            oAC.animVert = false;
            oAC.animHoriz = false;
            oAC.useShadow = true;
            oAC.typeAhead = true;

            return {
                oDS: oDS,
                oAC: oAC
            };
        }();
        </script>';

        return($sReturn);
    }

    function Dojo_DialogBox_Setup()
    {
        $sReturn = '<script type="text/javascript">
		  dojo.require("dijit.Dialog");
		</script>';

        return($sReturn);
    }

    // &nbsp;<img onmouseover="dijit.byId(\'Test1\').show()" src="../images/icons/iconQuestion.gif" border="0" align="absmiddle" alt="Employee Information" />
    function Dojo_DialogBox_Show($sDialogId, $sDialogTitle, $sContents)
    {
        $sReturn = '<div dojoType="dijit.Dialog" id="' . $sDialogId . '" title="' . $sDialogTitle . '" class="nihilo">
         ' . $sContents . '
        </div> ';

        return($sReturn);
    }
}

?>