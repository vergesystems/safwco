<?php

class clsDateTime
{
    public $aMonths;

    function __construct()
    {
        $this->aMonths = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

    }

    function GetListBoxOfMonths($iSelectedMonth, $sSelectId, $sStylesheet = "")
    {
        $sReturn = '<select name="' . $sSelectId . '" ' . $sStylesheet . ' id="' . $sSelectId . '">';
        for ($i=0; $i < count($this->aMonths); $i++)
            $sReturn .= '<option ' . (($iSelectedMonth == ($i+1)) ? 'selected="true"' : '') . ' value="' . ($i+1) . '">' . $this->aMonths[$i] . '</option>';
        $sReturn .= '</select>';

        return($sReturn);
    }

    function GetListBoxOfYears($iStartYear, $iEndYear, $iSelectedYear, $sSelectId, $sStylesheet = "")
    {
        $sReturn = '<select name="' . $sSelectId . '" ' . $sStylesheet . ' id="' . $sSelectId . '">';
        for ($i=$iStartYear; $i <= $iEndYear; $i++)
        {
            $sReturn .= '<option ' . (($iSelectedYear == ($iStartYear+$iCounter)) ? 'selected="true"' : '')  . ' value="' . ($iStartYear+$iCounter) . '">' . ($iStartYear+$iCounter) . '</option>';
            $iCounter++;
        }

        $sReturn .= '</select>';

        return($sReturn);
    }

    function GetListBoxOfDates($iSelectedDate, $sSelectId, $sStylesheet = "")
    {
        $sReturn = '<select name="' . $sSelectId . '" ' . $sStylesheet . ' id="' . $sSelectId . '">';
        for ($i=1; $i <= 31; $i++)
            $sReturn .= '<option ' . (($iSelectedDate == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $i . '</option>';
        $sReturn .= '</select>';

        return($sReturn);
    }

    function GetDate($iStartYear, $iEndYear, $iSelectedDate, $iSelectedMonth, $iSelectedYear, $sSelectDateId, $sSelectMonthId, $sSelectYearId, $sStylesheet = "")
    {
        $sReturn = $this->GetListBoxOfDates($iSelectedDate, $sSelectDateId, $sStylesheet) . '&nbsp;&nbsp;';
        $sReturn .= $this->GetListBoxOfMonths($iSelectedMonth, $sSelectMonthId, $sStylesheet) . '&nbsp;&nbsp;';
        $sReturn .= $this->GetListBoxOfYears($iStartYear, $iEndYear, $iSelectedYear, $sSelectYearId, $sStylesheet);

        return($sReturn);
    }

    function GetTime($sSelectHourId, $sSelectMinuteId, $sSelectAMPM, $iSelectedHour = 1, $iSelectedMinute = 1, $iSelectedAMPM = 'am', $sStylesheet = "")
    {
        $sHour = '<select name="' . $sSelectHourId . '" ' . $sStylesheet . ' id="' . $sSelectHourId . '">';
        for ($i=1; $i <= 12; $i++)
        {
            if ($i < 10) $iHour = '0' . $i;
            else $iHour = $i;

            $sHour .= '<option ' . (($iSelectedHour == $iHour) ? 'selected="true"' : '') . ' value="' . $iHour . '">' . $iHour . '</option>';
        }
        $sHour .= '</select>';

        $sMinute = '<select name="' . $sSelectMinuteId . '" ' . $sStylesheet . ' id="' . $sSelectMinuteId . '">';
        for ($i=0; $i <= 59; $i++)
        {
            if ($i < 10) $iMinute = '0' . $i;
            else $iMinute = $i;

            $sMinute .= '<option ' . (($iSelectedMinute == $iMinute) ? 'selected="true"' : '') . ' value="' . $iMinute . '">' . $iMinute . '</option>';
        }
        $sMinute .= '</select>';

        $sAMPM = '<select name="' . $sSelectAMPM . '" ' . $sStylesheet . ' id="' . $sSelectAMPM . '">';
        $sAMPM .= '<option ' . (($iSelectedAMPM == 'am') ? 'selected="true"' : '') . ' value="am">am</option>';
        $sAMPM .= '<option ' . (($iSelectedAMPM == 'pm') ? 'selected="true"' : '') . ' value="pm">pm</option>';
        $sAMPM .= '</select>';

        return($sHour . ' : ' . $sMinute . ' ' . $sAMPM);
    }
}


?>