<?php

# #########################################
# Error Handler Class
# by Naveed Memon - naveedmemon@yahoo.com
#
# Simply include this file and it will take care of all of the errors, errors are emailed to the administrator
# They can also be sent as an SMS message as CC. More documentation coming soon...
# #########################################

// REQUIRED PHP HEADERS FOR DEBUG SET ALL TO 1
ini_set("assert.warning", "1");
ini_set("display_errors", "1");
ini_set("display_startup_errors", "1");
ini_set("html_errors", "1");

// ERROR Handler Definitions
define('E_USER_ALL',    E_USER_NOTICE | E_USER_WARNING | E_USER_ERROR);
define('E_NOTICE_ALL',  E_NOTICE | E_USER_NOTICE);
define('E_WARNING_ALL', E_WARNING | E_USER_WARNING | E_CORE_WARNING | E_COMPILE_WARNING);
define('E_ERROR_ALL',   E_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_USER_ERROR);
define('E_NOTICE_NONE', E_ALL & ~E_NOTICE_ALL);
define('E_DEBUG',       0x10000000);
define('E_VERY_ALL',    E_ERROR_ALL | E_WARNING_ALL | E_NOTICE_ALL | E_DEBUG);

$old_error_handler = set_error_handler("fnErrorHandler");

// error handler function
function fnErrorHandler($errno, $errstr, $errfile, $errline, $smtp = false)
{
	global $objErrorHandler;

	if ($errno != 8)
		$objErrorHandler->fnTrapError($errno, $errstr, $errfile, $errline, $smtp);
}

class clsErrorHandler
{
	public $varErrorEmailAddress;
	public $varErrorEmailName;
	public $varErrorEmailCCAddress;
	public $varErrorSubject;
	public $varDebug;
    public $varSMTP;

	// Constructor
	function __construct($varEmail, $varEmailName, $varEmailSubject, $varEmailCC = "", $varSMTP = false)
	{
		$this->varErrorEmailAddress = $varEmail;
		$this->varErrorEmailName = $varEmailName;
		$this->varErrorEmailCCAddress = $varEmailCC;
		$this->varErrorSubject = $varEmailSubject;
		$this->varDebug = false;

        $this->varSMTP = $varSMTP;
	}

	function fnTriggerError($varErrorMessage)
	{
		trigger_error($varErrorMessage);
	}

	function fnTrapError($errno, $errstr, $errfile, $errline, $smtp = false)
	{
		global $objGeneral;

		switch ($errno)
		{
			/*case FATAL:
				$varErrorMessage = "<b>FATAL ERROR: </b> [$errno] $errstr<br />\n
					  Fatal error in line $errline of file $errfile
					  , PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n
					  Aborting...<br />\n";
				break;
            */
			case E_ERROR:
				$varErrorMessage = "<b>ERROR</b> [$errno] $errstr<br />\n";
				break;

			case E_WARNING:
				$varErrorMessage = "<b>WARNING</b> [$errno] $errstr<br />\n";
				break;

			default:
				$varErrorMessage = "[$errno] $errstr<br />\n";
				break;
		}

		$varError = '
<html><body><pre>
<b>Error Generated in E-Commerce System</b><hr size=1>
Server      : ' . $_SERVER["SERVER_NAME"] . '
Date &amp; Time : ' . date("F j, Y, g:i a") . '
Error       : ' . trim($varErrorMessage) . '
Line        : ' . $errline . '
FileName    : ' . $errfile . '
Script      : ' . $_SERVER["SCRIPT_NAME"] . '
Remote Host : ' . $_SERVER["REMOTE_ADDR"] . '
Query String: ' . $_SERVER["QUERY_STRING"] . '
Arguments   : <br><br><br><b>REQUEST VARIABLES</b><hr size=1><br>';

		foreach($_REQUEST as $key => $value)
			$varError .= '            : <b>' . $key . '</b> = ' . $value . '<br>';

		$varError .= '<br><br><b>SERVER VARIABLES</b><hr size=1><br>';
		foreach($_SERVER as $key => $value)
			$varError .= '            : <b>' . $key . '</b> = ' . $value . '<br>';

		$varError .= '<br><br><b>ENVIRONMENT VARIABLES</b><hr size=1><br>';
		foreach($_ENV as $key => $value)
			$varError .= '            : <b>' . $key . '</b> = ' . $value . '<br>';

		$varError .= '</pre></body></html>';

		print('<br><span class="color:maroon">We\'re Sorry, this site has generated an error.<br>The error message has been logged and emailed to the Webmaster.<br>This problem will be fixed within 24 hours...</span>');

		if ($this->varDebug)
			print($varError);
		else
		{
			$objGeneral->fnSendMail('Verge Error Handler', 'bugs@vergesystems.com', $this->varErrorEmailName, $this->varErrorEmailAddress, $this->varErrorSubject, $varError, true, '', $smtp);
    	}
		exit(0);
	}
}
?>