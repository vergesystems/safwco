<?php

/*
 *Class for download / upload files
 * Protocols: FTP, FTPS, HTTP, HTTPS and SSH2
 * Compatibility * PHP version 4 or higher
 * @ Author Leonardo Gon�alves Ramos <leogr2003@yahoo.com.br>
 * @ Copyright Copyright (c) 2009, Leonardo Gon�alves Ramos
 * /
*/

class Transmission {


/*
 * properties
 */

	/*
	 * Protocol for transmission
	 */
	var $protocol;

	/*
	 * IP or hostname of the remote server
	 */
	var $server;

	/*
	 * Directory of the remote file
	 *
	 */
	var $dirRemoteFile;

	/*
	 * Transmission user
	 */
	var $user;

	/*
	 * Transmission password
	 */
	var $password;

	/*
	 * Connection port
	 */
	var $conPort;

	/*
	 * Exceptions
	 */
	var $exceptions = Array();



/*
 * 	METHODS
 */

	/*
	 * Class Constructor	 
	 * $protocol: Ex.: "SSH2" or "FTP"	 
	 */
	function Transmission ($protocol) {
		switch ($protocol) {
			case "SSH2":
				$this->dirRemoteFile = "ssh2.sftp://";
				$this->conPort = ":22";
				break;
			case "FTP":
				$this->dirRemoteFile = "ftp://";
				$this->conPort = ":21";		
				break;
			case "FTPS":
				$this->dirRemoteFile = "ftps://";
				$this->conPort = ":21";		
				break;
			case "HTTP":
				$this->dirRemoteFile = "http://";
				$this->conPort = ":80";		
				break;
			case "HTTPS":
				$this->dirRemoteFile = "https://";
				$this->conPort = ":443";		
				break;
			default:
				$this->dirRemoteFile = null;
				break;						
		}
	}
/*
 * Validate the connection to remote server 
 * $server: eg: "exemplo.com.br" or "192.168.0.125" 
 * $dirRemoteFile: ex: /home/files/file.pdf 
 * $user: optional parameter 
 * $password: optional parameter
 * $port: Optional parameter
*/
	function setConnection($server,$remoteFile,$user,$password,$port) {

		/*
		 * Protocol is invalid
		 */
		if($this->dirRemoteFile==null) {
			$this->setExceptions("Error: Protocol is invalid.");
		}

		/*
		 * Format address of the server
		 */
		if($server=="") {
			$this->setExceptions("Error: The address of the remote server has not.");
		} else {
			$this->server = $server;
		}

		/*
		 * Format remote directory
		 */
		if($remoteFile=="") {
			$this->setExceptions("Error: The path / file was not specified.");
		} else {
			if(substr($remoteFile,0,1)!="/") {
				$remoteFile = "/".$remoteFile;
			}
		}

		/*
		 * If no username or password, the connection will be as public
		 */
		if(is_null($user) OR is_null($password)) {
			$this->dirRemoteFile .= $this->server.$this->conPort.$remoteFile;
		} else {
			$this->dirRemoteFile .= $user.":".$password."@".$server.$this->conPort.$remoteFile;
		}
	}
	
	/*
	 * Method to receive exceptions
	 */
	function setExceptions($msg) {
			array_push($this->exceptions, $msg);
	}
	
	/*
	 * Returns all exceptions generated
	 */
	function getExceptions() {
			$num = 0;
			foreach ($this->exceptions as $exc) {
				$ar_exc .= "$num) ".$exc;
				$num++;
			}
			return $ar_exc;
	}
}


/*
 *Class for upload files
 * Protocols: FTP, FTPS, HTTP, HTTPS and SSH2
 * Compatibility * PHP version 4 or higher
 * @ Author Leonardo Gon�alves Ramos <leogr2003@yahoo.com.br>
 * @ Copyright Copyright (c) 2009, Leonardo Gon�alves Ramos
*/
class Download extends Transmission {

	/*
	 * Content of file
	 */
	var $content;

	/*
	 * Class Constructor
	 */
	function Download($protocol) {
		parent::Transmission($protocol);
	}

	/*
	 * See 'setConnection' of Transmission
	 */
	function setConnection($server,$remoteFile,$user,$password,$port) {
		parent::setConnection($server,$remoteFile,$user,$password,$port);
	}

	/*
	 *  Check the file to download
	 */
	function start() {

		if(is_file(parent::$this->dirRemoteFile)) {
			$this->setContent(parent::$this->dirRemoteFile);
			return true;
		} else {
			parent::setExceptions("Error: File not found.");
			return false;
		}
	}

	/*
	 * Set file content
	 */
	function setContent($dir) {
		$this->content = $dir;
	}

	/*
	 * Content for download
	 */
	function getContent() {
		$file_name = basename(parent::$this->dirRemoteFile);

		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=".$file_name);
		header("Content-Transfer-Encoding: binary");

		return readfile($this->content);
	}

	/*
	 * Return exceptions
	 */
	function getExceptions() {
		return parent::getExceptions();
	}

}

/*
 *Class for upload files
 * Protocols: FTP, FTPS, HTTP, HTTPS and SSH2
 * Compatibility * PHP version 4 or higher
 * @ Author Leonardo Gon�alves Ramos <leogr2003@yahoo.com.br>
 * @ Copyright Copyright (c) 2009, Leonardo Gon�alves Ramos
*/
class Upload extends Transmission {

	/*
	 * Directory of your site
	 */
	var $dirLocalFile;

	/*
	 * Class Constructor
	 */
	function Upload($protocol) {
		parent::Transmission($protocol);
	}

	function setConnection($dirLocalFile,$server,$remoteFile,$user,$password,$port) {

		if($dirLocalFile=="") {
			parent::setExceptions("Error: The file / site not specified.");
		} else {
			$this->dirLocalFile = $dirLocalFile;
		}
		parent::setConnection($server,$remoteFile,$user,$password,$port);

	}
	/*
	 * $dirFile: Directory/File. Ex.: /My Documents/webFiles/document.pdf
	 * $fileSize: Specifies the maximum size of the file to validate upload (optional)
	 * $arrayExtensions: Array of valid extensions to upload (optional)
	 */
	function fileCheck($dirFile,$fileSize,$arrayExtensions) {
		if(filesize($dirFile)>$fileSize) {
			parent::setExceptions("Error: The file size exceeded the permitted limit.");
			return false;
		} else {
			$extValidate = false;
			foreach($arrayExtensions as $pos => $ext) {
				$file_array = pathinfo($dirFile);
				if($ext==$file_array['extension']) {
					$extValidate = true;
					return true;
					break;
				}
			}
			if(!$extValidate) {
				parent::setExceptions("Error: the file extension is invalid.");
				return false;
			}
		}
	}

	/*
	 * Starts the File Upload
	 * $file: If $file is "MOVE", the method will delete the file folder location.
	 * If $file is "COPY" will be maintained in the file folder location.
	*/
	function start($file) {

		/*
		 * Copy (transmits) the file to the remote server
		 */
		if(!copy($this->dirLocalFile,parent::$this->dirRemoteFile)) {
			parent::setExceptions("Error: Unable to copy the server certificate for the documents. Please contact the administrator of the GED.");
			return false;
		} else {
			if($file=="MOVE" OR is_null($file) OR (!isset($file))) {
				if(!unlink($this->dirLocalFile)) {
					parent::setExceptions("Error: Unable to remove the file's folder location.");
					return false;
				}
				else {
					return true;
				}
			} else {
				return true;
			}
		}
	}

	function getExceptions() {
		return parent::getExceptions();
	}

}

?>