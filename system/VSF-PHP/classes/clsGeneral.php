<?php
# ######################################
# PHP General Functions Class
# by Naveed Memon: naveedmemon@gmail.com
# ######################################

class clsGeneral
{
    public $aUSStatesList;
    public $aCurrencies;
    public $aCountriesList;

    public $aTimeZones;

    public $aNationalityList;

    function __construct()
    {
    	$this->aCountriesList = array(
        "ac" => "Ascension Island",
        "ad" => "Andorra",
        "ae" => "United Arab Emirates",
        "af" => "Afghanistan",
        "ag" => "Antigua and Barbuda",
        "ai" => "Anguilla",
        "al" => "Albania",
        "am" => "Armenia",
        "an" => "Netherlands Antilles",
        "ao" => "Angola",
        "aq" => "Antarctica",
        "ar" => "Argentina",
        "as" => "American Samoa",
        "at" => "Austria",
        "au" => "Australia",
        "aw" => "Aruba",
        "az" => "Azerbaijan",
        "ba" => "Bosnia and Herzegovina",
        "bb" => "Barbados",
        "bd" => "Bangladesh",
        "be" => "Belgium",
        "bf" => "Burkina Faso",
        "bg" => "Bulgaria",
        "bh" => "Bahrain",
        "bi" => "Burundi",
        "bj" => "Benin",
        "bm" => "Bermuda",
        "bn" => "Brunei Darussalam",
        "bo" => "Bolivia",
        "br" => "Brazil",
        "bs" => "Bahamas",
        "bt" => "Bhutan",
        "bv" => "Bouvet Island",
        "bw" => "Botswana",
        "by" => "Belarus",
        "bz" => "Belize",
        "ca" => "Canada",
        "cc" => "Cocos (Keeling) Islands",
        "cd" => "Congo, Democratic Republic of the",
        "cf" => "Central African Republic",
        "cg" => "Congo, Republic of",
        "ch" => "Switzerland",
        "ci" => "Cote d'Ivoire",
        "ck" => "Cook Islands",
        "cl" => "Chile",
        "cm" => "Cameroon",
        "cn" => "China",
        "co" => "Colombia",
        "cr" => "Costa Rica",
        "cu" => "Cuba",
        "cv" => "Cap Verde",
        "cx" => "Christmas Island",
        "cy" => "Cyprus",
        "cz" => "Czech Republic",
        "de" => "Germany",
        "dj" => "Djibouti",
        "dk" => "Denmark",
        "dm" => "Dominica",
        "do" => "Dominican Republic",
        "dz" => "Algeria",
        "ec" => "Ecuador",
        "ec" => "England",
        "ee" => "Estonia",
        "eg" => "Egypt",
        "eh" => "Western Sahara",
        "er" => "Eritrea",
        "es" => "Spain",
        "et" => "Ethiopia",
        "fi" => "Finland",
        "fj" => "Fiji",
        "fk" => "Falkland Islands (Malvina)",
        "fm" => "Micronesia, Federal State of",
        "fo" => "Faroe Islands",
        "fr" => "France",
        "ga" => "Gabon",
        "gd" => "Grenada",
        "ge" => "Georgia",
        "gf" => "French Guiana",
        "gg" => "Guernsey",
        "gh" => "Ghana",
        "gi" => "Gibraltar",
        "gl" => "Greenland",
        "gm" => "Gambia",
        "gn" => "Guinea",
        "gp" => "Guadeloupe",
        "gq" => "Equatorial Guinea",
        "gr" => "Greece",
        "gs" => "South Georgia",
        "gt" => "Guatemala",
        "gu" => "Guam",
        "gw" => "Guinea-Bissau",
        "gy" => "Guyana",
        "hk" => "Hong Kong",
        "hm" => "Heard and McDonald Islands",
        "hn" => "Honduras",
        "hr" => "Croatia/Hrvatska",
        "ht" => "Haiti",
        "hu" => "Hungary",
        "id" => "Indonesia",
        "ie" => "Ireland",
        "il" => "Israel",
        "im" => "Isle of Man",
        "in" => "India",
        "io" => "British Indian Ocean Territory",
        "iq" => "Iraq",
        "ir" => "Iran (Islamic Republic of)",
        "is" => "Iceland",
        "it" => "Italy",
        "je" => "Jersey",
        "jm" => "Jamaica",
        "jo" => "Jordan",
        "jp" => "Japan",
        "ke" => "Kenya",
        "kg" => "Kyrgyzstan",
        "kh" => "Cambodia",
        "ki" => "Kiribati",
        "km" => "Comoros",
        "kn" => "Saint Kitts and Nevis",
        "kp" => "Korea, Democratic People's Republic",
        "kr" => "Korea, Republic of",
        "kw" => "Kuwait",
        "ky" => "Cayman Islands",
        "kz" => "Kazakhstan",
        "la" => "Lao People's Democratic Republic",
        "lb" => "Lebanon",
        "lc" => "Saint Lucia",
        "li" => "Liechtenstein",
        "lk" => "Sri Lanka",
        "lr" => "Liberia",
        "ls" => "Lesotho",
        "lt" => "Lithuania",
        "lu" => "Luxembourg",
        "lv" => "Latvia",
        "ly" => "Libyan Arab Jamahiriya",
        "ma" => "Morocco",
        "mc" => "Monaco",
        "md" => "Moldova, Republic of",
        "mg" => "Madagascar",
        "mh" => "Marshall Islands",
        "mk" => "Macedonia, Former Yugoslav Republic",
        "ml" => "Mali",
        "mm" => "Myanmar",
        "mn" => "Mongolia",
        "mo" => "Macau",
        "mp" => "Northern Mariana Islands",
        "mq" => "Martinique",
        "mr" => "Mauritania",
        "ms" => "Montserrat",
        "mt" => "Malta",
        "mu" => "Mauritius",
        "mv" => "Maldives",
        "mw" => "Malawi",
        "mx" => "Mexico",
        "my" => "Malaysia",
        "mz" => "Mozambique",
        "na" => "Namibia",
        "nc" => "New Caledonia",
        "ne" => "Niger",
        "nf" => "Norfolk Island",
        "ng" => "Nigeria",
        "ni" => "Nicaragua",
        "nl" => "Netherlands",
        "no" => "Norway",
        "np" => "Nepal",
        "nr" => "Nauru",
        "nu" => "Niue",
        "nz" => "New Zealand",
        "om" => "Oman",
        "pa" => "Panama",
        "pe" => "Peru",
        "pf" => "French Polynesia",
        "pg" => "Papua New Guinea",
        "ph" => "Philippines",
        "pk" => "Pakistan",
        "pl" => "Poland",
        "pm" => "St. Pierre and Miquelon",
        "pn" => "Pitcairn Island",
        "pr" => "Puerto Rico",
        "ps" => "Palestinian Territories",
        "pt" => "Portugal",
        "pw" => "Palau",
        "py" => "Paraguay",
        "qa" => "Qatar",
        "re" => "Reunion Island",
        "ro" => "Romania",
        "ru" => "Russian Federation",
        "rw" => "Rwanda",
        "sa" => "Saudi Arabia",
        "sb" => "Solomon Islands",
        "sc" => "Seychelles",
        "sd" => "Sudan",
        "se" => "Sweden",
        "sg" => "Singapore",
        "sh" => "St. Helena",
        "si" => "Slovenia",
        "sj" => "Svalbard and Jan Mayen Islands",
        "sk" => "Slovak Republic",
        "sl" => "Sierra Leone",
        "sm" => "San Marino",
        "sn" => "Senegal",
        "so" => "Somalia",
        "sr" => "Suriname",
        "st" => "Sao Tome and Principe",
        "sv" => "El Salvador",
        "sy" => "Syrian Arab Republic",
        "sz" => "Swaziland",
        "tc" => "Turks and Caicos Islands",
        "td" => "Chad",
        "tf" => "French Southern Territories",
        "tg" => "Togo",
        "th" => "Thailand",
        "tj" => "Tajikistan",
        "tk" => "Tokelau",
        "tm" => "Turkmenistan",
        "tn" => "Tunisia",
        "to" => "Tonga",
        "tp" => "East Timor",
        "tr" => "Turkey",
        "tt" => "Trinidad and Tobago",
        "tv" => "Tuvalu",
        "tw" => "Taiwan",
        "tz" => "Tanzania",
        "ua" => "Ukraine",
        "ug" => "Uganda",
        "ua" => "United Arab Emirates",
        "uk" => "United Kingdom",
        "um" => "US Minor Outlying Islands",
        "us" => "United States",
        "uy" => "Uruguay",
        "uz" => "Uzbekistan",
        "va" => "Holy See (City Vatican State)",
        "vc" => "Saint Vincent and the Grenadines",
        "ve" => "Venezuela",
        "vg" => "Virgin Islands (British)",
        "vi" => "Virgin Islands (USA)",
        "vn" => "Vietnam",
        "vu" => "Vanuatu",
        "wf" => "Wallis and Futuna Islands",
        "ws" => "Western Samoa",
        "ye" => "Yemen",
        "yt" => "Mayotte",
        "yu" => "Yugoslavia",
        "za" => "South Africa",
        "zm" => "Zambia",
        "zw" => "Zimbabwe");
        
    	$this->aCurrencies = array(
    	"USD" => "United States Dollars (USD)",
    	"EUR" => "Euro (EUR)",
    	"GBP" => "United Kingdom Pounds (GBP)",
    	"CAD" => "Canada Dollars (CAD)",
        "PKR" => "Pakistan Rupees (PKR)",
    	"---" => "--------------------",
    	"ARS" => "Argentina Pesos (ARS)",
		"AUD" => "Australia Dollars (AUD)",
		"BRL" => "Brazil Reais (BRL)",
		"CLP" => "Chile Pesos (CLP)",
		"CNY" => "China Yuan Renminbi (CNY)",
		"CYP" => "Cyprus Pounds (CYP)",
		"CZK" => "Czech Republic Koruny (CZK)",
		"DKK" => "Denmark Kroner (DKK)",
		"EEK" => "Estonia (EEK)",
		"HKD" => "Hong Kong Dollars (HKD)",
		"HUF" => "Hungary Forint (HUF)",
		"IDR" => "Indonesian Rupiah (IDR)",
		"INR" => "India Rupees (INR)",
		"ISK" => "Iceland Krona (ISK)",
		"JMD" => "Jamaica Dollars (JMD)",
		"JPY" => "Japan Yen (JPY)",
		"LVL" => "Latvia Lati (LVL)",
		"LTL" => "Lithuania Litai (LTL)",
		"MTL" => "Malta Liri (MTL)",
		"MXN" => "Mexico Pesos (MXN)",
		"MYR" => "Malaysia (MYR)",
		"NZD" => "New Zealand Dollars (NZD)",
		"NOK" => "Norway Kroner (NOK)",
		"PLN" => "Poland Zlotych (PLN)",
		"SGD" => "Singapore Dollars (SGD)",
		"SKK" => "Slovakia Koruny (SKK)",
		"SIT" => "Slovenia Tolars (SIT)",
		"ZAR" => "South Africa Rand (ZAR)",
		"KRW" => "South Korea Won (KRW)",
		"SEK" => "Sweden Kronor (SEK)",
		"CHF" => "Switzerland Francs (CHF)",
		"TWD" => "Taiwan New Dollars (TWD)",
		"UYU" => "Uruguay Pesos (UYU)",
        "VND" => "Vietnamese Dong (VND)");

		$this->aNationalityList = array("", "Afghan", "Albanian", "Algerian", "American", "Andorran", "Angolan", "Argentine",
		"Armenian", "Australian", "Austrian", "Azerbaijani", "Bahamian", "Bahraini", "Bangladesh", "Barbadian", "Belarusian",
		"Belgian", "Belizean", "Beninese", "Bermudian", "Bhutanese", "Bosnian", "Botswanan", "Brazilian", "Bruneian", "Bulgarian", "Burkinabe",
		"Burmese", "Burundian", "Cambodian", "Cameroonian", "Canadia", "Cape Verdian", "Caymanian", "Central African", "Chadian",
		"Chilean", "Chinese", "Christmas Islander", "Colombian", "Comoran", "Congolese", "Congolese", "Costa Rican", "Croat",
		"Cuban", "Cypriot", "Czech", "Danish", "Djibouti", "Dominican", "Dutch", "Dutch Antillean", "East Timorese", "Ecuadorian",
		"Egyptian", "Emirati",  "English", "Equatorial Guinean", "Eritrean", "Estonian", "Ethiopian", "Falkland Islander", "Faroese",
		"Fijian", "Filipino", "Finnish", "French", "French Polynesian", "Gabonese", "Gambian", "Georgian", "German",
		"Ghanaian", "Greek", "Greenlander", "Grenadian", "Guatemalan", "Guinea-Bissauan", "Guinean", "Guyanese", "Haitian",
		"Honduran", "Hungarian", "I-Kiribati", "Icelandic", "Indian", "Indonesian", "Iranian", "Iraqi", "Irish", "Israeli",
		"Italian", "Ivorian", "Jamaican", "Japanese", "Jordanian", "Kazakh", "Kenyan", "Kittitian", "Kuwaiti", "Kyrgyz", "Laotian",
		"Latvian", "Lebanese", "Liberian", "Libyan", "Liechtensteinish", "Lithuanian", "Luxembourgian", "Macedonian", "Malagasy",
		"Malawian", "Malaysian", "Maldivan", "Malian", "Maltese", "Marshallese", "Mauritanian", "Mauritian", "Mexican", "Micronesian",
		"Moldovan", "Monegasque", "Mongolian", "Montenegrin", "Montserratian", "Morrocan", "Mosotho", "Mozambican", "Namibian",
		"Nauruan", "Nepalese", "New Caledonian", "New Zealander", "Ni-Vanuatu", "Nicaraguan", "Nigerian", "Nigerien", "Niuean",
		"Norfolk Islander", "North Korean", "Norwegian", "Omani", "Other", "Pakistani", "Palauan", "Panamanian", "Papua New Guinean",
		"Paraguayan", "Peruvian", "Polish", "Portuguese", "Puerto Rican", "Qatari", "Romanian", "Russian", "Rwandan", "Saint Lucian",
		"Salvadoran", "Sammarinese", "Samoan", "Sao Tomean", "Saudi", "Scottish", "Senegalese", "Serbian", "Seychellois", "Sierra Leonean",
		"Singaporean", "Slovakian", "Slovenian", "Solomon Islander", "Somalian", "South African", "South Korean", "Spanish",
		"Sri Lankan", "Sudanese", "Surinamer", "Swazi", "Swedish", "Swiss", "Syrian", "Taiwanese", "Tajik", "Tanzanian",
		"Thai", "Togolese", "Tongan", "Trinidadian", "Tunisian", "Turkish", "Turkmenistani", "Ugandan", "Ukrainian","Uruguayan",
		"Uzbekistani", "Venezuelan", "Vietnamese", "Welsh", "Yemeni", "Zambian", "Zimbabwean");

        $this->aUSStatesList = array(
		"al" => "Alabama",
		"ak" => "Alaska",
		"ar" => "Arkansas",
		"az" => "Arizona",
		"ca" => "California",
		"co" => "Colorado",
		"ct" => "Connecticut",
		"de" => "Delaware",
		"fl" => "Florida",
		"ga" => "Georgia",
		"hi" => "Hawaii",
		"ia" => "Iowa",
		"id" => "Idaho",
		"il" => "Illinois",
		"in" => "Indiana",
		"ks" => "Kansas",
		"ky" => "Kentucky",
		"la" => "Louisiana",
		"ma" => "Massachusetts",
		"md" => "Maryland",
		"me" => "Maine",
		"mi" => "Michigan",
		"mn" => "Minnesota",
		"mo" => "Missouri",
		"ms" => "Mississippi",
		"mt" => "Montana",
		"nc" => "North Carolina",
		"nd" => "North Dakota",
		"ne" => "Nebraska",
		"nh" => "New Hampshire",
		"nj" => "New Jersey",
		"nm" => "New Mexico",
		"nv" => "Nevada",
		"ny" => "New York",
		"oh" => "Ohio",
		"ok" => "Oklahoma",
		"or" => "Oregon",
		"ot" => "Other",
		"pa" => "Pennsylvania",
		"ri" => "Rhode Island",
		"sc" => "South Carolina",
		"sd" => "South Dakota",
		"tn" => "Tennessee",
		"tx" => "Texas",
		"ut" => "Utah",
		"va" => "Virginia",
		"vt" => "Vermont",
		"wa" => "Washington",
		"wi" => "Wisconsin",
		"wv" => "West Virginia",
		"wy" => "Wyoming");

        $this->aTimeZones = array('Kwajalein' => '(GMT-12:00) International Date Line West',
		'Pacific/Midway' => '(GMT-11:00) Midway Island',
		'Pacific/Samoa' => '(GMT-11:00) Samoa',
		'Pacific/Honolulu' => '(GMT-10:00) Hawaii',
		'America/Anchorage' => '(GMT-09:00) Alaska',
		'America/Los_Angeles' => '(GMT-08:00) Pacific Time (US &amp; Canada)',
		'America/Tijuana' => '(GMT-08:00) Tijuana, Baja California',
		'America/Denver' => '(GMT-07:00) Mountain Time (US &amp; Canada)',
		'America/Chihuahua' => '(GMT-07:00) Chihuahua',
		'America/Mazatlan' => '(GMT-07:00) Mazatlan',
		'America/Phoenix' => '(GMT-07:00) Arizona',
		'America/Regina' => '(GMT-06:00) Saskatchewan',
		'America/Tegucigalpa' => '(GMT-06:00) Central America',
		'America/Chicago' => '(GMT-06:00) Central Time (US &amp; Canada)',
		'America/Mexico_City' => '(GMT-06:00) Mexico City',
		'America/Monterrey' => '(GMT-06:00) Monterrey',
		'America/New_York' => '(GMT-05:00) Eastern Time (US &amp; Canada)',
		'America/Bogota' => '(GMT-05:00) Bogota',
		'America/Lima' => '(GMT-05:00) Lima',
		'America/Rio_Branco' => '(GMT-05:00) Rio Branco',
		'America/Indiana/Indianapolis' => '(GMT-05:00) Indiana (East)',
		'America/Caracas' => '(GMT-04:30) Caracas',
		'America/Halifax' => '(GMT-04:00) Atlantic Time (Canada)',
		'America/Manaus' => '(GMT-04:00) Manaus',
		'America/Santiago' => '(GMT-04:00) Santiago',
		'America/La_Paz' => '(GMT-04:00) La Paz',
		'America/St_Johns' => '(GMT-03:30) Newfoundland',
		'America/Argentina/Buenos_Aires' => '(GMT-03:00) Georgetown',
		'America/Sao_Paulo' => '(GMT-03:00) Brasilia',
		'America/Godthab' => '(GMT-03:00) Greenland',
		'America/Montevideo' => '(GMT-03:00) Montevideo',
		'Atlantic/South_Georgia' => '(GMT-02:00) Mid-Atlantic',
		'Atlantic/Azores' => '(GMT-01:00) Azores',
		'Atlantic/Cape_Verde' => '(GMT-01:00) Cape Verde Is.',
		'Europe/Dublin' => '(GMT) Dublin',
		'Europe/Lisbon' => '(GMT) Lisbon',
		'Europe/London' => '(GMT) London',
		'Africa/Monrovia' => '(GMT) Monrovia',
		'Atlantic/Reykjavik' => '(GMT) Reykjavik',
		'Africa/Casablanca' => '(GMT) Casablanca',
		'Europe/Belgrade' => '(GMT+01:00) Belgrade',
		'Europe/Bratislava' => '(GMT+01:00) Bratislava',
		'Europe/Budapest' => '(GMT+01:00) Budapest',
		'Europe/Ljubljana' => '(GMT+01:00) Ljubljana',
		'Europe/Prague' => '(GMT+01:00) Prague',
		'Europe/Sarajevo' => '(GMT+01:00) Sarajevo',
		'Europe/Skopje' => '(GMT+01:00) Skopje',
		'Europe/Warsaw' => '(GMT+01:00) Warsaw',
		'Europe/Zagreb' => '(GMT+01:00) Zagreb',
		'Europe/Brussels' => '(GMT+01:00) Brussels',
		'Europe/Copenhagen' => '(GMT+01:00) Copenhagen',
		'Europe/Madrid' => '(GMT+01:00) Madrid',
		'Europe/Paris' => '(GMT+01:00) Paris',
		'Africa/Algiers' => '(GMT+01:00) West Central Africa',
		'Europe/Amsterdam' => '(GMT+01:00) Amsterdam',
		'Europe/Berlin' => '(GMT+01:00) Berlin',
		'Europe/Rome' => '(GMT+01:00) Rome',
		'Europe/Stockholm' => '(GMT+01:00) Stockholm',
		'Europe/Vienna' => '(GMT+01:00) Vienna',
		'Europe/Minsk' => '(GMT+02:00) Minsk',
		'Africa/Cairo' => '(GMT+02:00) Cairo',
		'Europe/Helsinki' => '(GMT+02:00) Helsinki',
		'Europe/Riga' => '(GMT+02:00) Riga',
		'Europe/Sofia' => '(GMT+02:00) Sofia',
		'Europe/Tallinn' => '(GMT+02:00) Tallinn',
		'Europe/Vilnius' => '(GMT+02:00) Vilnius',
		'Europe/Athens' => '(GMT+02:00) Athens',
		'Europe/Bucharest' => '(GMT+02:00) Bucharest',
		'Europe/Istanbul' => '(GMT+02:00) Istanbul',
		'Asia/Jerusalem' => '(GMT+02:00) Jerusalem',
		'Asia/Amman' => '(GMT+02:00) Amman',
		'Asia/Beirut' => '(GMT+02:00) Beirut',
		'Africa/Windhoek' => '(GMT+02:00) Windhoek',
		'Africa/Harare' => '(GMT+02:00) Harare',
		'Asia/Kuwait' => '(GMT+03:00) Kuwait',
		'Asia/Riyadh' => '(GMT+03:00) Riyadh',
		'Asia/Baghdad' => '(GMT+03:00) Baghdad',
		'Africa/Nairobi' => '(GMT+03:00) Nairobi',
		'Asia/Tbilisi' => '(GMT+03:00) Tbilisi',
		'Europe/Moscow' => '(GMT+03:00) Moscow',
		'Europe/Volgograd' => '(GMT+03:00) Volgograd',
		'Asia/Tehran' => '(GMT+03:30) Tehran',
		'Asia/Muscat' => '(GMT+04:00) Muscat',
		'Asia/Baku' => '(GMT+04:00) Baku',
		'Asia/Yerevan' => '(GMT+04:00) Yerevan',
		'Asia/Yekaterinburg' => '(GMT+05:00) Ekaterinburg',
		'Asia/Karachi' => '(GMT+05:00) Karachi',
		'Asia/Tashkent' => '(GMT+05:00) Tashkent',
		'Asia/Kolkata' => '(GMT+05:30) Calcutta',
		'Asia/Colombo' => '(GMT+05:30) Sri Jayawardenepura',
		'Asia/Katmandu' => '(GMT+05:45) Kathmandu',
		'Asia/Dhaka' => '(GMT+06:00) Dhaka',
		'Asia/Almaty' => '(GMT+06:00) Almaty',
		'Asia/Novosibirsk' => '(GMT+06:00) Novosibirsk',
		'Asia/Rangoon' => '(GMT+06:30) Yangon (Rangoon)',
		'Asia/Krasnoyarsk' => '(GMT+07:00) Krasnoyarsk',
		'Asia/Bangkok' => '(GMT+07:00) Bangkok',
		'Asia/Jakarta' => '(GMT+07:00) Jakarta',
		'Asia/Brunei' => '(GMT+08:00) Beijing',
		'Asia/Chongqing' => '(GMT+08:00) Chongqing',
		'Asia/Hong_Kong' => '(GMT+08:00) Hong Kong',
		'Asia/Urumqi' => '(GMT+08:00) Urumqi',
		'Asia/Irkutsk' => '(GMT+08:00) Irkutsk',
		'Asia/Ulaanbaatar' => '(GMT+08:00) Ulaan Bataar',
		'Asia/Kuala_Lumpur' => '(GMT+08:00) Kuala Lumpur',
		'Asia/Singapore' => '(GMT+08:00) Singapore',
		'Asia/Taipei' => '(GMT+08:00) Taipei',
		'Australia/Perth' => '(GMT+08:00) Perth',
		'Asia/Seoul' => '(GMT+09:00) Seoul',
		'Asia/Tokyo' => '(GMT+09:00) Tokyo',
		'Asia/Yakutsk' => '(GMT+09:00) Yakutsk',
		'Australia/Darwin' => '(GMT+09:30) Darwin',
		'Australia/Adelaide' => '(GMT+09:30) Adelaide',
		'Australia/Canberra' => '(GMT+10:00) Canberra',
		'Australia/Melbourne' => '(GMT+10:00) Melbourne',
		'Australia/Sydney' => '(GMT+10:00) Sydney',
		'Australia/Brisbane' => '(GMT+10:00) Brisbane',
		'Australia/Hobart' => '(GMT+10:00) Hobart',
		'Asia/Vladivostok' => '(GMT+10:00) Vladivostok',
		'Pacific/Guam' => '(GMT+10:00) Guam',
		'Pacific/Port_Moresby' => '(GMT+10:00) Port Moresby',
		'Asia/Magadan' => '(GMT+11:00) Magadan',
		'Pacific/Fiji' => '(GMT+12:00) Fiji',
		'Asia/Kamchatka' => '(GMT+12:00) Kamchatka',
		'Pacific/Auckland' => '(GMT+12:00) Auckland',
		'Pacific/Tongatapu' => '(GMT+13:00) Nukualofa');
    }

	# ############################################################################
	# Function to Redirect to a new Location
	# Usage: $objClass->fnRedirect('http://www.yahoo.com');
	# ############################################################################
	function fnRedirect($varLocation, $bHTMLRedirect = false, $iHTMLRedirectSeconds = 0)
	{
		if ($bHTMLRedirect)
			print('<meta http-equiv="refresh" content="' . $iHTMLRedirectSeconds . ';URL=' . $varLocation . '">');
		else
			header("Location: ". $varLocation . "");

		exit();
	}

	function fnValidateEmail($varEmail)
	{
		/*if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $varEmail))
			return(true);
		else
			return(false);*/

        $pattern = '/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])' .
        '(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';

        return(preg_match($pattern, $varEmail));
	}

	//date difference function
	function DateDiff($interval, $date1, $date2)
	{
	   	// Function roughly equivalent to the ASP "DateDiff" function
	   	//convert the dates into timestamps
		$date1 = strtotime($date1);
		$date2 = strtotime($date2);
		$seconds = $date2 - $date1;

	   	//if $date1 > $date2
	   	//change substraction order
	   	//convert the diff to +ve integer

        // Removed by Naveed to show Negative Values too : 2008-09-06
        /*
	   	if ($seconds < 0)
	   	{
			   $tmp = $date1;
			   $date1 = $date2;
			   $date2 = $tmp;
			   $seconds = 0-$seconds;
	   	}*/

	   	//reconvert the timestamps into dates
	   	if ($interval =='y' || $interval=='m') {
		   $date1 = date("Y-m-d h:i:s", $date1);
		   $date2=  date("Y-m-d h:i:s", $date2);
	   	}

	   	switch($interval)
	   	{
		   case "y":
			   list($year1, $month1, $day1) = explode('-', $date1);
			   list($year2, $month2, $day2) = explode('-', $date2);
			   $time1 = (date('H',$date1)*3600) + (date('i',$date1)*60) + (date('s',$date1));
			   $time2 = (date('H',$date2)*3600) + (date('i',$date2)*60) + (date('s',$date2));
			   $diff = $year2 - $year1;

			   if($month1 > $month2) {
				   $diff -= 1;
			   } elseif($month1 == $month2) {
				   if($day1 > $day2) {
					   $diff -= 1;
				   } elseif($day1 == $day2) {
					   if($time1 > $time2) {
						   $diff -= 1;
					   }
				   }
			   }
			   break;
		   case "m":
			   list($year1, $month1, $day1) = explode('-', $date1);
			   list($year2, $month2, $day2) = explode('-',$date2);
			   $time1 = (date('H',$date1)*3600) + (date('i',$date1)*60) + (date('s',$date1));
			   $time2 = (date('H',$date2)*3600) + (date('i',$date2)*60) + (date('s',$date2));
			   $diff = ($year2 * 12 + $month2) - ($year1 * 12 + $month1);
			   if($day1 > $day2) {
				   $diff -= 1;
			   } elseif($day1 == $day2) {
				   if($time1 > $time2) {
					   $diff -= 1;
				   }
			   }
			   break;
		   case "w":
			   // Only simple seconds calculation needed from here on
			   $diff = floor($seconds / 604800);
			   break;
		   case "d":
			   $diff = floor($seconds / 86400);
			   break;
		   case "h":
			   $diff = floor($seconds / 3600);
			   break;
		   case "i":
			   $diff = floor($seconds / 60);
			   break;
		   case "s":
			   $diff = $seconds;
			   break;
	   }

	   return $diff;
	}

	# ############################################################################
	# Function to Find Date Difference
	# Usage: $objClass->fnNow();
	# ############################################################################
	function fnDateDiff($varDate1, $varDate2 = '')
	{
		// $varDate1 is subtracted from $varDate2
		// If $varDate2 is not specified, then current date is assumed.

		list($varDate1_Month, $varDate1_Day, $varDate1_Year) = explode('[/.-]', $varDate1);
		if (!$varDate2)
		{
			$varDate2_Year = date("Y");		// Current Year
			$varDate2_Month = date("m");		// Current Month
			$varDate2_Day = date("d");		// Current Date
		}
		else
			list($varDate2_Month, $varDate2_Day, $varDate2_Year) = explode('[/.-]', $varDate2);

		$varDate1 = mktime(0,0,0, $varDate1_Month, $varDate1_Day, $varDate1_Year);	// Get Unix Timestamp
		$varDate2 = mktime(0,0,0, $varDate2_Month, $varDate2_Day, $varDate2_Year);	// Get Unix Timestamp

		$varDifference = $varDate2 - $varDate1;
		return(floor($varDifference / 60 / 60 / 24));
	}

	# ############################################################################
	# Function to Current Date & Time in the format of MySQL DateTime Field
	# Usage: $objClass->fnNow();
	# ############################################################################
	function fnNow()
	{
		return(date("Y-m-d H:i:s"));
	}

	function fnSetCookie($varCookieName, $varCookieValue, $varCookieExpire)
	{
		setcookie($varCookieName, $varCookieValue, $varCookieExpire);
	}

	function fnGetCookie($varCookieName)
	{
		return($_COOKIE[$varCookieName]);
	}

	# ############################################################################
	# Function to Get QueryString and Form Post value, Default = AutoDetect
	# Usage: $objClass->fnGet("search"); same as $objClass->fnGet("search", "GET");
	#        -or- $objClass->fnGet("search", "POST");
	# ############################################################################
	function fnGet($varQuery, $varType = "AUTO")
	{
		if ($varType == "AUTO")
			if (isset($_GET["$varQuery"]))
				return($_GET["$varQuery"]);
			elseif (isset($_POST["$varQuery"]))
				return($_POST["$varQuery"]);
		elseif ($varType == "GET")
			if (isset($_GET["$varQuery"]))
				return($_GET["$varQuery"]);
		elseif ($varType == "POST")
			if (isset($_POST["$varQuery"]))
				return($_POST["$varQuery"]);

		return("");
	}


	# ############################################################################
	# Function to Generate Random Password of length n, Default = 6 characters
	# Usage: $objClass->fnRandomPasswordGenerator(16);
	# ############################################################################
	function fnRandomPasswordGenerator($length = 6)
	{
		$pass = '';
		$new = '';

		// all the chars we want to use
		$all = explode(" ",
		 "a b c d e f g h i j k l m n o p q r s t u v w x y z "
		 ."A B C D E F G H I J K L M N O P Q R S T U V W X Y Z "
		 ."0 1 2 3 4 5 6 7 8 9");

		for($i=0; $i < $length; $i++)
		{
			srand((double)microtime()*1000000);
			$pass .= $all[rand(0,61)];
			$arr[] = $all[rand(0,61)];
			$new .= $arr[$i];
		}

		return $new;
	}

	# ############################################################################
	# Function to Get a Value from XML Settings file provided as a second argument
	# Default is ../main/settings.xml: standard for all of our current projects
	# Usage: $objClass->fnGetProperty("SiteURL", "settings.xml");
	# ############################################################################
	function fnGetProperty($varPropName, $varXMLFile = '../main/settings.xml')
	{
		$objXML = new clsXML($varXMLFile);
		$varValue = $objXML->evaluate("//" . $varPropName);
		$varRetValue = $objXML->get_content($varValue[0]);
		$objXML = NULL;
		return($varRetValue);
	}

	# ############################################################################
	# Function to Set a Value from XML Settings file provided as a third argument
	# Default is ../main/settings.xml: standard for all of our current projects
	# Usage: $objClass->fnGetProperty("SiteURL", "http://www.yahoo.com", "settings.xml");
	# ############################################################################
	function fnSetProperty($varPropName, $varPropValue, $varXMLFile = '../main/settings.xml')
	{
		$objXML = new clsXML($varXMLFile);
		$varValue = $objXML->evaluate("//" . $varPropName);
		$objXML->set_content($varValue[0], $varPropValue);

		$varContents = $objXML->get_file();
		$file = fopen($varXMLFile, 'w');
		fwrite($file, '<?xml version="1.0"?>' . "\r\n");
		fwrite($file, html_entity_decode($varContents));
		return(true);
	}

	# ############################################################################
	# Function to Convert any HTML file into PDF
	# Can save into a pdf file on server, or stream to the user's browser
	# If you want to stream to the browser: $objClass->fnURL2PDF("http://www.yahoo.com", "pdf.pdf");
	# If you want to save on the server: $objClass->fnURL2PDF("http://www.yahoo.com", "", "../folder/filename.pdf");
	# Usage: $objClass->fnURL2PDF("http://www.yahoo.com", "pdf.pdf"); Default is USA
	# ############################################################################
	function fnURL2PDF($varURL, $varFileName = "pdf.pdf", $varSavePath = "")
	{
		$varSocket = fsockopen("www.easysw.com", 80, $errno, $errstr, 1000);
		if (!$varSocket) die("$errstr ($errno)\n");

		fwrite($varSocket, "GET /htmldoc/pdf-o-matic.php?URL=" . $varURL . "&FORMAT=.pdf HTTP/1.0\r\n");
		fwrite($varSocket, "Host: www.easysw.com\r\n");
		fwrite($varSocket, "Referer: http://www.easysw.com/htmldoc/pdf-o-matic.php\r\n");
		fwrite($varSocket, "\r\n");

		$varHeaders = "";
		while ($varStr = trim(fgets($varSocket, 4096)))
			$varHeaders .= "$varStr\n";

		$varBody = "";
		while (!feof($varSocket))
			$varBody .= fgets($varSocket, 4096);

		if ($varSavePath != '')
		{
			// Save the File
			$varFileHandle = @fopen($varSavePath,'w');
			$varBytes = @fwrite($varFileHandle, $varBody);
		}
		else
		{
			//Download file
			if(isset($HTTP_SERVER_VARS['HTTP_USER_AGENT']) and strpos($HTTP_SERVER_VARS['HTTP_USER_AGENT'],'MSIE'))
				Header('Content-Type: application/force-download');
			else
				Header('Content-Type: application/octet-stream');
			if(headers_sent())
				die('Some data has already been output to browser, can\'t send PDF file');
			Header('Content-Length: '.strlen($varBody));
			Header('Content-disposition: attachment; filename='.$varFileName);
			echo $varBody;
		}
		return(true);
	}

	# ############################################################################
	# Function to Retrieve List of all US States with their Codes as an <OPTION> list
	# Usage: $objClass->fnUSStatesOptionsList(); Default is Alamaba
	# ############################################################################
	function fnCurrencyOptionsList($sCurrency = "USD")
	{
      	$sCurrency = strtoupper($sCurrency);
		$str = '';


		// while(list($cc,$cv)=each($this->aCurrencies))
		// {
		// 	$varSelected2 = (strcmp($cc,$sCurrency) == 0) ? 'selected' : "";
		// 	$str .= "<option value=\"$cc\" $varSelected2>$cv</option>\n";
		// }


        foreach ($this->aCurrencies as $key => $value) {
             $str .= "<option value=\"$key\" $varSelected2>$value</option>\n";
        }

		return $str;
	}

	function fnNationalityOptionsList($sCurrent = "")
	{
		$sReturn = '';
		for ($i=0; $i < count($this->aNationalityList); $i++)
		{
			$sReturn .= '<option ' . (($sCurrent == $this->aNationalityList[$i]) ? 'selected="true"' : '') . ' value="' . $this->aNationalityList[$i] . '">' . $this->aNationalityList[$i] . '</option>';
		}

		return($sReturn);
	}

    function Minutes2Hours($mins)
    {
        if ($mins < 0)
        {
            $min = Abs($mins);
        } else {
                $min = $mins;
            }
            $H = Floor($min / 60);
            $M = ($min - ($H * 60)) / 100;
            $hours = $H +  $M;
            if ($mins < 0) {
                $hours = $hours * (-1);
            }
            $expl = explode(".", $hours);
            $H = $expl[0];
            if (empty($expl[1])) {
                $expl[1] = 00;
            }
            $M = $expl[1];
            if (strlen($M) < 2) {
                $M = $M . 0;
            }
            $hours = $H . "." . $M;
            return $hours;
   }

	# ############################################################################
	# Function to Retrieve List of all US States with their Codes as an <OPTION> list
	# Usage: $objClass->fnUSStatesOptionsList(); Default is Alamaba
	# ############################################################################
	function fnUSStatesOptionsList($varSelected = "al")
	{
		$varStatesList = array(
		"--" => " ",
		"al" => "Alabama",
		"ak" => "Alaska",
		"ar" => "Arkansas",
		"az" => "Arizona",
		"ca" => "California",
		"co" => "Colorado",
		"ct" => "Connecticut",
		"de" => "Delaware",
		"fl" => "Florida",
		"ga" => "Georgia",
		"hi" => "Hawaii",
		"ia" => "Iowa",
		"id" => "Idaho",
		"il" => "Illinois",
		"in" => "Indiana",
		"ks" => "Kansas",
		"ky" => "Kentucky",
		"la" => "Louisiana",
		"ma" => "Massachusetts",
		"md" => "Maryland",
		"me" => "Maine",
		"mi" => "Michigan",
		"mn" => "Minnesota",
		"mo" => "Missouri",
		"ms" => "Mississippi",
		"mt" => "Montana",
		"nc" => "North Carolina",
		"nd" => "North Dakota",
		"ne" => "Nebraska",
		"nh" => "New Hampshire",
		"nj" => "New Jersey",
		"nm" => "New Mexico",
		"nv" => "Nevada",
		"ny" => "New York",
		"oh" => "Ohio",
		"ok" => "Oklahoma",
		"or" => "Oregon",
		"ot" => "Other",
		"pa" => "Pennsylvania",
		"ri" => "Rhode Island",
		"sc" => "South Carolina",
		"sd" => "South Dakota",
		"tn" => "Tennessee",
		"tx" => "Texas",
		"ut" => "Utah",
		"va" => "Virginia",
		"vt" => "Vermont",
		"wa" => "Washington",
		"wi" => "Wisconsin",
		"wv" => "West Virginia",
		"wy" => "Wyoming"
      );

      	$varSelected = strtolower($varSelected);
		$str = '';
		while(list($cc,$cv)=each($varStatesList))
		{
			$varSelected2 = (strcmp($cc,$varSelected) == 0) ? 'SELECTED' : "";
			$str .= "<OPTION VALUE=\"$cc\" $varSelected2>$cv</OPTION>\n";
		}

		return $str;
	}

	# ############################################################################
	# Function to Retrieve List of all Countries with their Codes as an <OPTION> list
	# Usage: $objClass->fnCountryOptionsList(); Default is USA
	# ############################################################################
	function fnCountryOptionsList($varSelected = "pk")
	{
      	$varSelected = strtolower($varSelected);

      	$str = '';
        
        if(is_array($this->aCountriesList) && count($this->aCountriesList) >0 ){
            foreach ($this->aCountriesList as $key => $value) {
                 $str .= "<option value=\"$key\" $varSelected2>$value</option>\n";
            }
        }else{

                 $str .= "<option value='0' >-</option>";
        }


		return $str;
	}

	function TimeZonesList($sSelected = "Asia/Karachi")
	{
      	$sReturn = '';

		// while(list($sValue, $sName) = each($this->aTimeZones))
		// 	$sReturn .= '<option value="' . $sValue . '" ' . ((strcmp($sValue, $sSelected) == 0) ? 'selected' : '') . '>' . $sName . '</option>\n';


        foreach ($this->aTimeZones as $key => $value) {
             $sReturn .= "<option value=\"$key\" $varSelected2>$value</option>\n";
        }


		return $sReturn;
	}

	function fnRemoveCompleteDirectory($dirname)
	{
		// Sanity check
		if (!file_exists($dirname)) {
			return false;
		}

		// Simple delete for a file
		if (is_file($dirname)) {
			return unlink($dirname);
		}

		// Loop through the folder
		$dir = dir($dirname);
		while (false !== $entry = $dir->read()) {
			// Skip pointers
			if ($entry == '.' || $entry == '..') {
				continue;
			}

			// Recurse
			$this->fnRemoveCompleteDirectory("$dirname/$entry");
		}

		// Clean up
		$dir->close();
		return rmdir($dirname);
	}

	// $varZip = "32434"; print(fnGetZip($varZip));
	function fnGetZip($varZipCode)
	{
		return(0);
		if (!is_numeric($varZipCode))
			return(0);

		$varResult = "";
		$fd = @fopen("http://zipinfo.com/cgi-local/zipsrch.exe?zip=$varZipCode","r");
		while(!feof($fd))
			$varResult .= fread($fd, 1024);         //read 1024 bytes at a time
		fclose($fd);                          //close the connection

		if (preg_match("/is not currently assigned/", $varResult))
			return("NOT FOUND!|");

		$start = strpos($varResult, "<tr><th>Mailing");
		$end = $start + 200;
		$end = $end - $start;
		$ziptable = substr( $varResult, $start, $end );

		$ziptable = str_replace('<tr><th>Mailing<BR>Name</th><th>State<BR>Code</th><th>ZIP<BR>Code</th></tr><tr><td align=center>', '', $ziptable);
		$ziptable = str_replace('</font></td><td align=center>', '|', $ziptable);
		$ziptable = str_replace('</font></td></tr></table>', '', $ziptable);

		$arrResult = explode('|', $ziptable);

		if ((strlen($arrResult[1]) == 2))
			$varReturn = $arrResult[0] . "|" . $arrResult[1];
		else
			$varReturn = "NOT FOUND!|";

		return($varReturn);
	}

	function fnExtractString($str, $start, $end)
	{
		$str_low = $str;
		$pos_start = strpos($str_low, $start);
		$pos_end = strpos($str_low, $end, ($pos_start + strlen($start)));

		if (($pos_start !== false) && ($pos_end !== false) )
		{
			$pos1 = $pos_start + strlen($start);
			$pos2 = $pos_end - $pos1;
			return substr($str, $pos1, $pos2);
		}
		else
			return("Not Found!");
	}

	function calcElapsedTime($time)
	{
		   // calculate elapsed time (in seconds!)
		   $diff = time()-$time;
		   $yearsDiff = floor($diff/60/60/24/365);
		   $diff -= $yearsDiff*60*60*24*365;
		   $monthsDiff = floor($diff/60/60/24/30);
		   $diff -= $monthsDiff*60*60*24*30;
		   $weeksDiff = floor($diff/60/60/24/7);
		   $diff -= $weeksDiff*60*60*24*7;
		   $daysDiff = floor($diff/60/60/24);
		   $diff -= $daysDiff*60*60*24;
		   $hrsDiff = floor($diff/60/60);
		   $diff -= $hrsDiff*60*60;
		   $minsDiff = floor($diff/60);
		   $diff -= $minsDiff*60;
		   $secsDiff = $diff;
		   return (''.$yearsDiff.' year'.(($yearsDiff <> 1) ? "s" : "").', '.$monthsDiff.' month'.(($monthsDiff <> 1) ? "s" : "").', '.$weeksDiff.' week'.(($weeksDiff <> 1) ? "s" : "").', '.$daysDiff.' day'.(($daysDiff <> 1) ? "s" : "").', '.$hrsDiff.' hour'.(($hrsDiff <> 1) ? "s" : "").', '.$minsDiff.' minute'.(($minsDiff <> 1) ? "s" : "").', '.$secsDiff.' second'.(($secsDiff <> 1) ? "s" : "").'');
	}

    function EncryptPassword($sPassword, $objEncryption)
	{
		if ($sPassword == '')
			return('');

		$sTmpPassword = '';
		for ($i = 0; $i <= 15; $i++)
			$sTmpPassword .= $sPassword;

		$sTmpPassword = substr($sTmpPassword, 0, 16);
		$sTmpPassword = $objEncryption->fnEncrypt($sTmpPassword, $sTmpPassword);

		return($sTmpPassword);
	}

    // this function deletes all files and sub directories directories in a directory
    // bool remdir( str 'directory path' )
    function DeleteDir($dir)
    {
       if (substr($dir, strlen($dir)-1, 1) != '/')
           $dir .= '/';

       if ($handle = opendir($dir))
       {
           while ($obj = readdir($handle))
           {
               if ($obj != '.' && $obj != '..')
               {
                   if (is_dir($dir.$obj))
                   {
                       if (!$this->DeleteDir($dir.$obj))
                           return false;
                   }
                   elseif (is_file($dir.$obj))
                   {
                       if (!unlink($dir.$obj))
                           return false;
                   }
               }
           }

           closedir($handle);

           if (!@rmdir($dir))
               return false;
           return true;
       }
       return false;
    }

    function HelpToolTip($sTipName, $sTipTitle, $sTipText)
    {
        $iRandom = rand(1000, 5000);
        $sReturn = '
        <div id="' . $sTipName . '" name="' . $sTipName . '" style="visibility:hidden;position:absolute;z-index:1000;top:-100"></div>
        <script language="JavaScript1.2"  type="text/javascript">
        var RandomNum' . $iRandom . ' = Math.floor(Math.random()*110);
        Text[RandomNum' . $iRandom . ']=["' . $sTipTitle . '","' . $sTipText . '"]
        Style[RandomNum' . $iRandom . ']=["#FFFFFF","#ff0000","","","",,"black","#e8e8ff","","","",,,,2,"#ff0000",2,,,,,"",1,,,]

        var TipId="' . $sTipName . '";
        var FiltersEnabled = 1;
        mig_clay();
        </script>
        <a href="#noanchor" onMouseOver="stm5(Text[RandomNum' . $iRandom . '],Style[RandomNum' . $iRandom . '])" onMouseOut="htm()"><img src="../images/icons/iconHelp.gif" border=0 /></a>
        ';

        return($sReturn);
    }

    function ShowError($iError = '', $aErrorMessages, $sErrorMessage2 = "", $sImagesFolder = "../images")
    {
        if ($iError == '') $iError = $this->fnGet("error");
        if ($iError == '') return('');

        if ($iError > 100)
        {
        	$sErrMsg = $aErrorMessages[$iError];

            if ($iError == 101) $sErrMsg = $sErrorMessage2;
        	$aTemp = explode("|", $sErrMsg);
        	if ($aTemp[0] == 1)
        	{
        		$sErrTableColor = '#FFFFA9';
        		$sErrTableImg = $sImagesFolder . '/iconError.gif';
        	}
        	else if ($aTemp[0] == 2)
        	{
        		$sErrTableColor = '#C4D2F7';
        		$sErrTableImg = $sImagesFolder . '/iconErrorTick.gif';
        	}
        	$sErrMsg = $aTemp[1];

        	$sErrorMessage = '
        		<br><table width="80%" bgcolor="' . $sErrTableColor . '" border="1" bordercolor="#C0C0C0" style="border-collapse:collapse;" cellpadding="0" cellspacing="0" align=center>
        		<tr valign="middle">
                 <td>
            	  <table width="100%" border="0">
            	   <tr>
            		<td align="left" width="10%"><img src="' . $sErrTableImg . '" border="0"></td>
                    <td align="center">
            		 <span style="font-family: Verdana; font-weight: bold; font-size:12px; color:#000000;">' . $sErrMsg . '</span>
            		</td>
            		<td width="10%"></td>
            	   </tr>
            	  </TABLE>
        		 </td>
        	    </tr>
        	    </table>
        	';
        }

        return($sErrorMessage);
    }

    function DHTMLWindow($sWindowName, $sWindowTitle, $sWindowContents, $iHeight = 200, $iWidth = 200, $sBGColor = "#cbd7ee", $sBorderColor = "#5d79b2")
    {
    	$sReturn = '
    	<div class="window" id="' . $sWindowName . '" style="BORDER-LEFT-COLOR: ' . $sBorderColor . '; LEFT: 350px; BORDER-BOTTOM-COLOR: ' . $sBorderColor . '; WIDTH: ' . $iWidth . 'px; BORDER-TOP-COLOR: ' . $sBorderColor . '; TOP: 50px; BACKGROUND-COLOR: ' . $sBorderColor . '; BORDER-RIGHT-COLOR: ' . $sBorderColor . '">
		 <div class="titleBar" style="COLOR: ' . $sBGColor . '; BACKGROUND-COLOR: ' . $sBorderColor . '">
		  <span class=titleBarText>' . $sWindowTitle . '</span>
		  <img class=titleBarButtons height=14 alt="" src="' . cFrontEnd_SystemFolder . '/DHTMLWindow/images/altbuttons.gif" width="50" usemap="#' . $sWindowName . '" />
	 	  <map id="' . $sWindowName . '" name="' . $sWindowName . '">
		   <area title="Minimize" onclick="this.parentWindow.minimize();return false;" shape="RECT" alt="" coords="0,0,15,13" href="#noanchor">
		   <area title="Restore" onclick="this.parentWindow.restore();return false;" shape="RECT" alt="" coords="16,0,31,13" href="#noanchor">
		   <area title="Close" onclick="this.parentWindow.close();return false;" shape="RECT" alt="" coords="34,0,49,13" href="#noanchor">
		  </map>
		 </div>
		 <div class="clientArea" style="border-left-color: ' . $sBorderColor . '; bordor-bottom-color: ' . $sBorderColor . '; COLOR: ' . $sBorderColor . '; BORDER-TOP-COLOR: ' . $sBorderColor . '; SCROLLBAR-BASE-COLOR: ' . $sBorderColor . '; height: ' . $iHeight . 'px; background-color: ' . $sBGColor . '; border-right-color: ' . $sBorderColor . ';">
		 ' . $sWindowContents . '
		 </div>
		</div>';
    	return($sReturn);
    }

    function DaysInMonth($year, $month)
    {
    	return date("t", mktime (0,0,0,$month,1,$year));
	}

	function GetMonthName($m=0)
	{
		return (($m==0 ) ? date(�F�) : date(�F�, mktime(0,0,0,$m)));
	}

	function DateCalculate($this_date, $num_days)
	{
		$my_time = strtotime ($this_date); //converts date string to UNIX timestamp
		$timestamp = $my_time + ($num_days * 86400); //calculates # of days passed ($num_days) * # seconds in a day (86400)
		$return_date = date("Y-m-d",$timestamp);  //puts the UNIX timestamp back into string format

		return $return_date;//exit function and return string
	}

	function GetFolderSize($path)
   	{
		if(!is_dir($path))
			return filesize($path);
		$dir = opendir($path);
		while($file = readdir($dir))
		{
			if(is_file($path."/".$file))$size+=filesize($path."/".$file);
			if(is_dir($path."/".$file) && $file!="." && $file !="..")$size +=$this->GetFolderSize($path."/".$file);
		}
		return $size;
	}

	// Usage : size_hum_read(filesize($file));
	function size_hum_read($size)
	{
  		$i=0;
  		$iec = array("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");
  		while (($size/1024)>1)
  		{
   			$size=$size/1024;
   			$i++;
  		}
  		return substr($size,0,strpos($size,'.')+4).$iec[$i];
	}

	// Hugh Bothwell  hugh_bothwell@hotmail.com
	// August 31 2001
	// Number-to-word converter
	function NumberToWord($iNumber)
	{
		$ones = array("", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve", " Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen");
		$tens = array("", "", " Twenty", " Thirty", " Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety");
		$triplets = array("", " Thousand", " Million", " Billion", " Trillion", " Quadrillion", " Quintillion", " Sextillion", " Septillion", " Octillion", " Nonillion");

 		// recursive fn, converts three digits per pass
 		$num = (int) $iNumber;    // make sure it's an integer

 		if ($num < 0) return "negative".convertTri(-$num, 0);
 		if ($num == 0) return "zero";
		return $this->convertTri($num, 0, $ones, $tens, $triplets);
	}

	function convertTri($num, $tri, $ones, $tens, $triplets)
	{
  		// chunk the number, ...rxyy
  		$r = (int) ($num / 1000);
  		$x = ($num / 100) % 10;
  		$y = $num % 100;

  		// init the output string
  		$str = "";

  		// do hundreds
  		if ($x > 0) $str = $ones[$x] . " hundred";

  		// do ones and tens
  		if ($y < 20) $str .= $ones[$y];
  		else $str .= $tens[(int) ($y / 10)] . $ones[$y % 10];

  		// add triplet modifier only if there
  		// is some output to be modified...
  		if ($str != "") $str .= $triplets[$tri];

  		// continue recursing?
  		if ($r > 0)
   			return $this->convertTri($r, $tri+1, $ones, $tens, $triplets).$str;
  		else
   			return $str;
 	}

 	function CeilToNearest($dValue, $dCeilTo)
 	{
 		$dValue = number_format($dValue, 0, ".", "");

		if ($dValue % $dCeilTo == 0)
			return($dValue);

		return(number_format($dValue + ($dCeilTo - ($dValue % $dCeilTo)), 0, ".", ""));
 	}

    function fnSendMail($varFromName, $varFromEmail, $varToName, $varToEmail, $varSubject, $varBody, $varHTMLEmail = true, $varBCC = '', $bSMTP = false)
    {
    	global $objGeneral;
    	global $varErrorLogFile;

        if ($bSMTP == true)
        {
            // Usage Example:
            $mail = new clsEmail();
            $mail->IsSMTP();                                   // send via SMTP
            $mail->Host     = cSMTP_Host; // "smtp.gmail.com"; // SMTP servers
            $mail->Port     = cSMTP_Port; // "587"; // SMTP servers
            $mail->SMTPAuth = cSMTP_Auth; // true;     // turn on SMTP authentication
            $mail->Username = cSMTP_Username; // "info@webhumanresource.com";  // SMTP username
            $mail->Password = cSMTP_Password; //"webhr2020"; // SMTP password
            $mail->Timeout = 50;
            $mail->SMTPKeepAlive = true;

            $mail->From     = $varFromEmail;
            $mail->FromName = $varFromName;
            $mail->AddAddress($varToEmail,$varToName);

            $mail->WordWrap = 50;                              // set word wrap
            $mail->IsHTML($varHTMLEmail);                     // send as HTML

            $mail->Subject  =  $varSubject;
            $mail->Body     =  $varBody;

            if(!$mail->Send())
                return(false);
            else
                return(true);
        }
        else
        {
        	// To send HTML mail, the Content-type header must be set
        	$headers  = 'MIME-Version: 1.0' . "\r\n";
    	    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        	// Additional headers
        	//$headers .= 'To: ' . $varToName . ' <' . $varToEmail . '>' . "\r\n";
        	$headers .= 'From: ' . $varFromName . ' <' . $varFromEmail . '>' . "\r\n";
            $headers .= 'Reply-To: ' . $varFromName . ' <' . $varFromEmail . '>' . "\r\n";
            //$headers .= 'To: ' . $varToEmail . "\r\n";
        	//$headers .= 'From: ' . $varFromEmail . "\r\n";
            $headers .= 'Reply-To: ' . $varFromEmail . "\r\n";
            $headers .= "X-Mailer:PHP 5.1\r\n";
            $headers .= "X-Sender: " . $varFromEmail . "\n";
            if ($varBCC != '') $headers .= 'Bcc: ' . $varBCC . "\r\n";

        	// Mail it
        	return(mail($varToEmail, $varSubject, $varBody, $headers));
        }
    }


    function fnDateConvert($varDate)
    {
    	$varRetDate = explode(" ", $varDate);
    	$varRetDate = $varRetDate[0];
    	$varRetDate = explode("-", $varRetDate);
    	$varRetDate = $varRetDate[1] . '/' . $varRetDate[2] . '/' . $varRetDate[0];
    	return($varRetDate);
    }

    function fnLightWeightEditor($varWidth, $varHeight, $varName, $varValue='')
    {
    	$varStr = '
    	<script language="Javascript1.2"><!-- // load htmlarea
    	_editor_url = "../main/";                     // URL to htmlarea files
    	var win_ie_ver = parseFloat(navigator.appVersion.split("MSIE")[1]);
    	if (navigator.userAgent.indexOf(\'Mac\')        >= 0) { win_ie_ver = 0; }
    	if (navigator.userAgent.indexOf(\'Windows CE\') >= 0) { win_ie_ver = 0; }
    	if (navigator.userAgent.indexOf(\'Opera\')      >= 0) { win_ie_ver = 0; }
    	if (win_ie_ver >= 5.5) {
    	  document.write(\'<scr\' + \'ipt src="\' +_editor_url+ \'jsLightWeightEditor.js"\');
    	  document.write(\' language="Javascript1.2"></scr\' + \'ipt>\');
    	} else { document.write(\'<scr\'+\'ipt>function editor_generate() { return false; }</scr\'+\'ipt>\'); }
    	// --></script>

    	<textarea name="' . $varName . '" style="width:' . $varWidth . '; height:' . $varHeight . '">
    	' . $varValue . '
    	</textarea><br>

    	<script language="javascript1.2">
    	var config = new Object();    // create new config object

    	config.width = "' . $varWidth . 'px";
    	config.height = "' . $varHeight . 'px";
    	config.bodyStyle = \'background-color: white; font-family: "Verdana"; font-size: x-small;\';
    	config.debug = 0;

    	// NOTE:  You can remove any of these blocks and use the default config!
    	config.toolbar = [
    		[\'fontname\'],
    		[\'fontsize\'],
    		[\'fontstyle\'],
    		[\'linebreak\'],
    		[\'bold\',\'italic\',\'underline\',\'separator\'],
    	//  [\'strikethrough\',\'subscript\',\'superscript\',\'separator\'],
    		[\'justifyleft\',\'justifycenter\',\'justifyright\',\'separator\'],
    		[\'OrderedList\',\'UnOrderedList\',\'Outdent\',\'Indent\',\'separator\'],
    		[\'forecolor\',\'backcolor\',\'separator\'],
    		[\'HorizontalRule\',\'Createlink\',\'htmlmode\',\'separator\'], // \'InsertImage\',
    		[\'popupeditor\'], //\'about\',\'help\',
    	];

    	config.fontnames = {
    		"Arial":           "arial, helvetica, sans-serif",
    		"Courier New":     "courier new, courier, mono",
    		"Georgia":         "Georgia, Times New Roman, Times, Serif",
    		"Tahoma":          "Tahoma, Arial, Helvetica, sans-serif",
    		"Times New Roman": "times new roman, times, serif",
    		"Verdana":         "Verdana, Arial, Helvetica, sans-serif",
    		"impact":          "impact",
    		"WingDings":       "WingDings"
    	};
    	config.fontsizes = {
    		"1 (8 pt)":  "1",
    		"2 (10 pt)": "2",
    		"3 (12 pt)": "3",
    		"4 (14 pt)": "4",
    		"5 (18 pt)": "5",
    		"6 (24 pt)": "6",
    		"7 (36 pt)": "7"
    	  };

    	config.fontstyles = [   // make sure classNames are defined in the page the content is being display as well in or they won\'t work!
    	  { name: "headline",     className: "headline",  classStyle: "font-family: arial black, arial; font-size: 28px; letter-spacing: -2px;" },
    	  { name: "arial red",    className: "headline2", classStyle: "font-family: arial black, arial; font-size: 12px; letter-spacing: -2px; color:red" },
    	  { name: "verdana blue", className: "headline4", classStyle: "font-family: verdana; font-size: 18px; letter-spacing: -2px; color:blue" }

    	// leave classStyle blank if it\'s defined in config.stylesheet (above), like this:
    	//  { name: "verdana blue", className: "headline4", classStyle: "" }
    	];

    	editor_generate(\'LightWeightEditor\',config);
    	</script>
    	';

    	return($varStr);
    }

    function RemoveExtraCharacters($sMessage)
    {
    	$sMessage = str_replace('<', '$lt;', $sMessage);
    	$sMessage = str_replace('>', '$gt;', $sMessage);
    	return($sMessage);
    }


    // Rich Text Editor Functions
    function RichTextEditor($sEditorName = '', $sContent = '', $iHeight = 120, $iWidth = 120)
    {
    	$sContent = rteSafe($sContent);

    	$sReturn = '
    	<script language="JavaScript" type="text/javascript" src="../../system/editor/html2xhtml.js"></script>
    	<script language="JavaScript" type="text/javascript" src="../../system/editor/richtext.js"></script>
    	<script language="JavaScript" type="text/javascript">
    	 initRTE("../../system/editor/images/", "../../system/editor/", "", true); //Usage: initRTE(imagesPath, includesPath, cssFile, genXHTML)
    	</script>
    	<noscript><p><b>Javascript must be enabled to use this form.</b></p></noscript>
    	<script language="JavaScript" type="text/javascript">
    	 writeRichText(\'' . $sEditorName . '\', \'' . $sContent . '\', ' . $iWidth . ', ' . $iHeight . ', true, false);  //Usage: writeRichText(fieldname, html, width, height, buttons, readOnly)
    	</script>
    	';

    	return($sReturn);
    }

    function rteSafe($strText)
    {
    	//returns safe code for preloading in the RTE
    	$tmpString = $strText;

    	//convert all types of single quotes
    	$tmpString = str_replace(chr(145), chr(39), $tmpString);
    	$tmpString = str_replace(chr(146), chr(39), $tmpString);
    	$tmpString = str_replace("'", "&#39;", $tmpString);

    	//convert all types of double quotes
    	$tmpString = str_replace(chr(147), chr(34), $tmpString);
    	$tmpString = str_replace(chr(148), chr(34), $tmpString);
    //	$tmpString = str_replace("\"", "\"", $tmpString);

    	//replace carriage returns & line feeds
    	$tmpString = str_replace(chr(10), " ", $tmpString);
    	$tmpString = str_replace(chr(13), " ", $tmpString);

    	return $tmpString;
    }

    function SortArray ($array, $index, $order='asc', $natsort=FALSE, $case_sensitive=FALSE)
    {
    	if(is_array($array) && count($array)>0)
    	{
    		foreach(array_keys($array) as $key) $temp[$key]=$array[$key][$index];
    		if(!$natsort)
    			($order=='asc')? asort($temp) : arsort($temp);
    		else
    		{
    			($case_sensitive)? natsort($temp) : natcasesort($temp);
    			if($order!='asc')
    				$temp=array_reverse($temp,TRUE);
    		}

    		foreach(array_keys($temp) as $key) (is_numeric($key))? $sorted[]=$array[$key] : $sorted[$key]=$array[$key];
    		return $sorted;
    	}
    	return $array;
    }

    function HelpDialog($iHelpId)
    {
    	$sReturn = '<a href="#noanchort" onClick="window.open(\'../main/help2.php?id=' . $iHelpId . '\', \'HelpWindow' . $iHelpId . '\', \'toolbar=no,width=500,height=465,directories=no,status=yes,location=no,scrollbars=no,resizable=no,menubar=no\');"><img src="../images/icons/iconHelp.gif" alt="Click for Help" title="Click for Help" border=0></a>';

    	return($sReturn);
    }

    function dumpAssociativeArray($array)
        {
    		    $res = '';
    		    $header = false;
    		    if (is_array($array) && sizeof($array)) {
    		        $res .= "<table border=1>\n";
    		        foreach(@$array as $values) {
    		            if (!$header) {
    		                $res .= "<th>" . implode("</th><th>", array_keys($values)) . "</th>\n";
    		                $header = true;
    		            }
    		            $res .= "<tr>\n";
    		            foreach($values as $key => $value) {
    		                $res .= "<td>" . ($value != '' ? $value : "&nbsp;") . "</td>";
    		                if ($key == 'full_path')
    		                	$varPath = $value;
    		            }
    		            $res .= "</tr>\n";
    		        }
    		        $res .= "</table>\n";
    		    }
    		    return $varPath;
    	}

    function dircpy($source, $dest, $overwrite = false)
    {
      $loc1 = '';
      if($handle = opendir($loc1 . $source)){        // if the folder exploration is sucsessful, continue
       while(false !== ($file = readdir($handle))){ // as long as storing the next file to $file is successful, continue
         if($file != '.' && $file != '..'){
           $path = $source . '/' . $file;
           if(is_file($loc1 . $path)){
             if(!is_file($loc1 . $dest . '/' . $file) || $overwrite)
               if(!@copy($loc1 . $path, $loc1 . $dest . '/' . $file)){
                 echo '<font color="red">File ('.$path.') could not be copied, likely a permissions problem.</font>';
               }
           } elseif(is_dir($loc1 . $path)){
             if(!is_dir($loc1 . $dest . '/' . $file))
               mkdir($loc1 . $dest . '/' . $file); // make subdirectory before subdirectory is copied
             dircpy($path, $dest . '/' . $file, $overwrite); //recurse!
           }
         }
       }
       closedir($handle);
      }
    } // end of dircpy()

    function getFiles($directory)
    {
    	// Try to open the directory
    	if($dir = opendir($directory))
    	{
    		// Create an array for all files found
    		$tmp = Array();

    		// Add the files
    		while($file = readdir($dir))
    		{
    			// Make sure the file exists
    			if($file != "." && $file != ".." && $file[0] != '.')
    			{
    				// If it's a directiry, list all files within it
    				if(is_dir($directory . "/" . $file))
    				{
    					$tmp2 = $this->getFiles($directory . "/" . $file);
    					if(is_array($tmp2))
    					{
    						$tmp = array_merge($tmp, $tmp2);
    					}
    				}
    				else
    				{
    					array_push($tmp, $directory . "/" . $file);
    				}
    			}
    		}

    		// Finish off the function
    		closedir($dir);
    		return $tmp;
    	}
    }



    /* Grid Control */
    function Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sPagingStringExtra, &$iPagingLimit, &$iOffSet, &$sCurrentURL)
    {
		$iPagingLimit = cPagingLimit;

		if ($iShow == 5) $iPagingLimit = 5;
		else if ($iShow == 10) $iPagingLimit = 10;
		else if ($iShow == 20) $iPagingLimit = 20;
		else if ($iShow == 50) $iPagingLimit = 50;

		$iNumOfPages = ceil($iTotalRecords / $iPagingLimit);
		$iOffSet = ($iPage - 1) * $iPagingLimit;

		$sCurrentURL = $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'];
		$sCurrentURL = preg_replace('/action=([a-z|A-Z|0-9]*)/i', '', $sCurrentURL);

		if ($iPage <= 1)
		{
			$sPagingFirstImage = '<img alt="Next Page" width="15" height="15" title="First Page" src="../images/paging/first_i.gif">';
			$sPagingPreviousImage = '<img alt="Next Page" width="15" height="15" title="Previous Page" src="../images/paging/prev_i.gif">';
		}
		else
		{
			$sCurrentURL = preg_replace('/&page=([0-9]*)/i', '', $sCurrentURL);
			$sPagingFirstImage = '<a href="' . $sCurrentURL . '&page=1"><img alt="First Page" width="15" height="15" title="First Page" border="0" src="../images/paging/first.gif"></a>';
			$sPagingPreviousImage = '<a href="' . $sCurrentURL . '&page=' . ($iPage-1) . '"><img border="0" width="15" height="15" alt="Previous Page" title="Previous Page" src="../images/paging/prev.gif"></a>';
		}

		if ($iPage < $iNumOfPages)
		{
			$sCurrentURL = preg_replace('/&page=([0-9]*)/i', '', $sCurrentURL);
			$sPagingNextImage = '<a href="' . $sCurrentURL . '&page=' . ($iPage+1) . '"><img alt="Next Page" title="Next Page" width="15" height="15" border="0" src="../images/paging/next.gif"></a>';
			$sPagingLastImage = '<a href="' . $sCurrentURL . '&page=' . $iNumOfPages . '"><img alt="Last Page" title="Last Page" width="15" height="15" border="0" src="../images/paging/last.gif"></a>';
		}
		else
		{
			$sPagingNextImage = '<img alt="Next Page" width="15" height="15" title="Next Page" src="../images/paging/next_i.gif">';
			$sPagingLastImage = '<img alt="Last Page" width="15" height="15" title="Last Page" src="../images/paging/last_i.gif">';
		}

		$sCurrentURL = preg_replace('/&show=([0-9]*)/i', '', $sCurrentURL);
		$sPagingLimitImages1 = '<a href="' . $sCurrentURL . '&show=5"><img border="0" src="../images/paging/5.gif" width="15" height="15" alt="Show 5 Entries Per Page" title="Show 5 Entries Per Page"></a>';
		$sPagingLimitImages2 = '<a href="' . $sCurrentURL . '&show=10"><img border="0" src="../images/paging/10.gif" width="15" height="15" alt="Show 10 Entries Per Page" title="Show 10 Entries Per Page"></a>';
		$sPagingLimitImages3 = '<a href="' . $sCurrentURL . '&show=20"><img border="0" src="../images/paging/20.gif" width="15" height="15" alt="Show 20 Entries Per Page" title="Show 20 Entries Per Page"></a>';
		$sPagingLimitImages4 = '<a href="' . $sCurrentURL . '&show=50"><img border="0" src="../images/paging/50.gif" width="15" height="15" alt="Show 50 Entries Per Page" title="Show 50 Entries Per Page"></a>';

		if ($iPagingLimit == 5)
			$sPagingLimitImages1 = '<img border="0" src="../images/paging/5i.gif" width="15" height="15" alt="Show 5 Entries Per Page" title="Show 5 Entries Per Page">';
		else if ($iPagingLimit == 10)
			$sPagingLimitImages2 = '<img border="0" src="../images/paging/10i.gif" width="15" height="15" alt="Show 10 Entries Per Page" title="Show 10 Entries Per Page">';
		else if ($iPagingLimit == 20)
			$sPagingLimitImages3 = '<img border="0" src="../images/paging/20i.gif" width="15" height="15" alt="Show 20 Entries Per Page" title="Show 20 Entries Per Page">';
		else if ($iPagingLimit == 50)
			$sPagingLimitImages4 = '<img border="0" src="../images/paging/50i.gif" width="15" height="15" alt="Show 50 Entries Per Page" title="Show 50 Entries Per Page">';

		$sPagingLimitImages = $sPagingLimitImages1 . $sPagingLimitImages2 . $sPagingLimitImages3 . $sPagingLimitImages4;

		$sPagingStringExtra .= '&nbsp;<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint2.gif" alt="Print" title="Print" border="0" /></a>';

		$sPagingString = '<table border="0" cellspacing="0" cellpadding="0"><tr><td>' . $sPagingStringExtra . '<img src="../images/spacer.gif" height="1" width="10" border="0" alt="spacer" /></td><td>' . $sPagingFirstImage . $sPagingPreviousImage . '</td><td valign="middle">&nbsp;&nbsp;Page <b>' . $iPage . '</b> of <b>' . $iNumOfPages . '</b>&nbsp;&nbsp;</td><td>' . $sPagingNextImage . $sPagingLastImage . '</td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>' . $sPagingLimitImages . '</td></tr></table>';

        $sCurrentURL = preg_replace('/&sortorder=([a-z|A-Z|0-9]*)/i', '', $sCurrentURL);
        $sCurrentURL = preg_replace('/&sortby=([a-z|A-Z|0-9|.]*)/i', '', $sCurrentURL);

		return($sPagingString);
    }
}
?>