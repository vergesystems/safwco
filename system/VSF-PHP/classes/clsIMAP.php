<?php

class clsIMAP
{
    public $sServerName;
    public $sEmailUserName;
    public $sEmailPassword;

    public $oMailBox;


    function __construct() { }

    function Connect($sServerName, $sEmailUserName, $sEmailPassword)
    {
        //$sServerName = "{mail.vergesecurity.com:143}INBOX"; // For a IMAP connection    (PORT 143)
        //$sServerName = "{mail.vergesecurity.com/notls:110}INBOX"; // For a POP3 connection    (PORT 110)

        $this->sServerName = "{mail.vergesecurity.com/notls}INBOX";
        $this->sEmailUserName = $sEmailUserName;
        $this->sEmailPassword = $sEmailPassword;

        $this->oMailBox = imap_open($this->sServerName, $this->sEmailUserName, $this->sEmailPassword);
        if (!$this->oMailBox) return("Could not open Mailbox - try again later!");
    }

    function GetMessages()
    {
        $oHeader = imap_check($this->oMailBox);
        if (!$oHeader) return(0);

        $iMessageCount = $oHeader->Nmsgs;
        return($iMessageCount);
    }

    function GetMessageHeader($iMessageNumber)
    {
        return(imap_fetchheader($this->oMailBox, $iMessageNumber));
    }

    function GetMessageStructure($iMessageNumber)
    {
        return(imap_fetchstructure($this->oMailBox, $iMessageNumber));
    }

    function GetMessageBodyStructure($iMessageNumber, $iCounter)
    {
        return(imap_bodystruct($this->oMailBox, $iMessageNumber, $iCounter));
    }

    function GetMessageBody($iMessageNumber, $iMessagePart)
    {
        return(imap_fetchbody($this->oMailBox, $iMessageNumber, $iMessagePart));
    }

    function DeleteMessage($iMessageNumber)
    {
        return(imap_delete($this->oMailBox, $iMessageNumber));
    }

    function Disconnect()
    {
        imap_expunge($this->oMailBox);
        imap_close($this->oMailBox);
    }


    function Attachment_DownloadFile($strFileType, $strFileName, $fileContent, $sDestinationFileName)
    {
        $ContentType = "application/octet-stream";

        switch ($strFileType)
        {
            case ".asf": $ContentType = "video/x-ms-asf"; break;
            case ".avi": $ContentType = "video/avi"; break;
            case ".doc": $ContentType = "application/msword"; break;
            case ".zip": $ContentType = "application/zip"; break;
            case ".xls": $ContentType = "application/vnd.ms-excel"; break;
            case ".gif": $ContentType = "image/gif"; break;
            case ".jpg": $ContentType = "image/jpeg"; break;
            case ".jpeg": $ContentType = "image/jpeg"; break;
            case ".wav": $ContentType = "audio/wav"; break;
            case ".mp3": $ContentType = "audio/mpeg3"; break;
            case ".mpg": $ContentType = "video/mpeg"; break;
            case ".mpeg": $ContentType = "video/mpeg"; break;
            case ".rtf": $ContentType = "application/rtf"; break;
            case ".htm": $ContentType = "text/html"; break;
            case ".html": $ContentType = "text/html"; break;
            case ".xml": $ContentType = "text/xml"; break;
            case ".xsl": $ContentType = "text/xsl"; break;
            case ".css": $ContentType = "text/css"; break;
            case ".php": $ContentType = "text/php"; break;
            case ".asp": $ContentType = "text/asp"; break;
            case ".pdf": $ContentType = "application/pdf"; break;
        }

	    //header ("Content-Type: $ContentType");
    	//header ("Content-Disposition: attachment; filename=$strFileName; size=$fileSize;");

    	// Updated oktober 29. 2005
	    if (substr($ContentType,0,4) == "text")
	        $sResult = imap_qprint($fileContent);
        else
	        $sResult = imap_base64($fileContent);

        $varFileHandle = @fopen($sDestinationFileName,'w');
        $varBytes = @fwrite($varFileHandle, $sResult);
        fclose($varFileHandle);
    }

    function get_mime_type(&$structure)
    {
        $primary_mime_type = array("TEXT", "MULTIPART","MESSAGE", "APPLICATION", "AUDIO","IMAGE", "VIDEO", "OTHER");
        if($structure->subtype)
   	        return $primary_mime_type[(int) $structure->type] . '/' .$structure->subtype;

        return "TEXT/PLAIN";
    }

    function get_part($stream, $msg_number, $mime_type, $structure = false,$part_number    = false)
    {
        $prefix = "";
   	    if(!$structure)
   	    	$structure = imap_fetchstructure($stream, $msg_number);

       	if($structure)
        {
   		    if($mime_type == get_mime_type($structure))
            {
   			    if(!$part_number)
   				    $part_number = "1";

   			    $text = imap_fetchbody($stream, $msg_number, $part_number);
   			    if($structure->encoding == 3)
   				    return imap_base64($text);
                else if($structure->encoding == 4)
   				    return imap_qprint($text);
                else
   			        return $text;
   	        }

		    if($structure->type == 1) // multipart
            {
           		while(list($index, $sub_structure) = each($structure->parts))
                {
   			        if($part_number)
   				        $prefix = $part_number . '.';

   			        $data = get_part($stream, $msg_number, $mime_type, $sub_structure,$prefix . ($index + 1));

                    if($data)
   				        return $data;
   		        }
   		    }
   	    }

        return false;
    }
}
?>