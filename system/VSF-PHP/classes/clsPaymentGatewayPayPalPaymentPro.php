<?php

// This class was taken from http://phppaypalpro.sourceforge.net/

/* examples.php *

/**
 * List of Examples
 *
 * This file contains an example of how each operation is supposed to be carried out.
 *
 * I have commented out all the examples, simply uncomment the operations you wish to test
 *
 * The examples here are presented using fictitious data, to reproduce the results you will have
 * to substitute the equivalent data with those of your own. Thank you.
 *
 * You are required to sign up for your own API username, password and signature
 *
 * The examples will be in the following order for all the operations supported by this program:
 *
 * Simply include the file paypal_base.php and then you can go ahead to select any of the operations available
 *
 * - DoDirectPayment
 * - SetExpressCheckout
 * - GetExpressCheckoutDetails
 * - DoExpressCheckoutPayment
 *
 * More of the API operations shall be supported in the subsequent versions of this program
 *
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @version 0.2.1
 * @package PaypalBase
 * @filesource
 */

/*

include('../../system/classes/clsPaymentGatewayPayPalPaymentPro.php');


$paymentAction = 'Sale';

$currencyId = 'USD';

$salutation = 'Mr.';
$fname      = 'Israel';
$lname      = 'Ekpo';
$address1   = '123 Main Street';
$address2   = 'Apt 987';
$city       = 'North Miami Beach';
$state      = 'FL';
$zip        = '33181';
$cc_country = 'US';

$phone      = '3059449455';

$cc_type    = 'Visa';
$cc_number  = '4147706547894046';
$cv2        = '917';
$exp_month  = '12';
$exp_year   = '2007';

$email       = 'naveedmemon@yahoo.com';

$item_desc   = 'Cool Sourceforge Software Example';

$order_desc  = 'Purchase from PerfectVista Technologies, Inc';
$custom      = 'WebPurchase';

$invoice     =  date('U');

$ip  = $_SERVER['REMOTE_ADDR'];

$unique_session_id = session_id();

$item1 = 40.00;
$item2 = 20.00;
$item3 = 30.00;

$tax1 = 7.00;
$tax2 = 14.00;
$tax3 = 21.00;

$item_total  = $item1 + $item2 + $item3;

$tax         = $tax1 + $tax2 + $tax3;

$shipping    = 25.00;
$handling    = 75.00;

$order_total = $item_total + $shipping + $handling + $tax;


// Setting up the Authentication information
// Such as Username, Password, Signature and Subject

$API = new WebsitePaymentsPro();

$API_USERNAME = 'naveedmemon_api1.yahoo.com'; //'ibb_api1.perfectvista.net';
$API_PASSWORD = 'EG7KTUSXZJRWZ8HW'; //'RMMP25ATEC3PZJX8';
$API_SIGNATURE = 'ALodBl9iI1vyAlz5PR-UYwZzPrlyAxP2SNZ.WjqnzY6TeT4N20ZTfzbA'; //'AiPC9BjkCyDFQXbSkoZcgqH3hpacA4EAG6uE6TDmFNlGFx6LWKnsoGLG';

$API->prepare($API_USERNAME, $API_PASSWORD, $API_SIGNATURE);


/*

// DoDirectPayment
//==========================================================================================================

$Paypal = $API->selectOperation('DoDirectPayment');
$Address = PayPalTypes::AddressType($fname . ''. $lname, $address1, $address2, $city, $state, $zip, $cc_country, $phone);
$PersonName = PayPalTypes::PersonNameType($salutation, $fname, '', $lname);
$PayerInfo = PayPalTypes::PayerInfoType($email, 'israelekpo', 'verified', $PersonName, $cc_country, '', $Address);
$CreditCardDetails = PayPalTypes::CreditCardDetailsType($cc_type, $cc_number, $exp_month, $exp_year, $PayerInfo, $cv2);
$PaymentDetails = PayPalTypes::PaymentDetailsType($order_total, $item_total, $shipping, $handling, $tax, $order_desc, $custom, $invoice, '', 'http://phppaypalpro.sourceforge.net/ipn_notify.php', $Address);
$Paypal->setParams($paymentAction, $PaymentDetails, $CreditCardDetails, $ip, $unique_session_id);
$Paypal->addPaymentItem('Perfume for Ladies', 'Item Number 90887', 1, $tax1, $item1, $currencyId);
$Paypal->addPaymentItem('Cologne for Gentlement', 'Item Number 90888', 1, $tax2, $item2, $currencyId);
$Paypal->addPaymentItem('Toys for Kids', 'Item Number 90889', 1, $tax3, $item3, $currencyId);

$Paypal->execute();

if ($Paypal->success())
{
	$response = $Paypal->getAPIResponse();
}
else
{
	$response = $Paypal->getAPIException();
}

print_r($response);
die();

var_dump($response);




// SetExpressCheckout
// On Success Will return a token with value like EC-7EG51014BE327234S
// Customer should then be redirected by your server to URL like
// https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-7EG51014BE327234S
// Customer will then enter payment at paypal.com and upon success customer will be sent back to the $ReturnURL
// that you provided in the operation with the tokon and PayerID appended as parameters to the $ReturnURL
//==========================================================================================================

$Paypal = $API->selectOperation('SetExpressCheckout');

$OrderTotal = 0.01;
$ReturnURL = 'http://phppaypalpro.sourceforge.net/return_url.php';
$CancelURL = 'http://phppaypalpro.sourceforge.net/cancel_url.php';
$PaymentAction = 'Sale'; // or Order
$Paypal->setParams($OrderTotal, $ReturnURL, $CancelURL, $PaymentAction);
$Paypal->execute();

if ($Paypal->success())
{
	$response = $Paypal->getAPIResponse();
} else
{
	$response = $Paypal->getAPIException();
}

header("Location: https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" . $response->Token);
die();

/*
// GetExpressCheckoutDetails

// At the $Return URL get the ExpressCheckoutDetails by calling this operation using the token
// The PayperID will be used in the next step, DoExpressCheckoutPayment
// This operation returns all the details about the person making the payment (name, contact info etc)
//==========================================================================================================

$Paypal = $API->selectOperation('GetExpressCheckoutDetails');

$Token = $_REQUEST['token'];

$Paypal->setParams($Token);

$Paypal->execute();

if ($Paypal->success())
{
	var_dump($Paypal->getAPIResponse());

}
else
{
	var_dump($Paypal->getAPIException());
}


// DoExpressCheckoutPayment
// The $token and $PayerID are used in this operation to complete the transaction.
// This is where the final Paypal Transaction ID like 4PJ31634YK772325W will be issued.
//==========================================================================================================


$Paypal = $API->selectOperation('DoExpressCheckoutPayment');

$PaymentAction = 'Sale'; // or Order

$Token = $_REQUEST['token'];

$PayerID = $_REQUEST['PayerID'];

$Address = PayPalTypes::AddressType($fname . ''. $lname, $address1, $address2, $city, $state, $zip, $cc_country, $phone);

$item_desc   = 'Cool Sourceforge Software';

$order_desc  = 'Purchase from PerfectVista Technologies';
$custom      = 'WebPurchase';

$invoice     =  date('U');

$ip  = $_SERVER['REMOTE_ADDR'];

$unique_session_id = session_id();

$item1 = 40.00;
$item2 = 20.00;
$item3 = 30.00;

$tax1 = 7.00;
$tax2 = 14.00;
$tax3 = 21.00;

$item_total  = $item1 + $item2 + $item3;

$tax         = $tax1 + $tax2 + $tax3;

$shipping    = 25.00;
$handling    = 75.00;

$order_total = $item_total + $shipping + $handling + $tax;

$PaymentDetails = PayPalTypes::PaymentDetailsType($order_total, $item_total, $shipping, $handling, $tax, $order_desc, $custom, $invoice, '', 'http://phppaypalpro.sourceforge.net/ipn_notify.php', $Address);

$Paypal->setParams($PaymentAction, $Token, $PayerID, $PaymentDetails);

$Paypal->addPaymentItem('Perfume for Ladies', 'Item Number 90887', 1, $tax1, $item1, $currencyId);
$Paypal->addPaymentItem('Cologne for Gentlement', 'Item Number 90888', 1, $tax2, $item2, $currencyId);
$Paypal->addPaymentItem('Toys for Kids', 'Item Number 90889', 1, $tax3, $item3, $currencyId);

$Paypal->execute();

if ($Paypal->success())
{

	var_dump($Paypal->getAPIResponse());
}
else
{
	var_dump($Paypal->getAPIException());
}



/*
//
// TransactionSearch
// Searching for a set of Transactions by passing certain criteria to the Paypal Webservice
//==========================================================================================================

$Paypal = $API->selectOperation('TransactionSearch');
$StartDate  = strtotime('2006-11-19');
$EndDate    = strtotime('2006-11-25');

$PayerEmail = 'sbb@perfectvista.net';
$ReceiverEmail = 'perfectvista@users.sourceforge.net';
$ReceiptID = '2XF249546X016870H';
$TransactionID = '8A5447438G757964A';
$PayerName = PayPalTypes::PersonNameType($salutation, $fname, '', $lname);
$AuctionItemNumber = '';
$InvoiceID = '9876542';
$CardNumber = '4010582588585520';

$TransactionClass = 'Sent'; // One of: Received, Masspay, MoneyRequest,
// OR FundsAdded, FundsWithdrawn, Referral, Fee, Subscription,
// OR Divdend, BillPay, Refund, CurrencyConversions, BalanceTransfer, Reversal,
// OR Shipping, BalanceAffecting, Echeck

$Amount = '9.35';
$CurrencyCode = 'USD';
$Status = 'Success'; // One of: Processing, Success, Denied, Reversed
$currencyID = 'USD';

// If the value is an empty string that parameter will be excluded from the search criteria.

$Paypal->setParams($StartDate, $EndDate, $PayerEmail, $ReceiverEmail, $ReceiptID, $TransactionID, $PayerName, $AuctionItemNumber, $InvoiceID, $CardNumber, $TransactionClass, $Amount, $CurrencyCode, $Status, $currencyId);

$Paypal->execute();

if ($Paypal->success())
{
	$response = $Paypal->getAPIResponse();
	
} 
else 
{
	$response = $Paypal->getAPIException();
}

echo '<pre>';
var_dump($response);
echo '</pre>';

*/

/*
// GetTransactionDetails
// Retrieves the Transaction Details for a specific transaction by passing certain criteria to the Paypal Webservice.
// The details available here are better than those for the Transaction Search
//==========================================================================================================

$Paypal = $API->selectOperation('GetTransactionDetails');

$TransactionID = '11A317146P6709549';

$Paypal->setParams($TransactionID);

$Paypal->execute();

if ($Paypal->success())
{
	$response = $Paypal->getAPIResponse();

}

else
{
	$response = $Paypal->getAPIException();
}

echo "<pre>";
var_dump($response);
echo "</pre>";
*/





/*****************************************/
/********** config.inc.php ***************/
/*****************************************/

/**
 * Web Service Configuration File
 *
 * This file contains important constants and resources that are going to be
 * used throughout the entire phpPaypalPro system. This file contains the location of the 2 sets of WSDL documents.
 * The first one is used when making live transactions and the second one is used for Sanbox transactions
 *
 * You cannot use your sandbox login info for the live transactions or vice versa.
 *
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Israel Ekpo, 2006-2007
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @version 0.2.1
 * @package PaypalBase
 * @filesource
 */

/**
 * Location of Live WSDL File
 *
 * LIVE WSDL Files; Downloaded for Live Paypal Server using wget
 * The names spaces from the .xsd files are imported into the WSDL file.
 *
 * https://www.paypal.com/wsdl/PayPalSvc.wsdl
 * https://www.paypal.com/wsdl/eBLBaseComponents.xsd
 * https://www.paypal.com/wsdl/CoreComponentTypes.xsd
 *
 * @since version 0.1.1 This was modified in version 0.1.1 to this new location
 * @name LIVE_WSDL
 */
define('LIVE_WSDL', cVSFFolder . '/classes/PayPalWSDL/live/PayPalSvc.wsdl');

/**
 * Location of Sandbox WSDL File
 *
 * Sandbox WSDL Files; Downloaded from the Sandbox Server using wget
 * The namespaces from the .xsd files are imported into the WSDL file.
 *
 * https://www.sandbox.paypal.com/wsdl/PayPalSvc.wsdl
 * https://www.sandbox.paypal.com/wsdl/CoreComponentTypes.xsd
 * https://www.sandbox.paypal.com/wsdl/eBLBaseComponents.xsd
 *
 * @since version 0.1.1 This was modified in version 0.1.1 to this new location
 * @name SANDBOX_WSDL
 */
define('SANDBOX_WSDL', 'classes/PayPalWSDL/sandbox/PayPalSvc.wsdl');

/**
 * API STATUS
 *
 * Whether or not we are running the script in live mode
 *
 * @name API_IS_LIVE
 */
define('API_IS_LIVE', true);

/**
 * Connection TimeOut
 *
 * The connection timeout is the number of seconds
 * the service will wait before timeing out.
 *
 * @name API_CONNECTION_TIMEOUT
 */
define('API_CONNECTION_TIMEOUT', 600);

$API_WSDL = (API_IS_LIVE)? LIVE_WSDL : SANDBOX_WSDL;

/**
 * WSDL File Location
 *
 * This file contains a list of all the operations available
 * in the webservice.
 *
 * @name API_WSDL
 */
define('API_WSDL', $API_WSDL);

/**
 * Payload Schema Version
 *
 * This refers to the version of the request payload schema.
 * As of January 21, 2007, the version was 2.4
 *
 * @name API_VERSION
 */
define('API_VERSION', '2.4');




/**********************************************/
/********** dodirectpayment.php ***************/
/**********************************************/

/**
 * DirectPayment Object
 *
 * This class is used to perform the DoDirectPayment operation
 *
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @version 0.2.1
 * @package DoDirectPayment
 * @filesource
 */


/**
 * Used to invoke the DoDirectPayment Operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package DoDirectPayment
 */
final class DoDirectPayment extends PaypalAPI implements OperationsTemplate
{   
    /**
     * Message Sent to the Webservice
     *
     * @var array
     * @access private
     */
    private $apiMessage;

    /**
     * Prepares the message to be sent
     *
     * This method prepares the message to be sent to the 
     * Paypal Webservice
     * 
     * @access public
     * @param string $PaymentAction
     * @param PaymentDetailsType $PaymentDetails
     * @param CreditCardDetailsType $CreditCard
     * @param string $IPAddress
     * @param string $MerchantSessionId
     */
    public function setParams($PaymentAction, $PaymentDetails, $CreditCard, $IPAddress, $MerchantSessionId)
    {
        $this->apiMessage = PayPalTypes::DoDirectPaymentRequestDetailsType($PaymentAction, $PaymentDetails, $CreditCard, $IPAddress, $MerchantSessionId);
    }
    
    /**
     * Adds Item to the Payment Details
     *
     * @param string $Name
     * @param float $Number
     * @param integer $Quantity
     * @param float $Tax
     * @param float $Amount
     * @param string $currencyID
     */
    public function addPaymentItem($Name = '', $Number = '', $Quantity = 1, $Tax = '', $Amount = '', $currencyID = 'USD')
    {              
        $this->apiMessage['PaymentDetails']['PaymentDetailsItem'][] = PayPalTypes::PaymentDetailsItemType($Name, $Number, $Quantity, $Tax, $Amount, $currencyID);
    }
    
    /**
     * Executes the DoDirectPayment Operation
     *
     * Prepares the final message and the calls the Webservice operation. If it is successfull the response is registered
     * and the OperationStatus is set to true, otherwise the Operation status will be set to false and an Exception of the type
     * soapFault will be registered instead.
     * 
     * @throws SoapFault
     * @access public
     */
    public function execute()
    {
        try
        {
            $this->apiMessage['Version'] = API_VERSION;
            
            $this->apiMessage['DoDirectPaymentRequestDetails'] = $this->apiMessage;
            
            $this->apiMessage = array('DoDirectPaymentRequest' => $this->apiMessage);
            
            $this->apiMessage = array($this->apiMessage);
            
            parent::registerAPIResponse(PayPalBase::getSoapClient()->__soapCall('DoDirectPayment', $this->apiMessage, null, PayPalBase::getSoapHeader()));
            
            PaypalBase::setOperationStatus(true);           
        }
        
        catch (SoapFault $Exception)
        {
            parent::registerAPIException($Exception);
            
            PaypalBase::setOperationStatus(false);
        }
    }
}


/*******************************************************/
/********** doexpresscheckoutpayment.php ***************/
/*******************************************************/

/**
 * DoExpressCheckoutPayment Object
 *
 * This class is used to perform the DoExpressCheckoutPayment operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Israel Ekpo, 2006-2007
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @version 0.2.1
 * @package ExpressCheckout
 * @filesource
 */


/**
 * Used to invoke the DoExpressCheckoutPayment Operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package ExpressCheckout
 */
final class DoExpressCheckoutPayment extends PaypalAPI implements OperationsTemplate
{   
    /**
     * Message Sent to the Webservice
     *
     * @var array
     * @access private
     */
    private $apiMessage;

    /**
     * Prepares the message to be sent
     *
     * This method prepares the message to be sent to the 
     * Paypal Webservice
     * 
     * @access public
     * @param string $PaymentAction
     * @param string $Token
     * @param string $PayerID
     * @param string $PaymentDetails
     */
    public function setParams($PaymentAction, $Token, $PayerID, $PaymentDetails)
    {
        $this->apiMessage = PayPalTypes::DoExpressCheckoutPaymentRequestDetailsType($PaymentAction, $Token, $PayerID, $PaymentDetails);
    }
    
    /**
     * Adds Item to the Payment Details
     *
     * @param string $Name
     * @param float $Number
     * @param integer $Quantity
     * @param float $Tax
     * @param float $Amount
     * @param string $currencyID
     */
    public function addPaymentItem($Name = '', $Number = '', $Quantity = 1, $Tax = '', $Amount = '', $currencyID = 'USD')
    {              
        $this->apiMessage['PaymentDetails']['PaymentDetailsItem'][] = PayPalTypes::PaymentDetailsItemType($Name, $Number, $Quantity, $Tax, $Amount, $currencyID);
    }
    
    /**
     * Executes the DoExpressCheckoutPayment Operation
     *
     * Prepares the final message and the calls the Webservice operation. If it is successfull the response is registered
     * and the OperationStatus is set to true, otherwise the Operation status will be set to false and an Exception of the type
     * soapFault will be registered instead.
     * 
     * @throws SoapFault
     * @access public
     */
    public function execute()
    {
        try
        {
            $this->apiMessage['Version'] = API_VERSION;
        	
            $this->apiMessage['DoExpressCheckoutPaymentRequestDetails'] = $this->apiMessage;
        
            $this->apiMessage = array('DoExpressCheckoutPaymentRequest' => $this->apiMessage);
            
            $this->apiMessage = array($this->apiMessage);
            
            parent::registerAPIResponse(PayPalBase::getSoapClient()->__soapCall('DoExpressCheckoutPayment', $this->apiMessage, null, PayPalBase::getSoapHeader()));
            
            PaypalBase::setOperationStatus(true);          
        }
        
        catch (SoapFault $Exception)
        {
            parent::registerAPIException($Exception);
            
            PaypalBase::setOperationStatus(false);
        }
    }
}












/********************************************************/
/********** getexpresscheckoutdetails.php ***************/
/********************************************************/

/**
 * GetExpressCheckoutDetails Object
 *
 * This class is used to perform the GetExpressCheckoutDetails operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @version 0.2.0
 * @package ExpressCheckout
 * @filesource
 */


/**
 * Used to invoke the GetExpressCheckoutDetails Operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package ExpressCheckout
 */
final class GetExpressCheckoutDetails extends PaypalAPI implements OperationsTemplate
{   
    /**
     * Message Sent to the Webservice
     *
     * @var array
     * @access private
     */
    private $apiMessage;
    
    /**
     * Token value returned from setExpressCheckout
     *
     * This is a 20 single-byte characters timestamped token, the value of which was returned by SetExpressCheckoutResponse
     * 
     * @var string
     * @access private
     */
    private $tokenValue;
    
    
    /**
     * Prepares the message to be sent
     *
     * This method prepares the message to be sent to the 
     * Paypal Webservice
     * 
     * @access public
     * @param string $Token
     */
    public function setParams($Token)
    {
        $this->tokenValue = $Token;
    }
    
    /**
     * Executes the Operation
     *
     * Prepares the final message and the calls the Webservice operation. If it is successfull the response is registered
     * and the OperationStatus is set to true, otherwise the Operation status will be set to false and an Exception of the type
     * soapFault will be registered instead.
     * 
     * @throws SoapFault
     * @access public
     */
    public function execute()
    {
        try
        {
            $this->apiMessage['Version'] = API_VERSION;
        	
            $this->apiMessage['Token']   = $this->tokenValue;
            
            $this->apiMessage = array('GetExpressCheckoutDetailsRequest' => $this->apiMessage);
            
            $this->apiMessage = array($this->apiMessage);
            
            parent::registerAPIResponse(PayPalBase::getSoapClient()->__soapCall('GetExpressCheckoutDetails', $this->apiMessage, null, PayPalBase::getSoapHeader()));
            
            PaypalBase::setOperationStatus(true);           
        }
        
        catch (SoapFault $Exception)
        {
            parent::registerAPIException($Exception);
            
            PaypalBase::setOperationStatus(false);
        }
    }
}






/****************************************************/
/********** gettransactiondetails.php ***************/
/****************************************************/

/**
 * GetTransactionDetails Object
 *
 * This class is used to perform the GetTransactionDetails operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @version 0.2.1
 * @package TransactionSearch
 * @filesource
 */


/**
 * Used to invoke the GetExpressCheckoutDetails Operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package TransactionSearch
 */
final class GetTransactionDetails extends PaypalAPI implements OperationsTemplate
{   
    /**
     * Message Sent to the Webservice
     *
     * @var array
     * @access private
     */
    private $apiMessage;
    
    /**
     * The TransactionID
     * 
     * The TransactionID to get details for.
     *  
     * @var string
     * @access private
     */
    private $TransactionID;
    
    
    /**
     * Prepares the message to be sent
     *
     * This method prepares the message containing the TransactionID to be sent to the 
     * Paypal Web service
     * 
     * @access public
     * @param string $TransactionID
     */
    public function setParams($TransactionID)
    {
        $this->TransactionID = $TransactionID;
    }

    /**
     * Executes the GetTransactionDetails Operation
     *
     * Prepares the final message and the calls the Webservice operation. If it is successfull the response is registered
     * and the OperationStatus is set to true, otherwise the Operation status will be set to false and an Exception of the type
     * soapFault will be registered instead.
     * 
     * @throws SoapFault
     * @access public
     */
    public function execute()
    {
        try
        {
            $this->apiMessage['Version'] = API_VERSION;
        	
            $this->apiMessage['TransactionID']   = $this->TransactionID;
            
            $this->apiMessage = array('GetTransactionDetailsRequest' => $this->apiMessage);
            
            $this->apiMessage = array($this->apiMessage);
            
            parent::registerAPIResponse(PayPalBase::getSoapClient()->__soapCall('GetTransactionDetails', $this->apiMessage, null, PayPalBase::getSoapHeader()));
            
            PaypalBase::setOperationStatus(true);           
        }
        
        catch (SoapFault $Exception)
        {
            parent::registerAPIException($Exception);
            
            PaypalBase::setOperationStatus(false);
        }
    }
}




/*******************************************/
/********** paypal_types.php ***************/
/*******************************************/

/**
 * Preparation of WebService DataTypes
 *
 * Copyright (C) 2007 Israel Ekpo
 * 
 * Contains the class which has methods that generates a list of datatypes (arrays) used by the webs service.
 * Most of the datatypes are passed to the SoapClient->__soapCall() method as multi-dimensional arrays.
 * 
 * @package PaypalTypes
 * @author Israel Ekpo <israelekpo@sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @version 0.2.1
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @filesource
 */

/**
 * This class contains the list of all the Data Types used when contacting the Paypal webservice
 * 
 * @package PaypalTypes
 * @author Israel Ekpo <israelekpo@sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @filesource
 */
class PayPalTypes
{   
    /**
     * Formats the Amount according to the BasicAmountType
     *
     * This method creates a classical example of an element that has an attribute.
     * For example <element attribute1='attr1' attribute2='attr2'>value</element>
     * will be created as array('_' => 'value', 'attr1' => 'attribute1', 'attr2' => 'attribute2');
     * 
     * @param string $amount
     * @param string $currencyID
     * @usedby PaypalTypes::PaymentDetailsType()
     * @usedby PaypalTypes::PaymentDetailsItemType()
     * @return BasicAmountType
     */
    static public function BasicAmountType($amount = '0.00', $currencyID = 'USD')
    {
        $BasicAmountType['_'] = number_format($amount, 2);
        $BasicAmountType['currencyID'] = $currencyID;
        
        return $BasicAmountType;
    }
    
    /**
     * Prepares the AddressType
     * 
     * This method is used to generate the AddressType for Payment information.
     *
     * @param string $Name
     * @param string $Street1
     * @param string $Street2
     * @param string $CityName
     * @param string $StateOrProvince
     * @param string $PostalCode
     * @param string $Country
     * @param string $Phone
     * @return AddressType
     */
    static public function AddressType($Name = '', $Street1 = '', $Street2 = '', $CityName = '', $StateOrProvince = '', $PostalCode = '', $Country = '', $Phone = '')
    {
        $AddressType['Name'] = $Name;
        $AddressType['Street1'] = $Street1;
        $AddressType['Street2'] = $Street2;
        $AddressType['CityName'] = $CityName;
        $AddressType['StateOrProvince'] = $StateOrProvince;
        $AddressType['Country'] = $Country;
        $AddressType['PostalCode'] = $PostalCode;
        $AddressType['Phone'] = $Phone;
        
        return $AddressType;
    }
    
    /**
     * Prepares the PersonNameType
     * 
     * The person name type is used to format the name information for onward inclusion in the SOAP message.
     *
     * @param string $Salutation
     * @param string $FirstName
     * @param string $MiddleName
     * @param string $LastName
     * @param string $Suffix
     * @return PersonNameType
     */
    static public function PersonNameType($Salutation = '', $FirstName = '', $MiddleName = '', $LastName = '', $Suffix = '')
    {
        if ($Salutation)
        {
            $PersonNameType['Salutation'] = $Salutation;
        }
        
        $PersonNameType['FirstName'] = $FirstName;
        
        if ($MiddleName)
        {
            $PersonNameType['MiddleName'] = $MiddleName;
        }
        
        $PersonNameType['LastName']  = $LastName;
        
        if ($Suffix)
        {
            $PersonNameType['Suffix'] = $Suffix;
        }
        
        return $PersonNameType;
    }
    
    /**
     * Prepares the PayerInfoType
     *
     * The PayerInfoType uses the AddressType and PersonNameType to generate a multi-dimensional array
     * that will in turn be used as part of a SOAP message.
     * 
     * @param EmailAddressType $Payer e.g buyer@gmail.com
     * @param string $PayerID
     * @param string $PayerStatus verified, unverified
     * @param PersonNameType $PayerName
     * @param string $PayerCountry
     * @param string $PayerBusiness
     * @param AddressType $Address
     * @param string $ContactPhone
     * @return PayerInfoType
     */
    static public function PayerInfoType($Payer = '', $PayerID = '', $PayerStatus = 'verified', $PayerName, $PayerCountry= 'US', $PayerBusiness = '', $Address, $ContactPhone = '')
    {
		$PayerInfoType['Payer'] = $Payer;
		$PayerInfoType['PayerID'] = $PayerID;
		$PayerInfoType['PayerStatus'] = $PayerStatus;
		$PayerInfoType['PayerName'] = $PayerName;
		$PayerInfoType['PayerCountry'] = $PayerCountry;
		
		if ($PayerBusiness)
		{
		    $PayerInfoType['PayerBusiness'] = $PayerBusiness;
		}
		
		$PayerInfoType['Address'] = $Address;
		
		if ($ContactPhone)
		{
		    $PayerInfoType['ContactPhone'] = $ContactPhone;
		}
		
		return $PayerInfoType;
    }
    
    /**
     * Makes the CreditCardDetailsType
     * 
     * This is a very essential data type because it has to be formatted correctly. This method is used to 
     * generate an array that is going to be part of the payment details submitted to paypal.
     *
     * @param string $CreditCardType Visa,MasterCard,Amex,Discover,Solo,Switch
     * @param string $CreditCardNumber
     * @param int $ExpMonth
     * @param int $ExpYear
     * @param PayerInfoType $CardOwner
     * @param string $CVV2
     * @return CreditCardDetailsType
     */
    static public function CreditCardDetailsType($CreditCardType, $CreditCardNumber, $ExpMonth, $ExpYear, $CardOwner, $CVV2 = '')
    {
        $CreditCardDetailsType['CreditCardType'] = $CreditCardType;
        $CreditCardDetailsType['CreditCardNumber'] = $CreditCardNumber;
        $CreditCardDetailsType['ExpMonth'] = $ExpMonth;
        $CreditCardDetailsType['ExpYear'] = $ExpYear;
        $CreditCardDetailsType['CardOwner'] = $CardOwner;
        
        if($CVV2)
        {
            $CreditCardDetailsType['CVV2'] = $CVV2;
        }
        
        return $CreditCardDetailsType;
    }
    
    /**
     * Makes PaymentDetailsItemType
     *
     * This generates details about a particular payment item. An array is returned which contains the name, quantity
     * and amound of each item added to the payment.
     * 
     * @param string $Name
     * @param string $Number
     * @param integer $Quantity
     * @param BasicAmountType $Tax
     * @param BasicAmountType $Amount
     * @param string $CurrencyID
     * @return PaymentDetailsItemType
     */
    static public function PaymentDetailsItemType($Name = '', $Number = '', $Quantity = 1, $Tax = '', $Amount = '', $CurrencyID = 'USD')
    {
        $PaymentDetailsItemType['Name'] = $Name;
        $PaymentDetailsItemType['Number'] = $Number;
        $PaymentDetailsItemType['Quantity'] = $Quantity;
        
        if ($Tax)
        {
            $PaymentDetailsItemType['Tax'] = self::BasicAmountType($Tax, $CurrencyID);
        }
        
        if ($Amount)
        {
            $PaymentDetailsItemType['Amount'] = self::BasicAmountType($Amount, $CurrencyID);
        }
        
        return $PaymentDetailsItemType;
    }
    
    /**
     * Creates the PaymentDetailsType
     *
     * This is a critical part of the DoDirectPayment and DoExpressCheckOutPayment operations.
     * The payment details is an overall summary of the payment to be sent to the paypal web service.
     * 
     * @param BasicAmountType $OrderTotal
     * @param BasicAmountType $ItemTotal
     * @param BasicAmountType $ShippingTotal
     * @param BasicAmountType $HandlingTotal
     * @param BasicAmountType $TaxTotal
     * @param string $OrderDescription
     * @param string $Custom
     * @param string $InvoiceID
     * @param string $ButtonSource
     * @param string $NotifyURL Paypal IPN URL
     * @param AddressType $ShipToAddress
     * @param PaymentDetailsItemType $PaymentDetailsItem
     * @param string $CurrencyID
     * @return PaymentDetailsType
     */
    static public function PaymentDetailsType($OrderTotal, $ItemTotal ='0.00', $ShippingTotal = '0.00', $HandlingTotal = '0.00', $TaxTotal = '0.00', $OrderDescription = '', $Custom = '', $InvoiceID = '', $ButtonSource = '', $NotifyURL = '', $ShipToAddress = '', $PaymentDetailsItem = array(), $CurrencyID= 'USD')
    {
        $PaymentDetailsType['OrderTotal'] = self::BasicAmountType($OrderTotal, $CurrencyID);
        $PaymentDetailsType['ItemTotal'] = self::BasicAmountType($ItemTotal, $CurrencyID);
        $PaymentDetailsType['ShippingTotal'] = self::BasicAmountType($ShippingTotal, $CurrencyID);
        $PaymentDetailsType['HandlingTotal'] = self::BasicAmountType($HandlingTotal, $CurrencyID);
        $PaymentDetailsType['TaxTotal'] = self::BasicAmountType($TaxTotal, $CurrencyID);
        $PaymentDetailsType['OrderDescription'] = $OrderDescription;
        $PaymentDetailsType['Custom'] = $Custom;
        $PaymentDetailsType['InvoiceID'] = $InvoiceID;
        $PaymentDetailsType['ButtonSource'] = $ButtonSource;
        $PaymentDetailsType['NotifyURL'] = $NotifyURL;
        $PaymentDetailsType['ShipToAddress'] = $ShipToAddress;
        $PaymentDetailsType['PaymentDetailsItem'] = $PaymentDetailsItem;

        return $PaymentDetailsType;
    }
    
    /**
     * Prepares the DoDirectPaymentRequestDetailsType
     *
     * This basically returns a multi-dimensional array with the Payment Action. Credit Card information, User IP
     * address and the merchant's session id.
     * 
     * @param string $PaymentAction This could be a Sale or Order.
     * @param PaymentDetailsType $PaymentDetails
     * @param CreditCardDetailsType $CreditCard
     * @param string $IPAddress
     * @param string $MerchantSessionId
     * @return DoDirectPaymentRequestDetailsType
     */
    static public function DoDirectPaymentRequestDetailsType($PaymentAction, $PaymentDetails, $CreditCard, $IPAddress, $MerchantSessionId)
    {
        $DoDirectPaymentRequestDetailsType['PaymentAction'] = $PaymentAction;
        $DoDirectPaymentRequestDetailsType['PaymentDetails'] = $PaymentDetails;
        $DoDirectPaymentRequestDetailsType['CreditCard'] = $CreditCard;
        $DoDirectPaymentRequestDetailsType['IPAddress'] = $IPAddress;
        $DoDirectPaymentRequestDetailsType['MerchantSessionId'] = $MerchantSessionId;
        
        return $DoDirectPaymentRequestDetailsType;
    }
    
    /**
     * Prepares the SetExpressCheckoutRequestDetailsType
     * 
     * This is an array with the Amount, Return URL, Cancellation URL and Payment Action (Sale or Order)
     *
     * @param BasicAmountType $OrderTotal
     * @param string $ReturnURL
     * @param string $CancelURL
     * @param string $PaymentAction Sale or Order
     * @return SetExpressCheckoutRequestDetailsType
     */
    static public function SetExpressCheckoutRequestDetailsType($OrderTotal, $ReturnURL, $CancelURL, $PaymentAction, $currencyID = 'USD')
    {
    	$SetExpressCheckoutRequestDetailsType['OrderTotal'] = self::BasicAmountType($OrderTotal, $currencyID);
    	$SetExpressCheckoutRequestDetailsType['ReturnURL'] = $ReturnURL;
    	$SetExpressCheckoutRequestDetailsType['CancelURL'] = $CancelURL;
    	$SetExpressCheckoutRequestDetailsType['PaymentAction'] = $PaymentAction;
    	
    	return $SetExpressCheckoutRequestDetailsType;
    }
    
    /**
     * Makes the DoExpressCheckoutPaymentRequestDetailsType
     *
     * This returns the Payment action, Token value, Payer ID and Payment details as a multi-dimensional array.
     * 
     * @param string $PaymentAction Order or Sale
     * @param string $Token
     * @param string $PayerID
     * @param PaymentDetailsType $PaymentDetails
     * @return DoExpressCheckoutPaymentRequestDetailsType
     */
    static public function DoExpressCheckoutPaymentRequestDetailsType($PaymentAction, $Token, $PayerID, $PaymentDetails)
    {
    	$DoExpressCheckoutPaymentRequestDetailsType['PaymentAction'] = $PaymentAction;
    	$DoExpressCheckoutPaymentRequestDetailsType['Token'] = $Token;
    	$DoExpressCheckoutPaymentRequestDetailsType['PayerID'] = $PayerID;
    	$DoExpressCheckoutPaymentRequestDetailsType['PaymentDetails'] = $PaymentDetails;
    	
    	return $DoExpressCheckoutPaymentRequestDetailsType;
    }
    
    /**
     * Makes the UserIdPasswordType
     *
     * This prepares the authentication message to be passed to paypal. It is an array
     * with the Username, Password and Signature of the user. If the payment is being made on behalf of
     * another account then the username of that account has to be passed as the subject.
     * 
     * @param string $Username
     * @param string $Password
     * @param string $Signature
     * @param string $Subject
     * @return UserIdPasswordType
     */
    static public function UserIdPasswordType($Username = '', $Password = '', $Signature = '', $Subject= '')
    {
        
       $UserIdPasswordType['Username'] = $Username;
       $UserIdPasswordType['Password'] = $Password;
       $UserIdPasswordType['Signature'] = $Signature;
       
       if ($Subject)
       {
           $UserIdPasswordType['Subject'] = $Subject;
       }
       
       return $UserIdPasswordType;
    }
    
    /**
     * Returns the date in the ISO 8601 format
     *
     * Generates the ISO 8601 format using the UNIX timestamp supplied. Like 2007-02-02T01:36:06-05:00
     * which is basically the year, month, day, 24-hour format, minutes, seconds and time zone like -05:00 for EST
     * 
     * @param integer $timeStamp the Unix Timestamp
     * @return string
     */
    static public function dateTimeType($timeStamp = 0)
    {
    	$timeStamp = (int) $timeStamp;
    	
    	if (!$timeStamp)
    	{
    		return date('c');
    	}
    	else 
    	{
    		return date('c', $timeStamp);
    	}
    }

    /**
     * Generates the TransactionSearchRequestType
     * 
     * Prepares a multi-dimensional array to be used in the search.
     *
     * @uses self::dateTimeType
     * @param integer $StartDate UNIXTIMESTAMP
     * @param intefer $EndDate UNIXTIMESTAMP
     * @param string $PayerEmail
     * @param string $ReceiverEmail
     * @param string $ReceiptID
     * @param string $TransactionID
     * @param string $PayerName
     * @param string $AuctionItemNumber
     * @param string $InvoiceID
     * @param string $CardNumber
     * @param string $TransactionClass
     * @param BasicAmountType $Amount
     * @param string $CurrencyCode
     * @param string $Status
     * @return TransactionSearchRequestType
     */
    static public function TransactionSearchRequestType($StartDate, $EndDate = 0, $PayerEmail = '', $ReceiverEmail = '', $ReceiptID = '', $TransactionID = '', $PayerName = '', $AuctionItemNumber = '', $InvoiceID = '', $CardNumber = '', $TransactionClass = '', $Amount = '', $CurrencyCode = '', $Status = '', $currencyID = 'USD')
    {
    	$TransactionSearchRequestType['StartDate'] = self::dateTimeType($StartDate);
    	
    	if ($EndDate)
    	{
    		$TransactionSearchRequestType['EndDate'] = self::dateTimeType($EndDate);
    	}
    	
    	if ($PayerEmail)
    	{
    		$TransactionSearchRequestType['Payer'] = $PayerEmail;
    	}
    	
    	if ($ReceiverEmail)
    	{
    		$TransactionSearchRequestType['Receiver'] = $ReceiverEmail;
    	}
    	
    	if ($ReceiptID)
    	{
    		$TransactionSearchRequestType['ReceiptID'] = $ReceiptID;
    	}

    	if ($TransactionID)
    	{
    		$TransactionSearchRequestType['TransactionID'] = $TransactionID;
    	}
    	
    	if ($PayerName)
    	{
    		$TransactionSearchRequestType['PayerName'] = $PayerName;
    	}
    	
    	if ($AuctionItemNumber)
    	{
    		$TransactionSearchRequestType['AuctionItemNumber'] = $AuctionItemNumber;
    	}
    	    	
    	if ($InvoiceID)
    	{
    		$TransactionSearchRequestType['InvoiceID'] = $InvoiceID;
    	}
    	
    	if ($CardNumber)
    	{
    		$TransactionSearchRequestType['CardNumber'] = $CardNumber;
    	}
    	
    	if ($TransactionClass)
    	{
    		$TransactionSearchRequestType['TransactionClass'] = $TransactionClass; 
    	}
    	
    	if ($Amount)
    	{
    		$TransactionSearchRequestType['Amount'] = self::BasicAmountType($Amount, $currencyID);
    	}

    	if ($CurrencyCode)
    	{
    		$TransactionSearchRequestType['CurrencyCode'] = $CurrencyCode;
    	}
    	
    	if ($Status)
    	{
    		$TransactionSearchRequestType['Status'] = $Status;
    	}
    	
    	return $TransactionSearchRequestType;
    }
}



/*************************************************/
/********** setexpresscheckout.php ***************/
/*************************************************/

/**
 * SetExpressCheckout Object
 *
 * This class is used to perform the SetExpressCheckout operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @version 0.2.1
 * @package ExpressCheckout
 * @filesource
 */


/**
 * Used to invoke the SetExpressCheckout Operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package ExpressCheckout
 */
final class SetExpressCheckout extends PaypalAPI implements OperationsTemplate
{   
    /**
     * Message Sent to the Webservice
     *
     * @var array
     * @access private
     */
    private $apiMessage;

    /**
     * Prepares the message to be sent
     *
     * This method prepares the message to be sent to the 
     * Paypal Webservice
     * 
     * @access public
     * 
     * @param double $OrderTotal
     * @param string $ReturnURL
     * @param string $CancelURL
     * @param string $PaymentAction Sale or Order
     */
    public function setParams($OrderTotal, $ReturnURL, $CancelURL, $PaymentAction)
    {
        $this->apiMessage = PayPalTypes::SetExpressCheckoutRequestDetailsType($OrderTotal, $ReturnURL, $CancelURL, $PaymentAction);
    }
    
    /**
     * Executes the Operation
     *
     * Prepares the final message and the calls the Webservice operation. If it is successfull the response is registered
     * and the OperationStatus is set to true, otherwise the Operation status will be set to false and an Exception of the type
     * soapFault will be registered instead.
     * 
     * @access public
     */
    public function execute()
    {
        try
        {
            $this->apiMessage['Version'] = API_VERSION;
        	
            $this->apiMessage['SetExpressCheckoutRequestDetails'] = $this->apiMessage;
        
            $this->apiMessage = array('SetExpressCheckoutRequest' => $this->apiMessage);
            
            $this->apiMessage = array($this->apiMessage);
            
            parent::registerAPIResponse(PayPalBase::getSoapClient()->__soapCall('SetExpressCheckout', $this->apiMessage, null, PayPalBase::getSoapHeader()));
            
            PaypalBase::setOperationStatus(true);           
        }
        
        catch (SoapFault $Exception)
        {
            parent::registerAPIException($Exception);
            
            PaypalBase::setOperationStatus(false);
        }
    }
}


/************************************************/
/********** transactionsearch.php ***************/
/************************************************/

/**
 * TransactionSearch Object
 *
 * This class is used to perform the TransactionSearch operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @version 0.2.1
 * @package TransactionSearch
 * @filesource
 */


/**
 * Used to invoke the TransactionSearch Operation
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package TransactionSearch
 */
final class TransactionSearch extends PaypalAPI implements OperationsTemplate
{   
    /**
     * Message Sent to the Webservice
     *
     * @var array
     * @access private
     */
    private $apiMessage;
    
    
    /**
     * Generates the TransactionSearchRequestType
     * 
     * Prepares a multi-dimensional array for the message to be used in the search.
     *
     * @param integer $StartDate UNIXTIMESTAMP
     * @param intefer $EndDate UNIXTIMESTAMP
     * @param string $PayerEmail
     * @param string $ReceiverEmail
     * @param string $ReceiptID
     * @param string $TransactionID
     * @param string $PayerName
     * @param string $AuctionItemNumber
     * @param string $InvoiceID
     * @param string $CardNumber
     * @param string $TransactionClass
     * @param float $Amount
     * @param string $CurrencyCode
     * @param string $Status
     * @param string $currencyID
     */

    public function setParams($StartDate, $EndDate = 0, $PayerEmail = '', $ReceiverEmail = '', $ReceiptID = '', $TransactionID = '', $PayerName = '', $AuctionItemNumber = '', $InvoiceID = '', $CardNumber = '', $TransactionClass = '', $Amount = '', $CurrencyCode = '', $Status = '', $currencyID = 'USD')
    {
        $this->apiMessage = PayPalTypes::TransactionSearchRequestType($StartDate, $EndDate, $PayerEmail, $ReceiverEmail, $ReceiptID, $TransactionID, $PayerName, $AuctionItemNumber, $InvoiceID, $CardNumber, $TransactionClass, $Amount, $CurrencyCode, $Status, $currencyID);
    }
    
    /**
     * Executes the Operation
     *
     * Prepares the final message and the calls the Webservice operation. If it is successfull the response is registered
     * and the OperationStatus is set to true, otherwise the Operation status will be set to false and an Exception of the type
     * soapFault will be registered instead.
     * 
     * @throws SoapFault
     * @access public
     */
    public function execute()
    {
        try
        {
            $this->apiMessage['Version'] = API_VERSION;
            
            $this->apiMessage = array('TransactionSearchRequest' => $this->apiMessage);
            
            $this->apiMessage = array($this->apiMessage);
            
            parent::registerAPIResponse(PayPalBase::getSoapClient()->__soapCall('TransactionSearch', $this->apiMessage, null, PayPalBase::getSoapHeader()));
            
            PaypalBase::setOperationStatus(true);           
        }

        catch (SoapFault $Exception)
        {
            parent::registerAPIException($Exception);
            
            PaypalBase::setOperationStatus(false);
        }
    }
}



/******************************************/
/********** paypal_base.php ***************/
/******************************************/

/**
 * Paypal Base Files
 *
 * This file contains very important classes and interfaces that are going to be used in this program
 * 
 * - interface OperationsTemplate
 * - abstract class PaypalAPI
 * - static class PaypalBase
 * - final class PaypalRegistrar
 * - final class WebsitePaymentsPro
 * 
 * phpPaypalPro currently supports 4 major operations available for the Website Payments Pro SOAP API namely:
 * 
 * (a) DoDirectPayment
 * (b) SetExpressCheckout
 * (c) GetExpressCheckoutDetails
 * (d) DoExpressCheckoutPayment
 * 
 * Please be on the lookout as more operations are scheduled to be added in the immediate future.
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @version 0.2.1
 * @package PaypalBase
 * @filesource
 */

/**
 * Contains Webservice Data Types
 *
 * All the data types used by this program is prepared
 * by a class called PaypalTypes in this file.
 */
//require_once('paypal_types.php');

/**
 * Contains Configuration Data
 * 
 * Things like the API version, connection timeouts etc are 
 * registered in this module.
 */
//require_once('config.inc.php');

/**
 * Paypal Base Class
 *
 * This class contains important variables that are going to be
 * used throughout the entire system.
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package PaypalBase
 */
class PaypalBase
{
    /**
     * Soap Client
     *
     * @access private
     * @var soapClient
     */
    private static $soapClient;
    
    /**
     * Soap Header
     *
     * @access private
     * @var soapHeader
     */
    private static $soapHeader;
    
    /**
     * Was the Last Operation a Success
     *
     * @access private
     * @var bool
     */
    private static $lastOperationSuccessful;
    
    /**
     * API Reponse
     *
     * This is the last reponse recieved from the paypal web service
     * 
     * @access private
     * @var mixed
     */
    private static $apiResponse;
    
    /**
     * API Exception
     *
     * @access private
     * @var soapFault
     */
    private static $apiSoapException;

    /**
     * Sets the Value of the soapClient Attribute
     *
     * @static 
     * @access public
     * @param soapClient $soapClient
     */
    public static function setSoapClient($soapClient)
    {
        self::$soapClient = $soapClient;
    }
    
    /**
     * Sets the Value of the SoapHeader
     *
     * @static 
     * @access public
     * @param SoapHeader $soapHeader
     */
    public static function setSoapHeader($soapHeader)
    {
        self::$soapHeader = $soapHeader;
    }

    /**
     * Sets API response from Webservice
     *
     * @static 
     * @access public
     * @param mixed $apiResponse
     */
    public static function setApiResponse($apiResponse)
    {
        self::$apiResponse = $apiResponse;
    }
    
    /**
     * Registers the Exception that was thrown
     *
     * @static
     * @access public
     * @param soapFault $apiSoapException
     */
    public static function setAPISoapFault($apiSoapException)
    {
        self::$apiSoapException = $apiSoapException;
    }
    
    /**
     * Sets the Last Operation Status
     * 
     * This tells us whether or not the last operation was a success.
     * 
     * @static
     * @access public
     * @param bool $status
     */
    public static function setOperationStatus($status)
    {
        self::$lastOperationSuccessful = $status;
    }
    
    /**
     * Returns the Soap Client
     *
     * @static
     * @access public
     * @return SoapClient
     */
    public static function getSoapClient()
    {
        return self::$soapClient;
    }
    
    /**
     * Returns the Soap Header
     *
     * @static 
     * @access public
     * @return SoapHeader
     */
    public static function getSoapHeader()
    {
        return self::$soapHeader;
    }
    
    /**
     * Returns the last API Response
     *
     * This method returns the last response from the paypal webservice.
     * 
     * @static 
     * @access public
     * @return mixed
     */
    public static function getAPIResponse()
    {
        return self::$apiResponse;
    }
    
    /**
     * Returns the Exception object that was thrown
     *
     * @static
     * @access public
     * @return soapFault
     */
    public static function getAPIException()
    {
        return self::$apiSoapException;
    }
    
    /**
     * Returns the Last Operation Status
     *
     * It returns true if the last operation was a success and false
     * if it was not.
     * 
     * @static 
     * @access public
     * @return bool
     */
    public static function getOperationStatus()
    {
        return self::$lastOperationSuccessful;
    }
}

/**
 * Template for all Operations Objects
 *
 * This is the template for all the classes that are used for each operation.
 * Each and every one of them must have these two methods. The other methods will
 * be inherited from the abstract PaypalAPI class.
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package PaypalBase
 */
interface OperationsTemplate
{   
    /**
     * Calls the Paypal Web Service
     *
     * This method prepares the final message for the operation,
     * puts it in the right format and then invokes the __soapCall method
     * from the static soapClient. The name of the operation, the message, and the 
     * input headers are passed to the __soapCall() method
     */
    public function execute();
    
    /**
     * Tells us if the Operation was Successful
     *
     * If the operation was successful, this method will return true,
     * otherwise it will return false
     * 
     * @return bool
     */
    public function success(); 
}

/**
 * Abstract Class for Paypal API Core Methods
 *
 * This is an abstract class that contains 6 important methods that 
 * are used by all the Operations classes.
 * 
 * @abstract 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package PaypalBase
 */
abstract class PaypalAPI
{
    /**
     * Operation Status Registration
     *
     * This methods sets the lastOperationStatus attribute in the PaypalBase class.
     * If the last operation was a success, this attribute is set to true, otherwise
     * it will be set to false.
     * 
     * @access private
     * @param bool $status
     */
    protected function registerLastOperationStatus($status)
    {
        PayPalBase::setLastOperationStatus($status);
    }
    
    /**
     * API Response Registration
     *
     * This method registers the response recieved from the Paypal Webservice.
     * As long as there were no errors it will be set. If it returns only one value
     * then it may be an integer, float or a string. However it is returning 
     * multiple values then it may be an associative array or an object.
     * 
     * @access private
     * @param mixed $APIResponse
     */
    protected function registerAPIResponse($APIResponse)
    {
        PayPalBase::setApiResponse($APIResponse);
    }

    /**
     * API Exception Registration
     * 
     * This method registers any exception that occurs as a result of a soapFault
     * being thrown. If a soapFault object was throw while the Operations object
     * was calling the execute method, then this method will be invoked instead
     * of the registerAPIResponse() method.
     * 
     * @access private
     * @param SoapFault $APIException
     */
    protected function registerAPIException($APIException)
    {
        PayPalBase::setAPISoapFault($APIException);
    }
    
    /**
     * Was the Operation Successful
     * 
     * This method tells us whether or not the last operation was a success
     * 
     * @access public
     * @return bool
     */
    public function success()
    {
        return PaypalBase::getOperationStatus();
    }
    
    /**
     * Returns the API Response
     *
     * The response from the Paypal Webservice can be accessed from this method
     * 
     * @access public
     * @return mixed
     */
    public function getAPIResponse()
    {
    	return PaypalBase::getAPIResponse();
    }
    
    /**
     * Returns any SoapFault thrown
     *
     * Any exception that is thrown can be accessed from this method
     * 
     * @access public
     * @return SoapFault
     */
    public function getAPIException()
    {
    	return PaypalBase::getAPIException();
    }
}

/**
 * Paypal Registrar
 *
 * The purpose of this class is to create SoapClients and SoapHeaders just before each operation
 * is carried out.
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package PaypalBase
 */
final class PayPalRegistrar
{   
	/**
	 * Creates the Static SoapClient
	 *
	 * The soap_version element is used to indicate whether you are using SOAP 1.1 or SOAP 1.2
	 * The exceptions option is a boolean value defining whether soap errors throw exceptions of type SoapFault.
	 * 
	 * Setting the boolean trace option enables use of the methods  SoapClient->__getLastRequest,  SoapClient->__getLastRequestHeaders,  
	 * SoapClient->__getLastResponse and  SoapClient->__getLastResponseHeaders
	 * 
	 * The trace option is necessary if you will need to use the SoapClient->__getLastRequest() or SoapClient->__getLastReponse() methods
	 * later for debugging purposes. The connection_timeout option defines a timeout in seconds for the connection to the SOAP service.
	 * I have set it to 10 minutes. 
	 * 
	 * If for any reason you believe you software is going to take longer than usually, please feel free to 
	 * adjust the maximum_execution_time value either during runtime or in the php.ini configuration file.
	 * 
	 * @access public
	 */
    public static function registerSoapClient()
    {
        $clientOptions = array("soap_version" => SOAP_1_1, "exceptions" => true, "trace" => true, "connection_timeout " => API_CONNECTION_TIMEOUT);
        PayPalBase::setSoapClient(new SoapClient(API_WSDL, $clientOptions));
    }
    
    /**
     * Creates the static SoapHeader
     * 
     * This is the Custom Security Header passed to paypal.
     * 
     * The username here is not the same as you account username and so is the password.
     * The subject is the payment on whose behalf you have been authorized to make the operation call.
     *
     * @param string $Username
     * @param string $Password
     * @param string $Signature
     * @param string $Subject
     */
    public static function registerSoapHeader($Username, $Password, $Signature, $Subject = '')
    {
        $Credentials     = new SoapVar(PayPalTypes::UserIdPasswordType($Username, $Password, $Signature, $Subject), SOAP_ENC_OBJECT);
        $headerNameSpace = 'urn:ebay:api:PayPalAPI';
        $headerName      = 'RequesterCredentials';
        $headerData      = array("Credentials" =>  $Credentials);
        $mustUnderstand  = true;
        
        PayPalBase::setSoapHeader(new SoapHeader($headerNameSpace, $headerName, $headerData, $mustUnderstand));
    }
}

/**
 * Website Payments Pro Operations Factory
 *
 * The purpose of this class is to provide a way of selecting any operation object
 * from just a single source. It also intializes the soapClient and soapHeaders that 
 * are going to be used for any of the operations.
 * 
 * @author Israel Ekpo <perfectvista@users.sourceforge.net>
 * @copyright Copyright 2007, Israel Ekpo
 * @license http://phppaypalpro.sourceforge.net/LICENSE.txt BSD License
 * @package PaypalBase
 */
final class WebsitePaymentsPro
{
    /**
     * Prepares the System for an Operation call
     *
     * This is when the SoapClient and SoapHeader static objects are actually created.
     *
     * @param string $Username The API user name
     * @param string $Password The password for the API call. Different from account password
     * @param string $Signature The signature for the 3-token authentication
     * @param string $Subject The person on whose behalf the operation is made
     * @uses PaypalRegistrar::registerSoapClient()
     * @uses PaypalRegistrar::registerSoapHeader()
     * @access public
     */
    public function prepare($Username, $Password, $Signature, $Subject='')
    {
       PayPalRegistrar::registerSoapClient();

       PayPalRegistrar::registerSoapHeader($Username, $Password, $Signature, $Subject);      
    }   
    
    /**
     * WebSite Payments Pro Operations Factory
     *
     * The name of the operation is passed to this method so that
     * it will return the right object to do the job. It is not case sensitive,
     * so you do not have to worry about the case of the letters when passing the 
     * operation name to the method.
     *  
     * @access public
     * @param string $operation This is case insensitive
     * @return mixed The Operation you wish to call
     */
    public function selectOperation($operation)
    {
        switch (strtolower(trim($operation)))
        {
            case 'dodirectpayment':
            {
                //require_once('dodirectpayment.php');
                return new DoDirectPayment();
            }
                
            case 'setexpresscheckout':
            {
                //require_once('setexpresscheckout.php');
                return new SetExpressCheckout();
            }
            
            case 'getexpresscheckoutdetails':
            {
                //require_once('getexpresscheckoutdetails.php');
                return new GetExpressCheckoutDetails();
            }
            
            case 'doexpresscheckoutpayment':
            {
                //require_once('doexpresscheckoutpayment.php');
                return new DoExpressCheckoutPayment();
            }
            
            case 'transactionsearch':
        	{
        		//require_once('transactionsearch.php');
        		return new TransactionSearch();
        	}
        	
            case 'gettransactiondetails':
        	{
        		//require_once('gettransactiondetails.php');
        		return new GetTransactionDetails();
        	}
        }
    }
}

?>
