<?php
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
/*

Handles file uploads


For the lastest version go to:
http://www.phpclasses.org/browse.html/package/949.html

This class generates the form elements for a file upload and stores the
uploaded files in a directory.
- You restrict the file size and the allowed file types easily.
- The uploaded file information is returned in an associative array.


This class uses my form generating class, Form
(http://www.phpclasses.org/browse.html/package/931.html)

////////////////////////////////////////////////////////////////////////

    CONSTRUCTOR:
        function clsUploader()
    PUBLIC:
        function openForm($action, $add_on = '')
        function fileField($size = -1, $fileSize=1048576, $accept='text/*')
        function closeForm($addSubmitButton = true)
        function uploadTo($path, $overwrite=false, $allowedTypeArray=null)
        function wasSubmitted() {
        function debug()
    PRIVATE:
        function _error($msg)

    PUBLIC VARS:
        var $error

////////////////////////////////////////////////////////////////

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/
////////////////////////////////////////////////////////////////////////
/**
* Class for handling file uploads
*
* @author	    Lennart Groetzbach <lennartg@web.de>
* @copyright	Lennart Groetzbach <lennartg@web.de> - distributed under the LGPL
* @version 	    0.5 - 2003/01/13
*/

////////////////////////////////////////////////////////////////////////
/**
* @access   public
*/
class clsUploader {

////////////////////////////////////////////////////////////////////////
/**
* maximum file size
*
* @access   private
* @type     Integer
*/
public $_size = 1048576;

/**
* errors
*
* @access   public
* @type     String
*/
public $error = '';

////////////////////////////////////////////////////////////////////////
/**
* Constructor
*
* @access   public
*/
function __construct() {
    if (!ini_get("safe_mode")) {
    } else {
        die ("Turn 'safemode' in your php.ini off!");
    }
}
////////////////////////////////////////////////////////////////////////
//PUBLIC
////////////////////////////////////////////////////////////////////////
/**
* Returns debug information
*
* @access   public
*
* @return   String      debug information
*/
function debug() {
    $str = '';
    $str .='safemode: "' . (ini_get('safe_mode') ? 'on" (bad idea!)' : 'off"') . "\n";
    $str .='upload_tmp_dir: "' . ini_get('upload_tmp_dir') . "\"\n";
    $str .='local TMP dir: "' . getenv('TMP') . "\"\n";
    $str .='local TEMP dir: "' . getenv('TEMP') . "\"\n";
    $str .='upload_max_filesize: "' . ini_get("upload_max_filesize") . "\"\n";
    $str .='post_max_size: "' . ini_get("post_max_size") . "\"\n";
    return $str;
}

////////////////////////////////////////////////////////////////////////
/**
* Returns open form tag
*
* @access   public
*
* @param    String      $action     the target page
* @param    String      $add_on     add on for the tag
*
* @return   String      form tag
*/
function openForm($action, $add_on = '') {
    return Forms::openForm($action, 'post', '', true, '', $add_on);
}

////////////////////////////////////////////////////////////////////////
/**
* Returns input file field tag
*
* @access   public
*
* @param    Integer     $fileSize
* @param    Integer     $size
* @param    String      $accept
* @param    String      $add_on
*
* @return   String      input file tag
*/
function fileField($fileSize=1048576, $size=-1, $accept='text/*', $add_on='') {
    return Forms::fileField('', '', $fileSize, $size, $accept, $add_on);
}

////////////////////////////////////////////////////////////////////////
/**
* Returns close form tag and internal tags
* MUST BE CALLED!
*
* @access   public
*
* @param    Boolean     $addSubmitButton
*
* @return   String      closing form tags
*/
function closeForm($addSubmitButton = true) {
    return Forms::hidden('_uploaded', 'true') ."\n"
            . Forms::hidden('MAX_FILE_SIZE', $this->_size) ."\n"
            . ($addSubmitButton ? Forms::submitButton() : '') 
            . Forms::closeForm();
}

////////////////////////////////////////////////////////////////////////
/**
* Uploads files
*
* @access   public
*
* @param    String      $path               dir to move files to
* @param    Boolean     $overwrite          overwrite existing file?   
* @param    Array       $allowedTypeArray   array of allowed file types, if empty all files are allowed
*
* @return   Array       array with file information
*/
function uploadTo($path, $overwrite=false, $allowedTypeArray=null) {
    // fix path
    $path = str_replace('\\', '/', $path);
    if (substr($path, -1) != '/') {
        $path .= '/';
    }

    // does upload path exists?
    if ((file_exists($path)) && (is_writable($path))) {

        // for all files
        $res = array();

        // get the file list

        if (@$_FILES) {							// NAVEED: $_FILES instead of $_FILE for php5
            $files = &$_FILES;
        } else {
			$files = &$_GLOBALS['HTTP_POST_FILES'];
        }

        if ($files == "") return(""); // Naveed: december 2008

        // for all files...
        foreach($files as $key => $file)
        {
            // does the file exist?
			if (strpos($file['name'], "'") !== false) die("Invalid FileName, the filename cannot contains characters like: ' (apostrophe), etc.");

            if (!@$file['error'] && $file['size'] && $file['name'] != '') {
                // is the file type allowed?
                if (($allowedTypeArray == null) || (@in_array($file['type'], $allowedTypeArray))) {
                    // is it really an uploaded file?
                    if (is_uploaded_file($file['tmp_name'])) {
                        // does file exists?
                        $exists = file_exists($path . $file['name']);
                        // overwrite file?
                        if ($overwrite || !$exists) {
                            // move file to new destination
                            move_uploaded_file($file['tmp_name'], $path . $file['name']);
                            // store name, path, type and size information
                            array_push ($res, array('name' => $file['name'], 'full_path' => $path . $file['name'], 'type' => $file['type'], 'size' => $file['size'], 'overwritten' => $exists));
                        } else {
                            $this->error .= $this->_error("File \"" . $file['name'] . "\" already exists!");
                        }
                    } else {
                        $this->error .= $this->_error("File \"" . $file['name'] . "\" is not a file!");
                    }
                } else {
                    $this->error .= $this->_error("Content Type \"" .  $file['type'] . "\" for file \"".$file['name']."\" not allowed!");
                }
            } else {
                if (@$file['error'] && $file['error'] != 4) {
                    $this->error .= $this->_error("File \"" .  $file['name'] . "\" does not exist!");
                }
            }
        }
        return $res;
    }
    $this->_error("Path \"$path\" does not exist or is not writable!");
    return false;
}

////////////////////////////////////////////////////////////////////////
/**
* Checks if an upload was made
*
* @access   public
* @return   boolean
*/
function wasSubmitted() {
    return (@$GLOBALS['HTTP_POST_VARS']['_uploaded'] == "true");
}

////////////////////////////////////////////////////////////////////////
// PRIVATE
////////////////////////////////////////////////////////////////////////
/**
* Generates error message
*
* @access   private
* @param    String      $msg
* @return   String
*/
function _error($msg) {
    $this->error .= date('Y-m-d H:i:s') . ' | ' . basename($GLOBALS['HTTP_SERVER_VARS']['PHP_SELF'])  . ' | ' . $msg . "\n";
}


function dumpAssociativeArray($array)
{
    $res = '';
    $header = false;
    if (is_array($array) && sizeof($array)) {
        $res .= "<table border=1>\n";
        foreach(@$array as $values) {
            if (!$header) {
                $res .= "<th>" . implode("</th><th>", array_keys($values)) . "</th>\n";
                $header = true;
            }
            $res .= "<tr>\n";
            foreach($values as $key => $value) {
                $res .= "<td>" . ($value != '' ? $value : "&nbsp;") . "</td>";
                if ($key == 'full_path')
                	$varPath = $value;
            }
            $res .= "</tr>\n";
        }
        $res .= "</table>\n";
    }
    return $varPath;
}


////////////////////////////////////////////////////////////////////////
}
////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////
/*

Utility class for generating forms


For the lastest version go to:
http://www.phpclasses.org/browse.html/package/931.html



////////////////////////////////////////////////////////////////////////

function openForm($action, $method = 'post', $target = '', $fileUpload = false, $charset = '', $additional = '')
function closeForm()
function openFieldset($title, $pre = '')
function closeFieldset()
function hidden($name, $value='')
function checkBox($value, $group='', $selected=false, $additional='')
function checkBoxes($array, $group='', $selectArray, $additional='')
function radioButtons($array, $group='', $selectId=1, $additional = '')
function radioButton($value, $group='', $selected=false, $additional='')
function selectBox($array, $selected=0, $name='', $size=1, $multiple=false, $additional='')
function submitButton($title = '', $new = false, $newContent = '', $additional='')
function resetButton($title = '', $new = false, $newContent = '', $additional='')
function button($title, $name = '', $new = false, $newContent = '', $additional = '')
function textField($value, $name='', $hidden=false, $size =-1, $length =-1, $additional='')
function fileField($name = '', $file = '', $fileSize = 1000000, $size = -1, $accept="text/*", $additional='')
function textArea($value, $name='', $cols=-1, $rows=-1, $wrap='soft' ,$readOnly=false, $additional='')

////////////////////////////////////////////////////////////////

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.
    
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/
////////////////////////////////////////////////////////////////////////
/**
* @author	    Lennart Groetzbach <lennartg@web.de>
* @copyright	Lennart Groetzbach <lennartg@web.de> - distributed under the LGPL
* @version 	    0.5 - 2002/12/30
*/

class Forms {

////////////////////////////////////////////////////////////////////////

function openForm($action, $method = 'post', $target = '', $fileUpload = false, $charset = '', $additional = '') {
    $res = '';
    $res .= "<form action=\"$action\" method=\"$method\"";
    $res .= ($fileUpload == true ? " enctype=\"multipart/form-data\"" : '');
    $res .= ($charset == '' ? '' : " accept-charset=\"$charset\"");
    $res .= ($target == '' ? '' : " target=\"$target\"");
    $res .= ($additional ? " $additional" : '') . '>';
    return $res;
}

////////////////////////////////////////////////////////////////////////

function closeForm() {
    return "</form>";
}

////////////////////////////////////////////////////////////////////////

function openFieldset($title, $pre = '') {
    return "<fieldset><legend>$pre$title</legend>";
}

////////////////////////////////////////////////////////////////////////

function closeFieldset() {
    return "</fieldset>";
}

////////////////////////////////////////////////////////////////////////

function hidden($name, $value='') {
    return "<input type=\"hidden\" name=\"$name\" value=\"$value\">";
}

////////////////////////////////////////////////////////////////////////

function checkBox($value, $group='', $selected=false, $additional='') {
    static $count = 0;
    if ($group == '') {
        $group = 'checkBox' . ++$count;
    }    $res = '';
    $res .= "<input type=\"checkbox\" name=\"$group\" value=\"$value\"" . ($selected==false ? '' : " checked=\"checked\"") .  ($additional ? " $additional" : '') . ">"; 
    return $res;
}

////////////////////////////////////////////////////////////////////////

function checkBoxes($array, $group='', $selectArray, $additional='') {
    if (is_array($array) && is_array($selectArray)) {
        static $count = 0;
        if ($group == '') {
            $group = 'boxGroup' . ++$count;
        }
        $res = array();
        $i = 0;
        foreach($array as $value) {
            $i++;
            $res[$i]= Forms::checkBox($value, $group, $selectArray[$i - 1], $additional) . "\n";
        }
    }    
    return $res;
}

////////////////////////////////////////////////////////////////////////

function radioButtons($array, $group='', $selectId=1, $additional = '') {
    static $count = 0;
    if ($group == '') {
        $group = 'radioGroup' . ++$count;
    }
    if (is_array($array)) {
        $res = array();
        $i = 0;
        foreach($array as $value) {
            $res[$i++]= Forms::radioButton($value, $group, (int)($selectId == $i), $additional) . "\n";
        }
    }    
    return $res;
}

////////////////////////////////////////////////////////////////////////

function radioButton($value, $group='', $selected=false, $additional='') {
    static $count = 0;
        if ($group == '') {
            $group = 'radioButton' . ++$count;
        }
    $res = '';
    $res .= "<input type=\"radio\" name=\"$group\" value=\"$value\"" . ($selected==false ? '' : " checked=\"checked\"") . ($additional ? " $additional" : '') . ">"; 
    return $res;
}

////////////////////////////////////////////////////////////////////////

function selectBox($array, $selected=0, $name='', $size=1, $multiple=false, $additional='') {
    $res = '';
    static $count = 0;
    if (is_array($array)) {
        if ($name == '') {
            $name = 'selectBox' . ++$count;
        }
        $res .= "<select name=\"$name\" size=\"$size\"" . ($multiple==false ? '' : " multiple=\"multiple\"") . ($additional ? " $additional" : '') . ">\n";
        $i = 0;
        foreach($array as $value) {
            $res .= "<option" . ($selected == ++$i ? " selected=\"selected\"" : '') . ">$value</option>\n";
        }
        $res .="</select>\n";
    }
    return $res;
}

////////////////////////////////////////////////////////////////////////

function submitButton($title = '', $new = false, $newContent = '', $additional='') {
    $res = '';
    if ($new == true) {
        $res .= "<button type=\"submit\" $additional>$newContent$title</button>";
    } else {
        $res .= "<input type=\"submit\" " . ($title == '' ? '' : "value=\"$title\"") . ($additional ? " $additional" : '') . ">";
    }
    return $res;
}

////////////////////////////////////////////////////////////////////////

function resetButton($title = '', $new = false, $newContent = '', $additional='') {
    $res = '';
    if ($new == true) {
        $res .= "<button type=\"reset\" $additional>$newContent$title</button>";
    } else {
        $res .= "<input type=\"reset\" " . ($title == '' ? '' : "value=\"$title\"") . ($additional ? " $additional" : '') . ">";
    }
    return $res;
}

////////////////////////////////////////////////////////////////////////

function button($title, $name = '', $new = false, $newContent = '', $additional = '') {
    static $count = 0;
    if ($name == '') {
        $name = 'button' . ++$count;
    }
    $res = '';
    if ($new == true) {
        $res .= "<button type=\"button\" name=\"$name\" $additional>$newContent$title</button>";
    } else {
        $res .= "<input type=\"button\" name=\"$name\" value=\"$title\"".($additional ? " $additional" : '').">";
    }
    return $res;
}

////////////////////////////////////////////////////////////////////////

function textField($value, $name='', $hidden=false, $size =-1, $length =-1, $additional='') {
    $res = '';
    static $count = 0;
    if ($name == '') {
        $name = 'textField' . ++$count;
    }
    $res .= "<input name=\"$name\" type=\"" . ($hidden ? 'password' : 'text') . "\" value=\"$value\"";
    $res .= ($size != -1 ? " size=\"$size\"" : '');
    $res .= ($length != -1 ? " maxlength=\"$length\"" : '');
    $res .=  ($additional ? " $additional" : '') . ">";
    return $res;
}

////////////////////////////////////////////////////////////////////////

function fileField($name = '', $file = '', $fileSize = 1000000, $size = -1, $accept="text/*", $additional='') {
    static $count = 0;
    if ($name == '') {
        $name = 'fileField' . ++$count;
    }    $res = '';
    $res .= "<input type=\"file\" name=\"$name\"";
    $res .= ($size != -1 ? " size=\"$size\"" : '');
    $res .= ($file != '' ? " value=\"$file\"" : '');
    $res .= " maxlength=\"$fileSize\" accept=\"$accept\"".($additional ? " $additional" : '').">";
    return $res;
}
    

////////////////////////////////////////////////////////////////////////

function textArea($value, $name='', $cols=-1, $rows=-1, $wrap='soft' ,$readOnly=false, $additional='') {
    $res = '';
        static $count = 0;
    if ($name == '') {
        $name = 'textArea' . ++$count;
    }
    $res .= "<textarea name=\"$name\" wrap=\"$wrap\"";
    $res .= ($rows != -1 ? "rows=\"$rows\"" : '');
    $res .= ($cols != -1 ? "cols=\"$cols\"" : '');
    $res .= ($readOnly ? " readonly=\"readonly\"" : '');
    $res .= ($additional ? " $additional" : '') . ">";
    $res .= $value;
    $res .= '</textarea>';
    return $res;
}
////////////////////////////////////////////////////////////////////////
}
?>
