<?php

// http://www.edg3.co.uk/snippets/weather-location-codes/
class clsWeather
{
    public $aWeatherLocationCodes;

    function __construct()
    {
    	$this->aWeatherLocationCodes = array(
        "Chiniot, Pakistan" => "PKXX0001",
        "Faisalabad, Pakistan" => "PKXX0002",
        "Gujranwala, Pakistan" => "PKXX0003",
        "Gujrat, Pakistan" => "PKXX0004",
        "Hyderabad, Pakistan" => "PKXX0005",
        "Islamabad, Pakistan" => "PKXX0006",
        "Karachi, Pakistan" => "PKXX0008",
        "Kasur, Pakistan" => "PKXX0009",
        "Kohat, Pakistan" => "PKXX00010",
        "Lahore, Pakistan" => "PKXX00011");
    }

    function GetWeatherWidget($sLocation)
    {
        $sLocation = $this->aWeatherLocationCodes[$sLocation];
        $sReturn = '<embed title="Weather" src="http://www.weatherlet.com/weather.swf?locid=' . $sLocation . '&unit=m" quality="high" wmode="transparent" bgcolor="#CC00CC" width="184" height="76" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />';
        return($sReturn);
    }
}

?>