<?php

class clsjQuery
{
    public $aDataItems;

    function __construct($aDataItems = "")
    {
        $this->aDataItems = $aDataItems;
    }

    function Calendar($sFieldName)
    {
        $sReturn = '
    	<script>
	    jQuery(function() {
		    jQuery("#' . $sFieldName . '" ).datepicker({
    			showOn: "button",
	    		buttonImage: "../images/icons/iconCalendar2.png",
		    	buttonImageOnly: true,
                dateFormat: "yy-mm-dd"
		    });
	    });
	    </script>';

        return($sReturn);
    }


    function AutoCompleteLocal($sArrayName, $sFieldName, $sCSSStyle, $bFirst = true, $sDefaultViewValue = "", $sDefaultValue = "")
    {
        $sFieldName2 = $sFieldName . '_value';

        for ($i=0; $i < count($this->aDataItems); $i++)
        {
            if ($i > 0) $sItems .= ',';
            $sItems .= '{ value: "' . $this->aDataItems[$i][0] . '", display: "' . $this->aDataItems[$i][2] . '", label: "' . $this->aDataItems[$i][1] . '" }';
        }

        if ($bFirst)
        {
            $sReturn = '<script language="JavaScript" type="text/javascript">
            var ' . $sArrayName . ' = [ ' . $sItems . ' ];
            </script>';
        }

        $sReturn .= '
	    <script language="JavaScript" type="text/javascript">
	    jQuery(function()
        {
		    jQuery("#' . $sFieldName2 . '").autocomplete({
			    source: ' . $sArrayName . ',
                select: function( event, ui )
                {
				    jQuery("#' . $sFieldName2 . '").val(ui.item.display); // label
    				jQuery("#' . $sFieldName . '").val(ui.item.value);
	    			return false;
		    	},
			    focus: function( event, ui )
                {
				    jQuery( "#' . $sFieldName2 . '" ).val(ui.item.display);
				    return false;
			    }
            });
        });
	    </script>
        <input value="' . $sDefaultViewValue . '" id="' . $sFieldName2 . '" style="' . $sCSSStyle . '" /><input type="hidden" value="' . $sDefaultValue . '" name="' . $sFieldName . '" id="' . $sFieldName . '" />';
        // onchange="document.getElementById(\'' . $sFieldName . '\').value=\'\';"

        return($sReturn);
    }

    function AutoCompleteLocalMultiple($sArrayName, $sFieldName, $sCSSStyle, $bFirst = true, $sDefaultViewValue = "", $sDefaultValue = "")
    {
        $sFieldName2 = $sFieldName . '_value';

        $sReturn = '<script language="JavaScript" type="text/javascript">';

        if ($bFirst)
        {
            for ($i=0; $i < count($this->aDataItems); $i++)
            {
                if ($i > 0) $sItems .= ',';
                $sItems .= '{ value: "' . $this->aDataItems[$i][0] . '", display: "' . $this->aDataItems[$i][2] . '", label: "' . $this->aDataItems[$i][1] . '" }';
            }

            $sReturn .= 'var ' . $sArrayName . ' = [ ' . $sItems . ' ];';
        }

        $sReturn .= 'jQueryAutoComplete("#' . $sFieldName . '", ' . $sArrayName . ');
        </script>';

        $sReturn .= '<input value="' . $sDefaultViewValue . '" id="' . $sFieldName2 . '" style="' . $sCSSStyle . '" /><input type="hidden" value="' . $sDefaultValue . '" name="' . $sFieldName . '" id="' . $sFieldName . '" />';
        return($sReturn);
    }
}

?>