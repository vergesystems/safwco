<?php
global $objEmployee;
global $objGeneral;
global $objDatabase;
$iEmployeeId = $objEmployee->iEmployeeId;

$sReference = $objGeneral->fnGet("txtReference");
$sReference = $objDatabase->RealEscapeString($sReference);
$iGeneralJournalId = $objGeneral->fnGet("id");
$iGeneralJournalId = $objDatabase->RealEscapeString($iGeneralJournalId);

$varResult = $objDatabase->Query("
SELECT * FROM organization_employees AS E
INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalAddedBy = E.EmployeeId
WHERE GJ.GeneralJournalId='$iGeneralJournalId'");
if ($objDatabase->RowsNumber($varResult) <= 0)
    return(false);

$sFirstName = $objDatabase->Result($varResult, 0, "E.FirstName");
$sLastName = $objDatabase->Result($varResult, 0, "E.LastName");

$sReturn .= '
<table cellspacing="0" cellpadding="5" width="100%" align="center">
 <tr>
  <td align="center" width="20%" style="border-top: solid 1px #cecece;font-weight:bold; font-size:15px;">Prepared By <br />' . $sFirstName . ' '.  $sLastName . '</td>
  <td>&nbsp;</td>
  <td align="center" width="20%" style="border-top: solid 1px #cecece;font-weight:bold; font-size:15px;">Checked By <br />Manager (Finance & Accounts)</td>
  <td>&nbsp;</td>  
  <td align="center" width="20%" style="border-top: solid 1px #cecece; font-weight:bold; font-size:15px;">Reviewed By <br /> Chief Financial Officer</td>
  <td>&nbsp;</td>
  <td align="center" width="20%" style="border-top: solid 1px #cecece;font-weight:bold; font-size:15px;">Approved By <br />Executive Director</td>
  <td>&nbsp;</td>';
  if($sVoucherType != 'JV')
	$sReturn .='<td align="center" style="border-top: solid 1px #cecece; font-weight:bold; font-size:15px;">Received By</td><td>&nbsp;</td>';

$sReturn .='<td><br /><br />&nbsp;</td></tr></table>';

/*
$sReturn .= '
<table cellspacing="0" cellpadding="5" width="100%" align="center">
 <tr>
 <td align="left" style="border-bottom: solid 1px #0000000;font-weight:bold; font-size:15px;">Deliverables</td>
 </tr>
 <tr>
  <td style="font-size:15px;">1) Bills/Invoice/Receipts</td><td><img src="../../vf/images/icons/iconChecklistBlank.png" /><td>
  <td style="font-size:15px;">2) Quotations/Comparative Statement</td><td><img src="../../vf/images/icons/iconChecklistBlank.png" /><td>
 </tr>
 <tr>
  <td style="font-size:15px;">3) Training Sessions</td><td><img src="../../vf/images/icons/iconChecklistBlank.png" /><td>
  <td style="font-size:15px;">4) Check Photo copies</td><td><img src="../../vf/images/icons/iconChecklistBlank.png" /><td>
 </tr>
 <tr>
  <td style="font-size:15px;">5) Advance request</td><td><img src="../../vf/images/icons/iconChecklistBlank.png" /><td>
  <td style="font-size:15px;">6) Expenditures summary</td><td><img src="../../vf/images/icons/iconChecklistBlank.png" /><td>
 </tr>
 <tr>
  <td style="font-size:15px;">7) Agreements Vehicle/Office rent </td><td><img src="../../vf/images/icons/iconChecklistBlank.png" /><td>
  <td style="font-size:15px;">8) CVs Consultants/R-P</td><td><img src="../../vf/images/icons/iconChecklistBlank.png" /><td>
 </tr>
</table>';
*/

?>