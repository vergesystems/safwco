<?php

// Sysetm Information
define("cSystemName", "Verge Financials");
define("cSystemKeywords", "verge, fms, FMS");
define("cSystemDescription", "Verge Financials");

define("cOrganizationVoucherSeries", "DonorProjectWise");

define("cProjectUniqueName", "vergefinancials_vergesystems");
//define("cServerURL", "http://localhost/VergeFinancials");
define("cServerURL", "http://www.vergehost.com/~vfsafwco");
define("cCompanyName", "Verge Systems");

define("cGoogleMapsAPIKey", "ABQIAAAABmcA2ZhJpDP1jAwMfTqyNhRuZkRQN98AYwl9cXI9olgNcRPxkRQg68RuXb4HAJHaijTNXiyVMSPlRA");

define("cDemoMode", "on");
define("cEncryptionKey", "VHRMSEIR8E9DIEO2");
define("cFromEmailName", "Administrator");
define("cFromEmail", "info@vergesystems.com");
define("cSupportEmail", "info@vergesystems.com");

// Folders
define("cVSFFolder", "../../system/VSF-PHP");
define("cSystemFolder", "../../system");
define("cBackupFolder", "../../system/data/databackup");
define("cDataFolder", "../../system/data");
define("cSystemLogFolder", "../../system/data/systemlog");

define("cUserFiles", 'D:\VergeSystems\Web Projects\VergeSystems\VergeHRMS\system\data\userfiles');

// Constants
define("cPagingLimit", 10);
define("cAllowEditDeleteTransactions", "yes");
define("cVoucherEntries", 30);
define("cOrganizationFiscalYear", "07-01");			// July 1st

// Error Handler
define("cErrorHandlerSenderEmail", "apathan@vergesystems.com");
define("cErrorHandlerSenderName", "apathan@vergesystems.com");
define("cErrorHandlerSubject", "Errors in VF Safwco");
define("cErrorHandlerDatabaseErrorLogFile", "../../system/logs/MySQLDatabaseErrors.log");
define("cErrorHandlerEmailErrorLogFile", "../../system/logs/EmailErrors.log");

// Debug Mode
define("cDebugMode", true);


//Local
/*define("cDatabaseType", "mysql");
define("cDatabaseHost", "localhost");
define("cDatabaseUser", "root");
define("cDatabasePassword", "root");
define("cDatabaseDatabase", "vfsafwco_fis");*/


//Live
define("cDatabaseType", "mysqli");
define("cDatabaseHost", "localhost");
define("cDatabaseUser", "root");
define("cDatabasePassword", "");
define("cDatabaseDatabase", "vfsafwco_fis");





// Email Settings
define("cEmailSMTPHost", "localhost");
define("cEmailSMTPPort", "21");


// Upload Files
define("cUploadFileMaxLimit", 1048576);

// Miscenllenous
define("cMiscRestrictedKeywords", '"\'!@#$%^&*()_-=+]}[{\\;:|/?.>,<`~');
define("cMiscCookieExpireTime", (time()+60*60*24*30*30));
define("TTF_DIR", cVSFFolder . "/fonts/");
?>