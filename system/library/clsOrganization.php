<?php
include('../../system/library/clsOrganization_Regions.php');
include('../../system/library/clsOrganization_Stations.php');
include('../../system/library/clsOrganization_Departments.php');

class clsOrganization
{
    // Constructor
    function __construct()
    {
    }

    function ShowOrganizationMenu($sPage)
    {
    	global $objEmployee;		
    	
    	$sRegionalHierarchy = '<a ' . (($sPage == "RegionalHierarchy") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../organization/?page=RegionalHierarchy"><img src="../images/organization/iconRegionalHierarchy.gif" alt="Regional Hierarchy" title="Regional Hierarchy" border="0" /><br />Regional Hierarchy</a>';
    	$sStations = '<a ' . (($sPage == "Stations") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../organization/?page=Stations"><img src="../images/organization/iconStations.gif" alt="Stations" title="Stations" border="0" /><br />Stations</a>';
    	$sEmployees = '<a ' . (($sPage == "Employees") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../organization/?page=Employees"><img src="../images/organization/iconEmployees.gif" alt="Employees" title="Employees" border="0" /><br />Employees</a>';
    	$sDataBackup = '<a ' . (($sPage == "DataBackup") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../organization/?page=DataBackup"><img src="../images/organization/iconDataBackup.gif" alt="Data Backup" title="Data Backup" border="0" /><br />Data Backup</a>';
    	$sSystemSettings = '<a ' . (($sPage == "Settings") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../organization/?page=Settings"><img src="../images/organization/iconSettings.gif" alt="System Settings" title="System Settings" border="0" /><br />System Settings</a>';
    	$sSystemLog = '<a ' . (($sPage == "SystemLog") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../organization/?page=SystemLog"><img src="../images/organization/iconSystemLog.gif" alt="System Log" title="System Log" border="0" /><br />System Log</a>';

    	if (($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_DataBackup[0]) == 0)
    		$sDataBackup = '<img src="../images/organization/iconDataBackup_disabled.gif" alt="Data Backup" title="Data Backup" border="0" /><br />Data Backup';
		
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[0] == 0)
    		$sRegionalHierarchy = '<img src="../images/organization/iconRegionalHierarchy_disabled.gif" alt="Regional Hierarchy" title="Regional Hierarchy" border="0" /><br />Regional Hierarchy';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[0] == 0)
    		$sStations = '<img src="../images/organization/iconStations_disabled.gif" alt="Stations" title="Stations" border="0" /><br />Stations';

    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[0] == 0)
    		$sEmployees = '<img src="../images/organization/iconEmployees_disabled.gif" alt="Employees" title="Employees" border="0" /><br />Employees';

    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_SystemSettings[0] == 0)
    		$sSystemSettings = '<img src="../images/organization/iconSettings_disabled.gif" alt="System Settings" title="System Settings" border="0" /><br />System Settings';

    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_SystemLog[0] == 0)
    		$sSystemLog = '<img src="../images/organization/iconSystemLog_disabled.gif" alt="System Log" title="System Log" border="0" /><br />System Log';

    	$sReturn .= '
    	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
    	 <tr>
    	  <td valign="bottom">

         <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
    	  <tr>
    	   <!--<td align="center" width="10%">' . $sRegionalHierarchy . '</td>-->
     	   <td align="center" width="10%">' . $sStations . '</td>
    	   <td align="center" width="10%">' . $sEmployees . '</td>
    	   <td align="center" width="10%">' . $sDataBackup . '</td>
    	   <td align="center" width="10%">' . $sSystemSettings . '</td>
    	   <td align="center" width="10%">' . $sSystemLog . '</td>
    	  </tr>
    	 </table>
    	</td></tr></table>';

    	return($sReturn);
    }
	
	function ShowOrganizationPages($sPage)
	{
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		if ($sPage == "") $sPage = "Stations";
		
		switch($sPage)
		{
			case "RegionalHierarchy": 
				$aTabs[0][0] = 'Regions';
				$aTabs[0][1] = '../organization/regionalhierarchy.php';
				$aTabs[1][0] = 'Region Types';
				$aTabs[1][1] = '../organization/regionalhierarchy.php?pagetype=regiontypes';
				break;				
			case "Stations": 
				$aTabs[0][0] = 'Stations';
				$aTabs[0][1] = '../organization/stations.php';				
				break;
			case "Employees": 
				$aTabs[0][0] = 'Employees';
				$aTabs[0][1] = '../organization/employees.php';
				$aTabs[1][0] = 'Employee Types';
				$aTabs[1][1] = '../organization/employees.php?pagetype=employeetypes';
				$aTabs[2][0] = 'Departments';
				$aTabs[2][1] = '../organization/employees.php?pagetype=departments';
				break;		
			case "DataBackup":
				$aTabs[0][0] = 'Data Backup';
				$aTabs[0][1] = '../organization/databackup.php';
				break;
			case "Settings":
				$aTabs[0][0] = 'System Settings';
				$aTabs[0][1] = '../organization/settings.php';
				break;
			case "SystemLog":
				$aTabs[0][0] = 'System Log';
				$aTabs[0][1] = '../organization/systemlog.php';
				break;
		}
		
		$sReturn = $objDHTMLSuite->TabBar($aTabs, $this->ShowOrganizationMenu($sPage));
		return($sReturn);
	}
	
    function SaveSettings()
    {
    	global $objDatabase;
    	global $objEmployee;
    	global $objGeneral;
		global $objSystemLog;

    	// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_SystemSettings[0] == 0) // View Disabled
			die('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

    	$iNumber = $objGeneral->fnGet("num");

    	for ($i=0; $i < $iNumber; $i++)
    	{
    		$sSettingName = $objGeneral->fnGet("hdnSettingName" . $i);
    		$sSettingValue = $objGeneral->fnGet("txtSettingValue" . $i);
			$sSettingValue = $objDatabase->RealEscapeString($sSettingValue);
    		$varResult = $objDatabase->Query("UPDATE organization_settings SET SettingValue='$sSettingValue' WHERE SettingName='$sSettingName'");
    	}
	  	
		$objSystemLog->AddLog("Save System Setting");


    	return(1104);
    }

    function ShowAllSystemSettings()
    {
    	global $objDatabase;
    	global $objEmployee;

    	// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_SystemSettings[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

    	$sReturn = '
    	<form method="post" action="">
    	<table bordercolor="#E6E6E6" border="1" style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td width="30%" align="left"><span class="WhiteHeading">Setting Name</span></td>
		  <td align="left"><span class="WhiteHeading">Setting Value</span></td>
		 </tr>';


    	$varResult = $objDatabase->Query("SELECT * FROM organization_settings AS S ORDER BY S.SettingName");
    	for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{
    		$sSettingName = $objDatabase->Result($varResult, $i, "S.SettingName");
    		$sSettingValue = $objDatabase->Result($varResult, $i, "S.SettingValue");

    		$sReturn .= '<tr>
    		 <td>' . $sSettingName . '<input type="hidden" name="hdnSettingName' . $i . '" id="hdnSettingName' . $i . '" value="' . $sSettingName . '" /></td>
    		 <td><input class="form1" size="50" type="text" name="txtSettingValue' . $i . '" id="txtSettingValue' . $i . '" value="' . $sSettingValue . '" /></td>
    		</tr>';
    	}

    	$sReturn .= '
    	 <tr><td colspan="2" align="center"><br /><input type="submit" value="Save Settings" class="AdminFormButton1" /></td></tr>
    	</table><input type="hidden" name="action" id="action" value="SaveSettings" /><input type="hidden" name="num" id="num" value="' . $i . '" /></form>';

    	return($sReturn);
    }
}

?>