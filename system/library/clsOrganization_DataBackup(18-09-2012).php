<?php

class clsDataBackup
{
    // Constructor
    function __construct()
    {

    }

    function ShowAllBackups()
    {
		global $objGeneral;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_DataBackup[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sBackupFolder = cBackupFolder;

        $sButtons_Refresh = '<a href="#noanchor" onclick="window.location=\'../organization/databackup_showall.php\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';

		$sButtons = $sButtons_Refresh;

		$sReturn = '
		<table border=0 cellspacing=0 cellpadding=3 width="100%" align=center>
		 <tr>
		  <td colspan=2 align=left><span class="heading">Data Backup Files</span></td>
		  <td colspan=2 align=right>' . $sButtons . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
         <td width="40%" align="left"><span class="WhiteHeading">Backup File Name</span></td>
		 <td width="20%" align="left"><span class="WhiteHeading">Created On</span></td>
		 <td width="20%" align="left"><span class="WhiteHeading">File Size</span></td>
		 <td width="2%" colspan="2"><span class="WhiteHeading">Operations</span></td>
		</tr>';

        $aTemp = $objGeneral->getFiles($sBackupFolder);

		for ($i=0; $i < count($aTemp); $i++)
		{
			$aBackupFiles[$i][0] = $aTemp[$i];
			$aBackupFiles[$i][1] = filemtime($aTemp[$i]);
			$aBackupFiles[$i][2] = filesize($aTemp[$i]);
		}

		$aBackupFiles = $objGeneral->SortArray($aBackupFiles, 1, "desc");

		$iNumberOfBackups = 0;
        for ($i=0; $i < count($aBackupFiles); $i++)
        {
			$aFile = explode('/', $aBackupFiles[$i][0]);
            $sFileName = $aFile[count($aFile)-1];

			if (strpos($sFileName, '.rar') === false) continue;
			$sFileName = str_replace('.rar', '', $sFileName);

			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

            $sDeleteDataBackup = '<td class="GridTD" align=center><a href="#noanchor" onClick="if(confirm(\'Do you really want to delete this Backup File?\')) {window.location = \'?action=DeleteBackup&filename=' . urldecode($sFileName) . '\';}"><img src="../images/icons/iconDelete.gif" border=0 alt="Delete this Backup File" title="Delete this Backup File"></a></td>';
            if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_DataBackup[3] == 0)
            	$sDeleteDataBackup = '';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align=left valign=top>' . $sFileName . '<img src="../images/spacer.gif"></td>
			 <td class="GridTD" align=left valign=top>' . date ("F d, Y", $aBackupFiles[$i][1]) . '<img src="../images/spacer.gif"></td>
			 <td class="GridTD" align=left valign=top>' . number_format(($aBackupFiles[$i][2]/(1024*1024)),2) . ' MB<img src="../images/spacer.gif"></td>
			 <td class="GridTD" align=center><a href="../organization/databackup_showall.php?action=DownloadBackup&filename=' . $sFileName . '"><img src="../images/icons/iconBackup.gif" border=0 alt="Download this Backup File" title="Download this Backup File"></a></td>
			 ' . $sDeleteDataBackup . '
			</tr>';

			$iNumberOfBackups++;
		}

		$sReturn .= '</table>
		<table border=0 cellspacing=0 cellpadding=3 width="100%" align=center>
		 <tr>
		  <td align=left width="10%">
           <input onclick="' . (($iNumberOfBackups >= 1) ? 'alert(\'Sorry, another data backup already exists in the system,\nplease delete the previous backup first!\');' : 'if(confirm(\'Do you want to take the current Data Backup?\')) {window.location=\'?action=DataBackup\'}') . '" type="button" class="AdminFormButton1" value="Backup Data">
          </td>
		 </tr>
		 <tr>
		  <td style="color:red;">Please Note: For the system\'s security, the backup file must be deleted immedietly once it has been downloaded!</td>
		 </tr>
		</table>

        <br /><br />
        <table border=0 cellspacing=0 cellpadding=3 width="95%" align=center>
         <tr><td align=center width="10"><img src="../images/icons/iconBackup.gif" title="Download this Backup File" alt="Download this Backup File"></td><td align="left">Download this Backup File</td></tr>
         <tr><td align=center width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Backup File" alt="Delete this Backup File"></td><td align="left">Delete this Backup File</td></tr>
        </table>';

		return($sReturn);
    }

    function DeleteBackup($sFileName)
    {
        global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_DataBackup[3] == 0) // Delete Disabled
			return(1010);

        $sBackupFolder = cBackupFolder;
		$sDatabaseFile = $sBackupFolder . '/' . $sFileName . '.rar';

        if (file_exists($sDatabaseFile))
        {
            unlink($sDatabaseFile);

   	       	include('../../system/library/clsOrganization_SystemLog.php');
			$objSystemLog = new clsSystemLog();
  			$objSystemLog->AddLog("Delete Data Backup - " . $sFileName);

            return(1102);
        }
        else
            return(1103);
    }

	function DownloadBackup($sFileName)
	{
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_DataBackup[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$sDatabaseFile = cBackupFolder . '/' . $sFileName . '.rar';

		// Download file
		header("Location: " . $sDatabaseFile);
		exit(0);
	}

    function DataBackup()
    {
        global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_DataBackup[0] == 0) // View Disabled
			return(1010);

		$sDatabaseFile = cBackupFolder . '/' . date("Y-m-d") . '.sql.rar';

		$sTemp = shell_exec('mysqldump --opt -Q -h ' . cDatabaseHost . ' -u ' . cDatabaseUser . ' --password=' . cDatabasePassword . ' ' . cDatabaseDatabase . ' | gzip > ' . $sDatabaseFile);

		include('../../system/library/clsOrganization_SystemLog.php');
		$objSystemLog = new clsSystemLog();
		$objSystemLog->AddLog("Data Backup");

        return(1100);
    }

}