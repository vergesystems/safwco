<?php

/* Departments */
class clsDepartments extends clsOrganization
{

    // Constructor
    function __construct()
    {

    }

    function ShowAllDepartments($sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Departments[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

		$sTitle = 'Departments';
        if ($sSortBy == "") $sSortBy = "D.DepartmentId DESC";

        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((D.DepartmentName LIKE '%$sSearch%') OR (D.DepartmentCode LIKE '%$sSearch%') OR (D.DepartmentId LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (E.UserName LIKE '%$sSearch%')) ";
			$sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

		$iTotalRecords = $objDatabase->DBCount("organization_departments AS D INNER JOIN organization_employees AS E ON E.EmployeeId = D.EmployeeId_DepartmentHead", "D.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=departments&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM organization_departments AS D
		INNER JOIN organization_employees AS E ON E.EmployeeId = D.EmployeeId_DepartmentHead
        WHERE D.OrganizationId='" . cOrganizationId . "' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn .= '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td align="left"><span class="WhiteHeading">Department Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=D.DepartmentName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Department Name in Ascending Order" title="Sort by Department Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=D.DepartmentName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Department Name in Descending Order" title="Sort by Department Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Department Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=D.DepartmentCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Department Code in Ascending Order" title="Sort by Department Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=D.DepartmentCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Department Code in Descending Order" title="Sort by Department Code in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Department Head&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=E.FirstName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Department Head in Ascending Order" title="Sort by Department Head in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=E.FirstName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Department Head in Descending Order" title="Sort by Department Head in Descending Order" border="0" /></a></span></td>
		 <td width="2%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iDepartmentId = $objDatabase->Result($varResult, $i, "D.DepartmentId");
			$sDepartmentCode = $objDatabase->Result($varResult, $i, "D.DepartmentCode");
            $sDepartmentName = $objDatabase->Result($varResult, $i, "D.DepartmentName");

            $sEmployee_DepartmentHead = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");

            $sEditDepartment = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update '. $objDatabase->RealEscapeString($sDepartmentName) .'\', \'../organization/employees.php?pagetype=departments_details&action2=edit&id=' . $iDepartmentId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Department Details" title="Edit this Department Details"></a></td>';
            $sDeleteDepartment = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Department?\')) {window.location = \'?action=DeleteDepartment&id=' . $iDepartmentId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Department" title="Delete this Department"></a></td>';

    	    // Employee Roles
    	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Departments[2] == 0)	$sEditDepartment = '';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Departments[3] == 0)	$sDeleteDepartment = '';
				
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sDepartmentName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sDepartmentCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sEmployee_DepartmentHead . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" />
			 </td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sDepartmentName) . '\', \'../organization/employees.php?pagetype=departments_details&id=' . $iDepartmentId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Department Details" title="View this Department Details"></a></td>
			 ' . $sEditDepartment . $sDeleteDepartment . '
			</tr>';
		}

		$sAddDepartment = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Department\', \'../organization/employees.php?pagetype=departments_details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Department">';

        // Employee Roles
        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Departments[1] == 0) // Update Disabled
			$sAddNewDepartment = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		  ' . $sAddDepartment . '
		  &nbsp;&nbsp;&nbsp;
          <form method="GET" action="">Search for a Department:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Department" title="Search for Department" border="0"><input type="hidden" name="pagetype" id="pagetype" value="departments" /></form>
          </td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Department Details" alt="View this Department Details"></td><td>View this Department Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Department Details" alt="Edit this Department Details"></td><td>Edit this Department Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Department" alt="Delete this Department"></td><td>Delete this Department</td></tr>
        </table>';

		return($sReturn);
    }

    function DepartmentDetails($iDepartmentId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *
		FROM organization_departments AS D
		INNER JOIN organization_employees AS E ON  E.EmployeeId = D.EmployeeId_DepartmentHead
		INNER JOIN organization_employees AS E2 ON E2.EmployeeId = D.DepartmentAddedBy
		LEFT  JOIN organization_departments AS D2 ON D2.DepartmentId = D.DepartmentParentId
		WHERE D.OrganizationId='" . cOrganizationId . "' AND E.OrganizationId='" . cOrganizationId . "'  AND D.DepartmentId = '$iDepartmentId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=departments_details&action2=edit&id=' . $iDepartmentId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Department" title="Edit this Department" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Department?\')) {window.location = \'?pagetype=departments_details&action=DeleteDepartment&id=' . $iDepartmentId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Department" title="Delete this Department" /></a>';

        // Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Departments[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Departments[3] == 0)	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Department Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iEmployeeId = $objDatabase->Result($varResult, 0, "E.EmployeeId");
		    	$iEmployeeId_DepartmentHead = $objDatabase->Result($varResult, 0, "D.EmployeeId_DepartmentHead");
				$iDepartmentId = $objDatabase->Result($varResult, 0, "D.DepartmentId");
		        $sDepartmentName = $objDatabase->Result($varResult, 0, "D.DepartmentName");
		        $sDescription = $objDatabase->Result($varResult, 0, "D.Description");
		        $sNotes = $objDatabase->Result($varResult, 0, "D.Notes");
		        $sDepartmentCode = $objDatabase->Result($varResult, 0, "D.DepartmentCode");

		        $sDepartmentHead = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
		        $sDepartmentHead = $sDepartmentHead . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sDepartmentHead . '\', \'../organization/employees_details.php?id=' . $iEmployeeId_DepartmentHead . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$iDepartmentParentId = $objDatabase->Result($varResult, 0, "D.DepartmentParentId");
				$sDepartmentParent = $objDatabase->Result($varResult, 0, "D2.DepartmentName");

				if ($sDepartmentParent == '') $sDepartmentParent = "No Parent";
				else $sDepartmentParent = $sDepartmentParent . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sDepartmentParent . '\', \'../organization/employees.php?pagetype=departments_details&id=' . $iDepartmentParentId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Department Information" title="Parent Department Information" border="0" /></a>';

		        $dDepartmentAddedOn = $objDatabase->Result($varResult, 0, "D.DepartmentAddedOn");
				$sDepartmentAddedOn = date("F j, Y", strtotime($dDepartmentAddedOn)) . ' at ' . date("g:i a", strtotime($dDepartmentAddedOn));

				$iDepartmentAddedBy = $objDatabase->Result($varResult, 0, "E2.EmployeeId");
				$sDepartmentAddedBy = $objDatabase->Result($varResult, 0, "E2.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E2.LastName");
				$sDepartmentAddedBy = $sDepartmentAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sDepartmentAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iDepartmentAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
		    }

			if ($sAction2 == "edit")
			{
   		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

				$sDepartmentName = '<input type="text" name="txtDepartmentName" id="txtDepartmentName" class="form1" value="' . $sDepartmentName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDepartmentCode = '<input type="text" name="txtDepartmentCode" id="txtDepartmentCode" class="form1" value="' . $sDepartmentCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';

				$sDepartmentHead = '<select class="form1" name="selDepartmentHead" id="selDepartmentHead">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.OrganizationId='" . cOrganizationId . "' ORDER BY E.FirstName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sDepartmentHead .= '<option ' . (($iEmployeeId_DepartmentHead == $objDatabase->Result($varResult2, $i, "E.EmployeeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "E.EmployeeId") . '">' . $objDatabase->Result($varResult2, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult2, $i, "E.LastName") . '</option>';
				$sDepartmentHead .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDepartmentHead\'), \'../organization/employees_details.php?id=\'+GetSelectedListBox(\'selDepartmentHead\'), \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>&nbsp;<span style="color:red;">*</span>';
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
								
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sDepartmentParent = '<select class="form1" name="selDepartmentParentId" id="selDepartmentParentId">
				<option value="0">No Parent</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_departments AS D WHERE D.OrganizationId='" . cOrganizationId . "' AND D.DepartmentId <> '$iDepartmentId' ORDER BY D.DepartmentId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
		            if ($iDepartmentParentId == $objDatabase->Result($varResult2, $i, "D.DepartmentId"))
						$sDepartmentParent .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "D.DepartmentId") . '">' . $objDatabase->Result($varResult2, $i, "D.DepartmentName") . '</option>';
					else
						$sDepartmentParent .= '<option value="' . $objDatabase->Result($varResult2, $i, "D.DepartmentId") . '">' . $objDatabase->Result($varResult2, $i, "D.DepartmentName") . '</option>';
				}
				$sDepartmentParent .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selDepartmentParentId\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDepartmentParentId\'), \'../organization/employees.php?pagetype=departments_details&id=\'+GetSelectedListBox(\'selDepartmentParentId\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Department Information" title="Parent Department Information" border="0" /></a>';
			}
			else if ($sAction2 == "addnew")
			{
				$iDepartmentId = "";
		        $sDepartmentName = $objGeneral->fnGet("txtDepartmentName");
		        $sDepartmentCode = $objGeneral->fnGet("txtDepartmentCode");

				$sDepartmentAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				$sDepartmentAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sDepartmentAddedBy = $sDepartmentAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sDepartmentAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';				

		        $sDepartmentHead = '<select class="form1" name="selDepartmentHead" id="selDepartmentHead">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.OrganizationId='" . cOrganizationId . "' ORDER BY E.FirstName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sDepartmentHead .= '<option ' . (($iEmployeeId_DepartmentHead == $objDatabase->Result($varResult2, $i, "E.EmployeeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "E.EmployeeId") . '">' . $objDatabase->Result($varResult2, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult2, $i, "E.LastName") . '</option>';
				$sDepartmentHead .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDepartmentHead\'), \'../organization/employees_details.php?id=\'+GetSelectedListBox(\'selDepartmentHead\'), \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>&nbsp;<span style="color:red;">*</span>';

		        $sDescription = $objGeneral->fnGet("txtDescription");
		        $sNotes = $objGeneral->fnGet("txtNotes");
				
   		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sDepartmentName = '<input type="text" name="txtDepartmentName" id="txtDepartmentName" class="form1" value="' . $sDepartmentName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDepartmentCode = '<input type="text" name="txtDepartmentCode" id="txtDepartmentCode" class="form1" value="' . $sDepartmentCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

				$sDepartmentParent = '<select class="form1" name="selDepartmentParentId" id="selDepartmentParentId">
				<option value="0">No Parent</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_departments AS D WHERE D.OrganizationId='" . cOrganizationId . "' AND D.DepartmentId <> '$iDepartmentId' ORDER BY D.DepartmentId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
		            if ($iDepartmentParentId == $objDatabase->Result($varResult2, $i, "D.DepartmentId"))
						$sDepartmentParent .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "D.DepartmentId") . '">' . $objDatabase->Result($varResult2, $i, "D.DepartmentName") . '</option>';
					else
						$sDepartmentParent .= '<option value="' . $objDatabase->Result($varResult2, $i, "D.DepartmentId") . '">' . $objDatabase->Result($varResult2, $i, "D.DepartmentName") . '</option>';
				}
				$sDepartmentParent .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selDepartmentParentId\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDepartmentParentId\'), \'../organization/employees.php?pagetype=departments_details&id=\'+GetSelectedListBox(\'selDepartmentParentId\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Department Information" title="Parent Department Information" border="0" /></a>';
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
	        function ValidateForm()
	        {
	        	if (GetVal(\'txtDepartmentName\') == "") return(AlertFocus(\'Please enter a valid Department Name\', \'txtDepartmentName\'));
	        	if (GetVal(\'txtDepartmentCode\') == "") return(AlertFocus(\'Please enter a valid Department Code\', \'txtDepartmentCode\'));

             	return true;
	        }
	        </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="GridTR"><td colspan="2"><span class="title2">Department Information:</span></td></tr>
			 <!--<tr bgcolor="#edeff1"><td width="20%">Department Id:</td><td><strong>' . $iDepartmentId . ' (Automatically Assigned)</strong></td></tr>-->
			 <tr><td valign="top" style="width:200px;">Parent Department:</td><td ><strong>' . $sDepartmentParent . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Department Name:</td><td><strong>' . $sDepartmentName . '</strong></td></tr>
			 <tr><td>Department Code:</td><td><strong>' . $sDepartmentCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Department Head:</td><td><strong>' . $sDepartmentHead . '</strong></td></tr>
		     <tr><td colspan="2" valign="top">Description:<br /><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="GridTR"><td colspan="2"><span class="title2">Extra Information:</span></td></tr>
		     <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Department Added On:</td><td><strong>' . $sDepartmentAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Department Added By:</td><td><strong>' . $sDepartmentAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Department" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iDepartmentId . '"><input type="hidden" name="action" id="action" value="UpdateDepartment"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Department" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewDepartment"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }
	
	function AddNewDepartment($iDepartmentParentId, $sDepartmentName, $sDepartmentCode, $iEmployeeId_DepartmentHead, $sDescription, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
		global $objEmployee;
		global $objSystemLog;

        $varNow = $objGeneral->fnNow();
        $iEmployeeId = $objEmployee->iEmployeeId;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Departments[1] == 0) // Add Disabled
			return(1010);

        $sDepartmentName = $objDatabase->RealEscapeString($sDepartmentName);
        $sDepartmentCode = $objDatabase->RealEscapeString($sDepartmentCode);
        $sDescription = $objDatabase->RealEscapeString($sDescription);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if ($objDatabase->DBCount("organization_departments AS D", "D.OrganizationId='" . cOrganizationId . "' AND D.DepartmentCode='$sDepartmentCode'"))
			return(3300);
		
        $varResult = $objDatabase->Query("INSERT INTO organization_departments
        (OrganizationId, DepartmentParentId, DepartmentName, DepartmentCode, EmployeeId_DepartmentHead, Description, Notes, DepartmentAddedOn, DepartmentAddedBy)
        VALUES ('" . cOrganizationId . "', '$iDepartmentParentId', '$sDepartmentName', '$sDepartmentCode', '$iEmployeeId_DepartmentHead', '$sDescription', '$sNotes', '$varNow', '$objEmployee->iEmployeeId')");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {
	        $varResult = $objDatabase->Query("SELECT * FROM organization_departments AS D WHERE D.OrganizationId='" . cOrganizationId . "' AND D.DepartmentName='$sDepartmentName' AND D.DepartmentAddedOn='$varNow'");
    	    if ($objDatabase->RowsNumber($varResult) > 0)
        	{	       		
  				$objSystemLog->AddLog("Add New Department - " . $sDepartmentName);

  				$objGeneral->fnRedirect('?pagetype=departments_details&error=3301&id=' . $objDatabase->Result($varResult, 0, "D.DepartmentId"));
    	    }
        }

        return(3302);
    }
	
    function UpdateDepartment($iDepartmentId, $iDepartmentParentId, $sDepartmentName, $sDepartmentCode, $iEmployeeId_DepartmentHead, $sDescription, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Departments[2] == 0) // Update Disabled
			return(1010);

        $sDepartmentName = $objDatabase->RealEscapeString($sDepartmentName);
        $sDepartmentCode = $objDatabase->RealEscapeString($sDepartmentCode);
        $sDescription = $objDatabase->RealEscapeString($sDescription);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if ($objDatabase->DBCount("organization_departments AS D", "D.OrganizationId='" . cOrganizationId . "' AND D.DepartmentId <> '$iDepartmentId' AND D.DepartmentCode='$sDepartmentCode'"))
			return(3300);
				
        $varResult = $objDatabase->Query("
        UPDATE organization_departments SET
			DepartmentParentId='$iDepartmentParentId',
        	DepartmentName='$sDepartmentName',
            DepartmentCode='$sDepartmentCode',
            EmployeeId_DepartmentHead='$iEmployeeId_DepartmentHead',
            Description='$sDescription',
            Notes='$sNotes'
        WHERE OrganizationId='" . cOrganizationId . "' AND DepartmentId='$iDepartmentId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {	       	
  			$objSystemLog->AddLog("Update Department - " . $sDepartmentName);
  		    $objGeneral->fnRedirect('../organization/employees.php?pagetype=departments_details&error=3303&id=' . $iDepartmentId);
        }
        else
            return(3304);
    }

    function DeleteDepartment($iDepartmentId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
        // Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Departments[3] == 0) // Delete Disabled
		{
			if ($objGeneral->fnGet("pagetype") == "departments_details")
				$objGeneral->fnRedirect('?pagetype=departments_details&id=' . $iDepartmentId . '&error=1010');
			else
				$objGeneral->fnRedirect('?pagetype=departments&error=1010');
		}			
		
		if ($objDatabase->DBCount("organization_departments AS D", "D.OrganizationId='" . cOrganizationId . "' AND  D.DepartmentId='$iDepartmentId'") < 0) return(3306);
		if ($objDatabase->DBCount("organization_employees AS E", "E.OrganizationId='" . cOrganizationId . "' AND E.DepartmentId='$iDepartmentId'") > 0) return(3307);
			
		$varResult = $objDatabase->Query("SELECT * FROM organization_departments AS D WHERE D.OrganizationId='" . cOrganizationId . "' AND D.DepartmentId='$iDepartmentId'");
		
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Department Id');
			$sDepartmentName = $objDatabase->Result($varResult, 0, "D.DepartmentName");

        $varResult = $objDatabase->Query("DELETE FROM organization_departments WHERE DepartmentId='$iDepartmentId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {	       	
  			$objSystemLog->AddLog("Delete Department -  " . $sDepartmentName);
			if ($objGeneral->fnGet("pagetype") == "departments_details")
				$objGeneral->fnRedirect('?pagetype=departments_details&id=' . $iDepartmentId . '&error=3305');
			else
				$objGeneral->fnRedirect('?pagetype=departments&error=3305');			
        }
        else
            return(3306);
    }
    
	function DepartmentsList()
	{
		global $objDatabase;

		$aDepartments = array();
		$varResult = $objDatabase->Query("SELECT * FROM organization_departments AS D WHERE D.OrganizationId='" . cOrganizationId . "'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sDepartmentCode = $objDatabase->Result($varResult, $i, "D.DepartmentCode");
			$sDepartmentName = $objDatabase->Result($varResult, $i, "D.DepartmentName");
			$iDepartmentId = $objDatabase->Result($varResult, $i, "D.DepartmentId");
						
			$aDepartments[$i][0] = $iDepartmentId;
			$aDepartments[$i][1] = $sDepartmentCode. ' - ' . $sDepartmentName;
			$aDepartments[$i][2] = $sDepartmentCode;
		}
		
		return($aDepartments);
	} 
}

?>