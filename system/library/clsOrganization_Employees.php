<?php

class clsEmployees
{
    public $iEmployeeId;
	public $sEmployeeUserName;
	
	public $iEmployeeTypeId;
	
	public $iEmployeeStationId;
	
	public $sTimeZone;

	public $objEmployeeRoles;

	public $sEmployeeFirstName;
	public $sEmployeeLastName;

	public $aSystemSettings;
	
	
	public $aStatus;
	public $aStatusType;
	public $aGender;
	public $aSalutation;
	
    // Constructor
    function __construct()
    {
		$this->aGender = array("Male", "Female");
		$this->aStatus = array("Inactive", "Active");
		$this->aStatusType = array(0, 0,);
		$this->aSalutation = array("Mr.", "Miss", "Mrs.", "Ms.", "Dr.");
    }

    function EmployeeLogin($sEmployeeUserName, $sEmployeePassword, $sRemember, $sRedirect = "")
	{
		// Global Class Objects
		global $objGeneral;
		global $objSystemLog;
		global $objDatabase;
		global $objEncryption;
		
		/*$sEmployeeUserName = $objDatabase->RealEscapeString($sEmployeeUserName);
		$sEmployeePassword = $objDatabase->RealEscapeString($sEmployeePassword);*/

		$sEmployeeUserName = $objDatabase->RealEscapeString($sEmployeeUserName);
		$sEmployeePassword = $objDatabase->RealEscapeString($sEmployeePassword);


		if ($sRemember != 'on')
			$tCookieExpireTime = (time()+60*60*1);		// Cookie time is 1 hour
		else
			$tCookieExpireTime = cMiscCookieExpireTime;

		$sTmpPassword = $objGeneral->EncryptPassword($sEmployeePassword, $objEncryption);
		$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E 
		INNER JOIN organizations AS O ON O.OrganizationId = E.OrganizationId
		WHERE O.OrganizationCode='" . cOrganizationCode . "' AND E.UserName='$sEmployeeUserName' AND E.Password='$sTmpPassword'");

		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(2000);
		else
		{
			if ($objDatabase->Result($varResult, 0, "Status") == 0)
				return(2001);

			$iEmployeeId = $objDatabase->Result($varResult, 0, "E.EmployeeId");

			$_SESSION[cOrganizationCode . "_EmployeeId"] = $iEmployeeId;
			setcookie(cOrganizationCode . "_ckEmployeeId", $iEmployeeId, $tCookieExpireTime, "/");
			$varNow = $objGeneral->fnNow();

			$varResult = $objDatabase->Query("UPDATE organization_employees SET EmployeeLastLoginDateTime=EmployeeCurrentLoginDateTime, EmployeeCurrentLoginDateTime='$varNow' WHERE OrganizationId='" . cOrganizationId . "' AND EmployeeId='$iEmployeeId'");
			$objSystemLog->AddLog("User Login");
			$objGeneral->fnRedirect("../home/");
		}

		return(2000);
	}

    function GetEmployeeInfo()
    {
        global $objDatabase;
        global $objEncryption;

        $iEmployeeId = $this->iEmployeeId;

        $varResult = $objDatabase->Query("
        SELECT * FROM organization_employees AS E
        WHERE E.EmployeeId='$iEmployeeId'");
        if ($objDatabase->RowsNumber($varResult) <= 0)
            return(false);

        $sEmployeeRoles = $objDatabase->Result($varResult, 0, "E.EmployeeRoles");

        $this->iEmployeeId = $objDatabase->Result($varResult, 0, "E.EmployeeId");
        $this->iEmployeeTypeId = $objDatabase->Result($varResult, 0, "E.EmployeeTypeId");
        $this->sEmployeeFirstName = $objDatabase->Result($varResult, 0, "E.FirstName");
        $this->sEmployeeLastName = $objDatabase->Result($varResult, 0, "E.LastName");
        $this->sEmployeeUserName = $objDatabase->Result($varResult, 0, "E.UserName");
        $this->iEmployeeStationId = $objDatabase->Result($varResult, 0, "E.StationId");

        $varResult2 = $objDatabase->Query("SELECT * FROM organization_settings AS S");
        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
			$this->aSystemSettings[$objDatabase->Result($varResult2, $i, "S.SettingName")] = $objDatabase->Result($varResult2, $i, "S.SettingValue");

        $objEmployeeRoles = new clsEmployeeRoles();

        //$objEmployeeRoles = unserialize($objEncryption->fnDecrypt(cEncryptionKey, $sEmployeeRoles));
        $objEmployeeRoles = unserialize($sEmployeeRoles);

		$this->objEmployeeRoles = $objEmployeeRoles;

        return(true);
    }

    function EmployeeLogout()
    {
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;

        
  		$objSystemLog->AddLog("User Logout");

		$_SESSION[cOrganizationCode . "_EmployeeId"] = "";
		setcookie(cOrganizationCode . "_ckEmployeeId", "", cMiscCookieExpireTime, "/");

		$this->iEmployeeId = 0;
		$objEmployee = "";
		$objEmployee = NULL;

		$objGeneral->fnRedirect("../login/");
    }

    function EmployeeVerify()
	{
		global $objDatabase;
		global $objDatabaseMaster;
		global $objGeneral;
		global $objEncryption;
		global $objEmployee;

		if (($_SESSION[cOrganizationCode . "_EmployeeId"] == "") && ($_COOKIE[cOrganizationCode . "_ckEmployeeId"] == ""))
			return(false);
		else
		{
			if (($_COOKIE[cOrganizationCode . "_ckEmployeeId"] != '') && ($_COOKIE[cOrganizationCode . "_ckEmployeeId"] != 0))
				$_SESSION[cOrganizationCode . "_EmployeeId"] = $_COOKIE[cOrganizationCode . "_ckEmployeeId"];

			$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E 
			INNER JOIN organization_stations AS S ON S.StationId = E.StationId
			INNER JOIN organizations AS O ON O.OrganizationId = E.OrganizationId
			WHERE O.OrganizationCode='" . cOrganizationCode . "' AND E.EmployeeId='" . $_SESSION[cOrganizationCode . "_EmployeeId"] . "'");
			if ($objDatabase->RowsNumber($varResult) <= 0)
				return(false);

			$this->iEmployeeId = $_SESSION[cOrganizationCode . "_EmployeeId"];
			$this->iEmployeeTypeId = $objDatabase->Result($varResult, 0, "E.EmployeeTypeId");
			$this->sEmployeeFirstName = $objDatabase->Result($varResult, 0, "E.FirstName");
			$this->sEmployeeLastName = $objDatabase->Result($varResult, 0, "E.LastName");
			$this->sEmployeeUserName = $objDatabase->Result($varResult, 0, "E.UserName");
			$this->iEmployeeStationId = $objDatabase->Result($varResult, 0, "E.StationId");
			
			$this->sTimeZone = $objDatabase->Result($varResult, 0, "S.StationTimeZone");
			$this->sLanguage = $objDatabase->Result($varResult, 0, "O.Language");

	   		//$this->aSystemSettings['OrganizationName'] = cOrganizationName;
			//$this->aSystemSettings['CurrencySign'] = $objDatabase->Result($varResult, 0, "O.BaseCurrencySign");			
			//$this->aSystemSettings['HR_Recruitment_JobCandidates_CandidateInformationCounter'] = 5;
			
			//Code by A.Majeed...
			$varResultX = $objDatabase->Query("SELECT * FROM organization_settings AS SS");
			for ($x=0; $x < $objDatabase->RowsNumber($varResultX); $x++)
				$this->aSystemSettings[$objDatabase->Result($varResultX, $x, "SS.SettingName")] = $objDatabase->Result($varResultX, $x, "SS.SettingValue");
			
			
			
			$sEmployeeRoles = $objDatabase->Result($varResult, 0, "E.EmployeeRoles");

			$objEmployeeRoles = new clsEmployeeRoles();
			$objEmployeeRoles = unserialize($sEmployeeRoles);
			
			date_default_timezone_set($this->sTimeZone);

			$this->objEmployeeRoles = $objEmployeeRoles;
			/*
			$varResult = $objDatabaseMaster->Query("SELECT * FROM ui_elements AS E");
			for ($i=0; $i < $objDatabaseMaster->RowsNumber($varResult); $i++)
				$this->aLabels[$objDatabaseMaster->Result($varResult, $i, "E.LabelName")] = $objDatabaseMaster->Result($varResult, $i, "E." . $this->sLanguage);
			*/	
		}

		return(true);
	}

    function ShowEmployeeLogin()
    {
    	// <input type="checkbox" id="rememberme" name="rememberme" checked="false" /> Remember Me			 </td>

    	$sReturn .= '
    	<br /><br /><br /><br />
		<script type="text/javascript">
		function setLoginTextBoxes()
		{
			document.getElementById("username").value="Enter user name...";
			document.getElementById("mockpass").value="Enter password...";		
		}
		function SetOnBlurUserBox()
		{	
			if(document.getElementById("username").value=="")
				document.getElementById("username").value="Enter user name...";
		}
		function SetOnFocusUserBox()
		{		
			if(document.getElementById("username").value=="Enter user name...")		
				document.getElementById("username").value="";
		}		
		function ShowHideMockPassword()
		{
			document.getElementById("mockpass").style.display="none"; 
			document.getElementById("realpass").style.display=""; 
			document.getElementById("realpass").focus();
		}
		function ShowHidePassword()
		{
			if(document.getElementById("realpass").value=="") 
			{
				document.getElementById("mockpass").style.display=""; 
				document.getElementById("realpass").style.display="none";
			}
		}
		</script>		
		<table style="width:400px; height:360px; background:url(../../system/config/loginbox.jpg) no-repeat;" border="0" cellspacing="0" cellpadding="0" align="center">
		 <tr>
		  <td align="center">
		   <br /><br /><br />
		   <table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		    <form action="index.php" method="post">
		    <tr>
			 <td align="right" style="width:130px;"><span style="font-family:Tahoma, Arial; color:#ffffff;">Username:</span></td>
			 <td align="left"><input type="text" id="username" size="20" name="username" onBlur="SetOnBlurUserBox();" onFocus="SetOnFocusUserBox();" /></td>
			</tr>
			<tr>
			 <td valign="top" align="right"><span style="font-family:Tahoma, Arial; color:#ffffff;">Password:</span></td>			 
			 <td align="left"><input type="text" id="mockpass" name="mockpass" value="Password" size="20" onFocus="ShowHideMockPassword();" />
			 <input type="password" name="password" id="realpass" size="20" style="display: none;" onBlur="ShowHidePassword();" />
			  <br />
			</tr>
			<tr>
			 <td></td>
			 <td align="left">
			  <input type="submit" class="AdminFormButton1" value="Login..." />
			 </td>
			</tr>
		    <tr>
		     <td colspan="3"><br /><!--<a href="../login/forgotpassword.php">Forgot Your Password ?</a>--></td>
		    </tr>
			<input type="hidden" name="action" id="action" value="login" />
			</form>
		   </table>
		   </div>
		   <br><br>
		  </td>
		 </tr>
		</table>
		<script type="text/javascript">
			setLoginTextBoxes()
		</script>	
		<br />';

    	return($sReturn);
    }
	
	//Employees List
	function EmployeesList()
	{
		global $objDatabase;
		
		$aEmployees = array();
		$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.OrganizationId='" . cOrganizationId . "' AND E.Status='1'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sFirstName = $objDatabase->Result($varResult, $i, "E.FirstName");
			$sLastName = $objDatabase->Result($varResult, $i, "E.LastName");
			$sUserName = $objDatabase->Result($varResult, $i, "E.UserName");
			$iEmployeeId = $objDatabase->Result($varResult, $i, "E.EmployeeId");
			
			$aEmployees[$i][0] = $iEmployeeId;
			$aEmployees[$i][1] = $sUserName . ' - ' . $sFirstName . ' ' . $sLastName;
			$aEmployees[$i][2] = $sUserName;
		}
		
		return($aEmployees);
	} 
	
    function ShowEmployeeHome()
    {
    	global $objDatabase;
		global $objEmployee;

    	$tHour = date("G");
    	if ($tHour < 12) $sGreeting = "Good Morning";
    	else if ($tHour < 17) $sGreeting = "Good Afternoon";
    	else $sGreeting = "Good Evening";

    	$sCurrentTime = date("l, F j, Y") . '&nbsp;&nbsp;&nbsp;' . date("g:i a");

    	$iEmployeeId = $this->iEmployeeId;
    	$varResult = $objDatabase->Query("
    	SELECT * FROM organization_employees AS E
    	INNER JOIN organization_employees_types AS ET ON ET.EmployeeTypeId = E.EmployeeTypeId
    	INNER JOIN organization_stations AS S ON S.StationId = E.StationId
    	WHERE E.OrganizationId='" . cOrganizationId . "' AND E.EmployeeId='$iEmployeeId'");
    	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry Invalid Employee...');

    	$sEmployeeUserName = $objDatabase->Result($varResult, 0, "E.UserName");
    	$sEmployeeTypeName = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeName");
    	$sStationName = $objDatabase->Result($varResult, 0, "S.StationName");
    	$dLastLoginDateTime = $objDatabase->Result($varResult, 0, "E.EmployeeLastLoginDateTime");
		
		if($dLastLoginDateTime == '1970-01-01' || $dLastLoginDateTime == "")	$sLastLoginDateTime = "-";			
    	else $sLastLoginDateTime = date("F j, Y", strtotime($dLastLoginDateTime)) . ' at ' . date("g:i a", strtotime($dLastLoginDateTime));
/*
		// Bank Accounts
		$sBankAccounts = '<tr><td style="border-bottom:1px dotted; color:green;">Bank</td><td style="border-bottom:1px dotted; color:green;">Account Title / Number</td><td style="border-bottom:1px dotted; color:green;" align="right">Balance</td>';
		$varResult = $objDatabase->Query("
		SELECT
			B.BankName AS 'BankName',
			B.BankAbbreviation AS 'BankAbbreviation',
			BA.AccountTitle AS 'AccountTitle',
			BA.BankAccountNumber AS 'AccountNumber',
			(SUM(GJE.Debit) - SUM(GJE.Credit)) AS 'Balance'

		FROM fms_banking_bankaccounts AS BA
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		GROUP BY BA.BankAccountId");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sBankAccounts .= '<tr>
			 <td style="font-size:11px; font-family:Tahoma, Arial;">' . $objDatabase->Result($varResult, $i, "BankAbbreviation") . '</td>
			 <td style="font-size:11px; font-family:Tahoma, Arial;">' . $objDatabase->Result($varResult, $i, "AccountTitle") . ' [ ' . $objDatabase->Result($varResult, $i, "AccountNumber") . ' ]</td>
			 <td style="font-size:11px; font-family:Tahoma, Arial;" align="right">' . number_format($objDatabase->Result($varResult, $i, "Balance"), 0) . '</td>
			</tr>';
		}

		// Chart of Accounts
		$sChartOfAccounts = '<tr><td style="border-bottom:1px dotted; color:green;">Chart of Accounts</td><td style="border-bottom:1px dotted; color:green;" align="right">Balance</td>';
		$varResult = $objDatabase->Query("SELECT
		 CACT.CategoryName AS 'CategoryName',
		 CACT.CategoryCode AS 'CategoryCode',
		 IF(CACT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_chartofaccounts_categories AS CACT
		INNER JOIN fms_accounts_chartofaccounts_controls AS CAC ON CAC.ChartOfAccountsCategoryId = CACT.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CAC.ChartOfAccountsControlId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		GROUP BY CACT.ChartOfAccountsCategoryId");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");
			$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");
			$dCategoryBalance = $objDatabase->Result($varResult, $i, "Balance");
			$sChartOfAccounts .= '<tr>
			 <td><a style="font-size:11px; font-family:Tahoma, Arial;" href="#noanchor" onclick="OpenChartOfAccountsCategories(\'' . $sCategoryCode . '\');"><img id="imgChartOfAccountsCategories' . $sCategoryCode . '" src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Click to show more" title="Click to show more" />&nbsp;' . $sCategoryName . '</a></td>
			 <td style="font-size:11px; font-family:Tahoma, Arial;" align="right">' . number_format($dCategoryBalance, 0) . '</td>
			</tr>
			<tr style="display:none;" id="trChartOfAccountsConrols' . $sCategoryCode . '"><td colspan="2"><div id="divChartOfAccountsControls' . $sCategoryCode . '"></div></td></tr>';
		}


		// Customers Outstanding Invoices
		$sCustomersOutstandingInvoices = '<tr><td style="border-bottom:1px dotted; color:green;">Invoice Date</td><td style="border-bottom:1px dotted; color:green;">Customer</td><td style="border-bottom:1px dotted; color:green;" align="right">Amount</td>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_customers_invoices AS I
		INNER JOIN fms_customers AS C ON C.CustomerId = I.CustomerId
		WHERE I.Status='1'
		ORDER BY I.InvoiceDate DESC");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sCustomersOutstandingInvoices .= '<tr id="trCustomerInvoices' . ($i+1) . '" ' . (($i >= 5) ? 'style="display:none;"' : '') . '>
			 <td>' . date("F j, Y", strtotime($objDatabase->Result($varResult, $i, "I.InvoiceDate"))) . '</td>
			 <td>' . $objDatabase->Result($varResult, $i, "C.CompanyName") . '</td>
			 <td align="right">' . number_format($objDatabase->Result($varResult, $i, "I.TotalAmount")) . '</td>
			</tr>';

			if ($i == 4) $sCustomersOutstandingInvoices .= '<tr id="trCustomerInvoicesMore"><td colspan="3"><a href="#noanchor" onclick="ShowAllCustomerInvoices();">...more</a></td></tr>';
		}

		$sCustomersOutstandingInvoices .= '
		<tr>
		 <td colspan="2">
		  <script language="JavaScript" type="text/javascript">
		   function ShowAllCustomerInvoices()
		   {
				HideDiv("trCustomerInvoicesMore");
				for (i=0; i < ' . $i . '; i++)
					ShowDiv("trCustomerInvoices" + (i+1));
		   }
		  </script>
		 </td>
		</tr>';
*/

		//include('../../system/library/fms/clsFMS_Reports.php');
		//$objGraph = new clsFMS_Reports_Graphs();

		//$sBudgetGraph = $objGraph->Graph_CurrentBudgetGraph();
		//$sIncomeVsExpenditureGraph = $objGraph->IncomeVsExpenditureGraph(date("Y"), date("m"),false);
		
		//Current Fiscal Year
		$iCurrentMonth = date("m");
		$dYear = date("Y");
		if($iCurrentMonth >= 7) 
		{
			$dYear2 = $dYear;
			$dYear = $dYear +1;
			$sFiscalYear = 'July , '. $dYear2 . ' - June, ' . $dYear;
		}
		else 
		{			
			$dYear2 = $dYear -1;
			$sFiscalYear = 'July , '. $dYear2 . ' - June, ' . $dYear;
		}
		
		if ($objEmployee->objEmployeeRoles->iEmployeeRole_Home_BankAccounts[0] == 0)
			$sBankAccounts = '';

		if ($objEmployee->objEmployeeRoles->iEmployeeRole_Home_ChartOfAccounts[0] == 0)
			$sChartOfAccounts = '';

		if ($objEmployee->objEmployeeRoles->iEmployeeRole_Home_CustomerOutStandingInvoices[0] == 0)
			$sCustomersOutstandingInvoices = '';

		if ($objEmployee->objEmployeeRoles->iEmployeeRole_Home_IncomeVSExpendituresGraph[0] == 0)
			$sIncomeVsExpenditureGraph = '';
			
		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';			

    	$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
    	 <tr>
    	  <td><span style="font-size:14px; font-weight:bold;">' . $sGreeting . ' ' . $this->sEmployeeFirstName . ' ' . $this->sEmployeeLastName . '!</span></td>
    	  <td align="right"><span style="font-size:12px; font-weight:normal;">' . $sButtons_Refresh . '&nbsp;&nbsp;&nbsp;' . $sCurrentTime . '</span></td>
    	 </tr>
    	</table>
    	<table border="0" cellspacing="0" cellpadding="3" width="100%" align="left">
    	 <tr>
    	  <td width="45%" valign="top">
    	   <fieldset><legend style="font-weight:bold;">Your Information</legend>
    	    <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
    	     <tr><td style="width:160px;">User Name:</td><td><span style="font-weight:bold;">' . $sEmployeeUserName . '</span></td></tr>
    	     <tr><td>Employee Type:</td><td><span style="font-weight:bold;">' . $sEmployeeTypeName . '</span></td></tr>
    	     <tr><td>Station:</td><td><span style="font-weight:bold;">' . $sStationName . '</span></td></tr>
    	     <tr><td>Last Login On:</td><td><span style="font-weight:bold;">' . $sLastLoginDateTime . '</span></td></tr>
    	    </table>
    	   </fieldset>

		   <br />
			<!--
    	   <fieldset><legend style="font-weight:bold;">Bank Accounts</legend>
    	    <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
    	     ' . $sBankAccounts . '
    	    </table>
    	   </fieldset>

		   <br />
		   

    	   <fieldset><legend style="font-weight:bold;">Chart of Accounts</legend>
    	    <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
    	     ' . $sChartOfAccounts . '
    	    </table>
    	   </fieldset>

		   <br />

		   <fieldset><legend style="font-weight:bold;">Customers Outstanding Invoices</legend>
    	    <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
    	     ' . $sCustomersOutstandingInvoices . '
    	    </table>
    	   </fieldset>

    	  </td>
		  <td width="10%"></td>
		  <td width="45%" valign="top">
		   <fieldset><legend style="font-weight:bold;">Current Budget Utilization</legend>
    	    <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
			 <tr><td align="center"><br />' . $sBudgetGraph . '<br /></td></tr>
    	    </table>
    	   </fieldset>
		   <br />
		   <fieldset><legend style="font-weight:bold;">Income Vs. Expenditure for '. $sFiscalYear. '</legend>
    	    <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
			 <tr><td align="center"><br />' . $sIncomeVsExpenditureGraph . '<br /></td></tr>
    	    </table>
    	   </fieldset>
		   -->
		  </td>
		 </tr>
		</table>
		<script type="text/javascript" language="JavaScript">
		function OpenChartOfAccountsCategories(sChartOfAccountsCategoryCode)
		{
			ShowHideDiv("trChartOfAccountsConrols" + sChartOfAccountsCategoryCode);
			document.getElementById("imgChartOfAccountsCategories" + sChartOfAccountsCategoryCode).src = ((document.getElementById("imgChartOfAccountsCategories" + sChartOfAccountsCategoryCode).src.indexOf("plus.gif", 0) > 0) ? "../images/icons/minus.gif" : "../images/icons/plus.gif");

			xajax_AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountsControls(sChartOfAccountsCategoryCode);
		}
		
		function OpenChartOfAccountsControls(iChartOfAccountsControlId)
		{
			ShowHideDiv("trChartOfAccounts" + iChartOfAccountsControlId);
			document.getElementById("imgChartOfAccountsControls" + iChartOfAccountsControlId).src = ((document.getElementById("imgChartOfAccountsControls" + iChartOfAccountsControlId).src.indexOf("plus.gif", 0) > 0) ? "../images/icons/minus.gif" : "../images/icons/plus.gif");
			xajax_AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccounts(iChartOfAccountsControlId);
		}
		
		function OpenChartOfAccountsControls_Children(iChartOfAccountsControlId)
		{							
			ShowHideDiv("trChartOfAccounts" + iChartOfAccountsControlId);
			document.getElementById("imgChartOfAccountsControls" + iChartOfAccountsControlId).src = ((document.getElementById("imgChartOfAccountsControls" + iChartOfAccountsControlId).src.indexOf("plus.gif", 0) > 0) ? "../images/icons/minus.gif" : "../images/icons/plus.gif");
			xajax_AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountsControls_Children(iChartOfAccountsControlId);
		}
		
		function OpenChartOfAccountsChildren(iChartOfAccountsId, bChildControl)
		{		 
			if(bChildControl == null) bChildControl = false;
						
			ShowHideDiv("trChartOfAccountsChildren" + iChartOfAccountsId);
			document.getElementById("imgChartOfAccountsParents" + iChartOfAccountsId).src = ((document.getElementById("imgChartOfAccountsParents" + iChartOfAccountsId).src.indexOf("plus.gif", 0) > 0) ? "../images/icons/minus.gif" : "../images/icons/plus.gif");
			xajax_AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountsChildren(iChartOfAccountsId, bChildControl);
		}
		</script>';
    	return($sReturn);
    }

    function ShowEmployeeMainMenu()
    {
    	global $objDatabase;
    	global $objEmployee;

    	$sCurrentFolder = $_SERVER['SCRIPT_NAME'];
    	$aCurrentFolder = explode('/', $sCurrentFolder);
    	$sCurrentFolder = $aCurrentFolder[count($aCurrentFolder)-2];

    	if ($objEmployee->iEmployeeId != "")
    	{
	    	$sReturn = '
			<td>
			<script type="text/javascript" language="JavaScript1.2" src="../include/js/stmenu.js"></script>
			<script type="text/javascript" language="JavaScript1.2">
			<!--
			stm_bm(["menu700a",600,"../images/template/buttons/","blank.gif",0,"","",0,0,250,0,20,1,0,0,"","",0,0,1,0,"default","hand","../include/js/"],this);
			stm_bp("p0",[0,4,0,0,0,0,0,0,100,"",-2,"",-2,90,0,0,"#000000","transparent","",0,0,0,"#000000"]);
			stm_ai("p0i0",[0,"","","",-1,-1,0,"../home/","_self","","","","",0,0,0,"","",0,0,0,1,1,"",0,"",0,"home' . (($sCurrentFolder == "home") ? '2' : '') . '.gif","home' . (($sCurrentFolder == "home") ? '2' : '') . '.gif",3,3,0,0,"#0099CC","#000000","#FFFFFF","#FFFFFF","bold 9pt Arial","bold 9pt Arial",0,0],84,26);
			stm_aix("p0i1","p0i0",[0,"","","",-1,-1,0,"../organization/","_self","","","","",0,0,0,"","",0,0,0,1,1,"",0,"",0,"organization' . (($sCurrentFolder == "organization") ? '2' : '') . '.gif","organization' . (($sCurrentFolder == "organization") ? '2' : '') . '.gif",3,3,0,0,"#000000","#000000","#FFFFFF","#FFFFFF","9pt Arial","9pt Arial"],92,26);
			stm_bp("p1",[1,4,0,0,0,0,8,0,100,"",-2,"",-2,60,0,0,"#f1f1f1","transparent","",3,1,1,"#ffffff #a7a7a7 #a7a7a7 #a7a7a7"]);
			//stm_aix("p1i0","p0i0",[0,"Regional Hierarchy","","",-1,-1,0,"../organization/?page=RegionalHierarch","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p1i0","p0i0",[0,"Stations","","",-1,-1,0,"../organization/?page=Stations","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p1i1","p0i0",[0,"Employees","","",-1,-1,0,"../organization/?page=Employees","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p1i2","p0i0",[0,"Data Backup","","",-1,-1,0,"../organization/?page=DataBackup","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p1i3","p0i0",[0,"System Settings","","",-1,-1,0,"../organization/?page=Settings","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p1i4","p0i0",[0,"System Log","","",-1,-1,0,"../organization/?page=SystemLog","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_ep();
			stm_aix("p0i2","p0i1",[0,"","","",-1,-1,0,"../accounts/","_self","","","","",0,0,0,"","",0,0,0,1,1,"",0,"",0,"accounts' . (($sCurrentFolder == "accounts") ? '2' : '') . '.gif","accounts' . (($sCurrentFolder == "accounts") ? '2' : '') . '.gif"],84,26);
			stm_bpx("p2","p1",[]);
			stm_aix("p2i0","p1i0",[0,"General Journal","","",-1,-1,0,"../accounts/?page=GeneralJournal","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p2i1","p1i0",[0,"General Ledger","","",-1,-1,0,"#","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p2i2","p1i0",[0,"Chart of Accounts","","",-1,-1,0,"../accounts/?page=ChartOfAccounts","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p2i3","p1i0",[0,"Quick Entries","","",-1,-1,0,"../accounts/?page=QuickEntries","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			//stm_aix("p2i4","p1i0",[0,"Source of Income","","",-1,-1,0,"../accounts/?page=SourceOfIncome","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p2i4","p1i0",[0,"Donors","","",-1,-1,0,"../accounts/?page=Donors","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p2i5","p1i0",[0,"Close Financial Year","","",-1,-1,0,"../accounts/?page=CloseFinancialYear","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p2i6","p1i0",[0,"Budget","","",-1,-1,0,"../accounts/?page=Budget","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p2i7","p1i0",[0,"Budget Allocation","","",-1,-1,0,"../accounts/?page=BudgetAllocation","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_ep();
			stm_aix("p0i3","p0i1",[0,"","","",-1,-1,0,"../vendors/","_self","","","","",0,0,0,"","",0,0,0,1,1,"",0,"",0,"vendors' . (($sCurrentFolder == "vendors") ? '2' : '') . '.gif","vendors' . (($sCurrentFolder == "vendors") ? '2' : '') . '.gif"],84,26);
			stm_bpx("p3","p1",[]);
			stm_aix("p3i0","p1i0",[0,"Vendors","","",-1,-1,0,"../vendors/?page=Vendors","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p3i1","p1i0",[0,"Fixed Assets","","",-1,-1,0,"../vendors/?page=FixedAssets","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p3i2","p1i0",[0,"Purchase Requests","","",-1,-1,0,"../vendors/?page=PurchaseRequests","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p3i3","p1i0",[0,"Quotations","","",-1,-1,0,"../vendors/?page=Quotations","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p3i4","p1i0",[0,"Purchase Orders","","",-1,-1,0,"../vendors/?page=PurchaseOrders","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_ep();
            stm_aix("p0i4","p0i1",[0,"","","",-1,-1,0,"../customers/","_self","","","","",0,0,0,"","",0,0,0,1,1,"",0,"",0,"customers' . (($sCurrentFolder == "customers") ? '2' : '') . '.gif","customers' . (($sCurrentFolder == "customers") ? '2' : '') . '.gif"],84,26);
			stm_bpx("p4","p1",[]);
			stm_aix("p4i0","p1i0",[0,"Customers","","",-1,-1,0,"../customers/?page=Customers","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p4i1","p1i0",[0,"Quotations","","",-1,-1,0,"../customers/?page=Quotations","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p4i2","p1i0",[0,"Sales Orders","","",-1,-1,0,"../customers/?page=SalesOrders","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p4i3","p1i0",[0,"Invoices","","",-1,-1,0,"../customers/?page=Invoices","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p4i4","p1i0",[0,"Receipts","","",-1,-1,0,"../customers/?page=Receipts","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p4i5","p1i0",[0,"Jobs","","",-1,-1,0,"../customers/?page=Jobs","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_ep();
			stm_aix("p0i5","p0i1",[0,"","","",-1,-1,0,"../banking/","_self","","","","",0,0,0,"","",0,0,0,1,1,"",0,"",0,"banking' . (($sCurrentFolder == "banking") ? '2' : '') . '.gif","banking' . (($sCurrentFolder == "banking") ? '2' : '') . '.gif"],84,26);
			stm_bpx("p5","p1",[]);
			stm_aix("p5i0","p1i0",[0,"Bank Deposits","","",-1,-1,0,"../banking/?page=BankDeposits","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p5i1","p1i0",[0,"Bank Withdrawals","","",-1,-1,0,"../banking/?page=BankWithdrawals","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p5i2","p1i0",[0,"Bank Reconciliation","","",-1,-1,0,"../banking/?page=BankReconciliation","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p5i3","p1i0",[0,"Bank Check Books","","",-1,-1,0,"../banking/?page=BankCheckBooks","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p5i4","p1i0",[0,"Bank Accounts","","",-1,-1,0,"../banking/?page=BankAccounts","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p5i5","p1i0",[0,"Banks","","",-1,-1,0,"../banking/?page=Banks","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_ep();
			stm_aix("p0i6","p0i1",[0,"","","",-1,-1,0,"../reports/","_self","","","","",0,0,0,"","",0,0,0,1,1,"",0,"",0,"reports' . (($sCurrentFolder == "reports") ? '2' : '') . '.gif","reports' . (($sCurrentFolder == "reports") ? '2' : '') . '.gif"],84,26);
			stm_bpx("p6","p1",[]);
			stm_aix("p6i0","p1i0",[0,"Accounts Reports","","",-1,-1,0,"../reports/?page=Accounts","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p6i1","p1i0",[0,"Financial Statements","","",-1,-1,0,"../reports/?page=FinancialStatements","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p6i2","p1i0",[0,"Vendor Reports","","",-1,-1,0,"../reports/?page=Vendors","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p6i3","p1i0",[0,"Customer Reports","","",-1,-1,0,"../reports/?page=Customers","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p6i4","p1i0",[0,"Banking Reports","","",-1,-1,0,"../reports/?page=Banking","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p6i5","p1i0",[0,"Graphs","","",-1,-1,0,"../reports/?page=Graphs","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_ep();
			stm_aix("p0i7","p0i1",[0,"","","",-1,-1,0,"../help/","_self","","","","",0,0,0,"","",0,0,0,1,1,"",0,"",0,"help' . (($sCurrentFolder == "help") ? '2' : '') . '.gif","help' . (($sCurrentFolder == "help") ? '2' : '') . '.gif"],91,26);
			stm_bpx("p7","p1",[]);
			stm_aix("p7i0","p1i0",[0,"Introduction","","",-1,-1,0,"../help/?page=Introduction","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p7i1","p1i0",[0,"The Organization","","",-1,-1,0,"../help/?page=Organization","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p7i2","p1i0",[0,"Accounts","","",-1,-1,0,"../help/?page=Accounts","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p7i3","p1i0",[0,"Vendors","","",-1,-1,0,"../help/?page=Vendors","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p7i4","p1i0",[0,"Customers","","",-1,-1,0,"../help/?page=Customers","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p7i5","p1i0",[0,"Banking","","",-1,-1,0,"../help/?page=Banking","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_aix("p7i6","p1i0",[0,"Reports","","",-1,-1,0,"../help/?page=Reports","_self","","","","",8,0,0,"","",0,0,0,0,1,"#ffffff",0,"#5f6154",0,"","",3,3,0,0,"#CCCC00","#CCCC00","#999999","#FFFFFF","11px Arial","11px Arial"],180,20);
			stm_ep();
			stm_ep();
			stm_em();
			//-->
			</script>

			</td>
       		<td>&nbsp;</td>';
    	}
    	else
			$sReturn = '<td>&nbsp;</td>';

	    return($sReturn);
    }

	function ShowEmployeeLoggedInTop()
	{
		$sChartOfAccounts = '<a href="#noanchor" onclick="jsOpenWindow(\'../accounts/chartofaccountsdirectory.php\',900,620);"><img src="../images/accounts/iconChartOfAccountsList.png" border="0" alt="Chart Of Accounts List" title="Chart Of Accounts List" /></a>';
		$sCustomersDirectory = '<a href="#noanchor" onclick="jsOpenWindow(\'../customers/customersdirectory.php\',900,620);"><img src="../images/customers/iconCustomersDirectory.gif" border="0" alt="Customers Directory" title="Customers Directory" /></a>';
		$sVendorsDirectory = '<a href="#noanchor" onclick="jsOpenWindow(\'../vendors/vendorsdirectory.php\',900,620);"><img src="../images/vendors/iconVendorsDirectory.gif" border="0" alt="Vendors Directory" title="Vendors Directory" /></a>';
		$sGeneralJournalEntry = '<a href="#noanchor" onclick="jsOpenWindow(\'../accounts/generaljournal.php?pagetype=details&action2=addnew&popup=1\', 900, 620);"><img src="../images/icons/iconAdd.png" border="0" alt="Add General Journal" title="Add General Journal" /></a>&nbsp;';

		$sReturn = '<table border="0" cellspacing="0" cellpadding="0" align="right">
		 <tr><td align="right" style="font-size:12px; font-family:Tahoma, Arial;">' . $sGeneralJournalEntry. '&nbsp;' . $sChartOfAccounts. '&nbsp;' . $sVendorsDirectory . '&nbsp;' . $sCustomersDirectory . '&nbsp;' .	 $this->sEmployeeFirstName . ' ' . $this->sEmployeeLastName . ' signed in&nbsp;&nbsp;|&nbsp;&nbsp;</td><td align="right"><img src="../images/icons/iconSignOut.gif" alt="Sign Out" title="Sign Out" border="0" /></td><td><a style="font-family:Tahoma, Verdana, Arial;" href="../login/?action=logout">&nbsp;Sign Out&nbsp;</a></span>&nbsp;&nbsp;&nbsp;</td></tr>
		</table>';

		return($sReturn);
	}

    function ShowAllEmployees($sSearch, $iStationId = 0, $iEmployeeTypeId = 0)
    {
		global $objDatabase;
		global $objGeneral;
        global $objEmployee;

   		// Employee Roles
   		if ($this->objEmployeeRoles->aEmployeeRole_Employees_Employees[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		if ($iStationId == '') $iStationId = 0;
		if ($iEmployeeTypeId == '') $iEmployeeTypeId = 0;

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

        $sTitle = "Employees";
        if ($sSortBy == "") $sSortBy = "E.EmployeeId";

        if ($sSearch != "")
        {
			$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((E.EmployeeId LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (E.EmailAddress LIKE '%$sSearch%') OR (E.PhoneNumber LIKE '%$sSearch%') OR (E.MobileNumber LIKE '%$sSearch%') OR (ET.EmployeeTypeName LIKE '%$sSearch%')) ";
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        if ($iStationId > 0)
        {
        	$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.StationId='$iStationId'");
        	if ($objDatabase->RowsNumber($varResult) > 0)
           		$sTitle = 'Employees from ' . $objDatabase->Result($varResult, 0, "S.StationName");
        }

        if ($iEmployeeTypeId > 0)
        {
        	$varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.EmployeeTypeId='$iEmployeeTypeId'");
        	if ($objDatabase->RowsNumber($varResult) > 0)
           		$sTitle = 'Employees as ' . $objDatabase->Result($varResult, 0, "ET.EmployeeTypeName");
        }

        if ($iStationId == 0) $sEmployeeStationString = "";
        else $sEmployeeStationString = " AND E.StationId='$iStationId'";

        if ($iEmployeeTypeId == 0) $sEmployeeTypeString = "";
        else $sEmployeeTypeString = " AND E.EmployeeTypeId='$iEmployeeTypeId'";
		/*
        if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess == 0) // Access on employee branch only
			$sEmployeeRestriction = " AND E.StationId='$objEmployee->iEmployeeStationId'";
		*/

        $iTotalRecords = $objDatabase->DBCount("organization_employees AS E INNER JOIN organization_employees_types AS ET ON ET.EmployeeTypeId=E.EmployeeTypeId", "E.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sEmployeeStationString $sEmployeeTypeString $sEmployeeRestriction");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM organization_employees AS E
		INNER JOIN organization_employees_types AS ET ON ET.EmployeeTypeId = E.EmployeeTypeId
        WHERE E.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sEmployeeStationString $sEmployeeTypeString $sEmployeeRestriction
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">		 
		 <td align="left"><span class="WhiteHeading">Employee Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=E.FirstName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Employee Name in Ascending Order" title="Sort by Employee Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=E.FirstName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Employee Name in Descending Order" title="Sort by Employee Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Employee Type&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=ET.EmployeeTypeName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Employee Type in Ascending Order" title="Sort by Employee Type in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=ET.EmployeeTypeName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Employee Type Name in Descending Order" title="Sort by Employee TYpe Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">UserName&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=E.UserName&sortorder="><img src="../images/sort_up.gif" alt="Sort by User Name in Ascending Order" title="Sort by User Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=E.UserName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by User Name in Descending Order" title="Sort by User Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=E.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Employee Status in Ascending Order" title="Sort by Employee Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=E.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Employee Status in Descending Order" title="Sort by Employee Status in Descending Order" border="0" /></a></span></td>
		 <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iEmployeeId = $objDatabase->Result($varResult, $i, "E.EmployeeId");
			$sUserName = $objDatabase->Result($varResult, $i, "E.UserName");
            $sFirstName = $objDatabase->Result($varResult, $i, "E.FirstName");
            $sLastName = $objDatabase->Result($varResult, $i, "E.LastName");
            $sEmployeeType = $objDatabase->Result($varResult, $i, "ET.EmployeeTypeName");
            $iEmployeeStatus = $objDatabase->Result($varResult, $i, "E.Status");
            $sEmployeeStatus = ($iEmployeeStatus == 1) ? 'Active' : 'Inactive';

            $sEditEmployee = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString($sUserName) . '\', \'../organization/employees.php?pagetype=details&action2=edit&id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Employee Details" title="Edit this Employee Details"></a></td>';
            $sDeleteEmployee = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Employee?\')) {window.location = \'?action=DeleteEmployee&id=' . $iEmployeeId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Employee" title="Delete this Employee"></a></td>';
            $sEmployeeRoles = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sUserName) . ' Roles\', \'../organization/employees_roles.php?id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/organization/iconEmployeeRoles.gif" border="0" alt="Employee Roles" title="Employee Roles"></a></td>';
            $sEmployeeDocuments = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Documents of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sUserName)) . '\', \'../common/documents_show.php?componentname=EmployeesDocuments&id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconFolder.gif" border="0" alt="Employee Documents" title="Employee Documents"></a></td>';
			$sEmployeesLedger = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Employees Ledger ' . $objDatabase->RealEscapeString(stripslashes($sUserName)) . '\', \'../organization/employees.php?pagetype=employeesledger&id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" alt="Employees Ledger" title="Employees Ledger"></a></td>';

            if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[2] == 0)	$sEditEmployee = '<td class="GridTD">&nbsp;</td>';
            if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[3] == 0)	$sDeleteEmployee = '<td class="GridTD">&nbsp;</td>';
            if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeRoles[0] == 0)	$sEmployeeRoles = '<td class="GridTD">&nbsp;</td>';
            if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeDocuments[0] == 0)	$sEmployeeDocuments = '<td class="GridTD">&nbsp;</td>';

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sFirstName . ' ' . $sMiddleInitials . ' ' . $sLastName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sEmployeeType . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sUserName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sEmployeeStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sFirstName . ' ' . $sLastName) . '\', \'../organization/employees.php?pagetype=details&id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Employee Details" title="View this Employee Details"></a></td>
			 ' . $sEditEmployee . '
			 ' . $sEmployeeRoles . '
			 ' . $sEmployeeDocuments . '
			 '. $sEmployeesLedger . '
			 ' . $sDeleteEmployee . '
		    </tr>';
		}

		$sStationSelect = '<select class="form1" name="selStation" onchange="window.location=\'?station=\'+GetSelectedListBox(\'selStation\')+\'&employeetype=\'+GetSelectedListBox(\'selEmployeeType\');" id="selStation">
		<option value="0">Employees from All Stations</option>';
		$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S $sEmployeeRestriction2 ORDER BY S.StationName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sStationSelect .= '<option ' . (($iStationId == $objDatabase->Result($varResult, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "S.StationId") . '">' . $objDatabase->Result($varResult, $i, "S.StationName") . '</option>';
		$sStationSelect .= '</select>';

		$sEmployeeTypeSelect = '<select class="form1" name="selEmployeeType" onchange="window.location=\'?station=\'+GetSelectedListBox(\'selStation\')+\'&employeetype=\'+GetSelectedListBox(\'selEmployeeType\');" id="selEmployeeType">
		<option value="0">All Employee Types</option>';
		$varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET $sEmployeeRestriction2 ORDER BY ET.EmployeeTypeName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sEmployeeTypeSelect .= '<option ' . (($iEmployeeTypeId == $objDatabase->Result($varResult, $i, "ET.EmployeeTypeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "ET.EmployeeTypeId") . '">' . $objDatabase->Result($varResult, $i, "ET.EmployeeTypeName") . '</option>';
		$sEmployeeTypeSelect .= '</select>';

		$sAddNewEmployee = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Employee\', \'../organization/employees.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add New Employee">';

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[1] == 0) // Add Disabled
			$sAddNewEmployee = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		  ' . $sAddNewEmployee . '
		  &nbsp;&nbsp;&nbsp;
          <form method="GET" action="">Search for an Employee:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for an Employee" title="Search for an Employee" border="0"></form>
          </td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>' . $sStationSelect . '&nbsp;&nbsp;' . $sEmployeeTypeSelect . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Employee Details" alt="View this Employee Details"></td><td>View this Employee Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Employee Details" alt="Edit this Employee Details"></td><td>Edit this Employee Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/organization/iconEmployeeRoles.gif" title="Employee Roles" alt="Employee Roles"></td><td>Employee Roles</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Employee" alt="Delete this Employee"></td><td>Delete this Employee</td></tr>
        </table>';

		return($sReturn);
    }

    function ShowEmployeeDetails($iEmployeeId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM organization_employees AS E
		INNER JOIN organization_employees_types AS ET ON ET.EmployeeTypeId = E.EmployeeTypeId
		INNER JOIN organization_stations AS S ON S.StationId = E.StationId
		INNER JOIN organization_departments AS D ON D.departmentId = E.departmentId
		WHERE E.OrganizationId='" . cOrganizationId . "' AND E.EmployeeId = '$iEmployeeId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iEmployeeId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Employee" title="Edit this Employee" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Employee?\')) {window.location = \'?pagetype=details&action=DeleteEmployee&id=' . $iEmployeeId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Employee" title="Delete this Employee" /></a>';

        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[2] == 0)	$sButtons_Edit = '';
        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[3] == 0)	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Employee Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iEmployeeId = $objDatabase->Result($varResult, 0, "E.EmployeeId");
		        $sUserName = $objDatabase->Result($varResult, 0, "E.UserName");
		        $sPassword = "********";

		        // Employee Address
		        $sAddress = $objDatabase->Result($varResult, 0, "E.Address");
		        $sCity = $objDatabase->Result($varResult, 0, "E.City");
		        $sState = $objDatabase->Result($varResult, 0, "E.State");
		        $sZipCode = $objDatabase->Result($varResult, 0, "E.ZipCode");
		        $sCountry = $objDatabase->Result($varResult, 0, "E.Country");

		        $sFirstName = $objDatabase->Result($varResult, 0, "E.FirstName");
		        $sLastName = $objDatabase->Result($varResult, 0, "E.LastName");
		        $sFatherName = $objDatabase->Result($varResult, 0, "E.FatherName");

		        $sDesignation = $objDatabase->Result($varResult, 0, "E.Designation");
		        $sNICNumber = $objDatabase->Result($varResult, 0, "E.NICNumber");
				$sPhoneNumber = $objDatabase->Result($varResult, 0, "E.PhoneNumber");
				$sMobileNumber = $objDatabase->Result($varResult, 0, "E.MobileNumber");
				$sEmailAddress = $objDatabase->Result($varResult, 0, "E.EmailAddress");
				/*
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees_reportto AS ER
				INNER JOIN organization_employees AS E ON E.EmployeeId = ER.EmployeeId_ReportTo
				WHERE ER.EmployeeId='$iEmployeeId'");
				if ($objDatabase->RowsNumber($varResult2) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					{
						$iEmployeeId_ReportTo = $objDatabase->Result($varResult2, $i, "ER.EmployeeId_ReportTo");
						$sEmployeeName_ReportTo_Temp = $objDatabase->Result($varResult2, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult2, $i, "E.LastName");
						$sEmployeeName_ReportTo .= $sEmployeeName_ReportsTo_Temp . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sEmployeeName_ReportTo_Temp . '\', \'../organization/employees.php?pagetype=details&id=' . $iEmployeeId_ReportTo . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" alt="Employee Information" title="Employee Information" border="0" /></a><br />';

						$aEmployeeReportTo[$i] = $iEmployeeId_ReportTo;
					}
				}
				else
				{
					$aEmployeeReportTo[0] = 0;
					$sEmployeeName_ReportTo = "-";
				}
				*/
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees_stations AS ES
				INNER JOIN organization_stations AS S ON S.StationId = ES.StationId
				WHERE ES.EmployeeId='$iEmployeeId'");
				if ($objDatabase->RowsNumber($varResult2) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					{
						$iStationId_Access = $objDatabase->Result($varResult2, $i, "ES.StationId");
						$sStationName_Access_Temp = $objDatabase->Result($varResult2, $i, "S.StationName");
						$sStationName_Access .= $sStationName_Access_Temp . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $sStationName_Access_Temp . '\', \'../organization/stations.php?pagetype=details&id=' . $iStationId_Access . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" alt="Station Information" title="Station Information" border="0" /></a><br />';

						$aStationAccess[$i] = $iStationId_Access;
					}
				}
				else
				{
					$aStationAccess[0] = 0;
					$sStationName_Access = "-";
				}
				
				// Chart Of Accounts
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees_chartofaccounts AS ECAC
				INNER JOIN fms_accounts_chartofaccounts AS CAC ON CAC.ChartOfAccountsId = ECAC.ChartOfAccountsId
				WHERE ECAC.EmployeeId='$iEmployeeId'
				ORDER BY CAC.ChartOfAccountsCategoryId");
				if ($objDatabase->RowsNumber($varResult2) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					{
						$iChartOfAccountsId = $objDatabase->Result($varResult2, $i, "CAC.ChartOfAccountsId");
						$sChartOfAccounts_Temp = $objDatabase->Result($varResult2, $i, "CAC.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult2, $i, "CAC.AccountTitle");
						$sChartOfAccounts .= $sChartOfAccounts_Temp . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $sChartOfAccounts_Temp . '\', \'../accounts/chartofaccounts.php?pagetype=details&id=' . $iChartOfAccountsId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" alt="Chart of Account Information" title="Chart of Account Information" border="0" /></a><br />';

						$aChartOfAccounts[$i] = $iChartOfAccountsId;
					}
				}
				else
				{
					$aChartOfAccounts[0] = 0;
					$sChartOfAccounts = "-";
				}
				
				// Donor Projects
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees_donorprojects AS EDP
				INNER JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId = EDP.DonorProjectId
				WHERE EDP.EmployeeId='$iEmployeeId'");
				if ($objDatabase->RowsNumber($varResult2) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					{
						$iDonorProjectId = $objDatabase->Result($varResult2, $i, "DP.DonorProjectId");
						$sDonorProjects_Temp = $objDatabase->Result($varResult2, $i, "DP.ProjectCode") . ' - ' . $objDatabase->Result($varResult2, $i, "DP.ProjectTitle");
						$sDonorProjects .= $sDonorProjects_Temp . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $sDonorProjects_Temp . '\', \'../accounts/donorprojects.php?pagetype=details&id=' . $iDonorProjectId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" alt="Donor Project Information" title="Donor Project Information" border="0" /></a><br />';

						$aDonorProjects[$i] = $iDonorProjectId;
					}
				}
				else
				{
					$aDonorProjects[0] = 0;
					$sDonorProjects = "-";
				}
				
		        $sNotes = $objDatabase->Result($varResult, 0, "E.Notes");
		        $iStatus = $objDatabase->Result($varResult, 0, "E.Status");
		        $sStatus = ($iStatus == 1) ? 'Active' : 'Inactive';
				$iApplyRestriction = $objDatabase->Result($varResult, 0, "E.ApplyRestriction");
				$sApplyRestriction = (($iApplyRestriction == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$dRestrictionAmount = $objDatabase->Result($varResult, 0, "E.RestrictionAmount");
				$sRestrictionAmount = number_format($dRestrictionAmount, 2);

				$dEmployeeAddedDateTime = $objDatabase->Result($varResult, 0, "E.EmployeeAddedDateTime");
				$sEmployeeAddedDateTime = date("F j, Y", strtotime($dEmployeeAddedDateTime)) . ' at ' . date("g:i a", strtotime($dEmployeeAddedDateTime));
				
				$iStationId = $objDatabase->Result($varResult, 0, "E.StationId");
				$sEmployeeStation = $objDatabase->Result($varResult, 0, "S.StationName");
				$sEmployeeStation = $sEmployeeStation . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $sEmployeeStation . '\', \'../organization/stations.php?pagetype=details&id=' . $iStationId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" alt="Station Information" title="Station Information" border="0" /></a><br />';
				$sEmployeeType = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeName");
				
				$iDepartmentId = $objDatabase->Result($varResult, 0, "E.DepartmentId");
				$sDeparmentName = $objDatabase->Result($varResult, 0, "D.DepartmentName");
				$sDeparmentName = $sDeparmentName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $sDeparmentName . '\', \'../organization/departments?pagetype=details&id=' . $iDepartmentId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" alt="Department Information" title="Department Information" border="0" /></a><br />';
				$iEmployeeTypeId = $objDatabase->Result($varResult, 0, "E.EmployeeTypeId");
		    }


			if ($sAction2 == "edit")
			{
  		        $sStatus = '<select class="form1" name="selStatus" id="selStatus">';
		        $sStatus .= '<option ' . (($iStatus == 1) ? 'selected="true"' : '') . ' value="1">Active</option>';
		        $sStatus .= '<option ' . (($iStatus == 0) ? 'selected="true"' : '') . ' value="0">Inactive</option>';
		        $sStatus .= '</select>';

		        $sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';

		        $sReturn .= '<form method="post" onsubmit="return AddNewEmployee();" action="">';

		        $sUserName = '<input type="text" name="txtUserName" id="txtUserName" class="form1" value="' . $sUserName . '" size="30" />';
		        $sPassword = '<input type="password" name="txtPassword" id="txtPassword" class="form1" value="' . $sPassword . '" size="30" />';

		        $sFirstName = '<input type="text" name="txtFirstName" id="txtFirstName" class="form1" value="' . $sFirstName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
		        $sLastName = '<input type="text" name="txtLastName" id="txtLastName" class="form1" value="' . $sLastName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
		        $sFatherName = '<input type="text" name="txtFatherName" id="txtFatherName" class="form1" value="' . $sFatherName . '" size="30" />';
		        $sNICNumber = '<input type="text" name="txtNICNumber" id="txtNICNumber" class="form1" value="' . $sNICNumber . '" size="30" />&nbsp;<span style="color:red;">*</span>';
		        $sDesignation = '<input type="text" name="txtDesignation" id="txtDesignation" class="form1" value="' . $sDesignation . '" size="30" />&nbsp;<span style="color:red;">*</span>';

				$sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sStationCode = '<input type="text" name="txtStationCode" id="txtStationCode" class="form1" value="' . $sStationCode . '" size="30" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sMobileNumber = '<input type="text" name="txtMobileNumber" id="txtMobileNumber" class="form1" value="' . $sMobileNumber . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sEmailAddress = '<input type="text" name="txtEmailAddress" id="txtEmailAddress" class="form1" value="' . $sEmailAddress . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sCountry = $sCountryString;

				$sEmployeeType = '<select class="form1" name="selEmployeeType" id="selEmployeeType">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.OrganizationId='" . cOrganizationId . "' ORDER BY ET.EmployeeTypeId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sEmployeeType .= '<option ' . (($iEmployeeTypeId == $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeId") . '">' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeName") . '</option>';
				$sEmployeeType .= '</select>';

				$sEmployeeStation = '<select class="form1" name="selStation" id="selStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' ORDER BY S.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sEmployeeStation .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
				$sEmployeeStation .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
				
				$sDeparmentName = '<select class="form1" name="selDepartment" id="selDepartment">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_departments AS D WHERE D.OrganizationId='" . cOrganizationId . "' ORDER BY D.DepartmentName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sDeparmentName .= '<option ' . (($iDepartmentId == $objDatabase->Result($varResult2, $i, "D.DepartmentId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "D.DepartmentId") . '">' . $objDatabase->Result($varResult2, $i, "D.DepartmentName") . ' - ' . $objDatabase->Result($varResult2, $i, "D.DepartmentCode") . '</option>';
				$sDeparmentName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDepartment\'), \'../organization/departments.php?pagetype=details&id=\'+GetSelectedListBox(\'selDepartment\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Department Information" title="Department Information" border="0" /></a>';
				/*
				$sEmployeeName_ReportsTo = '<select style="width:300px;" multiple="yes" size="7" class="form1" name="selEmployeeReportsTo[]" id="selEmployeeReportsTo[]">
				<option value="0">None</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.OrganizationId='" . cOrganizationId . "' AND E.EmployeeId <> '$iEmployeeId' ORDER BY E.EmployeeId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sEmployeeName_ReportsTo .= '<option ' . (in_array($objDatabase->Result($varResult2, $i, "E.EmployeeId"), $aEmployeeReportTo) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "E.EmployeeId") . '">' . $objDatabase->Result($varResult2, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult2, $i, "E.LastName") . '</option>';
				}
				$sEmployeeName_ReportsTo .= '</select>';
				*/
				$sStationName_Access = '<select style="width:300px;" multiple="yes" size="7" class="form1" name="selStationAccess[]" id="selStationAccess[]">
				<option value="0">None</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' ORDER BY S.StationId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sStationName_Access .= '<option ' . (in_array($objDatabase->Result($varResult2, $i, "S.StationId"), $aStationAccess) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName"). '</option>';
				}
				$sStationName_Access .= '</select>';
				
				$sApplyRestriction = '<input type="checkbox" ' . (($iApplyRestriction == 1) ? 'checked="true"' : '') . ' name="chkApplyRestriction" id="chkApplyRestriction" value="on" />';
				$sRestrictionAmount = '<input type="text" name="txtRestrictionAmount" id="txtRestrictionAmount" class="form1" value="' . $dRestrictionAmount . '" size="10" />';
				
				$sChartOfAccounts = '<select style="width:300px;" multiple="yes" size="7" class="form1" name="selChartOfAccounts[]" id="selChartOfAccounts[]">
				<option value="0">None</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CAC WHERE CAC.OrganizationId='" . cOrganizationId . "' ORDER BY CAC.ChartOfAccountsCategoryId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sChartOfAccounts .= '<option ' . (in_array($objDatabase->Result($varResult2, $i, "CAC.ChartOfAccountsId"), $aChartOfAccounts) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "CAC.ChartOfAccountsId") . '">' . $objDatabase->Result($varResult2, $i, "CAC.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult2, $i, "CAC.AccountTitle") . '</option>';
				}
				$sChartOfAccounts .= '</select>';
				
				$sDonorProjects = '<select style="width:300px;" multiple="yes" size="7" class="form1" name="selDonorProjects[]" id="selDonorProjects[]">
				<option value="0">None</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.OrganizationId='" . cOrganizationId . "' AND DP.Status='1' ORDER BY DP.ProjectTitle");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sDonorProjects .= '<option ' . (in_array($objDatabase->Result($varResult2, $i, "DP.DonorProjectId"), $aDonorProjects) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult2, $i, "DP.ProjectCode") . ' - ' . $objDatabase->Result($varResult2, $i, "DP.ProjectTitle") . '</option>';
				}
				$sDonorProjects .= '</select>';
			}
			else if ($sAction2 == "addnew")
			{

				$iStatus = 1;
				$iEmployeeId = "";

				$sUserName = $objGeneral->fnGet("txtUserName");
		        $sPassword = $objGeneral->fnGet("txtPassword");

		        $sFirstName = $objGeneral->fnGet("txtFirstName");
		        $sLastName = $objGeneral->fnGet("txtLastName");
		        $sFatherName = $objGeneral->fnGet("txtFatherName");
				$sNICNumber = $objGeneral->fnGet("txtNICNumber");
				$sDesignation = $objGeneral->fnGet("txtDesignation");
				$sEmailAddress = $objGeneral->fnGet("txtEmailAddress");

				$sPhoneNumber = $objGeneral->fnGet("txtPhoneNumber");
				$sMobileNumber = $objGeneral->fnGet("txtMobileNumber");

				$sAddress = $objGeneral->fnGet("txtAddress");
		        $sCity = $objGeneral->fnGet("txtCity");
		        $sZipCode = $objGeneral->fnGet("txtZipCode");
		        $sState = $objGeneral->fnGet("txtState");
		        $sCountry = $objGeneral->fnGet("txtCountry");

		        $sNotes = $objGeneral->fnGet("txtNotes");

  		        $sStatus = '<select class="form1" name="selStatus" id="selStatus">';
		        $sStatus .= '<option ' . (($iStatus == 1) ? 'selected="true"' : '') . ' value="1">Active</option>';
		        $sStatus .= '<option ' . (($iStatus == 0) ? 'selected="true"' : '') . ' value="0">Inactive</option>';
		        $sStatus .= '</select>';

		        $sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == ''){
		        	// die("aaaa");
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        }
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';

		        $sReturn .= '<form method="post" onsubmit="return AddNewEmployee();" action="">';

		        $sUserName = '<input type="text" name="txtUserName" id="txtUserName" class="form1" value="' . $sUserName . '" size="30" />';
		        $sPassword = '<input type="password" name="txtPassword" id="txtPassword" class="form1" value="' . $sPassword . '" size="30" />';

		        $sFirstName = '<input type="text" name="txtFirstName" id="txtFirstName" class="form1" value="' . $sFirstName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
		        $sLastName = '<input type="text" name="txtLastName" id="txtLastName" class="form1" value="' . $sLastName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
		        $sFatherName = '<input type="text" name="txtFatherName" id="txtFatherName" class="form1" value="' . $sFatherName . '" size="30" />';
		        $sNICNumber = '<input type="text" name="txtNICNumber" id="txtNICNumber" class="form1" value="' . $sNICNumber . '" size="30" />&nbsp;<span style="color:red;">*</span>';
		        $sDesignation = '<input type="text" name="txtDesignation" id="txtDesignation" class="form1" value="' . $sDesignation . '" size="30" />&nbsp;<span style="color:red;">*</span>';

				$sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sStationCode = '<input type="text" name="txtStationCode" id="txtStationCode" class="form1" value="' . $sStationCode . '" size="30" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sMobileNumber = '<input type="text" name="txtMobileNumber" id="txtMobileNumber" class="form1" value="' . $sMobileNumber . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sEmailAddress = '<input type="text" name="txtEmailAddress" id="txtEmailAddress" class="form1" value="' . $sEmailAddress . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sCountry = $sCountryString;

				$sEmployeeType = '<select class="form1" name="selEmployeeType" id="selEmployeeType">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.OrganizationId='" . cOrganizationId . "' ORDER BY ET.EmployeeTypeId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sEmployeeType .= '<option value="' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeId") . '">' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeName") . '</option>';
				$sEmployeeType .= '</select>';

				$sEmployeeStation = '<select class="form1" name="selStation" id="selStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' ORDER BY S.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sEmployeeStation .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
				$sEmployeeStation .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
				
				$sDeparmentName = '<select class="form1" name="selDepartment" id="selDepartment">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_departments AS D WHERE D.OrganizationId='" . cOrganizationId . "' ORDER BY D.DepartmentName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sDeparmentName .= '<option ' . (($iDepartmentId == $objDatabase->Result($varResult2, $i, "D.DepartmentId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "D.DepartmentId") . '">' . $objDatabase->Result($varResult2, $i, "D.DepartmentName") . ' - ' . $objDatabase->Result($varResult2, $i, "D.DepartmentCode") . '</option>';
				$sDeparmentName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDepartment\'), \'../organization/departments.php?pagetype=details&id=\'+GetSelectedListBox(\'selDepartment\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Department Information" title="Department Information" border="0" /></a>';
				
				$sEmployeeAddedDateTime = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				/*
				$sEmployeeName_ReportsTo = '<select style="width:300px;" multiple="yes" size="7" class="form1" name="selEmployeeReportsTo[]" id="selEmployeeReportsTo[]">
				<option value="0">None</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.OrganizationId='" . cOrganizationId . "' AND E.EmployeeId <> '$iEmployeeId' ORDER BY E.EmployeeId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sEmployeeName_ReportsTo .= '<option value="' . $objDatabase->Result($varResult2, $i, "E.EmployeeId") . '">' . $objDatabase->Result($varResult2, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult2, $i, "E.LastName") . '</option>';
				$sEmployeeName_ReportsTo .= '</select>';
				*/
				$sStationName_Access = '<select style="width:300px;" multiple="yes" size="7" class="form1" name="selStationAccess[]" id="selStationAccess[]">
				<option value="0">None</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' ORDER BY S.StationId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sStationName_Access .= '<option value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName"). '</option>';
				}
				$sStationName_Access .= '</select>';
				
				$sChartOfAccounts = '<select style="width:300px;" multiple="yes" size="7" class="form1" name="selChartOfAccounts[]" id="selChartOfAccounts[]">
				<option value="0">None</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CAC WHERE CAC.OrganizationId='" . cOrganizationId . "' ORDER BY CAC.ChartOfAccountsCategoryId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sChartOfAccounts .= '<option value="' . $objDatabase->Result($varResult2, $i, "CAC.ChartOfAccountsId") . '">' . $objDatabase->Result($varResult2, $i, "CAC.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult2, $i, "CAC.AccountTitle") . '</option>';
				}
				$sChartOfAccounts .= '</select>';
				
				$sDonorProjects = '<select style="width:300px;" multiple="yes" size="7" class="form1" name="selDonorProjects[]" id="selDonorProjects[]">
				<option value="0">None</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.OrganizationId='" . cOrganizationId . "' AND DP.Status='1' ORDER BY DP.ProjectTitle");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sDonorProjects .= '<option value="' . $objDatabase->Result($varResult2, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult2, $i, "DP.ProjectCode") . ' - ' . $objDatabase->Result($varResult2, $i, "DP.ProjectTitle") . '</option>';
				}
				$sDonorProjects .= '</select>';
				
				$sApplyRestriction = '<input type="checkbox" ' . (($iApplyRestriction == 1) ? 'checked="true"' : '') . ' name="chkApplyRestriction" id="chkApplyRestriction" value="on" />';
				$sRestrictionAmount = '<input type="text" name="txtRestrictionAmount" id="txtRestrictionAmount" class="form1" value="' . $dRestrictionAmount . '" size="10" />';
				
			}

			$sReturn .= '
			<script type="text/javascript" language="JavaScript">
			 function AddNewEmployee()
			 {
				//if (document.getElementById(\'txtUserName\').value=="") return(AlertFocus("Please Enter the Employee User Name!", "txtUserName"));
				//if (document.getElementById(\'txtPassword\').value == "") return(AlertFocus("Please enter the employee password!", "txtPassword"));
				if (document.getElementById(\'txtFirstName\').value == "") return(AlertFocus("Please enter the employee First Name!", "txtFirstName"));
				if (document.getElementById(\'txtLastName\').value == "") return(AlertFocus("Please enter the employee Last Name!", "txtLastName"));

			 	if ((GetVal("txtNICNumber").length != 13) && (GetVal("txtNICNumber").length != 15) && (GetVal("txtNICNumber").length != 10)) return(AlertFocus("Please enter the Customer NIC Number. In case of CNIC, enter 15 digits, for Old NICs enter 13 digits, for token, enter 10 digits", "txtNICNumber"));

				if (document.getElementById(\'txtDesignation\').value == "") return(AlertFocus("Please enter the employee Designation!", "txtDesignation"));
				if (document.getElementById(\'txtEmailAddress\').value == "") return(AlertFocus("Please enter the employee E-Mail Address!", "txtEmailAddress"));
				if (!isEmailAddress(GetVal(\'txtEmailAddress\'))) return(AlertFocus("Please enter the valid E-Mail Address!", "txtEmailAddress"));
				if (document.getElementById(\'txtAddress\').value == "") return(AlertFocus("Please enter the employee Address!", "txtAddress"));
				if (document.getElementById(\'txtCity\').value == "") return(AlertFocus("Please enter the employee City!", "txtCity"));
				if (document.getElementById(\'txtPhoneNumber\').value == "") return(AlertFocus("Please enter the employee phone number!", "txtPhoneNumber"));
				if (document.getElementById(\'txtMobileNumber\').value == "") return(AlertFocus("Please enter the employee mobile number!", "txtMobileNumber"));

				return(true);
			 }
			</script>

			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Employee Information:</span></td></tr>
			 <tr>
			  <td width="20%">Employee Type:</td><td width="30%"><strong>' . $sEmployeeType . '</strong></td>
			  <td rowspan="3" colspan="2" valign="top">Station Access:<br /><strong>' . $sStationName_Access . '</strong></td>
			 </tr>
			<!--
			 <tr bgcolor="#edeff1">
			  <td>Employee Type:</td><td><strong>' . $sEmployeeType . '</strong></td>			  			  
			 </tr>
			-->
			 <tr bgcolor="#edeff1"><td>Station:</td><td><strong>' . $sEmployeeStation . '</strong></td></tr>
			 <tr><td>Department:</td><td><strong>' . $sDeparmentName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>UserName:</td><td><strong>' . $sUserName . '</strong></td></tr>
			 <tr><td>Password:</td><td><strong>' . $sPassword . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Status:</td><td><strong>' . $sStatus . '</strong></td></tr>
			 <tr>
			  <td>Donor Projects:</td><td><strong>' . $sDonorProjects . '</strong></td>
			 </tr>
			 <tr bgcolor="#edeff1">
			  <td>Chart Of Accounts:</td><td><strong>' . $sChartOfAccounts . '</strong></td>
			 </tr>
			 <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Employee Personal Information:</span></td></tr>
			 <tr>
			  <td>First Name:</td><td><strong>' . $sFirstName . '</strong></td>
			  <td width="20%">Last Name:</td><td><strong>' . $sLastName . '</strong></td>
			 </tr>
			 <tr bgcolor="#edeff1">
			  <td>Father\'s Name:</td><td><strong>' . $sFatherName . '</strong></td>
			  <td>NIC Number:</td><td><strong>' . $sNICNumber . '</strong></td>
			 </tr>
			 <tr>
			  <td>Designation:</td><td><strong>' . $sDesignation . '</strong></td>
			  <td>Email Address:</td><td><strong>' . $sEmailAddress . '</strong></td>
			 </tr>
			  <tr bgcolor="#edeff1">
			  <td>Apply Restriction:</td><td><strong>' . $sApplyRestriction . '</strong></td>
			  <td>Restriction Amount:</td><td><strong>' . $sRestrictionAmount . '</strong></td>
			 </tr>
		     <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Employee Address:</span></td></tr>
			 <tr>
			  <td>Address:</td><td><strong>' . $sAddress . '</strong></td>
			  <td>City:</td><td><strong>' . $sCity . '</strong></td>
			 </tr>
			 <tr bgcolor="#edeff1">
			  <td>State/Province:</td><td><strong>' . $sState . '</strong></td>
			  <td>Zip Code:</td><td><strong>' . $sZipCode . '</strong></td>
			 </tr>
			 <tr>
			  <td>Country:</td><td><strong>' . $sCountry . '</strong></td>
			  <td>Phone Number:</td><td><strong>' . $sPhoneNumber . '</strong></td>
			 </tr>
			 <tr bgcolor="#edeff1"><td>Mobile Number:</td><td colspan="3"><strong>' . $sMobileNumber . '</strong></td></tr>
		     <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Extra Information:</span></td></tr>
		     <tr><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Employee Added On:</td><td colspan="3"><strong>' . $sEmployeeAddedDateTime . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Employee" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iEmployeeId . '"><input type="hidden" name="action" id="action" value="UpdateEmployee"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Employee" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewEmployee"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';

		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

    function UpdateEmployee($iEmployeeId, $iEmployeeTypeId, $iStationId, $iDepartmentId, $sUserName, $sPassword, $aStationAccess, $aChartOfAccounts, $aDonorProjects, $iStatus, $sFirstName, $sLastName, $sFatherName, $sNICNumber, $sDesignation, $sEmailAddress, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sMobileNumber, $sNotes, $iApplyRestriction, $dRestrictionAmount)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEncryption;
		global $objEmployee;
		global $objSystemLog;
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[1] == 0)
			return(1010);
		
        // If employee type is being updated, then update its roles too
        $varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId='$iEmployeeId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Id');
        $iEmployeeTypeId_Temp = $objDatabase->Result($varResult, 0, "E.EmployeeTypeId");

        $varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.EmployeeTypeId='$iEmployeeTypeId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Type Id');
        $sEmployeeRoles = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeRoles");

        if ($iEmployeeTypeId != $iEmployeeTypeId_Temp)
        	$sEmployeeRolesString = ", EmployeeRoles='$sEmployeeRoles'";		
        // Username already added in the database
		if($sUserName != "")
			if ($objDatabase->DBCount("organization_employees AS E", "E.EmployeeId <> '$iEmployeeId' AND E.UserName='$sUserName'") > 0) return(2003);
		
		if($dRestrictionAmount == "") $dRestrictionAmount = 0;
		if ($iApplyRestriction == "on") $iApplyRestriction = 1; 
		else 
		{
			$iApplyRestriction = 0;
			$dRestrictionAmount = 0;
		}

        $sUserName = $objDatabase->RealEscapeString($sUserName);
        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
        $sCountry = $objDatabase->RealEscapeString($sCountry);
        $sNotes = $objDatabase->RealEscapeString($sNotes);

        if ($sPassword != "********")
        	$sPasswordChangeString = ", Password='" . $objGeneral->EncryptPassword($sPassword, $objEncryption) . "'";

        $varResult = $objDatabase->Query("
        UPDATE organization_employees SET
        	EmployeeTypeId='$iEmployeeTypeId',
        	StationId='$iStationId',
			DepartmentId='$iDepartmentId',
            UserName='$sUserName',
            FirstName='$sFirstName',
            LastName='$sLastName',
            FatherName='$sFatherName',
            NICNumber='$sNICNumber',
            Designation='$sDesignation',
            EmailAddress='$sEmailAddress',
            Address='$sAddress',
            City='$sCity',
            State='$sState',
            ZipCode='$sZipCode',
            Country='$sCountry',
            PhoneNumber='$sPhoneNumber',
            MobileNumber='$sMobileNumber',
			ApplyRestriction='$iApplyRestriction',
			RestrictionAmount='$dRestrictionAmount',
            Notes='$sNotes',
            Status='$iStatus'
            $sPasswordChangeString
        	$sEmployeeRolesString

        WHERE EmployeeId='$iEmployeeId'");

    	$varResult = $objDatabase->Query("DELETE from organization_employees_stations WHERE EmployeeId='$iEmployeeId'");
		$varResult = $objDatabase->Query("DELETE from organization_employees_chartofaccounts WHERE EmployeeId='$iEmployeeId'");
		$varResult = $objDatabase->Query("DELETE from organization_employees_donorprojects WHERE EmployeeId='$iEmployeeId'");
    	
		// Add Station Access
    	for ($i=0; $i < count($aStationAccess); $i++)
    	{
    		// Already Added
    		$iStationId_Access = $aStationAccess[$i];
    		if ($objDatabase->DBCount("organization_employees_stations AS ES", "ES.StationId='$iStationId_Access' AND ES.EmployeeId='$iEmployeeId'") <= 0)
    			if ($objDatabase->DBCount("organization_stations AS S", "S.StationId='$iStationId_Access'") > 0)
    				$varResult = $objDatabase->Query("INSERT INTO organization_employees_stations (EmployeeId, StationId) VALUES ('$iEmployeeId', '$iStationId_Access')");
    	}
		
		// Add Employees Chart Of Accounts
    	for ($i=0; $i < count($aChartOfAccounts); $i++)
    	{
    		// Already Added
    		$iChartOfAccountsId = $aChartOfAccounts[$i];
			if ($objDatabase->DBCount("organization_employees_chartofaccounts AS ECAC", "ECAC.ChartOfAccountsId='$iChartOfAccountsId' AND ECAC.EmployeeId='$iEmployeeId'") <= 0)
    			if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CAC", "CAC.ChartOfAccountsId='$iChartOfAccountsId'") > 0)
    				$varResult = $objDatabase->Query("INSERT INTO organization_employees_chartofaccounts (EmployeeId, ChartOfAccountsId) VALUES ('$iEmployeeId', '$iChartOfAccountsId')");
    	}
		// Add Employees Donor Projects
    	for ($i=0; $i < count($aDonorProjects); $i++)
    	{
    		// Already Added
    		$iDonorProjectId = $aDonorProjects[$i];
			if ($objDatabase->DBCount("organization_employees_donorprojects AS EDP", "EDP.DonorProjectId='$iDonorProjectId' AND EDP.EmployeeId='$iEmployeeId'") <= 0)
    			if ($objDatabase->DBCount("fms_accounts_donors_projects AS DP", "DP.DonorProjectId='$iDonorProjectId'") > 0)
    				$varResult = $objDatabase->Query("INSERT INTO organization_employees_donorprojects (EmployeeId, DonorProjectId) VALUES ('$iEmployeeId', '$iDonorProjectId')");
    	}

		$objSystemLog->AddLog("Update Employee - " . $sUserName);

		$objGeneral->fnRedirect('../organization/employees.php?pagetype=details&error=2007&id=' . $iEmployeeId);
    }

    function DeleteEmployee($iEmployeeId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objSystemLog;

  		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[3] == 0) // Delete Disabled
			return(1010);
		if ($iEmployeeId == 1) return(1011);
		/*
        if ($objDatabase->DBCount("organization_employees_documents AS ED", "ED.EmployeeId='$iEmployeeId'") > 0) return(2015);
        $varResult = $objDatabase->Query("DELETE FROM organization_systemlog WHERE EmployeeId='$iEmployeeId'");
		*/

        $varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId='$iEmployeeId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Invalid Employee Id');
        $sEmployeeName = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' .$objDatabase->Result($varResult, 0, "E.LastName");

        $varResult = $objDatabase->Query("DELETE FROM organization_employees WHERE EmployeeId='$iEmployeeId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {	
			$objSystemLog->AddLog("Delete Employee - " . $sEmployeeName);
            return(2008);
        }
        else
            return(2009);
    }

    function AddNewEmployee($iEmployeeTypeId, $iStationId, $iDepartmentId, $sUserName, $sPassword, $aStationAccess, $aChartOfAccounts, $aDonorProjects, $iStatus, $sFirstName, $sLastName, $sFatherName, $sNICNumber, $sDesignation, $sEmailAddress, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sMobileNumber, $sNotes, $iApplyRestriction, $dRestrictionAmount)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEncryption;
		global $objEmployee;
		global $objSystemLog;
		
        $varNow = $objGeneral->fnNow();
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[1] == 0)
			return(1010);
				
        $varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.EmployeeTypeId='$iEmployeeTypeId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Type Id');
        $sEmployeeRoles = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeRoles");
		
		$sUserName = strtolower($sUserName);
        $sUserName = str_replace(' ', '', $sUserName);
		$sUserName = $objDatabase->RealEscapeString($sUserName);		
        // Username already added in the database
		if($sUserName != "")
			if ($objDatabase->DBCount("organization_employees AS E", "E.UserName='$sUserName'") > 0) return(2003);
		
		if($dRestrictionAmount == "") $dRestrictionAmount = 0;
		if ($iApplyRestriction == "on") $iApplyRestriction = 1; 
		else 
		{
			$iApplyRestriction = 0;
			$dRestrictionAmount = 0;
		}
		
        $sPassword = $objGeneral->EncryptPassword($sPassword, $objEncryption);

        $sUserName = strtolower($sUserName);
        $sUserName = str_replace(' ', '', $sUserName);
		$sFirstName = $objDatabase->RealEscapeString($sFirstName);
		$sLastName = $objDatabase->RealEscapeString($sLastName);		
		$sFatherName = $objDatabase->RealEscapeString($sFatherName);
		$sNICNumber = $objDatabase->RealEscapeString($sNICNumber);
		$sDesignation = $objDatabase->RealEscapeString($sDesignation);
		$sEmailAddress = $objDatabase->RealEscapeString($sEmailAddress);				
        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
        $sCountry = $objDatabase->RealEscapeString($sCountry);
		$sPhoneNumber = $objDatabase->RealEscapeString($sPhoneNumber);
		$sMobileNumber = $objDatabase->RealEscapeString($sMobileNumber);
        $sNotes = $objDatabase->RealEscapeString($sNotes);

        $varResult = $objDatabase->Query("INSERT INTO organization_employees (
		OrganizationId,
        EmployeeTypeId,
        StationId,
		DepartmentId,
        UserName,
        Password,               
        FirstName,
        LastName,
        FatherName,
        Designation,
        NICNumber,
        EmailAddress,
        Address,
        City,
        State,
        ZipCode,
        Country,
        PhoneNumber,
        MobileNumber,
		ApplyRestriction,
		RestrictionAmount,		
        Notes,
        EmployeeRoles,
        Status,
        EmployeeAddedDateTime
        ) VALUES (
		'" . cOrganizationId . "', 
        '$iEmployeeTypeId',
        '$iStationId',
		'$iDepartmentId',
        '$sUserName',
        '$sPassword',       
        '$sFirstName',
        '$sLastName',
        '$sFatherName',
        '$sDesignation',
        '$sNICNumber',
        '$sEmailAddress',
        '$sAddress',
        '$sCity',
        '$sState',
        '$sZipCode',
        '$sCountry',
        '$sPhoneNumber',
        '$sMobileNumber',
		 '$iApplyRestriction',
		'$dRestrictionAmount',
        '$sNotes',
        '$sEmployeeRoles',
        '$iStatus',
        '$varNow')");

        $varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.UserName='$sUserName' AND E.EmployeeAddedDateTime='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {
        	$iEmployeeId = $objDatabase->Result($varResult, 0, "E.EmployeeId");

        	// Add Station Access
    		for ($i=0; $i < count($aStationAccess); $i++)
    		{
    			// Already Added
    			$iStationId_Access = $aStationAccess[$i];
    			if ($objDatabase->DBCount("organization_employees_stations AS ES", "ES.StationId='$iStationId_Access' AND ES.EmployeeId='$iEmployeeId'") <= 0)
    				if ($objDatabase->DBCount("organization_stations AS S", "S.StationId='$iStationId_Access'") > 0)
    					$varResult = $objDatabase->Query("INSERT INTO organization_employees_stations (EmployeeId, StationId) VALUES ('$iEmployeeId', '$iStationId_Access')");
    		}
			
			// Add Employees Chart Of Accounts
    		for ($i=0; $i < count($aChartOfAccounts); $i++)
    		{
    			// Already Added
    			$iChartOfAccountsId = $aChartOfAccounts[$i];
				if ($objDatabase->DBCount("organization_employees_chartofaccounts AS ECAC", "ECAC.ChartOfAccountsId='$iChartOfAccountsId' AND ECAC.EmployeeId='$iEmployeeId'") <= 0)
    				if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CAC", "CAC.ChartOfAccountsId='$iChartOfAccountsId'") > 0)
    					$varResult = $objDatabase->Query("INSERT INTO organization_employees_chartofaccounts (EmployeeId, ChartOfAccountsId) VALUES ('$iEmployeeId', '$iChartOfAccountsId')");
    		}
			// Add Employees Donor Projects
    		for ($i=0; $i < count($aDonorProjects); $i++)
    		{
    			// Already Added
    			$iDonorProjectId = $aDonorProjects[$i];
				if ($objDatabase->DBCount("organization_employees_donorprojects AS EDP", "EDP.DonorProjectId='$iDonorProjectId' AND EDP.EmployeeId='$iEmployeeId'") <= 0)
    				if ($objDatabase->DBCount("fms_accounts_donors_projects AS DP", "DP.DonorProjectId='$iDonorProjectId'") > 0)
    					$varResult = $objDatabase->Query("INSERT INTO organization_employees_donorprojects (EmployeeId, DonorProjectId) VALUES ('$iEmployeeId', '$iDonorProjectId')");
    		}
        	
			$objSystemLog->AddLog("Add New Employee - " . $sUserName);

            $objGeneral->fnRedirect('?pagetype=details&error=2004&id=' . $iEmployeeId);
        }
        else
            return(2005);
    }
	
	// Employees Ledger	
	function ShowAllEmployeesLedger($iEmployeeId, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_Employees[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$varResult = $objDatabase->Query("
		SELECT * FROM organization_employees AS E
		WHERE E.EmployeeId = '$iEmployeeId'");		
		if ($objDatabase->RowsNumber($varResult) <= 0)		
			return('<br /><br /><div align="center">Sorry, No records found...</div><br /><br />');
		$sEmployeeName = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = 'Ledger Details Of ' . $sEmployeeName;

        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((GJ.GeneralJournalId LIKE '%$sSearch%') OR (GJ.TransactionDate LIKE '%$sSearch%') OR (GJE.Detail LIKE '%$sSearch%') OR (GJE.Debit LIKE '%$sSearch%') OR (GJE.Credit LIKE '%$sSearch%'))";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        if ($sSortBy == "") $sSortBy = "E.FirstName";

		$iTotalRecords = $objDatabase->DBCount("organization_employees AS E INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.EmployeeId = E.EmployeeId INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId", "E.EmployeeId = '$iEmployeeId' $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?id='. $iVendorId . '&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM organization_employees AS E
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.VendorId = E.EmployeeId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
        WHERE E.EmployeeId = '$iEmployeeId' AND CA.ChartOfAccountsCategoryId !='5' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td width = "15%" align="left"><span class="WhiteHeading">Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.GeneralJournalId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Id in Ascending Order" title="Sort by Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.GeneralJournalId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Id in Descending Order" title="Sort by Id in Descending Order" border="0" /></a></span></td>
         <td width = "15%" align="left"><span class="WhiteHeading">Reference &nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.Reference&sortorder="><img src="../images/sort_up.gif" alt="Sort by Reference in Ascending Order" title="Sort by Reference in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.GeneralJournalId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Reference Number in Descending Order" title="Sort by Reference Number in Descending Order" border="0" /></a></span></td>
		 <td width="20%" align="left"><span class="WhiteHeading">Transaction Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Transaction Date in Ascending Order" title="Sort by Transaction Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Transaction Date in Descending Order" title="Sort by Transaction Date in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Detail&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Detail&sortorder="><img src="../images/sort_up.gif" alt="Sort by Detail in Ascending Order" title="Sort by Vendor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Detail&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Detail in Descending Order" title="Sort by Detail in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Debit&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Debit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Debit in Ascending Order" title="Sort by Debit in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Debit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Debit in Descending Order" title="Sort by Debit in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Credit&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Credit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Credit in Ascending Order" title="Sort by Debit in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Credit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Credit in Descending Order" title="Sort by Credit in Descending Order" border="0" /></a></span></td>
         <td width="2%" colspan="2"><span class="WhiteHeading">Operations</span></td>
		</tr>
		';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iEmployeeId = $objDatabase->Result($varResult, $i, "E.EmployeeId");
			$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
			$dTransactionDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
			$sDetail = $objDatabase->Result($varResult, $i, "GJE.Detail");
            $sReference = $objDatabase->Result($varResult, $i, "GJ.Reference");
			$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
			$dDebit = $objDatabase->Result($varResult, $i, "GJE.Debit");
			$dCredit = $objDatabase->Result($varResult, $i, "GJE.Credit");

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td align="left" valign="top">' . $iGeneralJournalId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
             <td align="left" valign="top">' . $sReference . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sTransactionDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sDetail . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . number_format($dDebit, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . number_format($dCredit, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
             <td class="GridTD" align="center">
              <a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Transaction Details\', \'../accounts/generaljournal.php?pagetype=details&id=' . $iGeneralJournalId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Transaction Details" title="View this Transaction Details"></a>
              <a href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&txtId=' . $iGeneralJournalId . '\', 800,600);" style="font-family: Verdana, Arial; font-size:12px;"><img border="0" src = "../images/icons/iconPrint2.gif" tilte="Print Receipt" alt="Print Receipt" /></a>
             </td>
			</tr>';
		}		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
          <form method="GET" action=""><td align="left" colspan="2">Search for a Transaction:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" /><input type="hidden" name="id" id="id" value="' . $iVendorId . '" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Vendor" title="Search for a Vendor" border="0"></td> </form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>';

		return($sReturn);
    }
	
	
   	function GetEmployeeRolesOption($sOptionName, $aOptionValue, $bView = true, $bAdd = true, $bUpdate = true, $bDelete = true)
	{
		$sReturn = '';

		if ($bView) $sReturn .= '<td align="center"><input ' . (($aOptionValue[0] == 1) ? 'checked="true"' : '') . ' type="checkbox" name="' . $sOptionName . '_View" id="' . $sOptionName . '_View" value="on" /></td>';
		else $sReturn .= '<td></td>';

		if ($bAdd) $sReturn .= '<td align="center"><input ' . (($aOptionValue[1] == 1) ? 'checked="true"' : '') . ' type="checkbox" name="' . $sOptionName . '_Add" id="' . $sOptionName . '_Add" value="on" /></td>';
		else $sReturn .= '<td></td>';

		if ($bUpdate) $sReturn .= '<td align="center"><input ' . (($aOptionValue[2] == 1) ? 'checked="true"' : '') . ' type="checkbox" name="' . $sOptionName . '_Update" id="' . $sOptionName . '_Update" value="on" /></td>';
		else $sReturn .= '<td></td>';

		if ($bDelete) $sReturn .= '<td align="center"><input ' . (($aOptionValue[3] == 1) ? 'checked="true"' : '') . ' type="checkbox" name="' . $sOptionName . '_Delete" id="' . $sOptionName . '_Delete" value="on" /></td>';
		else $sReturn .= '<td></td>';

		return($sReturn);
	}

   	function GetEmployeeRolesOptionYesNo($sOptionName, $iOptionValue)
	{
		$sReturn = '<td colspan="4" align="center"><input type="radio" value="1" ' . (($iOptionValue == 1) ? 'checked="true"' : '') . ' name="' . $sOptionName . '" id="' . $sOptionName . '">Yes</option>
		&nbsp;<input type="radio" value="0" ' . (($iOptionValue == 0) ? 'checked="true"' : '') . ' name="' . $sOptionName . '" id="' . $sOptionName . '">No</option></td>';

		return($sReturn);
	}

	function EmployeeRolesValues($iId, $objEmployeeRoles, $sType = "EmployeeRoles")
	{
		global $objDatabase;
		
		$sEmployeeTypesList = '<select class="form1" name="selCopyRoles_EmployeeType" id="selCopyRoles_EmployeeType">';
		$varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ED ORDER BY ED.EmployeeTypeName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sEmployeeTypesList .= '<option value="' . $objDatabase->Result($varResult, $i, "ED.EmployeeTypeId") . '">' . $objDatabase->Result($varResult, $i, "ED.EmployeeTypeName") . '</option>';
		$sEmployeeTypesList .= '</select>';

		$sEmployeeList = '<select class="form1" name="selCopyRoles_Employee" id="selCopyRoles_Employee">';
		$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E ORDER BY E.FirstName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sEmployeeList .= '<option value="' . $objDatabase->Result($varResult, $i, "E.EmployeeId") . '">' . $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName") . ' ( ' . $objDatabase->Result($varResult, $i, "E.UserName") . ' )</option>';
		$sEmployeeList .= '</select>';
		
		$sCopyRoles = '<tr><td colspan="5"><hr size="1" /></td></tr>
		     <tr><td colspan="5"><a style="font-size:14px; font-weight:bold; color:green;" href="#End" onclick="ShowHideDiv(\'divCopyRoles\');"><img src="../images/icons/plus.gif" align="absmiddle" border="0" alt="Copy Roles from Types" title="Copy Roles from Types"/>&nbsp;&nbsp;Copy Roles</a></td></tr>
		     <tr>
		      <td colspan="5">
		       <div id="divCopyRoles" style="display:none;">
		        <fieldset style="width:80%;" align="center">
				 <table border="0" cellspacing="0" cellpadding="10" width="90%" align="center">
				  <tr>
				   <td align="center">
				    <form method="post" action="">
				    <span style="font-size:14px; font-weight:bold; color:blue;">Copy Roles from  Employee Type</span>
				    <br /><br />
				    ' . $sEmployeeTypesList . '
				    <br /><br />
				    <input type="submit" class="AdminFormButton1" value="Copy Roles" />
				    <input type="hidden" name="id" id="id" value="' . $iId . '" />
				    <input type="hidden" name="action" id="action" value="CopyRoles_EmployeeType" />
				    </form>
				   </td>
				   <td align="center">
				    <form method="post" action="">
				    <span style="font-size:14px; font-weight:bold; color:blue;">Copy Roles from an Employee</span>
				    <br /><br />
				    ' . $sEmployeeList . '
				    <br /><br />
				    <input type="submit" class="AdminFormButton1" value="Copy Roles" />
				    <input type="hidden" name="id" id="id" value="' . $iId . '" />
				    <input type="hidden" name="action" id="action" value="CopyRoles_Employee" />
				    </form>
				   </td>
				  </tr>
				 </table>
		        </fieldset>
		       </div>
		      </td>
		     </tr>';
			
		$sReturn .= '
		   <form method="post" action="">
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td><span class="WhiteHeading">Role Type</span></td><td width="2%" align="center"><span class="WhiteHeading">View</span></td><td width="2%" align="center"><span class="WhiteHeading">Add</span></td><td width="2%" align="center"><span class="WhiteHeading">Update</span></td><td width="2%" align="center"><span class="WhiteHeading">Delete</span></td></tr>
			 <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Home:</span></td></tr>
		      <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Home:</span></td></tr>
			  <tr bgcolor="#edeff1"><td width="35%">+ Bank Accounts:</td>' . $this->GetEmployeeRolesOption('BankAccounts', $objEmployeeRoles->iEmployeeRole_Home_BankAccounts, true, false, false, false) . '</tr>
			  <tr><td>+ Chart Of Accounts:</td>' . $this->GetEmployeeRolesOption('ChartOfAccounts', $objEmployeeRoles->iEmployeeRole_Home_ChartOfAccounts, true, false, false, false) . '</tr>
		      <tr bgcolor="#edeff1"><td>+ Customer OutStanding Invoices:</td>' . $this->GetEmployeeRolesOption('CustomerOutStandingInvoices', $objEmployeeRoles->iEmployeeRole_Home_CustomerOutStandingInvoices, true, false, false, false) . '</tr>
			  <tr><td>+ Income VS Expenditures Graph:</td>' . $this->GetEmployeeRolesOption('IncomeVSExpendituresGraph', $objEmployeeRoles->iEmployeeRole_Home_IncomeVSExpendituresGraph, true, false, false, false) . '</tr>			
		     <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">The Organization:</span></td></tr>
			 <!--
		      <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Regional Hierarchy:</span></td></tr>
			   <tr><td width="35%">+ Region Types:</td>' . $this->GetEmployeeRolesOption('RegionTypes', $objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Regions:</td>' . $this->GetEmployeeRolesOption('Regions', $objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions) . '</tr>
			-->
		      <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Stations:</span></td></tr>			   
			   <tr bgcolor="#edeff1"><td>+ Stations:</td>' . $this->GetEmployeeRolesOption('Stations', $objEmployeeRoles->aEmployeeRole_Organization_Stations) . '</tr>
			   <tr><td>Station Map:</td>' . $this->GetEmployeeRolesOption('Organization_Stations_StationMap', $objEmployeeRoles->iEmployeeRole_Organization_Stations_StationMap, true, false, false, false) . '</tr>
		      <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Employees:</span></td></tr>
			   <tr bgcolor="#edeff1"><td>+ Employees:</td>' . $this->GetEmployeeRolesOption('Employees', $objEmployeeRoles->aEmployeeRole_Employees_Employees) . '</tr>
			   <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;+ Employee Roles:</td>' . $this->GetEmployeeRolesOption('EmployeeRoles', $objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeRoles) . '</tr>
			   <tr bgcolor="#edeff1"><td>&nbsp;&nbsp;&nbsp;&nbsp;+ Employee Documents:</td>' . $this->GetEmployeeRolesOption('EmployeeDocuments', $objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeDocuments) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Employee Types:</td>' . $this->GetEmployeeRolesOption('EmployeeTypes', $objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes) . '</tr>
			   <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;+ Employee Type Roles:</td>' . $this->GetEmployeeRolesOption('EmployeeTypeRoles', $objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes_EmployeeRoles) . '</tr>			   
			   <tr bgcolor="#edeff1"><td>+ Departments:</td>' . $this->GetEmployeeRolesOption('Departments', $objEmployeeRoles->aEmployeeRole_Organization_Departments) . '</tr>			   
  		      <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Data Backup:</span></td></tr>
			   <tr><td>+ Data Backup:</td>' . $this->GetEmployeeRolesOption('DataBackup', $objEmployeeRoles->aEmployeeRole_Organization_DataBackup, true, false, false, true) . '</tr>
  		      <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">System Settings:</span></td></tr>
			   <tr><td>+ System Settings:</td>' . $this->GetEmployeeRolesOption('SystemSettings', $objEmployeeRoles->aEmployeeRole_Organization_SystemSettings, true, false, false, false) . '</tr>
  		      <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">System Log:</span></td></tr>
			   <tr><td>+ System Log:</td>' . $this->GetEmployeeRolesOption('SystemLog', $objEmployeeRoles->aEmployeeRole_Organization_SystemLog, true, false, false, false) . '</tr>
			  <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Accounts:</span></td></tr>
			
			 <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Accounts:</span></td></tr>
			  <tr><td>+ Chart Of Accounts:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_ChartOfAccounts', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts) . '</tr>			 
			  <!--<tr><td>+ Chart Of Account Types:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_ChartOfAccountTypes', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccountTypes) . '</tr>-->
			  <tr bgcolor="#edeff1"><td>+ General Journal:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_GeneralJournals', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals) . '</tr>
			  <tr><td>&nbsp;&nbsp;+ General Journal Documents:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_GeneralJournal_Documents', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournal_Documents) . '</tr>
			  <tr bgcolor="#edeff1"><td>+ Quick Entries:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_QuickEntries', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries) . '</tr>
			  <tr><td>+ General Ledger:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_GeneralLedgers', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralLedgers, true, false, false,false) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Close Financial Year:</span></td></tr>
			  <tr><td>+ Close Financial Year:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_CloseFinancialYear', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Budget:</span></td></tr>			  
			  <tr><td>+ Budgets:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_Budgets', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets) . '</tr>
			  <tr bgcolor="#edeff1"><td>&nbsp;&nbsp;&nbsp;&nbsp;+ Change Budget Amount:</td>' . $this->GetEmployeeRolesOptionYesNo('FMS_Accounts_Budgets_ChangeBudgetAmount', $objEmployeeRoles->iEmployeeRole_FMS_Accounts_Budgets_ChangeBudgetAmount) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Budget Allocation:</span></td></tr>
			  <tr><td>+ Budget Allocations:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_BudgetAllocations', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations) . '</tr>
			 <!--
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Source Of Income:</span></td></tr>
			  <tr><td>+ Source Of Income:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_SourceOfIncome', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome) . '</tr>
			 -->
			  <tr bgcolor="#edeff1"><td>+ Donors:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_Donors', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors) . '</tr>
			  <tr><td>+ Donors Projects:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_Donors_Projects', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects) . '</tr>
			  <tr bgcolor="#edeff1"><td>+ Donors Projects Activities:</td>' . $this->GetEmployeeRolesOption('FMS_Accounts_Donors_ProjectsActivities', $objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities) . '</tr>
			 
			 <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Vendors:</span></td></tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Vendors:</span></td></tr>
			  <tr><td>+ Vendors:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors) . '</tr>
			  <tr bgcolor="#edeff1"><td>+ Vendor Types:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_VendorTypes', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes) . '</tr>
              <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Fixed Assets:</span></td></tr>
			   <tr bgcolor="#ffffff"><td>+ Fixed Assets:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_FixedAssets', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Fixed Asset Categories:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_FixedAssets_Categories', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories) . '</tr>
               <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Manufacturers:</span></td></tr>
			  <tr><td>+ Manufacturers:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_FixedAssets_Manufacturers', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Purchase Requests:</span></td></tr>
			   <tr bgcolor="#edeff1"><td>+ Purchase Requests:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_PurchaseRequests', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests) . '</tr>
			   <tr><td>&nbsp;&nbsp;+ Purchase Request Documents:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_PurchaseRequests_Documents', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests_Documents) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Quotations:</span></td></tr>
			   <tr bgcolor="#edeff1"><td>+ Quotations:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_Quotations', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations) . '</tr>
			   <tr><td>&nbsp;&nbsp;+ Quotation Documents:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_Quotations_Documents', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations_Documents) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Purchase Orders:</span></td></tr>
			   <tr bgcolor="#edeff1"><td>+ Purchase Orders:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_PurchaseOrders', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders) . '</tr>
			   <tr><td>&nbsp;&nbsp;+ Purchase Order Documents:</td>' . $this->GetEmployeeRolesOption('FMS_Vendors_PurchaseOrders_Documents', $objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders_Documents) . '</tr>
			 <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Banking:</span></td></tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Banks:</span></td></tr>
			  <tr><td>+ Banks:</td>' . $this->GetEmployeeRolesOption('FMS_Banking_Banks', $objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Bank Accounts:</span></td></tr>
			  <tr><td>+ Bank Accounts:</td>' . $this->GetEmployeeRolesOption('FMS_Banking_BankAccounts', $objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Bank Check Books:</span></td></tr>
			  <tr><td>+ Bank Check Books:</td>' . $this->GetEmployeeRolesOption('FMS_Banking_BankCheckBooks', $objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks) . '</tr>
			  <tr bgcolor="#edeff1"><td>&nbsp;&nbsp;+  Cancelled Checks:</td>' . $this->GetEmployeeRolesOption('FMS_Banking_BankCheckBooks_CancelledChecks', $objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Bank Deposits:</span></td></tr>
			  <tr><td>+ Bank Deposits:</td>' . $this->GetEmployeeRolesOption('FMS_Banking_BankDeposits', $objEmployeeRoles->aEmployeeRole_FMS_Banking_BankDeposits) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Bank Withdrawals:</span></td></tr>
			  <tr><td>+ Bank Withdrawals:</td>' . $this->GetEmployeeRolesOption('FMS_Banking_BankWithdrawals', $objEmployeeRoles->aEmployeeRole_FMS_Banking_BankWithdrawals) . '</tr>
			  <tr bgcolor="#edeff1"><td>+ Bank Reconciliation:</td>' . $this->GetEmployeeRolesOption('FMS_Banking_BankReconciliation', $objEmployeeRoles->iEmployeeRole_FMS_Banking_BankReconciliation, true, false, false, false) . '</tr>
				 		  
			  <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Customers Management:</span></td></tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Customers:</span></td></tr>
			  <tr><td>+ Customers:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_Customers', $objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Quotations:</span></td></tr>
			  <tr><td>+ Quotations:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_Quotations', $objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations) . '</tr>
			  <tr bgcolor="#edeff1"><td>&nbsp;&nbsp;+ Quotations Documents:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_Quotations_Documents', $objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations_Documents) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Invoices:</span></td></tr>
			  <tr><td>+ Invoices:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_Invoices', $objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices) . '</tr>
			  <tr bgcolor="#edeff1"><td>&nbsp;&nbsp;+ Invoice Documents:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_Invoices_Documents', $objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices_Documents) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Recurring Invoices:</span></td></tr>
			  <tr><td>+ Recurring Invoices:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_RecurringInvoices', $objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Sales Orders:</span></td></tr>
			  <tr><td>+ Sales Orders:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_SalesOrders', $objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders) . '</tr>
			  <tr bgcolor="#edeff1"><td>&nbsp;&nbsp;+ Sales Order Documents:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_SalesOrders_Documents', $objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders_Documents) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Receipts:</span></td></tr>
			  <tr><td>+ Receipts:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_Receipts', $objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts) . '</tr>
			  <tr bgcolor="#edeff1"><td>&nbsp;&nbsp;+ Receipt Documents:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_Receipts_Documents', $objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts_Documents) . '</tr>
			  <tr bgcolor="#d1d1d1"><td colspan="5"><span class="title3">Jobs:</span></td></tr>
			  <tr><td>+ Jobs:</td>' . $this->GetEmployeeRolesOption('FMS_Customers_Jobs', $objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs) . '</tr>
			  <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Reports:</span></td></tr>
			  <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Customer Reports:</span></td></tr>
		       <tr bgcolor="#edeff1"><td>+ Customer Reports:</td>' . $this->GetEmployeeRolesOption('CustomerReports', $objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports, true, false, false, false) . '</tr>
			   <tr><td>+ Customers Report:</td>' . $this->GetEmployeeRolesOption('CustomerReports_CustomersList', $objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersList, true, false, false, false) . '</tr>
			   <tr><td>+ Customers Ledger Report:</td>' . $this->GetEmployeeRolesOption('CustomerReports_CustomersLedgerReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersLedgerReport, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Quotations Report:</td>' . $this->GetEmployeeRolesOption('CustomerReports_QuotationsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_QuotationsReport, true, false, false, false) . '</tr>		   
			   <tr><td>+ Sales Orders Report:</td>' . $this->GetEmployeeRolesOption('CustomerReports_SalesOrdersReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_SalesOrdersReport, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Invoices Report:</td>' . $this->GetEmployeeRolesOption('CustomerReports_InvoicesReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_InvoicesReport, true, false, false, false) . '</tr>
			   <tr><td>+ Receipts Report:</td>' . $this->GetEmployeeRolesOption('CustomerReports_ReceiptsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_ReceiptsReport, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Jobs Report:</td>' . $this->GetEmployeeRolesOption('CustomerReports_JobsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_JobsReport, true, false, false, false) . '</tr>
			  
			  <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Accounts Reports:</span></td></tr>
			   <tr bgcolor="#edeff1"><td>+ Accounts Reports:</td>' . $this->GetEmployeeRolesOption('AccountsReports', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Chart Of Accounts Report:</td>' . $this->GetEmployeeRolesOption('AccountsReports_ChartOfAccountsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ChartOfAccountsReport, true, false, false, false) . '</tr>
			   <tr><td>+ General Journal Report:</td>' . $this->GetEmployeeRolesOption('AccountsReports_GeneralJournalReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralJournalReport, true, false, false, false) . '</tr>
			   <tr><td>+ General Journal Receipt:</td>' . $this->GetEmployeeRolesOption('AccountsReports_GeneralJournalReceipt', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralJournalReceipt, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ General Ledger Report:</td>' . $this->GetEmployeeRolesOption('AccountsReports_GeneralLedgerReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerReport, true, false, false, false) . '</tr>		   
			   <tr><td>+ General Ledger Activity Report:</td>' . $this->GetEmployeeRolesOption('AccountsReports_GeneralLedgerActivityReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerActivityReport, true, false, false, false) . '</tr>
			   <tr><td>+ General Ledger Activity Date Wise Report:</td>' . $this->GetEmployeeRolesOption('AccountsReports_GeneralLedgerActivityDateWiseReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerActivityDateWiseReport, true, false, false, false) . '</tr>
			   <tr><td>+ Budget List Report:</td>' . $this->GetEmployeeRolesOption('AccountsReports_BudgetListReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetListReport, true, false, false, false) . '</tr>
			   <tr><td>+ Budget Allocation Report:</td>' . $this->GetEmployeeRolesOption('AccountsReports_BudgetAllocationReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetAllocationReport, true, false, false, false) . '</tr>
			   <tr><td>+ Budget Utilization Report:</td>' . $this->GetEmployeeRolesOption('AccountsReports_BudgetUtilizationReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetUtilizationReport, true, false, false, false) . '</tr>
					
			  <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Vendor Reports:</span></td></tr>
			   <tr bgcolor="#edeff1"><td>+ Vendor Reports:</td>' . $this->GetEmployeeRolesOption('VendorReports', $objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports, true, false, false, false) . '</tr>
			   <tr><td>+ Vendors Report:</td>' . $this->GetEmployeeRolesOption('VendorReports_VendorsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_VendorsReport, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Fixed Assets Report:</td>' . $this->GetEmployeeRolesOption('VendorReports_FixedAssetsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetsReport, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Fixed Asset Barcodes Report:</td>' . $this->GetEmployeeRolesOption('VendorReports_FixedAssetBarcodesReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetBarcodesReport, true, false, false, false) . '</tr>
			   <tr><td>+ Vendors Ledger Report:</td>' . $this->GetEmployeeRolesOption('VendorReports_VendorsLedgerReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_VendorsLedgerReport, true, false, false, false) . '</tr> 
			   <tr bgcolor="#edeff1"><td>+ Purchase Requests Report:</td>' . $this->GetEmployeeRolesOption('VendorReports_PurchaseRequestsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_PurchaseRequestsReport, true, false, false, false) . '</tr>		   
			   <tr><td>+ Quotations Report:</td>' . $this->GetEmployeeRolesOption('VendorReports_QuotationsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_QuotationsReport, true, false, false, false) . '</tr>
			   <tr><td>+ Fixed Asset Depreciation Report:</td>' . $this->GetEmployeeRolesOption('VendorReports_FixedAssetDepreciationReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetDepreciationReport, true, false, false, false) . '</tr>			  
			  <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Banking Reports:</span></td></tr>
			   <tr bgcolor="#edeff1"><td>+ Banking Reports:</td>' . $this->GetEmployeeRolesOption('BankingReports', $objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports, true, false, false, false) . '</tr>
			   <tr><td>+ Bank Ledger Report:</td>' . $this->GetEmployeeRolesOption('BankingReports_BankLedgerReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankLedgerReport, true, false, false, false) . '</tr>
			   <tr><td>+ Bank Deposits Report:</td>' . $this->GetEmployeeRolesOption('BankingReports_BankDepositsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Bank Withdrawals Report:</td>' . $this->GetEmployeeRolesOption('BankingReports_BankWithdrawalsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankWithdrawalsReport, true, false, false, false) . '</tr>
			   <tr><td>+ Bank Reconciliation Report:</td>' . $this->GetEmployeeRolesOption('BankingReports_BankReconciliationReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankReconciliationReport, true, false, false, false) . '</tr>
			   <tr><td>+ Bank Check Books Report:</td>' . $this->GetEmployeeRolesOption('BankingReports_BankCheckBooksReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankCheckBooksReport, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Bank Accounts Report:</td>' . $this->GetEmployeeRolesOption('BankingReports_BankAccountsReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankAccountsReport, true, false, false, false) . '</tr>
			   <tr><td>+ Banks Report:</td>' . $this->GetEmployeeRolesOption('BankingReports_BanksReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport, true, false, false, false) . '</tr>			   
			
			  <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Financial Reports:</span></td></tr>
			   <tr bgcolor="#edeff1"><td>+ Financial Reports:</td>' . $this->GetEmployeeRolesOption('FinancialReports', $objEmployeeRoles->aEmployeeRole_FMS_Reports_FinancialReports, true, false, false, false) . '</tr>
			   <!--<tr><td>+ Bank Statement Report:</td>' . $this->GetEmployeeRolesOption('FinancialReports_BankLedgerReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankLedgerReport, true, false, false, false) . '</tr>-->
			   <tr><td>+ Balance Sheet Report:</td>' . $this->GetEmployeeRolesOption('FinancialReports_BalanceSheetReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_FinancialReports_BalanceSheetReport, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Income Statement Report:</td>' . $this->GetEmployeeRolesOption('FinancialReports_IncomeStatementReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_FinancialReports_IncomeStatementReport, true, false, false, false) . '</tr>
			   <tr><td>+ Cash Flow Report:</td>' . $this->GetEmployeeRolesOption('FinancialReports_CashFlowReport', $objEmployeeRoles->aEmployeeRole_FMS_Reports_FinancialReports_CashFlowReport, true, false, false, false) . '</tr>
			  
			  <tr class="Details_Title_TR"><td colspan="5"><span class="Details_Title">Graphs:</span></td></tr>
			   <tr bgcolor="#edeff1"><td>+ Graphs:</td>' . $this->GetEmployeeRolesOption('Graphs', $objEmployeeRoles->aEmployeeRole_FMS_Graphs, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Income VS Expenditure:</td>' . $this->GetEmployeeRolesOption('Graphs_IncomeVSExpenditure', $objEmployeeRoles->aEmployeeRole_FMS_Graphs_IncomeVSExpenditure, true, false, false, false) . '</tr>
			   <tr bgcolor="#edeff1"><td>+ Sales Graph:</td>' . $this->GetEmployeeRolesOption('Graphs_SalesGraph', $objEmployeeRoles->aEmployeeRole_FMS_Graphs_SalesGraph, true, false, false, false) . '</tr>
			
			  <tr class="Details_Title_TR"><td colspan="5"><span class="WhiteHeading">Other Roles</span></td></tr>
			   <tr><td>+ Employee has access on All Chart Of Accounts:</td>' . $this->GetEmployeeRolesOptionYesNo('EmployeeAccessOnAllChartOfAccounts', $objEmployeeRoles->iEmployeeRole_Employees_EmployeeChartOfAccountAccess) . '</tr>
		       <tr><td>+ Employee has access on All Projects:</td>' . $this->GetEmployeeRolesOptionYesNo('EmployeeAccessOnAllProjects', $objEmployeeRoles->iEmployeeRole_FMS_Accounts_AccessAllProjects) . '</tr>
		       <tr><td>+ Employee has access on All Station:</td>' . $this->GetEmployeeRolesOptionYesNo('EmployeeAccessOnAllBranches', $objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess) . '</tr>
		       <!--<tr bgcolor="#edeff1"><td>+ Employee has access on All Regions:</td>' . $this->GetEmployeeRolesOptionYesNo('EmployeeAccessOnAllRegions', $objEmployeeRoles->iEmployeeRole_Employees_EmployeeRegionAccess) . '</tr>-->
		     <tr><td colspan="5" align="center"><br /><input type="submit" class="AdminFormButton1" value="Save Roles" /></td></tr>
			<input type="hidden" name="action" id="action" value="Set' . $sType . '"><input type="hidden" name="id" id="id" value="' . $iId . '">
		     </form>
			' . $sCopyRoles . '
			</table><a name="End" />';

		return($sReturn);
	}

	function ShowEmployeeRoles($iEmployeeId)
	{
		global $objDatabase;
		global $objEncryption;

		$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId='$iEmployeeId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Id');

		$sEmployeeUserName = $objDatabase->Result($varResult, 0, "E.UserName");
		$sEmployeeRoles = $objDatabase->Result($varResult, 0, "E.EmployeeRoles");

		$objEmployeeRoles = new clsEmployeeRoles();
		//$objEmployeeRoles = unserialize($objEncryption->fnDecrypt(cEncryptionKey, $sEmployeeRoles));
		$objEmployeeRoles = unserialize($sEmployeeRoles);
		
		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		
		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Employee Roles for ' . $sEmployeeUserName . '</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">
		<form method="post" action="">';

		$sReturn .= $this->EmployeeRolesValues($iEmployeeId, $objEmployeeRoles);

		$sReturn .= '
		<input type="hidden" name="action" id="action" value="SetEmployeeRoles"><input type="hidden" name="id" id="id" value="' . $iEmployeeId . '"></form>
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}

	function SetEmployeeRoles($iEmployeeId)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEncryption;

		// ToDo: Uncomment this
	    // if ($iEmployeeId == 1) return(2110);

	    $sRoles = $this->SaveEmployeeRoles();
        $varResult = $objDatabase->Query("UPDATE organization_employees SET EmployeeRoles='$sRoles' WHERE EmployeeId='$iEmployeeId'");

		return(2108);
	}
	
	function CopyRoles($sCopyRoleTo, $sCopyRoleFrom, $iId, $iCopyFromId)
	{
		global $objDatabase;
				
		if ($sCopyRoleTo == "EmployeeType")
		{
			if ($sCopyRoleFrom == "EmployeeType")
			{
				$varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.EmployeeTypeId='$iCopyFromId'");
				if ($objDatabase->RowsNumber($varResult) > 0)
					$sRoles = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeRoles");
			}
			else
			{
				$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId='$iCopyFromId'");
				if ($objDatabase->RowsNumber($varResult) > 0)
					$sRoles = $objDatabase->Result($varResult, 0, "E.EmployeeRoles");
			}

			$varResult = $objDatabase->Query("UPDATE organization_employees_types SET EmployeeTypeRoles='$sRoles' WHERE EmployeeTypeId='$iId'");
		}
		else
		{
			if ($sCopyRoleFrom == "EmployeeType")
			{
				$varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.EmployeeTypeId='$iCopyFromId'");
				if ($objDatabase->RowsNumber($varResult) > 0)
					$sRoles = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeRoles");
			}
			else
			{
				$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId='$iCopyFromId'");
				if ($objDatabase->RowsNumber($varResult) > 0)
					$sRoles = $objDatabase->Result($varResult, 0, "E.EmployeeRoles");
			}

			$varResult = $objDatabase->Query("UPDATE organization_employees SET EmployeeRoles='$sRoles' WHERE EmployeeId='$iId'");
		}

		return(2023);
	}
	
	function SaveEmployeeRoles()
	{
		global $objGeneral;
		global $objEncryption;

		$objEmployeeRoles = new clsEmployeeRoles();		
		
		/* Home */
		$objEmployeeRoles->iEmployeeRole_Home_BankAccounts[0] = ($objGeneral->fnGet("BankAccounts_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->iEmployeeRole_Home_ChartOfAccounts[0] = ($objGeneral->fnGet("ChartOfAccounts_View") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->iEmployeeRole_Home_CustomerOutStandingInvoices[0] = ($objGeneral->fnGet("CustomerOutStandingInvoices_View") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->iEmployeeRole_Home_IncomeVSExpendituresGraph[0] = ($objGeneral->fnGet("IncomeVSExpendituresGraph_View") == "on") ? 1 : 0; // Delete

		/* The Organization */
		
		// Regions
		$objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[0] = ($objGeneral->fnGet("RegionTypes_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[1] = ($objGeneral->fnGet("RegionTypes_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[2] = ($objGeneral->fnGet("RegionTypes_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[3] = ($objGeneral->fnGet("RegionTypes_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[0] = ($objGeneral->fnGet("Regions_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[1] = ($objGeneral->fnGet("Regions_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[2] = ($objGeneral->fnGet("Regions_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[3] = ($objGeneral->fnGet("Regions_Delete") == "on") ? 1 : 0; // Delete
		
		$objEmployeeRoles->aEmployeeRole_Organization_Stations[0] = ($objGeneral->fnGet("Stations_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Organization_Stations[1] = ($objGeneral->fnGet("Stations_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Organization_Stations[2] = ($objGeneral->fnGet("Stations_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Organization_Stations[3] = ($objGeneral->fnGet("Stations_Delete") == "on") ? 1 : 0; // Delete		
		
		$objEmployeeRoles->iEmployeeRole_Organization_Stations_StationMap[0] = ($objGeneral->fnGet("Organization_Stations_StationMap_View") == "on") ? 1 : 0; // View
		
		$objEmployeeRoles->aEmployeeRole_Employees_Employees[0] = ($objGeneral->fnGet("Employees_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Employees_Employees[1] = ($objGeneral->fnGet("Employees_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Employees_Employees[2] = ($objGeneral->fnGet("Employees_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Employees_Employees[3] = ($objGeneral->fnGet("Employees_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeDocuments[0] = ($objGeneral->fnGet("EmployeeDocuments_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeDocuments[1] = ($objGeneral->fnGet("EmployeeDocuments_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeDocuments[2] = ($objGeneral->fnGet("EmployeeDocuments_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeDocuments[3] = ($objGeneral->fnGet("EmployeeDocuments_Delete") == "on") ? 1 : 0; // Delete
		
		$objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeRoles[0] = ($objGeneral->fnGet("EmployeeRoles_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeRoles[1] = ($objGeneral->fnGet("EmployeeRoles_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeRoles[2] = ($objGeneral->fnGet("EmployeeRoles_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Employees_Employees_EmployeeRoles[3] = ($objGeneral->fnGet("EmployeeRoles_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[0] = ($objGeneral->fnGet("EmployeeTypes_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[1] = ($objGeneral->fnGet("EmployeeTypes_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[2] = ($objGeneral->fnGet("EmployeeTypes_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[3] = ($objGeneral->fnGet("EmployeeTypes_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes_EmployeeRoles[0] = ($objGeneral->fnGet("EmployeeTypeRoles_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes_EmployeeRoles[1] = ($objGeneral->fnGet("EmployeeTypeRoles_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes_EmployeeRoles[2] = ($objGeneral->fnGet("EmployeeTypeRoles_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes_EmployeeRoles[3] = ($objGeneral->fnGet("EmployeeTypeRoles_Delete") == "on") ? 1 : 0; // Delete
		
		$objEmployeeRoles->aEmployeeRole_Organization_Departments[0] = ($objGeneral->fnGet("Departments_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Organization_Departments[1] = ($objGeneral->fnGet("Departments_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Organization_Departments[2] = ($objGeneral->fnGet("Departments_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Organization_Departments[3] = ($objGeneral->fnGet("Departments_Delete") == "on") ? 1 : 0; // Delete
		
		$objEmployeeRoles->aEmployeeRole_Organization_DataBackup[0] = ($objGeneral->fnGet("DataBackup_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Organization_DataBackup[3] = ($objGeneral->fnGet("DataBackup_Delete") == "on") ? 1 : 0; // Delete
		$objEmployeeRoles->aEmployeeRole_Organization_SystemSettings[0] = ($objGeneral->fnGet("SystemSettings_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Organization_SystemLog[0] = ($objGeneral->fnGet("SystemLog_View") == "on") ? 1 : 0; // View
		/* End of The Organizaion */

		
		//$objEmployeeRoles->aEmployeeRole_Reports_StandardReports[0] = ($objGeneral->fnGet("StandardReports_View") == "on") ? 1 : 0; // View

		
		/* Accounts */
		// Chart of Accounts
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[0] = ($objGeneral->fnGet("FMS_Accounts_ChartOfAccounts_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[1] = ($objGeneral->fnGet("FMS_Accounts_ChartOfAccounts_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[2] = ($objGeneral->fnGet("FMS_Accounts_ChartOfAccounts_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[3] = ($objGeneral->fnGet("FMS_Accounts_ChartOfAccounts_Delete") == "on") ? 1 : 0; // Delete
		
		// Chart of Account Types
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccountTypes[0] = ($objGeneral->fnGet("FMS_Accounts_ChartOfAccountTypes_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccountTypes[1] = ($objGeneral->fnGet("FMS_Accounts_ChartOfAccountTypes_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccountTypes[2] = ($objGeneral->fnGet("FMS_Accounts_ChartOfAccountTypes_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Accounts_ChartOfAccountTypes[3] = ($objGeneral->fnGet("FMS_Accounts_ChartOfAccountTypes_Delete") == "on") ? 1 : 0; // Delete
		
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[0] = ($objGeneral->fnGet("FMS_Accounts_GeneralJournals_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[1] = ($objGeneral->fnGet("FMS_Accounts_GeneralJournals_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[2] = ($objGeneral->fnGet("FMS_Accounts_GeneralJournals_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[3] = ($objGeneral->fnGet("FMS_Accounts_GeneralJournals_Delete") == "on") ? 1 : 0; // Delete
		
		// Quick Entries
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[0] = ($objGeneral->fnGet("FMS_Accounts_QuickEntries_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[1] = ($objGeneral->fnGet("FMS_Accounts_QuickEntries_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[2] = ($objGeneral->fnGet("FMS_Accounts_QuickEntries_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[3] = ($objGeneral->fnGet("FMS_Accounts_QuickEntries_Delete") == "on") ? 1 : 0; // Delete
		
		
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournal_Documents[0] = ($objGeneral->fnGet("FMS_Accounts_GeneralJournal_Documents_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournal_Documents[1] = ($objGeneral->fnGet("FMS_Accounts_GeneralJournal_Documents_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournal_Documents[2] = ($objGeneral->fnGet("FMS_Accounts_GeneralJournal_Documents_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournal_Documents[3] = ($objGeneral->fnGet("FMS_Accounts_GeneralJournal_Documents_Delete") == "on") ? 1 : 0; // Delete
		
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralLedgers[0] = ($objGeneral->fnGet("FMS_Accounts_GeneralLedgers_View") == "on") ? 1 : 0; // View
		
		// Close Financial Year
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear[0] = ($objGeneral->fnGet("FMS_Accounts_CloseFinancialYear_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear[1] = ($objGeneral->fnGet("FMS_Accounts_CloseFinancialYear_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear[2] = ($objGeneral->fnGet("FMS_Accounts_CloseFinancialYear_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear[3] = ($objGeneral->fnGet("FMS_Accounts_CloseFinancialYear_Delete") == "on") ? 1 : 0; // Delete
		
		
		// Source Of Income
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[0] = ($objGeneral->fnGet("FMS_Accounts_SourceOfIncome_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[1] = ($objGeneral->fnGet("FMS_Accounts_SourceOfIncome_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[2] = ($objGeneral->fnGet("FMS_Accounts_SourceOfIncome_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[3] = ($objGeneral->fnGet("FMS_Accounts_SourceOfIncome_Delete") == "on") ? 1 : 0; // Delete
		
		// Donors
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[0] = ($objGeneral->fnGet("FMS_Accounts_Donors_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[1] = ($objGeneral->fnGet("FMS_Accounts_Donors_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[2] = ($objGeneral->fnGet("FMS_Accounts_Donors_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[3] = ($objGeneral->fnGet("FMS_Accounts_Donors_Delete") == "on") ? 1 : 0; // Delete
		
		// Donors Projects
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[0] = ($objGeneral->fnGet("FMS_Accounts_Donors_Projects_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[1] = ($objGeneral->fnGet("FMS_Accounts_Donors_Projects_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[2] = ($objGeneral->fnGet("FMS_Accounts_Donors_Projects_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[3] = ($objGeneral->fnGet("FMS_Accounts_Donors_Projects_Delete") == "on") ? 1 : 0; // Delete
		
		// Donors Projects Accounts
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[0] = ($objGeneral->fnGet("FMS_Accounts_Donors_ProjectsActivities_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[1] = ($objGeneral->fnGet("FMS_Accounts_Donors_ProjectsActivities_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[2] = ($objGeneral->fnGet("FMS_Accounts_Donors_ProjectsActivities_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[3] = ($objGeneral->fnGet("FMS_Accounts_Donors_ProjectsActivities_Delete") == "on") ? 1 : 0; // Delete
			
		// Budget
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[0] = ($objGeneral->fnGet("FMS_Accounts_Budgets_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[1] = ($objGeneral->fnGet("FMS_Accounts_Budgets_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[2] = ($objGeneral->fnGet("FMS_Accounts_Budgets_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[3] = ($objGeneral->fnGet("FMS_Accounts_Budgets_Delete") == "on") ? 1 : 0; // Delete
		$objEmployeeRoles->iEmployeeRole_FMS_Accounts_Budgets_ChangeBudgetAmount = ($objGeneral->fnGet("FMS_Accounts_Budgets_ChangeBudgetAmount")); // Increase
		$objEmployeeRoles->iEmployeeRole_FMS_Accounts_Budgets_Decrease[0] = ($objGeneral->fnGet("FMS_Accounts_Budgets_Decrease_Add") == "on") ? 1 : 0; // Decrease
		
		// Budget Allocation
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[0] = ($objGeneral->fnGet("FMS_Accounts_BudgetAllocations_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[1] = ($objGeneral->fnGet("FMS_Accounts_BudgetAllocations_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[2] = ($objGeneral->fnGet("FMS_Accounts_BudgetAllocations_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[3] = ($objGeneral->fnGet("FMS_Accounts_BudgetAllocations_Delete") == "on") ? 1 : 0; // Delete
		

		/* End of Accounts */

     	/* Vendors */
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[0] = ($objGeneral->fnGet("FMS_Vendors_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[1] = ($objGeneral->fnGet("FMS_Vendors_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[2] = ($objGeneral->fnGet("FMS_Vendors_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[3] = ($objGeneral->fnGet("FMS_Vendors_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[0] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[1] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[2] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[3] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[0] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Categories_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[1] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Categories_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[2] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Categories_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[3] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Categories_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[0] = ($objGeneral->fnGet("FMS_Vendors_VendorTypes_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[1] = ($objGeneral->fnGet("FMS_Vendors_VendorTypes_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[2] = ($objGeneral->fnGet("FMS_Vendors_VendorTypes_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[3] = ($objGeneral->fnGet("FMS_Vendors_VendorTypes_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[0] = ($objGeneral->fnGet("FMS_Vendors_PurchaseOrders_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[1] = ($objGeneral->fnGet("FMS_Vendors_PurchaseOrders_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[2] = ($objGeneral->fnGet("FMS_Vendors_PurchaseOrders_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[3] = ($objGeneral->fnGet("FMS_Vendors_PurchaseOrders_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders_Documents[0] = ($objGeneral->fnGet("FMS_Vendors_PurchaseOrders_Documents_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders_Documents[1] = ($objGeneral->fnGet("FMS_Vendors_PurchaseOrders_Documents_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders_Documents[2] = ($objGeneral->fnGet("FMS_Vendors_PurchaseOrders_Documents_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders_Documents[3] = ($objGeneral->fnGet("FMS_Vendors_PurchaseOrders_Documents_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[0] = ($objGeneral->fnGet("FMS_Vendors_PurchaseRequests_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[1] = ($objGeneral->fnGet("FMS_Vendors_PurchaseRequests_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[2] = ($objGeneral->fnGet("FMS_Vendors_PurchaseRequests_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[3] = ($objGeneral->fnGet("FMS_Vendors_PurchaseRequests_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests_Documents[0] = ($objGeneral->fnGet("FMS_Vendors_PurchaseRequests_Documents_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests_Documents[1] = ($objGeneral->fnGet("FMS_Vendors_PurchaseRequests_Documents_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests_Documents[2] = ($objGeneral->fnGet("FMS_Vendors_PurchaseRequests_Documents_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests_Documents[3] = ($objGeneral->fnGet("FMS_Vendors_PurchaseRequests_Documents_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[0] = ($objGeneral->fnGet("FMS_Vendors_Quotations_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[1] = ($objGeneral->fnGet("FMS_Vendors_Quotations_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[2] = ($objGeneral->fnGet("FMS_Vendors_Quotations_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[3] = ($objGeneral->fnGet("FMS_Vendors_Quotations_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations_Documents[0] = ($objGeneral->fnGet("FMS_Vendors_Quotations_Documents_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations_Documents[1] = ($objGeneral->fnGet("FMS_Vendors_Quotations_Documents_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations_Documents[2] = ($objGeneral->fnGet("FMS_Vendors_Quotations_Documents_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations_Documents[3] = ($objGeneral->fnGet("FMS_Vendors_Quotations_Documents_Delete") == "on") ? 1 : 0; // Delete
		/* End of Vendors */

        /* Manufacturers */
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[0] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Manufacturers_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[1] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Manufacturers_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[2] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Manufacturers_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[3] = ($objGeneral->fnGet("FMS_Vendors_FixedAssets_Manufacturers_Delete") == "on") ? 1 : 0; // Delete
        /*End of Manufacturers */

     	/* FixedAssets */
		$objEmployeeRoles->aEmployeeRole_Organization_FixedAssets[0] = ($objGeneral->fnGet("FixedAssets_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Organization_FixedAssets[1] = ($objGeneral->fnGet("FixedAssets_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Organization_FixedAssets[2] = ($objGeneral->fnGet("FixedAssets_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Organization_FixedAssets[3] = ($objGeneral->fnGet("FixedAssets_Delete") == "on") ? 1 : 0; // Delete

		$objEmployeeRoles->aEmployeeRole_Organization_FixedAssets_Categories[0] = ($objGeneral->fnGet("FixedAssets_Categories_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Organization_FixedAssets_Categories[1] = ($objGeneral->fnGet("FixedAssets_Categories_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Organization_FixedAssets_Categories[2] = ($objGeneral->fnGet("FixedAssets_Categories_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Organization_FixedAssets_Categories[3] = ($objGeneral->fnGet("FixedAssets_Categories_Delete") == "on") ? 1 : 0; // Delete
     	/* End of FixedAssets */
     	
     	/* Services */
		$objEmployeeRoles->aEmployeeRole_Organization_Services[0] = ($objGeneral->fnGet("Services_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_Organization_Services[1] = ($objGeneral->fnGet("Services_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_Organization_Services[2] = ($objGeneral->fnGet("Services_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_Organization_Services[3] = ($objGeneral->fnGet("Services_Delete") == "on") ? 1 : 0; // Delete
     	/* End of Services */
     	
		/* Banking section */
     	/* Banks */
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[0] = ($objGeneral->fnGet("FMS_Banking_Banks_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[1] = ($objGeneral->fnGet("FMS_Banking_Banks_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[2] = ($objGeneral->fnGet("FMS_Banking_Banks_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[3] = ($objGeneral->fnGet("FMS_Banking_Banks_Delete") == "on") ? 1 : 0; // Delete
     	/* End of Banks */
     	
     	/* Bank Accounts */
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[0] = ($objGeneral->fnGet("FMS_Banking_BankAccounts_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[1] = ($objGeneral->fnGet("FMS_Banking_BankAccounts_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[2] = ($objGeneral->fnGet("FMS_Banking_BankAccounts_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[3] = ($objGeneral->fnGet("FMS_Banking_BankAccounts_Delete") == "on") ? 1 : 0; // Delete
     	/* End of Bank Accounts */
     	
     	/* Bank Check Books */
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[0] = ($objGeneral->fnGet("FMS_Banking_BankCheckBooks_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[1] = ($objGeneral->fnGet("FMS_Banking_BankCheckBooks_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[2] = ($objGeneral->fnGet("FMS_Banking_BankCheckBooks_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[3] = ($objGeneral->fnGet("FMS_Banking_BankCheckBooks_Delete") == "on") ? 1 : 0; // Delete
     	/* End of Banks Checks*/
     	
		/* Cancelled Checks*/
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[0] = ($objGeneral->fnGet("FMS_Banking_BankCheckBooks_CancelledChecks_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[1] = ($objGeneral->fnGet("FMS_Banking_BankCheckBooks_CancelledChecks_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[2] = ($objGeneral->fnGet("FMS_Banking_BankCheckBooks_CancelledChecks_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[3] = ($objGeneral->fnGet("FMS_Banking_BankCheckBooks_CancelledChecks_Delete") == "on") ? 1 : 0; // Delete
     	/* End of Banks Checks*/
		
		/* Bank Deposits */
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankDeposits[0] = ($objGeneral->fnGet("FMS_Banking_BankDeposits_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankDeposits[1] = ($objGeneral->fnGet("FMS_Banking_BankDeposits_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankDeposits[2] = ($objGeneral->fnGet("FMS_Banking_BankDeposits_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankDeposits[3] = ($objGeneral->fnGet("FMS_Banking_BankDeposits_Delete") == "on") ? 1 : 0; // Delete
     	/* End of Banks Deposits*/
     	
		/* Bank Withdrawals */
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankWithdrawals[0] = ($objGeneral->fnGet("FMS_Banking_BankWithdrawals_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankWithdrawals[1] = ($objGeneral->fnGet("FMS_Banking_BankWithdrawals_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankWithdrawals[2] = ($objGeneral->fnGet("FMS_Banking_BankWithdrawals_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Banking_BankWithdrawals[3] = ($objGeneral->fnGet("FMS_Banking_BankWithdrawals_Delete") == "on") ? 1 : 0; // Delete
     	/* End of Banks Withdrawals*/
		/* Bank Reconciliation */
		
		$objEmployeeRoles->iEmployeeRole_FMS_Banking_BankReconciliation[0] = ($objGeneral->fnGet("FMS_Banking_BankReconciliation_View") == "on") ? 1 : 0; // View
     	/* Cutomers */
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[0] = ($objGeneral->fnGet("FMS_Customers_Customers_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[1] = ($objGeneral->fnGet("FMS_Customers_Customers_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[2] = ($objGeneral->fnGet("FMS_Customers_Customers_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[3] = ($objGeneral->fnGet("FMS_Customers_Customers_Delete") == "on") ? 1 : 0; // Delete
		/* End of Cutomers */
		
		/* Quotations */
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[0] = ($objGeneral->fnGet("FMS_Customers_Quotations_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[1] = ($objGeneral->fnGet("FMS_Customers_Quotations_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[2] = ($objGeneral->fnGet("FMS_Customers_Quotations_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[3] = ($objGeneral->fnGet("FMS_Customers_Quotations_Delete") == "on") ? 1 : 0; // Delete
		/* End of Quotations */
		
		/* Quotations Documents*/
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations_Documents[0] = ($objGeneral->fnGet("FMS_Customers_Quotations_Documents_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations_Documents[1] = ($objGeneral->fnGet("FMS_Customers_Quotations_Documents_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations_Documents[2] = ($objGeneral->fnGet("FMS_Customers_Quotations_Documents_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations_Documents[3] = ($objGeneral->fnGet("FMS_Customers_Quotations_Documents_Delete") == "on") ? 1 : 0; // Delete
		/* End of Quotations Documents*/
		
		/* Invoices */
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[0] = ($objGeneral->fnGet("FMS_Customers_Invoices_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[1] = ($objGeneral->fnGet("FMS_Customers_Invoices_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[2] = ($objGeneral->fnGet("FMS_Customers_Invoices_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[3] = ($objGeneral->fnGet("FMS_Customers_Invoices_Delete") == "on") ? 1 : 0; // Delete
		/* End of Invoices */
		
		/* Invoices Documents*/
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices_Documents[0] = ($objGeneral->fnGet("FMS_Customers_Invoices_Documents_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices_Documents[1] = ($objGeneral->fnGet("FMS_Customers_Invoices_Documents_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices_Documents[2] = ($objGeneral->fnGet("FMS_Customers_Invoices_Documents_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices_Documents[3] = ($objGeneral->fnGet("FMS_Customers_Invoices_Documents_Delete") == "on") ? 1 : 0; // Delete
		/* End of Invoices Documents */
		
		/* Recurring Invoices */
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[0] = ($objGeneral->fnGet("FMS_Customers_RecurringInvoices_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[1] = ($objGeneral->fnGet("FMS_Customers_RecurringInvoices_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[2] = ($objGeneral->fnGet("FMS_Customers_RecurringInvoices_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[3] = ($objGeneral->fnGet("FMS_Customers_RecurringInvoices_Delete") == "on") ? 1 : 0; // Delete
		/* End of Invoices */
		
		/* Sales Orders */
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[0] = ($objGeneral->fnGet("FMS_Customers_SalesOrders_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[1] = ($objGeneral->fnGet("FMS_Customers_SalesOrders_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[2] = ($objGeneral->fnGet("FMS_Customers_SalesOrders_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[3] = ($objGeneral->fnGet("FMS_Customers_SalesOrders_Delete") == "on") ? 1 : 0; // Delete
		/* End of Sales Orders */
		
		/* Sales Orders Documents*/
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders_Documents[0] = ($objGeneral->fnGet("FMS_Customers_SalesOrders_Documents_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders_Documents[1] = ($objGeneral->fnGet("FMS_Customers_SalesOrders_Documents_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders_Documents[2] = ($objGeneral->fnGet("FMS_Customers_SalesOrders_Documents_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders_Documents[3] = ($objGeneral->fnGet("FMS_Customers_SalesOrders_Documents_Delete") == "on") ? 1 : 0; // Delete
		/* End of Sales Orders  Documents*/
		
		/* Receipts */
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[0] = ($objGeneral->fnGet("FMS_Customers_Receipts_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[1] = ($objGeneral->fnGet("FMS_Customers_Receipts_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[2] = ($objGeneral->fnGet("FMS_Customers_Receipts_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[3] = ($objGeneral->fnGet("FMS_Customers_Receipts_Delete") == "on") ? 1 : 0; // Delete
		/* End of Receipts */
		
		/* Receipts Documents*/
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts_Documents[0] = ($objGeneral->fnGet("FMS_Customers_Receipts_Documents_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts_Documents[1] = ($objGeneral->fnGet("FMS_Customers_Receipts_Documents_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts_Documents[2] = ($objGeneral->fnGet("FMS_Customers_Receipts_Documents_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts_Documents[3] = ($objGeneral->fnGet("FMS_Customers_Receipts_Documents_Delete") == "on") ? 1 : 0; // Delete
		/* End of Receipts Documents*/
		
		/* Jobs */
     	$objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[0] = ($objGeneral->fnGet("FMS_Customers_Jobs_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[1] = ($objGeneral->fnGet("FMS_Customers_Jobs_Add") == "on") ? 1 : 0; // Add
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[2] = ($objGeneral->fnGet("FMS_Customers_Jobs_Update") == "on") ? 1 : 0; // Update
		$objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[3] = ($objGeneral->fnGet("FMS_Customers_Jobs_Delete") == "on") ? 1 : 0; // Delete
		/* End of Jobs */
		
		/* Reports */
		// Customer Reports
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports[0] = ($objGeneral->fnGet("CustomerReports_View") == "on") ? 1 : 0; // View 
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersList[0] = ($objGeneral->fnGet("CustomerReports_CustomersList_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersLedgerReport[0] = ($objGeneral->fnGet("CustomerReports_CustomersLedgerReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_QuotationsReport[0] = ($objGeneral->fnGet("CustomerReports_QuotationsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_SalesOrdersReport[0] = ($objGeneral->fnGet("CustomerReports_SalesOrdersReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_InvoicesReport[0] = ($objGeneral->fnGet("CustomerReports_InvoicesReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_ReceiptsReport[0] = ($objGeneral->fnGet("CustomerReports_ReceiptsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_JobsReport[0] = ($objGeneral->fnGet("CustomerReports_JobsReport_View") == "on") ? 1 : 0; // View
		// End of Customer Reports
		
		// Vendor Reports
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports[0] = ($objGeneral->fnGet("VendorReports_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_VendorsReport[0] = ($objGeneral->fnGet("VendorReports_VendorsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetsReport[0] = ($objGeneral->fnGet("VendorReports_FixedAssetsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetBarcodesReport[0] = ($objGeneral->fnGet("VendorReports_FixedAssetBarcodesReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_VendorsLedgerReport[0] = ($objGeneral->fnGet("VendorReports_VendorsLedgerReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_QuotationsReport[0] = ($objGeneral->fnGet("VendorReports_QuotationsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_PurchaseRequestsReport[0] = ($objGeneral->fnGet("VendorReports_PurchaseRequestsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetDepreciationReport[0] = ($objGeneral->fnGet("VendorReports_FixedAssetDepreciationReport_View") == "on") ? 1 : 0; // View
		// End of Vendor Reports

		// Accounts Reports
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports[0] = ($objGeneral->fnGet("AccountsReports_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ChartOfAccountsReport[0] = ($objGeneral->fnGet("AccountsReports_ChartOfAccountsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralJournalReport[0] = ($objGeneral->fnGet("AccountsReports_GeneralJournalReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralJournalReceipt[0] = ($objGeneral->fnGet("AccountsReports_GeneralJournalReceipt_View") == "on") ? 1 : 0; // View 
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerActivityDateWiseReport[0] = ($objGeneral->fnGet("AccountsReports_GeneralLedgerActivityDateWiseReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerActivityReport[0] = ($objGeneral->fnGet("AccountsReports_GeneralLedgerActivityReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerReport[0] = ($objGeneral->fnGet("AccountsReports_GeneralLedgerReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetAllocationReport[0] = ($objGeneral->fnGet("AccountsReports_BudgetAllocationReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetListReport[0] = ($objGeneral->fnGet("AccountsReports_BudgetListReport_View") == "on") ? 1 : 0; // View		
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetUtilizationReport[0] = ($objGeneral->fnGet("AccountsReports_BudgetUtilizationReport_View") == "on") ? 1 : 0; // View
		

		// End of Accounts Reports

		// Banking Reports
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports[0] = ($objGeneral->fnGet("BankingReports_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankLedgerReport[0] = ($objGeneral->fnGet("BankingReports_BankLedgerReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] = ($objGeneral->fnGet("BankingReports_BankDepositsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankWithdrawalsReport[0] = ($objGeneral->fnGet("BankingReports_BankWithdrawalsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankReconciliationReport[0] = ($objGeneral->fnGet("BankingReports_BankReconciliationReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankCheckBooksReport[0] = ($objGeneral->fnGet("BankingReports_BankCheckBooksReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankAccountsReport[0] = ($objGeneral->fnGet("BankingReports_BankAccountsReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport[0] = ($objGeneral->fnGet("BankingReports_BanksReport_View") == "on") ? 1 : 0; // View
		// End of Banking Reports
				
		// Financial Reports
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_FinancialReports[0] = ($objGeneral->fnGet("FinancialReports_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_FinancialReports_BalanceSheetReport[0] = ($objGeneral->fnGet("FinancialReports_BalanceSheetReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_FinancialReports_CashFlowReport[0] = ($objGeneral->fnGet("FinancialReports_CashFlowReport_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Reports_FinancialReports_IncomeStatementReport[0] = ($objGeneral->fnGet("FinancialReports_IncomeStatementReport_View") == "on") ? 1 : 0; // View
		// End of Financial Reports

		// Graph Reports
		$objEmployeeRoles->aEmployeeRole_FMS_Graphs[0] = ($objGeneral->fnGet("Graphs_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Graphs_IncomeVSExpenditure[0] = ($objGeneral->fnGet("Graphs_IncomeVSExpenditure_View") == "on") ? 1 : 0; // View
		$objEmployeeRoles->aEmployeeRole_FMS_Graphs_SalesGraph[0] = ($objGeneral->fnGet("Graphs_SalesGraph_View") == "on") ? 1 : 0; // View
		//End of Graphs

		/* End of Reports */
		$objEmployeeRoles->iEmployeeRole_Employees_EmployeeChartOfAccountAccess = $objGeneral->fnGet("EmployeeAccessOnAllChartOfAccounts");
		$objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess = $objGeneral->fnGet("EmployeeAccessOnAllBranches");
		$objEmployeeRoles->iEmployeeRole_Employees_EmployeeRegionAccess = $objGeneral->fnGet("EmployeeAccessOnAllRegions");
		$objEmployeeRoles->iEmployeeRole_FMS_Accounts_AccessAllProjects = $objGeneral->fnGet("EmployeeAccessOnAllProjects");


		$sRoles = serialize($objEmployeeRoles);
		return($sRoles);
	}

	function GetEmployeesSelectListBox($iEmployeeId, $sListBoxName = "selEmployee", $sExtraString = "")
	{
		global $objDatabase;
		global $objEmployee;

		$sEmployeeSelect = '<select class="form1" name="' . $sListBoxName . '" id="' . $sListBoxName .'">';
		
		//if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess_AllEmployeesUnderEmployee == 1)
			//$sEmployeeRestriction = $this->GetEmployeesUnderEmployee($objEmployee->iEmployeeId);
		
		$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE 1=1 AND E.Status='1' $sEmployeeRestriction ORDER BY E.FirstName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iCurrentEmployeeId = $objDatabase->Result($varResult, $i, "E.EmployeeId");
			$sEmployeeSelect .= '<option ' . (($iEmployeeId == $iCurrentEmployeeId) ? 'selected="true"' : '') . ' value="' . $iCurrentEmployeeId . '">' . $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName") . '</option>';
		}
		$sEmployeeSelect .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selEmployee\'), \'../organization/employees.php?pagetype=details&id=\'+GetSelectedListBox(\'selEmployee\'), \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
		
		return($sEmployeeSelect);
	}
	
	function GetEmployeesListBox($sListBoxName, $iEmployeeId)
	{
		global $objDatabase;
		global $objEmployee;
		
		$sEmployeeName = '<select class="form1" name="' . $sListBoxName . '" id="' . $sListBoxName . '">';
		$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE 1=1 AND E.Status='1' $sEmployeeRestriction ORDER BY E.FirstName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{			
			$sEmployeeName .= '<option value="' . $objDatabase->Result($varResult, $i, "E.EmployeeId") . '">' . $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName") . '</option>';
		}
		$sEmployeeName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selEmployee\'), \'../organization/employees.php?pagetype=details&id=\'+GetSelectedListBox(\'selEmployee\'), \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
		
		return($sEmployeeName);
	}
}

/* Employee Types Class */
class clsEmployees_Types extends clsEmployees
{
	function ShowAllEmployeeTypes($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

        $sTitle = "Employee Types";
		if ($sSortBy == "") $sSortBy = "ET.EmployeeTypeId";

        if ($sSearch != "")
        {
			$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND (ET.EmployeeTypeName LIKE '%$sSearch%') ";
			$sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        $iTotalRecords = $objDatabase->DBCount("organization_employees_types AS ET LEFT JOIN organization_employees_types AS ET2 ON ET2.EmployeeTypeId = ET.EmployeeTypeParentId", "ET.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=employeetypes&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM organization_employees_types AS ET
		LEFT JOIN organization_employees_types AS ET2 ON ET2.EmployeeTypeId = ET.EmployeeTypeParentId
        WHERE ET.OrganizationId='" . cOrganizationId . "' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">		 
		 <td align="left"><span class="WhiteHeading">Employee Type Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=ET.EmployeeTypeName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Employee Type Name in Ascending Order" title="Sort by Employee Type Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=ET.EmployeeTypeName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Employee Type Name in Descending Order" title="Sort by Employee Type Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Employee Type Parent&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=ET.EmployeeTypeParentId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Employee Type Parent in Ascending Order" title="Sort by Employee Type Parent in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=ET.EmployeeTypeParentId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Employee Type Parent in Descending Order" title="Sort by Employee Type Parent in Descending Order" border="0" /></a></span></td>
		 <td width="4%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			$iEmployeeTypeId = $objDatabase->Result($varResult, $i, "ET.EmployeeTypeId");
            $sEmployeeTypeName = $objDatabase->Result($varResult, $i, "ET.EmployeeTypeName");

            $sEmployeeTypeParentName = $objDatabase->Result($varResult, $i, "ET2.EmployeeTypeName");

            if ($sEmployeeTypeParentName == "") $sEmployeeTypeParentName = "No Parent";

           	$sEmployeeTypeRoles = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sEmployeeTypeName) . ' Roles\', \'../organization/employees_employeetypes_roles.php?id=' . $iEmployeeTypeId . '\', \'520px\', true);"><img src="../images/organization/iconEmployeeRoles.gif" border="0" alt="Employee Type Roles" title="Employee Type Roles"></a></td>';
           	$sEditEmployeeType = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString($sEmployeeTypeName) . '\', \'../organization/employees.php?pagetype=employeetypes_details&action2=edit&id=' . $iEmployeeTypeId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Employee Type Details" title="Edit Employee Type Details"></a></td>';
           	$sDeleteEmployeeType = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Employee Type?\')) {window.location = \'?action=DeleteEmployeeType&id=' . $iEmployeeTypeId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Employee Type" title="Delete this Employee Type"></a></td>';

	        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[2] == 0) // Edit Disabled
	        	$sEditEmployeeType = '';
	        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[3] == 0) // Delete Disabled
	        	$sDeleteEmployeeType = '';
	        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes_EmployeeRoles[0] == 0) // View Disabled
	        	$sEmployeeTypeRoles = '';

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td align="left" valign="top">' . $sEmployeeTypeName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sEmployeeTypeParentName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sEmployeeTypeName) . '\', \'../organization/employees.php?pagetype=employeetypes_details&id=' . $iEmployeeTypeId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Employee Type Details" title="View this Employee Type Details"></a></td>
			 ' . $sEditEmployeeType . '
			 ' . $sEmployeeTypeRoles . '
			 ' . $sDeleteEmployeeType . '
			</tr>';
		}

		$sAddNewEmployeeType = '<td style="width:100px;"><input onclick="window.top.CreateTab(\'tabContainer\', \'Add Employee Type\', \'../organization/employees.php?pagetype=employeetypes_details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Employee Type"></td>';
        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[1] == 0) // Add Disabled
           	$sAddNewEmployeeType = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  ' . $sAddNewEmployeeType . '
          <td align="left" colspan="2"><form method="GET" action="">Search for an Employee Type:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for an Employee Type" title="Search for an Employee Type" border="0"><input type="hidden" name="pagetype" id="pagetype" value="employeetypes" /></form></td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Employee Type Details" alt="View this Employee Type Details"></td><td>View this Employee Type Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Employee Type Details" alt="Edit this Employee Type Details"></td><td>Edit this Employee Type Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/organization/iconEmployeeRoles.gif" title="Employee Roles" alt="Employee Roles"></td><td>Employee Roles</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Employee Type" alt="Delete this Employee Type"></td><td>Delete this Employee Type</td></tr>
        </table>';

		return($sReturn);
	}

	function SetEmployeeTypeRoles($iEmployeeTypeId)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEncryption;

		// ToDo: Uncomment this to disable changing Administrato Roles
	    //if ($iEmployeeTypeId == 1) return(2110);

	    $sRoles = $this->SaveEmployeeRoles();
        $varResult = $objDatabase->Query("UPDATE organization_employees_types SET EmployeeTypeRoles='$sRoles' WHERE OrganizationId='" . cOrganizationId . "' AND EmployeeTypeId='$iEmployeeTypeId'");
		return(2108);
	}

	function ShowEmployeeTypeRoles($iEmployeeTypeId)
	{
		global $objDatabase;
		global $objEncryption;

		$varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.OrganizationId='" . cOrganizationId . "' AND ET.EmployeeTypeId='$iEmployeeTypeId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Type Id');

		$sEmployeeTypeName = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeName");
		$sEmployeeTypeRoles = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeRoles");

		$objEmployeeRoles = new clsEmployeeRoles();
		//$objEmployeeRoles = unserialize($objEncryption->fnDecrypt(cEncryptionKey, $sEmployeeTypeRoles));
		$objEmployeeRoles = unserialize($sEmployeeTypeRoles);
		
		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		
		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Employee Type Roles for ' . $sEmployeeTypeName . '</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">
		<form method="post" action="">';

		$sReturn .= $this->EmployeeRolesValues($iEmployeeTypeId, $objEmployeeRoles, "EmployeeTypeRoles");

		$sReturn .= '
		<input type="hidden" name="action" id="action" value="SetEmployeeTypeRoles"><input type="hidden" name="id" id="id" value="' . $iEmployeeTypeId . '"></form>
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}

    function EmployeeTypeDetails($iEmployeeTypeId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM organization_employees_types AS ET
		LEFT JOIN organization_employees_types AS ET2 on ET2.EmployeeTypeId = ET.EmployeeTypeParentId
		WHERE ET.OrganizationId='" . cOrganizationId . "' AND ET.EmployeeTypeId = '$iEmployeeTypeId'");


		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=employeetypes_details&action2=edit&id=' . $iEmployeeTypeId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Employee Type" title="Edit this Employee Type" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Employee Type?\')) {window.location = \'?pagetype=employeetypes_details&action=DeleteEmployeeType&id=' . $iEmployeeTypeId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Employee Type" title="Delete this Employee Type" /></a>';

        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[2] == 0)	$sButtons_Edit = '';		
        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[3] == 0)	$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Employee Type Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');


		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iEmployeeTypeId = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeId");
		        $sEmployeeTypeName = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeName");

		        $iEmployeeTypeParentId = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeParentId");
		        $sEmployeeTypeParentName = $objDatabase->Result($varResult, 0, "ET2.EmployeeTypeName");
		        if ($sEmployeeTypeParentName == '') $sEmployeeTypeParentName = "No Parent";

		        $sNotes = $objDatabase->Result($varResult, 0, "ET.Notes");
		        $dEmployeeTypeAddedDateTime = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeAddedDateTime");
		        $sEmployeeTypeAddedDateTime = date("F j, Y", strtotime($dEmployeeTypeAddedDateTime)) . ' at ' . date("g:i a", strtotime($dEmployeeTypeAddedDateTime));
		    }

			if ($sAction2 == "edit")
			{
		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sEmployeeTypeName = '<input type="text" name="txtEmployeeTypeName" id="txtEmployeeTypeName" class="form1" value="' . $sEmployeeTypeName . '" size="30" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sEmployeeTypeParentName = '<select class="form1" name="selEmployeeTypeParent" id="selEmployeeTypeParent">
				<option value="0">No Parent</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.OrganizationId='" . cOrganizationId . "' AND ET.EmployeeTypeId <> '$iEmployeeTypeId' ORDER BY ET.EmployeeTypeId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
		            if ($iEmployeeTypeParentId == $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeId"))
						$sEmployeeTypeParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeId") . '">' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeName") . '</option>';
					else
						$sEmployeeTypeParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeId") . '">' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeName") . '</option>';
				}
				$sEmployeeTypeParentName .= '</select>';
			}
			else if ($sAction2 == "addnew")
			{
				$iEmployeeTypeId = "";
		        $sEmployeeTypeName = $objGeneral->fnGet("txtEmployeeTypeName");
		        $sNotes = $objGeneral->fnGet("txtNotes");

		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sEmployeeTypeName = '<input type="text" name="txtEmployeeTypeName" id="txtEmployeeTypeName" class="form1" value="' . $sEmployeeTypeName . '" size="30" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sEmployeeTypeParentName = '<select class="form1" name="selEmployeeTypeParent" id="selEmployeeTypeParent">
				<option value="0">No Parent</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.OrganizationId='" . cOrganizationId . "' AND ET.EmployeeTypeId <> '$iEmployeeTypeId' ORDER BY ET.EmployeeTypeId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
		            if ($iEmployeeTypeParentId == $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeId"))
						$sEmployeeTypeParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeId") . '">' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeName") . '</option>';
					else
						$sEmployeeTypeParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeId") . '">' . $objDatabase->Result($varResult2, $i, "ET.EmployeeTypeName") . '</option>';
				}
				$sEmployeeTypeParentName .= '</select>';

				$sEmployeeTypeAddedDateTime = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
	        function ValidateForm()
	        {
	        	if (GetVal(\'txtEmployeeTypeName\') == "") return(AlertFocus(\'Please enter a valid Employee Type Name\', \'txtEmployeeTypeName\'));
	         	return true;
	        }
	        </script>
		    <table border=0 cellspacing=0 cellpadding=5 width="100%" align=center>
		     <tr class="Details_Title_TR"><td colspan=2><span class="Details_Title">Employee Type Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top" style="width:200px;">Employee Type Name:</td><td><strong>' . $sEmployeeTypeName . '</strong></td></tr>
			 <tr><td width="35%">Employee Type Parent:</td><td><strong>' . $sEmployeeTypeParentName . '</strong></td></tr>
		     <tr class="Details_Title_TR"><td colspan=2><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr><td width="35%">Employee Type Added Date/Time:</td><td><strong>' . $sEmployeeTypeAddedDateTime . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iEmployeeTypeId . '"><input type="hidden" name="action" id="action" value="UpdateEmployeeType"></form></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewEmployeeType"></form></div>';

		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

    function AddNewEmployeeType($sEmployeeTypeName, $iEmployeeTypeParentId, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		
		$varNow = $objGeneral->fnNow();

        $sEmployeeTypeName = $objDatabase->RealEscapeString($sEmployeeTypeName);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[1] == 0)
			return(1010);
		
        if ($objDatabase->DBCount("organization_employees_types AS ET", "ET.EmployeeTypeName='$sEmployeeTypeName'") > 0)
            return(2100);

        $varResult = $objDatabase->Query("INSERT INTO organization_employees_types
        (OrganizationId, EmployeeTypeName, EmployeeTypeParentId, Notes, EmployeeTypeAddedDateTime)
        VALUES ('" . cOrganizationId . "', '$sEmployeeTypeName', '$iEmployeeTypeParentId', '$sNotes', '$varNow')");

        $varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.OrganizationId='" . cOrganizationId . "' AND ET.EmployeeTypeName='$sEmployeeTypeName'");
        if ($objDatabase->RowsNumber($varResult) > 0)
            $objGeneral->fnRedirect('?pagetype=employeetypes_details&error=2101&id=' . $objDatabase->Result($varResult, 0, "ET.EmployeeTypeId"));
        else
            return(2102);
    }

    function UpdateEmployeeType($iEmployeeTypeId, $sEmployeeTypeName, $iEmployeeTypeParentId, $sNotes)
    {
        global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;

        $sEmployeeTypeName = $objDatabase->RealEscapeString($sEmployeeTypeName);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[2] == 0)
			return(1010);		
		
        $varResult = $objDatabase->Query("
        UPDATE organization_employees_types SET
            EmployeeTypeName='$sEmployeeTypeName',
            EmployeeTypeParentId='$iEmployeeTypeParentId',
            Notes='$sNotes'

        WHERE OrganizationId='" . cOrganizationId . "' AND EmployeeTypeId='$iEmployeeTypeId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {          	
			$objSystemLog->AddLog("Update Employee Type - " . $sEmployeeTypeName);			
			$objGeneral->fnRedirect('?pagetype=employeetypes_details&error=2103&id=' . $iEmployeeTypeId);
            //return(2103);
        }
        else
            return(2104);
    }

    function DeleteEmployeeType($iEmployeeTypeId)
    {
        global $objDatabase;
		global $objGeneral;
		global $objSystemLog;
		global $objEmployee;

        if ($iEmployeeTypeId == 1) return(2109);
  		// Employee Roles
		if($objEmployee->objEmployeeRoles->aEmployeeRole_Employees_EmployeeTypes[3] == 0)
		{
			if ($objGeneral->fnGet("pagetype") == "employeetypes_details")
				$objGeneral->fnRedirect('?pagetype=employeetypes_details&id=' . $iEmployeeTypeId . '&error=1010');
			else
				$objGeneral->fnRedirect('?pagetype=employeetypes&error=1010');
		}
		
        if ($objDatabase->DBCount("organization_employees AS E", "E.OrganizationId='" . cOrganizationId . "' AND E.EmployeeTypeId='$iEmployeeTypeId'") > 0) return(2111);
        if ($objDatabase->DBCount("organization_employees_types AS ET", "ET.OrganizationId='" . cOrganizationId . "' AND ET.EmployeeTypeParentId='$iEmployeeTypeId'") > 0) return(2112);

        $varResult = $objDatabase->Query("SELECT * FROM organization_employees_types AS ET WHERE ET.OrganizationId='" . cOrganizationId . "' AND ET.EmployeeTypeId='$iEmployeeTypeId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Type Id');
        $sEmployeeTypeName = $objDatabase->Result($varResult, 0, "ET.EmployeeTypeName");

        $varResult = $objDatabase->Query("DELETE FROM organization_employees_types WHERE EmployeeTypeId='$iEmployeeTypeId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {			
			$objSystemLog->AddLog("Delete Employee Type - " . $sEmployeeTypeName);
			
			if ($objGeneral->fnGet("pagetype") == "employeetypes_details")
				$objGeneral->fnRedirect('?pagetype=employeetypes_details&id=' . $iEmployeeTypeId . '&error=2105');
			else
				$objGeneral->fnRedirect('?pagetype=employeetypes&error=2105');
        }
        else
            return(2106);
    }
}


class clsEmployeeRoles
{
	/* Arrays
	   0 = View
	   1 = Add
	   2 = Edit
	   3 = Delete

	   Values = 0 or 1
	   0 = Don't Allow
	   1 = Allow

	   */

	/* The Organization */
	
	// Regional Hierarchy
	public $aEmployeeRole_Organization_RegionalHierarchy_RegionTypes;
	public $aEmployeeRole_Organization_RegionalHierarchy_Regions;
	
	// Stations	
	public $aEmployeeRole_Organization_Stations;
	public $iEmployeeRole_Organization_Stations_StationMap;
	public $aEmployeeRole_Organization_Departments;
	
	// System
	public $aEmployeeRole_Organization_DataBackup;
	public $aEmployeeRole_Organization_SystemSettings;
	public $aEmployeeRole_Organization_SystemLog;
	
	//Home
	public $iEmployeeRole_Home_BankAccounts;
	public $iEmployeeRole_Home_ChartOfAccounts;
	public $iEmployeeRole_Home_CustomerOutStandingInvoices;
	public $iEmployeeRole_Home_IncomeVSExpendituresGraph;
	
	// Employees
	public $iEmployeeRole_Employees_EmployeeChartOfAccountAccess;
	public $iEmployeeRole_Employees_EmployeeStationAccess;								// 0 = Access on All Stations, 1 = Access on Employee's Branch
	public $iEmployeeRole_Employees_EmployeeRegionAccess;								// 0 = Access on All Regions, 1 = Access on Employee's Assigned Regions
	
	public $aEmployeeRole_Employees_EmployeeTypes;
	public $aEmployeeRole_Employees_EmployeeTypes_EmployeeRoles;
	public $aEmployeeRole_Employees_Employees;
	public $aEmployeeRole_Employees_Employees_EmployeeTargets;
	public $aEmployeeRole_Employees_Employees_EmployeeRoles;
	public $aEmployeeRole_Employees_Employees_EmployeeDocuments;


	/* End of The Organization */

		
	/* Accounts */
	public $aEmployeeRole_FMS_Accounts_ChartOfAccounts;	
	public $aEmployeeRole_FMS_Accounts_ChartOfAccountTypes;
	public $aEmployeeRole_FMS_Accounts_Budgets;	
	public $iEmployeeRole_FMS_Accounts_Budgets_ChangeBudgetAmount;	
	public $aEmployeeRole_FMS_Accounts_BudgetAllocations;	
	public $aEmployeeRole_FMS_Accounts_GeneralJournals;
	public $aEmployeeRole_FMS_Accounts_GeneralJournal_Documents;
	public $aEmployeeRole_FMS_Accounts_QuickEntries;
	public $aEmployeeRole_FMS_Accounts_GeneralLedgers;
	public $aEmployeeRole_FMS_Accounts_SourceOfIncome;	
	public $aEmployeeRole_FMS_Accounts_CloseFinancialYear;
	public $aEmployeeRole_FMS_Accounts_Donors;
	public $aEmployeeRole_FMS_Accounts_DonorsProjects;
	public $aEmployeeRole_FMS_Accounts_DonorsProjectsActivities;

	public $iEmployeeRole_FMS_Accounts_AccessAllProjects;

	/* End of Accounts */
	
	/* Customers */
	public $aEmployeeRole_FMS_Customers_Customers;
	public $aEmployeeRole_FMS_Customers_Quotations;
	public $aEmployeeRole_FMS_Customers_Quotations_Documents;
	public $aEmployeeRole_FMS_Customers_Invoices;
	public $aEmployeeRole_FMS_Customers_Invoices_Documents;
	public $aEmployeeRole_FMS_Customers_SalesOrders;
	public $aEmployeeRole_FMS_Customers_RecurringInvoices;
	public $aEmployeeRole_FMS_Customers_SalesOrders_Documents;
	public $aEmployeeRole_FMS_Customers_Receipts;
	public $aEmployeeRole_FMS_Customers_Receipts_Documents;
	public $aEmployeeRole_FMS_Customers_Jobs;
	
	/* End of Customers */
	
	/* Vendors */
	public $aEmployeeRole_FMS_Vendors_Vendors;
	public $aEmployeeRole_FMS_Vendors_VendorTypes;
	public $aEmployeeRole_FMS_Vendors_FixedAssets;
	public $aEmployeeRole_FMS_Vendors_FixedAssets_Categories;
	public $aEmployeeRole_FMS_Vendors_PurchaseOrders;
	public $aEmployeeRole_FMS_Vendors_PurchaseOrders_Documents;
	public $aEmployeeRole_FMS_Vendors_PurchaseRequests;
	public $aEmployeeRole_FMS_Vendors_PurchaseRequests_Documents;
	public $aEmployeeRole_FMS_Vendors_Quotations;
	public $aEmployeeRole_FMS_Vendors_Quotations_Documents;
	/* End of Vendors */

    /*Manufacturers */
    public $aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers;

    /* End of Manufacturers */
	
	/* Banking */
	public $aEmployeeRole_FMS_Banking_Banks;	
	public $aEmployeeRole_FMS_Banking_BankAccounts;
	public $aEmployeeRole_FMS_Banking_BankCheckBooks;
	public $aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks;
	public $aEmployeeRole_FMS_Banking_BankDeposits;
	public $aEmployeeRole_FMS_Banking_BankWithdrawals;
	public $iEmployeeRole_FMS_Banking_BankReconciliation;
	/* End of Vendors */
	
	/* Reports */
	
	/* Customer Reports */
	public $aEmployeeRole_FMS_Reports_CustomerReports;
	public $aEmployeeRole_FMS_Reports_CustomerReports_CustomersList;
	public $aEmployeeRole_FMS_Reports_CustomerReports_CustomersLedgerReport;
	public $aEmployeeRole_FMS_Reports_CustomerReports_QuotationsReport;
	public $aEmployeeRole_FMS_Reports_CustomerReports_SalesOrdersReport;
	public $aEmployeeRole_FMS_Reports_CustomerReports_InvoicesReport;
	public $aEmployeeRole_FMS_Reports_CustomerReports_ReceiptsReport;
	public $aEmployeeRole_FMS_Reports_CustomerReports_JobsReport;
	/* End of Customer Reports
	
	/* Vendor Reports */
	public $aEmployeeRole_FMS_Reports_VendorReports;
	public $aEmployeeRole_FMS_Reports_VendorReports_VendorsReport;
	public $aEmployeeRole_FMS_Reports_VendorReports_FixedAssetsReport;
	public $aEmployeeRole_FMS_Reports_VendorReports_FixedAssetBarcodesReport;
	public $aEmployeeRole_FMS_Reports_VendorReports_VendorsLedgerReport;
	public $aEmployeeRole_FMS_Reports_VendorReports_QuotationsReport;
	public $aEmployeeRole_FMS_Reports_VendorReports_PurchaseRequestsReport;
	public $aEmployeeRole_FMS_Reports_VendorReports_FixedAssetDepreciationReport;
	/* End of Vendor Reports
	
	/* Banking Reports */
	public $aEmployeeRole_FMS_Reports_BankingReports;
	public $aEmployeeRole_FMS_Reports_BankingReports_BankLedgerReport;
	public $aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport;
	public $aEmployeeRole_FMS_Reports_BankingReports_BankWithdrawalsReport;
	public $aEmployeeRole_FMS_Reports_BankingReports_BankReconciliationReport;
	public $aEmployeeRole_FMS_Reports_BankingReports_BankCheckBooksReport;
	public $aEmployeeRole_FMS_Reports_BankingReports_BankAccountsReport;
	public $aEmployeeRole_FMS_Reports_BankingReports_BanksReport;
	/* End of Banking Reports	
	
	/* Accounts Reports */
	public $aEmployeeRole_FMS_Reports_AccountsReports;
	public $aEmployeeRole_FMS_Reports_AccountsReports_ChartOfAccountsReport;
	public $aEmployeeRole_FMS_Reports_AccountsReports_GeneralJournalReport;
	public $aEmployeeRole_FMS_Reports_AccountsReports_GeneralJournalReceipt;
	public $aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerReport;
	public $aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerActivityReport;
	public $aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerActivityDateWiseReport;	
	public $aEmployeeRole_FMS_Reports_AccountsReports_BudgetListReport;
	public $aEmployeeRole_FMS_Reports_AccountsReports_BudgetAllocationReport;
	public $aEmployeeRole_FMS_Reports_AccountsReports_BudgetUtilizationReport;

	/* End of Accounts Reports	*/
	
	/* Financial Reports*/
	public $aEmployeeRole_FMS_Reports_FinancialReports;
	public $aEmployeeRole_FMS_Reports_FinancialReports_BalanceSheetReport;
	public $aEmployeeRole_FMS_Reports_FinancialReports_IncomeStatementReport;
	public $aEmployeeRole_FMS_Reports_FinancialReports_CashFlowReport;
	/* End of Financial Reports */
	
	/* Financial Reports*/
	public $aEmployeeRole_FMS_Graphs;
	public $aEmployeeRole_FMS_Graphs_IncomeVSExpenditure;
	public $aEmployeeRole_FMS_Graphs_SalesGraph;	
	/* End of Graphs */
	
	/* End of Reports */

}

?>