<?php

/* Regions Class */
class clsRegions extends clsOrganization
{
	function ShowAllRegions($sSearch, $iRegionTypeId = 0)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = 'Regions';
		if ($sSortBy == "") $sSortBy = "R.RegionId DESC";

        if ($sSearch != "")
        {
        	$sSearch = mysql_escape_string($sSearch);
            $sSearchCondition = " AND ((R.RegionId LIKE '%$sSearch%') OR (R.RegionName LIKE '%$sSearch%') OR (R.RegionCode LIKE '%$sSearch%') OR (RT.RegionTypeName LIKE '%$sSearch%')) ";
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

		if ($iRegionTypeId == '') $iRegionTypeId = 0;
        if ($iRegionTypeId > 0)
        {
        	$varResult = $objDatabase->Query("SELECT * FROM organization_regions_types AS RT WHERE RT.RegionTypeId='$iRegionTypeId'");
        	if ($objDatabase->RowsNumber($varResult) > 0)
           		$sTitle = $objDatabase->Result($varResult, 0, "RT.RegionTypeName");
				
	        $sRegionTypeString = " AND RT.RegionTypeId='$iRegionTypeId'";
        }

        $iTotalRecords = $objDatabase->DBCount("organization_regions AS R INNER JOIN organization_regions_types AS RT ON RT.RegionTypeId = R.RegionTypeId", "1=1 $sSearchCondition $sRegionTypeString");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM organization_regions AS R
		INNER JOIN organization_regions_types AS RT ON RT.RegionTypeId = R.RegionTypeId
        WHERE 1=1 $sSearchCondition $sRegionTypeString
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td align="left"><span class="WhiteHeading">Region Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=R.RegionId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Region Id in Ascending Order" title="Sort by Region Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=R.RegionId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Region Id in Descending Order" title="Sort by Region Id in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Region Type Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=RT.RegionTypeName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Region Type Name in Ascending Order" title="Sort by Region Type Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=RT.RegionTypeName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Region Type Name in Descending Order" title="Sort by Region Type Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Region Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=R.RegionName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Region Name in Ascending Order" title="Sort by Region Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=R.RegionName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Region Name in Descending Order" title="Sort by Region Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Region Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=R.RegionCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Region Code in Ascending Order" title="Sort by Region Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=R.RegionCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Region Code in Descending Order" title="Sort by Region Code in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Description&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=R.RegionDescription&sortorder="><img src="../images/sort_up.gif" alt="Sort by Region Description in Ascending Order" title="Sort by Region Description in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=R.RegionDescription&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Region Description in Descending Order" title="Sort by Region Description in Descending Order" border="0" /></a></span></td>
		 <td width="2%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iRegionId = $objDatabase->Result($varResult, $i, "R.RegionId");
            $sRegionTypeName = $objDatabase->Result($varResult, $i, "RT.RegionTypeName");
            $sRegionDescription = $objDatabase->Result($varResult, $i, "R.RegionDescription");
            $sRegionName = $objDatabase->Result($varResult, $i, "R.RegionName");
            $sRegionCode = $objDatabase->Result($varResult, $i, "R.RegionCode");

            $sEditRegionDetails = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . mysql_escape_string($sRegionName) . '\', \'../organization/regionalhierarchy_regions_details.php?action2=edit&id=' . $iRegionId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Region Details" title="Edit Region Details"></a></td>';
            $sDeleteRegionDetails = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Region?\')) {window.location = \'?action=DeleteRegion&id=' . $iRegionId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Region" title="Delete this Region"></a></td>';

            if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[2] == 0) // Update Disabled
            	$sEditRegionDetails = '';

            if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[3] == 0) // Update Disabled
            	$sDeleteRegionDetails = '';

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $iRegionId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sRegionTypeName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sRegionName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sRegionCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sRegionDescription . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string($sRegionName) . '\', \'../organization/regionalhierarchy_regions_details.php?id=' . $iRegionId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Region Details" title="View this Region Details"></a></td>
			 ' . $sEditRegionDetails . '
			 ' . $sDeleteRegionDetails . '
			</tr>';
		}

		$sAddNewRegion = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Region\', \'../organization/regionalhierarchy_regions_details.php?action2=addnew\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add Region">';

		$sRegionTypeSelect = '<select class="form1" name="selRegionType" onchange="window.location=\'?regiontype=\'+GetSelectedListBox(\'selRegionType\');" id="selRegionType">
		<option value="0">All Region Types</option>';
		$varResult = $objDatabase->Query("SELECT * FROM organization_regions_types AS RT ORDER BY RT.RegionTypeName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sRegionTypeSelect .= '<option ' . (($iRegionTypeId == $objDatabase->Result($varResult, $i, "RT.RegionTypeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "RT.RegionTypeId") . '">' . $objDatabase->Result($varResult, $i, "RT.RegionTypeName") . '</option>';
		$sRegionTypeSelect .= '</select>';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[1] == 0) // Add Disabled
         	$sAddNewRegion = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddNewRegion . '
           <form method="GET" action="">&nbsp;&nbsp;&nbsp;Search for a Region:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Region" title="Search for a Region" border="0"></form>
          </td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>' . $sRegionTypeSelect . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Region Details" alt="View this Region Details"></td><td>View this Region Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Region Details" alt="Edit this Region Details"></td><td>Edit this Region Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Region" alt="Delete this Region"></td><td>Delete this Region</td></tr>
        </table>';

		return($sReturn);
	}

    function RegionDetails($iRegionId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM organization_regions AS R
		INNER JOIN organization_regions_types AS RT ON RT.RegionTypeId = R.RegionTypeId
		INNER JOIN organization_employees AS E ON E.EmployeeId = R.RegionAddedBy
		LEFT JOIN organization_regions AS R2 ON R2.RegionId = R.RegionParentId
		WHERE R.RegionId = '$iRegionId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iRegionId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Region" title="Edit this Region" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Region?\')) {window.location = \'?action=DeleteRegion&id=' . $iRegionId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Region" title="Delete this Region" /></a>';

        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[2] == 0) // Update Disabled
        	$sButtons_Edit = '';

        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[3] == 0) // Delete Disabled
        	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Region Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iRegionId = $objDatabase->Result($varResult, 0, "R.RegionId");
		    	$iRegionTypeId = $objDatabase->Result($varResult, 0, "RT.RegionTypeId");

		    	$sRegionTypeName = $objDatabase->Result($varResult, 0, "RT.RegionTypeName");
		        $sRegionTypeName = $sRegionTypeName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'' . $sRegionTypeName .'\' , \'../organization/regionalhierarchy_regiontypes_details.php?id=' . $iRegionTypeId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Region Type Details" title="Region Type Details" /></a>';
		        $sRegionName = $objDatabase->Result($varResult, 0, "R.RegionName");
		        $sRegionCode = $objDatabase->Result($varResult, 0, "R.RegionCode");
		        $iRegionParentId = $objDatabase->Result($varResult, 0, "R.RegionParentId");
		        $sNotes = $objDatabase->Result($varResult, 0, "R.Notes");

		        $iStatus = $objDatabase->Result($varResult, 0, "R.Status");
		        $sStatus = ($iStatus == 0) ? 'Inactive' : 'Active';

		        $sRegionParentName = $objDatabase->Result($varResult, 0, "R2.RegionName");
		        $sRegionDescription = $objDatabase->Result($varResult, 0, "R.RegionDescription");

		        if ($sRegionParentName == '') $sRegionParentName = 'No Parent';
		        else $sRegionParentName = $sRegionParentName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'' . $sRegionParentName .'\' , \'../organization/regionalhierarchy_regions_details.php?id=' . $iRegionParentId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Region Parent Details" title="Region Parent Details" /></a>';

		        $dRegionAddedOn = $objDatabase->Result($varResult, 0, "R.RegionAddedOn");
		        $sRegionAddedOn = date("F j, Y", strtotime($dRegionAddedOn)) . ' at ' . date("g:i a", strtotime($dRegionAddedOn));
				
				$iRegionAddedBy = $objDatabase->Result($varResult, 0, "R.RegionAddedBy");
				$sRegionAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sRegionAddedBy = $sRegionAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(str_replace('"', '', $sRegionAddedBy)) . '\', \'../employees/employees_details.php?id=' . $iRegionAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
		    }

			if ($sAction2 == "edit")
			{
		        $sStatus = '<select class="form1" name="selStatus" id="selStatus">';
		        $sStatus .= '<option ' . (($iStatus == 1) ? 'selected="true"' : '') . ' value="1">Active</option>';
		        $sStatus .= '<option ' . (($iStatus == 0) ? 'selected="true"' : '') . ' value="0">Inactive</option>';
		        $sStatus .= '</select>';

		        $sRegionTypeName = '<select class="form1" name="selRegionTypeId" id="selRegionTypeId">';
		        $varResult2 = $objDatabase->Query("SELECT * FROM organization_regions_types AS RT");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					if ($iRegionTypeId == $objDatabase->Result($varResult2, $i, "RT.RegionTypeId"))
						$sRegionTypeName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . '</option>';
					else
						$sRegionTypeName .= '<option value="' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . '</option>';
				}
		        $sRegionTypeName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selRegionTypeId\'), \'../organization/regionalhierarchy_regiontypes_details.php?id=\'+GetSelectedListBox(\'selRegionTypeId\'), \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Region Type Details" title="Region Type Details" /></a>';

		        $sRegionParentName = '<select class="form1" name="selRegionParentId" id="selRegionParentId">
		        <option value="0">No Parent</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM organization_regions AS R INNER JOIN organization_regions_types AS RT ON RT.RegionTypeId = R.RegionTypeId WHERE R.RegionId <> '$iRegionId' ORDER BY RT.RegionTypeId");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iRegionParentId == $objDatabase->Result($varResult2, $i, "R.RegionId"))
		                $sRegionParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "R.RegionId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . ' - ' . $objDatabase->Result($varResult2, $i, "R.RegionName") . '</option>';
		            else if ($iRegionId == $objDatabase->Result($varResult2, $i, "R.RegionId"))
		            	$sRegionParentName .= '';
		            else
		                $sRegionParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "R.RegionId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . ' - ' . $objDatabase->Result($varResult2, $i, "R.RegionName") . '</option>';
		        }
		        $sRegionParentName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selRegionParentId\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selRegionParentId\'), \'../organization/regionalhierarchy_regions_details.php?id=\'+GetSelectedListBox(\'selRegionParentId\'), \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Region Parent Details" title="Region Parent Details" /></a>';

		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">
		        <script language="JavaScript" type="text/javascript">
		         function ValidateForm()
		         {
		         	if (GetVal(\'txtRegionName\') == "") return(AlertFocus(\'Please enter a valid Region Name\', \'txtRegionName\'));
					if (GetVal(\'txtRegionCode\') == "") return(AlertFocus(\'Please enter a valid Region Code\', \'txtRegionCode\'));

		         	return true;
		         }
		        </script>';

		        $sRegionName = '<input type="text" name="txtRegionName" id="txtRegionName" class="form1" value="' . $sRegionName . '" size="30" />';
		        $sRegionCode = '<input type="text" name="txtRegionCode" id="txtRegionCode" class="form1" value="' . $sRegionCode . '" size="15" />';
		        $sRegionDescription = '<textarea class="form1" name="txtRegionDescription" id="txtRegionDescription" rows="4" cols="50">' . $sRegionDescription . '</textarea>';

		        $sNotes = '<textarea class="form1" rows="5" cols="60" name="txtNotes" id="txtNotes">'. $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$dRegionAddedOn = $objGeneral->fnNow();
				$sRegionAddedOn = date("F j, Y", strtotime($dRegionAddedOn)) . ' at ' . date("g:i a", strtotime($dRegionAddedOn));
				
				$sRegionAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sRegionAddedBy = $sRegionAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(str_replace('"', '', $sRegionAddedBy)) . '\', \'../employees/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

				$iRegionTypeId = "";

		        $sStatus = '<select class="form1" name="selStatus" id="selStatus">';
		        $sStatus .= '<option value="1">Active</option>';
		        $sStatus .= '<option value="0">Inactive</option>';
		        $sStatus .= '</select>';

		        $sRegionTypeName = '<select class="form1" name="selRegionTypeId" id="selRegionTypeId">';
		        $varResult2 = $objDatabase->Query("SELECT * FROM organization_regions_types AS RT");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iRegionTypeId == $objDatabase->Result($varResult2, $i, "RT.RegionTypeId"))
		                $sRegionTypeName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . '</option>';
		            else
		                $sRegionTypeName .= '<option value="' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . '</option>';
		        }
		        $sRegionTypeName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selRegionTypeId\'), \'../organization/regionalhierarchy_regiontypes_details.php?id=\'+GetSelectedListBox(\'selRegionTypeId\'), \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Region Type Details" title="Region Type Details" /></a>';

		        $sRegionParentName = '<select class="form1" name="selRegionParentId" id="selRegionParentId">
		        <option value="0">No Parent</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM organization_regions AS R INNER JOIN organization_regions_types AS RT ON RT.RegionTypeId = R.RegionTypeId WHERE R.RegionId <> '$iRegionId' ORDER BY RT.RegionTypeId");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iRegionParentId == $objDatabase->Result($varResult2, $i, "R.RegionId"))
		                $sRegionParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "R.RegionId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . ' - ' . $objDatabase->Result($varResult2, $i, "R.RegionName") . '</option>';
		            else
		                $sRegionParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "R.RegionId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . ' - ' . $objDatabase->Result($varResult2, $i, "R.RegionName") . '</option>';
				}
		        $sRegionParentName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selRegionParentId\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selRegionParentId\'), \'../organization/regionalhierarchy_regions_details.php?id=\'+GetSelectedListBox(\'selRegionParentId\'), \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Region Parent Details" title="Region Parent Details" /></a>';

		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">
		        <script language="JavaScript" type="text/javascript">
		         function ValidateForm()
		         {
		         	if (GetVal(\'txtRegionName\') == "")
		         		return(AlertFocus(\'Please enter a valid Region Name\', \'txtRegionName\'));
					if (GetVal(\'txtRegionCode\') == "")
		         		return(AlertFocus(\'Please enter a valid Region Code\', \'txtRegionCode\'));

		         	return true;
		         }
		        </script>';

		        $sRegionName = '<input type="text" name="txtRegionName" id="txtRegionName" class="form1" value="' . $sRegionName . '" size="30" />';
		        $sRegionCode = '<input type="text" name="txtRegionCode" id="txtRegionCode" class="form1" value="' . $sRegionCode . '" size="15" />';
		        $sRegionDescription = '<textarea class="form1" name="txtRegionDescription" id="txtRegionDescription" rows="4" cols="50">' . $sRegionDescription . '</textarea>';
		        $sNotes = '<textarea class="form1" rows="5" cols="60" name="txtNotes" id="txtNotes">'. $sNotes . '</textarea>';
			}


			$sReturn .= '
			<table border=0 cellspacing=0 cellpadding=5 width="100%" align=center>
		     <tr class="GridTR"><td colspan="4"><span class="title2">Region Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td width="20%">Region Id:</td><td colspan="3"><strong>' . $iRegionId . ' (Automatically Assigned)</strong></td></tr>
			 <tr><td>Region Type:</td><td colspan="3"><strong>' . $sRegionTypeName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Region Name:</td><td colspan="3"><strong>' . $sRegionName . '</strong></td></tr>
			 <tr><td>Region Code:</td><td colspan="3"><strong>' . $sRegionCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Region Parent:</td><td colspan="3"><strong>' . $sRegionParentName . '</strong></td></tr>
			 <tr><td valign="top">Region Description:</td><td colspan="3"><strong>' . $sRegionDescription . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Status:</td><td colspan="3"><strong>' . $sStatus . '</strong></td></tr>
		     <tr class="GridTR"><td colspan="4"><span class="title2">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes</td><td colspan="3">' . $sNotes . '</td></tr>
			 <tr bgcolor="#edeff1"><td>Region Added On:</td><td colspan="3"><strong>' . $sRegionAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Region Added By:</td><td colspan="3"><strong>' . $sRegionAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iRegionId . '"><input type="hidden" name="action" id="action" value="UpdateRegion"></form></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Region" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewRegion"></form></div>';

		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

    function GetRegionalHierarchy($iRegionId, $bFirst = true)
    {
    	global $objDatabase;

    	if ($bFirst)
    		$sReturn .= " AND ((R.RegionId = '$iRegionId')";

    	$varResult = $objDatabase->Query("SELECT * FROM organization_regions AS R WHERE R.RegionParentId='$iRegionId'");
    	if ($objDatabase->RowsNumber($varResult) > 0)
    	{
    		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    		{
    			$iNewRegionid = $objDatabase->Result($varResult, $i, "R.RegionId");
    			$sReturn .= " OR (R.RegionId = '" . $iNewRegionid . "')";
    			$sReturn .= $this->GetRegionalHierarchy($iNewRegionid, false);
    		}
    	}

    	if ($bFirst)
   			$sReturn .= ")";
    	return($sReturn);
    }

    function ShowSearchRegionPopUp($sType = "")
    {
    	global $objDatabase;

    	$sReturn = '<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Search for a Region</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		$sReturn .= '<table bordercolor="#E6E6E6" border="1" style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;" cellspacing="0" cellpadding="3" width="100%" align="center">
			<tr bgcolor="#7990bf">
			 ' . (($sType == "AddMultiple") ? '<td></td>' : '') . '
			 <td align="left"><span class="WhiteHeading">Region Type</span></td>
			 <td align="left"><span class="WhiteHeading">Region Name</span></td>
			 <td align="left"><span class="WhiteHeading">Region Parent</span></td>
			</tr>';

    	$varResult = $objDatabase->Query("SELECT * FROM organization_regions AS R
    	INNER JOIN organization_regions_types AS RT ON RT.RegionTypeId = R.RegionTypeId
    	LEFT JOIN organization_regions AS R2 on R2.RegionId = R.RegionParentId");

    	for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{
    		$iRegionId = $objDatabase->Result($varResult, $i, "R.RegionId");
    		$sRegionName = $objDatabase->Result($varResult, $i, "R.RegionName");
    		$sRegionType = $objDatabase->Result($varResult, $i, "RT.RegionTypeName");
    		$sRegionParent = $objDatabase->Result($varResult, $i, "R2.RegionName");


    		if ($sType == "AddMultiple")
    		{
    			$sAddMultiple = '<td align="center"><input type="checkbox" name="chkRegion' . $i . '" id="chkRegion' . $i . '" value="on" />
    			 <input type="hidden" name="hdnRegionName' . $i . '" id="hdnRegionName' . $i . '" value="' . $sRegionName . ' ' . $sLastName . '" />
    			 <input type="hidden" name="hdnRegionType' . $i . '" id="hdnRegionType' . $i . '" value="' . $sRegionType . '" />
    			 <input type="hidden" name="hdnRegionId' . $i . '" id="hdnRegionId' . $i . '" value="' . $iRegionId . '" />
    			</td>';
    		}

    		$sReturn .= '<tr>
    		 ' . $sAddMultiple . '
    		 <td>' . $sRegionType . '</td>
    		 <td>' . $sRegionName . '</td>
    		 <td>' . $sRegionParent . '</td>
    		</tr>';
    	}

    	$sReturn .= '</table>';

    	$sReturn .= '</td></tr></table></td></tr></table>';

    	if ($sType == "AddMultiple")
    	{
    		$sReturn .= '<br /><div align="center"><input type="button" class="AdminFormButton1" onclick="AddMultipleRegions()" value="Add Selected Regions" /></div><br />

    		<script type="text/javascript" language="JavaScript">
    		 var iTotalRegions = ' . $i . ';

    		 function AddMultipleRegions()
    		 {
    		 	var sRegionsMultipleAdd = \'\';
    		 	var j = 0;
    		 	for (i = 0; i < iTotalRegions; i++)
    		 	{
    		 		if (document.getElementById(\'chkRegion\'+i).checked)
    		 		{
    		 			sRegionsMultipleAdd += document.getElementById(\'hdnRegionId\' + i).value + \'|\';
    		 			sRegionsMultipleAdd += document.getElementById(\'hdnRegionName\' + i).value + \'|\';
    		 			sRegionsMultipleAdd += document.getElementById(\'hdnRegionType\' + i).value + \'|\';
    		 			sRegionsMultipleAdd += \'~\';
    		 			j++;
    		 		}
    		 	}

				window.opener.AddNewRegions(sRegionsMultipleAdd);
    		 	window.close();
    		 }
    		</script>
    		';

    	}

    	return($sReturn);
    }

    function AddNewRegion($sRegionName, $sRegionCode, $iRegionTypeId, $iRegionParentId, $sRegionDescription, $sNotes, $iStatus)
    {
        global $objDatabase;
        global $objGeneral;
   		global $objEmployee;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[1] == 0) // View Disabled
			return(1010);

		$sRegionCode = mysql_escape_string($sRegionCode);
		$sRegionName = mysql_escape_string($sRegionName);

		if ($objDatabase->DBCount("organization_regions AS R", "R.RegionCode='$sRegionCode'"))
			return(3036);

        $sRegionTypeName = mysql_escape_string($sRegionTypeName);
        $sRegionDescription = mysql_escape_string($sRegionDescription);
        $sNotes = mysql_escape_string($sNotes);

        if ($objDatabase->DBCount("organization_regions_types AS RT", "RT.RegionTypeId='$iRegionTypeId'") <= 0)
        	return(3008);

        $varNow = $objGeneral->fnNow();

        $varResult = $objDatabase->Query("INSERT INTO organization_regions
        (RegionTypeId, RegionParentId, RegionName, RegionCode, RegionDescription, Notes, Status, RegionAddedOn, RegionAddedBy)
        VALUES ('$iRegionTypeId', '$iRegionParentId', '$sRegionName', '$sRegionCode', '$sRegionDescription', '$sNotes', '$iStatus', '$varNow', '$objEmployee->iEmployeeId')");

        $varResult = $objDatabase->Query("SELECT * FROM organization_regions AS R WHERE R.RegionName='$sRegionName' AND R.RegionAddedOn='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {
	       	
  			$objSystemLog->AddLog("Add New Region - " . $objDatabase->Result($varResult, 0, "R.RegionName"));

            $objGeneral->fnRedirect('?error=3020&id=' . $objDatabase->Result($varResult, 0, "R.RegionId"));
        }
        else
            return(3021);
    }

    function UpdateRegion($iRegionId, $sRegionName, $sRegionCode, $iRegionTypeId, $iRegionParentId, $sRegionDescription, $sNotes, $iStatus)
    {
        global $objDatabase;
        global $objGeneral;
   		global $objEmployee;
		global $objEmployee;

   		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[2] == 0) // Update Disabled
			return(1010);


        $sRegionName = mysql_escape_string($sRegionName);
        $sRegionCode = mysql_escape_string($sRegionCode);
        $sRegionDescription = mysql_escape_string($sRegionDescription);
        $sNotes = mysql_escape_string($sNotes);


		if ($objDatabase->DBCount("organization_regions AS R", "R.RegionId <> '$iRegionId' AND R.RegionCode='$sRegionCode'"))
			return(3036);

        $varResult = $objDatabase->Query("
        UPDATE organization_regions SET
            RegionTypeId='$iRegionTypeId',
            RegionParentId='$iRegionParentId',
            RegionName='$sRegionName',
            RegionCode='$sRegionCode',
            RegionDescription='$sRegionDescription',
            Notes='$sNotes',
            Status='$iStatus'

        WHERE RegionId='$iRegionId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {	       	
  			$objSystemLog->AddLog("Update Region - " . $sRegionName);

            $objGeneral->fnRedirect('../organization/regionalhierarchy_regions_details.php?error=3022&id=' . $iRegionId);
        }
        else
            return(3023);
    }

    function DeleteRegion($iRegionId)
    {
        global $objDatabase;
  		global $objEmployee;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_Regions[3] == 0) // Delete Disabled
			return(1010);


        if ($objDatabase->DBCount("organization_regions AS R", "R.RegionParentId = '$iRegionId'") > 0) return(3026);		
		if ($objDatabase->DBCount("organization_stations AS S", "S.RegionId = '$iRegionId'") > 0) return(3031);

		$varResult = $objDatabase->Query("SELECT * FROM organization_regions AS R WHERE R.RegionId='$iRegionId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die("Sorry, Invalid Region Id");

		$sRegionName = $objDatabase->Result($varResult, 0, "R.RegionName");

        $varResult = $objDatabase->Query("DELETE FROM organization_regions WHERE RegionId='$iRegionId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {
	       	
  			$objSystemLog->AddLog("Delete Region - " . $sRegionName);

            return(3024);
        }
        else
            return(3025);
    }
}

/* Region Types Class */
class clsRegions_Types extends clsOrganization
{
	function ShowAllRegionTypes($sSearch)
	{
		global $objDatabase;
		global $objGeneral;

		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

		$sTitle = 'Region Types';
        if ($sSortBy == "") $sSortBy = "RT.RegionTypeId DESC";

        if ($sSearch != "")
        {
        	$sSearch = mysql_escape_string($sSearch);
            $sSearchCondition = " AND ((RT.RegionTypeName LIKE '%$sSearch%') OR (RT.RegionTypeId LIKE '%$sSearch%')) ";
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        $iTotalRecords = $objDatabase->DBCount("organization_regions_types AS RT", "1=1 $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM organization_regions_types AS RT
		LEFT JOIN organization_regions_types AS RT2 ON RT2.RegionTypeId = RT.RegionParentId
        WHERE 1=1 $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td align="left"><span class="WhiteHeading">Region Type Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=RT.RegionTypeId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Region Type Id in Ascending Order" title="Sort by Region Type Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=RT.RegionTypeId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Region Type Id in Descending Order" title="Sort by Region Type Id in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Region Type Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=RT.RegionTypeName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Region Type Name in Ascending Order" title="Sort by Region Type Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=RT.RegionTypeName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Region Type Name in Descending Order" title="Sort by Region Type Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Region Parent Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=RT.RegionParentId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Region Parent in Ascending Order" title="Sort by Region Parent in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=RT.RegionParentId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Region Parent in Descending Order" title="Sort by Region Parent in Descending Order" border="0" /></a></span></td>
		 <td width="2%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iRegionTypeId = $objDatabase->Result($varResult, $i, "RT.RegionTypeId");
            $sRegionTypeName = $objDatabase->Result($varResult, $i, "RT.RegionTypeName");
            $sRegionParentName = $objDatabase->Result($varResult, $i, "RT2.RegionTypeName");

            if ($sRegionParentName == '') $sRegionParentName = 'No Parent';

            $sUpdateRegionType = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . mysql_escape_string($sRegionTypeName) . '\', \'../organization/regionalhierarchy_regiontypes_details.php?action2=edit&id=' . $iRegionTypeId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Region Type Details" title="Edit Region Type Details"></a></td>';
            $sDeleteRegionType = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Region Type?\')) {window.location = \'?action=DeleteRegionType&id=' . $iRegionTypeId . '&show=' . $iShow . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Region Type" title="Delete this Region Type"></a></td>';

            if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[2] == 0) // Update Disabled
            	$sUpdateRegionType = '';

            if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[3] == 0) // Delete Disabled
            	$sDeleteRegionType = '';

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $iRegionTypeId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sRegionTypeName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sRegionParentName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string($sRegionTypeName) . '\', \'../organization/regionalhierarchy_regiontypes_details.php?id=' . $iRegionTypeId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Region Type Details" title="View this Region Type Details"></a></td>
	 		 ' . $sUpdateRegionType . '
		 	 ' . $sDeleteRegionType . '
			</tr>';
		}

		$sAddNewRegionType = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Region Type\', \'../organization/regionalhierarchy_regiontypes_details.php?action2=addnew\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add Region Type">';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[1] == 0) // Add Disabled
			$sAddNewRegionType = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddNewRegionType . '
           <form method="GET" action="">&nbsp;&nbsp;&nbsp;Search for a Region Type:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Region Type" title="Search for a Region Type" border="0"></form>
          </td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Region Type Details" alt="View this Region Type Details"></td><td>View this Region Type Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Region Type Details" alt="Edit this Region Type Details"></td><td>Edit this Region Type Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Region Type" alt="Delete this Region Type"></td><td>Delete this Region Type</td></tr>
        </table>';

		return($sReturn);
	}

    function RegionTypeDetails($iRegionTypeId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM organization_regions_types AS RT
		LEFT JOIN organization_regions_types AS RT2 ON RT2.RegionTypeId = RT.RegionParentId
		WHERE RT.RegionTypeId = '$iRegionTypeId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iRegionTypeId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Region Type" title="Edit this Region Type" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Region Type?\')) {window.location = \'?action=DeleteRegionType&id=' . $iRegionTypeId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Region" title="Delete this Region" /></a>';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[2] == 0) // Update Disabled
         	$sButtons_Edit = '&nbsp;';

        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[3] == 0) // Delete Disabled
         	$sButtons_Delete = '&nbsp;';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Region Type Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iRegionTypeId = $objDatabase->Result($varResult, 0, "RT.RegionTypeId");
		        $sRegionTypeName = $objDatabase->Result($varResult, 0, "RT.RegionTypeName");
		        $iRegionParentId = $objDatabase->Result($varResult, 0, "RT.RegionParentId");

		        $sRegionParentName = $objDatabase->Result($varResult, 0, "RT2.RegionTypeName");
		        if ($sRegionParentName == '') $sRegionParentName = 'No Parent';
				else $sRegionParentName = $sRegionParentName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'' . $sRegionParentName .'\' , \'../organization/regionalhierarchy_regiontypes_details.php?id=' . $iRegionParentId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Region Type Parent Details" title="Region Type Parent Details" /></a>';
		    }

			if ($sAction2 == "edit")
			{
		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">
		        <script language="JavaScript" type="text/javascript">
		         function ValidateForm()
		         {
		         	if (GetVal(\'txtRegionTypeName\') == "") return(AlertFocus(\'Please enter a valid Region Type Name\', \'txtRegionTypeName\'));
		         	return true;
		         }
		        </script>';

				$sRegionTypeName = '<input type="text" name="txtRegionTypeName" id="txtRegionTypeName" class="form1" value="' . $sRegionTypeName . '" size="30" />';

		        $sRegionParentName = '<select class="form1" name="selRegionTypeParentId" id="selRegionTypeParentId">
		        <option value="0">No Parent</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM organization_regions_types AS RT WHERE RT.RegionTypeId <> '$iRegionTypeId'");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iRegionParentId == $objDatabase->Result($varResult2, $i, "RT.RegionTypeId"))
		                $sRegionParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . '</option>';
		            else
		                $sRegionParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . '</option>';
		        }
		        $sRegionParentName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selRegionTypeParentId\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selRegionTypeParentId\'), \'../organization/regionalhierarchy_regiontypes_details.php?id=\'+GetSelectedListBox(\'selRegionTypeParentId\'), \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Region Type Parent Details" title="Region Type Parent Details" /></a>';
			}
			else if ($sAction2 == "addnew")
			{
				$iRegionTypeId = "";
		        $sRegionTypeName = $objGeneral->fnGet("txtRegionTypeName");
		        $iRegionParentId = $objGeneral->fnGet("selRegionParentId");

		        $sRegionParentName = '<select class="form1" name="selRegionTypeParentId" id="selRegionTypeParentId">
		        <option value="0">No Parent</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM organization_regions_types AS RT WHERE RT.RegionTypeId <> '$iRegionTypeId'");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		            if ($iRegionParentId == $objDatabase->Result($varResult2, $i, "RT.RegionTypeId"))
		                $sRegionParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . '</option>';
		            else
		                $sRegionParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeId") . '">' . $objDatabase->Result($varResult2, $i, "RT.RegionTypeName") . '</option>';
		        $sRegionParentName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selRegionTypeParentId\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selRegionTypeParentId\'), \'../organization/regionalhierarchy_regiontypes_details.php?id=\'+GetSelectedListBox(\'selRegionTypeParentId\'), \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Region Type Parent Details" title="Region Type Parent Details" /></a>';

		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">
		        <script language="JavaScript" type="text/javascript">
		         function ValidateForm()
		         {
		         	if (GetVal(\'txtRegionTypeName\') == "")
		         		return(AlertFocus(\'Please enter a valid Region Type Name\', \'txtRegionTypeName\'));

		         	return true;
		         }
		        </script>';

				$sRegionTypeName = '<input type="text" name="txtRegionTypeName" id="txtRegionTypeName" class="form1" value="' . $sRegionTypeName . '" size="30" />';
			}

			$sReturn .= '
			<table border=0 cellspacing=0 cellpadding=5 width="100%" align=center>
		     <tr class="GridTR"><td colspan=2><span class="title2">Region Type Information:</span></td></tr>
			 <tr><td width="20%">Region Type Id:</td><td><strong>' . $iRegionTypeId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Region Type Name:</td><td><strong>' . $sRegionTypeName . '</strong></td></tr>
			 <tr><td>Region Parent Name:</td><td><strong>' . $sRegionParentName . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iRegionTypeId . '"><input type="hidden" name="action" id="action" value="UpdateRegionType"></form></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Region Type" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewRegionType"></form></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

    function AddNewRegionType($sRegionTypeName, $iRegionParentId)
    {
        global $objDatabase;
        global $objGeneral;
		global $objEmployee;
        global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[1] == 0) // Add Disabled
			return(1010);

        $sRegionTypeName = mysql_escape_string($sRegionTypeName);

        if ($objDatabase->DBCount("organization_regions_types AS RT", "RT.RegionTypeName='$sRegionTypeName'") > 0)
            return(3000);

        if (($iRegionParentId > 0) && ($objDatabase->DBCount("organization_regions_types AS RT", "RT.RegionTypeId='$iRegionParentId'") <= 0))
        	return(3001);

        $varResult = $objDatabase->Query("INSERT INTO organization_regions_types
        (RegionTypeName, RegionParentId)
        VALUES ('$sRegionTypeName', '$iRegionParentId')");

        $varResult = $objDatabase->Query("SELECT * FROM organization_regions_types AS RT WHERE RT.RegionTypeName='$sRegionTypeName'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {
        	
  			$objSystemLog->AddLog("Add New Region Type - " . $objDatabase->Result($varResult, 0, "RT.RegionTypeName"));
            $objGeneral->fnRedirect('?error=3002&id=' . $objDatabase->Result($varResult, 0, "RT.RegionTypeId"));
        }
        else
            return(3003);
    }

    function UpdateRegionType($iRegionTypeId, $sRegionTypeName, $iRegionParentId)
    {
        global $objDatabase;
        global $objEmployee;
        global $objGeneral;
		global $objEmployee;

        // Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[2] == 0) // Update Disabled
			return(1010);

        $sRegionTypeName = mysql_escape_string($sRegionTypeName);

        $varResult = $objDatabase->Query("
        UPDATE organization_regions_types SET
            RegionTypeName='$sRegionTypeName',
            RegionParentId='$iRegionParentId'

        WHERE RegionTypeId='$iRegionTypeId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Update Region Type - " . $sRegionTypeName);

            $objGeneral->fnRedirect('../organization/regionalhierarchy_regiontypes_details.php?error=3004&id=' . $iRegionTypeId);
        }
        else
            return(3005);
    }

    function DeleteRegionType($iRegionTypeId)
    {
        global $objDatabase;

        global $objEmployee;
		global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_RegionalHierarchy_RegionTypes[3] == 0) // Delete Disabled
			return(1010);

		$varResult = $objDatabase->Query("SELECT * FROM organization_regions AS R WHERE R.RegionTypeId='$iRegionTypeId'");
		if ($objDatabase->RowsNumber($varResult) > 0) return(3009);

		$varResult = $objDatabase->Query("SELECT * FROM organization_regions_types AS RT WHERE RT.RegionParentId='$iRegionTypeId'");
		if ($objDatabase->RowsNumber($varResult) > 0) return(3010);

		$varResult = $objDatabase->Query("SELECT * FROM organization_regions_types AS RT WHERE RT.RegionTypeId='$iRegionTypeId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Invalid Region Type Id');
		$sRegionTypeName = $objDatabase->Result($varResult, 0, "RT.RegionTypeName");

        $varResult = $objDatabase->Query("DELETE FROM organization_regions_types WHERE RegionTypeId='$iRegionTypeId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {	       	
  			$objSystemLog->AddLog("Delete Region Type - " . $sRegionTypeName);
            return(3006);
        }
        else
            return(3007);
    }
}
?>