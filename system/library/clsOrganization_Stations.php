<?php

/* Stations */
class clsStations extends clsOrganization
{
    // Constructor
    function __construct()
    {
    }

    function ShowAllStations($sSearch, $iStationType = 0)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = "Stations";
        if ($sSortBy == "") $sSortBy = "S.StationName";

        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((S.StationName LIKE '%$sSearch%') OR (S.StationCode LIKE '$sSearch') OR (S.City LIKE '%$sSearch%') OR (S.Country LIKE '%$sSearch%') OR (ST.TypeName='%$sSearch%'))";
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }
		
		if ($iStationType == '') $iStationType = 0;
        if ($iStationType > 0)
        {
			$varResult = $objDatabase->Query("SELECT * FROM fms_common_types AS ST WHERE ST.OrganizationId='" . cOrganizationId . "' AND ST.TypeId='$iStationType'");
         	if ($objDatabase->RowsNumber($varResult) > 0)
				$sTitle = "Stations as " . $objDatabase->Result($varResult, 0, "ST.TypeName");
	        $sStationTypeString = " AND S.StationType='$iStationType'";
        }
		
		$iTotalRecords = $objDatabase->DBCount("organization_stations AS S INNER JOIN fms_common_types AS ST ON ST.TypeId=S.StationType", "S.OrganizationId='" . cOrganizationId . "' AND ST.ComponentName='Organization_StationTypes' $sSearchCondition $sStationTypeString");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM organization_stations AS S
		INNER JOIN fms_common_types AS ST ON ST.TypeId = S.StationType
        WHERE S.OrganizationId='" . cOrganizationId . "' AND ST.ComponentName='Organization_StationTypes' $sSearchCondition $sStationTypeString
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td class="GridTD" align="left"><span class="WhiteHeading">Station Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=S.StationName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Station Name in Ascending Order" title="Sort by Station Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=S.StationName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Station Name in Descending Order" title="Sort by Station Name in Descending Order" border="0" /></a></span></td>
		 <td class="GridTD" align="left"><span class="WhiteHeading">Station Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=S.StationCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Station Code in Ascending Order" title="Sort by Station Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=S.StationCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Station Code in Descending Order" title="Sort by Station Code in Descending Order" border="0" /></a></span></td>
		 <td class="GridTD" align="left"><span class="WhiteHeading">Station Type&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=S.StationType&sortorder="><img src="../images/sort_up.gif" alt="Sort by Station Type Id in Ascending Order" title="Sort by Station Type Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=S.StationType&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Station Type Id in Descending Order" title="Sort by Station Type Id in Descending Order" border="0" /></a></span></td>
		 <td class="GridTD" align="left"><span class="WhiteHeading">City&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=S.City&sortorder="><img src="../images/sort_up.gif" alt="Sort by Station City in Ascending Order" title="Sort by Station City in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=S.City&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Station City in Descending Order" title="Sort by Station City in Descending Order" border="0" /></a></span></td>
		 <td class="GridTD" width="4%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			$iStationId = $objDatabase->Result($varResult, $i, "S.StationId");
			$sStationCode = $objDatabase->Result($varResult, $i, "S.StationCode");
            $sStationName = $objDatabase->Result($varResult, $i, "S.StationName");
            $sStationCity = $objDatabase->Result($varResult, $i, "S.City");
			$sStationState = $objDatabase->Result($varResult, $i, "S.State");
			$sStationTypeName = $objDatabase->Result($varResult, $i, "ST.TypeName");
			$sStationCountry = $objDatabase->Result($varResult, $i, "S.Country");
			$sPhoneNumber = $objDatabase->Result($varResult, $i, "S.PhoneNumber");
			$sLatitude = $objDatabase->Result($varResult, $i, "S.Latitude");
			$sLongitude = $objDatabase->Result($varResult, $i, "S.Longitude");

            $sEditStation = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Station\', \'../organization/stations.php?pagetype=details&action2=edit&id=' . $iStationId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Station Details" title="Edit this Station Details"></a></td>';
            $sDeleteStation = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Station?\')) {window.location = \'?action=DeleteStation&id=' . $iStationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Station" title="Delete this Station"></a></td>';
			$sStationMap = '<td class="GridTD" align="center"><a href="#noanchor" onclick="jsOpenWindow(\'../organization/googlemap.php?stationname=' . urlencode($sStationName) . '&city=' . $sStationCity . '&state=' . $sStationState . '&country=' . $sStationCountry . '&phone=' . $sPhoneNumber . '&latitude=' . $sLatitude . '&longitude=' . $sLongitude . '\', 800,600);"><img src="../images/icons/iconMap.png" border="0" alt="Google Map" title="Google Map"></a></td>';

    	    // Employee Roles
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[2] == 0)	$sEditStation = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[3] == 0)	$sDeleteStation = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->iEmployeeRole_Organization_Stations_StationMap[0] == 0)	$sStationMap = '<td class="GridTD">&nbsp;</td>';

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sStationName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStationCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStationTypeName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStationCity . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sStationName). '\', \'../organization/stations.php?pagetype=details&id=' . $iStationId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Station Details" title="View this Station Details"></a></td>
			 ' . $sEditStation . '
 			 '. $sStationMap . '
			 ' . $sDeleteStation . '
			</tr>';
		}

		$sAddNewStation = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add New Station\', \'../organization/stations.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add New Station">';

        // Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[1] == 0) // Update Disabled
			$sAddNewStation = '';

		$varResult = $objDatabase->Query("SELECT * FROM fms_common_types AS ST WHERE ST.OrganizationId='" . cOrganizationId . "' AND ST.ComponentName='Organization_StationTypes'");
		$sStationTypeSelect = '<select class="form1" name="selStationType" onchange="window.location=\'?stationtype=\'+GetSelectedListBox(\'selStationType\');" id="selStationType">
		<option value="0">All Station Types</option>';
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sStationTypeSelect .= '<option ' . (($iStationType == $objDatabase->Result($varResult, $i, "ST.TypeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "ST.TypeId") . '">' . $objDatabase->Result($varResult, $i, "ST.TypeName") . '</option>';
		$sStationTypeSelect .= '</select>';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
          <td align="left">' . $sAddNewStation . '
          &nbsp;&nbsp;
          <form method="GET" action="">Search for a Station:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Station" title="Search for Station" border="0"></form>
          </td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>' . $sStationTypeSelect . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Station Details" alt="View this Station Details"></td><td>View this Station Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Station Details" alt="Edit this Station Details"></td><td>Edit this Station Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Station" alt="Delete this Station"></td><td>Delete this Station</td></tr>
        </table>';

		return($sReturn);
    }

    function StationDetails($iStationId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM organization_stations AS S
		INNER JOIN fms_common_types AS ST ON ST.TypeId = S.StationType
		INNER JOIN organization_employees AS E ON E.EmployeeId = S.StationAddedBy
		LEFT  JOIN organization_stations AS S2 ON S2.StationId = S.StationParentId
		WHERE S.OrganizationId='" . cOrganizationId . "' AND S.StationId='$iStationId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iStationId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Station" title="Edit this Station" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Station?\')) {window.location = \'?action=DeleteStation&id=' . $iStationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Station" title="Delete this Station" /></a>';

        // Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[3] == 0)	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Station Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iStationId = $objDatabase->Result($varResult, 0, "S.StationId");
		        $sStationName = $objDatabase->Result($varResult, 0, "S.StationName");

		        // Station Address
		        $sAddress = $objDatabase->Result($varResult, 0, "S.Address");
		        $sCity = $objDatabase->Result($varResult, 0, "S.City");
		        $sState = $objDatabase->Result($varResult, 0, "S.State");
		        $sZipCode = $objDatabase->Result($varResult, 0, "S.ZipCode");
		        $sCountryCode = $objDatabase->Result($varResult, 0, "S.Country");
				$sCountry = $objGeneral->aCountriesList[$sCountryCode];
		        $sStationCode = $objDatabase->Result($varResult, 0, "S.StationCode");
		        $sNotes = $objDatabase->Result($varResult, 0, "S.Notes");

				$dStationAddedOn = $objDatabase->Result($varResult, 0, "S.StationAddedOn");
				$sStationAddedOn = date("F j, Y", strtotime($dStationAddedOn)) . ' at ' . date("g:i a", strtotime($dStationAddedOn));

				$iStationAddedBy = $objDatabase->Result($varResult, 0, "E.EmployeeId");
				$sStationAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sStationAddedBy = $sStationAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sStationAddedBy)) . '\', \'../employees/employees.php?pagetype=details&id=' . $iStationAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

				$iStationParentId = $objDatabase->Result($varResult, 0, "S.StationParentId");
				$iStationType = $objDatabase->Result($varResult, 0, "S.StationType");
				$sStationTypeName = $objDatabase->Result($varResult, 0, "ST.TypeName");
				
				$sStationTimeZone = $objDatabase->Result($varResult, 0, "S.StationTimeZone");
				$sStationTimeZoneString = $objGeneral->aTimeZones[$sStationTimeZone];
				
				$sStationCurrency = $objDatabase->Result($varResult, 0, "S.StationCurrency");
				$sStationCurrencyString = $objGeneral->aCurrencies[$sStationCurrency];
				$sStationCurrencySign = $objDatabase->Result($varResult, 0, "S.StationCurrencySign");

				$sParentStation = $objDatabase->Result($varResult, 0, "S2.StationName");

				if ($sParentStation == '') $sParentStation = "No Parent";
				else $sParentStation = $sParentStation . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sParentStation . '\', \'../organization/stations.php?pagetype=details&id=' . $iStationParentId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Station Information" title="Parent Station Information" border="0" /></a>';

				$sPhoneNumber = $objDatabase->Result($varResult, 0, "S.PhoneNumber");
				$sFaxNumber = $objDatabase->Result($varResult, 0, "S.FaxNumber");
				$sEmailAddress = $objDatabase->Result($varResult, 0, "S.EmailAddress");
				$sWebsite = $objDatabase->Result($varResult, 0, "S.Website");
				$sLatitude = $objDatabase->Result($varResult, 0, "S.Latitude");
				$sLongitude = $objDatabase->Result($varResult, 0, "S.Longitude");
		    }

			if ($sAction2 == "edit")
			{
		        $sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountryCode);
		        $sCountryString .= '</select>';

   		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

				$sStationName = '<input type="text" name="txtStationName" id="txtStationName" class="form1" value="' . $sStationName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sStationCode = '<input type="text" name="txtStationCode" id="txtStationCode" class="form1" value="' . $sStationCode . '" size="18" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />';
				$sEmailAddress = '<input type="text" name="txtEmailAddress" id="txtEmailAddress" class="form1" value="' . $sEmailAddress . '" size="30" />';
				$sWebsite = '<input type="text" name="txtWebsite" id="txtWebsite" class="form1" value="' . $sWebsite . '" size="30" />';
				
				$sLatitude = '<input type="text" name="txtLatitude" id="txtLatitude" class="form1" value="' . $sLatitude . '" size="30" />';
				$sLongitude = '<input type="text" name="txtLongitude" id="txtLongitude" class="form1" value="' . $sLongitude . '" size="30" />';

				$sFaxNumber = '<input type="text" name="txtFaxNumber" id="txtFaxNumber" class="form1" value="' . $sFaxNumber . '" size="30" />';
				$sCountry = $sCountryString;

				$sStationTypeName = '<select class="form1" name="selStationType" id="selStationType">';
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_common_types AS ST WHERE ST.OrganizationId='" . cOrganizationId . "' AND ST.ComponentName='Organization_StationTypes'");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sStationTypeName .= '<option ' . (($iStationType == $objDatabase->Result($varResult2, $i, "ST.TypeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "ST.TypeId") . '">' . $objDatabase->Result($varResult2, $i, "ST.TypeName") . '</option>';
				$sStationTypeName .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/types.php?componentname=Organization_StationTypes\', \'Station Types\', \'700 420\');"><img src="../images/icons/plus.gif" align="center" border="0" alt="Manage Station Types" title="Manage Station Types" /></a>';

				$sParentStation = '<select class="form1" name="selStationParentId" id="selStationParentId">
				<option value="0">No Parent</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' AND S.StationId <> '$iStationId' ORDER BY S.StationId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sParentStation .= '<option ' . (($iStationParentId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
				$sParentStation .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selStationParentId\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStationParentId\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStationParentId\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Station Information" title="Parent Station Information" border="0" /></a>';
				
				$sStationTimeZoneString = '<select class="form1" name="selStationTimeZone" id="selStationTimeZone">';
				$sStationTimeZoneString .= $objGeneral->TimeZonesList($sStationTimeZone);
				$sStationTimeZoneString .= '</select>';
				
				$sStationCurrencyString = '<select class="form1" name="selStationCurrency" id="selStationCurrency">';
				$sStationCurrencyString .= $objGeneral->fnCurrencyOptionsList($sStationCurrency);
				$sStationCurrencyString .= '</select>';
				
				$sStationCurrencySign = '<input type="text" name="txtStationCurrencySign" id="txtStationCurrencySign" class="form1" value="' . $sStationCurrencySign . '" size="15" />';
			}
			else if ($sAction2 == "addnew")
			{
				$iStationId = "";
		        $sStationName = $objGeneral->fnGet("txtStationName");
		        $sAddress = $objGeneral->fnGet("txtAddress");
		        $sCity = $objGeneral->fnGet("txtCity");
		        $sZipCode = $objGeneral->fnGet("txtZipCode");
		        $sState = $objGeneral->fnGet("txtState");
		        $sCountry = $objGeneral->fnGet("txtCountry");
				$sStationTimeZone = $objGeneral->fnGet("selStationTimeZone");
				$sStationCurrency = $objGeneral->fnGet("selStationCurrency");
				$sStationCurrencySign = $objGeneral->fnGet("txtStationCurrencySign");
		        $sStationCode = $objGeneral->fnGet("txtStationCode");
		        $sNotes = $objGeneral->fnGet("txtNotes");
				$sPhoneNumber = $objGeneral->fnGet("txtPhoneNumber");
		        $sEmailAddress = $objGeneral->fnGet("txtEmailAddress");
				$sFaxNumber = $objGeneral->fnGet("txtFaxNumber");
		        $sWebsite = $objGeneral->fnGet("txtWebsite");
				$sLatitude = $objGeneral->fnGet("txtLatitude");
				$sLongitude = $objGeneral->fnGet("txtLongitude");

				$dStationAddedOn = $objGeneral->fnNow();
				$sStationAddedOn = date("F j, Y", strtotime($dStationAddedOn)) . ' at ' . date("g:i a", strtotime($dStationAddedOn));

				$sStationAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sStationAddedBy = $sStationAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sStationAddedBy)) . '\', \'../employees/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

		        $sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';

   		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

				$sStationName = '<input type="text" name="txtStationName" id="txtStationName" class="form1" value="' . $sStationName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sStationCode = '<input type="text" name="txtStationCode" id="txtStationCode" class="form1" value="' . $sStationCode . '" size="13	" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />';
				$sEmailAddress = '<input type="text" name="txtEmailAddress" id="txtEmailAddress" class="form1" value="' . $sEmailAddress . '" size="30" />';
				$sWebsite = '<input type="text" name="txtWebsite" id="txtWebsite" class="form1" value="' . $sWebsite . '" size="30" />';
				$sLatitude = '<input type="text" name="txtLatitude" id="txtLatitude" class="form1" value="' . $sLatitude . '" size="30" />';
				$sLongitude = '<input type="text" name="txtLongitude" id="txtLongitude" class="form1" value="' . $sLongitude . '" size="30" />';
				$sFaxNumber = '<input type="text" name="txtFaxNumber" id="txtFaxNumber" class="form1" value="' . $sFaxNumber . '" size="30" />';
				$sCountry = $sCountryString;

				$sStationTypeName = '<select class="form1" name="selStationType" id="selStationType">';
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_common_types AS ST WHERE ST.OrganizationId='" . cOrganizationId . "' AND ST.ComponentName='Organization_StationTypes'");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sStationTypeName .= '<option ' . (($iStationType == $objDatabase->Result($varResult2, $i, "ST.TypeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "ST.TypeId") . '">' . $objDatabase->Result($varResult2, $i, "ST.TypeName") . '</option>';
				$sStationTypeName .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/types.php?componentname=Organization_StationTypes\', \'Station Types\', \'700 420\');"><img src="../images/icons/plus.gif" align="center" border="0" alt="Manage Station Types" title="Manage Station Types" /></a>';

				$sParentStation = '<select class="form1" name="selStationParentId" id="selStationParentId">
				<option value="0">No Parent</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' AND S.StationId <> '$iStationId' ORDER BY S.StationId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sParentStation .= '<option ' . (($iStationParentId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
				$sParentStation .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selStationParentId\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStationParentId\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStationParentId\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Station Information" title="Parent Station Information" border="0" /></a>';
				
				$sStationTimeZoneString = '<select class="form1" name="selStationTimeZone" id="selStationTimeZone">';
				$sStationTimeZoneString .= $objGeneral->TimeZonesList($sStationTimeZone);
				$sStationTimeZoneString .= '</select>';

				$sStationCurrencyString = '<select class="form1" name="selStationCurrency" id="selStationCurrency">';
				$sStationCurrencyString .= $objGeneral->fnCurrencyOptionsList($sStationCurrency);
				$sStationCurrencyString .= '</select>';
				$sStationCurrencySign = '<input type="text" name="txtStationCurrencySign" id="txtStationCurrencySign" class="form1" value="' . $sStationCurrencySign . '" size="15" />';
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
	        function ValidateForm()
	        {
	        	if (GetVal(\'txtStationName\') == "") return(AlertFocus(\'Please enter a valid Station Name\', \'txtStationName\'));
	        	if (GetVal(\'txtStationCode\') == "") return(AlertFocus(\'Please enter a valid Station Station Code\', \'txtStationCode\'));

	        	return true;
	        }
	        </script> 
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="GridTR"><td colspan="3"><span class="title2">Station Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td style="width:200px;" valign="top">Station Type:</td><td>' . $sStationTypeName . '</td></tr>
			 <tr bgcolor="#edeff1"><td>Station Name:</td><td>' . $sStationName . '</td></tr>
			 <tr bgcolor="#ffffff"><td>Station Code:</td><td>' . $sStationCode . '</td></tr>
			 <tr bgcolor="#edeff1"><td>Parent Station:</td><td>' . $sParentStation . '</td></tr>
			 <tr bgcolor="#ffffff"><td>Station Time Zone:</td><td>' . $sStationTimeZoneString . '</td></tr>
			 <tr bgcolor="#edeff1"><td>Station Currency:</td><td>' . $sStationCurrencyString . '</td></tr>
			 <tr bgcolor="#ffffff"><td>Station Currency Sign:</td><td>' . $sStationCurrencySign . '</td></tr>
		     <tr class="GridTR"><td colspan="2"><span class="title2">Station Address:</span></td></tr>
			 <tr><td>Address:</td><td>' . $sAddress . '</td></tr>
			 <tr bgcolor="#edeff1"><td>City:</td><td>' . $sCity . '</td></tr>
			 <tr><td>State/Province:</td><td>' . $sState . '</td></tr>
			 <tr bgcolor="#edeff1"><td>Zip Code:</td><td>' . $sZipCode . '</td></tr>
			 <tr><td>Country:</td><td>' . $sCountry . '</td></tr>
			 <tr bgcolor="#edeff1"><td>Phone Number:</td><td>' . $sPhoneNumber . '</td></tr>
			 <tr><td>Fax Number:</td><td>' . $sFaxNumber . '</td></tr>
			 <tr bgcolor="#edeff1"><td>Email Address:</td><td>' . $sEmailAddress . '</td></tr>
			 <tr><td>Website:</td><td>' . $sWebsite . '</td></tr>
			 <tr bgcolor="#edeff1"><td>Latitude:</td><td>' . $sLatitude . '</td></tr>
			 <tr><td>Longitude:</td><td>' . $sLongitude . '</td></tr>
		     <tr class="GridTR"><td colspan="2"><span class="title2">Extra Information:</span></td></tr>
		     <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td>' . $sNotes . '</td></tr>
			 <tr bgcolor="#edeff1"><td>Station Added On:</td><td>' . $sStationAddedOn . '</td></tr>
			 <tr bgcolor="#ffffff"><td>Station Added By:</td><td>' . $sStationAddedBy . '</td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Station" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iStationId . '"><input type="hidden" name="action" id="action" value="UpdateStation"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add New Station" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewStation"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

    function AddNewStation($sStationName, $iStationType, $iStationParentId, $sStationTimeZone, $sStationCurrency, $sStationCurrencySign, $sStationCode, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sFaxNumber, $sEmailAddress, $sWebsite, $sLatitude, $sLongitude, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;		
		global $objEmployee;
		global $objSystemLog;

        $varNow = $objGeneral->fnNow();
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[1] == 0) // Add Disabled
			return(1010);

        $sStationName = $objDatabase->RealEscapeString($sStationName);
		$sStationCode = $objDatabase->RealEscapeString($sStationCode);
		$sStationCurrencySign = $objDatabase->RealEscapeString($sStationCurrencySign);
		$sStationTimeZone = $objDatabase->RealEscapeString($sStationTimeZone);
		$sStationCurrency = $objDatabase->RealEscapeString($sStationCurrency);
        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
        $sCountry = $objDatabase->RealEscapeString($sCountry);
        $sEmailAddress = $objDatabase->RealEscapeString($sEmailAddress);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		$sFaxNumber = $objDatabase->RealEscapeString($sFaxNumber);
		$sWebsite = $objDatabase->RealEscapeString($sWebsite);
		$sLatitude = $objDatabase->RealEscapeString($sLatitude);
		$sLongitude = $objDatabase->RealEscapeString($sLongitude);
		
		if ($objDatabase->DBCount("organization_stations AS S", "S.OrganizationId='" . cOrganizationId . "' AND S.StationCode='$sStationCode'"))
			return(3100);

        $varResult = $objDatabase->Query("INSERT INTO organization_stations
        (OrganizationId, StationName, StationType, StationParentId, StationTimeZone, StationCurrency, StationCurrencySign, StationCode, Address, City, State, ZipCode, Country, PhoneNumber, FaxNumber, EmailAddress, Website, Latitude, Longitude, Notes, StationAddedOn, StationAddedBy)
        VALUES
        ('" . cOrganizationId . "', '$sStationName', '$iStationType', '$iStationParentId', '$sStationTimeZone', '$sStationCurrency', '$sStationCurrencySign', '$sStationCode', '$sAddress', '$sCity', '$sState', '$sZipCode', '$sCountry', '$sPhoneNumber', '$sFaxNumber', '$sEmailAddress', '$sWebsite', '$sLatitude', '$sLongitude', '$sNotes', '$varNow', '$objEmployee->iEmployeeId')");

        $varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' AND S.StationName='$sStationName' AND S.StationAddedOn='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {
			$objSystemLog->AddLog("Add New Station - " . $sStationName);
            $objGeneral->fnRedirect('?pagetype=details&error=3101&id=' . $objDatabase->Result($varResult, 0, "S.StationId"));
        }
        else
            return(3102);
    }

    function UpdateStation($iStationId, $sStationName, $iStationType, $iStationParentId, $sStationTimeZone, $sStationCurrency, $sStationCurrencySign, $sStationCode, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sFaxNumber, $sEmailAddress, $sWebsite, $sLatitude, $sLongitude, $sNotes)
    {
        global $objDatabase;
        global $objEmployee;
        global $objGeneral;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[2] == 0) // Update Disabled
			return(1010);

        $sStationName = $objDatabase->RealEscapeString($sStationName);
		$sStationCode = $objDatabase->RealEscapeString($sStationCode);
		$sStationTimeZone = $objDatabase->RealEscapeString($sStationTimeZone);
		$sStationCurrency = $objDatabase->RealEscapeString($sStationCurrency);
		$sStationCurrencySign = $objDatabase->RealEscapeString($sStationCurrencySign);
        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
        $sCountry = $objDatabase->RealEscapeString($sCountry);
		$sEmailAddress = $objDatabase->RealEscapeString($sEmailAddress);
        $sPhoneNumber = $objDatabase->RealEscapeString($sPhoneNumber);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		$sFaxNumber = $objDatabase->RealEscapeString($sFaxNumber);
		$sWebsite = $objDatabase->RealEscapeString($sWebsite);
		$sLatitude = $objDatabase->RealEscapeString($sLatitude);
		$sLongitude = $objDatabase->RealEscapeString($sLongitude);
		
		if ($objDatabase->DBCount("organization_stations AS S", "S.OrganizationId='" . cOrganizationId . "' AND S.StationId <> '$iStationId' AND S.StationCode='$sStationCode'"))
			return(3100);
			
        $varResult = $objDatabase->Query("
        UPDATE organization_stations SET
        	StationType='$iStationType',
        	StationParentId='$iStationParentId',
			StationTimeZone='$sStationTimeZone',
			StationCurrency='$sStationCurrency',
			StationCurrencySign='$sStationCurrencySign',
            StationName='$sStationName',
            StationCode='$sStationCode',
            Address='$sAddress',
            City='$sCity',
            State='$sState',
            ZipCode='$sZipCode',
            Country='$sCountry',
            PhoneNumber='$sPhoneNumber',
			FaxNumber='$sFaxNumber',
            EmailAddress='$sEmailAddress',
			Website='$sWebsite',
			Latitude='$sLatitude',
			Longitude='$sLongitude',
            Notes='$sNotes'
        WHERE OrganizationId='" . cOrganizationId . "' AND StationId='$iStationId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {
			$objSystemLog->AddLog("Update Station - " . $sStationName);
            $objGeneral->fnRedirect('?pagetype=details&error=3103&id=' . $iStationId);
        }
        else
            return(3104);
    }

    function DeleteStation($iStationId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_Stations[3] == 0) // Delete Disabled
			return(1010);

		if ($objDatabase->DBCount("organization_employees AS E", "E.StationId='$iStationId'") > 0) return(3109);

		$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' AND S.StationId='$iStationId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Station Id');
		$sStationName = $objDatabase->Result($varResult, 0, "S.StationName");

        $varResult = $objDatabase->Query("DELETE FROM organization_stations WHERE StationId='$iStationId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {
			$objSystemLog->AddLog("Delete Station - " . $sStationName);
            return(3105);
        }
        else
            return(3106);
    }
	
	function GetStationAccess()
	{
		global $objDatabase;
		global $objEmployee;
		$iEmployeeId = $objEmployee->iEmployeeId;		
		
		$varResult = $objDatabase->Query("SELECT * FROM organization_employees_stations AS ES WHERE ES.EmployeeId= '$iEmployeeId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iNewStationId = $objDatabase->Result($varResult, $i, "ES.StationId");
				if($i == 0)	$sReturn .= " AND ((S.StationId = '$iNewStationId')";
				else
					$sReturn .= " OR (S.StationId = '" . $iNewStationId . "')";
			}
			
			$sReturn .= ")";
		}
		else
			$sReturn = "";		
		
		return($sReturn);
	}
	
	// Stations List
	function StationsList()
	{
		global $objDatabase;
		global $objEmployee;
		$iEmployeeId = $objEmployee->iEmployeeId;
		
		$sStationAccess = $this->GetStationAccess();
		/*
		if ($objDatabase->DBCount("organization_employees_stations AS ES", "ES.EmployeeId= '$iEmployeeId'") > 0)
		{
			$sEmployeesStationsTable = "INNER JOIN organization_employees_stations AS ES ON ES.StationId = S.StationId";
			$sStationRestriction = " AND ES.EmployeeId='$objEmployee->iEmployeeId'";
		}
		*/
			
		$aStations = array();
		$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S
		WHERE S.OrganizationId='" . cOrganizationId . "' AND S.Status='1' $sStationAccess");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$aStations[$i][0] = $objDatabase->Result($varResult, $i, "S.StationId");
			$aStations[$i][1] = $objDatabase->Result($varResult, $i, "S.StationName");
			$aStations[$i][2] = $objDatabase->Result($varResult, $i, "S.StationCode");
		}
		
		return($aStations);
	} 

}

?>