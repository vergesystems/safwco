<?php

class clsSystemLog
{
	function __construct() { }

	function AddLog($sEmployeeAction)
	{
		global $objGeneral;
		global $objEmployee;
		
		$sSystemLogFolder = cSystemLogFolder;

		$iEmployeeId = $objEmployee->iEmployeeId;
		$objEmployee->EmployeeVerify();
		if ($iEmployeeId == '') $iEmployeeId = $_SESSION[cProjectUniqueName . "_EmployeeId"];

		//$objEnvironmentDetails = new clsEnvironmentDetails();
		$sIPAddress = $_ENV['REMOTE_ADDR'];
		$sEmployeeUserName = $objEmployee->sEmployeeUserName;
		
		$sEmployeeAction = str_replace(",", " " , $sEmployeeAction);
		$sLogContents = date("F j Y, g:i a") . ',' . 
		$iEmployeeId . ',' . 
		$sEmployeeUserName . ',' . 
		$sIPAddress . ',' . 
		$sEmployeeAction . "\n";

		$fHandle = @fopen($sSystemLogFolder . '/' . date("F Y") . '.log','a');
		$bBytes = @fwrite($fHandle, $sLogContents);
		fclose($fHandle);
		
		return(true);
	}
	
	function ShowSystemLog($sSystemLogMonth)
	{		
		global $objDatabase;
		global $objGeneral;
		
		$sSystemLogFolder = cSystemLogFolder;
		
		$sReturn = '<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr>
		 <td colspan="4" class="heading">' . $sSystemLogMonth . '</td>
		</tr>
		<tr class="GridTR">
		 <td width="15%" align="left"><span class="WhiteHeading">Time</span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Employee Id</span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">IP Address</span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Action</span></td>
		</tr>';
		
		$fHandle = @fopen($sSystemLogFolder . '/' . $sSystemLogMonth . '.log', 'r');
		if ($fHandle)
		{			
			while (!feof($fHandle))
			{
				$buffer = fgets($fHandle, 4096);
				$aTemp = explode("," , $buffer);
				
				if ($buffer == "") continue;
				
				$sDate = $aTemp[0];
				$sTime = $aTemp[1];
				$iEmployeeId = $aTemp[2];
				$sEmployeeUserName = $aTemp[3];
				$sIPAddress = $aTemp[4];
				$sUserAction = $aTemp[5];
								
				if(substr($sUserAction, 0, 29) == 'Add New General Journal Entry')												  
				{
					$aDate = explode(" ", $sDate);				
					$aTime = explode(" ", $sTime);
					
					$sDate2 = $aDate[1];				
					$sMonth2 = date('m',strtotime($aDate[0]));				
					$sYear2 = $aDate[2];																											
					$dDate = $sYear2 . '-' . $sMonth2 . '-' . $sDate2 . ' ' . $aTime[1];
					
					$varResult2 = $objDatabase->Query("
					SELECT
					GJ.GeneralJournalId AS 'GeneralJournalId',
					GJ.Reference AS 'Reference'
					FROM fms_accounts_generaljournal AS GJ		
					WHERE 1=1 
					AND GJ.GeneralJournalAddedBy = '$iEmployeeId'
					AND (GJ.GeneralJournalAddedOn BETWEEN '$dDate:00' AND '$dDate:59')");
					if($objDatabase->RowsNumber($varResult2)>0)
					{					
						$iGeneralJournalId = $objDatabase->Result($varResult2, 0, "GeneralJournalId");
						$sReference = $objDatabase->Result($varResult2, 0, "Reference");
						$sReceipt = '<a href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&id=' . $iGeneralJournalId . '&txtReference='. $sReference .'\', 800,600);"><img src="../images/icons/iconPrint2.gif" border="0" alt="Receipt" title="Receipt"></a>';					
					}
				} 
				else $sReceipt = '';		
									
				$sReturn .= '<tr>
				 <td>' . $sDate . ', ' . $sTime . '</td>
				 <td>' . $sEmployeeUserName . ' (Id = ' . $iEmployeeId . ')&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sEmployeeUserName)) . '\', \'../employees/employees_details.php?id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="middle" alt="Employee Information" title="Employee Information" border="0" /></a></td>
				 <td>' . $sIPAddress . '</td>
				 <td>' . $sUserAction . $sReceipt . '</td>
				</tr>';
			}			
			fclose($fHandle);
		}		
		$sReturn .= '</table>';
		return($sReturn);
	}
	
    function ShowAllSystemLogs($sSearch)
    {
		global $objGeneral;
		global $objEmployee;
		
		$sSystemLogFolder = cSystemLogFolder;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Organization_SystemLog[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;
		
		$sTitle = "System Log";

        if ($sSearch != "")
        {
			$sSearch = mysql_escape_string($sSearch);
        
			// If there is a space in customer search
    		if (strpos($sSearch, ' ') !== false)
    		{
    			$aSearch = explode(' ', $sSearch);
    			if (count($aSearch) > 0)
    				$sSearch2 = $aSearch[0];
    		}
    		else
    			$sSearch2 = $sSearch;
			
			$sSearch = stripcslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
        }

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		//$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$iCounter = 0;
        $aTemp = $objGeneral->getFiles($sSystemLogFolder);
		
		for ($i=0; $i < count($aTemp); $i++)
		{
			$aLogFiles[$i][0] = $aTemp[$i];
			$aLogFiles[$i][1] = filemtime($aTemp[$i]);
		}
		
		$aLogFiles = $objGeneral->SortArray($aLogFiles, 1, "desc");
		
        for ($i=0; $i < count($aLogFiles); $i++)
        {
			$aFile = explode('/', $aLogFiles[$i][0]);
            $sFileName = $aFile[count($aFile)-1];
			
			if (strpos($sFileName, '.log') === false) continue;
			$sFileName = str_replace('.log', '', $sFileName);

			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

            $sLogFiles .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sFileName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
		 	 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Log Details\', \'../organization/systemlog_details.php?date=' . urlencode($sFileName) . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this System Log Details" title="View this System Log Details"></a></td>
			</tr>';
			
			$iCounter++;
		}
		
		$iTotalRecords = $iCounter;

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td width="15%" align="left"><span class="WhiteHeading">Date</span></td>
		 <td width="1%"><span class="WhiteHeading">Operations</span></td>
		</tr>' . $sLogFiles;

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Log Details" alt="View this Log Details"></td><td>View this Log Details</td></tr>
        </table>';
		//<form method="GET" action=""><td align="left" colspan="2">Search for a Log Entry:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Log Entry" title="Search for a Log Entry" border="0"></td></form>

		return($sReturn);
    }

}

class clsEnvironmentDetails
{
	public $sComputerOperatingSystem;

}

?>