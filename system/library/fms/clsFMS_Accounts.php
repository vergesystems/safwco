<?php

include('../../system/library/fms/clsFMS_Accounts_ChartOfAccounts.php');
include('../../system/library/fms/clsFMS_Accounts_Budget.php');

class clsAccounts
{
    // Constructor
    function __construct()
    {
    }
    
    function ShowAccountsMenu($sPage)
    {
    	global $objEmployee;   	
    	
		//$sSourceOfIncome = '<a ' . (($sCurrentFile == "sourceofincome.php") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../accounts/sourceofincome.php"><img src="../images/accounts/iconSourceOfIncome.gif" alt="Source Of Income" title="Source Of Income" border="0" /><br />Source Of Income</a>';
    	$sProducts = '<a ' . (($sPage == "Products") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../accounts/?page=Products"><img src="../images/accounts/iconProducts.gif" alt="Products" title="Products" border="0" /><br />Products</a>';
    	$sAccountsRegister = '<a ' . (($sPage == "AccountsRegister") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../accounts/?page=AccountsRegister"><img src="../images/accounts/iconAccountsRegister.gif" alt="Accounts Register" title="Accounts Register" border="0" /><br />Accounts Register</a>';
    	$sGeneralJournal = '<a ' . (($sPage == "GeneralJournal") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../accounts/?page=GeneralJournal"><img src="../images/accounts/iconGeneralJournal.gif" alt="General Journal" title="General Journal" border="0" /><br />General Journal</a>';
		//$sGeneralLedger = '<a ' . (($sPage == "GeneralLedger") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../accounts/?page=GeneralLedger"><img src="../images/accounts/iconGeneralLedger.gif" alt="General Ledger" title="General Ledger" border="0" /><br />General Ledger</a>';
    	$sChartOfAccounts = '<a ' . (($sPage == "ChartOfAccounts") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../accounts/?page=ChartOfAccounts"><img src="../images/accounts/iconChartOfAccounts.gif" alt="Chart Of Accounts" title="Chart Of Accounts" border="0" /><br />Chart Of Accounts</a>';
		$sDonors = '<a ' . (($sPage == "Donors") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../accounts/?page=Donors"><img src="../images/accounts/iconDonors.gif" alt="Donors" title="Donors" border="0" /><br />Donors</a>';
		$sCloseFinancialYear = '<a ' . (($sPage == "CloseFinancialYear") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../accounts/?page=CloseFinancialYear"><img src="../images/accounts/iconCloseFinancialYear.gif" alt="Close Financial Year" title="Close Financial Year" border="0" /><br />Close Financial Year</a>';
		$sQuickEntries = '<a ' . (($sPage == "QuickEntries") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../accounts/?page=QuickEntries"><img src="../images/accounts/iconQuickEntries.gif" alt="Quick Entries" title="Quick Entries" border="0" /><br />Quick Entries</a>';
		
    	$sBudget = '<a ' . (($sPage == "Budget") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../accounts/?page=Budget"><img src="../images/accounts/iconBudget.gif" alt="Budget" title="Budget" border="0" /><br />Budget</a>';
		$sBudgetAllocation = '<a ' . (($sPage == "BudgetAllocation") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../accounts/?page=BudgetAllocation"><img src="../images/accounts/iconBudgetAllocation.gif" alt="Budget Allocation" title="Budget Allocation" border="0" /><br />Budget Allocation</a>';
    	    	    	
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[0] == 0)
    		$sChartOfAccounts = '<img src="../images/accounts/iconChartOfAccounts_disabled.gif" alt="Chart Of Accounts" title="Chart Of Accounts" border="0" /><br />Chart Of Accounts';
			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[0] == 0)
    		$sGeneralJournal = '<img src="../images/accounts/iconGeneralJournal.gif" alt="General Journal" title="General Journal" border="0" /><br />General Journal';	
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralLedgers[0] == 0)
    		$sGeneralLedger = '<img src="../images/accounts/iconGeneralJournal.gif" alt="General Ledger" title="General Ledger" border="0" /><br />General Ledger';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[0] == 0)
    		//$sSourceOfIncome = '<img src="../images/accounts/iconGeneralJournal.gif" alt="General Journal" title="General Journal" border="0" /><br />General Journal';	
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear[0] == 0)
    		$sCloseFinancialYear = '<img src="../images/accounts/iconCloseFinancialYear_disabled.gif" alt="Close Financial Year" title="Close Financial Year" border="0" /><br />Close Financial Year';	

    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[0] == 0)
    		$sQuickEntries = '<img src="../images/accounts/iconQuickEntries_disabled.gif" alt="Quick Entries" title="Quick Entries" border="0" /><br />Quick Entries';	

    	//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budget[0] == 0)
    	//	$sBudget = '<img src="../images/accounts/iconBudget_disabled.gif" alt="Budget" title="Budget" border="0" /><br />Budget';	

    	//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budget_BudgetAllocation[0] == 0)
    	//	$sBudgetAllocation = '<img src="../images/accounts/iconBudgetAllocation_disabled.gif" alt="Budget Allocation" title="Budget Allocation" border="0" /><br />Budget Allocation';
			
    	$sReturn .= '
    	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
    	 <tr>
    	  <td valign="bottom">
           <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
	    	<tr>
    	     <td align="center" width="10%">' . $sGeneralJournal . '</td>
    	     <!--<td align="center" width="10%">' . $sGeneralLedger . '</td>-->
	    	 <td align="center" width="10%">' . $sChartOfAccounts . '</td>
			 <td align="center" width="10%">' . $sQuickEntries . '</td>
			 <td align="center" width="10%">' . $sDonors . '</td>
			 <td align="center" width="10%">' . $sCloseFinancialYear . '</td>
			 <td align="center" width="10%">' . $sBudget . '</td>
			 <td align="center" width="10%">' . $sBudgetAllocation . '</td>
    	 	</tr>
	       </table>
    	  </td></tr></table>';
    	
    	return($sReturn);
    }
    
	function ShowAccountsPages($sPage)
	{
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		if ($sPage == "") $sPage = "GeneralJournal";
		
		switch($sPage)
		{
			case "GeneralJournal": 
				$aTabs[0][0] = 'General Journal';
				$aTabs[0][1] = '../accounts/generaljournal.php';
				break;				
			case "GeneralLedger": 
				$aTabs[0][0] = 'General Ledger';
				$aTabs[0][1] = '../accounts/generalledger.php';
				break;
			case "ChartOfAccounts": 
				$aTabs[0][0] = 'Chart of Accounts';
				$aTabs[0][1] = '../accounts/chartofaccounts.php';
				break;
			case "QuickEntries": 
				$aTabs[0][0] = 'Quick Entries';
				$aTabs[0][1] = '../accounts/quickentries.php';
				break;
			case "Donors":
				$aTabs[0][0] = 'Donors';
				$aTabs[0][1] = '../accounts/donors.php';
				$aTabs[1][0] = 'Donor Projects';
				$aTabs[1][1] = '../accounts/donors.php?pagetype=donorprojects';	
				$aTabs[2][0] = 'Project Activities';
				$aTabs[2][1] = '../accounts/donors.php?pagetype=projectactivities';	
				break;
			case "CloseFinancialYear":
				$aTabs[0][0] = 'Close Financial Year';
				$aTabs[0][1] = '../accounts/closefinancialyear.php';
				break;	
			case "Budget":
				$aTabs[0][0] = 'Budget';
				$aTabs[0][1] = '../accounts/budget.php';
				break;
			case "BudgetAllocation":
				$aTabs[0][0] = 'Budget Allocation';
				$aTabs[0][1] = '../accounts/allocation.php';
				break;	
		}
		
		$sReturn = $objDHTMLSuite->TabBar($aTabs, $this->ShowAccountsMenu($sPage));
		return($sReturn);
	}
}

?>