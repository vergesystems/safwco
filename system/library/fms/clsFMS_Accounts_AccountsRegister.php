<?php

// Class: AccountsRegister
class clsAccounts_AccountsRegister
{
	public $aAccountType;
	// Class Constructor
	function __construct()
	{
		$this->aAccountType = array('Payment', 'Receipt', 'Journal Entry');
	}
	
	function ShowAllAccountsRegisters($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Account Register";
		if ($sSortBy == "") $sSortBy = "AR.AccountRegisterId DESC";

		if ($sSearch != "")
		{
			$sSearch = mysql_escape_string($sSearch);
			$sSearchCondition = " AND ((AR.Reference LIKE '%$sSearch%') OR (AR.AccountRegisterId LIKE '%$sSearch%') OR (AR.TotalDebit LIKE '%$sSearch%') OR (AR.TotalCredit LIKE '%$sSearch%'))";			
			
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_accountregister AS AR INNER JOIN organization_employees AS E ON E.EmployeeId = AR.AddedBy LEFT JOIN fms_customers AS C ON C.CustomerId = AR.CustomerId LEFT JOIN fms_vendors AS V ON V.VendorId = AR.VendorId", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_accountregister AS AR
		INNER JOIN organization_employees AS E ON E.EmployeeId = AR.AddedBy
		LEFT JOIN fms_customers AS C ON C.CustomerId = AR.CustomerId
		LEFT JOIN fms_vendors AS V ON V.VendorId = AR.VendorId
		WHERE 1=1 $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=AR.AccountRegisterId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Register Id in Ascending Order" title="Sort by Account Register Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=AR.AccountRegisterId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Register Id in Descending Order" title="Sort by Account Register Id in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Account Type&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=AR.AccountType&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Type in Ascending Order" title="Sort by Account Type in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=AR.AccountType&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Type in Descending Order" title="Sort by Account Type in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Reference&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=AR.Reference&sortorder="><img src="../images/sort_up.gif" alt="Sort by Reference in Ascending Order" title="Sort by Reference in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=AR.Reference&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Reference in Descending Order" title="Sort by Reference in Descending Order" border="0" /></a></span></td>
		  <!--<td align="left"><span class="WhiteHeading">Chart Of Account&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsId&sortorder="><img src="../images/sort_up.gif" alt="Sort by ChartOfAccountId in Ascending Order" title="Sort by ChartOfAccountId in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=AR.ChartOfAccountsId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by ChartOfAccountId in Descending Order" title="Sort by Chart Of AccountId in Descending Order" border="0" /></a></span></td>-->
		  <td align="left"><span class="WhiteHeading">Debit&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=AR.TotalDebit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Debit Amount in Ascending Order" title="Sort by Debit Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=AR.TotalDebit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Debit Amount in Descending Order" title="Sort by Debit Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Credit&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=AR.TotalCredit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Credit Amount in Ascending Order" title="Sort by Credit Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=AR.TotalCredit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Credit Amount in Descending Order" title="Sort by Credit Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Added on&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=AR.AddedOn&sortorder="><img src="../images/sort_up.gif" alt="Sort by Added on in Ascending Order" title="Sort by Added on in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=AR.AddedOn&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Added on in Descending Order" title="Sort by Added on in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iAccountRegisterId = $objDatabase->Result($varResult, $i, "AR.AccountRegisterId");			
			$iAccountType = $objDatabase->Result($varResult, $i, "AR.AccountType");
			$sAccountType = $this->aAccountType[$iAccountType];			
			$sReference = $objDatabase->Result($varResult, $i, "AR.Reference");			
			$iCustomerId = $objDatabase->Result($varResult, $i, "AR.CustomerId");
			$iVendorId = $objDatabase->Result($varResult, $i, "AR.VendorId");
			$iChartOfAccountId = $objDatabase->Result($varResult, $i, "AR.ChartOfAccountsId");			
			$dTotalDebit = $objDatabase->Result($varResult, $i, "AR.TotalDebit");
			$dTotalCredit = $objDatabase->Result($varResult, $i, "AR.TotalCredit");
			$sTotalDebit= number_format($dTotalDebit,2);
			$sTotalCredit= number_format($dTotalCredit,2);
			
			$dAddedOn = $objDatabase->Result($varResult, $i, "AR.AddedOn");
			$sAddedOn = date("F j,Y", strtotime($dAddedOn));
			
			$sEditAccountsRegister = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Accounts Register\', \'../accounts/accountsregister_details.php?action2=edit&id=' . $iAccountRegisterId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Accounts Register Entry Details" title="Edit this Accounts Register Entry Details"></a></td>';
			$sDeleteAccountsRegister = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Accounts Register?\')) {window.location = \'?action=DeleteAccountsRegister&id=' . $iAccountRegisterId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Accounts Register" title="Delete this Accounts Register Entry"></a></td>';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td align="left" valign="top">' . $iAccountRegisterId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sAccountType . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sReference . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sTotalDebit . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sTotalCredit . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sAddedOn . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Accounts Register Details\', \'../accounts/accountsregister_details.php?id=' . $iAccountRegisterId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Accounts Register Details" title="View this Accounts Register Details"></a></td>
			</tr>';
		}

		$sAddAccountsRegister = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add New Entry\', \'../accounts/accountsregister_details.php?action2=addnew\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add New">';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddAccountsRegister . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for an Accounts Register Entry:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for an Accounts Register Entry" title="Search for an Accounts Register Entry" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Accounts Register Entry Details" alt="View this Accounts Register Entry Details"></td><td>View this Accounts Register Entry Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Accounts Register Entry Details" alt="Edit this Accounts Register Entry Details"></td><td>Edit this Accounts Register Entry Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Accounts Register Entry" alt="Delete this Accounts Register Entry"></td><td>Delete this Accounts Register Entry</td></tr>
		</table>';

		return($sReturn);
	}
	
	function AccountsRegisterDetails($iAccountRegisterId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_accounts_accountregister AS AR
		INNER JOIN organization_employees AS E ON E.EmployeeId = AR.AddedBy
		LEFT JOIN fms_customers AS C ON C.CustomerId = AR.CustomerId
		LEFT JOIN fms_vendors AS V ON V.VendorId = AR.VendorId
		WHERE AR.AccountRegisterId = '$iAccountRegisterId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iAccountRegisterId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Accounts Register Entry" title="Edit this Accounts Register Entry" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Accounts Register Entry?\')) {window.location = \'?action=DeleteAccountsRegister&id=' . $iAccountRegisterId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Accounts Register Entry" title="Delete this Accounts Register Entry" /></a>';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Accounts Register Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iAccountRegisterId = $objDatabase->Result($varResult, 0, "AR.AccountRegisterId");
				
				$iAccountType = $objDatabase->Result($varResult, 0, "AR.AccountType");
				$sAccountType = $this->aAccountType[$iAccountType];
				
				$sReference = $objDatabase->Result($varResult, 0, "AR.Reference");
				$iCustomerId = $objDatabase->Result($varResult, 0, "AR.CustomerId");
				if($iCustomerId > 0)
				{
					$sCustomerFirstName = $objDatabase->Result($varResult, 0, "C.FirstName");
					$sCustomerFirstName = mysql_escape_string($sCustomerFirstName);
					$sCustomerFirstName = stripslashes($sCustomerFirstName);
					$sCustomerLastName = $objDatabase->Result($varResult, 0, "C.LastName");
					$sCustomerLastName = mysql_escape_string($sCustomerLastName);
					$sCustomerLastName = stripslashes($sCustomerLastName);
					$sCustomer = $sCustomerFirstName . ' ' . $sCustomerLastName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Customer Information\', \'../customers/customers_details.php?id=' . $iCustomerId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Customer Information" title="Customer Information" border="0" /></a>';				
				}
				$iVendorId = $objDatabase->Result($varResult, 0, "AR.VendorId");
				if($iVendorId > 0)
				{
					$sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");
					$sVendorName = mysql_escape_string($sVendorName);
					$sVendorName = stripslashes($sVendorName);
					$sVendor = $sVendorName  . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Vendor Information\', \'../vendors/vendors_details.php?id=' . $iVendorId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Vendor Information" title="Vendor Information" border="0" /></a>';;
				}
				
				$sAccountType .= '&nbsp;&raquo; ' . $sVendor . $sCustomer;
				
				$iChartOfAccountId = $objDatabase->Result($varResult, 0, "AR.ChartOfAccountsId");
				
				
				$dTotalDebit = $objDatabase->Result($varResult, 0, "AR.TotalDebit");
				$dTotalCredit = $objDatabase->Result($varResult, 0, "AR.TotalCredit");
				$sDescription = $objDatabase->Result($varResult, 0, "AR.Description");			
				
				$sNotes = $objDatabase->Result($varResult, 0, "AR.Notes");
				$sNotes = mysql_escape_string($sNotes);
				$sNotes = stripslashes($sNotes);
				
				$dAddedOn = $objDatabase->Result($varResult, 0, "AR.AddedOn");
				$sAddedOn = date("F j, Y", strtotime($dAddedOn)) . ' at ' . date("g:i a", strtotime($dAddedOn));
				
				$iAddedBy = $objDatabase->Result($varResult, 0, "AR.AddedBy");
				$sAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(str_replace('"', '', $sAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$varResult2 = $objDatabase->Query("SELECT * 
				FROM fms_accounts_accountregister_entries AS ARE
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= ARE.ChartOfAccountsId
				WHERE ARE.AccountRegisterId= '$iAccountRegisterId'");
						       	
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';
					
					$sDetail = $objDatabase->Result($varResult2, $i, "ARE.Detail");
					$sChartOfAccount = $objDatabase->Result($varResult2, $i, "CA.ChartOfAccountsCode");					
					$dDebit = $objDatabase->Result($varResult2, $i, "ARE.Debit");
					$dCredit = $objDatabase->Result($varResult2, $i, "ARE.Credit");					
					
					$sAccountRegisterParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center">' . ($i+1) . '</td>
					 <td>' . $sChartOfAccount . '</td> 
 					 <td>' . $sDetail . '</td>					 
					 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dDebit, 2) . '</td>
					 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dCredit, 2) . '</td>					 
					</tr>';
				}
				
				$sAccountRegisterEntries = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td width="14%"><span class="WhiteHeading">Chart of Accounts</span></td>				  				  
				  <td width="14%" align="center"><span class="WhiteHeading">Detail</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Debit</span></td>
				  <td width="12%" align="center"><span class="WhiteHeading">Credit</span></td>				  
				 </tr>
				' . $sAccountRegisterParticulars_Rows . '				
				<tr bgcolor="#ffffff">
				 <td align="right" colspan="3" style="font-size:13px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				 <td align="right" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:14px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalDebit, 0) . '</td>
				 <td align="right" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:14px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalCredit, 0) . '</td>
				</tr>				
				</table>';
				
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				if($iAccountType == 0) 
				{
					$sVendorStyle = "style=display:inline;";
					$sCustomerStyle = "style=display:none;";
				}
				elseif($iAccountType == 1)
				{
					$sVendorStyle = "style=display:none;";
					$sCustomerStyle = "style=display:inline;";
				}
				
				
				$sVendor = '<div id="divPaymentTo" name="divPaymentTo"' . $sVendorStyle . '><select name="selVendor" id="selVendor" class="form1">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_vendors AS V ORDER BY V.VendorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{
					$sVendor .= '<option ' . (($iVendorId == $objDatabase->Result($varResult, $i, "V.VendorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult, $i, "V.VendorName") . '</option>';
				}
				$sVendor .='</select></div>';
				
				$sCustomer = '<div id="divPaymentFrom" name="divPaymentFrom"' . $sCustomerStyle . '><select name="selCustomer" id="selCustomer" class="form1">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C ORDER BY C.FirstName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{
					$sCustomer .= '<option ' . (($iCustomerId == $objDatabase->Result($varResult, $i, "C.CustomerId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "C.CustomerId") . '">' . $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") . '</option>';
				}
				$sCustomer .='</select></div>';
				
				$sAccountType = '<select name="selAccountType" id="selAccountType" class="form1" onchange="ChangeTo();">';
				for($i = 0; $i < count($this->aAccountType); $i++)
					if($i != 2)					
						$sAccountType .= '<option ' . (($iAccountType == $i) ? 'selected="true"' : '') . 'value="' . $i . '">' . $this->aAccountType[$i] . '</option>';				
				$sAccountType .='</select>&nbsp;&raquo; ' . $sVendor . $sCustomer . '
				<script language="JavaScript" type="text/javascript">
				function ChangeTo()
				{				
	        		if (GetSelectedListBox("selAccountType") == 0)
	        		{
	        			document.getElementById("divPaymentTo").style.display = "inline";
	        			document.getElementById("divPaymentFrom").style.display = "none";
	        		}
	        		else
	        		{
	        			document.getElementById("divPaymentTo").style.display = "none";
	        			document.getElementById("divPaymentFrom").style.display = "inline";
	        		}
				}
				</script>';				
				
				
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount .'" />&nbsp;<span style="color:red;">*</span>';
				$sSalesTax = '<input type="text" name="txtSalesTax" id="txtSalesTax" class="form1" value="' . $dSalesTax .'" />';
				//$sReceiptAmount = '<input type="text" name="txtReceiptAmount" id="txtReceiptAmount" class="form1" value="' . $dReceiptAmount .'" />';
				//$sPaymentAmount = '<input type="text" name="txtPaymentAmount" id="txtPaymentAmount" class="form1" value="' . $dPaymentAmount .'" />';
				//$sBalance = '<input type="text" name="txtBalance" id="txtBalance" class="form1" value="' . $dBalance .'" />';
				
				$sReference = '<input type="text" name="txtReference" id="txtReference" class="form1" value="' . $sReference .'" maxlength="15" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$iAccountRegisterId = "";
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$sReference = $objGeneral->fnGet("txtReference");
				$dAmount = $objGeneral->fnGet("txtAmount");
				$dPaymentAmount = $objGeneral->fnGet("txtPaymentAmount");
				$dReceiptAmount = $objGeneral->fnGet("txtReceiptAmount");
				$dSalesTax = $objGeneral->fnGet("txtSalesTax");
				$dBalance = $objGeneral->fnGet("txtBalance");				
				$iAccountType = 2;
				
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sVendor = '<div id="divPaymentTo" name="divPaymentTo" style="display:none;"><select name="selVendor" id="selVendor" class="form1">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_vendors AS V ORDER BY V.VendorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{
					$sVendor .= '<option ' . (($iVendorId == $objDatabase->Result($varResult, $i, "V.VendorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult, $i, "V.VendorName") . '</option>';
				}
				$sVendor .='</select></div>';
				
				$sCustomer = '<div id="divPaymentFrom" name="divPaymentFrom" style="display:none;"><select name="selCustomer" id="selCustomer" class="form1">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C ORDER BY C.FirstName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{
					$sCustomer .= '<option ' . (($iCustomerId == $objDatabase->Result($varResult, $i, "C.CustomerId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "C.CustomerId") . '">' . $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") . '</option>';
				}
				$sCustomer .='</select></div>';
				
				$sAccountType = '<select name="selAccountType" id="selAccountType" class="form1" onchange="ChangeTo();">';
				for($i = 0; $i < count($this->aAccountType); $i++)					
					$sAccountType .= '<option ' . (($iAccountType == $i) ? 'selected="true"' : '') . 'value="' . $i . '">' . $this->aAccountType[$i] . '</option>';
				
				$sAccountType .='</select>&nbsp;&raquo; ' . $sVendor . $sCustomer . '
				<script language="JavaScript" type="text/javascript">
				function ChangeTo()
				{				
	        		if (GetSelectedListBox("selAccountType") == 0)
	        		{
	        			document.getElementById("divPaymentTo").style.display = "inline";
	        			document.getElementById("divPaymentFrom").style.display = "none";
	        		}
	        		else if (GetSelectedListBox("selAccountType") == 1)
	        		{
	        			document.getElementById("divPaymentTo").style.display = "none";
	        			document.getElementById("divPaymentFrom").style.display = "inline";
	        		}
					else
					{
						document.getElementById("divPaymentTo").style.display = "none";
	        			document.getElementById("divPaymentFrom").style.display = "none";
					}
				}
				</script>';
				
				$sReference = '<input type="text" name="txtReference" id="txtReference" class="form1" value="' . $sReference .'" maxlength="15" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount .'" />&nbsp;<span style="color:red;">*</span>';
				$sSalesTax = '<input type="text" name="txtSalesTax" id="txtSalesTax" class="form1" value="' . $dSalesTax .'" />';
				//$sReceiptAmount = '<input type="text" name="txtReceiptAmount" id="txtReceiptAmount" class="form1" value="' . $dReceiptAmount .'" />';
				//$sPaymentAmount = '<input type="text" name="txtPaymentAmount" id="txtPaymentAmount" class="form1" value="' . $dPaymentAmount .'" />';
				//$sBalance = '<input type="text" name="txtBalance" id="txtBalance" class="form1" value="' . $dBalance .'" />';
							
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				$sAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
                $sAddedBy = $sAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(str_replace('"', '', $sAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
								
				for ($k=0; $k < 50; $k++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = (($k <= 9) ? 'table-row' : 'none');
					
					$sChartOfAccount = '<select class="form1" name="selChartOfAccount_' . ($k+1) . '" id="selChartOfAccount_' . ($k+1) . '">';
	    			$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA Order by CA.AccountTitle");
					if ($objDatabase->RowsNumber($varResult2) <= 0) die('Sorry, Invalid Chart of Accounts Id');
					for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
						$sChartOfAccount .= '<option ' . (($iChartOfAccountsId == $objDatabase->Result($varResult2, $i, "CA.ChartOfAccountsId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult2, $i, "CA.ChartOfAccountsId") . '">' . $objDatabase->Result($varResult2, $i, "CA.ChartOfAccountsCode") . '</option>';
					$sChartOfAccount .= '</select>';
					
					
					$sAccountRegisterParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sChartOfAccount . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtAccountRegisterParticulars_Detail_' . ($k+1) . '" id="txtAccountRegisterParticulars_Detail_' . ($k+1) . '" size="80%" /></td>
					 <td align="left"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtAccountRegisterParticulars_Debit_' . ($k+1) . '" id="txtAccountRegisterParticulars_Debit_' . ($k+1) . '" size="10" /></td>
					 <td align="left"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtAccountRegisterParticulars_Credit_' . ($k+1) . '" id="txtAccountRegisterParticulars_Credit_' . ($k+1) . '" size="10" /></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
					</tr>';
				}				
				
				$sAccountRegisterEntries = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td width="14%"><span class="WhiteHeading">Chart of Accounts</span></td>				  				  
				  <td width="14%" align="center"><span class="WhiteHeading">Detail</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Debit</span></td>
				  <td width="12%" align="center"><span class="WhiteHeading">Credit</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				 ' . $sAccountRegisterParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="3" style="font-size:13px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
				  <td align="center" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divDebitTotalAmount" name="divDebitTotalAmount"></div></td>
				  <td align="center" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divCreditTotalAmount" name="divCreditTotalAmount"></div></td>
				 </tr>
				</table>
				<input type="hidden" name="TotalDebit" id="TotalDebit" value="" /><input type="hidden" name="TotalCredit" id="TotalCredit" value="" />';				
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			var iRowCounter = ' . $k . ';
			var iRowVisible = 11;
			
			function ValidateForm()
			{
				if (GetVal(\'txtReference\') == "") return(AlertFocus(\'Please enter a valid Reference \', \'txtReference\'));
				if (GetVal(\'TotalDebit\') != GetVal(\'TotalCredit\')) return(AlertFocus(\'Debit and Creidt amount must be equal\', \'txtAccountRegisterParticulars_Debit_1\'));			
				
				
				return true;
			}
			
			function UpdateTotal()
			{
				var dTotalDebit = 0;
				var dTotalCredit = 0;
				for (i=0; i < iRowCounter; i++)
				{					
					dDebit = Number(GetVal("txtAccountRegisterParticulars_Debit_" + i));
					dCredit = Number(GetVal("txtAccountRegisterParticulars_Credit_" + i));
					if((dDebit > 0 ) && (dCredit > 0))
					{
						dTotalDebit = 0;
						dTotalCredit = 0;
						alert("Please, there will be either Debit or Credit in a row but not both");
						break;
					}
					dTotalDebit += dDebit;
					dTotalCredit += dCredit;
				}		
				
				SetDivVal("divDebitTotalAmount", addCommas(dTotalDebit));
				SetDivVal("divCreditTotalAmount", addCommas(dTotalCredit));
				SetVal("TotalDebit", dTotalDebit);
				SetVal("TotalCredit", dTotalCredit);				
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Accounts Register Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" width="25%">Account Register Id:</td><td><strong>' . $iAccountRegisterId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Account Type:</td><td><strong>' . $sAccountType . '</tr></td>
			 <!--
			 <tr bgcolor="#edeff1"><td valign="top">Chart Of Account Id:</td><td><strong>' . $iChartOfAccountId . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Amount:</td><td><strong>' . $sAmount . '</strong></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top">Sales Tax:</td><td><strong>' . $sSalesTax . '</strong></td></tr>			 
			 -->
			 <tr bgcolor="#edeff1"><td valign="top">Reference:</td><td><strong>' . $sReference . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Account Register Entries:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">' . $sAccountRegisterEntries . '</td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">Description:</td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="4"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Added On:</td><td><strong>' . $sAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Added By:</td><td><strong>' . $sAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iAccountRegisterId . '"><input type="hidden" name="action" id="action" value="UpdateAccountsRegister"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add New" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewAccountsRegister"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewAccountsRegister($iAccountType, $iCustomerId, $iVendorId, $sReference, $sDescription, $dTotalDebit, $dTotalCredit, $sNotes, $iAuto=0)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;

		$varNow = $objGeneral->fnNow();
		$iAddedBy = $objEmployee->iEmployeeId;
	
		$sReference = mysql_escape_string($sReference);
		$sDescription = mysql_escape_string($sDescription);
		$sNotes = mysql_escape_string($sNotes);
		
		if($dTotalDebit != $dTotalCredit) return(3306);
		if($iAccountType == 0) 
		{
			$iCustomerId = 0;			
		}
		
		elseif($iAccountType == 1) 
		{
			$iVendorId = 0;
			$dPaymentAmount = 0;
			$dReceiptAmount = $dAmount;
		}
		else
		{
			$iCustomerId = 0;
			$iVendorId = 0;
		}

		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_accountregister
		(AccountType, CustomerId, VendorId, Reference, Description, TotalDebit, TotalCredit, Notes, Auto, AddedOn, AddedBy)
		VALUES ('$iAccountType', '$iCustomerId', '$iVendorId', '$sReference', '$sDescription', '$dTotalDebit', '$dTotalCredit', '$sNotes', '$iAuto', '$varNow', '$iAddedBy')");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_accountregister AS AR WHERE AR.Reference='$sReference' AND AR.AddedOn='$varNow'");			
			
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iAccountsRegisterId = $objDatabase->Result($varResult, 0, "AR.AccountRegisterId");
				
				for ($k=0; $k < 50; $k++)
				{
					$iChartOfAccountsId = $objGeneral->fnGet("selChartOfAccount_" . ($k +1));
					$sDetail = $objGeneral->fnGet("txtAccountRegisterParticulars_Detail_" . ($k +1));					
					$dDebit = $objGeneral->fnGet("txtAccountRegisterParticulars_Debit_" . ($k +1));
					$dCredit = $objGeneral->fnGet("txtAccountRegisterParticulars_Credit_" . ($k +1));
										
					if(($dDebit != "") || ($dDebit > 0) || ($dCredit !="") || ($dCredit > 0))
						$this->AddAccountRegisterEntries($iAccountsRegisterId, $iChartOfAccountsId, $sDetail, $dDebit, $dCredit);		
					
				}				
				
				$objSystemLog->AddLog("Add New Accounts Register Entry - " . $sReference);

				$objGeneral->fnRedirect('?error=3300&id=' . $objDatabase->Result($varResult, 0, "AR.AccountRegisterId"));
			}
		}

		return(3301);
	}
	
	function UpdateAccountsRegister ($iAccountRegisterId, $iAccountType, $iCustomerId, $iVendorId, $sReference, $sDescription, $dTotalDebit, $dTotalCredit, $sNotes, $iAuto=0)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		$sReference = mysql_escape_string($sReference);
		$sDescription = mysql_escape_string($sDescription);
		$sNotes = mysql_escape_string($sNotes);
		
		if($iAccountType == 0) 
		{
			$iCustomerId = 0;
			$dReceiptAmount = 0;
			$dPaymentAmount = $dAmount;
		}
		
		elseif($iAccountType == 1) 
		{
			$iVendorId = 0;
			$dPaymentAmount = 0;
			$dReceiptAmount = $dAmount;
		}

		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_accountregister 
		SET	
			Reference='$sReference',
			AccountType='$iAccountType',
			CustomerId='$iCustomerId',
			VendorId='$iVendorId',			
			Description='$sDescription',
			Notes='$sNotes'
		WHERE AccountRegisterId='$iAccountRegisterId'");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Update Accounts Register Entry - " . $sReference);
			$objGeneral->fnRedirect('../accounts/accountsregister_details.php?error=3302&id=' . $iAccountRegisterId);
			//return(3302);
		}
		else
			return(3303);
	}
	
	function DeleteAccountsRegister($iAccountRegisterId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;

		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_accountregister AS AR WHERE AR.AccountRegisterId='$iAccountRegisterId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(3305);
		
		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_accountregister WHERE AccountRegisterId='$iAccountRegisterId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Delete Accounts Register - " . $iAccountRegisterId);

			return(3304);
		}
		else
			return(3305);
	}
	
	/* Add Accounts Register Entries */
			
	function AddAccountRegisterEntries($iAccountsRegisterId, $iChartOfAccountsId, $sDetail, $dDebit, $dCredit)
	{
		global $objDatabase;
		global $objGeneral;
		
		$sDetail = mysql_escape_string($sDetail);
				
		if($dDebit == "" ) $dDebit = 0;
		if($dCredit == "" ) $dCredit = 0;
		
		if($dDebit > 0 && $dCredit > 0)
			return(false);
		
		$varResult = $objDatabase->Query("
		SELECT * 
		FROM fms_accounts_accountregister_entries AS ARE 
		WHERE ARE.ChartOfAccountsId='$iChartOfAccountsId' 
		ORDER BY AccountRegisterEntryId DESC LIMIT 1
		");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{	
			$dClosingBalance = $objDatabase->Result($varResult, 0, "ARE.ClosingBalance");
			$dOpeningBalance = $dClosingBalance;
			
			if($dDebit > 0) $dClosingBalance = $dClosingBalance - $dDebit;
			else	$dClosingBalance = $dClosingBalance + $dCredit;
			
		}
		else
		{
			$dOpeningBalance = $dClosingBalance = 0;
			
			if($dDebit > 0) $dClosingBalance = $dClosingBalance - $dDebit;
			else	$dClosingBalance = $dClosingBalance + $dCredit;
		}
		
		$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_accountregister_entries
		(AccountRegisterId, ChartOfAccountsId, Detail, Debit, Credit, OpeningBalance, ClosingBalance)
		VALUES ('$iAccountsRegisterId', '$iChartOfAccountsId', '$sDetail', '$dDebit', '$dCredit', '$dOpeningBalance', '$dClosingBalance')");
		
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
		
		
	}
	
}

?>