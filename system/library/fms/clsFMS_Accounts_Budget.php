<?php

// Class: clsFMS_Budget_Budgets
class clsFMS_Accounts_Budget
{
	public $aStatus;
	public $aBudgetAmountType;
	// Class Constructor
	function __construct()
	{
		$this->aStatus = array("On Going", "Completed");
		$this->aBudgetAmountType = array("Actual", "Increase", "Decrease");
	}


	function ShowAllBudgets($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");

		$sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        if ($sSortBy == "") $sSortBy = "B.BudgetId DESC";

		$iPagingLimit = cPagingLimit;

		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Budgets";
		if ($sSortBy == "") $sSortBy = "B.BudgetId";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((B.Title LIKE '%$sSearch%') OR (B.BudgetCode LIKE '%$sSearch%') OR (B.Description LIKE '%$sSearch%') OR (B.Notes LIKE '%$sSearch%') OR (E.FirstName) OR (E.LastName)) ";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_budget AS B INNER JOIN organization_employees AS E ON E.EmployeeId = B.BudgetAddedBy", "B.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_budget AS B
		INNER JOIN organization_employees AS E ON E.EmployeeId = B.BudgetAddedBy
		WHERE B.OrganizationId='" . cOrganizationId . "' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Budget Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BudgetCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Budget Code in Ascending Order" title="Sort by Budget Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BudgetCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Budget Code in Descending Order" title="Sort by Budget Code in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.Amount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Amount in Ascending Order" title="Sort by Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.Amount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Amount in Descending Order" title="Sort by Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">From&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BudgetStartDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Budget Start Date in Ascending Order" title="Sort by Budget Start Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BudgetStartDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Budget Start Date in Descending Order" title="Sort by Budget Start Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">To&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BudgetEndDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Budget End Date in Ascending Order" title="Sort by Budget End Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BudgetEndDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Budget End Date in Descending Order" title="Sort by Budget End Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="4%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iBudgetId = $objDatabase->Result($varResult, $i, "B.BudgetId");
			$sTitle = $objDatabase->Result($varResult, $i, "B.Title");
			$sBudgetCode = $objDatabase->Result($varResult, $i, "B.BudgetCode");

			$dAmount = $objDatabase->Result($varResult, $i, "B.Amount");
			$sAmount = number_format($dAmount, 2);

			$dBudgetStartDate = $objDatabase->Result($varResult, $i, "B.BudgetStartDate");
			$sBudgetStartDate = date("F j, Y", strtotime($dBudgetStartDate));

			$dBudgetEndDate = $objDatabase->Result($varResult, $i, "B.BudgetEndDate");
			$sBudgetEndDate = date("F j, Y", strtotime($dBudgetEndDate));

			$iStatus = $objDatabase->Result($varResult, $i, "B.Status");
			$sStatus = $this->aStatus[$iStatus];

			$iBudgetAddedBy = $objDatabase->Result($varResult, $i, "B.BudgetAddedBy");
			$sBudgetAddedBy = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");

			$dBudgetAddedOn = $objDatabase->Result($varResult, $i, "B.BudgetAddedOn");
			$sBudgetAddedOn = date("F j, Y", $dBudgetAddedOn);

			$sEditBudget = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sTitle)) . '\', \'../accounts/budget.php?pagetype=details&action2=edit&id=' . $iBudgetId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Details" title="Edit this Budget Details"></a></td>';
			$sDeleteBudget = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Budget?\')) {window.location = \'?action=DeleteBudget&id=' . $iBudgetId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget" title="Delete this Budget"></a></td>';
			$sChangeBudgetAmount = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../accounts/budget.php?pagetype=changebudgetamount&id=' . $iBudgetId . '\', \'Change Amount of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sTitle)) . '\', \'700 420\');"><img src="../images/icons/tick.gif" border="0" alt="Change Budget Amount" title="Change Budget Amount"></a></td>';

			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[2] == 0)	$sEditBudget = '<td class="GridTd">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[3] == 0) $sDeleteBudget = '<td class="GridTd">&nbsp;</td>';				
			if ($objEmployee->objEmployeeRoles->iEmployeeRole_FMS_Accounts_Budgets_ChangeBudgetAmount[0] == 0)	$sChangeBudgetAmount = '<td class="GridTd">&nbsp;</td>';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sBudgetCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td> 
			 <td class="GridTD" align="left" valign="top">' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sBudgetStartDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sBudgetEndDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sTitle) . '\', \'../accounts/budget.php?pagetype=details&id=' . $iBudgetId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Budget Details" title="View this Budget Details"></a></td>
			'. $sEditBudget. $sChangeBudgetAmount .  $sDeleteBudget . '</tr>';
			//' . $sEditBudget . $sDeleteBudget . '</tr>';
		}

		$sAddBudget = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Budget\', \'../accounts/budget.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Budget">';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddBudget . '
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Budget:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Budget" title="Search for a Budget" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Budget Details" alt="View this Budget Details"></td><td>View this Budget Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Budget Details" alt="Edit this Budget Details"></td><td>Edit this Budget Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/tick.gif" title="Change Budget Amount" alt="Change Budget Amount"></td><td>Change Budget Amount</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Budget" alt="Delete this Budget"></td><td>Delete this Budget</td></tr>
		</table>';

		return($sReturn);
	}

	function BudgetDetails($iBudgetId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
				
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();

		$varResult = $objDatabase->Query("
		SELECT *,B.Notes AS Notes, B.Description As Description
		FROM fms_accounts_budget AS B		
		INNER JOIN organization_employees AS E ON E.EmployeeId = B.BudgetAddedBy
		WHERE B.OrganizationId='" . cOrganizationId . "' AND B.BudgetId = '$iBudgetId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iBudgetId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget" title="Edit this Budget" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Budget?\')) {window.location = \'?pagetype=details&action=DeleteBudget&id=' . $iBudgetId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget" title="Delete this Budget" /></a>';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[3] == 0)	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Budget Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iBudgetId = $objDatabase->Result($varResult, 0, "B.BudgetId");

				$iBudgetAddedBy = $objDatabase->Result($varResult, 0, "B.BudgetAddedBy");
				$sBudgetAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sBudgetAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sBudgetAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iBudgetAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

				$sTitle = $objDatabase->Result($varResult, 0, "B.Title");
				$sBudgetCode = $objDatabase->Result($varResult, 0, "B.BudgetCode");
				/*
				$iDonorProjectId = $objDatabase->Result($varResult, 0, "B.DonorProjectId");
				if($iDonorProjectId > 0)
				{
					$sProjectCode = $objDatabase->Result($varResult, 0, "DP.ProjectCode");
					$sProjectTitle = $objDatabase->Result($varResult, 0, "DP.ProjectTitle");
					$sDonorProject = $sProjectCode . ' - ' . $sProjectTitle;
					$sDonorProject = $sDonorProject . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' .  $objDatabase->RealEscapeString(str_replace('"', '', $sProjectTitle)) .'\' , \'../accounts/donorprojects.php?pagetype=details&id=' . $iDonorProjectId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Project Details" title="Project Details" /></a>';
				}
				*/

				$dAmount = $objDatabase->Result($varResult, 0, "B.Amount");
				$sAmount = number_format($dAmount, 0);

				// TO DO
				$dRemainingAmount = 0;
				$dUsedAmount = 0;
				$varResult2 = $objDatabase->Query("
                SELECT
				SUM(GJE.Debit) AS 'UsedAmount'
    			FROM fms_accounts_generaljournal_entries AS GJE
    			INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
                INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
				INNER JOIN fms_accounts_budgetallocation AS BA ON BA.ChartOfAccountId = CA.ChartOfAccountsId
    			WHERE GJE.BudgetId='$iBudgetId' AND CA.ChartOfAccountsCategoryId = '4' AND GJ.IsDeleted ='0'");
				if($objDatabase->RowsNumber($varResult2) > 0)
					$dUsedAmount = $objDatabase->Result($varResult2, 0, "UsedAmount");

				$dRemainingAmount = $dAmount - $dUsedAmount;
				$sRemainingAmount = number_format($dRemainingAmount, 0);

				$dAllocatedAmount = 0;
				$varResult2 = $objDatabase->Query("SELECT SUM(BA.Amount) AS 'AllocatedAmount' FROM fms_accounts_budgetallocation AS BA WHERE BA.BudgetId = '$iBudgetId'");
				if($objDatabase->RowsNumber($varResult2) > 0)
					$dAllocatedAmount = $objDatabase->Result($varResult2, 0, "AllocatedAmount");

				$dRemainingAllocatedAmount = $dAmount - $dAllocatedAmount;
				$sRemainingAllocatedAmount = number_format($dRemainingAllocatedAmount, 0);

                $dBudgetReceived = 0;
				$varResult2 = $objDatabase->Query("
				SELECT
					SUM(GJE.Credit) AS 'BudgetReceived'
				FROM fms_accounts_generaljournal_entries AS GJE
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
                INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
    			WHERE GJE.BudgetId='$iBudgetId' AND CA.ChartOfAccountsCategoryId = '3' AND GJ.IsDeleted ='0'");
				if($objDatabase->RowsNumber($varResult2) > 0)
					$dBudgetReceived = $objDatabase->Result($varResult2, 0, "BudgetReceived");

				$dBudgetStartDate = $objDatabase->Result($varResult, 0, "B.BudgetStartDate");
				$sBudgetStartDate = date("F j, Y", strtotime($dBudgetStartDate));

				$dBudgetEndDate = $objDatabase->Result($varResult, 0, "B.BudgetEndDate");
				$sBudgetEndDate = date("F j, Y", strtotime($dBudgetEndDate));

				$iStatus = $objDatabase->Result($varResult, 0, "B.Status");
				$sStatus = $this->aStatus[$iStatus];

				$sBudgetInfo = '<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
				 <tr><td class="Tahoma16">Budget Amount:</td><td align="right" class="Tahoma16" style="color:blue;">' . number_format($dAmount, 0) . '</td></tr>
				 <tr><td class="Tahoma16">Budget Utilized:</td><td align="right" class="Tahoma16" style="color:red;">' . number_format($dUsedAmount, 0) . '</td></tr>
				 <tr><td class="Tahoma16" style="border-top:1px solid;">Remaining Budget:</td><td align="right" class="Tahoma16" style="border-top:1px solid black; color:green;">' . number_format($dRemainingAmount, 0) . '</td></tr>
				 <tr><td colspan="2" style="border-top:4px double;"><br /></td></tr>
				 <tr><td class="Tahoma16">Budget Allocated:</td><td align="right" class="Tahoma16" style="colorkey:green;">' . number_format(($dAllocatedAmount), 0) . '</td></tr>
				 <tr><td class="Tahoma16">Allocation Remaining:</td><td align="right" class="Tahoma16" style="color:blue;">' . number_format($dRemainingAllocatedAmount, 0) . '</td></tr>
                 <tr><td colspan="2" style="border-top:4px double;"><br /></td></tr>
				 <tr><td class="Tahoma16">Budget Received:</td><td align="right" class="Tahoma16" style="color:green;">' . number_format(($dBudgetReceived), 0) . '</td></tr>
				 <tr><td colspan="2" style="border-top:1px solid;"><br /></td></tr>
				</table>';

				$sDescription = $objDatabase->Result($varResult, 0, "Description");
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");

				$dBudgetAddedOn = $objDatabase->Result($varResult, 0, "B.BudgetAddedOn");
				$sBudgetAddedOn = date("F j, Y", strtotime($dBudgetAddedOn)) . ' at ' . date("g:i a", strtotime($dBudgetAddedOn));

				$sBudgetHistory = '<tr bgcolor="#edeff1"><td align="left" colspan="4">
				<table border="0" cellspacing="0" cellpadding="5" width="98%" align="center">
				<tr><td colspan="5"><span style="font-family:Tahoma, Arial; font-size:22px;">Budget Change History</span></td></tr>
				</table>
				<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="98%" align="center">
				<tr class="GridTR">
				 <td width="5%" align="center" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">S#</td>
				 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Date / Time</td>
				 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Title</td>
				 <td width="10%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Amount</td>
				 <td width="10%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Employee Name</td>
				 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Budget Amount Type</td>
				 <td width="30%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Description</td>
				</tr>';
				$varResult = $objDatabase->Query("SELECT *
				FROM fms_accounts_budgethistory AS BH
				INNER JOIN organization_employees AS E ON E.EmployeeId = BH.BudgetHistoryAddedBy
				WHERE BH.BudgetId='$iBudgetId'");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					{
						$sEmployeeName = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
						$sHistoryTitle = $objDatabase->Result($varResult, $i, "BH.Title");
						$dHistoryAmount = $objDatabase->Result($varResult, $i, "BH.Amount");
						$sHistoryAmount = number_format($dHistoryAmount, 2);
						$sHistoryDescription = $objDatabase->Result($varResult, $i, "BH.Description");
						$iChangeBudgetAmountType = $objDatabase->Result($varResult, $i, "BH.ChangeBudgetAmountType");
						$sChangeBudgetAmountType = $this->aBudgetAmountType[$iChangeBudgetAmountType];
						$dBudgetHistoryAddedOn = $objDatabase->Result($varResult, $i, "BH.BudgetHistoryAddedOn");
						$sBudgetHistoryAddedOn = date("F j, Y", strtotime($dBudgetHistoryAddedOn)) . ' at ' . date("g:i a", strtotime($dBudgetHistoryAddedOn));

						$sBudgetHistory .= '<tr>
						 <td align="center" style="font-size:11px; font-family:Tahoma, Arial;">' . ($i+1) . '</td>
						 <td>' . $sBudgetHistoryAddedOn . '</td>
						 <td>' . $sHistoryTitle . '</td>
						 <td>' . $sHistoryAmount . '</td>
						 <td>' . $sEmployeeName . '</td>
						 <td>' . $sChangeBudgetAmountType . '</td>
						 <td>' . $sHistoryDescription . '</td>
						</tr>';

					}
				}

				$sBudgetHistory .= '</table></td></tr>';
			}

			if ($sAction2 == "decrease")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBudgetCode = '<input type="text" name="txtBudgetCode" id="txtBudgetCode" class="form1" value="' . $sBudgetCode . '" size="20" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			if ($sAction2 == "increase")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBudgetCode = '<input type="text" name="txtBudgetCode" id="txtBudgetCode" class="form1" value="' . $sBudgetCode . '" size="20" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

				$sDonorProject = '<select name="selDonorProject" id="selDonorProject" class="form1">
				<option value="0">Select Donor Project</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP ORDER BY DP.ProjectTitle");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sDonorProject .= '<option ' . (($iDonorProjectId == $objDatabase->Result($varResult, $i, "DP.DonorProjectId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . ' - ' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '</option>';
				$sDonorProject .='</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selDonorProject\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDonorProject\'), \'../accounts/donorprojects.php?pagetype=details&id=\'+GetSelectedListBox(\'selDonorProject\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Project Details" title="Project Details" border="0" /></a>';

				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBudgetCode = '<input type="text" name="txtBudgetCode" id="txtBudgetCode" class="form1" value="' . $sBudgetCode . '" size="20" />&nbsp;<span style="color:red;">*</span>'; 
				$sBudgetStartDate = '<input type="text" name="txtBudgetStartDate" id="txtBudgetStartDate" class="form1" value="' . $dBudgetStartDate . '" size="10" />' . $objjQuery->Calendar('txtBudgetStartDate') . '&nbsp;<span style="color:red;">*</span>';
				$sBudgetEndDate = '<input type="text" name="txtBudgetEndDate" id="txtBudgetEndDate" class="form1" value="' . $dBudgetEndDate. '" size="10" />' . $objjQuery->Calendar('txtBudgetEndDate') . '&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

				$sReturn .= '
				<script language="JavaScript" type="text/javascript">
				function ValidateForm()
				{
					if (GetVal(\'txtTitle\') == "") return(AlertFocus(\'Please enter a valid Title\', \'txtTitle\'));
					if (GetVal(\'txtBudgetCode\') == "") return(AlertFocus(\'Please enter a valid Budget Code\', \'txtBudgetCode\'));

					return true;
				}
				</script>';

			}
			else if ($sAction2 == "addnew")
			{
				$iBudgetId = "";
				$sTitle = $objGeneral->fnGet("txtTitle");
				$sBudgetCode = $objGeneral->fnGet("txtBudgetCode");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$sAmount = $objGeneral->fnGet("txtAmount");
				$sBudgetStartDate = date('Y-m-d');
				$sBudgetEndDate = date('Y-m-d');

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

				$sBudgetAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeFirstName)) . ' ' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeLastName)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

				$sDonorProject = '<select name="selDonorProject" id="selDonorProject" class="form1">
				<option value="0">Select Donor Project</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP ORDER BY DP.ProjectTitle");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sDonorProject .= '<option ' . (($iDonorProjectId == $objDatabase->Result($varResult, $i, "DP.DonorProjectId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . ' - ' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '</option>';
				$sDonorProject .='</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selDonorProject\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDonorProject\'), \'../accounts/donorprojects.php?pagetype=details&id=\'+GetSelectedListBox(\'selDonorProject\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Project Details" title="Project Details" border="0" /></a>';

				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';
				for($i = 0; $i < count($this->aStatus); $i++)
					$sStatus .= '<option value = "' . $i . '">' . $this->aStatus[$i] . '</option>';
				$sStatus .= '</select>';

				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBudgetCode = '<input type="text" name="txtBudgetCode" id="txtBudgetCode" class="form1" value="' . $sBudgetCode . '" size="20" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $sAmount . '" size="20" />&nbsp;<span style="color:red;">*</span>';
				$sBudgetStartDate = '<input type="text" name="txtBudgetStartDate" id="txtBudgetStartDate" class="form1" value="' . $sBudgetStartDate . '" size="10" />' . $objjQuery->Calendar('txtBudgetStartDate', true) . '&nbsp;<span style="color:red;">*</span>';
				$sBudgetEndDate = '<input type="text" name="txtBudgetEndDate" id="txtBudgetEndDate" class="form1" value="' . $sBudgetEndDate . '" size="10" />' . $objjQuery->Calendar('txtBudgetEndDate', true) . '&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sBudgetAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));

				$sReturn .= '
				<script language="JavaScript" type="text/javascript">
				function ValidateForm()
				{
					if (GetVal(\'txtTitle\') == "") return(AlertFocus(\'Please enter a valid Title\', \'txtTitle\'));
					if (GetVal(\'txtBudgetCode\') == "") return(AlertFocus(\'Please enter a valid Budget Code\', \'txtBudgetCode\'));
					//if (!isDate(GetVal(\'txtBudgetStartDate\'))) return(AlertFocus(\'Please enter a valid Budget Start Date\', \'txtBudgetStartDate\'));
					//if (!isDate(GetVal(\'txtBudgetEndDate\'))) return(AlertFocus(\'Please enter a valid Budget End Date\', \'txtBudgetEndDate\'));
					if (!isNumeric(GetVal(\'txtAmount\'))) return(AlertFocus(\'Please enter a valid Amount\', \'txtAmount\'));

					return true;
				}
				</script>';
			}

			$sReturn .= '
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="3"><span class="Details_Title">Budget Information:</span></td></tr>			 
			 <!--<tr bgcolor="#edeff1"><td valign="top">Donor Project:</td><td><strong>' . $sDonorProject . '</strong></td></tr>-->
			 <tr bgcolor="#edeff1"><td>Title:</td><td><strong>' . $sTitle . '</strong></td><td style="width:300px;" valign="top" rowspan="12">' . $sBudgetInfo . '</td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Budget Code:</td><td><strong>' . $sBudgetCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Budget Start Date (From):</td><td><strong>' . $sBudgetStartDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Budget End Date (To):</td><td><strong>' . $sBudgetEndDate . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Amount:</td><td><strong>' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">Description:</td></tr>
			 <tr><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Status:</td><td><strong>' . $sStatus . '</strong><br /><br /></td></tr>
			 ' . $sBudgetHistory . '
			 <tr class="Details_Title_TR"><td colspan="3"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td colspan="2"><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Budget Added By:</td><td colspan="2"><strong>' . $sBudgetAddedBy . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Added On:</td><td colspan="2"><strong>' . $sBudgetAddedOn . '</strong></td></tr>
			</table>';

			//if ($sAction2 == "edit")
			//	$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Budget" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBudgetId . '"><input type="hidden" name="action" id="action" value="UpdateBudget"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Budget" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewBudget"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBudgetId . '"><input type="hidden" name="action" id="action" value="UpdateBudget"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';	
			else if ($sAction2 == "decrease")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Decrease Budget" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="decrease"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "increase")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Increase Budget" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="increase"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}
		
		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewBudget($sTitle, $sBudgetCode, $dAmount, $dBudgetStartDate, $dBudgetEndDate, $iStatus, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[1] == 0)
			return(1010);

		$varNow = $objGeneral->fnNow();
		$iEmployeeId = $objEmployee->iEmployeeId;
	
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		$sBudgetCode = $objDatabase->RealEscapeString($sBudgetCode);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);

		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_budget
		(OrganizationId, Title, BudgetCode, Amount, BudgetStartDate, BudgetEndDate, Status, Description, Notes, BudgetAddedBy, BudgetAddedOn)
		VALUES ('" . cOrganizationId . "', '$sTitle', '$sBudgetCode', '$dAmount', '$dBudgetStartDate', '$dBudgetEndDate', '$iStatus', '$sDescription', '$sNotes', '$iEmployeeId', '$varNow')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.OrganizationId='" . cOrganizationId . "' AND B.Title='$sTitle' AND B.BudgetAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iBudgetId = $objDatabase->Result($varResult, 0, "B.BudgetId");
				
				$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgethistory
				(BudgetId, Title, BudgetCode, Amount, Description, Notes, BudgetHistoryAddedBy, BudgetHistoryAddedOn)
				VALUES ('$iBudgetId', '$sTitle', '$sBudgetCode', '$dAmount', '$sDescription', '$sNotes', '$iEmployeeId', '$varNow')");
				
				$objSystemLog->AddLog("Add New Budget - " . $sTitle);
				$objGeneral->fnRedirect('?pagetype=details&error=13000&id=' . $objDatabase->Result($varResult, 0, "B.BudgetId"));
			}
		}

		return(13001);
	}
	
	function UpdateBudget($iBudgetId, $sTitle, $sBudgetCode, $dBudgetStartDate, $dBudgetEndDate, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[2] == 0)
			return(1010);
		
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		$sBudgetCode = $objDatabase->RealEscapeString($sBudgetCode);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);

		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_budget
		SET	
			Title = '$sTitle', 
			BudgetCode='$sBudgetCode',			
			BudgetStartDate='$dBudgetStartDate',
			BudgetEndDate='$dBudgetEndDate',
			Description = '$sDescription',
			Notes = '$sNotes'
		WHERE OrganizationId='" . cOrganizationId . "' AND BudgetId = '$iBudgetId'");
		if ($varResult)
		{			
			$objSystemLog->AddLog("Update Budget- " . $sTitle);
			$objGeneral->fnRedirect('?pagetype=details&error=13002&id=' . $iBudgetId);			
		}

		return(13003);
	}
	
	function IncreaseOrDecreaseBudget($iBudgetId, $sTitle, $sBudgetCode, $dAmount, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[2] == 0)
			return(1010);
		
		if($objDatabase->DBCount("fms_accounts_budget AS B", " B.BudgetId = '$iBudgetId'") <= 0)
			return(13007);
		
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		$sBudgetCode = $objDatabase->RealEscapeString($sBudgetCode);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		$iEmployeeId = $objEmployee->iEmployeeId;
		$varNow = $objGeneral->fnNow();
		
		if($objGeneral->fnGet("action") == 'decrease')
		{
			
			$varResult = $objDatabase->Query("SELECT B.Amount FROM fms_accounts_budget AS B WHERE B.OrganizationId='" . cOrganizationId . "' AND B.BudgetId = '$iBudgetId'");
			$dBudgetAmount = $objDatabase->Result($varResult, 0, "B.Amount");

			// TODO
			$dRemainingAmount = 0;
			// If Remaining Amount is less than Descreasing Amount than System should not allow to decrease
			//if($dRemainingAmount < $dAmount) return(13006);
			if($dRemainingAllocatedAmount < $dAmount) return(13006);
			
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budget
			SET
				Title = '$sTitle', 
				BudgetCode='$sBudgetCode',
				Amount = Amount - '$dAmount',
				Description = '$sDescription',
				Notes = '$sNotes'
			WHERE OrganizationId='" . cOrganizationId . "' AND BudgetId = '$iBudgetId'");
				
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgethistory
			(BudgetId, Title, Amount, BudgetHistoryAddedOn, BudgetHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetId', '$sTitle', '$dAmount', '$varNow', '$iEmployeeId', '$sDescription', '$sNotes')");
		}
		if($objGeneral->fnGet("action") == 'increase')	
		{
			$varResult3 = $objDatabase->Query("
			UPDATE fms_accounts_budget 
			SET	
				Title='$sTitle', 
				Amount= Amount + '$dAmount',
				Description = '$sDescription',
				Notes = '$sNotes'
			WHERE BudgetId='$iBudgetId'");
				
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgethistory
			(BudgetId, Title, BudgetCode, Amount, BudgetHistoryAddedOn, BudgetHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetId', '$sTitle', '$sBudgetCode', '$dAmount', '$varNow', '$iEmployeeId', '$sDescription', '$sNotes')");
		}
		if ($objDatabase->AffectedRows($varResult2) > 0)
		{			
			$objSystemLog->AddLog("Update Budget - " . $sTitle);
			$objGeneral->fnRedirect('../accounts/budget.php?pagetype=details&id='. $iBudgetId . ' &error=13002');
			//return(13002);
		}
		else
			return(13003);
	}
	
	function DeleteBudget($iBudgetId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[3] == 0)
		{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iBudgetId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}
								
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.OrganizationId='" . cOrganizationId . "' AND B.BudgetId = '$iBudgetId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(13005);

		$sTitle = $objDatabase->Result($varResult, 0, "B.Title");
		
		// Check Budgets usage in General Journal & Allocation
		if ($objDatabase->DBCount("fms_accounts_generaljournal AS GJ", "GJ.BudgetId= '$iBudgetId' AND GJ.IsDeleted ='0'") > 0) return(13008);
		if ($objDatabase->DBCount("fms_accounts_budgetallocation AS A", "A.BudgetId= '$iBudgetId'") > 0) return(13009);

		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_budget WHERE BudgetId = '$iBudgetId'");
		if ($varResult)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_accounts_budgethistory WHERE BudgetId = '$iBudgetId'");
						
			$objSystemLog->AddLog("Delete Budget - " . $sTitle);
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iBudgetId . '&error=13004');
			else
				$objGeneral->fnRedirect('?id=0&error=13004');
		}
		else
			return(13005);
	}
	
	function ShowBudgetAmount($iBudgetId)
	{
		global $objDatabase;
		global $objEmployee;		
		
		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr><td colspan="5"><span style="font-family:Tahoma, Arial; font-size:22px;">Budget Change History</span></td></tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr class="GridTR">
		 <td width="5%" align="center" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">S#</td>
		 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Date / Time</td>
		 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Title</td>
		 <td width="10%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Amount</td>
		 <td width="10%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Employee Name</td>
		 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Budget Amount Type</td>
		 <td width="30%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Description</td>
		</tr>';
		
		$bAllowUpdate = true;
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_budgethistory AS BH
		INNER JOIN organization_employees AS E ON E.EmployeeId = BH.BudgetHistoryAddedBy
		WHERE BH.BudgetId='$iBudgetId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			for ($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$sEmployeeName = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
				$sTitle = $objDatabase->Result($varResult, $i, "BH.Title");
				$dAmount = $objDatabase->Result($varResult, $i, "BH.Amount");
				$sAmount = number_format($dAmount, 2);
				$sDescription = $objDatabase->Result($varResult, $i, "BH.Description");
				$iChangeBudgetAmountType = $objDatabase->Result($varResult, $i, "BH.ChangeBudgetAmountType");
				$sChangeBudgetAmountType = $this->aBudgetAmountType[$iChangeBudgetAmountType];
				$dBudgetHistoryAddedOn = $objDatabase->Result($varResult, $i, "BH.BudgetHistoryAddedOn");
				$sBudgetHistoryAddedOn = date("F j, Y", strtotime($dBudgetHistoryAddedOn)) . ' at ' . date("g:i a", strtotime($dBudgetHistoryAddedOn));
												
				$sReturn .= '<tr>
				 <td align="center" style="font-size:11px; font-family:Tahoma, Arial;">' . ($i+1) . '</td>
				 <td>' . $sBudgetHistoryAddedOn . '</td>
				 <td>' . $sTitle . '</td>
				 <td>' . $sAmount . '</td>
				 <td>' . $sEmployeeName . '</td>
				 <td>' . $sChangeBudgetAmountType . '</td>				 
				 <td>' . $sDescription . '</td>
				</tr>';
				
			}
		}

		$sReturn .= '</table>';

		$sChangeBudgetAmountType = '<select name="selChangeBudgetAmountType" id="selChangeBudgetAmountType" style="font-size:14px; font-family:Tahoma, Arial;">';
		for ($i=1; $i < count($this->aBudgetAmountType); $i++)
			$sChangeBudgetAmountType .= '<option value="' . $i . '">' . $this->aBudgetAmountType[$i] . '</option>';
		$sChangeBudgetAmountType .= '</select>';
		
		$sReturn .= '
		<br />
		&nbsp;&nbsp;<img src="../images/icons/plus.gif" align="absmiddle" alt="Update Amount" title="Update Amount" border="0" /><a href="#noanchor" style="font-size:18px; font-family:Tahoma, Arial; color:green;" onclick="ShowHideDiv(\'divUpdateAmount\');">&nbsp;Update Amount</a>&nbsp;&nbsp;
		<div id="divUpdateAmount" style="display:none;" align="center">
		 <br />
		 <table border="1" bordercolor="#cdcdcd" cellspacing="0" cellpadding="5" width="80%" align="center">
		  <tr>
		   <td>
			<form method="post" action="" onsubmit="return window.top.MOOdalBox.open(\'../accounts/budget_changeamount.php?&id=' . $iBudgetId. '&action=changeamount&selChangeBudgetAmountType=\'+GetSelectedListBox(\'selChangeBudgetAmountType\')+\'&txtTitle=\'+GetVal(\'txtTitle\')+\'&txtAmount=\'+GetVal(\'txtAmount\')+\'&txtDescription=\'+GetVal(\'txtDescription\'), \'' . $sBudgetTitle . '\', \'700 420\');">
			<span style="font-size:14px; color:blue;">' . $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '</span>:&nbsp;<span style="font-size:11px; color:green;">[ ' . date("F j, Y g:i a") . ' ]</span>&nbsp;<br />
			<br />
			<div align="center">
			' . $sChangeBudgetAmountType . '
			<br /><br />
			<span class="Tahoma16">Title:</span>
			<input type="text" id="txtTitle" name="txtTitle" /><br /><br />
			<span class="Tahoma16">Amount:</span>
			<input type="text" id="txtAmount" name="txtAmount" /><br /> 
			<br /><br />
			<span class="Tahoma16">Description:</span><br />
			<textarea name="txtDescription" id="txtDescription" style="font-size:14px; font-family:Tahoma, Arial; width:500px; height:50px;"></textarea>
			</div>
			<br /><br />
			<div align="center"><input type="submit" class="AdminFormButton1" value="Submit" /></div>
			</form>
		   </td>
		  </tr>
		 </table>
		</div>';
							
		return($sReturn);		
	}
	
	function ChangeAmount($iBudgetId, $sTitle, $dAmount, $iChangeBudgetAmountType, $sDescription)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[2] == 0)
			return(1010);
		
		$iEmployeeId = $objEmployee->iEmployeeId;		
		$varNow = $objGeneral->fnNow();
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		if($dAmount == "") $dAmount = 0;
		if(!is_numeric($dAmount)) return(13003);		
		if($iChangeBudgetAmountType == 1)		// Increase Budget Amount
		{
			$varResult3 = $objDatabase->Query("UPDATE fms_accounts_budget SET Amount= Amount + '$dAmount' WHERE OrganizationId='" . cOrganizationId . "' AND BudgetId='$iBudgetId'");
				
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgethistory
			(BudgetId, Title, Amount, ChangeBudgetAmountType, BudgetHistoryAddedOn, BudgetHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetId', '$sTitle', '$dAmount', '$iChangeBudgetAmountType', '$varNow', '$iEmployeeId', '$sDescription', '$sDescription')");
		}
		else if($iChangeBudgetAmountType == 2)	// Decrease
		{
			$varResult = $objDatabase->Query("SELECT B.Amount FROM fms_accounts_budget AS B WHERE B.OrganizationId='" . cOrganizationId . "' AND B.BudgetId = '$iBudgetId'");
			$dBudgetAmount = $objDatabase->Result($varResult, 0, "B.Amount");
			
			// TODO
			$varResult2 = $objDatabase->Query("SELECT SUM(BA.Amount) AS 'AllocatedAmount' FROM fms_accounts_budgetallocation AS BA WHERE BA.BudgetId = '$iBudgetId'");
				if($objDatabase->RowsNumber($varResult2) > 0)	
					$dAllocatedAmount = $objDatabase->Result($varResult2, 0, "AllocatedAmount");
					
			$dRemainingAllocatedAmount = $dBudgetAmount - $dAllocatedAmount;
			$dRemainingAmount = 0;
			
			// If Remaining Amount is less than Descreasing Amount than System should not allow to decrease
			//if($dRemainingAmount < $dAmount) return(13006);
			if($dRemainingAllocatedAmount < $dAmount) return(13006);
			
			$varResult = $objDatabase->Query("UPDATE fms_accounts_budget SET Amount = Amount - '$dAmount'WHERE BudgetId = '$iBudgetId'");
						
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgethistory
			(BudgetId, Title, Amount, ChangeBudgetAmountType, BudgetHistoryAddedOn, BudgetHistoryAddedBy, Description, Notes)
			VALUES('$iBudgetId', '$sTitle', '$dAmount', '$iChangeBudgetAmountType', '$varNow', '$iEmployeeId', '$sDescription', '$sDescription')");
		}

		return(13002);
	}	
	
	function BudgetsList()
	{
		global $objDatabase;
		global $objEmployee;
		
		$aBudgets = array();
		
		$varResult = $objDatabase->Query("
		SELECT * 
		FROM fms_accounts_budget AS B		
		WHERE B.OrganizationId='" . cOrganizationId . "' AND B.Status='0'
		GROUP BY B.Title");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sBudgetCode = $objDatabase->Result($varResult, $i, "B.BudgetCode");
			$sTitle = $objDatabase->Result($varResult, $i, "B.Title");
			$iBudgetId = $objDatabase->Result($varResult, $i, "B.BudgetId");
			
			$aBudgets[$i][0] = $iBudgetId;
			$aBudgets[$i][1] = $sBudgetCode . ' - ' . $sTitle;
			$aBudgets[$i][2] = $sBudgetCode;
		}
		
		return($aBudgets);
	}
	
}
?>