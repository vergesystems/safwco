<?php

// Class: clsFMS_Budget_BudgetAllocation
class clsFMS_Accounts_BudgetAllocation
{	
	// Class Constructor
	public $aAllocationAmountType;
	public $aAllocationType;
	function __construct()
	{
		$this->aAllocationAmountType = array("Actual", "Increase", "Decrease");
		$this->aAllocationType = array("Monthly", "Quarterly", "On-whole");
	}

	
	function ShowAllBudgetAllocation($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");

		$sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        if ($sSortBy == "") $sSortBy = "BA.BudgetAllocationId DESC";

		$iPagingLimit = cPagingLimit;

		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Budget Allocation";
		if ($sSortBy == "") $sSortBy = "BA.BudgetAllocationId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((BA.Title LIKE '%$sSearch%') OR (BA.Description LIKE '%$sSearch%') OR (BA.Notes LIKE '%$sSearch%') OR (E.FirstName) OR (E.LastName)) ";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_budgetallocation AS BA INNER JOIN fms_accounts_budget AS B ON B.BudgetId = BA.BudgetId INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = BA.ChartOfAccountId INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BudgetAllocationAddedBy", "B.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_budgetallocation AS BA
		INNER JOIN fms_accounts_budget AS B ON B.BudgetId = BA.BudgetId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = BA.ChartOfAccountId
		INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BudgetAllocationAddedBy
		WHERE B.OrganizationId='" . cOrganizationId . "' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Budget&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Budget in Ascending Order" title="Sort by Budget in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Budget in Descending Order" title="Sort by Budget in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Chart of Account&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.AccountTitle&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Title in Ascending Order" title="Sort by Account Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CA.AccountTitle&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Title in Descending Order" title="Sort by Account Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.Amount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Amount in Ascending Order" title="Sort by Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.Amount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Amount in Descending Order" title="Sort by Amount in Descending Order" border="0" /></a></span></td>
		  <td width="4%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iBudgetAllocationId = $objDatabase->Result($varResult, $i, "BA.BudgetAllocationId");
			$sBudgetTitle = $objDatabase->Result($varResult, $i, "B.Title");
			$sTitle = $objDatabase->Result($varResult, $i, "BA.Title");
			$sChartOfAccount = $objDatabase->Result($varResult, $i, "CA.AccountTitle");
			$dAmount = $objDatabase->Result($varResult, $i, "BA.Amount");
			$sAmount = number_format($dAmount, 2);

			$sEditBudgetAllocation = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sTitle)) . '\', \'../accounts/allocation.php?pagetype=details&action2=edit&id=' . $iBudgetAllocationId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Details" title="Edit this Budget Details"></a></td>';
			$sDeleteBudgetAllocation = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Budget Allocation?\')) {window.location = \'?action=DeleteBudgetAllocation&id=' . $iBudgetAllocationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget Allocation" title="Delete this Budget Allocation"></a></td>';
			$sChangeAllocationAmount = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../accounts/allocation.php?pagetype=changeallocationamount&id=' . $iBudgetAllocationId. '\', \'Change Amount of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sTitle)) . '\', \'700 420\');"><img src="../images/icons/tick.gif" border="0" alt="Change Allocation Amount" title="Change Allocation Amount"></a></td>';
			//$sChangeBudgetAmount = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../accounts/budget_changeamount.php?id=' . $iBudgetId . '\', \'Change Amount of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sTitle)) . '\', \'700 420\');"><img src="../images/icons/tick.gif" border="0" alt="Change Budget Amount" title="Change Budget Amount"></a></td>';

			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[2] == 0) // Update Disabled
			{
				$sEditBudgetAllocation = '<td class="GridTD">&nbsp;</td>';
				$sChangeAllocationAmount = '<td class="GridTD">&nbsp;</td>';
			}
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[3] == 0) // Delete Disabled
				$sDeleteBudgetAllocation = '<td class="GridTD">&nbsp;</td>';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sBudgetTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sChartOfAccount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sTitle) . '\', \'../accounts/allocation.php?pagetype=details&id=' . $iBudgetAllocationId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Budget Allocation Details" title="View this Budget Allocation Details"></a></td>
			'. $sEditBudgetAllocation . $sChangeAllocationAmount.  $sDeleteBudgetAllocation . '</tr>';
		}

		$sAddBudgetAllocation = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Allocate Budget\', \'../accounts/allocation.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Allocate Budget">';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[1] == 0)
			$sAddBudgetAllocation = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddBudgetAllocation . '
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Budget Allocation:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Budget Allocation" title="Search for a Budget Allocation" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Budget Allocation Details" alt="View this Budget Allocation Details"></td><td>View this Budget Allocation Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Budget Allocation Details" alt="Edit this Budget Allocation Details"></td><td>Edit this Budget Allocation Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/tick.gif" title="Change Budget Allocation Amount" alt="Change Budget Allocation Amount"></td><td>Change Budget Allocation Amount</td></tr>
		 <!--<tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Budget Allocation" alt="Delete this Budget Allocation"></td><td>Delete this Budget Allocation</td></tr>-->
		</table>';

		return($sReturn);
	}

	function BudgetAllocationDetails($iBudgetAllocationId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *,BA.Notes AS Notes,BA.Description AS Description
		FROM fms_accounts_budgetallocation AS BA
		INNER JOIN fms_accounts_budget AS B ON B.BudgetId = BA.BudgetId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = BA.ChartOfAccountId
		INNER JOIN organization_stations AS S ON S.StationId = BA.StationId
		INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BudgetAllocationAddedBy
		WHERE B.OrganizationId='" . cOrganizationId . "' AND BA.BudgetAllocationId = '$iBudgetAllocationId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iBudgetAllocationId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Allocation" title="Edit this Budget Allocation" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Budget Allocation?\')) {window.location = \'?pagetype=details&action=DeleteBudgetAllocation&id=' . $iBudgetAllocationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget" title="Delete this Budget" /></a>';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[3] == 0)	$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Budget Allocation Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iBudgetId = $objDatabase->Result($varResult, 0, "B.BudgetId");
				$iBudgetAllocationId = $objDatabase->Result($varResult, 0, "BA.BudgetAllocationId");
				$iChartOfAccountId = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsId");

				$iStationId = $objDatabase->Result($varResult, $i, "BA.StationId");
				if($iStationId > 0)
				{
					$sStationName = $objDatabase->Result($varResult, $i, "S.StationName");
					$sStationName = $sStationName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' .  $objDatabase->RealEscapeString(str_replace('"', '', $sStationName)) .'\' , \'../organization/stations.php?pagetype=details&id=' . $iStationId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Station Details" title="Station Details" /></a>';
				}

				$iBudgetAllocationAddedBy = $objDatabase->Result($varResult, 0, "BA.BudgetAllocationAddedBy");
				$sBudgetAllocationAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sBudgetAllocationAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sBudgetAllocationAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iBudgetAllocationAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

				$sBudgetTitle = $objDatabase->Result($varResult, 0, "B.Title");
				$sBudgetTitle .='&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sBudgetTitle)) . '\', \'../accounts/budget.php?pagetype=details&id=' . $iBudgetId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Budget Information" title="Budget Information" border="0" /></a>';

				$sBudgetAllocationTitle = $objDatabase->Result($varResult, 0, "BA.Title");
				$sChartOfAccount = $objDatabase->Result($varResult, 0, "CA.AccountTitle");
				$sChartOfAccount .='&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sChartOfAccount)) . '\', \'../accounts/chartofaccounts.php?pagetype=details&id=' . $iChartOfAccountId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Information" title="Chart of Account Information" border="0" /></a>';

				$dAmount = $objDatabase->Result($varResult, 0, "BA.Amount");
				$sAmount = number_format($dAmount, 0);
				$iAllocationType = $objDatabase->Result($varResult, 0, "BA.AllocationType");
				$sAllocationType = $this->aAllocationType[$iAllocationType];

				$dRemainingAmount = 0;
				$dUsedAmount = 0;
				$varResult2 = $objDatabase->Query("
				SELECT
					SUM(GJE.Debit) AS 'UsedAmount'
				FROM fms_accounts_generaljournal_entries AS GJE
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
				WHERE GJ.OrganizationId='" . cOrganizationId . "' AND GJE.ChartOfAccountsId = '$iChartOfAccountId' AND GJE.BudgetId = '$iBudgetId'  AND GJ.IsDeleted ='0'");
				if($objDatabase->RowsNumber($varResult2) > 0)
					$dUsedAmount = $objDatabase->Result($varResult2, 0, "UsedAmount");

				$dRemainingAmount = $dAmount - $dUsedAmount;

				$sBudgetAllocationInfo = '<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
				 <tr><td class="Tahoma16">Amount:</td><td align="right" class="Tahoma16" style="color:blue;">' . number_format($dAmount, 0) . '</td></tr>
				 <tr><td class="Tahoma16">Utilized:</td><td align="right" class="Tahoma16" style="color:red;">' . number_format($dUsedAmount, 0) . '</td></tr>
				 <tr><td class="Tahoma16" style="border-top:1px solid;">Remaining Amount:</td><td align="right" class="Tahoma16" style="border-top:1px solid black; color:green;">' . number_format($dRemainingAmount, 0) . '</td></tr>
				</table>';

				// TODO
				$dRemainingAmount = 0;
				$sRemainingAmount = number_format($dRemainingAmount, 0);

				$sDescription = $objDatabase->Result($varResult, 0, "Description");
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
				
				$dBudgetAllocationAddedOn = $objDatabase->Result($varResult, 0, "BA.BudgetAllocationAddedOn");
				$sBudgetAllocationAddedOn = date("F j, Y", strtotime($dBudgetAllocationAddedOn)) . ' at ' . date("g:i a", strtotime($dBudgetAllocationAddedOn));
				
				$sAllocationHistory = '<tr bgcolor="#edeff1"><td align="left" colspan="4">
				<table border="0" cellspacing="0" cellpadding="5" width="98%" align="center">
				<tr><td colspan="5"><span style="font-family:Tahoma, Arial; font-size:22px;">Allocation History</span></td></tr>
				</table>
				<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="98%" align="center">
				<tr class="GridTR">
				 <td width="5%" align="center" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">S#</td>
				 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Date / Time</td>
				 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Title</td>
				 <td width="10%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Amount</td>
				 <td width="10%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Employee Name</td>
				 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Allocation Amount Type</td>
				 <td width="30%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Description</td>
				</tr>';
				
				$varResult = $objDatabase->Query("SELECT * 
				FROM fms_accounts_budgetallocationhistory AS AH
				INNER JOIN organization_employees AS E ON E.EmployeeId = AH.BudgetAllocationHistoryAddedBy
				WHERE AH.BudgetAllocationId	='$iBudgetAllocationId'");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					{
						$sEmployeeName = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
						$sHistoryTitle = $objDatabase->Result($varResult, $i, "AH.Title");
						$dHistoryAmount = $objDatabase->Result($varResult, $i, "AH.Amount");
						$sHistoryAmount = number_format($dHistoryAmount, 2);
						$sHistoryDescription = $objDatabase->Result($varResult, $i, "AH.Description");
						$iChangeAllocationAmountType = $objDatabase->Result($varResult, $i, "AH.ChangeAllocationAmountType");
						$sChangeAllocationAmountType = $this->aAllocationAmountType[$iChangeAllocationAmountType];
						$dBudgetAllocationHistoryAddedOn = $objDatabase->Result($varResult, $i, "AH.BudgetAllocationHistoryAddedOn");
						$sBudgetAllocationHistoryAddedOn = date("F j, Y", strtotime($dBudgetAllocationHistoryAddedOn)) . ' at ' . date("g:i a", strtotime($dBudgetAllocationHistoryAddedOn));
														
						$sAllocationHistory .= '<tr>
						 <td align="center" style="font-size:11px; font-family:Tahoma, Arial;">' . ($i+1) . '</td>
						 <td>' . $sBudgetAllocationHistoryAddedOn . '</td>
						 <td>' . $sHistoryTitle . '</td>
						 <td>' . $sHistoryAmount . '</td>
						 <td>' . $sEmployeeName . '</td>
						 <td>' . $sChangeAllocationAmountType . '</td>				 
						 <td>' . $sHistoryDescription . '</td>
						</tr>';						
					}
				}
				$sAllocationHistory .= '</table></td></tr>';
			}

			if ($sAction2 == "decrease")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sBudgetAllocationTitle = '<input type="text" name="txtBudgetAllocationTitle" id="txtBudgetAllocationTitle" class="form1" value="' . $sBudgetAllocationTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $sAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';							
			}
			if ($sAction2 == "increase")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sBudgetAllocationTitle = '<input type="text" name="txtBudgetAllocationTitle" id="txtBudgetAllocationTitle" class="form1" value="' . $sBudgetAllocationTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $sAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';								
			}
			else if ($sAction2 == "edit")
			{					
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';				
				
				$sChartOfAccount = '<select class="form1" name="selChartOfAccount" id="selChartOfAccount">
				<option value="0">Select Chart of Account</option>';
				$varResult = $objDatabase->Query("
				SELECT * FROM fms_accounts_chartofaccounts AS CA2
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA2.ChartOfAccountsControlId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CAT ON CAT.ChartOfAccountsCategoryId = CA2.ChartOfAccountsCategoryId
				WHERE CA2.OrganizationId='" . cOrganizationId . "' AND CA2.ChartOfAccountsId <> '$iChartOfAccountsId'
				ORDER BY CAT.ChartOfAccountsCategoryId, CC.ChartOfAccountsControlId, CA2.ChartOfAccountsCode");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					{
						if (($i == 0) || ($iTempChartOfAccountsCategoryId != $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId")))
						{
							$iTempChartOfAccountsCategoryId = $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId");
							$sChartOfAccount .= '<option style="color:blue;" value="0">' . $objDatabase->Result($varResult, $i, "CAT.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAT.CategoryName") . '</option>';
						}

						if (($i == 0) || ($iTempChartOfAccountsControlId != $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId")))
						{
							$iTempChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId");
							$sChartOfAccount .= '<option style="color:green;" value="0">&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CC.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CC.ControlName") . '</option>';
						}

						$sChartOfAccount .= '<option ' . (($iChartOfAccountId == $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult, $i, "CA2.AccountTitle") . '</option>';
					}
				}
				$sChartOfAccount .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selChartOfAccount\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccount\'), \'../accounts/chartofaccounts.php?pagetype=details&id=\'+GetSelectedListBox(\'selChartOfAccount\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Information" title="Chart of Account Information" border="0" /></a>';

				//End Chart of Accounts
				
				//Budget
				$sBudgetTitle = '<select name="selBudget" id="selBudget" class="form1">';
				$sBudgetTitle .= '<option value="0">Please Select Budget </option>';
				$varResultX = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.OrganizationId='" . cOrganizationId . "' ORDER BY B.Title");
				if($objDatabase->RowsNumber($varResultX) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResultX); $i++)				
						$sBudgetTitle .= '<option ' . (($iBudgetId == $objDatabase->Result($varResultX, $i, "B.BudgetId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResultX, $i, "B.BudgetId") . '">' . $objDatabase->Result($varResultX, $i, "B.Title") . '</option>';					
				}
				$sBudgetTitle .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selBudget\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selBudget\'), \'../accounts/budget.php?pagetype=details&id=\'+GetSelectedListBox(\'selBudget\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Budget Details" title="Budget Details" border="0" /></a>';				
				
				$sStationName = '<select name="selStation" id="selStation" class="form1">
				<option value="0"> Select Station</option>';
				$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' AND S.Status='1' ORDER BY S.StationName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult, $i, "S.StationId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "S.StationId") . '">' . $objDatabase->Result($varResult, $i, "S.StationName") . '</option>';
				$sStationName .='</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selStation\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Station Details" title="Station Details" border="0" /></a>';
				
				$sBudgetAllocationTitle = '<input type="text" name="txtBudgetAllocationTitle" id="txtBudgetAllocationTitle" class="form1" value="' . $sBudgetAllocationTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				//$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sAllocationType = '<select name="selAllocationType" id="selAllocationType" class="form1">';
				for($i = 0; $i < count($this->aAllocationType); $i++)
					$sAllocationType .= '<option ' . (($iAllocationType == $i)? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aAllocationType[$i] . '</option>';
				$sAllocationType .='</select></div>';
				
				
			}
			else if ($sAction2 == "addnew")
			{	
				$iBudgetAllocationId = "";
				$sBudgetAllocationTitle = $objGeneral->fnGet("txtBudgetAllocationTitle");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$dAmount = $objGeneral->fnGet("txtAmount");
				$dRemainingAmount = $objGeneral->fnGet("txtRemainingAmount");
				$iBudgetId = $objGeneral->fnGet("selBudget");
				$iChartOfAccountId = $objGeneral->fnGet("selChartOfAccount");
				
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sBudgetAllocationAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeFirstName)) . ' ' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeLastName)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				/*
				//Chart of Accounts
				// Only display Assets & Expenses, ChartOfAccountsCategoryId 1= Assets, 4= Expenses
				
				$sChartOfAccount = '<select name="selChartOfAccount" id="selChartOfAccount" class="form1">';
				$sChartOfAccount .= '<option value="0">Please Select Chart Of Account</option>';
				$varResultX = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.ChartOfAccountsCategoryId = '1' OR CA.ChartOfAccountsCategoryId = '4' ORDER BY CA.AccountTitle");
				if($objDatabase->RowsNumber($varResultX) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResultX); $i++)
					{
						$sChartOfAccount .= '<option' . (($iChartOfAccountId == $objDatabase->Result($varResultX, $i, "CA.ChartOfAccountsId")) ? 'selected="true"' : '')  . ' value="' . $objDatabase->Result($varResultX, $i, "CA.ChartOfAccountsId") . '">' . $objDatabase->Result($varResultX, $i, "CA.AccountTitle") . '</option>';
					}
				}
				$sChartOfAccount .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selChartOfAccount\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccount\'), \'../accounts/chartofaccounts.php?pagetype=details&id=\'+GetSelectedListBox(\'selChartOfAccount\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Details" title="Chart of Account Details" border="0" /></a>';
				//End Chart of Accounts
				*/
				$sChartOfAccount = '<select class="form1" name="selChartOfAccount" id="selChartOfAccount">
				<option value="0">Please Select Chart Of Account</option>';
				$varResult = $objDatabase->Query("
				SELECT * FROM fms_accounts_chartofaccounts AS CA2 
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA2.ChartOfAccountsControlId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CAT ON CAT.ChartOfAccountsCategoryId = CA2.ChartOfAccountsCategoryId
				WHERE CA2.OrganizationId='" . cOrganizationId . "' AND CA2.ChartOfAccountsId <> '$iChartOfAccountsId' 
				ORDER BY CAT.ChartOfAccountsCategoryId, CC.ChartOfAccountsControlId, CA2.ChartOfAccountsCode");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					{
						if (($i == 0) || ($iTempChartOfAccountsCategoryId != $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId")))
						{
							$iTempChartOfAccountsCategoryId = $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId");
							$sChartOfAccount .= '<option style="color:blue;" value="0">' . $objDatabase->Result($varResult, $i, "CAT.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAT.CategoryName") . '</option>';
						}
						
						if (($i == 0) || ($iTempChartOfAccountsControlId != $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId")))
						{
							$iTempChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId");
							$sChartOfAccount .= '<option style="color:green;" value="0">&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CC.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CC.ControlName") . '</option>';
						}
						
						$sChartOfAccount .= '<option ' . (($iChartOfAccountsId == $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult, $i, "CA2.AccountTitle") . '</option>';
					}
				}
				$sChartOfAccount .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selChartOfAccount\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccount\'), \'../accounts/chartofaccounts.php?pagetype=details&id=\'+GetSelectedListBox(\'selChartOfAccount\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Information" title="Chart of Account Information" border="0" /></a>';
				
				//Budget
				$sBudgetTitle = '<select name="selBudget" id="selBudget" class="form1">';
				$sBudgetTitle .= '<option value="0">Please Select Budget </option>';
				$varResultX = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.OrganizationId='" . cOrganizationId . "' ORDER BY B.Title");
				if($objDatabase->RowsNumber($varResultX) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResultX); $i++)					
						$sBudgetTitle .= '<option ' . (($iBudgetId == $objDatabase->Result($varResultX, $i, "B.BudgetId")) ? 'selected="true"' : '')  . ' value="' . $objDatabase->Result($varResultX, $i, "B.BudgetId") . '">' . $objDatabase->Result($varResultX, $i, "B.Title") . '</option>';					
				}
				$sBudgetTitle .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selBudget\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selBudget\'), \'../accounts/budget.php?pagetype=details&id=\'+GetSelectedListBox(\'selBudget\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Budget Details" title="Budget Details" border="0" /></a>';
				
				$sStationName = '<select name="selStation" id="selStation" class="form1">
				<option value="0"> Select Station</option>';
				$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' AND S.Status='1' ORDER BY S.StationName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult, $i, "S.StationId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "S.StationId") . '">' . $objDatabase->Result($varResult, $i, "S.StationName") . '</option>';
				$sStationName .='</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selStation\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Station Details" title="Station Details" border="0" /></a>';
				
				$sBudgetAllocationTitle = '<input type="text" name="txtBudgetAllocationTitle" id="txtBudgetAllocationTitle" class="form1" value="' . $sBudgetAllocationTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				
				$sAllocationType = '<select name="selAllocationType" id="selAllocationType" class="form1">';
				for($i = 0; $i < count($this->aAllocationType); $i++)
					$sAllocationType .= '<option value="' . $i . '">' . $this->aAllocationType[$i] . '</option>';
				$sAllocationType .='</select></div>';
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sBudgetAllocationAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}
						
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if(GetSelectedListBox(\'selBudget\') == "0") return(AlertFocus(\'Please select a Budget\', \'selBudget\'));
				if(GetSelectedListBox(\'selChartOfAccount\') == "0") return(AlertFocus(\'Please select a Chart of Account\', \'selChartOfAccount\'));
				if(GetSelectedListBox(\'selStation\') == "0") return(AlertFocus(\'Please select Station\', \'selStation\'));
				if (GetVal(\'txtBudgetAllocationTitle\') == "") return(AlertFocus(\'Please enter a valid Budget Allocation Title\', \'txtBudgetAllocationTitle\'));
				//if (!isDate(GetVal(\'txtBudgetStartDate\'))) return(AlertFocus(\'Please enter a valid Budget Start Date\', \'txtBudgetStartDate\'));
				//if (!isDate(GetVal(\'txtBudgetEndDate\'))) return(AlertFocus(\'Please enter a valid Budget End Date\', \'txtBudgetEndDate\'));
				if (!isNumeric(GetVal(\'txtAmount\'))) return(AlertFocus(\'Please enter a valid Amount\', \'txtAmount\'));				
				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Budget Allocation Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200;">Budget Title:</td><td><strong>' . $sBudgetTitle . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Chart of Accounts:</td><td><strong>' . $sChartOfAccount . '</strong></td><td style="width:300px;" valign="top" rowspan="4">' . $sBudgetAllocationInfo . '</td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Station:</td><td><strong>' . $sStationName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Budget Allocation Title:</td><td colspan="3"><strong>' . $sBudgetAllocationTitle . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Allocation Type:</td><td colspan="3"><strong>' . $sAllocationType . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Amount:</td><td colspan="3"><strong>' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount . '</strong></td></tr>
			 <!--<tr bgcolor="#ffffff"><td valign="top">Remaining Amount:</td><td colspan="3"><strong>' . $objEmployee->aSystemSettings["CurrencySign"] . $sRemainingAmount . '</strong></td></tr>-->
			 <tr bgcolor="#ffffff"><td valign="top" colspan="3">Description:</td></tr>
			 <tr><td colspan="4"><strong>' . $sDescription . '</strong></td></tr>
			 ' . $sAllocationHistory . '
			 <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td colspan="3"><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Added On:</td><td colspan="3"><strong>' . $sBudgetAllocationAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Allocation Added By:</td><td colspan="3"><strong>' . $sBudgetAllocationAddedBy . '</strong></td></tr>			 
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBudgetAllocationId . '"><input type="hidden" name="action" id="action" value="UpdateBudgetAllocation"><input type="hidden" name="txtAmount" id="txtAmount" value="' . $dAmount . '"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Allocate" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewBudgetAllocation"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "decrease")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Decrease" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="decrease"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "increase")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Increase" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="increase"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewBudgetAllocation($iBudgetId, $iChartOfAccountId, $iStationId, $sBudgetAllocationTitle, $iAllocationType, $dAmount, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[1] == 0)
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		$iEmployeeId = $objEmployee->iEmployeeId;
		//die($iBudgetId . ' - ' . $iChartOfAccountId . ' - ' . $sBudgetAllocationTitle . ' - ' . $dAmount . ' - ' . $sDescription . ' - ' . $sNotes);
		$sBudgetAllocationTitle = $objDatabase->RealEscapeString($sBudgetAllocationTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		$dAllocatedAmount = 0;
		if($objDatabase->DBCount("fms_accounts_budget AS B", " B.BudgetId = '$iBudgetId'") <= 0)
			return(13057);
		
		$varResult = $objDatabase->Query("SELECT B.Amount AS 'BudgetAmount' FROM fms_accounts_budget AS B WHERE B.OrganizationId='" . cOrganizationId . "' AND B.BudgetId = '$iBudgetId'");
		$dBudgetAmount = $objDatabase->Result($varResult, 0, "BudgetAmount");
		
		$varResult = $objDatabase->Query("SELECT SUM(BA.Amount) AS 'AllocatedAmount' FROM fms_accounts_budgetallocation AS BA WHERE BA.BudgetId = '$iBudgetId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$dAllocatedAmount = $objDatabase->Result($varResult, 0, "AllocatedAmount");
		}
		
		$dRemainingAllocationAmount = $dBudgetAmount - $dAllocatedAmount;
				
		//$dExpectedAllocatedAmount = $dAllocatedAmount + $dAmount;
		$dExpectedAllocatedAmount = $dAmount;
		if($dExpectedAllocatedAmount > $dRemainingAllocationAmount) return(13056);
		
		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_budgetallocation
		(OrganizationId, BudgetId, ChartOfAccountId, StationId, Title, AllocationType, Amount, Description, Notes, BudgetAllocationAddedBy, BudgetAllocationAddedOn)
		VALUES ('" . cOrganizationId . "', '$iBudgetId', '$iChartOfAccountId', '$iStationId', '$sBudgetAllocationTitle', '$iAllocationType', '$dAmount', '$sDescription', '$sNotes', '$iEmployeeId', '$varNow')");

		if ($varResult)
		{			
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budgetallocation AS BA WHERE BA.OrganizationId='" . cOrganizationId . "' AND BA.Title='$sBudgetAllocationTitle' AND BA.BudgetAllocationAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iBudgetAllocationId = $objDatabase->Result($varResult, 0, "BA.BudgetAllocationId");
				
				$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgetallocationhistory
				(BudgetAllocationId, Title, Amount, Description, Notes, BudgetAllocationHistoryAddedBy, BudgetAllocationHistoryAddedOn)
				VALUES ('$iBudgetAllocationId', '$sBudgetAllocationTitle', '$dAmount', '$sDescription', '$sNotes', '$iEmployeeId', '$varNow')");
				
				$objSystemLog->AddLog("Add Budget Allocation - " . $sBudgetAllocationTitle . ' for Chart Of Account Id ' . $iChartOfAccountId);

				$objGeneral->fnRedirect('../budget/allocation_details.php?error=13050&id=' . $iBudgetAllocationId);
			}
		}

		return(13051);
	}
	
	// Update Budget Allocation
	function UpdateBudgetAllocation($iBudgetAllocationId, $iBudgetId, $iChartOfAccountId, $iStationId, $sBudgetAllocationTitle, $iAllocationType, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[2] == 0)
			return(1010);
				
		$sBudgetAllocationTitle = $objDatabase->RealEscapeString($sBudgetAllocationTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_accounts_budget AS B", "B.OrganizationId='" . cOrganizationId . "' AND B.BudgetId = '$iBudgetId'") <= 0)
			return(13057);		
		
		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_budgetallocation 
		SET						
			BudgetId='$iBudgetId',			
			ChartOfAccountId='$iChartOfAccountId',
			StationId='$iStationId',
			Title='$sBudgetAllocationTitle',
			AllocationType='$iAllocationType',
			Description='$sDescription', 
			Notes='$sNotes'
		WHERE OrganizationId='" . cOrganizationId . "' AND BudgetAllocationId='$iBudgetAllocationId'");
		if ($varResult)
		{				
			$objSystemLog->AddLog("Update Budget Allocation  - " . $sBudgetAllocationTitle . ' for Chart Of Account Id ' . $iChartOfAccountId);

			$objGeneral->fnRedirect('../budget/allocation_details.php?error=13050&id=' . $iBudgetAllocationId);			
		}
		else
			return(13051);
	}
	
	function IncreaseOrDecreaseBudgetAllocation($iBudgetAllocationId, $dAmount, $sBudgetAllocationTitle)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		$sBudgetAllocationTitle = $objDatabase->RealEscapeString($sBudgetAllocationTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		$iEmployeeId = $objEmployee->iEmployeeId;
		$varNow = $objGeneral->fnNow();
		
		$varResult = $objDatabase->Query("
		SELECT B.BudgetId, B.RemainingAllocatedAmount, BA.RemainingAmount 
		FROM fms_accounts_budgetallocation AS BA
		INNER JOIN fms_accounts_budget AS B ON B.BudgetId = BA.BudgetId
		WHERE B.OrganizationId='" . cOrganizationId . "' AND BA.BudgetAllocationId = '$iBudgetAllocationId'");		
		$iBudgetId = $objDatabase->Result($varResult, 0, "B.BudgetId");
		$dRemainingAmount = $objDatabase->Result($varResult, 0, "BA.RemainingAmount");
		$dRemainingAllocatedAmount = $objDatabase->Result($varResult, 0, "B.RemainingAllocatedAmount");
		
		if($objGeneral->fnGet("action") == 'decrease')
		{	
			if($dRemainingAmount < $dAmount) return(13058);
			
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budget
			SET	
				RemainingAllocatedAmount= RemainingAllocatedAmount + '$dAmount'				
			WHERE BudgetId='$iBudgetId'");
			
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budgetallocation
			SET	
				Title='$sBudgetAllocationTitle', 
				Amount=Amount - '$dAmount',
				RemainingAmount = RemainingAmount - '$dAmount',
				Description='$sDescription',
				Notes='$sNotes'
			WHERE 
				BudgetAllocationId='$iBudgetAllocationId'");
				
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgetallocationhistory
			(BudgetAllocationId, Title, Amount, BudgetAllocationHistoryAddedOn, BudgetAllocationHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetAllocationId', '$sBudgetAllocationTitle', '$dAmount', '$varNow', '$iEmployeeId', '$sDescription', '$sNotes')");
		}		
		if($objGeneral->fnGet("action") == 'increase')	
		{
			//if($dRemainingAmount < $dAmount) return(13056);
			if($dRemainingAllocatedAmount < $dAmount) return(13058);
			
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budget
			SET	
				RemainingAllocatedAmount= RemainingAllocatedAmount - '$dAmount'				
			WHERE BudgetId='$iBudgetId'");
						
			$varResult3 = $objDatabase->Query("
			UPDATE fms_accounts_budgetallocation
			SET	
				Title='$sBudgetAllocationTitle', 
				Amount= Amount + '$dAmount',
				RemainingAmount= RemainingAmount + '$dAmount',
				Description='$sDescription',
				Notes='$sNotes'
			WHERE 
				BudgetAllocationId='$iBudgetAllocationId'");
				
			$varResult2 = $objDatabase->Query("
			INSERT INTO fms_accounts_budgetallocationhistory(BudgetAllocationId, Title, Amount, BudgetAllocationHistoryAddedOn, BudgetAllocationHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetAllocationId', '$sBudgetAllocationTitle', '$dAmount', '$varNow', '$iEmployeeId', '$sDescription', '$sNotes')");
		}
		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Update Budget - " . $sTitle);
			$objGeneral->fnRedirect('../accounts/allocation.php?pagetype=details&id='. $iBudgetAllocationId . ' &error=13052');
			//return(13002);
		}
		else
			return(13053);
	}
	
	function DeleteBudgetAllocation($iBudgetAllocationId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[3] == 0)
			return(1010);
				
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budgetallocation AS BA WHERE BA.OrganizationId='" . cOrganizationId . "' AND BA.BudgetAllocationId = '$iBudgetAllocationId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(13057);

		$sBudgetAllocationTitle = $objDatabase->Result($varResult, 0, "BA.Title");
		$iChartOfAccountsId = $objDatabase->Result($varResult, 0, "BA.ChartOfAccountId");
		
		//Check entries
		if ($objDatabase->DBCount("fms_accounts_generaljournal_entries GJE INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId", "GJE.ChartOfAccountsId= '$iChartOfAccountsId'  AND GJ.IsDeleted ='0'") > 0) return(13059);
		
		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_budgetallocation WHERE BudgetAllocationId = '$iBudgetAllocationId'");
		if ($varResult)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_accounts_budgetallocationhistory WHERE BudgetAllocationId = '$iBudgetAllocationId'");	
			$objSystemLog->AddLog("Delete Budget Allocation - " . $sBudgetAllocationTitle);
			
			return(13054);
		}
		else
			return(13055);
	}
	
	function ShowAllocationAmount($iBudgetAllocationId)
	{
		global $objDatabase;
		global $objEmployee;		
		
		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr><td colspan="5"><span style="font-family:Tahoma, Arial; font-size:22px;">Allocation Change History</span></td></tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr class="GridTR">
		 <td width="5%" align="center" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">S#</td>
		 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Date / Time</td>
		 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Title</td>
		 <td width="10%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Amount</td>
		 <td width="10%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Employee Name</td>
		 <td width="15%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Allocation Amount Type</td>
		 <td width="30%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Description</td>
		</tr>';
		
		$bAllowUpdate = true;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_budgetallocationhistory AS AH
		INNER JOIN organization_employees AS E ON E.EmployeeId = AH.BudgetAllocationHistoryAddedBy
		WHERE AH.BudgetAllocationId	='$iBudgetAllocationId'");
				
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			for ($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$sEmployeeName = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
				$sHistoryTitle = $objDatabase->Result($varResult, $i, "AH.Title");
				$dHistoryAmount = $objDatabase->Result($varResult, $i, "AH.Amount");
				$sHistoryAmount = number_format($dHistoryAmount, 2);
				$sHistoryDescription = $objDatabase->Result($varResult, $i, "AH.Description");
				$iChangeAllocationAmountType = $objDatabase->Result($varResult, $i, "AH.ChangeAllocationAmountType");
				$sChangeAllocationAmountType = $this->aAllocationAmountType[$iChangeAllocationAmountType];
				$dBudgetAllocationHistoryAddedOn = $objDatabase->Result($varResult, $i, "AH.BudgetAllocationHistoryAddedOn");
				$sBudgetAllocationHistoryAddedOn = date("F j, Y", strtotime($dBudgetAllocationHistoryAddedOn)) . ' at ' . date("g:i a", strtotime($dBudgetAllocationHistoryAddedOn));
												
				$sReturn .= '<tr>
				 <td align="center" style="font-size:11px; font-family:Tahoma, Arial;">' . ($i+1) . '</td>
				 <td>' . $sBudgetAllocationHistoryAddedOn . '</td>
				 <td>' . $sHistoryTitle . '</td>
				 <td>' . $sHistoryAmount . '</td>
				 <td>' . $sEmployeeName . '</td>
				 <td>' . $sChangeAllocationAmountType . '</td>				 
				 <td>' . $sHistoryDescription . '</td>
				</tr>';
			}
		}
		$sReturn .= '</table></td></tr>';

		$sChangeAllocationAmountType = '<select name="selChangeAllocationAmountType" id="selChangeAllocationAmountType" style="font-size:14px; font-family:Tahoma, Arial;">';
		for ($i=1; $i < count($this->aAllocationAmountType); $i++)
			$sChangeAllocationAmountType .= '<option value="' . $i . '">' . $this->aAllocationAmountType[$i] . '</option>';
		$sChangeAllocationAmountType .= '</select>';
		
		$sReturn .= '
		<br />
		&nbsp;&nbsp;<img src="../images/icons/plus.gif" align="absmiddle" alt="Update Amount" title="Update Amount" border="0" /><a href="#noanchor" style="font-size:18px; font-family:Tahoma, Arial; color:green;" onclick="ShowHideDiv(\'divUpdateAmount\');">&nbsp;Update Amount</a>&nbsp;&nbsp;
		<div id="divUpdateAmount" style="display:none;" align="center">
		 <br />
		 <table border="1" bordercolor="#cdcdcd" cellspacing="0" cellpadding="5" width="80%" align="center">
		  <tr>
		   <td>
			<form method="post" action="" onsubmit="return window.top.MOOdalBox.open(\'../accounts/allocation_changeamount.php?&id=' . $iBudgetAllocationId. '&action=changeamount&selChangeAllocationAmountType=\'+GetSelectedListBox(\'selChangeAllocationAmountType\')+\'&txtTitle=\'+GetVal(\'txtTitle\')+\'&txtAmount=\'+GetVal(\'txtAmount\')+\'&txtDescription=\'+GetVal(\'txtDescription\'), \'' . $sAllocationTitle . '\', \'700 420\');">
			<span style="font-size:14px; color:blue;">' . $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '</span>:&nbsp;<span style="font-size:11px; color:green;">[ ' . date("F j, Y g:i a") . ' ]</span>&nbsp;<br />
			<br />
			<div align="center">
			' . $sChangeAllocationAmountType . '
			<br /><br />
			<span class="Tahoma16">Title:</span>
			<input type="text" id="txtTitle" name="txtTitle" /><br /><br />
			<span class="Tahoma16">Amount:</span>
			<input type="text" id="txtAmount" name="txtAmount" /><br /> 
			<br /><br />
			<span class="Tahoma16">Description:</span><br />
			<textarea name="txtDescription" id="txtDescription" style="font-size:14px; font-family:Tahoma, Arial; width:500px; height:50px;"></textarea>
			</div>
			<br /><br />
			<div align="center"><input type="submit" class="AdminFormButton1" value="Submit" /></div>
			</form>
		   </td>
		  </tr>
		 </table>
		</div>';
							
		return($sReturn);		
	}
	
	function ChangeAmount($iBudgetAllocationId, $sTitle, $dAmount, $iChangeAllocationAmountType, $sDescription)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_BudgetAllocations[2] == 0)
			return(1010);
				
		$iEmployeeId = $objEmployee->iEmployeeId;		
		$varNow = $objGeneral->fnNow();
		
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		if($dAmount == "") $dAmount = 0;
		if(!is_numeric($dAmount)) return(13053);
		
		$varResult = $objDatabase->Query("SELECT B.Amount AS 'BudgetAmount' FROM fms_accounts_budget AS B 
		INNER JOIN fms_accounts_budgetallocation  AS BA ON BA.BudgetId = B.BudgetId
		WHERE B.OrganizationId='" . cOrganizationId . "' AND BA.BudgetAllocationId = '$iBudgetAllocationId'");
		$dBudgetAmount = $objDatabase->Result($varResult, 0, "BudgetAmount");
		
		$varResult = $objDatabase->Query("SELECT SUM(BA.Amount) AS 'AllocatedAmount' FROM fms_accounts_budgetallocation AS BA WHERE BA.OrganizationId='" . cOrganizationId . "' AND BA.BudgetId = '$iBudgetId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$dAllocatedAmount = $objDatabase->Result($varResult, 0, "AllocatedAmount");
		}
		
		$dRemainingAllocationAmount = $dBudgetAmount - $dAllocatedAmount;
		
		if($iChangeAllocationAmountType == 1)		// Increase Budget Amount
		{
			//if($dRemainingAmount < $dAmount) return(13056);
			if($dRemainingAllocationAmount < $dAmount) return(13058);
			
			$varResult3 = $objDatabase->Query("
			UPDATE fms_accounts_budgetallocation
			SET
				Amount= Amount + '$dAmount'				
			WHERE OrganizationId='" . cOrganizationId . "' AND BudgetAllocationId='$iBudgetAllocationId'");
				
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgetallocationhistory
			(BudgetAllocationId, Title, Amount, ChangeAllocationAmountType, BudgetAllocationHistoryAddedOn, BudgetAllocationHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetAllocationId', '$sTitle', '$dAmount', '$iChangeAllocationAmountType', '$varNow', '$iEmployeeId', '$sDescription', '$sDescription')");
			
		}
		else if($iChangeAllocationAmountType == 2)	// Decrease
		{			
			$varResult3 = $objDatabase->Query("
			UPDATE fms_accounts_budgetallocation
			SET
				Amount= Amount - '$dAmount'				
			WHERE OrganizationId='" . cOrganizationId . "' AND BudgetAllocationId='$iBudgetAllocationId'");
				
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgetallocationhistory
			(BudgetAllocationId, Title, Amount, ChangeAllocationAmountType, BudgetAllocationHistoryAddedOn, BudgetAllocationHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetAllocationId', '$sTitle', '$dAmount', '$iChangeAllocationAmountType', '$varNow', '$iEmployeeId', '$sDescription', '$sDescription')");			
		}
		
		return(13052);
	}

}
?>