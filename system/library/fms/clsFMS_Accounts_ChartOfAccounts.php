<?php

// Class: Chart Of Accounts
class clsChartOfAccounts
{
	public $aChartOfAccountStatus;
	// Class Constructor
	function __construct()
	{
		$this->aChartOfAccountStatus = array("In-active", "Active");
	}
	
	function ShowAllChartOfAccounts($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Chart Of Accounts";
		if ($sSortBy == "") $sSortBy = "CA.ChartOfAccountsId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((CA.AccountTitle LIKE '%$sSearch%') OR (CA.ChartOfAccountsId LIKE '%$sSearch%') OR (CA.ChartOfAccountsCode LIKE '%$sSearch%') OR (CA.Notes LIKE '%$sSearch%')) ";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_chartofaccounts AS CA INNER JOIN fms_accounts_chartofaccounts_categories AS CAC ON CAC.ChartOfAccountsCategoryId=CA.ChartOfAccountsCategoryId INNER JOIN fms_accounts_chartofaccounts_controls AS CACT ON CACT.ChartOfAccountsControlId=CA.ChartOfAccountsControlId INNER JOIN organization_employees AS E ON E.EmployeeId = CA.ChartOfAccountsAddedBy", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT
		*
		FROM fms_accounts_chartofaccounts AS CA
		INNER JOIN fms_accounts_chartofaccounts_categories AS CAC ON CAC.ChartOfAccountsCategoryId=CA.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts_controls AS CACT ON CACT.ChartOfAccountsControlId=CA.ChartOfAccountsControlId
		LEFT JOIN fms_accounts_chartofaccounts_controls AS PCACT ON PCACT.ChartOfAccountsControlId=CACT.ChartOfAccountsControlParentId
		INNER JOIN organization_employees AS E ON E.EmployeeId = CA.ChartOfAccountsAddedBy
		WHERE CA.OrganizationId='" . cOrganizationId . "' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td width="15%" align="left"><span class="WhiteHeading">Category Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsCategoryId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Chart of Accounts Category Id in Ascending Order" title="Sort by Chart of Accounts Category Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsCategoryId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Chart of Accounts Category Id in Descending Order" title="Sort by Chart of Accounts Category Id in Descending Order" border="0" /></a></span></td>
		  <td width="15%" align="left"><span class="WhiteHeading">Control Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsControlId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Chart of Accounts Control Code in Ascending Order" title="Sort by Chart of Accounts Control Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsControlId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Chart of Accounts Control Code in Descending Order" title="Sort by Chart of Accounts Control Code in Descending Order" border="0" /></a></span></td>
		  <td width="15%" align="left"><span class="WhiteHeading">Parent Control&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=PCACT.ChartOfAccountsControlId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Chart of Accounts Control Code in Ascending Order" title="Sort by Chart of Accounts Control Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=PCACT.ChartOfAccountsControlId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Chart of Accounts Control Code in Descending Order" title="Sort by Chart of Accounts Control Code in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Account Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Code in Ascending Order" title="Sort by Account Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Code in Descending Order" title="Sort by Account Code in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Account Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.AccountTitle&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Title in Ascending Order" title="Sort by Account Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CA.AccountTitle&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Title in Descending Order" title="Sort by Account Title in Descending Order" border="0" /></a></span></td>
		  <td width="4%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsId");
			$sCategoryCode = $objDatabase->Result($varResult, $i, "CAC.CategoryCode") . ' - ' .  $objDatabase->Result($varResult, $i, "CAC.CategoryName");
			$sControlCode = $objDatabase->Result($varResult, $i, "CACT.ControlCode") . ' - ' .  $objDatabase->Result($varResult, $i, "CACT.ControlName");
			$sParentControlCode = $objDatabase->Result($varResult, $i, "PCACT.ControlCode") . ' - ' .  $objDatabase->Result($varResult, $i, "PCACT.ControlName");
			$dBalance = 0;
			$sAccountTitle = $objDatabase->Result($varResult, $i, "CA.AccountTitle");
			$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode");

			$sEditChartOfAccount = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sAccountTitle)) . '\', \'../accounts/chartofaccounts.php?pagetype=details&action2=edit&id=' . $iChartOfAccountsId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Chart Of Account Details" title="Edit this Chart Of Account Details"></a></td>';
			$sDeleteChartOfAccount = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Chart Of Account?\')) {window.location = \'?action=DeleteChartOfAccount&id=' . $iChartOfAccountsId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete Chart Of Account" title="Delete Chart Of Account"></a></td>';

			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[2] == 0)$sEditChartOfAccount = '';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[3] == 0)$sDeleteChartOfAccount = '';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sCategoryCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sControlCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sParentControlCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sChartOfAccountsCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sAccountTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sAccountTitle)) . '\', \'../accounts/chartofaccounts.php?pagetype=details&id=' . $iChartOfAccountsId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Chart Of Account Details" title="View this Chart Of Account Details"></a></td>
			' . $sEditChartOfAccount . '
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Transaction Details\', \'../accounts/generalledger.php?pagetype=details&id=' . $iChartOfAccountsId . '\', \'520px\', true);"><img src="../images/icons/iconExport.gif" border="0" alt="View Transaction Details" title="View Transaction Details"></a></td>
			' . $sDeleteChartOfAccount . '</tr>';
		}

		$sAddChartOfAccount = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Chart Of Account\', \'../accounts/chartofaccounts.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Account">';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[1] == 0) // Add Disabled
			$sAddChartOfAccount = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddChartOfAccount . '
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Chart Of Account:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Chart Of Account" title="Search for a Chart Of Account" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Chart Of Account Details" alt="View this Chart Of Account Details"></td><td>View this Chart Of Account Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Chart Of Account Details" alt="Edit this Chart Of Account Details"></td><td>Edit this Chart Of Account Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconExport.gif" border="0" alt="View Transaction Details" title="View Transaction Details"></td><td>View Transaction Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Chart Of Account" alt="Delete this Chart Of Account"></td><td>Delete this Chart Of Account</td></tr>
		</table>';

		return($sReturn);
	}

	function ChartOfAccountDetails($iChartOfAccountsId, $iCategoryId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		//die($iChartOfAccountsId);
		//LEFT JOIN organization_stations AS S ON S.StationId = CA.StationId
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();


		$varResult = $objDatabase->Query("
		SELECT *,CA.Notes AS Notes,CA.AccountTitle AS AccountTitle,CA.ChartOfAccountsCode AS ChartOfAccountsCode
		FROM fms_accounts_chartofaccounts AS CA
		LEFT JOIN fms_accounts_chartofaccounts AS CA2 ON CA2.ChartOfAccountsId = CA.ParentChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CAC ON CAC.ChartOfAccountsCategoryId=CA.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts_controls AS CACT ON CACT.ChartOfAccountsControlId=CA.ChartOfAccountsControlId
		INNER JOIN organization_employees AS E ON E.EmployeeId = CA.ChartOfAccountsAddedBy
		WHERE CA.ChartOfAccountsId = '$iChartOfAccountsId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iChartOfAccountsId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Chart Of Account" title="Edit this Chart Of Account" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Chart Of Account?\')) {window.location = \'?action=DeleteChartOfAccount&id=' . $iChartOfAccountsId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Chart Of Account" title="Delete this Chart Of Account" /></a>';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[2] == 0)
			$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[3] == 0)
			$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Chart Of Account Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iChartOfAccountsId = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsId");
				$iChartOfAccountsCategoryId = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsCategoryId");
				$iChartOfAccountsControlId = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsControlId");
				$iParentChartOfAccountsId = $objDatabase->Result($varResult, 0, "CA.ParentChartOfAccountsId");
				$sParentChartOfAccountTitle = $objDatabase->Result($varResult, 0, "CA2.AccountTitle");
				if ($sParentChartOfAccountTitle == '') $sParentChartOfAccountTitle = 'No Parent';
		        else $sParentChartOfAccountTitle = $sParentChartOfAccountTitle . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' . $sParentChartOfAccountTitle .'\' , \'../accounts/chartofaccounts.php?pagetype=details&id=' . $iParentChartOfAccountsId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Chart of Account Parent Details" title="Chart of Account Parent Details" /></a>';

				$sAccountTitle = $objDatabase->Result($varResult, 0, "AccountTitle");
				$iChartOfAccountsCode = $objDatabase->Result($varResult, 0, "ChartOfAccountsCode");
				$sChartOfAccountsCode = $iChartOfAccountsCode;
				/*
				$iSourceOfIncomeId = $objDatabase->Result($varResult, $i, "CA.SourceOfIncomeId");
				$sSourceOfIncome = "";
				if($iSourceOfIncomeId > 0)
				{
					$sSourceOfIncome = $objDatabase->Result($varResult, $i, "SOI.Title");					
					$sSourceOfIncome = $sSourceOfIncome . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' .  $objDatabase->RealEscapeString(str_replace('"', '', $sSourceOfIncome)) .'\' , \'../accounts/sourceofincomephp?pagetype=details&id=' . $iSourceOfIncomeId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Source of Income Details" title="Source of Income Details" /></a>';
				}
				*/
				//$sAccountType = $objDatabase->Result($varResult, 0, "CAT.Title");				
				$sCategory = $objDatabase->Result($varResult, 0, "CAC.CategoryCode") . ' - ' . $objDatabase->Result($varResult, 0, "CAC.CategoryName");
				$sControlCode = $objDatabase->Result($varResult, 0, "CACT.ControlCode") . ' - ' . $objDatabase->Result($varResult, 0, "CACT.ControlName");
				/*
				$iDonorId = $objDatabase->Result($varResult, $i, "CA.DonorId");
				$iDonorProjectId = $objDatabase->Result($varResult, $i, "CA.DonorProjectId");
				$iActivityId = $objDatabase->Result($varResult, $i, "CA.DonorProjectActivityId");
				if($iDonorId > 0)
				{
					$sDonor = $objDatabase->Result($varResult, $i, "D.DonorName");
					if($iDonorProjectId > 0 ) $sDonorProject = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
					if($iActivityId > 0 ) $sProjectActivities = $objDatabase->Result($varResult, $i, "PA.ActivityTitle");
					$sDonorRow = '<tr id="trDonors" style="display:visible;" bgcolor="#edeff1"><td valign="top">Donor:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sDonor. '&nbsp;&raquo;&nbsp;</td><td>' . $sDonorProject . '&nbsp;&raquo;&nbsp;</td><td>' . $sProjectActivities . '</td></tr></table></td></tr>';
				}
				*/
				$iStatus = $objDatabase->Result($varResult, 0, "CA.Status");
				$sChartOfAccountStatus = $this->aChartOfAccountStatus[$iStatus];
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
				$dChartOfAccountsAddedOn = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsAddedOn");
				$sChartOfAccountsAddedOn = date("F j, Y", strtotime($dChartOfAccountsAddedOn)) . ' at ' . date("g:i a", strtotime($dChartOfAccountsAddedOn));
				
				$iChartOfAccountsAddedBy = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsAddedBy");
				$sChartOfAccountsAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sChartOfAccountsAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName)) . '\', \'../organization/employeesphp?pagetype=details&id=' . $iChartOfAccountsAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
			}
			
			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">
				<script language="JavaScript" type="text/javascript">
				function ValidateForm()
				{
					if(GetSelectedListBox(\'selCategory\') == 0) return(AlertFocus(\'Please select Category Code\', \'selCategory\'));
					if(GetSelectedListBox(\'selControl\') == 0) return(AlertFocus(\'Please select Control Code\', \'selControl\'));
					if (GetVal(\'txtChartOfAccountsCode\') == "") return(AlertFocus(\'Please enter a valid Chart of Account Code\', \'txtChartOfAccountsCode\'));
					if (GetVal(\'txtAccountTitle\') == "") return(AlertFocus(\'Please enter a valid Chart of Account Title\', \'txtAccountTitle\'));
					return true;
				}				
				</script>';
				
				$sChartOfAccountStatus = '<select class="form1" id="selChartOfAccountStatus" name="selChartOfAccountStatus">';
				for($i=0; $i < count($this->aChartOfAccountStatus); $i++)									
					$sChartOfAccountStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aChartOfAccountStatus[$i] . '</option>';				
				$sChartOfAccountStatus .='</select>';
				
				/*
				$sSourceOfIncome = '<select name="selSourceOfIncome" id="selSourceOfIncome" class="form1">
				<option value="0">Select Source of Income </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_sourceofincome AS SOI ORDER BY SOI.Title");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sSourceOfIncome .= '<option ' . (($iSourceOfIncomeId == $objDatabase->Result($varResult, $i, "SOI.SourceOfIncomeId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "SOI.SourceOfIncomeId") . '">' . $objDatabase->Result($varResult, $i, "SOI.Title") . '</option>';
				$sSourceOfIncome .='</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selSourceOfIncome\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selSourceOfIncome\'), \'../accounts/sourceofincomephp?pagetype=details&id=\'+GetSelectedListBox(\'selSourceOfIncome\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Source Of Income Information" title="Source Of Income Information" border="0" /></a>';
				*/
				/*
				$sAccountType = '<select class="form1" name="selAccountType" id="selAccountType">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_types AS CAT ORDER BY CAT.Title");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sAccountType .= '<option ' . (($iChartOfAccountsTypeId == $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsTypeId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsTypeId") . '">' . $objDatabase->Result($varResult, $i, "CAT.Title") . '</option>';
				}
				$sAccountType .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selAccountType\'), \'../accounts/chartofaccounttypesphp?pagetype=details&id=\'+GetSelectedListBox(\'selAccountType\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Account Type Information" title="Account Type Information" border="0" /></a>';
				*/
				if ($iCategoryId != '') $iChartOfAccountsCategoryId = $iCategoryId;
				$sCategory = '<select onchange="GetControlCode();" class="form1" name="selCategory" id="selCategory">
				<option value="0">Select Category Code</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCategory .= '<option ' . (($iChartOfAccountsCategoryId == $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsCategoryId")) ? 'selected="true"' : '')  . ' value="' . $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsCategoryId") . '">' . $objDatabase->Result($varResult, $i, "CAC.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAC.CategoryName") . '</option>';
				$sCategory .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="jsOpenWindow(\'../accounts/chartofaccounts_categorycodes.php?id=' . $iChartOfAccountsId . '&action2=' . $sAction2 . '\', 500, 500);"><img src="../images/icons/plus.gif" border="0" alt="Add New Category Code" title="Add New Category Code" /></a>';

				if ($iChartOfAccountsCategoryId == '') $iChartOfAccountsCategoryId = 1;

				$sControlCode = '<select class="form1" name="selControl" id="selControl" onChange="GetChartOfAccountCode();">
				<option value="0">Select Control Code</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CACT WHERE CACT.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId'");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sControlCode .= '<option ' . (($iChartOfAccountsControlId == $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId")) ? 'selected="true"' : '')  . ' value="' . $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId") . '">' . $objDatabase->Result($varResult, $i, "CACT.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CACT.ControlName") . '</option>';
				$sControlCode .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="jsOpenWindow(\'../accounts/chartofaccounts_controlcodes.php?action2=' . $sAction2 . '&id=' . $iChartOfAccountsId . '&catid=\'+GetSelectedListBox(\'selCategory\'), \'700\', \'500\');"><img src="../images/icons/plus.gif" border="0" alt="Add New Control Code" title="Add New Control Code" /></a>';
				
				/*
				$sDonor = '<select name="selDonor" id="selDonor" onchange="GetDonorProjects(GetSelectedListBox(\'selDonor\'),0);" class="form1">
				<option value="0">Select Donor</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sDonor .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
				$sDonor .='</select>';
				
				$sDonorProject = '<div id="divDonorProject" style="display:visible;"><select name="selDonorProject" id="selDonorProject" onchange="GetProjectActivities(GetSelectedListBox(\'selDonorProject\'),0);" class="form1">
				<option value="0">Select Project</option>';				
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.DonorId = '$iDonorId' ORDER BY DP.ProjectTitle");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sDonorProject .= '<option ' . (($iDonorProjectId == $objDatabase->Result($varResult, $i, "DP.DonorProjectId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '</option>';
				$sDonorProject .='</select>&nbsp;&raquo;&nbsp;</div>';
				
				$sProjectActivity = '<div id="divProjectActivity" style="display:visible;"><select name="selProjectActivity" id="selProjectActivity" class="form1">
				<option value="0">Select Project</option>';				
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.ActivityId = '$iDonorProjectId' ORDER BY PA.ActivityTitle");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sProjectActivity .= '<option ' . (($iDonorProjectId == $objDatabase->Result($varResult, $i, "PA.ActivityId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "PA.ActivityId") . '">' . $objDatabase->Result($varResult, $i, "PA.ActivityTitle") . '</option>';
				$sProjectActivity .='</select></div>';
								
				$sDonorRow = '<tr id="trDonors" style="display:visible;" bgcolor="#edeff1"><td valign="top">Donor:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sDonor  . '&nbsp;&raquo;&nbsp;</td><td>' . $sDonorProject . '</td><td>' . $sProjectActivity . '</td></tr></table></td></tr>';
				*/

				$sParentChartOfAccountTitle = '<select class="form1" name="selChartOfAccountParent" id="selChartOfAccountParent">
				<option value="0">No Parent</option>';
				$varResult = $objDatabase->Query("
				SELECT * FROM fms_accounts_chartofaccounts AS CA2
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA2.ChartOfAccountsControlId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CAT ON CAT.ChartOfAccountsCategoryId = CA2.ChartOfAccountsCategoryId
				WHERE CA2.ChartOfAccountsId <> '$iChartOfAccountsId' AND CA2.ChartOfAccountsControlId = '$iChartOfAccountsControlId' AND CA2.ParentChartOfAccountsId = '0'
				ORDER BY CAT.ChartOfAccountsCategoryId, CC.ChartOfAccountsControlId, CA2.ChartOfAccountsCode");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					{
						if (($i == 0) || ($iTempChartOfAccountsCategoryId != $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId")))
						{
							$iTempChartOfAccountsCategoryId = $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId");
							$sParentChartOfAccountTitle .= '<option style="color:blue;" value="0">' . $objDatabase->Result($varResult, $i, "CAT.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAT.CategoryName") . '</option>';
						}

						if (($i == 0) || ($iTempChartOfAccountsControlId != $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId")))
						{
							$iTempChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId");
							$sParentChartOfAccountTitle .= '<option style="color:green;" value="0">&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CC.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CC.ControlName") . '</option>';
						}

						$sParentChartOfAccountTitle .= '<option ' . (($iParentChartOfAccountsId == $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult, $i, "CA2.AccountTitle") . '</option>';
					}
				}
				$sParentChartOfAccountTitle .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selChartOfAccountParent\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccountParent\'), \'../accounts/chartofaccounts.php?pagetype=details&id=\'+GetSelectedListBox(\'selChartOfAccountParent\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Parent Information" title="Chart of Account Parent Information" border="0" /></a>';

				//$sAsOf = '<input size="10" type="text" id="txtAsOf" name="txtAsOf" class="form1" value="' . $dAsOf. '" />' . $objjQuery->Calendar('txtAsOf');
								
				$sChartOfAccountsCode = '<input type="text" name="txtChartOfAccountsCode" id="txtChartOfAccountsCode" class="form1" value="' . $iChartOfAccountsCode . '" size="20" />&nbsp;<span style="color:red;">*</span>';
				$sAccountTitle = '<input type="text" name="txtAccountTitle" id="txtAccountTitle" class="form1" value="' . $sAccountTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				
				/*
				// Donor Projects
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.Status='1'");
				$iNoOfDonorProjects = $objDatabase->RowsNumber($varResult);
				$sDonorProjects = 'var aDonorProjects = MultiDimensionalArray(' . $iNoOfDonorProjects . ', 4);';
				for ($i=0; $i < $iNoOfDonorProjects; $i++)
				{
					$sDonorProjects .= 'aDonorProjects[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "DP.DonorId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '";';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '";';
				}
				
				// Project Activities
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.Status='1'");
				$iNoOfProjectActivities = $objDatabase->RowsNumber($varResult);
				$sProjectActivities = 'var aProjectActivities= MultiDimensionalArray(' . $iNoOfProjectActivities . ', 4);';
				for ($i=0; $i < $iNoOfProjectActivities; $i++)
				{
					$sProjectActivities .= 'aProjectActivities[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "PA.DonorProjectId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "PA.ActivityId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityCode") . '";';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityTitle") . '";';
				}
				*/
				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{				
				$iChartOfAccountsId = "";
				$iChartOfAccountsCategoryId = $iCategoryId;
				//$sTransactionCode = $objGeneral->fnGet("txtTransactionCode");
				$sAccountTitle = $objGeneral->fnGet("txtAccountTitle");
				$iChartOfAccountsCode = $objGeneral->fnGet("txtChartOfAccountsCode");				
				$sNotes = $objGeneral->fnGet("txtNotes");
				$iStatus = 1;

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">
				<script language="JavaScript" type="text/javascript">
				function ValidateForm()
				{	
					if(GetSelectedListBox(\'selCategory\') == 0) return(AlertFocus(\'Please select Category Code\', \'selCategory\'));
					if(GetSelectedListBox(\'selControl\') == 0) return(AlertFocus(\'Please select Control Code\', \'selControl\'));
					if (GetVal(\'txtAccountTitle\') == "") return(AlertFocus(\'Please enter a valid Chart of Account Title\', \'txtAccountTitle\'));
					if (GetVal(\'txtChartOfAccountsCode\') == "") return(AlertFocus(\'Please enter a valid Chart of Account Code\', \'txtChartOfAccountsCode\'));

					return true;
				}
				</script>';
				$sChartOfAccountsAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName)) . '\', \'../organization/employeesphp?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sChartOfAccountStatus = '<select class="form1" id="selChartOfAccountStatus" name="selChartOfAccountStatus">';
				for($i=0; $i < count($this->aChartOfAccountStatus); $i++)									
					$sChartOfAccountStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aChartOfAccountStatus[$i] . '</option>';				
				$sChartOfAccountStatus .='</select>';
				/*
				$sAccountType = '<select class="form1" name="selAccountType" id="selAccountType">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_types AS CAT ORDER BY CAT.Title");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sAccountType .= '<option ' . (($iChartOfAccountsTypeId == $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsTypeId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsTypeId") . '">' . $objDatabase->Result($varResult, $i, "CAT.Title") . '</option>';
				}
				$sAccountType .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selAccountType\'), \'../accounts/chartofaccounttypesphp?pagetype=details&id=\'+GetSelectedListBox(\'selAccountType\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Account Type Information" title="Account Type Information" border="0" /></a>';
				$sSourceOfIncome = '<select name="selSourceOfIncome" id="selSourceOfIncome" class="form1">
				<option value="0">Select Source of Income </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_sourceofincome AS SOI ORDER BY SOI.Title");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sSourceOfIncome .= '<option value="' . $objDatabase->Result($varResult, $i, "SOI.SourceOfIncomeId") . '">' . $objDatabase->Result($varResult, $i, "SOI.Title") . '</option>';
				$sSourceOfIncome .='</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selSourceOfIncome\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selSourceOfIncome\'), \'../accounts/sourceofincomephp?pagetype=details&id=\'+GetSelectedListBox(\'selSourceOfIncome\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Source Of Income Information" title="Source Of Income Information" border="0" /></a>';
				*/
				
				/*
				$sDonor = '<select name="selDonor" id="selDonor" onchange="GetDonorProjects(GetSelectedListBox(\'selDonor\'),0);" class="form1">
				<option value="0">Select Donor</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sDonor .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
				$sDonor .='</select>';
				
				$sDonorProject = '<div id="divDonorProject" style="display:none;"><select name="selDonorProject" id="selDonorProject" onchange="GetProjectActivities(GetSelectedListBox(\'selDonorProject\'),0);" class="form1"></select>&nbsp;&raquo;&nbsp;</div>';
				$sProjectActivity = '<div id="divProjectActivity" style="display:none;"><select name="selProjectActivity" id="selProjectActivity" class="form1"></select></div>';
				*/
				//onchange="GetControlCode();"
				$sCategory = '<select  class="form1" name="selCategory" id="selCategory">
				<option value="0">Select Category Code</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCategory .= '<option ' . (($iChartOfAccountsCategoryId == $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsCategoryId")) ? 'selected="true"' : '')  . ' value="' . $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsCategoryId") . '">' . $objDatabase->Result($varResult, $i, "CAC.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAC.CategoryName") . '</option>';
				$sCategory .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="jsOpenWindow(\'../accounts/chartofaccounts_categorycodes.php?action2=' . $sAction2 . '\', 500, 500);"><img src="../images/icons/plus.gif" border="0" alt="Add New Category Code" title="Add New Category Code" /></a>';

				if ($iChartOfAccountsCategoryId == '') $iChartOfAccountsCategoryId = 1;
				//onChange="GetChartOfAccountCode();"
				$sControlCode = '<select class="form1" name="selControl" id="selControl" >
				<option value="0">Select Control Code</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CACT 				
				WHERE CACT.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId' AND CACT.ChartOfAccountsControlParentId = '0'");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{
					$iChartOfAccountsControlParentId = $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId");
					$sControlCode .= '<option value="' . $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId") . '">' . $objDatabase->Result($varResult, $i, "CACT.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CACT.ControlName") . '</option>';
					
					$sControlCode .= $this->GetChildControlCodes($iChartOfAccountsCategoryId, $iChartOfAccountsControlParentId, true);
				}
				$sControlCode .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="jsOpenWindow(\'../accounts/chartofaccounts_controlcodes.php?action2=' . $sAction2 . '&id=' . $iChartOfAccountsId . '&catid=\'+GetSelectedListBox(\'selCategory\'), \'700\', \'500\');"><img src="../images/icons/plus.gif" border="0" alt="Add New Control Code" title="Add New Control Code" /></a>';
				
				$sParentChartOfAccountTitle = '<select class="form1" name="selChartOfAccountParent" id="selChartOfAccountParent">
				<option value="0">No Parent</option>';				
				$sParentChartOfAccountTitle .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selChartOfAccountParent\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccountParent\'), \'../accounts/chartofaccounts.php?pagetype=details&id=\'+GetSelectedListBox(\'selChartOfAccountParent\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Parent Information" title="Chart of Account Parent Information" border="0" /></a>';
				
				/* Parent Chart of Acount Code 
					$varResult = $objDatabase->Query("
					SELECT * FROM fms_accounts_chartofaccounts AS CA2 
					INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA2.ChartOfAccountsControlId
					INNER JOIN fms_accounts_chartofaccounts_categories AS CAT ON CAT.ChartOfAccountsCategoryId = CA2.ChartOfAccountsCategoryId
					WHERE CA2.ChartOfAccountsId <> '$iChartOfAccountsId' 
					ORDER BY CAT.ChartOfAccountsCategoryId, CC.ChartOfAccountsControlId, CA2.ChartOfAccountsCode");
					if ($objDatabase->RowsNumber($varResult) > 0)
					{
						for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						{
							if (($i == 0) || ($iTempChartOfAccountsCategoryId != $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId")))
							{
								$iTempChartOfAccountsCategoryId = $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId");
								$sParentChartOfAccountTitle .= '<option style="color:blue;" value="0">' . $objDatabase->Result($varResult, $i, "CAT.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAT.CategoryName") . '</option>';
							}
							
							if (($i == 0) || ($iTempChartOfAccountsControlId != $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId")))
							{
								$iTempChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId");
								$sParentChartOfAccountTitle .= '<option style="color:green;" value="0">&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CC.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CC.ControlName") . '</option>';
							}
							
							$sParentChartOfAccountTitle .= '<option ' . (($iParentChartOfAccountsId == $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult, $i, "CA2.AccountTitle") . '</option>';
						}
					}
				*/
				
				
				$sAsOf = '<input size="10" type="text" id="txtAsOf" name="txtAsOf" class="form1" value="' . $dAsOf. '" />' . $objjQuery->Calendar('txtAsOf');
				
				$sChartOfAccountsCode = '<input type="text" name="txtChartOfAccountsCode" id="txtChartOfAccountsCode" class="form1" value="' . $iChartOfAccountsCode . '" size="20" />&nbsp;<span style="color:red;">*</span>';
				$sAccountTitle = '<input type="text" name="txtAccountTitle" id="txtAccountTitle" class="form1" value="' . $sAccountTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sChartOfAccountsAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				/*
				$sDonorRow = '<tr id="trDonors" style="display:visible;" bgcolor="#edeff1"><td valign="top">Donor:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sDonor  . '&nbsp;&raquo;&nbsp;</td><td>' . $sDonorProject . '</td><td>' . $sProjectActivity . '</td></tr></table></td></tr>';
				
				// Donor Projects
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.Status='1'");
				$iNoOfDonorProjects = $objDatabase->RowsNumber($varResult);
				$sDonorProjects = 'var aDonorProjects = MultiDimensionalArray(' . $iNoOfDonorProjects . ', 4);';
				for ($i=0; $i < $iNoOfDonorProjects; $i++)
				{
					$sDonorProjects .= 'aDonorProjects[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "DP.DonorId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '";';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '";';
				}
				
				// Project Activities
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.Status='1'");
				$iNoOfProjectActivities = $objDatabase->RowsNumber($varResult);
				$sProjectActivities = 'var aProjectActivities= MultiDimensionalArray(' . $iNoOfProjectActivities . ', 4);';
				for ($i=0; $i < $iNoOfProjectActivities; $i++)
				{
					$sProjectActivities .= 'aProjectActivities[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "PA.DonorProjectId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "PA.ActivityId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityCode") . '";';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityTitle") . '";';
				}
				*/
				
			}
			
			/*
			' . $sDonorProjects . '
			' . $sProjectActivities . '*/

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function GetChartOfAccountCode()
			{
				iControlId = GetSelectedListBox("selControl");
				SetVal("txtChartOfAccountsCode", "");
				OptionsList_RemoveAll("selChartOfAccountParent");
				
				if(iControlId > 0) xajax_AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountCode(GetSelectedListBox("selControl"), "selChartOfAccountParent");
			}
			function GetControlCode()
			{	
				SetVal("txtChartOfAccountsCode", "");
				OptionsList_RemoveAll("selChartOfAccountParent");
				FillSelectBox("selChartOfAccountParent", "No Parent", 0);
				xajax_AJAX_FMS_Accounts_ChartOfAccounts_GetControlCode(GetSelectedListBox("selCategory"), "selControl");				
			}
			</script>
			<script>
				jQuery(document).ready(function()
				{
					//GetControlCode-Start
					jQuery("select#selCategory").change(function()
					{	
						var iChartOfAccountsCategoryId = jQuery(this).val();
						var sAction2 = jQuery("#action").val();
						var sURL = "../../vf/accounts/chartofaccounts_details.php?action=GetControlCode&iChartOfAccountsCategoryId="+iChartOfAccountsCategoryId;
						
						if(iChartOfAccountsCategoryId > 0)
						{
							jQuery.ajax({
							  url: sURL,
							  cache: false,
							  type: "POST",
							  dataType:"json",
							  success: function(response)
							  {
							  	jQuery("select#selControl option").remove();
								jQuery.each(response, function(iIndex, aValue)
						  		{
							  		jQuery("select#selControl").append("<option value="+aValue[0]+">"+aValue[1]+"</option>");
						  		});
							  }
							});	
						}
					});
					//GetControlCode-End

					//GetChartOfAccountsCode-Start
					jQuery("select#selControl").change(function()
					{
						var iChartOfAccountsControlId = jQuery(this).val();
						var sAction2 = jQuery("#action").val();
						var sURL = "../../vf/accounts/chartofaccounts_details.php?action=GetChartOfAccountsCode&iChartOfAccountsControlId="+iChartOfAccountsControlId;
						
						if(iChartOfAccountsControlId > 0)
						{
							jQuery.ajax({
							  url: sURL,
							  cache: false,
							  type: "POST",
							  dataType:"json",
							  success: function(response)
							  {
							  	jQuery("input#txtChartOfAccountsCode").val(response.sLastCode);
							  	jQuery("select#selChartOfAccountParent option").remove();
								jQuery.each(response.aData, function(iIndex, aValue)
						  		{
							  		jQuery("select#selChartOfAccountParent").append("<option value="+aValue[0]+">"+aValue[1]+"</option>");
						  		});
							  }
							});	
						}
					});
					//GetChartOfAccountsCode-End
				});
			</script>				
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Chart Of Account Information:</span></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200px;">Category Code:</td><td><strong>' . $sCategory . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Control Code:</td><td><strong>' . $sControlCode . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Parent Chart of Account:</td><td><strong>' . $sParentChartOfAccountTitle . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Chart of Accounts Code:</td><td><strong>' . $sChartOfAccountsCode . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Account Title:</td><td><strong>' . $sAccountTitle . '</strong></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top">Status:</td><td><strong>' . $sChartOfAccountStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Added On:</td><td><strong>' . $sChartOfAccountsAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Added By:</td><td><strong>' . $sChartOfAccountsAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iChartOfAccountsId . '"><input type="hidden" name="action" id="action" value="UpdateChartOfAccount"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewChartOfAccount"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}
		
		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewChartOfAccount($iChartOfAccountsCategoryId, $iChartOfAccountsControlId, $iParentChartOfAccountsId, $iChartOfAccountsCode, $sAccountTitle, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[1] == 0)
			return(1010);

		if ($iParentChartOfAccountsId > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.ChartOfAccountsId='$iParentChartOfAccountsId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Request...');
			
			if ($objDatabase->Result($varResult, 0, "CA.ParentChartOfAccountsId") > 0)
				return(7010);
		}
		
		$varNow = $objGeneral->fnNow();
		$iChartOfAccountsAddedBy = $objEmployee->iEmployeeId;
		
		$sAccountTitle = $objDatabase->RealEscapeString($sAccountTitle);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		if($iStationId == "") $iStationId = 0;
		
		if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CA", "CA.ChartOfAccountsCode='$iChartOfAccountsCode'") > 0) return(7106);
				
		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_chartofaccounts
		(ChartOfAccountsCategoryId, ChartOfAccountsControlId, ParentChartOfAccountsId, ChartOfAccountsCode, AccountTitle, Status, Notes, ChartOfAccountsAddedOn, ChartOfAccountsAddedBy)
		VALUES ('$iChartOfAccountsCategoryId', '$iChartOfAccountsControlId', '$iParentChartOfAccountsId', '$iChartOfAccountsCode', '$sAccountTitle', '$iStatus', '$sNotes', '$varNow', '$iChartOfAccountsAddedBy')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.AccountTitle='$sAccountTitle' AND CA.ChartOfAccountsAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$objSystemLog->AddLog("Add New Chart Of Account - " . $sAccountTitle);
				
				$objGeneral->fnRedirect('?pagetype=details&error=7000&id=' . $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsId"). '&catid=' . $iChartOfAccountsCategoryId);
			}
		}

		return(7001);
	}
	
	function UpdateChartOfAccount($iChartOfAccountsId, $iChartOfAccountsCategoryId, $iChartOfAccountsControlId, $iParentChartOfAccountsId, $iChartOfAccountsCode, $sAccountTitle, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[2] == 0)
			return(1010);			
		
		$sAccountTitle = $objDatabase->RealEscapeString($sAccountTitle);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CA", "CA.ChartOfAccountsId <> '$iChartOfAccountsId' AND CA.ChartOfAccountsCode='$iChartOfAccountsCode'") > 0) return(7106);
		
		if($iStationId == "") $iStationId = 0;
		//if($iSourceOfIncomeId == "") $iSourceOfIncomeId = 0;
		//if($iDonorId == "") $iDonorId = 0;
		//if($iDonorProjectId == "") $iDonorProjectId = 0;
		//if($iActivityId == "") $iActivityId = 0;
		
		// Check Chart of Account Code must lie under Control Codes Range
		/*
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CAC WHERE CAC.ChartOfAccountsControlId = '$iChartOfAccountsControlId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$iStartRange = $objDatabase->Result($varResult, 0, "CAC.StartRange");
			$iEndRange = $objDatabase->Result($varResult, 0, "CAC.EndRange");
			
			if( ($iChartOfAccountsCode < $iStartRange ) || ($iChartOfAccountsCode > $iEndRange) )
			return(7009);
		}
		*/
				
		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_chartofaccounts
		SET	
			ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId',
			ChartOfAccountsControlId='$iChartOfAccountsControlId',
			ParentChartOfAccountsId='$iParentChartOfAccountsId',			
			ChartOfAccountsCode = '$iChartOfAccountsCode',			
			AccountTitle='$sAccountTitle',			
			Status='$iStatus',
			Notes='$sNotes'
		WHERE ChartOfAccountsId='$iChartOfAccountsId'");

		if ($varResult)
		{			
			$objSystemLog->AddLog("Update Chart Of Account - " . $sAccountTitle);			
			$objGeneral->fnRedirect('?pagetype=details&error=7002&id=' . $iChartOfAccountsId);
		}
		else
			return(7003);
	}
	
	function DeleteChartOfAccount($iChartOfAccountsId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[3] == 0)
			return(1010);
		// Check Chart of Account usage in Transactions & Parent and Allocation
		if ($objDatabase->DBCount("fms_accounts_generaljournal_entries AS GJE INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId", "GJE.ChartOfAccountsId= '$iChartOfAccountsId' AND GJ.IsDeleted ='0'") > 0) return(7006);
		if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CA2", "CA2.ParentChartOfAccountsId= '$iChartOfAccountsId'") > 0) return(7007);
		if ($objDatabase->DBCount("fms_banking_bankaccounts AS BA", "BA.ChartOfAccountsId= '$iChartOfAccountsId'") > 0) return(7008);
		if ($objDatabase->DBCount("fms_accounts_closefinancialyear_chartofaccounts AS CFAC", "CFAC.ChartOfAccountsId= '$iChartOfAccountsId'") > 0) return(7006);			
		if ($objDatabase->DBCount("fms_accounts_budgetallocation AS BA", "BA.ChartOfAccountId= '$iChartOfAccountsId'") > 0) return(7011);
			
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.ChartOfAccountsId='$iChartOfAccountsId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(7005);

		$sAccountTitle = $objDatabase->Result($varResult, 0, "CA.AccountTitle");
		$sAccountTitle = $objDatabase->RealEscapeString($sAccountTitle);		
		
		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_chartofaccounts WHERE ChartOfAccountsId='$iChartOfAccountsId'");
		if ($varResult)
		{			
			$objSystemLog->AddLog("Delete Chart Of Account - " . $sAccountTitle);
			return(7004);
		}
		else
			return(7005);
	}
	
	function GetChartOfAccountsIdByTransactionCode($sChartOfAccountsTransactionCode)
    {
    	global $objDatabase;

    	$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.TransactionCode='$sChartOfAccountsTransactionCode'");
    	if ($objDatabase->RowsNumber($varResult) > 0)
    		return($objDatabase->Result($varResult, 0, "CA.ChartOfAccountsId"));
    	else
    		return(0);
    }

    function ShowChartOfAccountsCategoryCodes($iChartOfAccountsId, $iChartOfAccountsCategoryId, $sAction2)
    {
    	global $objGeneral;
    	global $objDatabase;

		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

    	$sReturn = '<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Chart of Account Categories</span></td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">

		<table border="1" style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="Details_Title_TR">
		 <td width="30%" align="left"><span class="WhiteHeading">Category Code</span></td>
		 <td align="left"><span class="WhiteHeading">Category Name</span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Start Range</span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">End Range</span></td>
		 <td width="5%" colspan="5"><span class="WhiteHeading">Operations</span></td>
		</tr>';

    	$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC");
    	for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{
    		$iCategoryId = $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsCategoryId");
    		$sCategoryCode = $objDatabase->Result($varResult, $i, "CAC.CategoryCode");
    		$sCategoryName = $objDatabase->Result($varResult, $i, "CAC.CategoryName");
			$iStartRange = $objDatabase->Result($varResult, $i, "CAC.StartRange");
			$iEndRange = $objDatabase->Result($varResult, $i, "CAC.EndRange");
			$sStartRange = number_format($iStartRange, 0);
			$sEndRange = number_format($iEndRange, 0);
			
			$sEditCategory = '<td align="center"><a href="../accounts/chartofaccounts_categorycodes.php?action2=edit&id=' . $iChartOfAccountsId . '&categoryid='. $iCategoryId . '"><img src="../images/icons/iconEdit.gif" alt="Edit this Category Code" title="Edit this Category Code" border="0" /></a></td>';
    		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[2] == 0) // Update Disabled
				$sEditCategory = '';
			
			if (($sAction2 == "edit") && ($iChartOfAccountsCategoryId == $iCategoryId))
			{				
				$sReturn .= '<form method="post" action="../accounts/chartofaccounts_categorycodes.php?id=' . $iChartOfAccountsId. '" onsubmit="return ValidateForm();">
				<tr>					 
				 <td>' . $sCategoryCode . '</td>
    			 <td>' . $sCategoryName . '</td>
				 <td><input type="text" name="txtStartRange" id="txtStartRange" class="form1" value="' . $iStartRange . '" /></td>
				 <td><input type="text" name="txtEndRange" id="txtEndRange" class="form1" value="' . $iEndRange . '" /></td>				 
				 <td align="center" colspan="2"><input type="submit" class="AdminFormButton1" value="Update" /></td>
				</tr>
				<input type="hidden" name="id" id="id" value="' . $iChartOfAccountsId . '" />
				<input type="hidden" name="categoryid" id="categoryid" value="' . $iCategoryId . '" />
				<input type="hidden" name="action" id="action" value="UpdateCategory" />
				</form>';				
			}
			else
			{
					
				$sReturn .= '<tr>
    			 <td>' . $sCategoryCode . '</td>
    			 <td>' . $sCategoryName . '</td>
				 <td>' . $sStartRange . '&nbsp;</td>
				 <td>' . $sEndRange . '&nbsp;</td>
				 ' . $sEditCategory .'    		 
    			</tr>';
			}
    	}

    	$sReturn .= '    	     	 
    	</table>
		<script language="JavaScript" type="text/javascript">
		function ValidateForm()
		{	
			if (!isNumeric(GetVal("txtStartRange"))) return(AlertFocus(\'Please enter a Valid Start Range\', \'txtStartRange\'));
			if (!isNumeric(GetVal("txtEndRange"))) return(AlertFocus(\'Please enter a Valid End Range\', \'txtEndRange\'));
						
			return(true);
		}
		</script>';
		//CloseWindow3(\'id=' . $iChartOfAccountsId . '&action2=' . $sAction2 . '\');
    	$sReturn .= '</td></tr></table></td></tr></table>
    	<br />
    	<div align="center"><input type="button" class="formbutton" value="Close Window" onclick="window.close();" /></div>';

    	return($sReturn);

    }

    function AddNewCategory($sCategoryCode, $sCategoryName, $iSpecifiedStartRange, $iSpecifiedEndRange)
    {
    	global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[1] == 0) // Add Disabled
			return(1010);
			
    	$sCategoryName = $objDatabase->RealEscapeString($sCategoryName);
		$sCategoryCode = $objDatabase->RealEscapeString($sCategoryCode);
		if( ($iSpecifiedStartRange == $iSpecifiedEndRange) || ($iSpecifiedStartRange > $iSpecifiedEndRange))
			return(3405);
			
		// Check for Start & End Range
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC WHERE 1=1");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{    		
			$iStartRange = $objDatabase->Result($varResult, $i, "CAC.StartRange");
			$iEndRange = $objDatabase->Result($varResult, $i, "CAC.EndRange");
			
			if($iSpecifiedStartRange == $iStartRange && $iSpecifiedEndRange == $iEndRange)
				return(3405);
			if( ($iSpecifiedStartRange >= $iStartRange && $iSpecifiedEndRange <= $iEndRange) || ($iSpecifiedStartRange >= $iStartRange && $iSpecifiedStartRange <= $iEndRange) || ($iSpecifiedEndRange >= $iStartRange && $iSpecifiedEndRange <= $iEndRange) || ($iSpecifiedStartRange <= $iStartRange && $iSpecifiedEndRange >= $iEndRange) )
				return(3405);
		}
		
    	$varResult = $objDatabase->Query("INSERT INTO fms_accounts_chartofaccounts_categories (CategoryCode, CategoryName, StartRange, EndRange) VALUES ('$sCategoryCode', '$sCategoryName', '$iSpecifiedStartRange', '$iSpecifiedEndRange')");
    	if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Add New Chart of Accounts Category - " . $sCategoryName);
			
			return(3403);
		}
    	else
    		return(3404);
    }

    function DeleteCategory($iChartOfAccountsCategoryId)
    {
    	global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[3] == 0) // Delete Disabled
			return(1010);


    	if ($objDatabase->DBCount("fms_accounts_chartofaccounts_controls AS CACT", "CACT.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId'") > 0)
    		return(3400);
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC WHERE CAC.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Invalid Chart of Accounts Category Id');
        $sCategoryName = $objDatabase->Result($varResult, 0, "CAC.CategoryName");
		
    	$varResult = $objDatabase->Query("DELETE FROM fms_accounts_chartofaccounts_categories WHERE ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId'");
    	if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Delete Chart of Accounts Category - " . $sCategoryName);
			
			return(3401);
		}
    	else
    		return(3402);
    }
	
	function UpdateCategory($iChartOfAccountsCategoryId, $iSpecifiedStartRange, $iSpecifiedEndRange)
    {
    	global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[2] == 0) // Update Disabled
			return(1010);			
    	
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC WHERE CAC.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Invalid Chart of Accounts Category Id');
        $sCategoryName = $objDatabase->Result($varResult, 0, "CAC.CategoryName");
		
		
		if( ($iSpecifiedStartRange == $iSpecifiedEndRange) || ($iSpecifiedStartRange > $iSpecifiedEndRange))
			return(3408);
			
		// Check for Start & End Range
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC WHERE CAC.ChartOfAccountsCategoryId <> '$iChartOfAccountsCategoryId'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{    		
			$iStartRange = $objDatabase->Result($varResult, $i, "CAC.StartRange");
			$iEndRange = $objDatabase->Result($varResult, $i, "CAC.EndRange");
			
			if($iSpecifiedStartRange == $iStartRange && $iSpecifiedEndRange == $iEndRange)
				return(3408);
			if( ($iSpecifiedStartRange >= $iStartRange && $iSpecifiedEndRange <= $iEndRange) || ($iSpecifiedStartRange >= $iStartRange && $iSpecifiedStartRange <= $iEndRange) || ($iSpecifiedEndRange >= $iStartRange && $iSpecifiedEndRange <= $iEndRange) || ($iSpecifiedStartRange <= $iStartRange && $iSpecifiedEndRange >= $iEndRange) )
				return(3408);
		}
		
    	$varResult = $objDatabase->Query("
		UPDATE fms_accounts_chartofaccounts_categories
		SET
			StartRange='$iSpecifiedStartRange',
			EndRange='$iSpecifiedEndRange'
		WHERE ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId'");
    	if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Update Category - " . $sCategoryName);
			
			return(3406);
		}
    	else
    		return(3407);
    }
	
    function ShowChartOfAccountsControlCodes($iChartOfAccountsId, $iChartOfAccountCategoryId, $iChartOfAccountsControlId, $sAction2, $bAllowOperations = true)
    {
    	global $objGeneral;
    	global $objDatabase;
		global $objEmployee;
				
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[0] == 0) // View Disabled
		{		
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
			$bAllowOperations = false;
		}

    	$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC WHERE CAC.ChartOfAccountsCategoryId='$iChartOfAccountCategoryId'");
    	if ($objDatabase->RowsNumber($varResult) <= 0) die('Invalid Chart of Accounts Category');

    	$sCategoryCode = $objDatabase->Result($varResult, 0, "CAC.CategoryCode");
    	$sCategoryName = $objDatabase->Result($varResult, 0, "CAC.CategoryName");
		$iStartRange = $objDatabase->Result($varResult, 0, "CAC.StartRange");
		$iEndRange = $objDatabase->Result($varResult, 0, "CAC.EndRange");
		$sStartRange = number_format($iStartRange, 2);
		$sEndRange = number_format($iEndRange, 2);

    	$sReturn = '<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Control Codes for ' . $sCategoryCode . ' - ' . $sCategoryName . ' ( Range ' . $sStartRange  . ' - ' . $sEndRange . ')</span></td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">

		<table border="1" style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="Details_Title_TR">
		 <td width="30%" align="left"><span class="WhiteHeading">Control Code</span></td>
		 <td align="left"><span class="WhiteHeading">Control Name</span></td>
		 <td align="left"><span class="WhiteHeading">Parent Control</span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Start Range</span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">End Range</span></td>
		 ' . (($bAllowOperations) ? '<td width="5%" colspan="5"><span class="WhiteHeading">Operations</span></td>' : '') . '
		</tr>
		';

    	$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CACT
		LEFT JOIN fms_accounts_chartofaccounts_controls AS PCACT ON PCACT.ChartOfAccountsControlId = CACT.ChartOfAccountsControlParentId
		WHERE CACT.ChartOfAccountsCategoryId='$iChartOfAccountCategoryId'");
    	for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{
    		$iControlId = $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId");
    		$sControlCode = $objDatabase->Result($varResult, $i, "CACT.ControlCode");
    		$sControlName = $objDatabase->Result($varResult, $i, "CACT.ControlName");
			$iParentControlId = $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlParentId");
			$sParentControlName = $objDatabase->Result($varResult, $i, "PCACT.ControlName");
			$iStartRange = $objDatabase->Result($varResult, $i, "CACT.StartRange");
			$iEndRange = $objDatabase->Result($varResult, $i, "CACT.EndRange");
			$sStartRange = number_format($iStartRange, 0);
			$sEndRange = number_format($iEndRange, 0);
			
			if (($sAction2 == "edit") && ($iChartOfAccountsControlId == $iControlId))
			{
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CACT 
				WHERE CACT.ChartOfAccountsCategoryId='$iChartOfAccountCategoryId' AND CACT.ChartOfAccountsControlParentId = '0' AND ChartOfAccountsControlId <> '$iControlId'
				ORDER BY CACT.ControlName");			
				$sParentControlName = '<select class="form1" name="selParentControl" id="selParentControl">
				<option value="0">No Parent</option>';
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sParentControlName .= '<option ' . (($iParentControlId == $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId") . '">' . $objDatabase->Result($varResult, $i, "CACT.ControlName") . '</option>';
				$sParentControlName .= '</select>';
				
				$sReturn .= '<form method="post" action="../accounts/chartofaccounts_controlcodes.php?id=' . $iChartOfAccountsId. '&catid='. $iChartOfAccountCategoryId . '" onsubmit="return ValidateForm();">
				<tr>
				 <td width="30%"><input type="text" name="txtControlCode" id="txtControlCode" size="10" class="form1" value="' . $sControlCode . '" /></td>
    			 <td><input type="text" size="30" name="txtControlName" id="txtControlName" class="form1" value="'. $sControlName . '" /></td>
				 <td>'. $sParentControlName . '</td>
				 <td><input type="text" name="txtStartRange" id="txtStartRange" class="form1" value="' . $iStartRange . '" /></td>
				 <td><input type="text" name="txtEndRange" id="txtEndRange" class="form1" value="' . $iEndRange . '" /></td>
				 <td align="center" colspan="2"><input type="submit" class="AdminFormButton1" value="Update" /></td>
				</tr>
				<input type="hidden" name="id" id="id" value="' . $iChartOfAccountsId . '" />
				<input type="hidden" name="catid" id="catid" value="' . $iCategoryId . '" />
				<input type="hidden" name="controlid" id="controlid" value="' . $iChartOfAccountsControlId . '" /> 
				<input type="hidden" name="action" id="action" value="UpdateControl" />
				</form>
				<script language="JavaScript" type="text/javascript">
				function ValidateForm()
				{
					if (GetVal(\'txtControlCode\') == "") return(AlertFocus(\'Please enter a valid Control Code\', \'txtControlCode\'));
					if (GetVal(\'txtControlName\') == "") return(AlertFocus(\'Please enter a Control Name\', \'txtControlName\'));
					if (!isNumeric(GetVal("txtStartRange"))) return(AlertFocus(\'Please enter a Valid Start Range\', \'txtStartRange\'));
					if (!isNumeric(GetVal("txtEndRange"))) return(AlertFocus(\'Please enter a Valid End Range\', \'txtEndRange\'));
								
					return(true);
				}
				</script>';				
			}
			
			else
			{	
				$sEditControl = '<td align="center"><a href="../accounts/chartofaccounts_controlcodes.php?action2=edit&id=' . $iChartOfAccountsId . '&catid='. $iChartOfAccountCategoryId . '&controlid='. $iControlId . '"><img src="../images/icons/iconEdit.gif" alt="Edit this Category Code" title="Edit this Category Code" border="0" /></a></td>';
    			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[2] == 0) // Update Disabled
					$sEditControl = '';
			
				$sDeleteControl = '<td align="center"><a href="../accounts/chartofaccounts_controlcodes.php?action=DeleteControl&id=' . $iChartOfAccountsId . '&catid=' . $iChartOfAccountCategoryId . '&controlid=' . $iControlId . '&action2=' . $sAction2 . '"><img src="../images/icons/iconDelete.gif" alt="Delete this Control Code" title="Delete this Control Code" border="0" /></a></td>';
				
    			$sReturn .= '<tr>
    			 <td>' . $sControlCode . '</td>
    			 <td>' . $sControlName . '</td>
				 <td>' . $sParentControlName . '</td>
				 <td>' . $sStartRange . '</td>
				 <td>' . $sEndRange . '</td>
				 ' . (($bAllowOperations) ? '
				 ' . $sEditControl . '
				 ' . $sDeleteControl : '') . '
    			</tr>';
			}

    	}
		if ($bAllowOperations)
		{
			$iParentControlId = 0;
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CACT WHERE CACT.ChartOfAccountsCategoryId='$iChartOfAccountCategoryId' AND CACT.ChartOfAccountsControlParentId = '0' ORDER BY CACT.ControlName");			
			$sParentControlName = '<select class="form1" name="selParentControl" id="selParentControl">
			<option value="0">No Parent</option>';
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
				$sParentControlName .= '<option ' . (($iParentControlId == $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId") . '">' . $objDatabase->Result($varResult, $i, "CACT.ControlName") . '</option>';
			$sParentControlName .= '</select>';
			
    		$sReturn .= '
    		 <form method="post" action="../accounts/chartofaccounts_controlcodes.php" onsubmit="return ValidateForm();">
    		 <tr><td align="center" colspan="3"><br />Add New Control</td></tr>
    		 <tr>
    		  <td width="30%"><input type="text" name="txtControlCode" id="txtControlCode" size="10" class="form1" value="" /></td>
    		  <td><input type="text" size="30" name="txtControlName" id="txtControlName" class="form1" value="" /></td>
			  <td>'. $sParentControlName . '</td>
			  <td><input type="text" name="txtStartRange" id="txtStartRange" size="10" class="form1" value="" /></td>
			  <td><input type="text" name="txtEndRange" id="txtEndRange" size="10" class="form1" value="" /></td>
    		  <td width="5%"><input type="submit" class="AdminFormButton1" value="Add" /></td>
    		 </tr>
    		</table>
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if (GetVal(\'txtControlCode\') == "") return(AlertFocus(\'Please enter a valid Control Code\', \'txtControlCode\'));
					if (GetVal(\'txtControlName\') == "") return(AlertFocus(\'Please enter a Control Name\', \'txtControlName\'));
				if (!isNumeric(GetVal("txtStartRange"))) return(AlertFocus(\'Please enter a Valid Start Range\', \'txtStartRange\'));
				if (!isNumeric(GetVal("txtEndRange"))) return(AlertFocus(\'Please enter a Valid End Range\', \'txtEndRange\'));
							
				return(true);
			}
			</script>
    		<input type="hidden" name="id" id="id" value="' . $iChartOfAccountsId . '" />
    		<input type="hidden" name="catid" id="catid" value="' . $iChartOfAccountCategoryId . '" />
    		<input type="hidden" name="action" id="action" value="AddNewControl" />
    		<input type="hidden" name="action2" id="action2" value="' . $sAction2 . '" />
    		</form>';
		}
		//CloseWindow3(\'action2=' . $sAction2 . '&id=' . $iChartOfAccountsId . '&catid=' . $iChartOfAccountCategoryId . '\');
    	$sReturn .= '</td></tr></table></td></tr></table>
    	<br />
    	<div align="center"><input type="button" class="formbutton" value="Close Window" onclick="Window.close();" /></div>';

    	return($sReturn);

    }

    function AddNewControl($iChartOfAccountsCategoryId, $sControlCode, $sControlName, $iChartOfAccountsControlParentId, $iSpecifiedStartRange, $iSpecifiedEndRange)
    {
    	global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[1] == 0) // Add Disabled
			return(1010);

    	$sControlName = $objDatabase->RealEscapeString($sControlName);
		$sControlCode = $objDatabase->RealEscapeString($sControlCode);
		
		if( ($iSpecifiedStartRange == $iSpecifiedEndRange) || ($iSpecifiedStartRange > $iSpecifiedEndRange))
			return(3415);
		// Check for Start & End Range
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC WHERE CAC.ChartOfAccountsCategoryId = '$iChartOfAccountsCategoryId'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{    		
			$iStartRange = $objDatabase->Result($varResult, $i, "CAC.StartRange");
			$iEndRange = $objDatabase->Result($varResult, $i, "CAC.EndRange");			
				
			if( ($iSpecifiedStartRange < $iStartRange) || ($iSpecifiedStartRange > $iEndRange) || ($iSpecifiedEndRange < $iSpecifiedStartRange) || ($iSpecifiedEndRange > $iEndRange) )
				return(3415);
			
		}
		
		/* commented due to Parent & child relationship 
		// Check for Start & End Range in Controls
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CAC WHERE CAC.ChartOfAccountsCategoryId = '$iChartOfAccountsCategoryId'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{    		
			$iStartRange = $objDatabase->Result($varResult, $i, "CAC.StartRange");
			$iEndRange = $objDatabase->Result($varResult, $i, "CAC.EndRange");
			
			if($iSpecifiedStartRange == $iStartRange && $iSpecifiedEndRange == $iEndRange)
				return(3415);
				
			if( ($iSpecifiedStartRange >= $iStartRange && $iSpecifiedEndRange <= $iEndRange) || ($iSpecifiedStartRange >= $iStartRange && $iSpecifiedStartRange <= $iEndRange) || ($iSpecifiedEndRange >= $iStartRange && $iSpecifiedEndRange <= $iEndRange) || ($iSpecifiedStartRange <= $iStartRange && $iSpecifiedEndRange >= $iEndRange) )
				return(3415);
		}
		*/
		

    	$varResult = $objDatabase->Query("INSERT INTO fms_accounts_chartofaccounts_controls 
		(ChartOfAccountsCategoryId, ChartOfAccountsControlParentId, ControlCode, ControlName, StartRange, EndRange) 
		VALUES ('$iChartOfAccountsCategoryId', '$iChartOfAccountsControlParentId', '$sControlCode', '$sControlName', '$iSpecifiedStartRange', '$iSpecifiedEndRange')");
    	if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Add New Chart of Accounts Control Code - " . $sControlName);
			
			return(3413);
		}
    	else
    		return(3414);
    }
	
	function UpdateControl($iChartOfAccountsCategoryId, $iChartOfAccountsControlId, $sControlCode, $sControlName, $iChartOfAccountsControlParentId, $iSpecifiedStartRange, $iSpecifiedEndRange)
    {
    	global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[2] == 0) // Add Disabled
			return(1010);

    	$sControlName = $objDatabase->RealEscapeString($sControlName);
		$sControlCode = $objDatabase->RealEscapeString($sControlCode);
		
		if( ($iSpecifiedStartRange == $iSpecifiedEndRange) || ($iSpecifiedStartRange > $iSpecifiedEndRange))
			return(3418);
			
		// Check for Start & End Range in Categories
		//print('Speciefied Start Range' . $iSpecifiedStartRange . '<br />Speciefied End Range' . $iSpecifiedEndRange);
		//die();
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC WHERE CAC.ChartOfAccountsCategoryId = '$iChartOfAccountsCategoryId'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{    		
			$iStartRange = $objDatabase->Result($varResult, $i, "CAC.StartRange");
			$iEndRange = $objDatabase->Result($varResult, $i, "CAC.EndRange");			
				
			if( ($iSpecifiedStartRange < $iStartRange) || ($iSpecifiedStartRange > $iEndRange) || ($iSpecifiedEndRange < $iSpecifiedStartRange) || ($iSpecifiedEndRange > $iEndRange) )
				return(3418);
			
		}
		
		/* commented due to parent and child relationship
		// Check for Start & End Range in Controls
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CAC WHERE CAC.ChartOfAccountsCategoryId = '$iChartOfAccountsCategoryId' AND CAC.ChartOfAccountsControlId <> '$iChartOfAccountsControlId'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    	{    		
			$iStartRange = $objDatabase->Result($varResult, $i, "CAC.StartRange");
			$iEndRange = $objDatabase->Result($varResult, $i, "CAC.EndRange");
			
			if($iSpecifiedStartRange == $iStartRange && $iSpecifiedEndRange == $iEndRange)
				return(3418);
				
			if( ($iSpecifiedStartRange > $iStartRange && $iSpecifiedStartRange < $iEndRange) || ($iSpecifiedEndRange > $iStartRange && $iSpecifiedEndRange < $iEndRange) )	
				return(3418);
		}
		*/
				
    	$varResult = $objDatabase->Query("
		UPDATE fms_accounts_chartofaccounts_controls 
		SET
			ChartOfAccountsControlParentId='$iChartOfAccountsControlParentId',
			ControlCode='$sControlCode',
			ControlName='$sControlName',
			StartRange='$iSpecifiedStartRange',
			EndRange='$iSpecifiedEndRange'
		WHERE ChartOfAccountsControlId = '$iChartOfAccountsControlId'");
    	if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Update Control Code - " . $sControlName);
			
			return(3416);
		}
    	else
    		return(3417);
    }
	
    function DeleteControl($iChartOfAccountsCategoryId, $iChartOfAccountsControlId)
    {
    	global $objDatabase;
    	global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_ChartOfAccounts[3] == 0) // Delete Disabled
			return(1010);

    	if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CA", "CA.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId' AND CA.ChartOfAccountsControlId='$iChartOfAccountsControlId'") > 0)
    		return(7010);
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CAC WHERE CAC.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId' AND CAC.ChartOfAccountsControlId='$iChartOfAccountsControlId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Invalid Chart of Accounts Category Id');
        $sControlName = $objDatabase->Result($varResult, 0, "CAC.ControlName");
		
    	$varResult = $objDatabase->Query("DELETE FROM fms_accounts_chartofaccounts_controls WHERE ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId' AND ChartOfAccountsControlId='$iChartOfAccountsControlId'");
    	if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Delete Chart of Accounts Control Code - " . $sControlName);
			
			return(3411);
		}
    	else
    		return(3412);
    }
	
	function ChartOfAccountsList()
	{
		global $objDatabase;
		global $objEmployee;
		$iEmployeeId = $objEmployee->iEmployeeId;
		$sProjectCondition = "";
		
		$aChartOfAccounts = array();
		
		$sEmployeesChartOfAccountsTable = "";
		$sEmployeeRestriction = "";
		
		// Check Employees roles on Chart Of Accounts - If has access on all Chart Of Accounts then show all chart of Accounts
		if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeChartOfAccountAccess == 0) // Access on Chart Of Account
		{
			$sEmployeesChartOfAccountsTable = "INNER JOIN organization_employees_chartofaccounts AS ECAC ON ECAC.ChartOfAccountsId = CA.ChartOfAccountsId";
			$sEmployeeRestriction = " AND ECAC.EmployeeId='$objEmployee->iEmployeeId'";
		}
		
		$varResult = $objDatabase->Query("
		SELECT * 
		FROM fms_accounts_chartofaccounts AS CA		
		$sEmployeesChartOfAccountsTable
		$sEmployeeRestriction
		GROUP BY CA.ChartOfAccountsId");

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode");
			$sAccountTitle = $objDatabase->Result($varResult, $i, "CA.AccountTitle");
			$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsId");

			$aChartOfAccounts[$i][0] = $iChartOfAccountsId;
			$aChartOfAccounts[$i][1] = $sChartOfAccountsCode . ' - ' . $sAccountTitle;
			$aChartOfAccounts[$i][2] = $sChartOfAccountsCode;
		}
		
		return($aChartOfAccounts);
	}
	
	// Ajax Functions		
	function AJAX_GetChartOfAccountsControls($sChartOfAccountsCategoryCode)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		$varResult = $objDatabase->Query("SELECT 
		 CC.ChartOfAccountsControlId AS 'ChartOfAccountsControlId',
		 CC.ControlName AS 'ControlName',		 
		 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_chartofaccounts_categories AS CCAT
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
		LEFT JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		WHERE CCAT.CategoryCode='$sChartOfAccountsCategoryCode' AND CC.ChartOfAccountsControlParentId = '0'  AND GJ.IsDeleted ='0'
		GROUP BY CC.ChartOfAccountsControlId");
		
		$sCode = '<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">';
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "ChartOfAccountsControlId");
			$sCode .= '<tr><td style="width:18px;"></td><td><a href="#noanchor" onclick="OpenChartOfAccountsControls_Children(' . $iChartOfAccountsControlId . ');"><img id="imgChartOfAccountsControls' . $iChartOfAccountsControlId . '" src="../images/icons/plus.gif" align="absmiddle" border="0" alt="Click to show more" title="Click to show more" />&nbsp;' . $objDatabase->Result($varResult, $i, "ControlName") . '</a></td><td align="right">' . number_format($objDatabase->Result($varResult, $i, "Balance")) . '</td></tr><tr><td style="height:8px;" colspan="3"></td></tr><tr style="display:none;" id="trChartOfAccounts' . $iChartOfAccountsControlId . '"><td colspan="3"><div id="divChartOfAccounts' . $iChartOfAccountsControlId . '"></div></td></tr>';
		}
		$sCode .= '</table>';
		
		$objResponse->addScript('document.getElementById("divChartOfAccountsControls' . $sChartOfAccountsCategoryCode . '").innerHTML = \'' . $sCode . '\';');

		return($objResponse->getXML());
	}

	
	function AJAX_GetChartOfAccounts($iChartOfAccountsControlId)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		$varResult = $objDatabase->Query("SELECT 
		 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
		 CA.AccountTitle AS 'AccountTitle',
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_chartofaccounts_categories AS CCAT
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		WHERE CA.ChartOfACcountsControlId='$iChartOfAccountsControlId' 
		AND (CA.ParentChartOfAccountsId = '0' OR CA.ParentChartOfAccountsId = '-1') AND GJ.IsDeleted ='0'
		GROUP BY CA.ChartOfAccountsId");
		
		$sCode = '<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">';
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$dBalance = $objDatabase->Result($varResult, $i, "Balance");
			$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
			$sChartOfAccountsTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
			$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");
			
			$dChildBalance = 0;			
			
			$varResult2 = $objDatabase->Query("
			SELECT 
			 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
			 CA.AccountTitle AS 'AccountTitle',
			 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
			 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
			FROM fms_accounts_chartofaccounts_categories AS CCAT
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
			INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
			LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
			INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
			WHERE CA.ParentChartOfAccountsId='$iChartOfAccountsId'  AND GJ.IsDeleted ='0'
			GROUP BY CA.ChartOfAccountsId");
			if ($objDatabase->RowsNumber($varResult2) > 0)
			{
				for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
				{
					$dChildBalance += $objDatabase->Result($varResult2, $j, "Balance");
				}

				$sChartOfAccounts = '<tr><td style="width:70px;"></td><td><a href="#noanchor" onclick="OpenChartOfAccountsChildren(' . $iChartOfAccountsId . ', true);"><img id="imgChartOfAccountsParents' . $iChartOfAccountsId . '" src="../images/icons/plus.gif" align="absmiddle" border="0" alt="Click to show more" title="Click to show more" />&nbsp;' . $sChartOfAccountsCode . ' - ' . $sChartOfAccountsTitle . '</a></td><td align="right">' . number_format($dBalance + $dChildBalance) . '</td></tr><tr><td style="height:8px;" colspan="3"></td></tr><tr style="display:none;" id="trChartOfAccountsChildren' . $iChartOfAccountsId . '"><td colspan="3"><div id="divChartOfAccountsChildren' . $iChartOfAccountsId . '"></div></td></tr>';
			}
			else
				$sChartOfAccounts = '<tr><td style="width:70px;"></td><td>' . $sChartOfAccountsCode . ' - ' . $sChartOfAccountsTitle . '</td><td align="right">' . number_format($dBalance) . '</td></tr><tr><td style="height:8px;" colspan="3"></td></tr>';
			
			$sCode .= $sChartOfAccounts;
		}
		$sCode .= '</table>';
		
		$objResponse->addScript('document.getElementById("divChartOfAccounts' . $iChartOfAccountsControlId . '").innerHTML = \'' . $sCode . '\';');	

		return($objResponse->getXML());		
	}
	
	function AJAX_GetChartOfAccountsControls_Children($iChartOfAccountsControlParentId)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		// Show Child controls first
		$varResult = $objDatabase->Query("SELECT 
			CC.ChartOfAccountsControlId AS 'ChartOfAccountsControlId',
			CC.ControlName AS 'ControlName',		 
			IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'		
		FROM fms_accounts_chartofaccounts_categories AS CCAT
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		WHERE CC.ChartOfAccountsControlParentId='$iChartOfAccountsControlParentId'  AND GJ.IsDeleted ='0'
		GROUP BY CC.ChartOfAccountsControlId");
		
		$sCode = '<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">';
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "ChartOfAccountsControlId");
			$dBalance = $objDatabase->Result($varResult, $i, "Balance");
			$sControlName = $objDatabase->Result($varResult, $i, "ControlName");			
			
			$sCode .= '<tr><td style="width:40px;"></td><td><a href="#noanchor" onclick="OpenChartOfAccountsControls(' . $iChartOfAccountsControlId . ');"><img id="imgChartOfAccountsControls' . $iChartOfAccountsControlId . '" src="../images/icons/plus.gif" align="absmiddle" border="0" alt="Click to show more" title="Click to show more" />&nbsp;' . $sControlName . '</a></td><td align="right">' . number_format($dBalance) . '</td></tr><tr><td style="height:8px;" colspan="3"></td></tr><tr style="display:none;" id="trChartOfAccounts' . $iChartOfAccountsControlId . '"><td colspan="3"><div id="divChartOfAccounts' . $iChartOfAccountsControlId . '"></div></td></tr>';			
		}
		$sCode .= '</table>';
		/* End of Child */
		
		/* Show Chart Of Accounts */		
		$varResult = $objDatabase->Query("SELECT 
		 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
		 CA.AccountTitle AS 'AccountTitle',
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_chartofaccounts_categories AS CCAT
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		WHERE CA.ChartOfACcountsControlId='$iChartOfAccountsControlParentId' AND (CA.ParentChartOfAccountsId = '0' OR CA.ParentChartOfAccountsId = '-1'  AND GJ.IsDeleted ='0')
		GROUP BY CA.ChartOfAccountsId");
		
		$sCode .= '<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">';
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$dBalance = $objDatabase->Result($varResult, $i, "Balance");
			$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
			$sChartOfAccountsTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
			$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");
			
			$dChildBalance = 0;
			
			
			$varResult2 = $objDatabase->Query("
			SELECT 
			 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
			 CA.AccountTitle AS 'AccountTitle',
			 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
			 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
			FROM fms_accounts_chartofaccounts_categories AS CCAT
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
			INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
			LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
			INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
			WHERE CA.ParentChartOfAccountsId='$iChartOfAccountsId'  AND GJ.IsDeleted ='0'
			GROUP BY CA.ChartOfAccountsId");
			if ($objDatabase->RowsNumber($varResult2) > 0)
			{
				for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
				{
					$dChildBalance += $objDatabase->Result($varResult2, $j, "Balance");
				}

				$sChartOfAccounts = '<tr><td style="width:40px;"></td><td><a href="#noanchor" onclick="OpenChartOfAccountsChildren(' . $iChartOfAccountsId . ');"><img id="imgChartOfAccountsParents' . $iChartOfAccountsId . '" src="../images/icons/plus.gif" align="absmiddle" border="0" alt="Click to show more" title="Click to show more" />&nbsp;' . $sChartOfAccountsCode . ' - ' . $sChartOfAccountsTitle . '</a></td><td align="right">' . number_format($dBalance + $dChildBalance) . '</td></tr><tr><td style="height:8px;" colspan="3"></td></tr><tr style="display:none;" id="trChartOfAccountsChildren' . $iChartOfAccountsId . '"><td colspan="3"><div id="divChartOfAccountsChildren' . $iChartOfAccountsId . '"></div></td></tr>';
			}
			else
				$sChartOfAccounts = '<tr><td style="width:40px;"></td><td>' . $sChartOfAccountsCode . ' - ' . $sChartOfAccountsTitle . '</td><td align="right">' . number_format($dBalance) . '</td></tr><tr><td style="height:8px;" colspan="3"></td></tr>';
			
			$sCode .= $sChartOfAccounts;
		}
		$sCode .= '</table>';
		
		
		$objResponse->addScript('document.getElementById("divChartOfAccounts' . $iChartOfAccountsControlParentId . '").innerHTML = \'' . $sCode . '\';');

		return($objResponse->getXML());		
	}
	
	function AJAX_GetChartOfAccountsChildren($iChartOfAccountsId, $bChildControl)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		$varResult = $objDatabase->Query("SELECT 
		 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
		 CA.AccountTitle AS 'AccountTitle',
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_chartofaccounts_categories AS CCAT
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		WHERE CA.ParentChartOfAccountsId='$iChartOfAccountsId'  AND GJ.IsDeleted ='0'
		GROUP BY CA.ChartOfAccountsId");
		
		$sCode = '<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">';
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$dBalance = $objDatabase->Result($varResult, $i, "Balance");
			$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
			$sChartOfAccountsTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
			//$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");			
			$iWidth = (($bChildControl == 'true') ? '90' : '60');
			
			$sChartOfAccounts = $sChartOfAccountsCode . ' - ' . $sChartOfAccountsTitle;
			$sCode .= '<tr><td style="width:' . $iWidth . 'px;"></td><td>' . $sChartOfAccounts . '</td><td align="right">' . number_format($dBalance) . '</td></tr><tr><td style="height:8px;" colspan="3"></td></tr>';
		}
		$sCode .= '</table>';
		
		$objResponse->addScript('document.getElementById("divChartOfAccountsChildren' . $iChartOfAccountsId . '").innerHTML = \'' . $sCode . '\';');

		return($objResponse->getXML());		
	}
		
	function AJAX_GetChartOfAccountsCode($iChartOfAccountsControlId, $sTargetSelectBox)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		$aData = $this->GetParentChartOfAccounts($iChartOfAccountsControlId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");

		$varResult = $objDatabase->Query("SELECT MAX(ChartOfAccountsCode) AS 'LastCode' FROM fms_accounts_chartofaccounts AS CAC WHERE CAC.ChartOfAccountsControlId = '$iChartOfAccountsControlId'");
		if (($objDatabase->RowsNumber($varResult) > 0) && ($objDatabase->Result($varResult, 0, "LastCode") != ""))
		{
			$iLastCode = $objDatabase->Result($varResult, 0, "LastCode");
			$iNextCode = $iLastCode + 1;
															
    		$objResponse->addScript('SetVal("txtChartOfAccountsCode", "' . $iNextCode . '");');
		}
		else
		{
			$varResult = $objDatabase->Query("SELECT CAC.StartRange AS 'LastCode' FROM fms_accounts_chartofaccounts_controls AS CAC WHERE CAC.ChartOfAccountsControlId='$iChartOfAccountsControlId'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iLastCode = $objDatabase->Result($varResult, 0, "LastCode");
				$iNextCode = $iLastCode;
    			$objResponse->addScript('SetVal("txtChartOfAccountsCode", "' . $iNextCode . '");');
			}
		}

		return($objResponse->getXML());
	}
	


	function AJAX_GetControlCode($iChartOfAccountsCategoryId, $sTargetSelectBox)
	{
		$objResponse = new xajaxResponse();   
	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
	   	$aData = $this->GetControls($iChartOfAccountsCategoryId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		return($objResponse->getXML());
	}
	
	function AjaxGetChartOfAccountsCodeById($iChartOfAccountsControlId)
	{
		global $objDatabase;
	   	
		$aData = $this->GetParentChartOfAccounts($iChartOfAccountsControlId);
	
		$varResult = $objDatabase->Query("SELECT MAX(ChartOfAccountsCode) AS 'LastCode' FROM fms_accounts_chartofaccounts AS CAC WHERE CAC.ChartOfAccountsControlId = '$iChartOfAccountsControlId'");
		if (($objDatabase->RowsNumber($varResult) > 0) && ($objDatabase->Result($varResult, 0, "LastCode") != ""))
		{
			$iLastCode = $objDatabase->Result($varResult, 0, "LastCode");
			$iNextCode = $iLastCode + 1;
		}
		else
		{
			$varResult = $objDatabase->Query("SELECT CAC.StartRange AS 'LastCode' FROM fms_accounts_chartofaccounts_controls AS CAC WHERE CAC.ChartOfAccountsControlId='$iChartOfAccountsControlId'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iLastCode = $objDatabase->Result($varResult, 0, "LastCode");
				$iNextCode = $iLastCode;
			}
		}

		$aResponse['aData']     = $aData;
		$aResponse['sLastCode'] = $iNextCode;

		echo json_encode($aResponse);die;
	}


	function AjaxGetControlCodeById($iChartOfAccountsCategoryId)
	{
	   	$aData = $this->GetControls($iChartOfAccountsCategoryId);
	 	echo json_encode($aData);die;
	}



	function GetControls($iChartOfAccountsCategoryId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_chartofaccounts_controls AS CAC		
		WHERE CAC.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId' AND CAC.ChartOfAccountsControlParentId = '0'");
		
		$aResult[0][0] = 0;
		$aResult[0][1] = "Select Control Code";		
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iChartOfAccountsControlParentId = $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsControlId");
			$iSno++;
			
			$aResult[$iSno][0] = $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsControlId");
			$aResult[$iSno][1] = $objDatabase->Result($varResult, $i, "CAC.ControlCode") . ' - ' .$objDatabase->Result($varResult, $i, "CAC.ControlName");
			
			if ($objDatabase->DBCount("fms_accounts_chartofaccounts_controls AS CAC", "CAC.ChartOfAccountsControlParentId='$iChartOfAccountsControlParentId'") > 0)
			{ 
				$varResult2 = $objDatabase->Query("SELECT 
				 CC.ChartOfAccountsControlId AS 'ChartOfAccountsControlId',
				 CC.ControlName AS 'ControlName',		 
				 CC. ControlCode AS 'ControlCode'
				FROM fms_accounts_chartofaccounts_controls AS CC 
				WHERE CC.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId' AND CC.ChartOfAccountsControlParentId = '$iChartOfAccountsControlParentId'
				ORDER BY CC.ControlName");		
				for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
				{
					$iSno++;
					$aResult[$iSno][0] = $objDatabase->Result($varResult2, $j, "CC.ChartOfAccountsControlId");
					$aResult[$iSno][1] = $objDatabase->Result($varResult2, $j, "CC.ControlCode") . ' - ' .$objDatabase->Result($varResult2, $j, "CC.ControlName");
				}
			}
		}
		return($aResult);
	}
	
	function GetParentChartOfAccounts($iChartOfAccountsControlId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_chartofaccounts AS CA
		WHERE CA.ChartOfAccountsControlId='$iChartOfAccountsControlId' AND CA.ParentChartOfAccountsId = '0'");		
		$aResult[0][0] = 0;
		$aResult[0][1] = "NO Parent";
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{						
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode") . '-' .$objDatabase->Result($varResult, $i, "CA.AccountTitle");
		}
		return($aResult);
	}
	
	
	function ChartOfAccountsDirectory($sSearch)
	{
		global $objDatabase;

		$dDateRange = date('Y-m-d');
				
		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location=\'../accounts/chartofaccountsdirectory.php\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		
		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((CA.ChartOfAccountsCode LIKE '%$sSearch%') OR (CA.AccountTitle LIKE '%$sSearch%') OR (CACT.CategoryName LIKE '%$sSearch%') OR (CACT.CategoryCode LIKE '%$sSearch%') OR (CC.ControlName LIKE '%$sSearch%') OR (CC.ControlCode LIKE '%$sSearch%'))";
			$sTitle = ' - Search Results for "' . $sSearch . '"';
		}   

		$sReturn = '<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span style="font-size:18px; font-family:Tahoma, Arial;">Chart of Accounts ' . $sTitle . '</span></td><td align="right"><form method="GET" action="">Search:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Chart of Accounts Entry" title="Search for Chart of Accounts Entry" border="0"></form>' . $sButtons . '</td></tr></table>';
		
		// Get Categories
		$sQuery = "
		SELECT
		 CACT.CategoryName AS 'CategoryName',
		 CACT.CategoryCode AS 'CategoryCode',
		 CACT.ChartOfAccountsCategoryId AS 'CategoryId',
		 IF(CACT.DebitIncrease='1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_chartofaccounts_categories AS CACT
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CACT.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		WHERE 1=1   AND GJ.IsDeleted ='0' $sSearchCondition
		GROUP BY CACT.ChartOfAccountsCategoryId";
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		
		$sReturn .= '<br />
		<table border="0" bordercolor="#cecece" cellspacing="0" cellpadding="5" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" width="1%">&nbsp;</td>
		  <td align="center" width="1%">&nbsp;</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Chart of Accounts</td>
		  <td align="right" style="width:100px; font-weight:bold; font-size:12px;">Balance</td>
	     </tr>
	     </thead>';
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{				
				$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
				$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");
				$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");				
				$dBalance = $objDatabase->Result($varResult, $i, "Balance");
				
				$sReturn .= '<tr style="background-color:#d6e6ed;">
				<td style="border-top:1px solid; font-family:Tahoma, Arial; font-size:16px;" colspan="3"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selChartOfAccountCategory='. $iCategoryId . '&selChartOfAccountControl='. '\', 800,600);" />' . $sCategoryCode . ' - '.  $sCategoryName . '</a></td>
				<td align="right" style="border-top:1px solid; font-family:Tahoma, Arial; font-size:16px;">' . number_format($dBalance, 0) . '</td> 
				</tr>';
				
				// Get Controls [ Parents ]
				$varResult2 = $objDatabase->Query("SELECT 
				 CC.ChartOfAccountsControlId AS 'ChartOfAccountsControlId',
				 CC.ControlName AS 'ControlName',
				 CC.ChartOfAccountsControlParentId AS 'ControlParent',
				 IF(CCAT.DebitIncrease='1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
				FROM fms_accounts_chartofaccounts_categories AS CCAT
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
				LEFT JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
				LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
				WHERE CCAT.CategoryCode='$sCategoryCode' AND CC.ChartOfAccountsControlParentId='0'  AND GJ.IsDeleted ='0'
				GROUP BY CC.ChartOfAccountsControlId");				
				
				for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
				{
					$iChartOfAccountsControlId = $objDatabase->Result($varResult2, $j, "ChartOfAccountsControlId");
					$sControlName = $objDatabase->Result($varResult2, $j, "ControlName");
					$dBalance = $objDatabase->Result($varResult2, $j, "Balance");
					$iChartOfAccountsControlParentId = $objDatabase->Result($varResult2, $j, "ControlParent");
					
					$sChildControls = "";
					$iChildChartOfAccountsControlId = 0;
					$dChildBalance = 0;
					// Get Controls [ Children ]
					$varResult3 = $objDatabase->Query("SELECT 
						CC.ChartOfAccountsControlId AS 'ChartOfAccountsControlId',
						CC.ControlName AS 'ControlName',
						CC.ChartOfAccountsControlParentId AS 'ControlParent',
					IF(CCAT.DebitIncrease='1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
					FROM fms_accounts_chartofaccounts_categories AS CCAT
					INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
					INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
					LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
					INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
					WHERE CCAT.CategoryCode='$sCategoryCode' AND CC.ChartOfAccountsControlParentId='$iChartOfAccountsControlId'  AND GJ.IsDeleted ='0'
					GROUP BY CC.ChartOfAccountsControlId");
					if ($objDatabase->RowsNumber($varResult3) > 0)
					{
						for ($k=0; $k < $objDatabase->RowsNumber($varResult3); $k++)
						{
							$iChildChartOfAccountsControlId = $objDatabase->Result($varResult3, $k, "ChartOfAccountsControlId");
							$sChildControlName = $objDatabase->Result($varResult3, $k, "ControlName");
							$dChildBalance = $objDatabase->Result($varResult3, $k, "Balance");
							$dChildBalance += $dChildBalance;
							
							$sChildControls .='<tr style="background-color:#fff8cf">
							 <td>&nbsp;</td>
							 <td>&nbsp;</td>
							 <td align="left" style="font-family:Tahoma, Arial; font-size:14px;"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&txtDateRangeEnd='.$dDateRange.'&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selChartOfAccountCategory='. $iCategoryId . '&selChartOfAccountControl='. $iChildChartOfAccountsControlId . '\', 800,600);" />&nbsp;&nbsp;&nbsp;' . $sChildControlName . '</a></td>
							 <td align="right" style="font-family:Tahoma, Arial; font-size:14px;">' . number_format($dChildBalance, 0) . '</td>
							</tr>';
							
							$sChildControls .= $this->GetChartOfAccounts($iChildChartOfAccountsControlId);
						}
					}
					
					$sReturn .='<tr style="background-color:#fff3a0;">
					 <td>&nbsp;</td>
					 <td style="font-family:Tahoma, Arial; font-size:14px;" colspan="2"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&txtDateRangeEnd='.$dDateRange.'&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId .  '&selChartOfAccountCategory='. $iCategoryId . '&selChartOfAccountControl='. $iChartOfAccountsControlId . '\', 800,600);" />' . $sControlName . '</a></td>
					 <td style="font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dBalance+$dChildBalance, 0) . '</td> 
					</tr>';

					$sReturn .= $this->GetChartOfAccounts($iChartOfAccountsControlId);
					$sReturn .= $sChildControls;					
				}
			}
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma;font-size:16px;" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

	   	return($sReturn);
	}
	
	function GetChartOfAccounts($iChartOfAccountsControlId)
	{
		global $objDatabase;
		
		// Get Chart of Accounts - Parents
		$varResult = $objDatabase->Query("SELECT 
		 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
		 CA.AccountTitle AS 'AccountTitle',
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_chartofaccounts_categories AS CCAT
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		WHERE CA.ChartOfACcountsControlId='$iChartOfAccountsControlId'  AND GJ.IsDeleted ='0' AND (CA.ParentChartOfAccountsId='0' OR CA.ParentChartOfAccountsId='-1')
		GROUP BY CA.ChartOfAccountsId");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$dBalance = $objDatabase->Result($varResult, $i, "Balance");
			$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
			$sChartOfAccountsTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
			$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");
			
			$dChildBalance = 0;
			$sChildControls = "";
			
			// Get Chart of Accounts [ Children ]
			$varResult2 = $objDatabase->Query("
			SELECT 
			 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
			 CA.AccountTitle AS 'AccountTitle',
			 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
			 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
			FROM fms_accounts_chartofaccounts_categories AS CCAT
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
			INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
			LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
			INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
			WHERE CA.ParentChartOfAccountsId='$iChartOfAccountsId' AND GJ.IsDeleted ='0'
			GROUP BY CA.ChartOfAccountsId");
			if ($objDatabase->RowsNumber($varResult2) > 0)
			{
				for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
				{
					$sChildChartOfAccountsCode = $objDatabase->Result($varResult2, $j, "ChartOfAccountsCode");
					$sChildChartOfAccountTitle = $objDatabase->Result($varResult2, $j, "AccountTitle");
					$iChartOfAccountsId = $objDatabase->Result($varResult2, $j, "ChartOfAccountsId");
					$dTempChildBalance = $objDatabase->Result($varResult2, $j, "Balance");
					
					$sChildControls .='<tr>
					 <td>&nbsp;</td>
					 <td>&nbsp;</td>
					 <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $sChildChartOfAccountsCode . ' - ' . $sChildChartOfAccountTitle . '</td>
					 <td align="right">' . number_format($dTempChildBalance, 0) . '</td>
					</tr>';
					
					$dChildBalance += $dTempChildBalance;
				}
			}
			
			$sReturn .='<tr>
			 <td>&nbsp;</td>
			 <td>&nbsp;</td>
			 <td align="left">' . $sChartOfAccountsCode . ' - ' . $sChartOfAccountsTitle . '</td>
			 <td align="right">' . number_format($dBalance + $dChildBalance, 0) . '</td>
			</tr>' . $sChildControls;
		}
		
		return($sReturn);
	}
		
	function GetChildControlCodes($iChartOfAccountsCategoryId, $iChartOfAccountsControlParentId, $sSelectBox=false, &$iSno="")
	{
		global $objDatabase;
		
		$sControlCode = '';
		
		$varResult = $objDatabase->Query("SELECT 
		 CC.ChartOfAccountsControlId AS 'ChartOfAccountsControlId',
		 CC.ControlName AS 'ControlName',		 
		 CC. ControlCode AS 'ControlCode'
		FROM fms_accounts_chartofaccounts_controls AS CC 
		WHERE CC.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId' AND CC.ChartOfAccountsControlParentId = '$iChartOfAccountsControlParentId'
		ORDER BY CC.ControlName");		
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{			
			$iChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "ChartOfAccountsControlId");
			if($sSelectBox)
				$sControlCode .= '<option style="color:green;" value="' . $iChartOfAccountsControlId . '">&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CC.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CC.ControlName") . '</option>';
			else
			{	
				$iSno++;
				$sControlCode = array();
				$sControlCode[$iSno][0] = $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId");
				$sControlCode[$iSno][1] = $objDatabase->Result($varResult, $i, "CC.ControlCode") . ' - ' .$objDatabase->Result($varResult, $i, "CC.ControlName");
				//$iChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "ChartOfAccountsControlId");
			}	
		}
		
		return($sControlCode);
	}
}

?>