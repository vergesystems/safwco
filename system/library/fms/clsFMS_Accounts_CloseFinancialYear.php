<?php

// Class: CloseFinancialYear
class clsCloseFinancialYear
{
	// Class Constructor
	function __construct()
	{
	}
	
	function ShowCloseFinancialYear()
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear[1] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
			
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();
		
		$sAction2 = $objGeneral->fnGet("action2");
		$sTitle = $objGeneral->fnGet("txtTitle");
		$iResetSeries = $objGeneral->fnGet("chkResetSeries");
		$dClosingDate = $objGeneral->fnGet("txtClosingDate");
		$dOpeningDate = $objGeneral->fnGet("txtOpeningDate");
		if ($dClosingDate == "") $dClosingDate = date("Y-m-d");
		if ($dOpeningDate == "") $dOpeningDate = date("Y-m-d");
		
		
		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		
		$sTitle = '<input type="text" size="28" id="txtTitle" name="txtTitle" class="Tahoma14" value="' . $sTitle . '" />';
		$sOpeningDate = '<input type="text" size="10" id="txtOpeningDate" name="txtOpeningDate" class="Tahoma14" value="' . $dOpeningDate . '" />' . $objjQuery->Calendar('txtOpeningDate');
		$sClosingDate = '<input type="text" size="10" id="txtClosingDate" name="txtClosingDate" class="Tahoma14" value="' . $dClosingDate . '" />' . $objjQuery->Calendar('txtClosingDate');
		$sResetReferenceSeries = '<input type="checkbox" ' . (($iResetSeries == "on") ? 'checked="true"' : '') . ' name="chkResetSeries" id="chkResetSeries" value="on" />';
		
		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Close Financial Year</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		$sReturn .= '<br />
		<script language="JavaScript" type="text/javascript">
		function CloseFinancialYear()
		{
			if (GetVal(\'txtTitle\') == "") return(AlertFocus(\'Please enter a valid Title\', \'txtTitle\'));
			if (!isDate(GetVal(\'txtOpeningDate\'))) return(AlertFocus(\'Please select a valid Opening Date\', \'txtOpeningDate\'));
			if (!isDate(GetVal(\'txtClosingDate\'))) return(AlertFocus(\'Please select a valid Closing Date\', \'txtClosingDate\'));
			
			return(true);
		}
		
		</script>
		<table border="0" cellspacing="0" cellpadding="3" style="width:650px;" align="center">
		 <tr>
		  <td>
		   <fieldset><legend style="font-size:14px; font-weight:bold;">Close Financial Year Criteria:</legend>
		    <form method="post" action="#ReviewCloseFinancialYear" onsubmit="return CloseFinancialYear();">
		    <table border="0" cellspacing="5" cellpadding="3" width="95%" align="center">
		     <tr><td class="Tahoma16" width="30%" align="right">Title:</td><td>' . $sTitle . '</td></tr>
		     <tr><td class="Tahoma16" align="right">Closing Between:</td><td class="Tahoma16">' . $sOpeningDate . ' - to - ' . $sClosingDate . '</td></tr>
			 <tr><td class="Tahoma16" align="right">Reset Reference Series:</td><td>' . $sResetReferenceSeries . '</td></tr>
			 <tr><td></td><td><input type="submit" class="AdminFormButton1" value="Review" /></td></tr>
		    </table>
		    <input type="hidden" name="action2" id="action2" value="ReviewCloseFinancialYear" />
		    </form>
		   </fieldset>
		   <br />
		  </td>
		 </tr>
		</table>';
		
		if ($sAction2 == "ReviewCloseFinancialYear")
			$sReturn .= $this->ShowReviewCloseFinancialYear();
		
		$sReturn .= '</td></tr></table>
		</td></tr></table>';
		
		return($sReturn);	
	}

	function ShowReviewCloseFinancialYear()
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		
		$sTitle = $objGeneral->fnGet("txtTitle");
		$dOpeningDate = $objGeneral->fnGet("txtOpeningDate");
		$dClosingDate = $objGeneral->fnGet("txtClosingDate");
		$iResetSeries = $objGeneral->fnGet("chkResetSeries");
		
		$iResetSeries = ($iResetSeries == "on") ? 1 : 0;
		
		$sReturn = '
		<script language="JavaScript" type="text/javascript">
		function ConfirmCloseFinancialYear()
		{
			if(confirm(\'Do you really want to Close the Financial Year?\'))
				return(true);
			
			return(false);
		}
		</script>
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr>
		  <td>
		  <a name="#ReviewCloseFinancialYear"></a>
		   <form method="post" action="../accounts/closefinancialyear_show.php" onsubmit="return ConfirmCloseFinancialYear();">
		   <table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="70%" align="center">
		    <tr style="background-color:#ffffff;">
		     <td><span class="title2" style="color:#003366">Review closing balances to close financial year as of ' . date("F j, Y", strtotime($dClosingDate)) . ':</span></td>
   	        </tr>';
		
		$iTransactions = $objDatabase->DBCount("fms_accounts_generaljournal AS GJ", "(GJ.TransactionDate BETWEEN '$dOpeningDate' AND '$dClosingDate')  AND GJ.IsDeleted ='0' AND GJ.TransactionClosed='0'");
		$sReturn .= '<tr><td class="Tahoma16" align="center">
		 <br /><br />
		 <span style="font-weight:bold;">' . $iTransactions . '</span> transactions will be closed!
		 <br /><br /><br />
		</td></tr>';
		
		$sReturn .= '
		   </table>
		  </td>
		 </tr>
	     <tr bgcolor="#ffffff">
		  <td colspan="3" align="center"><br /><input type="submit" class="AdminFormButton1" value="Close Year" /><br /><br /></td>
		 </tr>
		</table>
		<input type="hidden" name="hdnTitle" id="hdnTitle" value="' . $sTitle . '" />
		<input type="hidden" name="hdnOpeningDate" id="hdnOpeningDate" value="' . $dOpeningDate . '" />
		<input type="hidden" name="hdnClosingDate" id="hdnClosingDate" value="' . $dClosingDate . '" />
		<input type="hidden" name="hdnResetSeries" id="hdnResetSeries" value="' . $iResetSeries . '" />
		<input type="hidden" name="action" id="action" value="CloseFinancialYear" />
	    </form>';

		return($sReturn);
	}

	function CloseFinancialYear($sTitle, $dOpeningDate, $dClosingDate, $iResetSeries)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
				
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear[1] == 0) // View Disabled	
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		$iEmployeeId = $objEmployee->iEmployeeId;
		
		// Check if Year is closed on Same Date, then it can not be reclosed.
		if ($objDatabase->DBCount("fms_accounts_closefinancialyear AS CFY", "CFY.ClosingDate='$dClosingDate'") > 0) 
			return(7406);
	
		$sTitle = mysql_escape_string($sTitle);

		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_closefinancialyear
		(Title, OpeningDate, ClosingDate, ResetSeries, AddedOn, AddedBy) VALUES ('$sTitle', '$dOpeningDate', '$dClosingDate', '$iResetSeries', '$varNow', '$iEmployeeId')");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal SET TransactionClosed='1' WHERE (TransactionDate BETWEEN '$dOpeningDate' AND '$dClosingDate') AND TransactionClosed='0'");
			
			if ($iResetSeries == 1)
				$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_series SET Series='0'");			
			
			$objSystemLog->AddLog("Close Financial Year - " . $sTitle);

			return(7400);
		}

		return(7401);
	}
	
	function DeleteCloseFinancialYear($iCloseFinancialYearId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear[3] == 0) // View Disabled
			return(1010);

		//$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_closefinancialyear AS CFY WHERE CFY.CloseFinancialYearId='$iCloseFinancialYearId'");
		//if ($objDatabase->RowsNumber($varResult) <= 0)
		//	return(7405);

		$sTitle = $objDatabase->Result($varResult, 0, "CFY.Title");

		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_closefinancialyear WHERE CloseFinancialYearId='$iCloseFinancialYearId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_accounts_closefinancialyear_chartofaccounts WHERE CloseFinancialYearId='$iCloseFinancialYearId'");
			
			$objSystemLog->AddLog("Delete Close Financial Year - " . $sTitle);

			return(7404);
		}
		else
			return(7405);
	}


	function CloseFinancialYearDetails($iCloseFinancialYearId)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_accounts_closefinancialyear AS CFY
		INNER JOIN organization_employees AS E ON E.EmployeeId = CFY.AddedBy
		WHERE CFY.CloseFinancialYearId = '$iCloseFinancialYearId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		
		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">CloseFinancialYear Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{			
				$iCloseFinancialYearId = $objDatabase->Result($varResult, 0, "CFY.CloseFinancialYearId");
				$sTitle = $objDatabase->Result($varResult, 0, "CFY.Title");
				$dClosingDate = $objDatabase->Result($varResult, 0, "CFY.ClosingDate");
				$sClosingDate = date("F j, Y", strtotime($dClosingDate));
				$sDescription = $objDatabase->Result($varResult, 0, "CFY.Description");
				$dAddedOn = $objDatabase->Result($varResult, 0, "CFY.AddedOn");
				$sAddedOn = date("F j, Y", strtotime($dAddedOn)) . ' at ' . date("g:i a", strtotime($dAddedOn));
				$iAddedBy = $objDatabase->Result($varResult, 0, "CFY.AddedBy");
				$sAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sAddedBy = $sAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(str_replace('"', '', $sAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$varResult2 = $objDatabase->Query("SELECT *
				FROM fms_accounts_closefinancialyear_chartofaccounts AS CFYC
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= CFYC.ChartOfAccountsId
				WHERE CFYC.CloseFinancialYearId= '$iCloseFinancialYearId'");
						       	
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';
					
					$sChartOfAccount = $objDatabase->Result($varResult2, $i, "CA.AccountTitle") . '-' . $objDatabase->Result($varResult2, $i, "CA.ChartOfAccountsCode");
					$dClosingBalance = $objDatabase->Result($varResult2, $i, "CFYC.ClosingBalance");
					$sClosingBalance = number_format($dClosingBalance, 2);
					
					
					$sChartOfAccounts_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="left">' . $sChartOfAccount . '</td>					 
					 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . $sClosingBalance . '</td>					 
					</tr>';
				}
				
				$sChartOfAccounts = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="60%" align="left">
		         <tr class="GridTR">				  
				  <td style="width="80%""><span class="WhiteHeading">Chart of Accounts</span></td>
				  <td align="right"><span class="WhiteHeading">Balance</span></td>
				 </tr>
				 ' . $sChartOfAccounts_Rows . '
				</table>';
				
			}
			
			$sReturn .= '
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Close Financial Year Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" width="20%">Close Financial Year Id:</td><td><strong>' . $iCloseFinancialYearId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Title:</td><td><strong>' . $sTitle . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Closing Date:</td><td><strong>' . $sClosingDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Description:</td><td><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Chart Of Accounts:</span></td></tr>
			 <tr><td colspan="2">' . $sChartOfAccounts . '</td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top">Added On:</td><td><strong>' . $sAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Added By:</td><td><strong>' . $sAddedBy . '</strong></td></tr>
			</table>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}

/*	
	function ShowCloseFinancialYearWizard($sAction2, $iError="0")
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_CloseFinancialYear[1] == 0) // View Disabled
			return(1010);
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();		
		
		if($iError == "7400")
		{
			$sReturn = '<br /><br /><div align="center" style="font-size:22px; font-family: Tahoma, Arial;"><img src="bor/images/iconSuccess.gif" alt="Success" title="Success" /><br />Financial Year has been closed successfully...</div>';
		}
		if($iError == "7406")
		{
			$sReturn = '<br /><br /><div align="center" style="font-size:22px; font-family: Tahoma, Arial;"><img src="bor/images/iconFail.gif" alt="Sorry!" title="Sorry!" /><br />Sorry, Unable to close financial Year, it is closed earlier on specified date!</div>';
		}
		if($iError == "7401")
		{
			$sReturn = '<br /><br /><div align="center" style="font-size:22px; font-family: Tahoma, Arial;"><img src="bor/images/iconFail.gif" alt="Sorry!" title="Sorry!" /><br />Sorry, Unable to close financial Year!</div>';
		}
		
		$sReturn = '<form method="post" action="" onsubmit="return ValidateForm();">
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr><td colspan="5"><span style="font-family:Tahoma, Arial; font-size:22px;">Close Financial Year</span></td></tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr>
		 <td>';
		
		if ($sAction2 == "addnew")
		{	
			$iCloseFinancialYearId = "";
			$sTitle = $objGeneral->fnGet("txtTitle");
			$sDescription = $objGeneral->fnGet("txtDescription");
			$dClosingDate = $objGeneral->fnGet("txtClosingDate");
			
			$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="Tahoma14" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
			$sClosingDate = '<input type="text" size="10" id="txtClosingDate" name="txtClosingDate" class="Tahoma14" value="' . $dClosingDate . '" />' . $objjQuery->Calendar('txtClosingDate') . '&nbsp;<span style="color:red;">*</span>';
			$sDescription = '<textarea name="txtDescription" id="txtDescription" class="Tahoma14" rows="5" cols="60">' . $sDescription . '</textarea>';		
			
			$sReturn .= '
			  <div id="divStep1">
			  <table border="0" style="background-color:#efefef" cellspacing="0" cellpadding="8" width="100%" align="center"><tr><td style="font-family: Times New Roman, Arial, Verdana; font-size:20px; font-weight:bold; font-style:italic;">Close Financial Year:</td></tr></table>
			   <br /><br />
			   <table border="0" cellspacing="0" cellpadding="8" width="80%" align="center">
			    <tr><td width="35%" align="right" class="Tahoma16">Title:</td><td>' . $sTitle . '</td></tr>
				<tr><td align="right" class="Tahoma16">Closing Date:</td><td>' . $sClosingDate . '</td></tr>
				<tr><td align="right" class="Tahoma16">Description:</td><td>' . $sDescription . '</td></tr>				
			   </table>';
		}
		
		if ($sAction2 == "addnew")
			$sReturn .= '<br /><br /><div align=center><input type="BUTTON" style="font-size:18px;" value="Close Financial Year" onClick="MOOdalBox.open(\'closefinancialyear_wizard.php?action=AddNewCloseFinancialYear&txtTitle=\'+GetVal(\'txtTitle\')+\'&txtClosingDate=\'+GetVal(\'txtClosingDate\')+\'&txtDescription=\'+GetVal(\'txtDescription\'), \'Close Financial Year...\', \'650 400\');" /><input type="hidden" name="action" id="action" value="AddNewCloseFinancialYear"></form></div><br /><div align="left">&nbsp;<span style="font-family:Tahoma, Arial; font-size:12px;color:red;">* Required Fields</span></div>';

		$sReturn .= '  
		 </td>
		</tr>
		</table>
		</form>';
		
		return($sReturn);		
	}
*/
}

?>