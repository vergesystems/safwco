<?php

// Class: Donor
class clsDonors
{
	// Class Constructor
	function __construct()
	{
	}

	
	function ShowAllDonors($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Donors";
		if ($sSortBy == "") $sSortBy = "D.DonorId DESC";

		if ($sSearch != "")
		{
			$sSearchCondition = " AND ((D.DonorName LIKE '%$sSearch%') OR (D.DonorCode LIKE '%$sSearch%'))";
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_donors AS D", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_donors AS D		
		WHERE 1=1 $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Donor Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=D.DonorName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Donor Name in Ascending Order" title="Sort by Donor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=D.DonorName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Donor Name in Descending Order" title="Sort by Donor Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Donor Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=D.DonorCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Donor Code in Ascending Order" title="Sort by Donor Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=D.DonorCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Donor Code in Descending Order" title="Sort by Donor Code in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			
			$iDonorId = $objDatabase->Result($varResult, $i, "D.DonorId");
			$sDonorName = $objDatabase->Result($varResult, $i, "D.DonorName");
			$sDonorCode = $objDatabase->Result($varResult, $i, "D.DonorCode");
			$sDescription = $objDatabase->Result($varResult, $i, "D.Description");
			$sNotes = $objDatabase->Result($varResult, $i, "D.Notes");
			$dDonorAddedOn = $objDatabase->Result($varResult, $i, "D.DonorAddedOn");
			$iDonorAddedBy = $objDatabase->Result($varResult, $i, "D.DonorAddedBy");

			$sEditDonor = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update' . $objDatabase->RealEscapeString(stripslashes($sDonorName)) . '\', \'../accounts/donors_details.php?action2=edit&id=' . $iDonorId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Donor Details" title="Edit Donor Details"></a></td>';
			$sDeleteDonor = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Donor?\')) {window.location = \'?action=DeleteDonor&id=' . $iDonorId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete Donor" title="Delete Donor"></a></td>';
			
			if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[2] == 0)$sEditDonor = '';				
			//if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[3] == 0)$sDeleteDonor = '';
			
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sDonorName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sDonorCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>			 
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' .  $objDatabase->RealEscapeString(stripslashes($sDonorName)) . '\', \'../accounts/donors_details.php?id=' . $iDonorId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Donor Details" title="View this Donor Details"></a></td>
			 ' . $sEditDonor . $sDeleteDonor . '</tr>';
		}

		$sAddDonor = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Donor\', \'../accounts/donors_details.php?action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Donor">';
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[1] == 0)
			$sAddDonor = '';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddDonor . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Donor:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Donor" title="Search for Donor" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Donor Details" alt="View this Donor Details"></td><td>View this Donor Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Donor Details" alt="Edit this Donor Details"></td><td>Edit this Donor Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Donor" alt="Delete this Donor"></td><td>Delete this Donor</td></tr>
		</table>';

		return($sReturn);
	}
	
	function DonorDetails($iDonorId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *,D.Notes AS Notes
		FROM fms_accounts_donors AS D
		INNER JOIN organization_employees AS E ON E.EmployeeId = D.DonorAddedBy
		WHERE D.DonorId = '$iDonorId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iDonorId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Donor" title="Edit this Donor" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Donor?\')) {window.location = \'?action=DeleteDonor&id=' . $iDonorId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Donor" title="Delete this Donor" /></a>';
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[2] == 0)$sButtons_Edit = '';		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[3] == 0)$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Donor Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iDonorId = $objDatabase->Result($varResult, 0, "D.DonorId");
				$sDonorName = $objDatabase->Result($varResult, 0, "D.DonorName");
				$sDonorCode = $objDatabase->Result($varResult, 0, "D.DonorCode");
				$sDescription = $objDatabase->Result($varResult, 0, "D.Description");
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
				$dDonorAddedOn = $objDatabase->Result($varResult, 0, "D.DonorAddedOn");
				$sDonorAddedOn = date("F j, Y", strtotime($dDonorAddedOn)) . ' at ' . date("g:i a", strtotime($dDonorAddedOn));
				$iDonorAddedBy = $objDatabase->Result($varResult, 0, "D.DonorAddedBy");
				$sDonorAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sDonorAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName)) . '\', \'../organization/employees_details.php?id=' . $iDonorAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sDonorName = '<input type="text" name="txtDonorName" id="txtDonorName" class="form1" value="' . $sDonorName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDonorCode = '<input type="text" name="txtDonorCode" id="txtDonorCode" class="form1" value="' . $sDonorCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$iDonorId = "";
				$sDonorName = $objGeneral->fnGet("txtDonorName");
				$sDonorCode = $objGeneral->fnGet("txtDonorCode");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sDonorAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sDonorAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sDonorAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sDonorName = '<input type="text" name="txtDonorName" id="txtDonorName" class="form1" value="' . $sDonorName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDonorCode = '<input type="text" name="txtDonorCode" id="txtDonorCode" class="form1" value="' . $sDonorCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sDonorAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if (GetVal(\'txtDonorName\') == "") return(AlertFocus(\'Please enter a valid Donor Name\', \'txtDonorName\'));
				if (GetVal(\'txtDonorCode\') == "") return(AlertFocus(\'Please enter a valid Donor Code\', \'txtDonorCode\'));

				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Donor Information:</span></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200px;">Donor Name:</td><td><strong>' . $sDonorName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Donor Code:</td><td><strong>' . $sDonorCode . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Donor Added On:</td><td><strong>' . $sDonorAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Donor Added By:</td><td><strong>' . $sDonorAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Donor" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iDonorId . '"><input type="hidden" name="action" id="action" value="UpdateDonor"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Donor" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewDonor"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewDonor($sDonorName, $sDonorCode, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[1] == 0)
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		$iDonorAddedBy = $objEmployee->iEmployeeId;
	
		$sDonorName = $objDatabase->RealEscapeString($sDonorName);
		$sDonorCode = $objDatabase->RealEscapeString($sDonorCode);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);

		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_donors
		(DonorName, DonorCode, Description, Notes, DonorAddedOn, DonorAddedBy)
		VALUES ('$sDonorName', '$sDonorCode', '$sDescription', '$sNotes', '$varNow', '$iDonorAddedBy')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D WHERE D.DonorName='$sDonorName' AND D.DonorAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$objSystemLog->AddLog("Add New Donor - " . $sDonorName);
				$objGeneral->fnRedirect('?error=3201&id=' . $objDatabase->Result($varResult, 0, "D.DonorId"));
			}
		}

		return(3202);
	}
	
	function UpdateDonor($iDonorId, $sDonorName, $sDonorCode, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[2] == 0)
			return(1010);
		
		$sDonorName = $objDatabase->RealEscapeString($sDonorName);
		$sDonorCode = $objDatabase->RealEscapeString($sDonorCode);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);

		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_donors SET
	
			DonorName='$sDonorName', 
			DonorCode='$sDonorCode', 
			Description='$sDescription', 
			Notes='$sNotes'
		WHERE DonorId='$iDonorId'");

		if ($varResult)
		{			
			$objSystemLog->AddLog("Update Donor - " . $sDonorName);
			$objGeneral->fnRedirect('?error=3203&id=' . $iDonorId);
		}
		else
			return(3204);
	}
	
	function DeleteDonor($iDonorId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors[3] == 0)
			return(1010);
				
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D WHERE D.DonorId='$iDonorId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(3005);

		$sDonorName = $objDatabase->Result($varResult, 0, "D.DonorName");
		// Check Donors usage
		if ($objDatabase->DBCount("fms_accounts_donors_projects AS DP", "DP.DonorId= '$iDonorId'") > 0) return(3207);

		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_donors WHERE DonorId='$iDonorId'");
		if ($varResult)
		{			
			$objSystemLog->AddLog("Delete Donor - " . $sDonorName);

			return(3205);
		}
		else
			return(3206);
	}

}

// Class: Project
class clsProjects extends clsDonors
{
	// Class Constructor
	public $aProjectStatus;
	function __construct()
	{
		$this->aProjectStatus = array("In-Active", "Active");
	}
	
	function ShowAllProjects($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
			
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Projects";
		if ($sSortBy == "") $sSortBy = "DP.DonorProjectId DESC";

		if ($sSearch != "")
		{
			$sSearchCondition = " AND ((DP.ProjectTitle LIKE '%$sSearch%') OR (DP.ProjectCode LIKE '%$sSearch%'))";
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_donors_projects AS DP INNER JOIN fms_accounts_donors AS D ON D.DonorId = DP.DonorId", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=donorprojects&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_donors_projects AS DP
		INNER JOIN fms_accounts_donors AS D ON D.DonorId = DP.DonorId
		WHERE 1=1 $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Donor Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=D.DonorCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Donor in Ascending Order" title="Sort by Donor in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=DP.DonorId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Donor in Descending Order" title="Sort by Donor in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Project Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=DP.ProjectTitle&sortorder="><img src="../images/sort_up.gif" alt="Sort by Project Title in Ascending Order" title="Sort by Project Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=DP.ProjectTitle&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Project Title in Descending Order" title="Sort by Project Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Project Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=DP.ProjectCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Project Code in Ascending Order" title="Sort by Project Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=DP.ProjectCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Project Code in Descending Order" title="Sort by Project Code in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Project Start Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=DP.ProjectStartDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Project Start Date in Ascending Order" title="Sort by Project Start Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=DP.ProjectStartDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Project Start Date in Descending Order" title="Sort by Project Start Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Project End Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=DP.ProjectEndDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Project End Date in Ascending Order" title="Sort by Project End Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=DP.ProjectEndDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Project End Date in Descending Order" title="Sort by Project End Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Project Budget&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=DP.ProjectBudget&sortorder="><img src="../images/sort_up.gif" alt="Sort by Project Budget in Ascending Order" title="Sort by Project Budget in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=DP.ProjectBudget&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Project Budget in Descending Order" title="Sort by Project Budget in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=DP.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=DP.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iDonorProjectId = $objDatabase->Result($varResult, $i, "DP.DonorProjectId");
			$sDonorCode = $objDatabase->Result($varResult, $i, "D.DonorCode");
			$sProjectTitle = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
			$sProjectCode = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
			$dProjectStartDate = $objDatabase->Result($varResult, $i, "DP.ProjectStartDate");
			$sProjectStartDate = date("F j, Y", strtotime($dProjectStartDate));
			$dProjectEndDate = $objDatabase->Result($varResult, $i, "DP.ProjectEndDate");
			$sProjectEndDate = date("F j, Y", strtotime($dProjectEndDate));
			$sProjectBudget = $objDatabase->Result($varResult, $i, "DP.ProjectBudget");
			$sDescription = $objDatabase->Result($varResult, $i, "DP.Description");
			$iStatus = $objDatabase->Result($varResult, $i, "DP.Status");
			$sStatus = $this->aProjectStatus[$iStatus];			

			$sEditProject = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Project' . $objDatabase->RealEscapeString(stripslashes($sProjectTitle)) . '\', \'../accounts/donorprojects_details.php?action2=edit&id=' . $iDonorProjectId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Project Details" title="Edit Project Details"></a></td>';
			$sDeleteProject = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Project?\')) {window.location = \'?action=DeleteProject&id=' . $iDonorProjectId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete Project" title="Delete Project"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[0] == 0)$sEditProject = '<td class="GridTD">&nbsp;</td>';				
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[0] == 0)$sDeleteProject = '<td class="GridTD">&nbsp;</td>';
			
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sDonorCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sProjectTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sProjectCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sProjectStartDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sProjectEndDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sProjectBudget . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $objDatabase->RealEscapeString(stripslashes($sProjectTitle)) . '\', \'../accounts/donorprojects_details.php?id=' . $iDonorProjectId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Project Details" title="View this Project Details"></a></td>
			' . $sEditProject . $sDeleteProject . '</tr>';
		}

		$sAddProject = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Project\', \'../accounts/donorprojects_details.php?action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Project">';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[1] == 0)
			$sAddProject = '';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddProject . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="../accounts/donorprojects_showall.php">Search for a Project:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Project" title="Search for Project" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Project Details" alt="View this Project Details"></td><td>View this Project Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Project Details" alt="Edit this Project Details"></td><td>Edit this Project Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Project" alt="Delete this Project"></td><td>Delete this Project</td></tr>
		</table>';

		return($sReturn);
	}
	
	function ProjectDetails($iDonorProjectId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();

		$varResult = $objDatabase->Query("
		SELECT *,DP.Notes AS Notes,DP.Description AS Description
		FROM fms_accounts_donors_projects AS DP
		INNER JOIN fms_accounts_donors AS D ON D.DonorId = DP.DonorId
		INNER JOIN organization_employees AS E ON E.EmployeeId = DP.DonorProjectAddedBy
		WHERE DP.DonorProjectId = '$iDonorProjectId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iDonorProjectId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Project" title="Edit this Project" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Project?\')) {window.location = \'?action=DeleteProject&id=' . $iDonorProjectId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Project" title="Delete this Project" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[2] == 0)
			$sButtons_Edit = '';
			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[3] == 0)
			$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Project Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iDonorProjectId = $objDatabase->Result($varResult, 0, "DP.DonorProjectId");
				$iDonorId = $objDatabase->Result($varResult, 0, "DP.DonorId");
				$sDonorName = $objDatabase->Result($varResult, 0, "D.DonorName");
				$sDonorCode = $objDatabase->Result($varResult, 0, "D.DonorCode");
				$sDonorName = $sDonorName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View ' . $sDonorName .'\' , \'../accounts/donors_details.php?id=' . $iDonorId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="View Donors Details" title="View Donors Details" /></a>';				
				$sProjectTitle = $objDatabase->Result($varResult, 0, "DP.ProjectTitle");
				$sProjectCode = $objDatabase->Result($varResult, 0, "DP.ProjectCode");
			
				$dProjectStartDate = $objDatabase->Result($varResult, 0, "DP.ProjectStartDate");
				$sProjectStartDate = date("F j, Y", strtotime($dProjectStartDate));
				$dProjectEndDate = $objDatabase->Result($varResult, 0, "DP.ProjectEndDate");
				$sProjectEndDate = date("F j, Y", strtotime($dProjectEndDate));
				$sProjectBudget = $objDatabase->Result($varResult, 0, "DP.ProjectBudget");

				$sDescription = $objDatabase->Result($varResult, 0, "Description");
				$iStatus = $objDatabase->Result($varResult, 0, "DP.Status");
				$sStatus = $this->aProjectStatus[$iStatus];
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
				$dDonorProjectAddedOn = $objDatabase->Result($varResult, 0, "DP.DonorProjectAddedOn");
				$sDonorProjectAddedOn = date("F j, Y", strtotime($dDonorProjectAddedOn)) . ' at ' . date("g:i a", strtotime($dDonorProjectAddedOn));
				$iDonorProjectAddedBy = $objDatabase->Result($varResult, 0, "DP.DonorProjectAddedBy");
				$sDonorProjectAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sDonorProjectAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName)) . '\', \'../organization/employees_details.php?id=' . $iDonorProjectAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sDonorName = '<select class="form1" name="selDonor" id="selDonor">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D WHERE 1=1 ORDER BY D.DonorName");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sDonorName .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . ' - ' . $objDatabase->Result($varResult, $i, "D.DonorName") . '</option>';
				}
				$sDonorName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selDonor\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDonor\'), \'../accounts/donors_details.php?id=\'+GetSelectedListBox(\'selDonor\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Donor Information" title="Donor Information" border="0" /></a>';
								
				$sProjectTitle = '<input type="text" name="txtProjectTitle" id="txtProjectTitle" class="form1" value="' . $sProjectTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sProjectCode = '<input type="text" name="txtProjectCode" id="txtProjectCode" class="form1" value="' . $sProjectCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sProjectStartDate  = '<input type="text" size="10" id="txtProjectStartDate" name="txtProjectStartDate" class="Tahoma14" value="' . $dProjectStartDate . '" />' . $objjQuery->Calendar('txtProjectStartDate') . '&nbsp;<span style="color:red;">*</span>';
				$sProjectEndDate  = '<input type="text" size="10" id="txtProjectEndDate" name="txtProjectEndDate" class="Tahoma14" value="' . $dProjectEndDate . '" />' . $objjQuery->Calendar('txtProjectEndDate') . '&nbsp;<span style="color:red;">*</span>';
				$sProjectBudget = '<input type="text" name="txtProjectBudget" id="txtProjectBudget" class="form1" value="' . $sProjectBudget . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aProjectStatus); $i++)				
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aProjectStatus[$i];				
				$sStatus .='</select>';
			}
			else if ($sAction2 == "addnew")
			{
				$iDonorProjectId = "";
				$sProjectTitle = $objGeneral->fnGet("txtProjectTitle");
				$sProjectCode = $objGeneral->fnGet("txtProjectCode");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$iStatus = 1;

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sDonorProjectAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sDonorProjectAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sDonorProjectAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sDonorName = '<select class="form1" name="selDonor" id="selDonor">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D WHERE 1=1 ORDER BY D.DonorName");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sDonorName .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . ' - ' . $objDatabase->Result($varResult, $i, "D.DonorName") . '</option>';
				}
				$sDonorName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selDonor\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDonor\'), \'../accounts/donors_details.php?id=\'+GetSelectedListBox(\'selDonor\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Donor Information" title="Donor Information" border="0" /></a>';
			
				$sProjectTitle = '<input type="text" name="txtProjectTitle" id="txtProjectTitle" class="form1" value="' . $sProjectTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sProjectCode = '<input type="text" name="txtProjectCode" id="txtProjectCode" class="form1" value="' . $sProjectCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sProjectStartDate  = '<input type="text" size="10" id="txtProjectStartDate" name="txtProjectStartDate" class="Tahoma14" value="' . $dProjectStartDate . '" />' . $objjQuery->Calendar('txtProjectStartDate') . '&nbsp;<span style="color:red;">*</span>';
				$sProjectEndDate  = '<input type="text" size="10" id="txtProjectEndDate" name="txtProjectEndDate" class="Tahoma14" value="' . $dProjectEndDate . '" />' . $objjQuery->Calendar('txtProjectEndDate') . '&nbsp;<span style="color:red;">*</span>';
				$sProjectBudget = '<input type="text" name="txtProjectBudget" id="txtProjectBudget" class="form1" value="' . $sProjectBudget . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';				
				$sDonorProjectAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aProjectStatus); $i++)				
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aProjectStatus[$i];				
				$sStatus .='</select>';
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if (GetVal(\'txtProjectTitle\') == "") return(AlertFocus(\'Please enter a valid Project Name\', \'txtProjectTitle\'));
				if (GetVal(\'txtProjectCode\') == "") return(AlertFocus(\'Please enter a valid Project Code\', \'txtProjectCode\'));

				return true;
			}
			</script> 
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Project Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200px;">Donor :</td><td><strong>' . $sDonorName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Project Title:</td><td><strong>' . $sProjectTitle . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Project Code:</td><td><strong>' . $sProjectCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Project Start Date:</td><td><strong>' . $sProjectStartDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Project End Date:</td><td><strong>' . $sProjectEndDate . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Project Budget:</td><td><strong>' . $sProjectBudget . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Status:</td><td><strong>' . $sStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Project Added On:</td><td><strong>' . $sDonorProjectAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Project Added By:</td><td><strong>' . $sDonorProjectAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Project" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iDonorProjectId . '"><input type="hidden" name="action" id="action" value="UpdateProject"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Project" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewProject"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewProject($iDonorId, $sProjectTitle, $sProjectCode, $sDescription, $iStatus, $sNotes, $dProjectStartDate, $dProjectEndDate, $dProjectBudget)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[1] == 0)
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		$iDonorProjectAddedBy = $objEmployee->iEmployeeId;
	
		$sProjectTitle = $objDatabase->RealEscapeString($sProjectTitle);
		$sProjectCode = $objDatabase->RealEscapeString($sProjectCode);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		// Check Project Codes exist
		if ($objDatabase->DBCount("fms_accounts_donors_projects AS DP", "DP.ProjectCode='$sProjectCode'") > 0) return(3217);
		
		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_donors_projects
		(DonorId, ProjectTitle, ProjectCode, Description, Status, Notes, DonorProjectAddedOn, DonorProjectAddedBy, ProjectStartDate, ProjectEndDate, ProjectBudget)
		VALUES ('$iDonorId', '$sProjectTitle', '$sProjectCode', '$sDescription', '$iStatus', '$sNotes', '$varNow', '$iDonorProjectAddedBy', '$dProjectStartDate', '$dProjectEndDate','$dProjectBudget')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.ProjectTitle='$sProjectTitle' AND DP.DonorProjectAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$objSystemLog->AddLog("Add New Project - " . $sProjectTitle);

				$objGeneral->fnRedirect('?error=3210&id=' . $objDatabase->Result($varResult, 0, "DP.DonorProjectId"));
			}
		}

		return(3211);
	}
	
	function UpdateProject ($iDonorProjectId, $iDonorId, $sProjectTitle, $sProjectCode, $sDescription, $iStatus, $sNotes, $dProjectStartDate, $dProjectEndDate, $dProjectBudget)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[2] == 0)
			return(1010);
		
		$sProjectTitle = $objDatabase->RealEscapeString($sProjectTitle);
		$sProjectCode = $objDatabase->RealEscapeString($sProjectCode);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		// Check Project Codes exist
		if ($objDatabase->DBCount("fms_accounts_donors_projects AS DP", "DP.DonorProjectId<>'$iDonorProjectId' AND DP.ProjectCode='$sProjectCode'") > 0) return(3218);

		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_donors_projects SET
	
			DonorId='$iDonorId', 
			ProjectTitle='$sProjectTitle', 
			ProjectCode='$sProjectCode', 
			Description='$sDescription', 
			Status='$iStatus', 
			Notes='$sNotes',
			ProjectStartDate = '$dProjectStartDate',
			ProjectEndDate = '$dProjectEndDate',
			ProjectBudget = '$dProjectBudget'
		WHERE DonorProjectId='$iDonorProjectId'");

		if ($varResult)
		{			
			$objSystemLog->AddLog("Update Project - " . $sProjectTitle);
			$objGeneral->fnRedirect('?error=3212&id=' . $iDonorProjectId);
			
		}
		else
			return(3213);
	}
	
	function DeleteProject($iDonorProjectId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_Projects[3] == 0)
			return(1010);
			
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.DonorProjectId='$iDonorProjectId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(3225);

		$sProjectTitle = $objDatabase->Result($varResult, 0, "DP.ProjectTitle");
		
		// Check Projects usage
		if ($objDatabase->DBCount("fms_accounts_donors_projects_activities AS PA", "PA.DonorProjectId= '$iDonorProjectId'") > 0) return(3216);

		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_donors_projects WHERE DonorProjectId='$iDonorProjectId'");
		if ($varResult)
		{			
			$objSystemLog->AddLog("Delete Project - " . $sProjectTitle);

			return(3214);
		}
		else
			return(3215);
	}

}

// Class: Project Activitites
class clsProjectActivities extends clsProjects
{
	// Class Constructor
	public $aActivityStatus;
	function __construct()
	{
		$this->aActivityStatus = array("In-Active", "Active");
	}
	
	function ShowAllProjectActivities($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Project Activities";
		if ($sSortBy == "") $sSortBy = "PA.ActivityId DESC";

		if ($sSearch != "")
		{
			$sSearchCondition = " AND ((PA.ActivityTitle LIKE '%$sSearch%') OR (PA.ActivityCode LIKE '%$sSearch%'))";
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_donors_projects_activities AS PA INNER JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId= PA.DonorProjectId", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=projectactivities&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_donors_projects_activities AS PA
		INNER JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId= PA.DonorProjectId
		WHERE 1=1 $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Project Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=DP.ProjectCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Project Code in Ascending Order" title="Sort by Project Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=DP.ProjectCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Project Code in Descending Order" title="Sort by Project Code in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Activity Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=PA.ActivityTitle&sortorder="><img src="../images/sort_up.gif" alt="Sort by Activity Title in Ascending Order" title="Sort by Activity Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=PA.ActivityTitle&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Activity Title in Descending Order" title="Sort by Activity Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Activity Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=PA.ActivityCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Activity Code in Ascending Order" title="Sort by Activity Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=PA.ActivityCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Activity Code in Descending Order" title="Sort by Activity Code in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=DP.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=DP.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iActivityId = $objDatabase->Result($varResult, $i, "PA.ActivityId");
			$sProjectCode = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
			$sActivityTitle = $objDatabase->Result($varResult, $i, "PA.ActivityTitle");
			$sActivityCode = $objDatabase->Result($varResult, $i, "PA.ActivityCode");
			$sDescription = $objDatabase->Result($varResult, $i, "PA.Description");
			$iStatus = $objDatabase->Result($varResult, $i, "PA.Status");
			$sStatus = $this->aActivityStatus[$iStatus];			

			$sEditActivity = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Activity' . $objDatabase->RealEscapeString(stripslashes($sActivityTitle)) . '\', \'../accounts/projectactivities_details.php?action2=edit&id=' . $iActivityId. '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Activity Details" title="Edit Activity Details"></a></td>';
			$sDeleteActivity = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Activity?\')) {window.location = \'?action=DeleteActivity&id=' . $iActivityId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete Activity" title="Delete Activity"></a></td>';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sProjectCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sActivityTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sActivityCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $objDatabase->RealEscapeString(stripslashes($sActivityTitle)) . '\', \'../accounts/projectactivities_details.php?id=' . $iActivityId. '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Activity Details" title="View this Activity Details"></a></td>
			' . $sEditActivity . $sDeleteActivity . '</tr>';
		}

		$sAddActivity = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Activity\', \'../accounts/projectactivities_details.php?action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Activity">';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[1] == 0)
			$sAddActivity = '';
			
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddActivity . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="../accounts/projectactivities_showall.php">Search for a Activity:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Activity" title="Search for Activity" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Activity Details" alt="View this Activity Details"></td><td>View this Activity Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Activity Details" alt="Edit this Activity Details"></td><td>Edit this Activity Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Activity" alt="Delete this Activity"></td><td>Delete this Activity</td></tr>
		</table>';

		return($sReturn);
	}
	
	function ProjectActivityDetails($iActivityId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *,PA.Notes AS Notes, PA.Description AS Description
		FROM fms_accounts_donors_projects_activities AS PA
		INNER JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId= PA.DonorProjectId
		INNER JOIN organization_employees AS E ON E.EmployeeId = PA.ActivityAddedBy
		WHERE PA.ActivityId = '$iActivityId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iActivityId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Project Activity" title="Edit this Project Activity" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Project Activity?\')) {window.location = \'?action=DeleteActivity&id=' . $iActivityId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Project Activity" title="Delete this Project Activity" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[2] == 0)$sButtons_Edit = '';			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[3] == 0)$sButtons_Delete = '';
			
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Project Activity Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iActivityId = $objDatabase->Result($varResult, 0, "PA.ActivityId");
				$iDonorProjectId = $objDatabase->Result($varResult, 0, "DP.DonorProjectId");
				$sProjectTitle = $objDatabase->Result($varResult, 0, "DP.ProjectTitle");				
				$sProjectTitle = $sProjectTitle . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View ' . $sProjectTitle .'\' , \'../accounts/donorprojects_details.php?id=' . $iDonorProjectId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="View Project Details" title="View Project Details" /></a>';				
				$sActivityTitle = $objDatabase->Result($varResult, 0, "PA.ActivityTitle");
				$sActivityCode = $objDatabase->Result($varResult, 0, "PA.ActivityCode");
				$sDescription = $objDatabase->Result($varResult, 0, "Description");
				$iStatus = $objDatabase->Result($varResult, 0, "PA.Status");
				$sStatus = $this->aActivityStatus[$iStatus];
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
				$dActivityAddedOn = $objDatabase->Result($varResult, 0, "PA.ActivityAddedOn");
				$sActivityAddedOn = date("F j, Y", strtotime($dActivityAddedOn)) . ' at ' . date("g:i a", strtotime($dActivityAddedOn));
				$iActivityAddedBy = $objDatabase->Result($varResult, 0, "PA.ActivityAddedBy");
				$sActivityAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sActivityAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName)) . '\', \'../organization/employees_details.php?id=' . $iActivityAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sProjectTitle = '<select class="form1" name="selDonorProject" id="selDonorProject">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE 1=1 ORDER BY DP.ProjectTitle");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sProjectTitle .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "DP.DonorProjectId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . ' - ' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '</option>';
				}
				$sProjectTitle .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selDonorProject\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDonorProject\'), \'../accounts/donorprojects_details.php?id=\'+GetSelectedListBox(\'selDonorProject\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Project Information" title="Project Information" border="0" /></a>';
								
				$sActivityTitle = '<input type="text" name="txtActivityTitle" id="txtActivityTitle" class="form1" value="' . $sActivityTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sActivityCode = '<input type="text" name="txtActivityCode" id="txtActivityCode" class="form1" value="' . $sActivityCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aActivityStatus); $i++)				
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aActivityStatus[$i];				
				$sStatus .='</select>';
			}
			else if ($sAction2 == "addnew")
			{
				$iActivityId = "";
				$sActivityTitle = $objGeneral->fnGet("txtActivityTitle");
				$sActivityCode = $objGeneral->fnGet("txtActivityCode");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$iStatus = 1;

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sActivityAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sActivityAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sActivityAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sProjectTitle = '<select class="form1" name="selDonorProject" id="selDonorProject">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE 1=1 ORDER BY DP.ProjectTitle");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sProjectTitle .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "DP.DonorProjectId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . ' - ' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '</option>';
				}
				$sProjectTitle .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selDonorProject\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDonorProject\'), \'../accounts/donorprojects_details.php?id=\'+GetSelectedListBox(\'selDonorProject\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Project Information" title="Project Information" border="0" /></a>';
				
				$sActivityTitle = '<input type="text" name="txtActivityTitle" id="txtActivityTitle" class="form1" value="' . $sActivityTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sActivityCode = '<input type="text" name="txtActivityCode" id="txtActivityCode" class="form1" value="' . $sActivityCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';				
				$ActivityAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aActivityStatus); $i++)				
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aActivityStatus[$i];				
				$sStatus .='</select>';
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if (GetVal(\'txtActivityTitle\') == "") return(AlertFocus(\'Please enter a valid Activity Name\', \'txtActivityTitle\'));
				if (GetVal(\'txtActivityCode\') == "") return(AlertFocus(\'Please enter a valid Activity Code\', \'txtActivityCode\'));

				return true;
			}
			</script> 
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Activity Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Project:</td><td><strong>' . $sProjectTitle . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" style="width:200px;">Activity Title:</td><td><strong>' . $sActivityTitle . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Activity Code:</td><td><strong>' . $sActivityCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Status:</td><td><strong>' . $sStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#ffffff"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Activity Added On:</td><td><strong>' . $sActivityAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Activity Added By:</td><td><strong>' . $sActivityAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Activity" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iActivityId. '"><input type="hidden" name="action" id="action" value="UpdateActivity"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Activity" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewActivity"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';


		return($sReturn);
	}
	
	function AddNewProjectActivity($iDonorProjectId, $sActivityTitle, $sActivityCode, $sDescription, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[1] == 0)
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		$iActivityAddedBy = $objEmployee->iEmployeeId;
	
		$sActivityTitle = $objDatabase->RealEscapeString($sActivityTitle);
		$sActivityCode = $objDatabase->RealEscapeString($sActivityCode);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		// Check Project Codes exist
		if ($objDatabase->DBCount("fms_accounts_donors_projects_activities AS PA", "PA.ActivityCode='$sActivityCode'") > 0) return(3227);

		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_donors_projects_activities
		(DonorProjectId, ActivityTitle, ActivityCode, Description, Status, Notes, ActivityAddedOn, ActivityAddedBy)
		VALUES ('$iDonorProjectId', '$sActivityTitle', '$sActivityCode', '$sDescription', '$iStatus', '$sNotes', '$varNow', '$iActivityAddedBy')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.ActivityTitle='$sActivityTitle' AND PA.ActivityAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$objSystemLog->AddLog("Add New Project Activity - " . $sActivityTitle);
				$objGeneral->fnRedirect('?error=3220&id=' . $objDatabase->Result($varResult, 0, "PA.ActivityId"));
			}
		}

		return(3221);
	}
	
	function UpdateProjectActivity($iActivityId, $iDonorProjectId, $sActivityTitle, $sActivityCode, $sDescription, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[2] == 0)
			return(1010);
		
		$sActivityTitle = $objDatabase->RealEscapeString($sActivityTitle);
		$sActivityCode = $objDatabase->RealEscapeString($sActivityCode);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		// Check Project Codes exist
		if ($objDatabase->DBCount("fms_accounts_donors_projects_activities AS PA", "PA.ActivityId<>'$iActivityId' AND PA.ActivityCode='$sActivityCode'") > 0) return(3228);

		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_donors_projects_activities 
		SET	
			DonorProjectId='$iDonorProjectId',
			ActivityTitle='$sActivityTitle', 
			ActivityCode='$sActivityCode', 
			Description='$sDescription', 
			Status='$iStatus', 
			Notes='$sNotes'
		WHERE ActivityId='$iActivityId'");

		if ($varResult)
		{			
			$objSystemLog->AddLog("Update Project Activity - " . $sActivityTitle);
			$objGeneral->fnRedirect('?error=3222&id=' . $iActivityId);			
		}
		else
			return(3223);
	}
	
	function DeleteProjectActivity($iActivityId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Donors_ProjectsActivities[3] == 0)
			return(1010);
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.ActivityId='$iActivityId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(3225);

		$sActivityTitle = $objDatabase->Result($varResult, 0, "PA.ActivityTitle");
		// Check Projects usage
		if ($objDatabase->DBCount("fms_accounts_generaljournal AS GJ", "GJ.ActivityId= '$iActivityId'") > 0) return(3226);

		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_donors_projects_activities WHERE ActivityId='$iActivityId'");
		if ($varResult)
		{			
			$objSystemLog->AddLog("Delete Project Activity - " . $sActivityTitle);
			
			return(3224);
		}
		else
			return(3225);
	}

}

?>
