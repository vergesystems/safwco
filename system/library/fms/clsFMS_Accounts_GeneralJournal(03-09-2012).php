<?php

// Class: GeneralJournal
class clsAccounts_GeneralJournal
{
	public $aEntryType;
	public $aPaymentType;
	public $aRecipient;
	public $aReceiptType;

	public $aStatus;
	public $aStatusType;

	// Class Constructor
	function __construct()
	{
		$this->aEntryType = array('Payment', 'Receipt', 'Journal Entry');
		$this->aPaymentType = array('Bank Payment', 'Cash Payment');
		$this->aRecipient = array('Vendor', 'Customer', 'Employee');
		$this->aReceiptType = array('Bank Receipt', 'Cash Receipt');
		$this->aBankPaymentType = array('CheckBook', 'Online Transfer', 'ATM');

		$this->aStatus = array('Posted', 'Initiated', 'Cancelled');
		$this->aStatusType = array(1, 0, 1);
		
		//$this->GetSeries(0, -1, 0);
	}
	
	function ShowAllGeneralJournal($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		// Employee Roles

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "General Journal";
		if ($sSortBy == "") $sSortBy = "GJ.TransactionDate DESC";
		
		//Advance Search
		$iView = $objGeneral->fnGet("view");
		if($iView == 1)
		{
			$sAdnacedSearch_VoucherNumber = $objGeneral->fnGet("txtVoucherNumber");
			$sAdnacedSearch_TransactionDateFrom = $objGeneral->fnGet("txtTransactionDateFrom");
			$sAdnacedSearch_TransactionDateTo = $objGeneral->fnGet("txtTransactionDateTo");
			$sAdnacedSearch_Amount = $objGeneral->fnGet("txtAmount");
			
			if($sAdnacedSearch_VoucherNumber != "") $sAdnacedSearchString = " AND (GJ.Reference = '$sAdnacedSearch_VoucherNumber')";
			if($sAdnacedSearch_Amount != "") $sAdnacedSearchString .= " AND (GJ.TotalDebit = '$sAdnacedSearch_Amount' OR GJ.TotalCredit = '$sAdnacedSearch_Amount')";
			if(($sAdnacedSearch_TransactionDateFrom != "")) 
				$sAdnacedSearchString .= " AND (GJ.TransactionDate = '$sAdnacedSearch_TransactionDateFrom')";
		}
		//End Advance Search
		
		if ($sSearch != "")
		{
			$sSearch = mysql_escape_string($sSearch);
			$sSearchCondition = " AND ((GJ.Reference LIKE '%$sSearch%') OR (GJ.GeneralJournalId LIKE '%$sSearch%') OR (GJ.TransactionDate LIKE '%$sSearch%') OR (GJ.CheckNumber LIKE '%$sSearch%') OR (GJ.TotalDebit LIKE '%$sSearch%') OR (GJ.Description LIKE '%$sSearch%'))";

			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}
		else $sSearchCondition = $sAdnacedSearchString;
		
		$iTotalRecords = $objDatabase->DBCount("fms_accounts_generaljournal AS GJ INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy", "1=1 AND GJ.IsDeleted ='0' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_generaljournal AS GJ
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
		WHERE 1=1 AND GJ.IsDeleted ='0' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Type&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.EntryType&sortorder="><img src="../images/sort_up.gif" alt="Sort by Entry Type in Ascending Order" title="Sort by Entry Type in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.EntryType&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Entry Type in Descending Order" title="Sort by Entry Type in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Transaction Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Transaction Date in Ascending Order" title="Sort by Transaction Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Transacation Date in Descending Order" title="Sort by Transaction Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Reference&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.Reference&sortorder="><img src="../images/sort_up.gif" alt="Sort by Reference in Ascending Order" title="Sort by Reference in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.Reference&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Reference in Descending Order" title="Sort by Reference in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.TotalDebit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Transaction Amount in Ascending Order" title="Sort by Transaction Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.TotalDebit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Transaction Amount in Descending Order" title="Sort by Transaction Amount in Descending Order" border="0" /></a></span></td>
		  <td width="6%" colspan="6"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");

			$iEntryType = $objDatabase->Result($varResult, $i, "GJ.EntryType");
			$sEntryType = $this->aEntryType[$iEntryType];
			$sReference = $objDatabase->Result($varResult, $i, "GJ.Reference");
			$dTotalDebit = $objDatabase->Result($varResult, $i, "GJ.TotalDebit");
			$dTotalCredit = $objDatabase->Result($varResult, $i, "GJ.TotalCredit");
			$sTotalDebit = number_format($dTotalDebit,2);
			$sTotalCredit = number_format($dTotalCredit,2);
			$dTransactionDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
			$dAddedOn = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalAddedOn");
			$sAddedOn = date("F j, Y", strtotime($dAddedOn));
			$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));

			if ($dTotalDebit == $dTotalCredit)
				$dTransactionAmount = $dTotalDebit;
			else
				$dTransactionAmount = 0;

			$sEditGeneralJournal = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . mysql_escape_string(str_replace('"', '', $sReference)) . '\', \'../accounts/generaljournal_details.php?action2=edit&id=' . $iGeneralJournalId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this General Journal Entry Details" title="Edit this General Journal Entry Details"></a></td>';
			$sDeleteGeneralJournal = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this General Journal Entry?\')) {window.location = \'?action=DeleteGeneralJournal&id=' . $iGeneralJournalId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this General Journal Entry" title="Delete this General Journal Entry"></a></td>';

			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[2] == 0)$sEditGeneralJournal = '';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[3] == 0)$sDeleteGeneralJournal = '';

			$sReceipt = '<td class="GridTD" align="center"><a href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&id=' . $iGeneralJournalId. '&txtReference=' . $sReference . '\', 800,600);"><img src="../images/icons/iconPrint2.gif" border="0" alt="Receipt" title="Receipt"></a></td>';
			$sDocuments = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/documents_show.php?componentname=Accounts_GeneralJournal&id=' . $iGeneralJournalId . '\', \'Documents of ' . mysql_escape_string(str_replace('"', '', $sReference)) . '\', \'700 420\');"><img src="../images/icons/save.gif" border="0" alt="General Journal Documents" title="General Journal Documents"></a></td>';

			if(strtolower(cAllowEditDeleteTransactions) != 'yes')
				$sEditGeneralJournal = $sDeleteGeneralJournal = '<td>&nbsp;</td>';

			$sChangeStatus = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/status_show.php?componentname=Accounts_GeneralJournal&id=' . $iGeneralJournalId . '\', \'Status Update for ' . mysql_escape_string(str_replace('"', '', $sReference)) . '\', \'700 420\');"><img src="../images/icons/tick.gif" border="0" alt="Update Status" title="Update Status"></a></td>';

			//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournal_Documents[0] == 0) // View Disabled
            //	$sDocuments = '<td>&nbsp;</td>';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sEntryType . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sTransactionDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sReference. '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . number_format($dTransactionAmount, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'General Jouranl Entry Details\', \'../accounts/generaljournal_details.php?id=' . $iGeneralJournalId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this General Journal Entry Details" title="View this General Journal Entry Details"></a></td>
			 ' . $sEditGeneralJournal . $sReceipt . $sChangeStatus . $sDocuments . $sDeleteGeneralJournal . '
			</tr>';
		}

		$sAddGeneralJournal = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add New Entry\', \'../accounts/generaljournal_details.php?action2=addnew\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add New">';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[1] == 0)
			$sAddGeneralJournal = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddGeneralJournal . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for General Journal Entry:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for General Journal Entry" title="Search for General Journal Entry" border="0"></form>
		  &nbsp;[&nbsp;<a href="#noanchor" onclick="ShowHideDiv(\'tableAdvanceSearch\');">Advanced Search</a>&nbsp;]</td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		  ' . $this->ShowAdvancedSearch('', 1) . '
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this General Journal Entry Details" alt="View this General Journal Entry Details"></td><td>View this General Journal Entry Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this General Journal Details" alt="Edit this General Journal Details"></td><td>Edit this General Journal Details</td></tr>		 
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this General Journal Entry" alt="Delete this General Journal Entry"></td><td>Delete this General Journal Entry</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconPrint2.gif" title="Receipt" alt="Receipt"></td><td>Receipt</td></tr>		 		 
		 <tr><td align="center" width="10"><img src="../images/icons/save.gif" title="General Journal Documents" alt="General Journal Documents"></td><td>General Journal Documents</td></tr>
		</table>';

		return($sReturn);
	}
	
	function ShowAdvancedSearch($sWidth = "70%", $iFormId)
    {	
    	global $objDatabase;
		
		$dTransactionDateFrom = date("Y-m-d");
		$dTransactionDateTo = date("Y-m-d");
		
	    // Calendar Control
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();
		
		$sTransactionDateFrom = '<input size="10" type="text" id="txtTransactionDateFrom" name="txtTransactionDateFrom" class="form1" value="" />' . $objjQuery->Calendar('txtTransactionDateFrom');
		$sTransactionDateTo = '<input size="10" type="text" id="txtTransactionDateTo" name="txtTransactionDateTo" class="form1" value="" />' . $objjQuery->Calendar('txtTransactionDateTo');

    	$sReturn = '<div id="divAdvancedSearch" name="divAdvancedSearch">
		 <form method="GET" action="">
		 <table border="0" cellspacing="0" cellpadding="3" width="40%" align="center" style="display:none;" id="tableAdvanceSearch">
		  <tr>
		   <td>
		    <fieldset><legend style="font-size:12px; font-weight:bold;">Advanced Search</legend>
		     <br />
		      <table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		       <tr><td align="left">Voucher Number:</td><td><input type="text" class="form1" name="txtVoucherNumber" id="txtVoucherNumber" /></td></tr>
			   <tr><td align="left">Amount:</td><td><input type="text" class="form1" name="txtAmount" id="txtAmount" /></td></tr>
		       <tr><td align="left">Transaction Date:</td><td>' . $sTransactionDateFrom . '</td></tr>
			  </table>
		     <br />
		     <div align="center"><input type="submit" value="Search" class="AdminFormButton1" /></div>
		    </fieldset>
		   </td>
		  </tr>
		 </table>
		 <input type="hidden" name="action" id="action" value="SearchTransaction" />
		 <input type="hidden" name="view" id="view" value="1" />
		 </form>
		</div>';

    	return($sReturn);
    }
	
	function GeneralJournalDetails($iGeneralJournalId, $sAction2, $bPopup = 0)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
				
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$iEmployeeId = $objEmployee->iEmployeeId;

		$iNoOfRowsInVoucher = $objEmployee->aSystemSettings['No_Of_Rows_In_Voucher'];
		// Array of Chart of Accounts
		include('../../system/library/fms/clsFMS_Accounts_ChartOfAccounts.php');
		$objChartOfAccounts = new clsChartOfAccounts();
		$aChartOfAccounts = $objChartOfAccounts->ChartOfAccountsList();
		
		include('../../system/library/fms/clsFMS_Vendors_Vendors.php');
		$objVendors = new clsVendors_Vendors();
		$aVendors = $objVendors->VendorsList();
		
		include('../../system/library/fms/clsFMS_Customers.php');
		$objCustomers = new clsCustomers_Customers();
		$aCustomers = $objCustomers->CustomersList();

		$aEmployees = $objEmployee->EmployeesList();
		
		//include('../../system/library/clsOrganization.php');
		//$objStations = new clsStations();
		//$aStations = $objStations->StationsList();
		
		//include('../../system/library/clsOrganization_Departments.php');
		//$objDepartments = new clsDepartments();
		//$aDepartments = $objDepartments->DepartmentsList();
		
		include('../../system/library/fms/clsFMS_Accounts_Budget.php');
		$objBudget = new clsFMS_Accounts_Budget();
		$aBudgets = $objBudget->BudgetsList();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();
		
		$objQuery = new clsjQuery($aChartOfAccounts);
			
		$objQueryVendor = new clsjQuery($aVendors);
		$objQueryCustomer = new clsjQuery($aCustomers);
		$objQueryEmployee = new clsjQuery($aEmployees);
		//$objQueryStation = new clsjQuery($aStations);
		//$objQueryDepartment = new clsjQuery($aDepartments);
		$objQueryBudget = new clsjQuery($aBudgets);
		
		// Donors / Projects & Activities
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorId");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$aDonors[$i][0] = $objDatabase->Result($varResult, $i, "D.DonorId");
			$aDonors[$i][1] = $objDatabase->Result($varResult, $i, "D.DonorCode");
			$aDonors[$i][2] = $objDatabase->Result($varResult, $i, "D.DonorName");
		}

		if ($objEmployee->objEmployeeRoles->iEmployeeRole_FMS_Accounts_AccessAllProjects == 1) // Can access All Projectts...
        {	
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP 
				INNER JOIN fms_accounts_donors AS D ON D.DonorId = DP.DonorId
				ORDER BY DP.DonorProjectId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{
					$aDonorProjects[$i][0] = $objDatabase->Result($varResult, $i, "DP.DonorId");
					$aDonorProjects[$i][1] = $objDatabase->Result($varResult, $i, "DP.DonorProjectId");
					$aDonorProjects[$i][2] = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
					$aDonorProjects[$i][3] = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
				}
		}else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP 						
			INNER JOIN organization_employees_donorprojects AS EDP ON EDP.DonorProjectId = DP.DonorProjectId
			INNER JOIN organization_employees AS E ON E.EmployeeId = EDP.EmployeeId 
			WHERE E.EmployeeId='$iEmployeeId' ORDER BY DP.DonorProjectId");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{
					$aDonorProjects[$i][0] = $objDatabase->Result($varResult, $i, "DP.DonorId");
					$aDonorProjects[$i][1] = $objDatabase->Result($varResult, $i, "DP.DonorProjectId");
					$aDonorProjects[$i][2] = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
					$aDonorProjects[$i][3] = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
				}
	
		}
	
		/*		
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP 
		INNER JOIN fms_accounts_donors AS D ON D.DonorId = DP.DonorId
		ORDER BY DP.DonorProjectId");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$aDonorProjects[$i][0] = $objDatabase->Result($varResult, $i, "DP.DonorId");
			$aDonorProjects[$i][1] = $objDatabase->Result($varResult, $i, "DP.DonorProjectId");
			$aDonorProjects[$i][2] = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
			$aDonorProjects[$i][3] = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
		}
		*/
		
		/*
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS DPA 
		INNER JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId = DPA.DonorProjectId
		INNER JOIN fms_accounts_donors AS D ON D.DonorId = DP.DonorId
		ORDER BY DPA.ActivityId");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$aDonorProjectsActivities[$i][0] = $objDatabase->Result($varResult, $i, "D.DonorId");
			$aDonorProjectsActivities[$i][1] = $objDatabase->Result($varResult, $i, "DP.DonorProjectId");
			$aDonorProjectsActivities[$i][2] = $objDatabase->Result($varResult, $i, "DPA.ActivityId");
			$aDonorProjectsActivities[$i][3] = $objDatabase->Result($varResult, $i, "DPA.ActivityCode");
			$aDonorProjectsActivities[$i][4] = $objDatabase->Result($varResult, $i, "DPA.ActivityTitle");
		}
		*/

		$k = 0;

		$varResult = $objDatabase->Query("
		SELECT *		
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
		LEFT JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId
		LEFT JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		LEFT JOIN fms_banking_bankcheckbooks AS BC ON BC.BankCheckBookId = GJ.BankCheckBookId
		WHERE GJ.GeneralJournalId = '$iGeneralJournalId' AND GJ.IsDeleted ='0'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iGeneralJournalId. '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this General Journal Entry" title="Edit this General Journal Entry" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this General Journal?\')) {window.location = \'?pagetype=details&action=DeleteDeleteGeneralJournal&id=' . $iGeneralJournalId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this General Journal Entry" title="Delete this General Journal Entry" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[2] == 0)	$sButtons_Edit = '';			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[3] == 0)	$sButtons_Delete = '';
				
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">General Journal Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iGeneralJournalId = $objDatabase->Result($varResult, 0, "GJ.GeneralJournalId");

				$iEntryType = $objDatabase->Result($varResult, 0, "GJ.EntryType");
				$iPaymentType = $objDatabase->Result($varResult, 0, "GJ.PaymentType");
				$sPaymentType = $this->aPaymentType[$iPaymentType];
				$iBankPaymentType = $objDatabase->Result($varResult, 0, "GJ.BankPaymentType");
				$sBankPaymentType = $this->aBankPaymentType[$iBankPaymentType];
								
				$iReceiptType = $objDatabase->Result($varResult, 0, "GJ.ReceiptType");
				$sReceiptType= $this->aReceiptType[$iReceiptType];				
				$sEntryType = $this->aEntryType[$iEntryType];
				
				$sReference = $objDatabase->Result($varResult, 0, "GJ.Reference");
				//$iSeries = $objDatabase->Result($varResult, 0, "GJ.Series");
												
				$dTransactionDate = $objDatabase->Result($varResult, 0, "GJ.TransactionDate");
				$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
				
				$iBankId = $objDatabase->Result($varResult, 0, "BA.BankId");
				$iBankAccountId = $objDatabase->Result($varResult, 0, "GJ.BankAccountId");
				if($iBankAccountId > 0)
				{
					$sBank = $objDatabase->Result($varResult, 0, "B.BankName");
					$sBankAccount = $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");
				}
				
				$iBankCheckBookId = $objDatabase->Result($varResult, 0, "GJ.BankCheckBookId");
				if($iBankCheckBookId > 0)
				{
					
					$sCheckPrefix = $objDatabase->Result($varResult, 0, "BC.CheckPrefix");
					$sBankCheckBook  = '<tr bgcolor="#efeff1" id="trCheckBook" style="display:visible;"><td valign="top">Check Book:</td><td><strong>' . $sCheckPrefix . '</strong></td></tr>';

					$sCheck = $objDatabase->Result($varResult, 0, "GJ.CheckNumber");
					$sCheckNumber = '<tr bgcolor="#ffffff" id="trCheckNumber" style="display:visible;"><td valign="top">Check Number:</td><td><strong>' . $sCheckPrefix . '&nbsp;&raquo;&nbsp;' . $sCheck . '</strong></td></tr>';
					$sReceiptType='';
				}
				
				if($iEntryType == 2) $sPaymentType = $sBankRow = $sReceiptType = "";				
				else if($iEntryType == 1) 
				{
					if($iReceiptType == 0)					
						$sBankRow = '<tr id="trBanks" style="display:visible;" bgcolor="#edeff1"><td valign="top">Bank:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sBank  . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankAccount . '&nbsp;&raquo;&nbsp;</td></tr></table></td></tr>';
											
					$sPaymentType = "";
				}
				
				else 
				{
					if($iPaymentType == 0)
						$sBankRow = '<tr id="trBanks" style="display:visible;" bgcolor="#edeff1"><td valign="top">Bank:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sBank  . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankAccount . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankPaymentType . '</td></tr></table></td></tr>';
					$sReceiptType = "";
				}
				if($iEntryType != 2) $sEntryType .= '&nbsp;&raquo;&nbsp;';
				
				$sEntryType .= $sPaymentType . $sReceiptType;
				// $sVendor . $sCustomer . $sEmployee
				
				$dTotalDebit = $objDatabase->Result($varResult, 0, "GJ.TotalDebit");
				$dTotalCredit = $objDatabase->Result($varResult, 0, "GJ.TotalCredit");
				$sDescription = $objDatabase->Result($varResult, 0, "GJ.Description");			
				
				$sNotes = $objDatabase->Result($varResult, 0, "GJ.Notes");
				$sNotes = mysql_escape_string($sNotes);
				$sNotes = stripslashes($sNotes);
				
				$dAddedOn = $objDatabase->Result($varResult, 0, "GJ.GeneralJournalAddedOn");
				$sAddedOn = date("F j, Y", strtotime($dAddedOn)) . ' at ' . date("g:i a", strtotime($dAddedOn));
				
				$iAddedBy = $objDatabase->Result($varResult, 0, "GJ.GeneralJournalAddedBy");
				$sAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(str_replace('"', '', $sAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				
				// General Journal Entries
				//AND CA.ChartOfAccountsCategoryId != '5'
				//LEFT JOIN organization_stations AS S ON S.StationId = GJE.StationId
				//LEFT JOIN organization_departments AS DPT ON DPT.DepartmentId = GJE.DepartmentId
				//LEFT JOIN fms_accounts_donors_projects_activities AS PA ON PA.ActivityId= GJE.DonorProjectActivityId
				//LEFT JOIN fms_vendors AS V ON V.VendorId = GJE.VendorId
				
				$varResult2 = $objDatabase->Query("SELECT *
				FROM fms_accounts_generaljournal_entries AS GJE
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId= GJE.GeneralJournalId
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= GJE.ChartOfAccountsId
				
				
				LEFT JOIN fms_accounts_budget AS BJ ON BJ.BudgetId = GJE.BudgetId
				LEFT JOIN fms_accounts_donors AS D ON D.DonorId= GJE.DonorId
				LEFT JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId= GJE.DonorProjectId
				
				LEFT JOIN organization_employees AS E ON E.EmployeeId = GJE.EmployeeId
				
				LEFT JOIN fms_customers AS C ON C.CustomerId= GJE.CustomerId
				WHERE GJE.GeneralJournalId= '$iGeneralJournalId' AND GJ.IsDeleted ='0'
				ORDER BY GJE.GeneralJournalEntryId");
						       	
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';
					
					$sDetail = $objDatabase->Result($varResult2, $i, "GJE.Detail");
					$sChartOfAccount = $objDatabase->Result($varResult2, $i, "CA.ChartOfAccountsCode");
					//$sStation = $objDatabase->Result($varResult2, $i, "S.StationCode");
					//$sDepartment = $objDatabase->Result($varResult2, $i, "DPT.DepartmentCode");
					$sBudget = $objDatabase->Result($varResult2, $i, "BJ.BudgetCode");
					$sDonor = $objDatabase->Result($varResult2, $i, "D.DonorCode");
					$sDonorProject = $objDatabase->Result($varResult2, $i, "DP.ProjectCode");
					//$sProjectActivity = $objDatabase->Result($varResult2, $i, "PA.ActivityCode");
					/*					
					$sRecipient = "";
					$iRecipient = $objDatabase->Result($varResult2, $i, "GJE.Recipient");
					if($iRecipient == 0)		// Vendor
						$sRecipient = 'Vendor &nbsp;&nbsp;' .  $objDatabase->Result($varResult2, $i, "V.VendorCode");
					else if($iRecipient == 1)		// Customer
						$sRecipient = 'Customer &nbsp;&nbsp;' .  $objDatabase->Result($varResult2, $i, "C.FirstName") . ' '. $objDatabase->Result($varResult2, $i, "C.LastName");
				    else if($iRecipient == 2)		// Employee
						$sRecipient = 'Employee &nbsp;&nbsp;' .  $objDatabase->Result($varResult2, $i, "E.FirstName") . ' '. $objDatabase->Result($varResult2, $i, "E.LastName");
					*/					
					
					$dDebit = $objDatabase->Result($varResult2, $i, "GJE.Debit");
					$dCredit = $objDatabase->Result($varResult2, $i, "GJE.Credit");
					$sDonor = $sDonor . ' - ' . $sDonorProject . ' - '. $sProjectActivity;
					
					$sGeneralJournalParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td>' . ($i+1) . '</td>
					 <td>' . $sChartOfAccount . '</td>
					 <!--<td>' . $sStation . '</td>-->
					 <!--<td>' . $sDepartment . '</td>-->
					 <td>' . $sBudget . '</td>
					 <td>' . $sDonor . '</td>
					 <!--<td>' . $sRecipient . '</td>-->
 					 <td>' . $sDetail . '</td>
					 <td align="right">' . number_format($dDebit, 0) . '</td>
					 <td align="right">' . number_format($dCredit, 0) . '</td>
					</tr>';
				}
				
				$sGeneralJournalEntries = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>
				  <td style="width:60px;"><span class="WhiteHeading">Chart of Accounts</span></td>
				  <!--<td style="width:100px;"><span class="WhiteHeading">Station</span></td>-->
				  <!--<td style="width:100px;"><span class="WhiteHeading">Department</span></td>-->
				  <td style="width:100px;"><span class="WhiteHeading">Budget</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Donor</span></td>
				  <!--<td style="width:100px;"><span class="WhiteHeading">Recipient</span></td>-->
				  <td align="left"><span class="WhiteHeading">Description</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Debit</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Credit</span></td>				  
				 </tr>
				' . $sGeneralJournalParticulars_Rows . '				
				<tr bgcolor="#ffffff">
				 <td align="right" colspan="8" style="font-size:13px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				 <td align="right" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:14px;">' . number_format($dTotalDebit, 0) . '</td>
				 <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:14px;">' . number_format($dTotalCredit, 0) . '</td>
				</tr>				
				</table>';
			}

			if ($sAction2 == "addnew")
			{				
				$iGeneralJournalId = "";
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$sReference = $objGeneral->fnGet("txtReference");
				$dAmount = $objGeneral->fnGet("txtAmount");				
				$dTransactionDate = $objGeneral->fnGet("txtTransactionDate");				
				$dBalance = $objGeneral->fnGet("txtBalance");
				$iStationId = $objGeneral->fnGet("selStation");
				
				$iEntryType = 2;
				$iPaymentType = 1;
				$iReceiptType = 1;
				if($dTransactionDate == '' ) $dTransactionDate = date("Y-m-d");
				
				$iEmployeeId = $objEmployee->iEmployeeId;
				
				$sReturn .= '<form id="frmEntry" name="frmEntry" method="post" action="" onsubmit="return ValidateForm();">';
				
				// Get Employee Restriction Amount
				$varResult = $objDatabase->Query("
				SELECT E.ApplyRestriction AS 'ApplyRestriction', E.RestrictionAmount AS 'RestrictionAmount' 
				FROM organization_employees AS E				
				WHERE 1=1 AND E.EmployeeId='$iEmployeeId'");
				$iApplyRestriction = $objDatabase->Result($varResult, 0, "ApplyRestriction");
				$dRestrictionAmount = $objDatabase->Result($varResult, 0, "RestrictionAmount");

				$sTransactionDate  = '<input type="text" size="10" id="txtTransactionDate" name="txtTransactionDate" class="Tahoma14" value="' . $dTransactionDate . '" />' . $objjQuery->Calendar('txtTransactionDate') . '&nbsp;<span style="color:red;">*</span>';
				$sBankAccount = '<div id="divBankAccount" style="display:none;"><select name="selBankAccount" id="selBankAccount" onchange="GetBankPaymentTypes();" class="Tahoma14"></select>';
				$sBankCheckBook = '<div id="divCheckBook" style="display:none;"><select name="selCheckBook" id="selCheckBook" onchange="GetNextAvailableCheck();" class="Tahoma14"></select>';
				
				$sCheckNumber = '<tr bgcolor="#ffffff" id="trCheckNumber" style="display:none;"><td valign="top">Check Number:</td><td>' . $sBankCheckBook . '&nbsp;&raquo;&nbsp;<input type="text" name="txtCheckNumber" id="txtCheckNumber" class="Tahoma14" value="" maxlength="15" /></td></tr>';
				
				$sBank = '<div id="divBanks" style="display:inline;"><select name="selBank" id="selBank" onchange="GetBankAccounts(GetSelectedListBox(\'selBank\'),0);" class="Tahoma14">
				<option value="0">Select Bank</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B ORDER BY B.BankName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sBank .= '<option ' . (($iBnkId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . '</option>';
				$sBank .='</select></div>';
				
				$sPaymentType = '<div id="divPaymentType" name="divPaymentType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selPaymentType" id="selPaymentType" class="Tahoma14" onchange="ChangePaymentType();">
				<option value="-1">Select Payment Type</option>';
				for($i = 0; $i < count($this->aPaymentType); $i++)
					$sPaymentType .= '<option value="' . $i . '">' . $this->aPaymentType[$i] . '</option>';
				$sPaymentType .='</select></div>';
				
				$sReceiptType = '<div id="divReceiptType" name="divReceiptType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selReceiptType" id="selReceiptType" class="Tahoma14" onchange="ChangeReceiptType();">
				<option value="-1">Select Receipt Type</option>';
				for($i = 0; $i < count($this->aReceiptType); $i++)
					$sReceiptType .= '<option value="' . $i . '">' . $this->aReceiptType[$i] . '</option>';
				$sReceiptType .='</select></div>';
				
				$sBankPaymentType = '<div id="divBankPaymentType" name="divBankPaymentType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selBankPaymentType" id="selBankPaymentType" class="Tahoma14" onchange="ChangeBankPaymentTypes(GetSelectedListBox(\'selBankPaymentType\'));">
				<option value="-1">Select Bank Payment Type</option>';
				for($i = 0; $i < count($this->aBankPaymentType); $i++)
					$sBankPaymentType .= '<option value="' . $i . '">' . $this->aBankPaymentType[$i] . '</option>';
				$sBankPaymentType .='</select></div>';
				
				$sBankRow = '<tr id="trBanks" style="display:none;" bgcolor="#edeff1"><td valign="top">Bank:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sBank  . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankAccount . '</td><td>' . $sBankPaymentType . '</td></tr></table></td></tr>';

				// Bank Accounts
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.Status='1'");
				$iNoOfBankAccounts = $objDatabase->RowsNumber($varResult);
				$sBankAccounts = 'var aBankAccounts = MultiDimensionalArray(' . $iNoOfBankAccounts . ', 4);';
				for ($i=0; $i < $iNoOfBankAccounts; $i++)
				{
					$sBankAccounts .= 'aBankAccounts[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BA.BankId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . '";';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "BA.AccountTitle") . '";';
				}
				
				// Bank Check Books
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BC WHERE BC.Status='0'");
				$iNoOfBankCheckBooks = $objDatabase->RowsNumber($varResult);
				$sBankCheckBooks = 'var aBankCheckBooks = MultiDimensionalArray(' . $iNoOfBankCheckBooks . ', 5);';
				for ($i=0; $i < $iNoOfBankCheckBooks; $i++)
				{
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BC.BankAccountId") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BC.BankCheckBookId") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BC.CheckPrefix") . '";';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][3] = ' . $objDatabase->Result($varResult, $i, "BC.CheckNumberStart") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][4] = ' . $objDatabase->Result($varResult, $i, "BC.CheckNumberEnd") . ';';
				}							
				
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_quickentries AS QE WHERE QE.Status='1' ORDER BY QE.EntryOrder");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sQuickEntriesList .= '<option value="' . $objDatabase->Result($varResult, $i, "QE.QuickEntryId") . '">' . $objDatabase->Result($varResult, $i, "QE.Title") . '</option>';

				$sQuickEntries = '<tr bgcolor="#edeff1"><td valign="top">Quick Entry:</td><td>
				    <select name="selQuickEntries" id="selQuickEntries" class="Tahoma14" onchange="LoadQuickEntry(GetSelectedListBox(\'selQuickEntries\'));">
				     <option value="0">Select Quick Entry</option>
					 ' . $sQuickEntriesList . '
				    </select></td></tr>';

				$sEntryType = '<table border="0" cellspacing="0" cellpadding="0"><tr><td><select name="selEntryType" id="selEntryType" class="Tahoma14" onchange="ChangeEntryType();">
				<option value="-1">Select Entry Type</option>';
				for($i = 0; $i < count($this->aEntryType); $i++)
					$sEntryType .= '<option value="' . $i . '">' . $this->aEntryType[$i] . '</option>';
				$sEntryType .='</select></td><td>' . $sPaymentType . '</td><td>' . $sReceiptType . '</td></tr></table>
				
				<script language="JavaScript" type="text/javascript">
				' . $sBankAccounts . '
				' . $sBankCheckBooks . '
				</script>';

				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="Tahoma14" value="' . $dAmount .'" />&nbsp;<span style="color:red;">*</span>';
				//$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sDescription = '<textarea name="txtDescription" id="txtDescription" class="Tahoma14" rows="10" cols="200">' . $sDescription . '</textarea>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="Tahoma14" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				$sAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
                $sAddedBy = $sAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(str_replace('"', '', $sAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$bFirst = true;
				$sDonor = $sDonorProject = $sProjectActivities = $sChartOfAccount = $sBudget = $sGeneralJournalParticulars_Rows = "";
				
				for ($k=0; $k < $iNoOfRowsInVoucher; $k++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = (($k <= 4) ? 'table-row' : 'none');			
					
					if ($k > 0) $bFirst = false;

					// Naveed : March 16, 2011 - Changed from AutoCompleteLocal to AutoCompleteLocalMultiple
					$sChartOfAccount[$k] = $objQuery->AutoCompleteLocalMultiple("ChartOfAccounts", "hdnChartOfAccountId_" . ($k+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					//$sStation = $objQueryStation->AutoCompleteLocalMultiple("Stations", "hdnStationId" . ($k+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					//$sDepartment = $objQueryDepartment->AutoCompleteLocalMultiple("Departments", "hdnDepartmentId" . ($k+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					$sBudget = $objQueryBudget->AutoCompleteLocalMultiple("Budgets", "hdnBudgetId" . ($k+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					
					//$sVendor = '<div id="divVendor' . ($k+1) . '" name="divVendor' . ($k+1) . '" style="display:none;">' . $objQueryVendor->AutoCompleteLocalMultiple("Vendors", "hdnVendorId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';
					//$sCustomer = '<div id="divCustomer' . ($k+1) . '" name="divCustomer' . ($k+1) . '" style="display:none;">' . $objQueryCustomer->AutoCompleteLocalMultiple("Customers", "hdnCustomerId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';
					//$sEmployee = '<div id="divEmployee' . ($k+1) . '" name="divEmployee' . ($k+1) . '" style="display:none;">' . $objQueryEmployee->AutoCompleteLocalMultiple("Employees", "hdnEmployeeId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';

					/*
					$sRecipient[$k] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selRecipient' . ($k+1) . '" id="selRecipient' . ($k+1) . '" class="Tahoma14" onchange="ChangeRecipient(GetSelectedListBox(\'selRecipient' . ($k+1) . '\'), ' . ($k+1) . ');">
					<option value="-1">Select Recipient</option>';
					for($i = 0; $i < count($this->aRecipient); $i++)
						$sRecipient[$k] .= '<option value="' . $i . '">' . $this->aRecipient[$i] . '</option>';
					$sRecipient[$k] .='</select></td><td>&raquo;</td><td>' . $sVendor . '</td><td>' . $sCustomer . '</td><td>' . $sEmployee . '</td></tr></table>';
					*/
					
					$sDonor[$k] = $this->GetDonorBoxes($k+1, $aDonors, $aDonorProjects, $aDonorProjectsActivities, $bFirst);

					$sGeneralJournalParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
					 <td valign="top" align="left" style="font-size:14px; font-family:Tahoma, Arial;">' . $sChartOfAccount[$k] . '</td>
					 <!--<td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sStation . '</td>-->
					 <!--<td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sDepartment . '</td>-->
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sBudget . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sDonor[$k] . '</td>
					 <!--<td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sRecipient[$k] . '</td>-->
					
 					 <td valign="top"><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtGeneralJournalParticulars_Detail_' . ($k+1) . '" id="txtGeneralJournalParticulars_Detail_' . ($k+1) . '" size="40%" /></td>
					 <td valign="top" align="center"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtGeneralJournalParticulars_Debit_' . ($k+1) . '" id="txtGeneralJournalParticulars_Debit_' . ($k+1) . '" size="10" /></td>
					 <td valign="top" align="center"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtGeneralJournalParticulars_Credit_' . ($k+1) . '" id="txtGeneralJournalParticulars_Credit_' . ($k+1) . '" size="10" /></td>
					 <td valign="top" align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
					</tr>';
				}
				
				$sGeneralJournalEntries = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>
				  <td style="width:60px;"><span class="WhiteHeading">Chart of Accounts</span></td>
				  <!--<td style="width:100px;"><span class="WhiteHeading">Station</span></td>-->
				  <!--<td style="width:100px;"><span class="WhiteHeading">Department</span></td>-->
				  <td style="width:100px;"><span class="WhiteHeading">Budget</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Donor</span></td>
				  <!--<td style="width:100px;"><span class="WhiteHeading">Recipient</span></td>-->
				  <td align="left"><span class="WhiteHeading">Description</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Debit</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Credit</span></td>
				  <td align="center">&nbsp;</td>
				 </tr>
				 ' . $sGeneralJournalParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="11">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="8" style="font-size:13px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
				  <td align="center" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divDebitTotalAmount" name="divDebitTotalAmount"></div></td>
				  <td align="center" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divCreditTotalAmount" name="divCreditTotalAmount"></div></td>
				 </tr>
				</table>
				<input type="hidden" name="hdnApplyRestriction" id="hdnApplyRestriction" value="' . $iApplyRestriction . '" /><input type="hidden" name="hdnRestrictionAmount" id="hdnRestrictionAmount" value="' . $dRestrictionAmount . '" />
				<input type="hidden" name="TotalDebit" id="TotalDebit" value="" /><input type="hidden" name="TotalCredit" id="TotalCredit" value="" />
				<input type="hidden" name="popup" id="popup" value="' . $bPopup . '" />
				<script language="JavaScript" type="text/javascript">
				 var iRowCounter = ' . ($k+1) . ';
				 var iRowVisible = 6;
				</script>';
			}
			else if ($sAction2 == "edit")
			{
				$iEmployeeId = $objEmployee->iEmployeeId;
				$sReturn .= '<form id="frmEntry" name="frmEntry" method="post" action="" onsubmit="return ValidateForm();">';

				// Get Employee Restriction Amount
				$varResult = $objDatabase->Query("
				SELECT E.ApplyRestriction AS 'ApplyRestriction', E.RestrictionAmount AS 'RestrictionAmount' 
				FROM organization_employees AS E				
				WHERE 1=1 AND E.EmployeeId='$iEmployeeId'");
				$iApplyRestriction = $objDatabase->Result($varResult, 0, "ApplyRestriction");
				$dRestrictionAmount = $objDatabase->Result($varResult, 0, "RestrictionAmount");
				

				$sTransactionDate  = '<input type="text" size="10" id="txtTransactionDate" name="txtTransactionDate" class="Tahoma14" value="' . $dTransactionDate . '" />' . $objjQuery->Calendar('txtTransactionDate') . '&nbsp;<span style="color:red;">*</span>';
				$sBankAccount = '<div id="divBankAccount" style="display:none;"><select name="selBankAccount" id="selBankAccount" onchange="GetBankPaymentTypes();" class="Tahoma14"></select>';
				$sBankCheckBook = '<div id="divCheckBook" style="display:none;"><select name="selCheckBook" id="selCheckBook" onchange="GetNextAvailableCheck();" class="Tahoma14"></select>';
				
				$sCheckNumber = '<tr bgcolor="#ffffff" id="trCheckNumber" style="display:none;"><td valign="top">Check Number:</td><td>' . $sBankCheckBook . '&nbsp;&raquo;&nbsp;<input type="text" name="txtCheckNumber" id="txtCheckNumber" class="Tahoma14" value="'. $sCheck . '" maxlength="15" /></td></tr>';
				
				$sBank = '<div id="divBanks" style="display:inline;"><select name="selBank" id="selBank" onchange="GetBankAccounts(GetSelectedListBox(\'selBank\'),0);" class="Tahoma14">
				<option value="0">Select Bank</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B ORDER BY B.BankName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sBank .= '<option ' . (($iBnkId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . '</option>';
				$sBank .='</select></div>';
				
				$sPaymentType = '<div id="divPaymentType" name="divPaymentType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selPaymentType" id="selPaymentType" class="Tahoma14" onchange="ChangePaymentType();">
				<option value="-1">Select Payment Type</option>';
				for($i = 0; $i < count($this->aPaymentType); $i++)
					$sPaymentType .= '<option value="' . $i . '">' . $this->aPaymentType[$i] . '</option>';
				$sPaymentType .='</select></div>';
				
				$sReceiptType = '<div id="divReceiptType" name="divReceiptType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selReceiptType" id="selReceiptType" class="Tahoma14" onchange="ChangeReceiptType();">
				<option value="-1">Select Receipt Type</option>';
				for($i = 0; $i < count($this->aReceiptType); $i++)
					$sReceiptType .= '<option value="' . $i . '">' . $this->aReceiptType[$i] . '</option>';
				$sReceiptType .='</select></div>';
				
				$sBankPaymentType = '<div id="divBankPaymentType" name="divBankPaymentType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selBankPaymentType" id="selBankPaymentType" class="Tahoma14" onchange="ChangeBankPaymentTypes(GetSelectedListBox(\'selBankPaymentType\'));">
				<option value="-1">Select Bank Payment Type</option>';
				for($i = 0; $i < count($this->aBankPaymentType); $i++)
					$sBankPaymentType .= '<option value="' . $i . '">' . $this->aBankPaymentType[$i] . '</option>';
				$sBankPaymentType .='</select></div>';
				
				$sBankRow = '<tr id="trBanks" style="display:none;" bgcolor="#edeff1"><td valign="top">Bank:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sBank  . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankAccount . '</td><td>' . $sBankPaymentType . '</td></tr></table></td></tr>';				

				// Bank Accounts
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.Status='1'");
				$iNoOfBankAccounts = $objDatabase->RowsNumber($varResult);
				$sBankAccounts = 'var aBankAccounts = MultiDimensionalArray(' . $iNoOfBankAccounts . ', 4);';
				for ($i=0; $i < $iNoOfBankAccounts; $i++)
				{
					$sBankAccounts .= 'aBankAccounts[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BA.BankId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . '";';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "BA.AccountTitle") . '";';
				}
				
				// Bank Check Books
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BC WHERE BC.Status='0'");
				$iNoOfBankCheckBooks = $objDatabase->RowsNumber($varResult);
				$sBankCheckBooks = 'var aBankCheckBooks = MultiDimensionalArray(' . $iNoOfBankCheckBooks . ', 5);';
				for ($i=0; $i < $iNoOfBankCheckBooks; $i++)
				{
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BC.BankAccountId") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BC.BankCheckBookId") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BC.CheckPrefix") . '";';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][3] = ' . $objDatabase->Result($varResult, $i, "BC.CheckNumberStart") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][4] = ' . $objDatabase->Result($varResult, $i, "BC.CheckNumberEnd") . ';';
				}							

				$sEntryType = '<table border="0" cellspacing="0" cellpadding="0"><tr><td><select name="selEntryType" id="selEntryType" class="Tahoma14" onchange="ChangeEntryType();">
				<option value="-1">Select Entry Type</option>';
				for($i = 0; $i < count($this->aEntryType); $i++)
					$sEntryType .= '<option value="' . $i . '">' . $this->aEntryType[$i] . '</option>';
				$sEntryType .='</select></td><td>' . $sPaymentType . '</td><td>' . $sReceiptType . '</td></tr></table>
				
				<script language="JavaScript" type="text/javascript">
				' . $sBankAccounts . '
				' . $sBankCheckBooks . '				
				</script>';
				
				//$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sDescription = '<textarea name="txtDescription" id="txtDescription" class="Tahoma14" rows="10" cols="300">' . $sDescription . '</textarea>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="Tahoma14" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				// General Journal Entries
				//AND CA.ChartOfAccountsCategoryId != '5'
				//LEFT JOIN organization_stations AS S ON S.StationId = GJE.StationId
				//LEFT JOIN organization_departments AS DPT ON DPT.DepartmentId = GJE.DepartmentId
				//LEFT JOIN fms_accounts_donors_projects_activities AS PA ON PA.ActivityId= GJE.DonorProjectActivityId
				//LEFT JOIN fms_vendors AS V ON V.VendorId = GJE.VendorId
				//LEFT JOIN fms_customers AS C ON C.CustomerId= GJE.CustomerId
				
				$varResult2 = $objDatabase->Query("SELECT *
				FROM fms_accounts_generaljournal_entries AS GJE
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId= GJE.GeneralJournalId
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= GJE.ChartOfAccountsId
				
				
				LEFT JOIN fms_accounts_budget AS BJ ON BJ.BudgetId = GJE.BudgetId
				LEFT JOIN fms_accounts_donors AS D ON D.DonorId= GJE.DonorId
				LEFT JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId= GJE.DonorProjectId
				
				LEFT JOIN organization_employees AS E ON E.EmployeeId = GJE.EmployeeId
				
				
				WHERE GJE.GeneralJournalId= '$iGeneralJournalId' AND GJ.IsDeleted ='0'
				ORDER BY GJE.GeneralJournalEntryId");
				
				$sGeneralJournalEntries = $sGeneralJournalParticulars_Rows = $sRecipient = "";
				$sDonor = $sDonorProject = $sProjectActivities = $sChartOfAccount = "";
				$sBudget = "";				
				$bFirst = true;
				
				$dTotalDebit = $dTotalCredit = 0;
				for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'table-row';
					
					$iChartOfAccountId = $objDatabase->Result($varResult2, $k, "CA.ChartOfAccountsId");
					$sChartOfAccountCode = $objDatabase->Result($varResult2, $k, "CA.ChartOfAccountsCode");
					//$iStationId = $objDatabase->Result($varResult2, $k, "GJE.StationId");					
					//$sStationCode = $objDatabase->Result($varResult2, $k, "S.StationCode");
					//$iDepartmentId = $objDatabase->Result($varResult2, $k, "GJE.DepartmentId");
					//$sDepartmentCode = $objDatabase->Result($varResult2, $k, "DPT.DepartmentCode");
					$iBudgetId = $objDatabase->Result($varResult2, $k, "GJE.BudgetId");
					$sBudgetCode = $objDatabase->Result($varResult2, $k, "BJ.BudgetCode");
					$iDonorId = $objDatabase->Result($varResult2, $k, "GJE.DonorId");
					$iDonorProjectId = $objDatabase->Result($varResult2, $k, "GJE.DonorProjectId");
					//$iDonorProjectActivityId = $objDatabase->Result($varResult2, $k, "GJE.DonorProjectActivityId");
					//$iRecipient = $objDatabase->Result($varResult2, $k, "GJE.Recipient");
					
					//$iVendorId = $objDatabase->Result($varResult2, $k, "GJE.VendorId");
					//$iCustomerId = $objDatabase->Result($varResult2, $k, "GJE.CustomerId");
					//$iEmployeeId = $objDatabase->Result($varResult2, $k, "GJE.EmployeeId");
										
					//$sVendorCode =	$objDatabase->Result($varResult2, $k, "V.VendorCode");
					//$sCustomerName = $objDatabase->Result($varResult2, $k, "C.FirstName") . ' '. $objDatabase->Result($varResult2, $k, "C.LastName");
				   // $sEmployeeName = $objDatabase->Result($varResult2, $k, "E.FirstName") . ' '. $objDatabase->Result($varResult2, $k, "E.LastName");	
					
					$sDetail = $objDatabase->Result($varResult2, $k, "GJE.Detail");
					$dDebit = $objDatabase->Result($varResult2, $k, "GJE.Debit");
					$dCredit = $objDatabase->Result($varResult2, $k, "GJE.Credit");
					
					if ($k > 0) $bFirst = false;
										
					$sChartOfAccount[$k] = $objQuery->AutoCompleteLocalMultiple("ChartOfAccounts", "hdnChartOfAccountId_" . ($k+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sChartOfAccountCode, $iChartOfAccountId);
					//$sStation = $objQueryStation->AutoCompleteLocalMultiple("Stations", "hdnStationId" . ($k+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sStationCode, $iStationId);
					//$sDepartment = $objQueryDepartment->AutoCompleteLocalMultiple("Departments", "hdnDepartmentId" . ($k+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sDepartmentCode, $iDepartmentId);
					$sBudget = $objQueryBudget->AutoCompleteLocalMultiple("Budgets", "hdnBudgetId" . ($k+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sBudgetCode, $iBudgetId);
										
					//$sVendor = '<div id="divVendor' . ($k+1) . '" name="divVendor' . ($k+1) . '" style="display: '.(($iRecipient == 0)? 'visible' : 'none'). ';">' . $objQueryVendor->AutoCompleteLocalMultiple("Vendors", "hdnVendorId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sVendorCode, $iVendorId) . '</div>';
					//$sCustomer = '<div id="divCustomer' . ($k+1) . '" name="divCustomer' . ($k+1) . '" style="display:'.(($iRecipient == 1)? 'visible' : 'none'). ';">' . $objQueryCustomer->AutoCompleteLocalMultiple("Customers", "hdnCustomerId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sCustomerName, $iCustomerId) . '</div>';
					//$sEmployee = '<div id="divEmployee' . ($k+1) . '" name="divEmployee' . ($k+1) . '" style="display:'.(($iRecipient == 2)? 'visible' : 'none'). ';">' . $objQueryEmployee->AutoCompleteLocalMultiple("Employees", "hdnEmployeeId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sEmployeeName, $iEmployeeId) . '</div>';
					
					/*
					$sRecipient[$k] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selRecipient' . ($k+1) . '" id="selRecipient' . ($k+1) . '" class="Tahoma14" onchange="ChangeRecipient(GetSelectedListBox(\'selRecipient' . ($k+1) . '\'), ' . ($k+1) . ');">
					<option value="-1">Select Recipient</option>';
					for($i = 0; $i < count($this->aRecipient); $i++)
						$sRecipient[$k] .= '<option '. (($iRecipient == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aRecipient[$i] . '</option>';
					$sRecipient[$k] .= '</select></td><td>&raquo;</td><td>' . $sVendor . '</td><td>' . $sCustomer . '</td><td>' . $sEmployee . '</td></tr></table>';
					*/
					
					/*					
					if($iDonorProjectId > 0)
					{
						$sDonorProject[$k] = '<div id="divDonorProject' . ($k+1) . '" style="display:visible;"><select name="selDonorProject' . ($k+1) . '" id="selDonorProject' . ($k+1) . '" onchange="GetProjectActivities(GetSelectedListBox(\'selDonorProject' . ($k+1) . '\'), \'selProjectActivity' . ($k+1) . '\', \'divProjectActivity' . ($k+1) . '\');" class="Tahoma14">
						<option value="0">Select Donor</option>';
						$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.DonorId = '$iDonorId' ORDER BY DP.ProjectTitle");
						for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
							$sDonorProject[$k] = $sDonorProject[$k] . '<option '. (($iDonorProjectId == $objDatabase->Result($varResult, $i, "DP.DonorProjectId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '</option>';
						$sDonorProject[$k] = $sDonorProject[$k] . '</select></div>';
						
					}
					else
					{
						$sDonorProject[$k] = '<div id="divDonorProject' . ($k+1) . '" style="display:none;"><select name="selDonorProject' . ($k+1) . '" id="selDonorProject' . ($k+1) . '" onchange="GetProjectActivities(GetSelectedListBox(\'selDonorProject' . ($k+1) . '\'), \'selProjectActivity' . ($k+1) . '\', \'divProjectActivity' . ($k+1) . '\');" class="Tahoma14"></select></div>';
					}
					if($iDonorProjectActivityId > 0)
					{
						$sProjectActivity[$k] = '<div id="divProjectActivity' . ($k+1) . '" style="display:visible;"><select name="selProjectActivity' . ($k+1) . '" id="selProjectActivity' . ($k+1) . '" class="Tahoma14">
						<option value="0">Select Donor</option>';
						$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.DonorProjectId = '$iDonorProjectId' ORDER BY PA.ActivityTitle");
						for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
							$sProjectActivity[$k] = $sProjectActivity[$k] . '<option '. (($iDonorProjectActivityId == $objDatabase->Result($varResult, $i, "PA.ActivityId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "PA.ActivityId") . '">' . $objDatabase->Result($varResult, $i, "PA.ActivityCode") . '</option>';
						$sProjectActivity[$k] = $sProjectActivity[$k] . '</select></div>';						
					}
					else
					{
						$sProjectActivity[$k] = '<div id="divProjectActivity' . ($k+1) . '" style="display:none;"><select name="selProjectActivity' . ($k+1) . '" id="selProjectActivity' . ($k+1) . '" class="Tahoma14"></select></div>';
					}
					
					$sDonor[$k] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selDonor' . ($k+1) . '" id="selDonor' . ($k+1) . '" onchange="GetDonorProjects(GetSelectedListBox(\'selDonor' . ($k+1) . '\'), \'selDonorProject' . ($k+1) . '\', \'selProjectActivity' . ($k+1) . '\', \'divDonorProject' . ($k+1) . '\');" class="Tahoma14">
					<option value="0">Select Donor</option>';
					$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
						$sDonor[$k] = $sDonor[$k] . '<option '. (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
					$sDonor[$k] = $sDonor[$k] . '</select></td><td>&raquo;</td><td>' . $sDonorProject[$k] . '</td><td>&raquo;</td><td>' . $sProjectActivity[$k] . '</td></tr></table>';
					*/
					
					$sDonor[$k] = $this->GetDonorBoxes($k+1, $aDonors, $aDonorProjects, $aDonorProjectsActivities, $bFirst, $iDonorId, $iDonorProjectId, $iDonorProjectActivityId);
					
					$sGeneralJournalParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
					 <td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sChartOfAccount[$k] . '</td>
					 <!--<td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sStation . '</td>-->
					 <!--<td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sDepartment . '</td>-->
					 <td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sBudget . '</td>
					 <td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sDonor[$k] . '</td>
					 <!--<td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sRecipient[$k] . '</td>-->
 					 <td valign="top" style="width:100px;"><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtGeneralJournalParticulars_Detail_' . ($k+1) . '" id="txtGeneralJournalParticulars_Detail_' . ($k+1) . '" value="'. $sDetail . '" size="40%" /></td>
					 <td valign="top" style="width:60px;" align="center"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtGeneralJournalParticulars_Debit_' . ($k+1) . '" id="txtGeneralJournalParticulars_Debit_' . ($k+1) . '" value="'. $dDebit . '" size="10" /></td>
					 <td valign="top" style="width:60px;" align="center"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtGeneralJournalParticulars_Credit_' . ($k+1) . '" id="txtGeneralJournalParticulars_Credit_' . ($k+1) . '" value="'. $dCredit . '" size="10" /></td>
					 <td valign="top" align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
					</tr>';
				}
				
				for ($j=$k; $j < $iNoOfRowsInVoucher; $j++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'none';										
					$bFirst = false;
					
					$sChartOfAccount[$j] = $objQuery->AutoCompleteLocalMultiple("ChartOfAccounts", "hdnChartOfAccountId_" . ($j+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					//$sStation = $objQueryStation->AutoCompleteLocalMultiple("Stations", "hdnStationId" . ($j+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					//$sDepartment = $objQueryDepartment->AutoCompleteLocalMultiple("Departments", "hdnDepartmentId" . ($j+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					$sBudget = $objQueryStation->AutoCompleteLocalMultiple("Budgets", "hdnBudgetId" . ($j+1), "width:60px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					
					//$sVendor = '<div id="divVendor' . ($j+1) . '" name="divVendor' . ($j+1) . '" style="display:none;">' . $objQueryVendor->AutoCompleteLocalMultiple("Vendors", "hdnVendorId" . ($j+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';
					//$sCustomer = '<div id="divCustomer' . ($j+1) . '" name="divCustomer' . ($j+1) . '" style="display:none;">' . $objQueryCustomer->AutoCompleteLocalMultiple("Customers", "hdnCustomerId" . ($j+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';
					//$sEmployee = '<div id="divEmployee' . ($j+1) . '" name="divEmployee' . ($j+1) . '" style="display:none;">' . $objQueryEmployee->AutoCompleteLocalMultiple("Employees", "hdnEmployeeId" . ($j+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';

					/*
					$sRecipient[$j] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selRecipient' . ($j+1) . '" id="selRecipient' . ($j+1) . '" class="Tahoma14" onchange="ChangeRecipient(GetSelectedListBox(\'selRecipient' . ($j+1) . '\'), ' . ($j+1) . ');">
					<option value="-1">Select Recipient</option>';
					for($i = 0; $i < count($this->aRecipient); $i++)
						$sRecipient[$j] .= '<option value="' . $i . '">' . $this->aRecipient[$i] . '</option>';
					$sRecipient[$j] .='</select></td><td>&raquo;</td><td>' . $sVendor . '</td><td>' . $sCustomer . '</td><td>' . $sEmployee . '</td></tr></table>';
					*/
					/*$sDonorProject[$j] = '<div id="divDonorProject' . ($j+1) . '" style="display:none;"><select name="selDonorProject' . ($j+1) . '" id="selDonorProject' . ($j+1) . '" onchange="GetProjectActivities(GetSelectedListBox(\'selDonorProject' . ($j+1) . '\'), \'selProjectActivity' . ($j+1) . '\', \'divProjectActivity' . ($j+1) . '\');" class="Tahoma14"></select></div>';
					$sProjectActivity[$j] = '<div id="divProjectActivity' . ($j+1) . '" style="display:none;"><select name="selProjectActivity' . ($j+1) . '" id="selProjectActivity' . ($j+1) . '" class="Tahoma14"></select></div>';
					
					$sDonor[$j] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selDonor' . ($j+1) . '" id="selDonor' . ($j+1) . '" onchange="GetDonorProjects(GetSelectedListBox(\'selDonor' . ($j+1) . '\'), \'selDonorProject' . ($j+1) . '\', \'selProjectActivity' . ($j+1) . '\', \'divDonorProject' . ($j+1) . '\');" class="Tahoma14">
					<option value="0">Select Donor</option>';					
					$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
						$sDonor[$j] .= '<option value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
					$sDonor[$j] .='</select></td><td>&raquo;</td><td>' . $sDonorProject[$j] . '</td><td>&raquo;</td><td>' . $sProjectActivity[$j] . '</td></tr></table>';
					*/
					
					$sDonor[$j] = $this->GetDonorBoxes($j+1, $aDonors, $aDonorProjects, $aDonorProjectsActivities,false);
									
					$sGeneralJournalParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
					 <td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sChartOfAccount[$j] . '</td>
					 <!--<td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sStation . '</td>-->
					 <!--<td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sDepartment . '</td>-->
					 <td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sBudget . '</td>
					 <td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sDonor[$j] . '</td>
					 <!--<td valign="top" align="center" style="width:60px; font-size:14px; font-family:Tahoma, Arial;">' . $sRecipient[$j] . '</td>-->
 					 <td valign="top" style="width:100px;"><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtGeneralJournalParticulars_Detail_' . ($j+1) . '" id="txtGeneralJournalParticulars_Detail_' . ($j+1) . '" size="40%" /></td>
					 <td valign="top" style="width:60px;" align="center"><input onkeyup="UpdateTotal(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtGeneralJournalParticulars_Debit_' . ($j+1) . '" id="txtGeneralJournalParticulars_Debit_' . ($j+1) . '" size="10" /></td>
					 <td valign="top" style="width:60px;" align="center"><input onkeyup="UpdateTotal(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtGeneralJournalParticulars_Credit_' . ($j+1) . '" id="txtGeneralJournalParticulars_Credit_' . ($j+1) . '" size="10" /></td>
					 <td valign="top" align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td>
					</tr>';
				}
								
				$sGeneralJournalEntries = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>
				  <td style="width:60px;"><span class="WhiteHeading">Chart of Accounts</span></td>
				  <!--<td style="width:60px;"><span class="WhiteHeading">Station</span></td>-->
				  <!--<td style="width:60px;"><span class="WhiteHeading">Department</span></td>-->
				  <td style="width:60px;"><span class="WhiteHeading">Budget</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Donor</span></td>
				  <!--<td style="width:100px;"><span class="WhiteHeading">Recipient</span></td>-->
				  <td align="left" style="width:100px;"><span class="WhiteHeading">Description</span></td>
				  <td style="width:60px;" align="center"><span class="WhiteHeading">Debit</span></td>
				  <td style="width:60px;" align="center"><span class="WhiteHeading">Credit</span></td>
				  <td style="width:16px;" align="center"></td>
				 </tr>
				 ' . $sGeneralJournalParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="11">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="8" style="font-size:13px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
				  <td align="center" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divDebitTotalAmount" name="divDebitTotalAmount">'. number_format($dTotalDebit, 0) . '</div></td>
				  <td align="center" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divCreditTotalAmount" name="divCreditTotalAmount">'. number_format($dTotalCredit, 0). '</div></td>
				  <td></td>
				 </tr>
				</table>
				<input type="hidden" name="hdnApplyRestriction" id="hdnApplyRestriction" value="' . $iApplyRestriction . '" /><input type="hidden" name="hdnRestrictionAmount" id="hdnRestrictionAmount" value="" />
				<input type="hidden" name="TotalDebit" id="TotalDebit" value="" /><input type="hidden" name="TotalCredit" id="TotalCredit" value="" />
				<input type="hidden" name="popup" id="popup" value="' . $bPopup . '" />
				<script language="JavaScript" type="text/javascript">
				 var iRowCounter = ' . ($j+1) . ';
				 var iRowVisible = ' . ($k+1) . ';
				 UpdateTotal();				
				</script>';				
			}			
			
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			var iVoucherEntries= '. $iNoOfRowsInVoucher. ';			
			function ValidateForm()
			{				
				if(GetSelectedListBox(\'selEntryType\') < 0) return(AlertFocus(\'Please select Entry Type\', \'selEntryType\'));
				if (!isDate(GetVal(\'txtTransactionDate\'))) return(AlertFocus(\'Please enter a valid Transaction Date\', \'txtTransactionDate\'));
				if (GetVal(\'TotalDebit\') != GetVal(\'TotalCredit\')) return(AlertFocus(\'Debit and Credit amount must be equal\', \'txtGeneralJournalParticulars_Debit_1\'));
				if ((GetVal(\'txtGeneralJournalParticulars_Debit_1\') == "") && (GetVal(\'txtGeneralJournalParticulars_Credit_1\') == "")) return(AlertFocus(\'You can not do any transaction without any single entry\', \'txtGeneralJournalParticulars_Debit_1\'));
				
				// Check Employees Restrictions
				iApplyRestriction = GetVal(\'hdnApplyRestriction\');
				if(iApplyRestriction == 1)
				{
					dRestrictionAmount = GetVal(\'hdnRestrictionAmount\');
					dTotalDebit = GetVal(\'TotalDebit\');
					if(dTotalDebit > dRestrictionAmount) return(AlertFocus(\'You can not post this transaction, because you are exceeding assigned transaction limit\', \'txtGeneralJournalParticulars_Debit_1\'));				
				}

				// Check chart of Accounts - hidden value
				for(j=1; j< iVoucherEntries; j++)
				{					
					if(GetVal("hdnChartOfAccountId_" + j + \'_value\')!= "")
					{						
						if (GetVal("hdnChartOfAccountId_" + j) == "") return(AlertFocus(\'Please select valid Chart Of Account\', "hdnChartOfAccountId_" + j + \'_value\'));
					}					
				}
				
				/*
				for(k=1; k< iVoucherEntries; k++)
				{					
					
					if(GetVal("hdnStationId" + k + \'_value\')!= "")
					{						
						if (GetVal("hdnStationId" + k) == "") return(AlertFocus(\'Please select valid Station Code\', "hdnStationId" + k + \'_value\'));
					}					

				}
				
				for(l=1; l < iVoucherEntries; l++)
				{					
					
					if(GetVal("hdnDepartmentId" + l + \'_value\')!= "")
					{						
						if (GetVal("hdnDepartmentId" + l) == "") return(AlertFocus(\'Please select valid Department Code\', "hdnDepartmentId" + l + \'_value\'));
					}					

				}
				*/
				
				if(confirm(\'Are you sure, you want to post the transaction?\'))
					return(true);
				
				return(false);
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">General Journal Information:</span></td></tr>			
			 ' . $sQuickEntries . '
			 <tr bgcolor="#ffffff"><td valign="top">Entry Type:</td><td><strong>' . $sEntryType . '</strong></td></tr>
			 ' . $sBankRow . ' '
			  . $sCheckNumber
			  . '
			 <tr bgcolor="#ffffff"><td valign="top">Reference:</td><td><strong>' . $sReference . '&nbsp;</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Transaction Date:</td><td><strong>' . $sTransactionDate . '</tr></td>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">General Journal Entries:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="2">' . $sGeneralJournalEntries . '</td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">Description:</td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Added On:</td><td><strong>' . $sAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Added By:</td><td><strong>' . $sAddedBy . '</strong></td></tr>
			</table>';
						
			if ($sAction2 == "edit")
			{
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iGeneralJournalId . '"><input type="hidden" name="action" id="action" value="UpdateGeneralJournal"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>
				<script language="JavaScript" type="text/javascript">
				iGeneralJournalId = GetVal("id");
				LoadGeneralJournal(iGeneralJournalId);
				</script>';
			}
			else if($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add New" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewGeneralJournal"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}		

		$sReturn .= '<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function GetDonorBoxes($iCounter, $aDonors, $aDonorProjects, $aDonorProjectActivities, $bFirst = true, $iDefaultDonorId = 0, $iDefaultProjectId = 0, $iDefaultActivityId = 0)
	{
		$sDonorProject = '<div id="divDonorProject' . $iCounter . '" style="display:none;"><select name="selDonorProject' . $iCounter . '" id="selDonorProject' . $iCounter . '" onchange="PopulateDonorProjectActivities(GetSelectedListBox(\'selDonorProject' . $iCounter . '\'), \'selProjectActivity' . $iCounter . '\', \'divProjectActivity' . $iCounter . '\');" class="Tahoma14"></select></div>';
		$sProjectActivity = '<div id="divProjectActivity' . $iCounter . '" style="display:none;"><select name="selProjectActivity' . $iCounter . '" id="selProjectActivity' . $iCounter . '" class="Tahoma14"></select></div>';
		
		if ($bFirst)
		{
			// Javascript Array of Donors
			for ($i=0; $i < count($aDonors); $i++)
				$sTempDonors .= 'aDonors[' . $i . '][0] = ' . $aDonors[$i][0] . ';aDonors[' . $i . '][1] = "' . $aDonors[$i][1] . '";aDonors[' . $i . '][2] = "' . $aDonors[$i][2] . '";';
			
			// Javascript Array of Donor Projects
			for ($i=0; $i < count($aDonorProjects); $i++)
				$sTempDonorProjects .= 'aDonorProjects[' . $i . '][0] = ' . $aDonorProjects[$i][0] . ';aDonorProjects[' . $i . '][1] = "' . $aDonorProjects[$i][1] . '";aDonorProjects[' . $i . '][2] = "' . $aDonorProjects[$i][2] . '";aDonorProjects[' . $i . '][3] = "' . $aDonorProjects[$i][3] . '";';

			// Javascript Array of Donor Project Activities
			for ($i=0; $i < count($aDonorProjectActivities); $i++)
				$sTempDonorProjectActivities .= 'aDonorProjectActivities[' . $i . '][0] = ' . $aDonorProjectActivities[$i][0] . ';aDonorProjectActivities[' . $i . '][1] = "' . $aDonorProjectActivities[$i][1] . '";aDonorProjectActivities[' . $i . '][2] = "' . $aDonorProjectActivities[$i][2] . '";aDonorProjectActivities[' . $i . '][3] = "' . $aDonorProjectActivities[$i][3] . '";aDonorProjectActivities[' . $i . '][4] = "' . $aDonorProjectActivities[$i][4] . '";';

			$sReturn = '<script language="JavaScript" type="text/javascript">
			 var aDonors = MultiDimensionalArray(' . count($aDonors) . ', 3);
			 var aDonorProjects = MultiDimensionalArray(' . count($aDonorProjects) . ', 4);
			 var aDonorProjectActivities = MultiDimensionalArray(' . count($aDonorProjectActivities) . ', 5);
			 ' . $sTempDonors . '
			 ' . $sTempDonorProjects . '
			 ' . $sTempDonorProjectActivities . '
			
			 function PopulateDonorProjects(iDonorId, selDonorProject, selDonorProjectActivity, divDonorProject)
			 {
				OptionsList_RemoveAll(selDonorProject);
				OptionsList_RemoveAll(selDonorProjectActivity);
				ShowDiv(divDonorProject);
				
				FillSelectBox(selDonorProject, "Select Project", 0);
				if(iDonorId > 0)
				{
					for (i = 0; i < aDonorProjects.length; i++)
					{
						if (aDonorProjects[i][0] == iDonorId)
							FillSelectBox(selDonorProject, aDonorProjects[i][2], aDonorProjects[i][1]);
					}
				}
			 }
			
			 function PopulateDonorProjectActivities(iDonorProjectId, selDonorProjectActivity, divDonorProjectActivity)
			 {
				OptionsList_RemoveAll(selDonorProjectActivity);
				ShowDiv(divDonorProjectActivity);

				FillSelectBox(selDonorProjectActivity, "Select Activity", 0);
				if(iDonorProjectId > 0)
				{
					for (i = 0; i < aDonorProjectActivities.length; i++)
					{
						if (aDonorProjectActivities[i][1] == iDonorProjectId)
							FillSelectBox(selDonorProjectActivity, aDonorProjectActivities[i][3], aDonorProjectActivities[i][2]);
					}
				}
			 }
			</script>';
		}

		$sReturn .= '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selDonor' . $iCounter . '" id="selDonor' . $iCounter . '" onchange="PopulateDonorProjects(GetSelectedListBox(\'selDonor' . $iCounter . '\'), \'selDonorProject' . $iCounter . '\', \'selProjectActivity' . $iCounter . '\', \'divDonorProject' . $iCounter . '\');" class="Tahoma14">
		<option value="0">Select Donor</option>';
		for($i = 0; $i < count($aDonors); $i++)
			$sReturn .= '<option ' . (($iDefaultDonorId == $aDonors[$i][0]) ? 'selected="true"' : '') . ' value="' . $aDonors[$i][0] . '">' . $aDonors[$i][1] . '</option>';
		$sReturn .='</select></td><td>&raquo;</td><td>' . $sDonorProject . '</td><td>&raquo;</td><td>' . $sProjectActivity . '</td></tr></table>';
		
		if ($iDefaultDonorId > 0)
		{
			$sReturn .= '<script language="JavaScript" type="text/javascript">
			document.getElementById("selDonor' . $iCounter . '").onchange();
			document.getElementById("selDonorProject' . $iCounter . '").value = ' . $iDefaultProjectId . ';
			document.getElementById("selDonorProject' . $iCounter . '").onchange();
			document.getElementById("selProjectActivity' . $iCounter . '").value = ' . $iDefaultActivityId . ';
			</script>';
		}
		
					
		return($sReturn);
	}
	
	function AJAX_LoadQuickEntry($iQuickEntryId)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_quickentries AS QE WHERE QE.QuickEntryId='$iQuickEntryId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$iQuickEntryId = $objDatabase->Result($varResult, 0, "QE.QuickEntryId");
			$iEntryType = $objDatabase->Result($varResult, 0, "QE.EntryType");
			$iPaymentType = $objDatabase->Result($varResult, 0, "QE.PaymentType");
			$iReceiptType = $objDatabase->Result($varResult, 0, "QE.ReceiptType");
			$iBankPaymentType = $objDatabase->Result($varResult, 0, "QE.BankPaymentType");
			$iBankId = $objDatabase->Result($varResult, 0, "QE.BankId");
			$iBankAccountId = $objDatabase->Result($varResult, 0, "QE.BankAccountId");
			$iBankCheckBookId = $objDatabase->Result($varResult, 0, "QE.BankCheckBookId");
			$iBudgetId = $objDatabase->Result($varResult, 0, "QE.BudgetId");
			$iStationId = $objDatabase->Result($varResult, 0, "QE.StationId");
			$iDepartmentId = $objDatabase->Result($varResult, 0, "QE.DepartmentId");
			$iDonorId = $objDatabase->Result($varResult, 0, "QE.DonorId");
			$iDonorProjectId = $objDatabase->Result($varResult, 0, "QE.DonorProjectId");
			$iDonorProjectActivityId = $objDatabase->Result($varResult, 0, "QE.DonorProjectActivityId");

    		$objResponse->addScript('document.getElementById("selEntryType").value = ' . $iEntryType . ';');
			$objResponse->addScript('document.getElementById("selBudget").value = ' . $iBudgetId . ';');
			$objResponse->addScript('ChangeEntryType();');
			$objResponse->addScript('document.getElementById("selStation").value = ' . $iStationId . ';');
			$objResponse->addScript('document.getElementById("selDepartment").value = ' . $iDepartmentId . ';');
			$objResponse->addScript('document.getElementById("selDonor").value = ' . $iDonorId . ';');
			$objResponse->addScript('GetDonorProjects(' . $iDonorId . ', ' . $iDonorProjectId . ');');
			$objResponse->addScript('GetProjectActivities(' . $iDonorProjectId . ', ' . $iDonorProjectActivityId . ');');
			
			// Payment
			if ($iEntryType == 0)
			{
				$objResponse->addScript('document.getElementById("selPaymentType").value = ' . $iPaymentType . ';');
				$objResponse->addScript('ChangePaymentType();');

				if ($iPaymentType == 0)
				{
					$objResponse->addScript('document.getElementById("selBank").value = ' . $iBankId . ';');
					$objResponse->addScript('GetBankAccounts(' . $iBankId . ', ' . $iBankAccountId . ');');
				}

				$objResponse->addScript('GetBankPaymentTypes();');
				$objResponse->addScript('document.getElementById("selBankPaymentType").value = ' . $iBankPaymentType . ';');
				$objResponse->addScript('ChangeBankPaymentTypes(' . $iBankPaymentType . ');');
				$objResponse->addScript('GetCheckBooks(' . $iBankAccountId . ', ' . $iBankCheckBookId . ');');
			}
			else		// Receipt
			{
				$objResponse->addScript('document.getElementById("selReceiptType").value = ' . $iReceiptType . ';');
				$objResponse->addScript('ChangeReceiptType();');

				if ($iPaymentType == 0)
				{
					$objResponse->addScript('document.getElementById("selBank").value = ' . $iBankId . ';');
					$objResponse->addScript('GetBankAccounts(' . $iBankId . ', ' . $iBankAccountId . ');');
				}

				$objResponse->addScript('GetBankPaymentTypes();');
			}

			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_quickentries_entries AS QEE 
			INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = QEE.ChartOfAccountsId
			LEFT JOIN organization_stations AS S ON S.StationId = QEE.StationId
			LEFT JOIN organization_departments AS DPT ON DPT.DepartmentId = QEE.DepartmentId
			LEFT JOIN fms_accounts_budget AS BJ ON BJ.BudgetId = QEE.BudgetId
			LEFT JOIN fms_accounts_donors AS D ON D.DonorId= QEE.DonorId
			LEFT JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId= QEE.DonorProjectId
			LEFT JOIN fms_accounts_donors_projects_activities AS PA ON PA.ActivityId= QEE.DonorProjectActivityId				
			LEFT  JOIN fms_vendors AS V ON V.VendorId = QEE.VendorId
			LEFT  JOIN fms_customers AS C ON C.CustomerId = QEE.CustomerId
			LEFT  JOIN organization_employees AS E ON E.EmployeeId = QEE.EmployeeId
			WHERE QEE.QuickEntryId='$iQuickEntryId'
			ORDER BY QEE.QuickEntryEntryId");
			
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iRecipientType = $objDatabase->Result($varResult, $i, "QEE.Recipient");
				
				$iChartOfAccountId = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsId");
				$sChartOfAccountCode = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode");
				$iStationId = $objDatabase->Result($varResult, $i, "QEE.StationId");					
				$sStationCode = $objDatabase->Result($varResult, $i, "S.StationCode");
				$iDepartmentId = $objDatabase->Result($varResult, $i, "QEE.DepartmentId");
				$sDepartmentCode = $objDatabase->Result($varResult, $i, "DPT.DepartmentCode");
				$iBudgetId = $objDatabase->Result($varResult, $i, "QEE.BudgetId");
				$sBudgetCode = $objDatabase->Result($varResult, $i, "BJ.BudgetCode");
				$iDonorId = $objDatabase->Result($varResult, $i, "QEE.DonorId");
				$iDonorProjectId = $objDatabase->Result($varResult, $i, "QEE.DonorProjectId");
				$iDonorProjectActivityId = $objDatabase->Result($varResult, $i, "QEE.DonorProjectActivityId");				
				$sDetails = $objDatabase->Result($varResult, $i, "QEE.Detail");
				
				$sDetails = str_replace('[PREVIOUS_MONTH]', date("F Y", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"))), $sDetails);

				$objResponse->addScript('SetVal("txtGeneralJournalParticulars_Detail_' . ($i+1) . '", "' . $sDetails . '");');
				$objResponse->addScript('SetVal("txtGeneralJournalParticulars_Debit_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "QEE.Debit") . '");');
				$objResponse->addScript('SetVal("txtGeneralJournalParticulars_Credit_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "QEE.Credit") . '");');
				
				$objResponse->addScript('SetVal("hdnChartOfAccountId_' . ($i+1) . '_value", "' . $sChartOfAccountCode . '");');
				$objResponse->addScript('SetVal("hdnChartOfAccountId_' . ($i+1) . '", "' . $iChartOfAccountId . '");');
				
				$objResponse->addScript('SetVal("hdnStationId' . ($i+1) . '_value", "' . $sStationCode . '");');
				$objResponse->addScript('SetVal("hdnStationId' . ($i+1) . '", "' . $iStationId . '");');
				
				$objResponse->addScript('SetVal("hdnDepartmentId' . ($i+1) . '_value", "' . $sDepartmentCode . '");');
				$objResponse->addScript('SetVal("hdnDepartmentId' . ($i+1) . '", "' . $iDepartmentId. '");');
				
				$objResponse->addScript('SetVal("hdnBudgetId' . ($i+1) . '_value", "' . $sBudgetCode. '");');
				$objResponse->addScript('SetVal("hdnBudgetId' . ($i+1) . '", "' . $iBudgetId. '");');
				if($iDonorId > 0)
				{
					$objResponse->addScript('document.getElementById("selDonor' . ($i+1) . '").value = ' . $iDonorId . ';');
					$objResponse->addScript('GetDonorProjects('. $iDonorId . ', "selDonorProject' . ($i+1) . '" , "selProjectActivity'. ($i+1) . '", "divDonorProject'. ($i+1) . '");');
					$objResponse->addScript('GetProjectActivities('. $iDonorProjectId . ', "selProjectActivity' . ($i+1) . '" , "divProjectActivity'. ($i+1) . '");');					
				}	
				
				if ($iRecipientType >= 0)
				{
					$objResponse->addScript('document.getElementById("selRecipient' . ($i+1) . '").value = ' . $iRecipientType . ';');
					$objResponse->addScript('ChangeRecipient('. $iRecipientType . ', ' . ($i+1) . ');');
					
					if ($iRecipientType == 0)	// Vendor
					{
						$iRecipientId = $objDatabase->Result($varResult, $i, "QEE.VendorId");
						$sRecipientName = $objDatabase->Result($varResult, $i, "V.VendorName");
					}
					else if ($iRecipientType == 1) // Customer
					{
						$iRecipientId = $objDatabase->Result($varResult, $i, "QEE.CustomerId");
						$sRecipientName = $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") . ' (' . $objDatabase->Result($varResult, $i, "C.CompanyName") . ')';
					}
					else if ($iRecipientType == 2) // Employee
					{
						$iRecipientId = $objDatabase->Result($varResult, $i, "QEE.EmployeeId");
						$sRecipientName = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
					}

					$objResponse->addScript('SetVal("txt' . $this->aRecipient[$iRecipientType] . 'Name' . ($i+1) . '", "' . $sRecipientName . '");');
					$objResponse->addScript('SetVal("hdn' . $this->aRecipient[$iRecipientType] . 'Id' . ($i+1) . '", "' . $iRecipientId . '");');
				}
				
				$objResponse->addScript('document.getElementById("selDonorProject' . ($i+1) . '").value = ' . $iDonorProjectId . ';');
				$objResponse->addScript('document.getElementById("selProjectActivity' . ($i+1) . '").value = ' . $iDonorProjectActivityId . ';');
				
				$objResponse->addScript('UpdateTotal();');
			}
		}

		return($objResponse->getXML());
	}
	
	function AJAX_LoadGeneralJournal($iGeneralJournalId)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal AS GJ 
		LEFT JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		WHERE GJ.GeneralJournalId='$iGeneralJournalId' AND GJ.IsDeleted ='0'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			//$iSeries = $objDatabase->Result($varResult, 0, "GJ.Series");
			$iGeneralJournalId = $objDatabase->Result($varResult, 0, "GJ.GeneralJournalId");
			$iEntryType = $objDatabase->Result($varResult, 0, "GJ.EntryType");
			$iPaymentType = $objDatabase->Result($varResult, 0, "GJ.PaymentType");
			$iReceiptType = $objDatabase->Result($varResult, 0, "GJ.ReceiptType");
			$iBankPaymentType = $objDatabase->Result($varResult, 0, "GJ.BankPaymentType");
			$sReference = $objDatabase->Result($varResult, 0, "GJ.Reference");
			$iSeries = $objDatabase->Result($varResult, 0, "GJ.Series");
			$iBankId = $objDatabase->Result($varResult, 0, "BA.BankId");
			$iBankAccountId = $objDatabase->Result($varResult, 0, "GJ.BankAccountId");
			$iBankCheckBookId = $objDatabase->Result($varResult, 0, "GJ.BankCheckBookId");
			$sCheckNumber = $objDatabase->Result($varResult, 0, "GJ.CheckNumber");
			
    		$objResponse->addScript('document.getElementById("selEntryType").value = ' . $iEntryType . ';');
			$objResponse->addScript('ChangeEntryType();');
			/*
			$iBudgetId = $objDatabase->Result($varResult, 0, "GJ.BudgetId");
			$iDonorId = $objDatabase->Result($varResult, 0, "GJ.DonorId");
			$iDonorProjectId = $objDatabase->Result($varResult, 0, "GJ.DonorProjectId");
			$iDonorProjectActivityId = $objDatabase->Result($varResult, 0, "GJ.DonorProjectActivityId");
			$objResponse->addScript('document.getElementById("selBudget").value = ' . $iBudgetId . ';');
			$objResponse->addScript('document.getElementById("selDonor").value = ' . $iDonorId . ';');
			$objResponse->addScript('GetDonorProjects(' . $iDonorId . ', ' . $iDonorProjectId . ');');
			$objResponse->addScript('GetProjectActivities(' . $iDonorProjectId . ', ' . $iDonorProjectActivityId . ');');
			*/
			$sVoucherType = (($iEntryType == 2) ? substr($sReference, 0, 2) : substr($sReference, 0, 3));
			
			// Payment
			if ($iEntryType == 0)
			{
				$objResponse->addScript('document.getElementById("selPaymentType").value = ' . $iPaymentType . ';');
				$objResponse->addScript('ChangePaymentType();');
				
				if ($iPaymentType == 0)
				{					
					$objResponse->addScript('document.getElementById("selBank").value = ' . $iBankId . ';');
					$objResponse->addScript('GetBankAccounts(' . $iBankId . ', ' . $iBankAccountId . ');');
				}

				$objResponse->addScript('GetBankPaymentTypes();');
				$objResponse->addScript('document.getElementById("selBankPaymentType").value = ' . $iBankPaymentType . ';');
				$objResponse->addScript('ChangeBankPaymentTypes(' . $iBankPaymentType . ');');
				$objResponse->addScript('GetCheckBooks(' . $iBankAccountId . ', ' . $iBankCheckBookId . ');');
				$objResponse->addScript('document.getElementById("txtCheckNumber").value = ' . $sCheckNumber. ';');
			}
			else		// Receipt
			{
				$objResponse->addScript('document.getElementById("selReceiptType").value = ' . $iReceiptType  . ';');
				$objResponse->addScript('ChangeReceiptType();');

				if ($iReceiptType == 0)
				{
					$objResponse->addScript('document.getElementById("selBank").value = ' . $iBankId . ';');
					$objResponse->addScript('GetBankAccounts(' . $iBankId . ', ' . $iBankAccountId . ');');
				}

				$objResponse->addScript('GetBankPaymentTypes();');
			}
			/*
			$objResponse->addScript('SetVal("txtReference", "' . $sVoucherType . '");');
			$objResponse->addScript('SetVal("txtSeries", "' . $iSeries . '");');
			*/
			
			/*
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_entries AS GJE 
			INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
			LEFT  JOIN fms_vendors AS V ON V.VendorId = GJE.VendorId
			LEFT  JOIN fms_customers AS C ON C.CustomerId = GJE.CustomerId
			LEFT  JOIN organization_employees AS E ON E.EmployeeId = GJE.EmployeeId
			WHERE GJE.GeneralJournalId='$iGeneralJournalId'
			ORDER BY GJE.GeneralJournalEntryId");
			
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iRecipientType = $objDatabase->Result($varResult, $i, "GJE.Recipient");
				$sDetails = $objDatabase->Result($varResult, $i, "GJE.Detail");
				
				$sDetails = str_replace('[PREVIOUS_MONTH]', date("F Y", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"))), $sDetails);

				$objResponse->addScript('SetVal("txtGeneralJournalParticulars_Detail_' . ($i+1) . '", "' . $sDetails . '");');
				$objResponse->addScript('SetVal("txtGeneralJournalParticulars_Debit_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "GJE.Debit") . '");');
				$objResponse->addScript('SetVal("txtGeneralJournalParticulars_Credit_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "GJE.Credit") . '");');
				
				$objResponse->addScript('SetVal("txtChartOfAccount_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "CA.AccountTitle") . ' ' . $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode") . '");');
				$objResponse->addScript('SetVal("hdnChartOfAccountId_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "GJE.ChartOfAccountsId") . '");');
				
				if ($iRecipientType >= 0)
				{
					$objResponse->addScript('document.getElementById("selRecipient' . ($i+1) . '").value = ' . $iRecipientType . ';');
					$objResponse->addScript('ChangeRecipient('. $iRecipientType . ', ' . ($i+1) . ');');
					
					if ($iRecipientType == 0)	// Vendor
					{
						$iRecipientId = $objDatabase->Result($varResult, $i, "GJE.VendorId");
						$sRecipientName = $objDatabase->Result($varResult, $i, "V.VendorName");
					}
					else if ($iRecipientType == 1) // Customer
					{
						$iRecipientId = $objDatabase->Result($varResult, $i, "GJE.CustomerId");
						$sRecipientName = $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") . ' (' . $objDatabase->Result($varResult, $i, "C.CompanyName") . ')';
					}
					else if ($iRecipientType == 2) // Employee
					{
						$iRecipientId = $objDatabase->Result($varResult, $i, "GJE.EmployeeId");
						$sRecipientName = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
					}

					$objResponse->addScript('SetVal("txt' . $this->aRecipient[$iRecipientType] . 'Name' . ($i+1) . '", "' . $sRecipientName . '");');
					$objResponse->addScript('SetVal("hdn' . $this->aRecipient[$iRecipientType] . 'Id' . ($i+1) . '", "' . $iRecipientId . '");');
				}
				
				$objResponse->addScript('UpdateTotal();');
			}
			*/
		}

		return($objResponse->getXML());
	}
	
	function GetSeries($iEntryType, $iReceiptType = -1, $iPaymentType = -1, $iBankAccountId = -1, $iProjectId = -1, $iStationId = -1)
	{
		global $objDatabase;
		
		$iSeries = 0;
		$varResult = $objDatabase->Query("SELECT MAX(GJ.Series) AS 'SeriesNumber' FROM fms_accounts_generaljournal AS GJ WHERE GJ.EntryType='$iEntryType' AND GJ.PaymentType='$iPaymentType' AND GJ.ReceiptType='$iReceiptType'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$iSeries = $objDatabase->Result($varResult, 0, "SeriesNumber");
		}
			
		die(' >> ' . $iSeries);
	}
	
	function AJAX_GetSeries($iEntryType, $iReceiptType = -1, $iPaymentType = -1, $iBankAccountId = -1, $iDonorProjectId = -1)
	{
		global $objDatabase;
	   	
		//$objResponse = new xajaxResponse();
		
		$sReciptTypeCondition = "";
		$sPaymentTypeCondition = "";
		$sBankAccountCondition = "";
				
		// Set Conditions here
		if($iReceiptType != -1) $sReciptTypeCondition = "AND (GJS.ReceiptType = '$iReceiptType')";
		if($iPaymentType != -1) $sPaymentTypeCondition = "AND (GJS.PaymentType = '$iPaymentType')";
		if($iBankAccountId != -1) $sBankAccountCondition = "AND (GJS.BankAccountId = '$iBankAccountId')";
		if($iDonorProjectId != -1) $sDonorProjectCondition = "AND (GJS.DonorProjectId = '$iDonorProjectId')";
						
		$varResult = $objDatabase->Query("SELECT GJS.Series AS 'LastNumber' FROM fms_accounts_generaljournal_series AS GJS WHERE GJS.EntryType='$iEntryType' $sReciptTypeCondition $sPaymentTypeCondition $sBankAccountCondition $sDonorProjectCondition");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$iLastNumber = $objDatabase->Result($varResult, 0, "LastNumber");
			$iNextNumber = $iLastNumber + 1;			
    		//$objResponse->addScript('SetVal("txtSeries", "' . $iNextNumber . '");');
		}
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_series AS GJS WHERE GJS.EntryType='$iEntryType' AND GJS.PaymentType='$iPaymentType' AND GJS.ReceiptType='$iReceiptType' AND GJS.BankAccountId='$iBankAccountId' AND GJS.DonorProjectId='$iDonorProjectId'");
			if ($objDatabase->RowsNumber($varResult) <= 0)
			{
				if ($iReceiptType == -1) $iReceiptType = 0;
				if ($iPaymentType == -1) $iPaymentType = 0;
				if ($iBankAccountId == -1) $iBankAccountId = 0;
				if ($iDonorProjectId == -1) $iDonorProjectId = 0;
				
				$varResult = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_series (EntryType, PaymentType, ReceiptType, BankAccountId, DonorProjectId, Series) VALUE ('$iEntryType', '$iPaymentType', '$iReceiptType', '$iBankAccountId', '$iDonorProjectId', '0')");
			}
			
			$iNextNumber = 1;															
    		//$objResponse->addScript('SetVal("txtSeries", "' . $iNextNumber . '");');			
		}

		return($iNextNumber);
		//return($objResponse->getXML());
	}
	
	function AJAX_GetNextAvailableCheck($iCheckBookId)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		$iNextCheckNumber = $this->FindNextAvailableCheck($iCheckBookId);
		$objResponse->addScript('SetVal("txtCheckNumber", "' . $iNextCheckNumber . '");');
		return($objResponse->getXML());
		
	}
	
	// To get Next Available Check
	function FindNextAvailableCheck($iBankCheckBookId)
	{
		global $objDatabase;
		$iNextCheckNumber = 0;
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BC WHERE BC.BankCheckBookId='$iBankCheckBookId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Book...');
		
		$iCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
		$iCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");
		
		$iNextCheckNumber = $iCheckNumberStart;
		
		$varResult = $objDatabase->Query("SELECT MAX(GJ.CheckNumber) AS 'BankCheckNumber' FROM fms_accounts_generaljournal AS GJ WHERE GJ.BankCheckBookId='$iBankCheckBookId'");
		if ($objDatabase->RowsNumber($varResult) > 0) 
			$iLastCheckNumberUsed = $objDatabase->Result($varResult, 0, "BankCheckNumber");
		
		if (($iLastCheckNumberUsed >= $iCheckNumberStart) && ($iLastCheckNumberUsed <= $iCheckNumberEnd))
			$iNextCheckNumber = $iLastCheckNumberUsed+1;
		
		$varResult = $objDatabase->Query("SELECT MAX(BCC.CancelledCheckNumber) AS 'BankCheckNumber' FROM fms_banking_bankcheckbooks_cancelledchecks AS BCC WHERE BCC.BankCheckBookId='$iBankCheckBookId'");
		if ($objDatabase->RowsNumber($varResult) > 0) 
			$iLastCheckNumberCancelled = $objDatabase->Result($varResult, 0, "BankCheckNumber");
			
		if ($iLastCheckNumberCancelled > $iLastCheckNumberUsed)
			$iNextCheckNumber = $iLastCheckNumberCancelled+1;
		
		return($iNextCheckNumber);
	}
	
	function AJAX_GetDonorProjects($iDonorId, $sTargetSelectBox)
	{
	   	$objResponse = new xajaxResponse();		
	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
	   	$aData = $this->GetDonorProjects($iDonorId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		return($objResponse->getXML());
	}
	
	function GetDonorProjects($iDonorId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_donors_projects AS DP
		WHERE DP.DonorId='$iDonorId'");		
		$aResult[0][0] = 0;
		$aResult[0][1] = "Select Project";
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{						
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "DP.DonorProjectId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
		}
		return($aResult);
	}
	
	function AJAX_GetProjectActivities($iDonorProjectId, $sTargetSelectBox)
	{
	   	$objResponse = new xajaxResponse();

	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
	   	$aData = $this->GetProjectActivities($iDonorProjectId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		return($objResponse->getXML());
	}
	
	function GetProjectActivities($iDonorProjectId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_donors_projects_activities AS PA
		WHERE PA.DonorProjectId='$iDonorProjectId'");		
		$aResult[0][0] = 0;
		$aResult[0][1] = "Select Activity";
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{						
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "PA.ActivityId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "PA.ActivityCode");
		}
		return($aResult);
	}
	
	function AddNewGeneralJournal($iEntryType, $dTransactionDate, $sDescription, $dTotalDebit, $dTotalCredit, $sNotes, $iPaymentType, $iReceiptType, $iBankPaymentType, $iBankCheckBookId=0, $sCheckNumber=0, $iBankAccountId=0, $bPopUp=0)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[1] == 0)
			return(1010);
		
		$iNoOfRowsInVoucher = $objEmployee->aSystemSettings['No_Of_Rows_In_Voucher'];
		
		$varNow = $objGeneral->fnNow();
		$iAddedBy = $objEmployee->iEmployeeId;
		
		$sDescription = mysql_escape_string($sDescription);
		$sNotes = mysql_escape_string($sNotes);
		
		if($dTotalDebit != $dTotalCredit) return(7210);
		
		if($iEntryType == 2) $iPaymentType = $iReceiptType = 0;
		$sReference = $sReference . $iSeries;		
		if($iBankCheckBookId == "") $iBankCheckBookId = 0;
		$iDonorProjectId = 0;
		$iVoucherEntries = $iNoOfRowsInVoucher;		
		// Check Chart Of Accounts
		for ($k=0; $k < $iVoucherEntries; $k++)
		{	
			$iChartOfAccountsId = $objGeneral->fnGet("hdnChartOfAccountId_" . ($k +1));
			$sDetail = $objGeneral->fnGet("txtGeneralJournalParticulars_Detail_" . ($k +1));
			$dDebit = $objGeneral->fnGet("txtGeneralJournalParticulars_Debit_" . ($k +1));
			$dCredit = $objGeneral->fnGet("txtGeneralJournalParticulars_Credit_" . ($k +1));
			
			// If Debit or Credit is not empty
			if($dDebit != 0 || $dCredit != 0)
			{
				if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CA", "CA.ChartOfAccountsId= '$iChartOfAccountsId'") < 0) return('Please select valid Chart Of Account');
			}
		}
		
		/* series now would be based on Donor Project */
		if(($iEntryType == 0)) 	
		{
			$iReceiptType = 0;
			if($iPaymentType == 1) 
			{
				$iBankCheckBookId = 0;
				$sCheckNumber = 0;
				$iBankAccountId = 0;
				
				// Get series based on Donor Project
				if(cOrganizationVoucherSeries == 'DonorProjectWise')
				{
					for ($k=0; $k < $iVoucherEntries; $k++)
					{			
						$iDonorProjectId = $objGeneral->fnGet("selDonorProject" . ($k +1));
						if($iDonorProjectId != "")
						{
							$iSeries = $this->AJAX_GetSeries($iEntryType, -1, $iPaymentType, -1, $iDonorProjectId);
							break;
						}
						// If counter reaches to last record and do not provide any project then, it get general series
						else if($k == ($iVoucherEntries -1))	$iSeries = $this->AJAX_GetSeries($iEntryType, -1, $iPaymentType, -1);
					}
				}
				else				
					$iSeries = $this->AJAX_GetSeries($iEntryType, -1, $iPaymentType, -1);
										
				$sReference = 'CPV'. $iSeries;
			}
			else
			{
				// Get series based on Donor Project
				if(cOrganizationVoucherSeries == 'DonorProjectWise')
				{					
					for ($k=0; $k < $iVoucherEntries; $k++)
					{			
						$iDonorProjectId = $objGeneral->fnGet("selDonorProject" . ($k +1));
						if($iDonorProjectId != "")
						{
							$iSeries = $this->AJAX_GetSeries($iEntryType, -1, $iPaymentType, -1, $iDonorProjectId);
							break;
						}
						
						// If counter reaches to last record and do not provide any project then, it get general series
						else if($k == ($iVoucherEntries -1))	$iSeries = $this->AJAX_GetSeries($iEntryType, -1, $iPaymentType, -1);						
					}
				}
				else
					$iSeries = $this->AJAX_GetSeries($iEntryType, -1, $iPaymentType, $iBankAccountId);
					
				$sReference = 'BPV'. $iSeries;
				// Check for Valid Cheque Number & Cheque number not used alreay
				if ($iBankPaymentType == 0)
				{
					$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS CB WHERE CB.BankCheckBookId='$iBankCheckBookId'");
					if ($objDatabase->RowsNumber($varResult) > 0)
					{
						$iCheckNumberStart = $objDatabase->Result($varResult, 0, "CB.CheckNumberStart");
						$iCheckNumberEnd = $objDatabase->Result($varResult, 0, "CB.CheckNumberEnd");					
						if(($sCheckNumber >= $iCheckNumberStart) && ($sCheckNumber <= $iCheckNumberEnd))
							$sCheckNumber = $sCheckNumber;
						else
							return(7208);
					}
					else
					{
						$iBankCheckBookId = 0;
						$sCheckNumber = 0;
					}
				}
			}
		}
		else if (($iEntryType == 1)) 
		{
			$iPaymentType = 0;
			$iBankCheckBookId = 0;
			$sCheckNumber = 0;			
			if($iReceiptType == 1) 
			{
				$iBankAccountId = 0;
				
				// Get series based on Donor Project
				if(cOrganizationVoucherSeries == 'DonorProjectWise')
				{
					for ($k=0; $k < $iVoucherEntries; $k++)
					{			
						$iDonorProjectId = $objGeneral->fnGet("selDonorProject" . ($k +1));
						if($iDonorProjectId != "")
						{
							$iSeries = $this->AJAX_GetSeries($iEntryType, $iReceiptType, -1, -1, $iDonorProjectId);
							break;
						}
						
						// If counter reaches to last record and do not provide any project then, it get general series
						else if($k == ($iVoucherEntries -1))	$iSeries = $this->AJAX_GetSeries($iEntryType, $iReceiptType,-1, -1);
					}
				}
				else
					$iSeries = $this->AJAX_GetSeries($iEntryType, $iReceiptType,-1, -1);
					
				$sReference = 'CRV'. $iSeries;
			}
			else
			{
				// Get series based on Donor Project
				if(cOrganizationVoucherSeries == 'DonorProjectWise')
				{
					for ($k=0; $k < $iVoucherEntries; $k++)
					{			
						$iDonorProjectId = $objGeneral->fnGet("selDonorProject" . ($k +1));
						if($iDonorProjectId != "")
						{
							$iSeries = $this->AJAX_GetSeries($iEntryType, $iReceiptType, -1, -1, $iDonorProjectId);
							break;
						}
						// If counter reaches to last record and do not provide any project then, it get general series
						else if($k == ($iVoucherEntries-1))	$iSeries = $this->AJAX_GetSeries($iEntryType, $iReceiptType, -1, -1);
					}
				}
				else
					$iSeries = $this->AJAX_GetSeries($iEntryType, $iReceiptType,-1, $iBankAccountId);
					
				$sReference = 'BRV'. $iSeries;
				
			}
		}	
		else
		{	
			$iBankCheckBookId = 0;
			$sCheckNumber = 0;
			$iBankAccountId = 0;
			$iDonorProjectId = 0;
			// Get series based on Donor Project
			if(cOrganizationVoucherSeries == 'DonorProjectWise')
			{
				for ($k=0; $k < $iVoucherEntries; $k++)
				{			
					$iDonorProjectId = $objGeneral->fnGet("selDonorProject" . ($k +1));
					if($iDonorProjectId != "" && $iDonorProjectId != 0)
					{
						$iSeries = $this->AJAX_GetSeries($iEntryType, -1,-1, -1, $iDonorProjectId);
						break;
					}
					// If counter reaches to last record and do not provide any project then, it get general series
					else if($k == ($iVoucherEntries -1))	$iSeries = $this->AJAX_GetSeries($iEntryType, -1,-1, -1);
				}
			}
			else
			{					
				$iSeries = $this->AJAX_GetSeries($iEntryType, -1,-1, -1);
			}
				
			$sReference = 'JV'. $iSeries;
		}

		$sBankAccountCondition = "";
		if($iBankAccountId > 0) $sBankAccountCondition = "AND (GJ.BankAccountId = '$iBankAccountId')";

		//if ($objDatabase->DBCount("fms_accounts_generaljournal AS GJ", "GJ.Reference='$sReference' $sBankAccountCondition") > 0) return(7206);
		
		// Check for Budget Amount & Remaining Amount
		for ($k=0; $k < $iVoucherEntries; $k++)
		{
			$iBudgetId = $objGeneral->fnGet("hdnBudgetId" . ($k +1));
			if($iBudgetId > 0)
			{
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.BudgetId='$iBudgetId'");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					$iChartOfAccountsId = $objGeneral->fnGet("hdnChartOfAccountId_" . ($k +1));
					$sDetail = $objGeneral->fnGet("txtGeneralJournalParticulars_Detail_" . ($k +1));
					$dDebit = $objGeneral->fnGet("txtGeneralJournalParticulars_Debit_" . ($k +1));
					$dCredit = $objGeneral->fnGet("txtGeneralJournalParticulars_Credit_" . ($k +1));
					
					// Check if Budget is allocated for Chart of Account
					if ($objDatabase->DBCount("fms_accounts_budgetallocation AS BA", "BA.ChartOfAccountId='$iChartOfAccountsId' AND BA.BudgetId='$iBudgetId'") > 0)
					{
						$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budgetallocation AS BA WHERE BA.ChartOfAccountId='$iChartOfAccountsId' AND BA.BudgetId='$iBudgetId'");
						if ($objDatabase->RowsNumber($varResult) > 0)
							$dAllocatedAmount = $objDatabase->Result($varResult, 0, "BA.Amount");
						
						$varResult = $objDatabase->Query("
						SELECT 
							CA.ChartOfAccountsCategoryId AS 'ChartOfAccountsCategoryId',
							CAC.DebitIncrease AS 'DebitIncrease'							
						FROM fms_accounts_chartofaccounts_categories AS CAC
						INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsCategoryId = CAC.ChartOfAccountsCategoryId
						WHERE CA.ChartOfAccountsId='$iChartOfAccountsId'");
						if ($objDatabase->RowsNumber($varResult) > 0)
						{
							$iDebitIncrease = $objDatabase->Result($varResult, 0, "DebitIncrease");
							$iChartOfAccountsCategoryId = $objDatabase->Result($varResult, 0, "ChartOfAccountsCategoryId");
						}
						
						$dUsedAmount = 0;						
						if($iDebitIncrease == 1)
						{
							if($dCredit > 0)
							{
								$varResult2 = $objDatabase->Query("
								SELECT 
									SUM(GJE.Credit) AS 'UsedAmount'
								FROM fms_accounts_generaljournal_entries AS GJE
								INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
								WHERE GJE.ChartOfAccountsId='$iChartOfAccountsId' AND GJE.BudgetId = '$iBudgetId' AND GJ.IsDeleted ='0' ");
								if ($objDatabase->RowsNumber($varResult2) > 0)
									$dUsedAmount = $objDatabase->Result($varResult2, 0, "UsedAmount");

								// Check if Credit is greater than Remaining Allocated Amount
								if(($dCredit + $dUsedAmount) > $dAllocatedAmount)
								{
									if( strtolower($objEmployee->aSystemSettings['FMS_Accounts_AllowTransactionExceddingBudget']) == 'yes')
									{										
									}
																		
									else return(7209);									
								}								
							}
							
							if($dDebit > 0)
							{
								$varResult2 = $objDatabase->Query("
								SELECT 
									SUM(GJE.Credit) AS 'UsedAmount'
								FROM fms_accounts_generaljournal_entries AS GJE
								INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
								WHERE GJE.ChartOfAccountsId='$iChartOfAccountsId' AND GJE.BudgetId = '$iBudgetId' AND GJ.IsDeleted ='0' ");
								if ($objDatabase->RowsNumber($varResult2) > 0)
									$dUsedAmount = $objDatabase->Result($varResult2, 0, "UsedAmount");
																
								if(($dDebit + $dUsedAmount) > $dAllocatedAmount)
								{
									if( strtolower($objEmployee->aSystemSettings['FMS_Accounts_AllowTransactionExceddingBudget']) == 'yes')
									{										
									}
									else return(7209);
								}
							}
						}
					}
				}
			}
		}
		
		// End of Budget Updation here
		$k = 0;
		
		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal
		(EntryType, TransactionDate, Reference, Description, TotalDebit, TotalCredit, Notes, PaymentType, ReceiptType, BankPaymentType, BankCheckBookId, BankAccountId, CheckNumber, Series, GeneralJournalAddedOn, GeneralJournalAddedBy)
		VALUES ('$iEntryType', '$dTransactionDate', '$sReference', '$sDescription', '$dTotalDebit', '$dTotalCredit', '$sNotes', '$iPaymentType', '$iReceiptType', '$iBankPaymentType', '$iBankCheckBookId', '$iBankAccountId', '$sCheckNumber', '$iSeries', '$varNow', '$iAddedBy')");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			if(cOrganizationVoucherSeries == 'DonorProjectWise')
			{
				$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_series SET Series=Series+1 WHERE EntryType='$iEntryType' AND PaymentType='$iPaymentType' AND ReceiptType='$iReceiptType' AND DonorProjectId='$iDonorProjectId'");
			}
			else
			{
				$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_series SET Series=Series+1 WHERE EntryType='$iEntryType' AND PaymentType='$iPaymentType' AND ReceiptType='$iReceiptType' AND BankAccountId='$iBankAccountId'");
			}
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal AS GJ WHERE GJ.GeneralJournalAddedOn='$varNow'");

			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iGeneralJournalId = $objDatabase->Result($varResult, 0, "GJ.GeneralJournalId");
				$sReference = $objDatabase->Result($varResult, 0, "GJ.Reference");
				for ($k=0; $k < $iVoucherEntries; $k++)
				{	
					$iChartOfAccountsId = $objGeneral->fnGet("hdnChartOfAccountId_" . ($k +1));
					$iStationId = $objGeneral->fnGet("hdnStationId" . ($k +1));
					$iDepartmentId = $objGeneral->fnGet("hdnDepartmentId" . ($k +1));
					$iBudgetId = $objGeneral->fnGet("hdnBudgetId" . ($k +1));
					$iDonorId = $objGeneral->fnGet("selDonor" . ($k +1));
					$iDonorProjectId = $objGeneral->fnGet("selDonorProject" . ($k +1));
					$iDonorProjectActivityId = $objGeneral->fnGet("selProjectActivity" . ($k +1));
										
					$sDetail = $objGeneral->fnGet("txtGeneralJournalParticulars_Detail_" . ($k +1));
					$dDebit = $objGeneral->fnGet("txtGeneralJournalParticulars_Debit_" . ($k +1));
					$dCredit = $objGeneral->fnGet("txtGeneralJournalParticulars_Credit_" . ($k +1));
					
					$iRecipient = $objGeneral->fnGet("selRecipient" . ($k +1));
					$iVendorId = $objGeneral->fnGet("hdnVendorId" . ($k +1));
					$iCustomerId = $objGeneral->fnGet("hdnCustomerId" . ($k +1));
					$iEmployeeId = $objGeneral->fnGet("hdnEmployeeId" . ($k +1));
					
					if(($dDebit != "") || ($dDebit > 0) || ($dCredit !="") || ($dCredit > 0))
						$this->AddGeneralJournalEntries($iGeneralJournalId, $iChartOfAccountsId, $iStationId, $iDepartmentId, $iBudgetId, $iDonorId, $iDonorProjectId, $iDonorProjectActivityId, $iRecipient, $iVendorId, $iCustomerId, $iEmployeeId, $sDetail, $dDebit, $dCredit);					
				}
				// Add General Journal Status
				$iDefaultStatus = 0;
				$iStatus = $iDefaultStatus . ', ' . $this->aStatusType[$iDefaultStatus];
				
				include('../../system/library/fms/clsFMS_Common_Status.php');
				$objStatus = new clsFMS_Common_Status();
				$objStatus->AddStatus("Accounts_GeneralJournal", $iGeneralJournalId, $iStatus, "");
				
				$objSystemLog->AddLog("Add New General Journal Entry - " . $sReference);
				
				if($bPopUp == 1)
					$objGeneral->fnRedirect('../accounts/generaljournal_details.php?error=7200&popup=1&id=' . $iGeneralJournalId . '&reference=' . $sReference . '&action2=addnew');
					//$objGeneral->fnRedirect('../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&txtId=' . $iGeneralJournalId );
				else
					$objGeneral->fnRedirect('../accounts/generaljournal_details.php?error=7200&id=' . $iGeneralJournalId . '&reference=' . $sReference);
			}
		}

		return(7201);
	}
	
	// Update General Journal
	function UpdateGeneralJournal($iGeneralJournalId, $iEntryType, $dTransactionDate, $sDescription, $dTotalDebit, $dTotalCredit, $sNotes, $iPaymentType, $iReceiptType, $iBankPaymentType, $iBankCheckBookId=0, $sCheckNumber=0, $iBankAccountId=0, $bPopUp=0)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[2] == 0)
			return(1010);

		$iNoOfRowsInVoucher = $objEmployee->aSystemSettings['No_Of_Rows_In_Voucher'];
		
		$sReference = mysql_escape_string($sReference);
		$sDescription = mysql_escape_string($sDescription);
		$sNotes = mysql_escape_string($sNotes);
				
		if($dTotalDebit != $dTotalCredit) return(7211);	
		if($iEntryType == 2) $iPaymentType = $iReceiptType = 0;
		
		$sReference = $sReference . $iSeries;
		if($iBankCheckBookId == "") $iBankCheckBookId = 0;
		$iVoucherEntries = $iNoOfRowsInVoucher;
		// Check Chart Of Accounts
		for ($k=0; $k < $iVoucherEntries; $k++)
		{	
			$iChartOfAccountsId = $objGeneral->fnGet("hdnChartOfAccountId_" . ($k +1));
			$sDetail = $objGeneral->fnGet("txtGeneralJournalParticulars_Detail_" . ($k +1));
			$dDebit = $objGeneral->fnGet("txtGeneralJournalParticulars_Debit_" . ($k +1));
			$dCredit = $objGeneral->fnGet("txtGeneralJournalParticulars_Credit_" . ($k +1));
			
			// If Debit or Credit is not empty
			if($dDebit != 0 || $dCredit != 0)
			{
				if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CA", "CA.ChartOfAccountsId= '$iChartOfAccountsId'") < 0) return('Please select valid Chart Of Account');
			}
		}
		
		if(($iEntryType == 0)) 	
		{
			$iReceiptType = 0;
			if($iPaymentType == 1) 
			{
				$iBankCheckBookId = 0;
				$sCheckNumber = 0;
				$iBankAccountId = 0;
			}
			else
			{
				// Check for Valid Cheque Number & Cheque number not used alreay
				if ($iBankPaymentType == 0)
				{
					$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS CB WHERE CB.BankCheckBookId='$iBankCheckBookId'");
					if ($objDatabase->RowsNumber($varResult) > 0)
					{
						$iCheckNumberStart = $objDatabase->Result($varResult, 0, "CB.CheckNumberStart");
						$iCheckNumberEnd = $objDatabase->Result($varResult, 0, "CB.CheckNumberEnd");					
						if(($sCheckNumber >= $iCheckNumberStart) && ($sCheckNumber <= $iCheckNumberEnd))
							$sCheckNumber = $sCheckNumber;
						else
							return(7208);
					}
					else
					{
						$iBankCheckBookId = 0;
						$sCheckNumber = 0;
					}
				}
			}
		}
		else if (($iEntryType == 1)) 
		{			
			$iPaymentType = 0;
			$iBankCheckBookId = 0;
			$sCheckNumber = 0;
			if($iReceiptType == 1) 
				$iBankAccountId = 0;
		}
		else
		{	
			$iBankCheckBookId = 0;
			$sCheckNumber = 0;
			$iBankAccountId = 0;
		}
		
		$sBankAccountCondition = "";
		if($iBankAccountId > 0) $sBankAccountCondition = "AND (GJ.BankAccountId = '$iBankAccountId')";
		if ($objDatabase->DBCount("fms_accounts_generaljournal AS GJ", "GJ.GeneralJournalId <> '$iGeneralJournalId' AND GJ.Reference='$sReference' AND GJ.IsDeleted ='0' $sBankAccountCondition") > 0) return(7206);
		
		// Check for Budget Amount & Remaining Amount
		for ($k=0; $k < $iVoucherEntries; $k++)
		{
			$iBudgetId = $objGeneral->fnGet("hdnBudgetId" . ($k +1));
			if($iBudgetId > 0)
			{
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.BudgetId='$iBudgetId'");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					$iChartOfAccountsId = $objGeneral->fnGet("hdnChartOfAccountId_" . ($k +1));
					$sDetail = $objGeneral->fnGet("txtGeneralJournalParticulars_Detail_" . ($k +1));
					$dDebit = $objGeneral->fnGet("txtGeneralJournalParticulars_Debit_" . ($k +1));
					$dCredit = $objGeneral->fnGet("txtGeneralJournalParticulars_Credit_" . ($k +1));
					
					// Check if Budget is allocated for Chart of Account
					if ($objDatabase->DBCount("fms_accounts_budgetallocation AS BA", "BA.ChartOfAccountId='$iChartOfAccountsId' AND BA.BudgetId='$iBudgetId'") > 0)
					{
						$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budgetallocation AS BA WHERE BA.ChartOfAccountId='$iChartOfAccountsId' AND BA.BudgetId='$iBudgetId'");
						$dAllocatedAmount = $objDatabase->Result($varResult, 0, "BA.Amount");
																								
						$varResult = $objDatabase->Query("
						SELECT 
							CA.ChartOfAccountsCategoryId AS 'ChartOfAccountsCategoryId',
							CAC.DebitIncrease AS 'DebitIncrease'							
						FROM fms_accounts_chartofaccounts_categories AS CAC
						INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsCategoryId = CAC.ChartOfAccountsCategoryId
						WHERE CA.ChartOfAccountsId='$iChartOfAccountsId'");						
						$iDebitIncrease = $objDatabase->Result($varResult, 0, "DebitIncrease");
						$iChartOfAccountsCategoryId = $objDatabase->Result($varResult, 0, "ChartOfAccountsCategoryId");
						
						$dUsedAmount = 0;						
						if($iDebitIncrease == 1)
						{
							if($dCredit > 0)
							{
								$varResult2 = $objDatabase->Query("
								SELECT 
									SUM(GJE.Credit) AS 'UsedAmount'
								FROM fms_accounts_generaljournal_entries AS GJE
								INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
								WHERE GJE.ChartOfAccountsId='$iChartOfAccountsId' AND GJE.GeneralJournalId <> '$iGeneralJournalId' AND GJE.BudgetId = '$iBudgetId' AND GJ.IsDeleted ='0'");
								if ($objDatabase->RowsNumber($varResult2) > 0)
								{
									$dUsedAmount = $objDatabase->Result($varResult2, 0, "UsedAmount");
								}
								// Check if Credit is greater than Remaining Allocated Amount
								if(($dCredit + $dUsedAmount) > $dAllocatedAmount)
								{
									if( strtolower($objEmployee->aSystemSettings['FMS_Accounts_AllowTransactionExceddingBudget']) == 'yes')
									{										
									}
																		
									else return(7209);									
								}								
							}
							
							if($dDebit > 0)
							{
								$varResult2 = $objDatabase->Query("
								SELECT 
									SUM(GJE.Credit) AS 'UsedAmount'
								FROM fms_accounts_generaljournal_entries AS GJE
								INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
								WHERE GJE.ChartOfAccountsId='$iChartOfAccountsId' AND GJE.GeneralJournalId <> '$iGeneralJournalId' AND GJE.BudgetId = '$iBudgetId'  AND GJ.IsDeleted ='0'");
								if ($objDatabase->RowsNumber($varResult2) > 0)
								{
									$dUsedAmount = $objDatabase->Result($varResult2, 0, "UsedAmount");
								}
																
								if(($dDebit + $dUsedAmount) > $dAllocatedAmount)
								{
									if( strtolower($objEmployee->aSystemSettings['FMS_Accounts_AllowTransactionExceddingBudget']) == 'yes')
									{										
									}
									else return(7209);
								}
							}
						}
					}
				}
			}
		}
		
		// End of Budget Updation here		
		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_generaljournal 
		SET
			EntryType='$iEntryType',
			TransactionDate='$dTransactionDate',
			Description='$sDescription',
			TotalDebit='$dTotalDebit',
			TotalCredit='$dTotalCredit',
			Notes='$sNotes',
			PaymentType='$iPaymentType',
			ReceiptType='$iReceiptType',
			BankPaymentType='$iBankPaymentType',
			BankCheckBookId='$iBankCheckBookId',
			BankAccountId='$iBankAccountId',
			CheckNumber='$sCheckNumber'			
		WHERE GeneralJournalId='$iGeneralJournalId'");
		
		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_generaljournal_entries WHERE GeneralJournalId='$iGeneralJournalId'");
				
		for ($k=0; $k < $iVoucherEntries; $k++)
		{
			$iChartOfAccountsId = $objGeneral->fnGet("hdnChartOfAccountId_" . ($k +1));
			$iStationId = $objGeneral->fnGet("hdnStationId" . ($k +1));
			$iDepartmentId = $objGeneral->fnGet("hdnDepartmentId" . ($k +1));
			$iBudgetId = $objGeneral->fnGet("hdnBudgetId" . ($k +1));
			$iDonorId = $objGeneral->fnGet("selDonor" . ($k +1));
			$iDonorProjectId = $objGeneral->fnGet("selDonorProject" . ($k +1));
			$iDonorProjectActivityId = $objGeneral->fnGet("selProjectActivity" . ($k +1));
								
			$sDetail = $objGeneral->fnGet("txtGeneralJournalParticulars_Detail_" . ($k +1));
			$dDebit = $objGeneral->fnGet("txtGeneralJournalParticulars_Debit_" . ($k +1));
			$dCredit = $objGeneral->fnGet("txtGeneralJournalParticulars_Credit_" . ($k +1));
			
			$iRecipient = $objGeneral->fnGet("selRecipient" . ($k +1));
			$iVendorId = $objGeneral->fnGet("hdnVendorId" . ($k +1));
			$iCustomerId = $objGeneral->fnGet("hdnCustomerId" . ($k +1));
			$iEmployeeId = $objGeneral->fnGet("hdnEmployeeId" . ($k +1));
			
			if(($dDebit != "" && $dDebit > 0) || ($dDebit > 0) || ($dCredit !="" && $dCredit > 0) || ($dCredit > 0))
				$this->AddGeneralJournalEntries($iGeneralJournalId, $iChartOfAccountsId, $iStationId, $iDepartmentId, $iBudgetId, $iDonorId, $iDonorProjectId, $iDonorProjectActivityId, $iRecipient, $iVendorId, $iCustomerId, $iEmployeeId, $sDetail, $dDebit, $dCredit);
		}
		
		$objSystemLog->AddLog("Update General Journal Entry - " . $sReference);
		
		if($bPopUp == 1)
			$objGeneral->fnRedirect('../accounts/generaljournal_details.php?error=7202&popup=1&printid=' . $iGeneralJournalId . '&action2=addnew');
			//$objGeneral->fnRedirect('../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&txtId=' . $iGeneralJournalId );
		else
			$objGeneral->fnRedirect('../accounts/generaljournal_details.php?error=7202&id=' . $iGeneralJournalId . '&printid=' . $iGeneralJournalId);
		
	}
		
	/* Add General Journal Entries */
	function AddGeneralJournalEntries($iGeneralJournalId, $iChartOfAccountsId, $iStationId, $iDepartmentId, $iBudgetId, $iDonorId, $iDonorProjectId, $iDonorProjectActivityId, $iRecipient, $iVendorId, $iCustomerId, $iEmployeeId, $sDetail, $dDebit, $dCredit)
	{
		global $objDatabase;
		global $objGeneral;	
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[1] == 0)
			return(1010);
				
		$sDetail = mysql_escape_string($sDetail);
		$dRunningBalance = 0;		
		if($dDebit == "" ) $dDebit = 0;
		if($dCredit == "" ) $dCredit = 0;
		if($iRecipient == "" ) $iRecipient = 0;
		if($iVendorId == "" ) $iVendorId = 0;
		if($iCustomerId == "" ) $iCustomerId = 0;
		if($iEmployeeId == "" ) $iEmployeeId = 0;
		if($iStationId == "") $iStationId = 0;
		if($iDepartmentId == "") $iDepartmentId = 0;
		if($iBudgetId == "") $iBudgetId = 0;
		if($iDonorId == "") $iDonorId = 0;
		if($iDonorProjectId == "") $iDonorProjectId = 0;
		if($iDonorProjectActivityId == "") $iDonorProjectActivityId = 0;
		
		if($dDebit > 0 && $dCredit > 0)
			return(false);
			
		$varResult = $objDatabase->Query("
		SELECT 
			CA.ChartOfAccountsCategoryId, 
			CAC.DebitIncrease, 
			CAC.CreditIncrease
		FROM fms_accounts_chartofaccounts_categories AS CAC
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsCategoryId = CAC.ChartOfAccountsCategoryId
		WHERE CA.ChartOfAccountsId='$iChartOfAccountsId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$iDebitIncrease = $objDatabase->Result($varResult, 0, "DebitIncrease");
			$iChartOfAccountsCategoryId = $objDatabase->Result($varResult, 0, "ChartOfAccountsCategoryId");
		}
		
		/*
		$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_entries
		(GeneralJournalId, ChartOfAccountsId, StationId, DepartmentId, BudgetId, DonorId, DonorProjectId, DonorProjectActivityId, Recipient, VendorId, CustomerId, EmployeeId, Detail, Debit, Credit)
		VALUES ('$iGeneralJournalId', '$iChartOfAccountsId', '$iStationId', '$iDepartmentId', '$iBudgetId', '$iDonorId', '$iDonorProjectId', '$iDonorProjectActivityId', '$iRecipient', '$iVendorId', '$iCustomerId', '$iEmployeeId', '$sDetail', '$dDebit', '$dCredit')");
		*/

		$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_entries
		(GeneralJournalId, ChartOfAccountsId, StationId, DepartmentId, BudgetId, DonorId, DonorProjectId, DonorProjectActivityId, Recipient, VendorId, CustomerId, EmployeeId, Detail, Debit, Credit)
		VALUES ('$iGeneralJournalId', '$iChartOfAccountsId', '0', '0', '$iBudgetId', '$iDonorId', '$iDonorProjectId', '0', '0', '0', '0', '0', '$sDetail', '$dDebit', '$dCredit')");

		
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
	}
	
	// Delete General Journal
	function DeleteGeneralJournal($iGeneralJournalId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[3] == 0)
			return(1010);
				
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal AS GJ WHERE GJ.GeneralJournalId= '$i$iGeneralJournalId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(7205);
		$sReference = $objDatabase->Result($varResult, 0, "GJ.Reference");
		/*
		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_generaljournal WHERE GeneralJournalId= '$i$iGeneralJournalId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_accounts_generaljournal_entries WHERE GeneralJournalId='$iGeneralJournalId'");
			
			$objSystemLog->AddLog("Delete General Journal - " . $sReference);

			return(7204);
		}
		*/
		
		$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal SET IsDeleted = '1' WHERE GeneralJournalId= '$i$iGeneralJournalId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_entries SET IsDeleted = '1' WHERE GeneralJournalId='$iGeneralJournalId'");
			
			$objSystemLog->AddLog("Delete General Journal - " . $sReference);
			return(7204);
		}
		


		else
			return(7205);
	}	
	
	/*
	function AJAX_FillBankAccounts($iBankId, $sTargetSelectBox, $iDefaultBankAccountId)
	{
	   	$objResponse = new xajaxResponse();

	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
		$objResponse->addScript("ShowDiv('divBankAccount');");
	   	$aData = $this->GetBankAccounts($iBankId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		
		if ($iDefaultBankAccountId > 0)	
			$objResponse->addScript("document.getElementById('$sTargetSelectBox').value = $iDefaultBankAccountId;");
		
		return($objResponse->getXML());
	}
	
	function GetBankAccounts($iBankId)
	{
		global $objDatabase;

		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA
		WHERE BA.BankId='$iBankId' AND BA.Status='1'");
		
		$aResult[0][0] = 0;
		$aResult[0][1] = "Select Bank Account";
		
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "BA.BankAccountId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "BA.AccountTitle") . '( ' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . ')';
		}

		return($aResult);
	}
	
	function AJAX_FillCheckBooks($iBankAccountId, $sTargetSelectBox, $iDefaultBankCheckBookId)
	{
	   	$objResponse = new xajaxResponse();

	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
		$objResponse->addScript("ShowDiv('divCheckBook');");
		$objResponse->addScript("ShowDiv('trCheckNumber');");
	   	$aData = $this->GetCheckBooks($iBankAccountId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
			
		if ($iDefaultBankCheckBookId > 0)
			$objResponse->addScript("document.getElementById('$sTargetSelectBox').value = " . $iDefaultBankCheckBookId . ";");
		
		return($objResponse->getXML());
	}
	
	function GetCheckBooks($iBankAccountId)
	{
		global $objDatabase;

		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS CB
		WHERE CB.BankAccountId='$iBankAccountId' AND CB.Status='1'");
		$aResult[0][0] = 0;
		$aResult[0][1] = "Select Check Book";
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "CB.BankCheckBookId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "CB.CheckPrefix") . '( ' . $objDatabase->Result($varResult, $i, "CB.CheckNumberStart") . '- ' .  $objDatabase->Result($varResult, $i, "CB.CheckNumberEnd") . ')';
			
		}

		return($aResult);
	}
	*/
}

?>