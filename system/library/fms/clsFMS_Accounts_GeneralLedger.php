<?php

// Class: General Ledger
class clsAccounts_GeneralLedger
{	
	// Class Constructor
	function __construct()
	{
		
	}
	
	function ShowAllGeneralLedger($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralLedgers[0] == 0) // View Disabled
			return(1010);	

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "General Ledger";
		if ($sSortBy == "") $sSortBy = "CA.ChartOfAccountsCode ASC";

		if ($sSearch != "")
		{
			$sSearch = mysql_escape_string($sSearch);
			$sSearchCondition = " AND ((CA.ChartOfAccountsCode LIKE '%$sSearch%') OR (CA.ChartOfAccountsId LIKE '%$sSearch%') OR (CA.AccountTitle LIKE '%$sSearch%'))";			
			
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_chartofaccounts AS CA ", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT 
			CA.ChartOfAccountsId,
			CA.ChartOfAccountsCode,
			CA.AccountTitle,
			SUM(GJE.Debit) AS 'Debit',
			SUM(GJE.Credit) AS 'Credit',
			IF(CACT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_chartofaccounts_categories AS CACT 
		INNER JOIN fms_accounts_chartofaccounts_controls AS CAC ON CAC.ChartOfAccountsCategoryId = CACT.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CAC.ChartOfAccountsControlId
		LEFT  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		WHERE 1=1 AND GJ.IsDeleted ='0' $sSearchCondition	 
		GROUP BY CA.ChartOfAccountsId 
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Account Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.AccountTitle&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Title in Ascending Order" title="Sort by Account Title  in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CA.AccountTitle&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Title in Descending Order" title="Sort by Account Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Account Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Code in Ascending Order" title="Sort by Account Code  in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Code in Descending Order" title="Sort by Account Code in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Debit</span></td>
		  <td align="right"><span class="WhiteHeading">Credit</span></td>
		  <td align="right"><span class="WhiteHeading">Balance</span></td>
		  <td width="1%"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsId");
			$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode");
			$sAccountTitle = $objDatabase->Result($varResult, $i, "CA.AccountTitle");
			$dBalance = $objDatabase->Result($varResult, $i, "Balance");
			$dDebit = $objDatabase->Result($varResult, $i, "Debit");
			$dCredit = $objDatabase->Result($varResult, $i, "Credit");
			
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sAccountTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sChartOfAccountsCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . number_format($dDebit, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . number_format($dCredit, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . number_format($dBalance, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'General Ledger Details\', \'../accounts/generalledger_details.php?id=' . $iChartOfAccountsId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this General Ledger Details" title="View this General Ledger Details"></a></td>
			</tr>';
		}

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
 		   <form method="GET" action="">Search for a Chart Of Account:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Chart Of Account:" title="Search for a Chart Of Account:" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this General Ledger Details" alt="View this General Ledger Details"></td><td>View this General Ledger Details</td></tr>		 
		</table>';

		return($sReturn);
	}
	
	function GeneralLedgerDetails($iChartOfAccountsId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralLedgers[0] == 0) // View Disabled
			return(1010);

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_accounts_chartofaccounts AS CA
		WHERE CA.ChartOfAccountsId= '$iChartOfAccountsId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
	        $sTitle = $objDatabase->Result($varResult, 0, "CA.AccountTitle");
		
		$sTitle = "General Ledger - " . $sTitle;
		
		$iTotalRecords = $objDatabase->DBCount("fms_accounts_generaljournal AS GJ INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId", "GJE.ChartOfAccountsId= '$iChartofAccountsId' AND GJ.IsDeleted ='0'");
		
		$varResult = $objDatabase->Query("
		SELECT 
			GJ.TransactionDate AS 'TransactionDate',
			GJ.GeneralJournalId,
			CACT.DebitIncrease AS 'DebitIncrease',
			GJ.Reference AS 'Reference',
			GJE.Detail AS 'Detail',
			GJE.Debit AS 'Debit',
			GJE.Credit AS 'Credit'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CACT ON CACT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId		
		WHERE GJE.ChartOfAccountsId= '$iChartOfAccountsId'  AND GJ.IsDeleted ='0'
		ORDER BY GJ.TransactionDate");	

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;
		
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr> <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td><td align="right">' . $sButtons . '</td></tr>		 
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left" style="font-size:13px; font-family:Tahoma, Arial;"><span class="WhiteHeading">Transaction Date&nbsp;&nbsp;</span></td>
		  <td align="left" style="font-size:13px; font-fasmily:Tahoma, Arial;"><span class="WhiteHeading">Ref No.&nbsp;&nbsp;</span></td>
		  <td align="left" style="font-size:13px; font-family:Tahoma, Arial;"><span class="WhiteHeading">Detail&nbsp;&nbsp;</span></td>
		  <td align="right" style="font-size:13px; font-family:Tahoma, Arial;"><span class="WhiteHeading">Debit&nbsp;&nbsp;</span></td>
		  <td align="right" style="font-size:13px; font-family:Tahoma, Arial;"><span class="WhiteHeading">Credit&nbsp;&nbsp;</span></td>
		  <td align="right" style="font-size:13px; font-family:Tahoma, Arial;"><span class="WhiteHeading">Balance&nbsp;&nbsp;</span></td>
		  <td width="1%" align="left" style="font-size:13px; font-family:Tahoma, Arial;"><span class="WhiteHeading">Operations&nbsp;&nbsp;</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
			$iDebitIncrease = $objDatabase->Result($varResult, $i, "DebitIncrease");
			$sReference = $objDatabase->Result($varResult, $i, "GJ.Reference");
			$dTransactionDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
			$sDetail = $objDatabase->Result($varResult, $i, "GJE.Detail");
			$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
			$dDebit = $objDatabase->Result($varResult, $i, "GJE.Debit");
			$dCredit = $objDatabase->Result($varResult, $i, "GJE.Credit");
			if($iDebitIncrease == 1)
			{	
				$dTransBalance = $dDebit - $dCredit;
			}
			else 
			{
				$dTransBalance = $dCredit - $dDebit;
			}
			$dBalance += $dTransBalance;
			
			$sDebit = number_format($dDebit, 0);
			$sCredit = number_format($dCredit, 0);
			
			if ($sDebit == 0) $sDebit = '';
			if ($sCredit == 0) $sCredit = '';
			
			$dTotal_Debit += $dDebit;
			$dTotal_Credit += $dCredit;
									
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sTransactionDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sReference . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sDetail . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td> 
			 <td class="GridTD" align="right" valign="top">' . $sDebit . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . $sCredit . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . number_format($dBalance, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'General Jouranl Entry Details\', \'../accounts/generaljournal_details.php?id=' . $iGeneralJournalId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Transaction Details" title="View this Transaction Details"></a></td>
			</tr>';
		}

		$sReturn .= '<tr style="background-color:#b1b1b1;">
		 <td class="GridTD" colspan="3" align="right">Total</td>
		 <td class="GridTD" align="right">' . number_format($dTotal_Debit, 0) . '</td>
		 <td class="GridTD" align="right">' . number_format($dTotal_Credit, 0) . '</td>
		 <td class="GridTD" align="right">' . number_format($dBalance, 0) . '</td>
		 <td></td>
		</tr>';
		
		return($sReturn);
	}
}

?>