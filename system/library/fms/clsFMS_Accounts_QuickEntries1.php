<?php

// Class: QuickEntry
class clsAccounts_QuickEntries
{
	public $aEntryType;
	public $aPaymentType;
	public $aRecipient;
	public $aReceiptType;
	public $aStatus;

	// Class Constructor
	function __construct()
	{
		$this->aEntryType = array('Payment', 'Receipt', 'Journal Entry');
		$this->aPaymentType = array('Bank Payment', 'Cash Payment');
		$this->aRecipient = array('Vendor', 'Customer', 'Employee');
		$this->aReceiptType = array('Bank Receipt', 'Cash Receipt');
		$this->aBankPaymentType = array('CheckBook', 'Online Transfer', 'ATM');
		$this->aStatus = array('In-active', 'Active');
	}
	
	function ShowAllQuickEntries($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Quick Entries";
		if ($sSortBy == "") $sSortBy = "QE.QuickEntryId DESC";

		if ($sSearch != "")
		{
			$sSearch = mysql_escape_string($sSearch);
			$sSearchCondition = " AND ((QE.Title LIKE '%$sSearch%') OR (QE.QuickEntryId LIKE '%$sSearch%'))";
			
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_generaljournal_quickentries AS QE INNER JOIN organization_employees AS E ON E.EmployeeId = QE.QuickEntryAddedBy", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_generaljournal_quickentries AS QE
		INNER JOIN organization_employees AS E ON E.EmployeeId = QE.QuickEntryAddedBy		
		WHERE 1=1 $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=QE.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=QE.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Type&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=QE.EntryType&sortorder="><img src="../images/sort_up.gif" alt="Sort by Entry Type in Ascending Order" title="Sort by Entry Type in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=QE.EntryType&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Entry Type in Descending Order" title="Sort by Entry Type in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=QE.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=QE.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Added on&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=QE.QuickEntryAddedOn&sortorder="><img src="../images/sort_up.gif" alt="Sort by Added on in Ascending Order" title="Sort by Added on in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=QE.QuickEntryAddedOn&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Added on in Descending Order" title="Sort by Added on in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iQuickEntryId = $objDatabase->Result($varResult, $i, "QE.QuickEntryId");
			$sTitle = $objDatabase->Result($varResult, $i, "QE.Title");			
			$iEntryType = $objDatabase->Result($varResult, $i, "QE.EntryType");
			$sEntryType = $this->aEntryType[$iEntryType];
			$iStatus = $objDatabase->Result($varResult, $i, "QE.Status");
			$sStatus = $this->aStatus[$iStatus];			
			$dAddedOn = $objDatabase->Result($varResult, $i, "QE.QuickEntryAddedOn");
			$sAddedOn = date("F j, Y", strtotime($dAddedOn));
												
			$sEditQuickEntry = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . mysql_escape_string(stripslashes($sTitle)) . '\', \'../accounts/quickentries.php?pagetype=details&action2=edit&id=' . $iQuickEntryId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quick Entry Details" title="Edit this Quick Entry Details"></a></td>';
			$sDeleteQuickEntry = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Quick Entry?\')) {window.location = \'?action=DeleteQuickEntry&id=' . $iQuickEntryId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete Quick Entry" title="Delete Quick Entry"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[2] == 0)$sEditQuickEntry = '';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[3] == 0)$sDeleteQuickEntry = '';
				
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sEntryType . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sAddedOn . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . mysql_escape_string(stripslashes($sTitle)). '\', \'../accounts/quickentries.php?pagetype=details&id=' . $iQuickEntryId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quick Entry Details" title="View this Quick Entry Details"></a></td>
			 ' . $sEditQuickEntry . $sDeleteQuickEntry. '
			</tr>';
		}

		$sAddQuickEntry = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add New Entry\', \'../accounts/quickentries.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add New">';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[1] == 0) // Add Disabled
			$sAddQuickEntry = '';
			
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddQuickEntry . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for Quick Entry:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for Quick Entry" title="Search for Quick Entry" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Quick Entry Details" alt="View this Quick Entry Details"></td><td>View this Quick Entry Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Quick Entry Details" alt="Edit this Quick Entry Details"></td><td>Edit this Quick Entry Details</td></tr>		 
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Quick Entry" alt="Delete this Quick Entry"></td><td>Delete this Quick Entry</td></tr>
		</table>';

		return($sReturn);
	}
	
	function QuickEntryDetails($iQuickEntryId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		// Array of Chart of Accounts
		include('../../system/library/fms/clsFMS_Accounts_ChartOfAccounts.php');
		$objChartOfAccounts = new clsChartOfAccounts();
		$aChartOfAccounts = $objChartOfAccounts->ChartOfAccountsList();
		
		include('../../system/library/fms/clsFMS_Vendors_Vendors.php');
		$objVendors = new clsVendors_Vendors();
		$aVendors = $objVendors->VendorsList();
		
		include('../../system/library/fms/clsFMS_Customers.php');
		$objCustomers = new clsCustomers_Customers();
		$aCustomers = $objCustomers->CustomersList();
		
		$aEmployees = $objEmployee->EmployeesList();
		
		include('../../system/library/clsOrganization.php');
		$objStations = new clsStations();
		$aStations = $objStations->StationsList();
		
		//include('../../system/library/clsOrganization_Departments.php');
		$objDepartments = new clsDepartments();
		$aDepartments = $objDepartments->DepartmentsList();
		
		include('../../system/library/fms/clsFMS_Accounts_Budget.php');
		$objBudget = new clsFMS_Accounts_Budget();
		$aBudgets = $objBudget->BudgetsList();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objQuery = new clsjQuery($aChartOfAccounts);
		
		$objQueryVendor = new clsjQuery($aVendors);
		$objQueryCustomer = new clsjQuery($aCustomers);
		$objQueryEmployee = new clsjQuery($aEmployees);
		$objQueryStation = new clsjQuery($aStations);
		$objQueryDepartment = new clsjQuery($aDepartments);
		$objQueryBudget = new clsjQuery($aBudgets);
		
		$iNoOfRowsInVoucher	 = 	$objEmployee->aSystemSettings['No_Of_Rows_In_Voucher'];
		
		$k = 0;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_accounts_generaljournal_quickentries AS QE		
		INNER JOIN organization_employees AS E ON E.EmployeeId = QE.QuickEntryAddedBy
		LEFT JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = QE.BankAccountId
		LEFT JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		LEFT JOIN fms_banking_bankcheckbooks AS BC ON BC.BankCheckBookId = QE.BankCheckBookId		
		WHERE QE.QuickEntryId = '$iQuickEntryId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iQuickEntryId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this QuickEntry" title="Edit this QuickEntry" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this QuickEntry?\')) {window.location = \'?action=DeleteQuickEntry&id=' . $iQuickEntryId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this QuickEntry" title="Delete this QuickEntry" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[2] == 0)$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[3] == 0)$sButtons_Delete = '';
			
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Quick Entry Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iQuickEntryId = $objDatabase->Result($varResult, 0, "QE.QuickEntryId");
				/*
				$iStationId = $objDatabase->Result($varResult, $i, "QE.StationId");				
				$sStationName = $objDatabase->Result($varResult, $i, "S.StationName");				
				$sStationName = $sStationName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' .  mysql_escape_string(str_replace('"', '', $sStationName)) .'\' , \'../organization/stations.php?pagetype=details&id=' . $iStationId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Station Details" title="Station Details" /></a>';
				
				$iDepartmentId = $objDatabase->Result($varResult, 0, "QE.DepartmentId");
				$sDeparmentName = $objDatabase->Result($varResult, 0, "DPT.DepartmentName");
				$sDeparmentName = $sDeparmentName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $sDeparmentName . '\', \'../organization/departments.php?pagetype=details&id=' . $iDepartmentId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" alt="Department Information" title="Department Information" border="0" /></a><br />';
				*/
				
				$sTitle = $objDatabase->Result($varResult, 0, "QE.Title");
				$iEntryOrder = $objDatabase->Result($varResult, 0, "QE.EntryOrder");
				$sEntryOrder = $iEntryOrder;
				$iEntryType = $objDatabase->Result($varResult, 0, "QE.EntryType");
				$sEntryType = $this->aEntryType[$iEntryType];
				$iPaymentType = $objDatabase->Result($varResult, 0, "QE.PaymentType");
				$sPaymentType = $this->aPaymentType[$iPaymentType];								
				$iReceiptType = $objDatabase->Result($varResult, 0, "QE.ReceiptType");				
				$sReceiptType= $this->aReceiptType[$iReceiptType];
				$iBankPaymentType = $objDatabase->Result($varResult, 0, "QE.BankPaymentType");
				$sBankPaymentType = $this->aBankPaymentType[$iBankPaymentType];
				$iStatus = $objDatabase->Result($varResult, 0, "QE.Status");
				$sStatus = $this->aStatus[$iStatus];
				/*
				$iBudgetId = $objDatabase->Result($varResult, $i, "QE.BudgetId");
				$sBudget = "";
				if($iBudgetId > 0)
				{
					$sBudget = $objDatabase->Result($varResult, $i, "BJ.Title");
					$sBudget = mysql_escape_string($sBudget);
					$sBudget = $sBudget . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' .  mysql_escape_string(str_replace('"', '', $sBudget)) .'\' , \'../accounts/budget.php?pagetype=details&id=' . $iBudgetId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Budget Details" title="Budget Details" /></a>';
				}
				else	$sBudget = "Select Budget";
								
				$iDonorId = $objDatabase->Result($varResult, $i, "QE.DonorId");
				$iDonorProjectId = $objDatabase->Result($varResult, $i, "QE.DonorProjectId");
				$iActivityId = $objDatabase->Result($varResult, $i, "QE.DonorProjectActivityId");
				if($iDonorId > 0)
				{
					$sDonor = $objDatabase->Result($varResult, $i, "D.DonorName");
					if($iDonorProjectId > 0 ) $sDonorProject = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
					if($iActivityId > 0 ) $sProjectActivities = $objDatabase->Result($varResult, $i, "PA.ActivityTitle");
					$sDonorRow = '<tr id="trDonors" style="display:visible;" bgcolor="#edeff1"><td valign="top">Donor:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sDonor. '&nbsp;&raquo;&nbsp;</td><td>' . $sDonorProject . '&nbsp;&raquo;&nbsp;</td><td>' . $sProjectActivities . '</td></tr></table></td></tr>';
				}
				*/
				$iBankId = $objDatabase->Result($varResult, 0, "QE.BankId");
				if($iBankId > 0)
				{
					$sBank = $objDatabase->Result($varResult, 0, "B.BankName");				
				}
				
				$iBankAccountId = $objDatabase->Result($varResult, 0, "QE.BankAccountId");
				if($iBankAccountId > 0)
				{
					$sBank = $objDatabase->Result($varResult, 0, "B.BankName");
					$sBankAccount = $objDatabase->Result($varResult, 0, "BA.AccountTitle") . ' (' . $objDatabase->Result($varResult, 0, "BA.BankAccountNumber") . ')';
				}
				
				$iBankCheckBookId = $objDatabase->Result($varResult, 0, "QE.BankCheckBookId");
				if($iBankCheckBookId > 0)
				{
					
					$sCheckPrefix = $objDatabase->Result($varResult, 0, "BC.CheckPrefix");
					$sBankCheckBook  = '<tr bgcolor="#efeff1" id="trCheckBook" style="display:visible;"><td valign="top">Check Book:</td><td><strong>' . $sCheckPrefix . '</strong></td></tr>';
					$sReceiptType='';
				}
				
				if($iEntryType == 2) $sPaymentType = $sBankRow = $sReceiptType = "";				
				else if($iEntryType == 1) 
				{
					if($iReceiptType == 0)					
						$sBankRow = '<tr id="trBanks" style="display:visible;" bgcolor="#edeff1"><td valign="top">Bank:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sBank  . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankAccount . '&nbsp;&raquo;&nbsp;</td></tr></table></td></tr>';
											
					$sPaymentType = "";
				}
				
				else 
				{
					if($iPaymentType == 0)
						$sBankRow = '<tr id="trBanks" style="display:visible;" bgcolor="#edeff1"><td valign="top">Bank:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sBank  . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankAccount . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankPaymentType . '</td></tr></table></td></tr>';
					$sReceiptType = "";
				}
				if($iEntryType != 2) $sEntryType .= '&nbsp;&raquo;&nbsp;';
				
				$sEntryType .= $sPaymentType . $sReceiptType;
				
				$dAddedOn = $objDatabase->Result($varResult, 0, "QE.QuickEntryAddedOn");
				$sAddedOn = date("F j, Y", strtotime($dAddedOn)) . ' at ' . date("g:i a", strtotime($dAddedOn));
				
				$iAddedBy = $objDatabase->Result($varResult, 0, "QE.QuickEntryAddedBy");
				$sAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(str_replace('"', '', $sAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$varResult2 = $objDatabase->Query("SELECT *
				FROM fms_accounts_generaljournal_quickentries_entries AS QEE
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= QEE.ChartOfAccountsId
				LEFT JOIN organization_stations AS S ON S.StationId = QEE.StationId
				LEFT JOIN organization_departments AS DPT ON DPT.DepartmentId = QEE.DepartmentId
				LEFT JOIN fms_accounts_budget AS BJ ON BJ.BudgetId = QEE.BudgetId
				LEFT JOIN fms_accounts_donors AS D ON D.DonorId= QEE.DonorId
				LEFT JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId= QEE.DonorProjectId
				LEFT JOIN fms_accounts_donors_projects_activities AS PA ON PA.ActivityId= QEE.DonorProjectActivityId
				LEFT JOIN organization_employees AS E ON E.EmployeeId = QEE.EmployeeId
				LEFT JOIN fms_vendors AS V ON V.VendorId = QEE.VendorId
				LEFT JOIN fms_customers AS C ON C.CustomerId = QEE.CustomerId
				WHERE QEE.QuickEntryId= '$iQuickEntryId'
				ORDER BY QEE.QuickEntryEntryId");
						       	
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';
					
					$sChartOfAccount = $objDatabase->Result($varResult2, $i, "CA.ChartOfAccountsCode");
					$sStation = $objDatabase->Result($varResult2, $i, "S.StationCode");
					$sDepartment = $objDatabase->Result($varResult2, $i, "DPT.DepartmentCode");
					$sBudget = $objDatabase->Result($varResult2, $i, "BJ.BudgetCode");
					$sDonor = $objDatabase->Result($varResult2, $i, "D.DonorCode");
					$sDonorProject = $objDatabase->Result($varResult2, $i, "DP.ProjectCode");
					$sProjectActivity = $objDatabase->Result($varResult2, $i, "PA.ActivityCode");
					
					$iRecipient = $objDatabase->Result($varResult2, $i, "QEE.Recipient");
					if($iRecipient == 0) 
						$sRecipient = $objDatabase->Result($varResult2, $i, "V.VendorCode") . '' . $objDatabase->Result($varResult2, $i, "V.VendorName");
					else if($iRecipient == 1) 
						$sRecipient = $objDatabase->Result($varResult2, $i, "C.FirstName") . '' . $objDatabase->Result($varResult2, $i, "C.LastName");
					else
						$sRecipient = $objDatabase->Result($varResult2, $i, "E.FirstName") . '' . $objDatabase->Result($varResult2, $i, "E.LastName");
														
					$sDetail = $objDatabase->Result($varResult2, $i, "QEE.Detail");
					$dDebit = $objDatabase->Result($varResult2, $i, "QEE.Debit");
					$dCredit = $objDatabase->Result($varResult2, $i, "QEE.Credit");
					$dTotalDebit += $dDebit;
					$dTotalCredit += $dCredit;
					$sDonor = $sDonor . '- '. $sDonorProject . ' - '. $sProjectActivity;
					
					$sQuickEntryParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td>' . ($i+1) . '</td>
					 <td>' . $sChartOfAccount . '</td>
					 <td>' . $sStation . '</td>
					 <td>' . $sDepartment . '</td>
					 <td>' . $sBudget . '</td>
					 <td>' . $sDonor . '</td>
					 <td>' . $sRecipient . '</td>
 					 <td>' . $sDetail . '</td>
					 <td align="right">' . number_format($dDebit, 0) . '</td>
					 <td align="right">' . number_format($dCredit, 0) . '</td>				 
					</tr>';
				}
				
				$sQuickEntryEntries = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		          <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Chart of Accounts</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Station</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Department</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Budget</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Donor</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Recipient</span></td>
				  <td align="left"><span class="WhiteHeading">Description</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Debit</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Credit</span></td>
				 </tr>
				' . $sQuickEntryParticulars_Rows . '				
				<tr bgcolor="#ffffff">
				 <td align="right" colspan="8" style="font-size:13px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				 <td align="right" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:14px;">' . number_format($dTotalDebit, 0) . '</td>
				 <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:14px;">' . number_format($dTotalCredit, 0) . '</td>
				</tr>				
				</table>';				
			}
			if ($sAction2 == "edit")
			{
				$sReturn .= '<form id="frmEntry" name="frmEntry" method="post" action="" onsubmit="return ValidateForm();">';
				$sBankAccount = '<div id="divBankAccount" style="display:none;"><select name="selBankAccount" id="selBankAccount" onchange="GetBankPaymentTypes();" class="Tahoma14"></select>';
				$sBankCheckBook = '<div id="divCheckBook" style="display:none;"><select name="selCheckBook" id="selCheckBook" class="Tahoma14"></select>';
				$sCheckBook = '<tr bgcolor="#ffffff" id="trCheckBook" style="display:none;"><td valign="top">Check Book:</td><td>' . $sBankCheckBook . '</td></tr>';
				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sEntryOrder = '<input type="text" name="txtEntryOrder" id="txtEntryOrder" class="form1" value="' . $iEntryOrder . '" size="10" />&nbsp;<span style="color:red;">*</span>';
								
				$sBank = '<div id="divBanks" style="display:inline;"><select name="selBank" id="selBank" onchange="GetBankAccounts(GetSelectedListBox(\'selBank\'),0);" class="Tahoma14">
				<option value="0">Select Bank </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B ORDER BY B.BankName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sBank .= '<option ' . (($iBnkId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . '</option>';
				$sBank .='</select></div>';				
				
				$sPaymentType = '<div id="divPaymentType" name="divPaymentType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selPaymentType" id="selPaymentType" class="Tahoma14" onchange="ChangePaymentType();">
				<option value="-1">Select Payment Type</option>';
				for($i = 0; $i < count($this->aPaymentType); $i++)
					$sPaymentType .= '<option value="' . $i . '">' . $this->aPaymentType[$i] . '</option>';
				$sPaymentType .='</select></div>';
				/*
				$sStationName = '<select name="selStation" id="selStation" class="Tahoma14">
				<option value="0"> Select Station</option>';
				$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.STATUS='1' ORDER BY S.StationName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult, $i, "S.StationId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "S.StationId") . '">' . $objDatabase->Result($varResult, $i, "S.StationName") . '</option>';
				$sStationName .='</select>';
				
				$sDeparmentName = '<select class="Tahoma14" name="selDepartment" id="selDepartment">
				<option value="0"> Select Department</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_departments AS D ORDER BY D.DepartmentName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sDeparmentName .= '<option ' . (($iDepartmentId == $objDatabase->Result($varResult2, $i, "D.DepartmentId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "D.DepartmentId") . '">' . $objDatabase->Result($varResult2, $i, "D.DepartmentName") . ' - ' . $objDatabase->Result($varResult2, $i, "D.DepartmentCode") . '</option>';
				$sDeparmentName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDepartment\'), \'../organization/departments.php?pagetype=details&id=\'+GetSelectedListBox(\'selDepartment\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Department Information" title="Department Information" border="0" /></a>';
				
				$sDonor = '<select name="selDonor" id="selDonor" onchange="GetDonorProjects(GetSelectedListBox(\'selDonor\'), 0);" class="Tahoma14">
				<option value="0">Select Donor</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sDonor .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
				$sDonor .='</select>';
								
				$sDonorProject = '<div id="divDonorProject" style="display:none;"><select name="selDonorProject" id="selDonorProject" onchange="GetProjectActivities(GetSelectedListBox(\'selDonorProject\'),0);" class="Tahoma14"></select>&nbsp;&raquo;&nbsp;</div>';
				$sProjectActivity = '<div id="divProjectActivity" style="display:none;"><select name="selProjectActivity" id="selProjectActivity" class="Tahoma14"></select></div>';
				$sDonorRow = '<tr id="trDonors" style="display:visible;" bgcolor="#edeff1"><td valign="top">Donor:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sDonor  . '&nbsp;&raquo;&nbsp;</td><td>' . $sDonorProject . '</td><td>' . $sProjectActivity . '</td></tr></table></td></tr>';
				
				// Donor Projects
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.Status='1'");
				$iNoOfDonorProjects = $objDatabase->RowsNumber($varResult);
				$sDonorProjects = 'var aDonorProjects = MultiDimensionalArray(' . $iNoOfDonorProjects . ', 4);';
				for ($i=0; $i < $iNoOfDonorProjects; $i++)
				{
					$sDonorProjects .= 'aDonorProjects[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "DP.DonorId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '";';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '";';
				}
				
				// Project Activities
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.Status='1'");
				$iNoOfProjectActivities = $objDatabase->RowsNumber($varResult);
				$sProjectActivities = 'var aProjectActivities= MultiDimensionalArray(' . $iNoOfProjectActivities . ', 4);';
				for ($i=0; $i < $iNoOfProjectActivities; $i++)
				{
					$sProjectActivities .= 'aProjectActivities[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "PA.DonorProjectId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "PA.ActivityId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityCode") . '";';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityTitle") . '";';
				}
				*/
				/*
				$sPaymentTo = '<div id="divPaymentTo" name="divPaymentTo" style="display:none;"><select name="selPaymentTo" id="selPaymentTo" class="Tahoma14" onchange="ChangePaymentTo();">
				<option value="-1">Select Payment To</option>';
				for($i = 0; $i < count($this->aPaymentTo); $i++)
					$sPaymentTo .= '<option value="' . $i . '">' . $this->aPaymentTo[$i] . '</option>';
				$sPaymentTo .='</select>&nbsp;&raquo;&nbsp;</div>';
				*/
				
				$sReceiptType = '<div id="divReceiptType" name="divReceiptType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selReceiptType" id="selReceiptType" class="Tahoma14" onchange="ChangeReceiptType();">
				<option value="-1">Select Receipt Type</option>';
				for($i = 0; $i < count($this->aReceiptType); $i++)
					$sReceiptType .= '<option value="' . $i . '">' . $this->aReceiptType[$i] . '</option>';
				$sReceiptType .='</select></div>';
				
				$sBankPaymentType = '<div id="divBankPaymentType" name="divBankPaymentType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selBankPaymentType" id="selBankPaymentType" class="Tahoma14" onchange="ChangeBankPaymentTypes(GetSelectedListBox(\'selBankPaymentType\'));">
				<option value="-1">Select Bank Payment Type</option>';
				for($i = 0; $i < count($this->aBankPaymentType); $i++)
					$sBankPaymentType .= '<option value="' . $i . '">' . $this->aBankPaymentType[$i] . '</option>';
				$sBankPaymentType .='</select></div>';
				
				$sBankRow = '<tr id="trBanks" style="display:none;" bgcolor="#edeff1"><td valign="top">Bank:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sBank  . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankAccount . '</td><td>' . $sBankPaymentType . '</td></tr></table></td></tr>';

				// Bank Accounts
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.Status='1'");
				$iNoOfBankAccounts = $objDatabase->RowsNumber($varResult);
				$sBankAccounts = 'var aBankAccounts = MultiDimensionalArray(' . $iNoOfBankAccounts . ', 4);';
				for ($i=0; $i < $iNoOfBankAccounts; $i++)
				{
					$sBankAccounts .= 'aBankAccounts[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BA.BankId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . '";';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "BA.AccountTitle") . '";';
				}
				
				// Bank Check Books
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BC WHERE BC.Status='1'");
				$iNoOfBankCheckBooks = $objDatabase->RowsNumber($varResult);
				$sBankCheckBooks = 'var aBankCheckBooks = MultiDimensionalArray(' . $iNoOfBankCheckBooks . ', 5);';
				for ($i=0; $i < $iNoOfBankCheckBooks; $i++)
				{
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BC.BankAccountId") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BC.BankCheckBookId") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BC.CheckPrefix") . '";';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][3] = ' . $objDatabase->Result($varResult, $i, "BC.CheckNumberStart") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][4] = ' . $objDatabase->Result($varResult, $i, "BC.CheckNumberEnd") . ';';
				}							
				
				$sEntryType = '<table border="0" cellspacing="0" cellpadding="0"><tr><td><select name="selEntryType" id="selEntryType" class="Tahoma14" onchange="ChangeEntryType();">
				<option value="-1">Select Entry Type</option>';
				for($i = 0; $i < count($this->aEntryType); $i++)
					$sEntryType .= '<option value="' . $i . '">' . $this->aEntryType[$i] . '</option>';
				$sEntryType .='</select></td><td>' . $sPaymentType . '</td><td>' . $sReceiptType . '</td></tr></table>
				
				<script language="JavaScript" type="text/javascript">
				' . $sBankAccounts . '
				' . $sBankCheckBooks . '				
				function ChangeEntryType()
				{
					document.getElementById("selBank").value = 0;
					document.getElementById("selBankAccount").value = 0;
					
					document.getElementById("selPaymentType").value = -1;
					document.getElementById("selReceiptType").value = -1;			
					
					HideDiv("trBanks");
        			//HideDiv("divPaymentTo");
					//HideDiv("divReceiptFrom");
        			HideDiv("divBankAccount");
					HideDiv("divBankPaymentType");
					
					if (GetSelectedListBox("selEntryType") == 0)
	        		{
	        			//document.getElementById("divPaymentTo").style.display = "inline";
						document.getElementById("divPaymentType").style.display = "inline";	        			
						//HideDiv("divReceiptFrom");
						HideDiv("divReceiptType");
	        		}
	        		else if (GetSelectedListBox("selEntryType") == 1)
	        		{
						document.getElementById("divPaymentType").style.display = "none";
	        			//document.getElementById("divReceiptFrom").style.display = "inline";
						document.getElementById("divReceiptType").style.display = "inline";
						document.getElementById("divCheckBook").style.display = "none";
						document.getElementById("trCheckBook").style.display = "none";
	        		}
					else if (GetSelectedListBox("selEntryType") == 2)
	        		{
						// Fill Out Reference No & Series												
						document.getElementById("divPaymentType").style.display = "none";
	        			//document.getElementById("divPaymentFrom").style.display = "none";
						//document.getElementById("divReceiptFrom").style.display = "none";
						document.getElementById("divCheckBook").style.display = "none";
						document.getElementById("trCheckBook").style.display = "none";
						document.getElementById("divBankAccount").style.display = "none";
					}
				}
	
				function ChangePaymentType()
				{				
	        		if ((GetSelectedListBox("selEntryType") == 0 ) && (GetSelectedListBox("selPaymentType") == 0 ))
	        		{
						ShowDiv("trBanks");						
						document.getElementById("selBank").value = 0;						
					}
					else if ((GetSelectedListBox("selEntryType") == 0 ) && (GetSelectedListBox("selPaymentType") == 1 ))
	        		{
						HideDiv("trBanks");
						HideDiv("divBankAccount");
						HideDiv("divCheckBook");
						HideDiv("trCheckBook");						
					}					
				}
				
				function ChangeReceiptType()
				{					
	        		if ((GetSelectedListBox("selEntryType") == 1) && (GetSelectedListBox("selReceiptType") == 0))
	        		{
						ShowDiv("trBanks");						
						document.getElementById("selBank").value = 0;						
					}
					else if ((GetSelectedListBox("selEntryType") == 1) && (GetSelectedListBox("selReceiptType") == 1))
	        		{
						HideDiv("trBanks");
						HideDiv("divBankAccount");
						HideDiv("divCheckBook");
						HideDiv("trCheckBook");						
					}					
				}			
				
				function ChangeRecipient(selRecipient, iRecipientNumber)
				{
					HideDiv("divVendor" + iRecipientNumber);
					HideDiv("divCustomer" + iRecipientNumber);
					HideDiv("divEmployee" + iRecipientNumber);
					
					if (selRecipient == 0)	// Vendor
						ShowDiv("divVendor" + iRecipientNumber);
					else if (selRecipient == 1)	// Customer
						ShowDiv("divCustomer" + iRecipientNumber);
					if (selRecipient == 2)	// Vendor
						ShowDiv("divEmployee" + iRecipientNumber);						
				}
				
				function GetBankPaymentTypes()
				{
					if ((GetSelectedListBox("selEntryType") == 0) && (GetSelectedListBox("selPaymentType") == 0))
						ShowDiv("divBankPaymentType");
					SetVal("txtCheckNumber", "");

					// Get Bank Account Series
					iEntryType = GetSelectedListBox("selEntryType");
					iReceiptType = GetSelectedListBox("selReceiptType");
					iPaymentType = GetSelectedListBox("selPaymentType");
					iBankAccountId = GetSelectedListBox("selBankAccount");					
				}				
				function ChangeBankPaymentTypes(iBankPaymentType)
				{					
					if (iBankPaymentType == 0)
					{
						ShowDiv("trCheckBook");
						ShowDiv("divCheckBook");
						GetCheckBooks(GetSelectedListBox(\'selBankAccount\'),0);
					}
					else
					{
						HideDiv("trCheckBook");
						HideDiv("divCheckBook");
					}	
				}
				
				function GetCheckBooks(iBankAccountId, iDefaultBankCheckBookId)
				{
					if ((GetSelectedListBox("selEntryType") == 0) && (GetSelectedListBox("selPaymentType") == 0))
					{
						OptionsList_RemoveAll("selCheckBook");						
						FillSelectBox("selCheckBook", "Select Check Book", 0);
						for (i=0; i < aBankCheckBooks.length; i++)
							if (aBankCheckBooks[i][0] == iBankAccountId)
	    						FillSelectBox("selCheckBook", aBankCheckBooks[i][2] + " (" + aBankCheckBooks[i][3] + " - " + aBankCheckBooks[i][4] + ")", aBankCheckBooks[i][1]);
			
						if (iDefaultBankCheckBookId > 0)
							document.getElementById("selCheckBook").value = iDefaultBankCheckBookId;

						//xajax_AJAX_FMS_Accounts_QuickEntry_FillCheckBooks(GetSelectedListBox("selBankAccount"), "selCheckBook", iDefaultBankCheckBookId);
					}
				}
				</script>';
								
				$sStatus = '<select class="form1" id="selStatus" name="selStatus">';
				for($i=0; $i < count($this->aStatus); $i++)									
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aStatus[$i] . '</option>';				
				$sStatus .='</select>';
				
				// General Journal Entries
				//AND CA.ChartOfAccountsCategoryId != '5'
				$varResult2 = $objDatabase->Query("SELECT *
				FROM fms_accounts_generaljournal_quickentries_entries AS QEE
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= QEE.ChartOfAccountsId
				LEFT JOIN organization_stations AS S ON S.StationId = QEE.StationId
				LEFT JOIN organization_departments AS DPT ON DPT.DepartmentId = QEE.DepartmentId
				LEFT JOIN fms_accounts_budget AS BJ ON BJ.BudgetId = QEE.BudgetId
				LEFT JOIN fms_accounts_donors AS D ON D.DonorId= QEE.DonorId
				LEFT JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId= QEE.DonorProjectId
				LEFT JOIN fms_accounts_donors_projects_activities AS PA ON PA.ActivityId= QEE.DonorProjectActivityId
				LEFT JOIN organization_employees AS E ON E.EmployeeId = QEE.EmployeeId
				LEFT JOIN fms_vendors AS V ON V.VendorId = QEE.VendorId
				LEFT JOIN fms_customers AS C ON C.CustomerId= QEE.CustomerId
				WHERE QEE.QuickEntryId= '$iQuickEntryId'
				ORDER BY QEE.QuickEntryEntryId");
				
				$sQuickEntryEntries = $sQuickEntryParticulars_Rows = "";				
				$sDonor = $sDonorProject = $sProjectActivities = $sChartOfAccount = "";
				$sBudget = "";				
				$bFirst = true;
				
				$dTotalDebit = $dTotalCredit = 0;
				for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'table-row';
					
					$iChartOfAccountId = $objDatabase->Result($varResult2, $k, "CA.ChartOfAccountsId");
					$sChartOfAccountCode = $objDatabase->Result($varResult2, $k, "CA.ChartOfAccountsCode");
					$iStationId = $objDatabase->Result($varResult2, $k, "QEE.StationId");					
					$sStationCode = $objDatabase->Result($varResult2, $k, "S.StationCode");
					$iDepartmentId = $objDatabase->Result($varResult2, $k, "QEE.DepartmentId");
					$sDepartmentCode = $objDatabase->Result($varResult2, $k, "DPT.DepartmentCode");
					$iBudgetId = $objDatabase->Result($varResult2, $k, "QEE.BudgetId");
					$sBudgetCode = $objDatabase->Result($varResult2, $k, "BJ.BudgetCode");
					$iDonorId = $objDatabase->Result($varResult2, $k, "QEE.DonorId");
					$iDonorProjectId = $objDatabase->Result($varResult2, $k, "QEE.DonorProjectId");
					$iDonorProjectActivityId = $objDatabase->Result($varResult2, $k, "QEE.DonorProjectActivityId");
					$iRecipient = $objDatabase->Result($varResult2, $k, "QEE.Recipient");
					
					$iVendorId = $objDatabase->Result($varResult2, $k, "QEE.VendorId");
					$iCustomerId = $objDatabase->Result($varResult2, $k, "QEE.CustomerId");
					$iEmployeeId = $objDatabase->Result($varResult2, $k, "QEE.EmployeeId");
										
					$sVendorCode =	$objDatabase->Result($varResult2, $k, "V.VendorCode");
					$sCustomerName = $objDatabase->Result($varResult2, $k, "C.FirstName") . ' '. $objDatabase->Result($varResult2, $k, "C.LastName");
				    $sEmployeeName = $objDatabase->Result($varResult2, $k, "E.FirstName") . ' '. $objDatabase->Result($varResult2, $k, "E.LastName");	
					
					$sDetail = $objDatabase->Result($varResult2, $k, "QEE.Detail");
					$dDebit = $objDatabase->Result($varResult2, $k, "QEE.Debit");
					$dCredit = $objDatabase->Result($varResult2, $k, "QEE.Credit");
					
					if ($k > 0) $bFirst = false;
										
					$sChartOfAccount[$k] = $objQuery->AutoCompleteLocal("ChartOfAccounts", "hdnChartOfAccountId_" . ($k+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sChartOfAccountCode, $iChartOfAccountId);
					$sStation = $objQueryStation->AutoCompleteLocal("Stations", "hdnStationId" . ($k+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sStationCode, $iStationId);
					$sDepartment = $objQueryDepartment->AutoCompleteLocal("Departments", "hdnDepartmentId" . ($k+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sDepartmentCode, $iDepartmentId);
					$sBudget = $objQueryBudget->AutoCompleteLocal("Budgets", "hdnBudgetId" . ($k+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sBudgetCode, $iBudgetId);
										
					$sVendor = '<div id="divVendor' . ($k+1) . '" name="divVendor' . ($k+1) . '" style="display: '.(($iRecipient == 0)? 'visible' : 'none'). ';">' . $objQueryVendor->AutoCompleteLocal("Vendors", "hdnVendorId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sVendorCode, $iVendorId) . '</div>';
					$sCustomer = '<div id="divCustomer' . ($k+1) . '" name="divCustomer' . ($k+1) . '" style="display:'.(($iRecipient == 1)? 'visible' : 'none'). ';">' . $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sCustomerName, $iCustomerId) . '</div>';
					$sEmployee = '<div id="divEmployee' . ($k+1) . '" name="divEmployee' . ($k+1) . '" style="display:'.(($iRecipient == 0)? 'visible' : 'none'). ';">' . $objQueryEmployee->AutoCompleteLocal("Employees", "hdnEmployeeId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst, $sEmployeeName, $iEmployeeId) . '</div>';

					$sRecipient[$k] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selRecipient' . ($k+1) . '" id="selRecipient' . ($k+1) . '" class="Tahoma14" onchange="ChangeRecipient(GetSelectedListBox(\'selRecipient' . ($k+1) . '\'), ' . ($k+1) . ');">
					<option value="-1">Select Recipient</option>';
					for($i = 0; $i < count($this->aRecipient); $i++)
						$sRecipient[$k] .= '<option '. (($iRecipient == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aRecipient[$i] . '</option>';
					$sRecipient[$k] .='</select></td><td>&raquo;</td><td>' . $sVendor . '</td><td>' . $sCustomer . '</td><td>' . $sEmployee . '</td></tr></table>';
					
					if($iDonorProjectId > 0)
					{
						$sDonorProject[$k] = '<div id="divDonorProject' . ($k+1) . '" style="display:visible;"><select name="selDonorProject' . ($k+1) . '" id="selDonorProject' . ($k+1) . '" onchange="GetProjectActivities_QuickEntries(GetSelectedListBox(\'selDonorProject' . ($k+1) . '\'), \'selProjectActivity' . ($k+1) . '\', \'divProjectActivity' . ($k+1) . '\');" class="Tahoma14">
						<option value="0">Select Donor</option>';
						$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.DonorId = '$iDonorId' ORDER BY DP.ProjectTitle");
						for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
							$sDonorProject[$k] = $sDonorProject[$k] . '<option '. (($iDonorProjectId == $objDatabase->Result($varResult, $i, "DP.DonorProjectId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '</option>';
						$sDonorProject[$k] = $sDonorProject[$k] . '</select></div>';
						
					}
					else
					{
						$sDonorProject[$k] = '<div id="divDonorProject' . ($k+1) . '" style="display:none;"><select name="selDonorProject' . ($k+1) . '" id="selDonorProject' . ($k+1) . '" onchange="GetProjectActivities_QuickEntries(GetSelectedListBox(\'selDonorProject' . ($k+1) . '\'), \'selProjectActivity' . ($k+1) . '\', \'divProjectActivity' . ($k+1) . '\');" class="Tahoma14"></select></div>';
					}
					if($iDonorProjectActivityId > 0)
					{
						$sProjectActivity[$k] = '<div id="divProjectActivity' . ($k+1) . '" style="display:visible;"><select name="selProjectActivity' . ($k+1) . '" id="selProjectActivity' . ($k+1) . '" class="Tahoma14">
						<option value="0">Select Donor</option>';
						$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.DonorProjectId = '$iDonorProjectId' ORDER BY PA.ActivityTitle");
						for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
							$sProjectActivity[$k] = $sProjectActivity[$k] . '<option '. (($iDonorProjectActivityId == $objDatabase->Result($varResult, $i, "PA.ActivityId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "PA.ActivityId") . '">' . $objDatabase->Result($varResult, $i, "PA.ActivityCode") . '</option>';
						$sProjectActivity[$k] = $sProjectActivity[$k] . '</select></div>';						
					}
					else
					{
						$sProjectActivity[$k] = '<div id="divProjectActivity' . ($k+1) . '" style="display:none;"><select name="selProjectActivity' . ($k+1) . '" id="selProjectActivity' . ($k+1) . '" class="Tahoma14"></select></div>';
					}
					
					$sDonor[$k] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selDonor' . ($k+1) . '" id="selDonor' . ($k+1) . '" onchange="GetDonorProjects_QuickEntries(GetSelectedListBox(\'selDonor' . ($k+1) . '\'), \'selDonorProject' . ($k+1) . '\', \'selProjectActivity' . ($k+1) . '\', \'divDonorProject' . ($k+1) . '\');" class="Tahoma14">
					<option value="0">Select Donor</option>';
					$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
						$sDonor[$k] = $sDonor[$k] . '<option '. (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
					$sDonor[$k] = $sDonor[$k] . '</select></td><td>&raquo;</td><td>' . $sDonorProject[$k] . '</td><td>&raquo;</td><td>' . $sProjectActivity[$k] . '</td></tr></table>';
					
					$sQuickEntryParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sChartOfAccount[$k] . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sStation . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sDepartment . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sBudget . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sDonor[$k] . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sRecipient[$k] . '</td>
 					 <td valign="top"><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtQuickEntryParticulars_Detail_' . ($k+1) . '" id="txtQuickEntryParticulars_Detail_' . ($k+1) . '" value="'. $sDetail . '" size="40%" /></td>
					 <td valign="top" align="center"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtQuickEntryParticulars_Debit_' . ($k+1) . '" id="txtQuickEntryParticulars_Debit_' . ($k+1) . '" value="'. $dDebit . '" size="10" /></td>
					 <td valign="top" align="center"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtQuickEntryParticulars_Credit_' . ($k+1) . '" id="txtQuickEntryParticulars_Credit_' . ($k+1) . '" value="'. $dCredit . '" size="10" /></td>
					 <td valign="top" align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
					</tr>';
				}
				
				for ($j=$k; $j < 10; $j++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'none';										
					$bFirst = false;
					
					$sChartOfAccount[$j] = $objQuery->AutoCompleteLocal("ChartOfAccounts", "hdnChartOfAccountId_" . ($j+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					$sStation = $objQueryStation->AutoCompleteLocal("Stations", "hdnStationId" . ($j+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					$sDepartment = $objQueryDepartment->AutoCompleteLocal("Departments", "hdnDepartmentId" . ($j+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					$sBudget = $objQueryStation->AutoCompleteLocal("Budgets", "hdnBudgetId" . ($j+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					
					$sVendor = '<div id="divVendor' . ($j+1) . '" name="divVendor' . ($j+1) . '" style="display:none;">' . $objQueryVendor->AutoCompleteLocal("Vendors", "hdnVendorId" . ($j+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';
					$sCustomer = '<div id="divCustomer' . ($j+1) . '" name="divCustomer' . ($j+1) . '" style="display:none;">' . $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId" . ($j+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';
					$sEmployee = '<div id="divEmployee' . ($j+1) . '" name="divEmployee' . ($j+1) . '" style="display:none;">' . $objQueryEmployee->AutoCompleteLocal("Employees", "hdnEmployeeId" . ($j+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';

					$sRecipient[$j] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selRecipient' . ($j+1) . '" id="selRecipient' . ($j+1) . '" class="Tahoma14" onchange="ChangeRecipient(GetSelectedListBox(\'selRecipient' . ($j+1) . '\'), ' . ($j+1) . ');">
					<option value="-1">Select Recipient</option>';
					for($i = 0; $i < count($this->aRecipient); $i++)
						$sRecipient[$j] .= '<option value="' . $i . '">' . $this->aRecipient[$i] . '</option>';
					$sRecipient[$j] .='</select></td><td>&raquo;</td><td>' . $sVendor . '</td><td>' . $sCustomer . '</td><td>' . $sEmployee . '</td></tr></table>';
					
					$sDonorProject[$j] = '<div id="divDonorProject' . ($j+1) . '" style="display:none;"><select name="selDonorProject' . ($j+1) . '" id="selDonorProject' . ($j+1) . '" onchange="GetProjectActivities(GetSelectedListBox(\'selDonorProject' . ($j+1) . '\'), \'selProjectActivity' . ($j+1) . '\', \'divProjectActivity' . ($j+1) . '\');" class="Tahoma14"></select></div>';
					$sProjectActivity[$j] = '<div id="divProjectActivity' . ($j+1) . '" style="display:none;"><select name="selProjectActivity' . ($j+1) . '" id="selProjectActivity' . ($j+1) . '" class="Tahoma14"></select></div>';
					
					$sDonor[$j] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selDonor' . ($j+1) . '" id="selDonor' . ($j+1) . '" onchange="GetDonorProjects(GetSelectedListBox(\'selDonor' . ($j+1) . '\'), \'selDonorProject' . ($j+1) . '\', \'selProjectActivity' . ($j+1) . '\', \'divDonorProject' . ($j+1) . '\');" class="Tahoma14">
					<option value="0">Select Donor</option>';					
					$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
						$sDonor[$j] .= '<option value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
					$sDonor[$j] .='</select></td><td>&raquo;</td><td>' . $sDonorProject[$j] . '</td><td>&raquo;</td><td>' . $sProjectActivity[$j] . '</td></tr></table>';
									
					$sQuickEntryParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sChartOfAccount[$j] . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sStation . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sDepartment . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sBudget . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sDonor[$j] . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sRecipient[$j] . '</td>
 					 <td valign="top"><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtQuickEntryParticulars_Detail_' . ($j+1) . '" id="txtQuickEntryParticulars_Detail_' . ($j+1) . '" size="40%" /></td>
					 <td valign="top" align="center"><input onkeyup="UpdateTotal(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtQuickEntryParticulars_Debit_' . ($j+1) . '" id="txtQuickEntryParticulars_Debit_' . ($j+1) . '" size="10" /></td>
					 <td valign="top" align="center"><input onkeyup="UpdateTotal(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtQuickEntryParticulars_Credit_' . ($j+1) . '" id="txtQuickEntryParticulars_Credit_' . ($j+1) . '" size="10" /></td>
					 <td valign="top" align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
					</tr>';
				}
				$sQuickEntryEntries = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Chart of Accounts</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Station</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Department</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Budget</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Donor</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Recipient</span></td>
				  <td align="left"><span class="WhiteHeading">Description</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Debit</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Credit</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				 ' . $sQuickEntryParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="11">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="8" style="font-size:13px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
				  <td align="center" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divDebitTotalAmount" name="divDebitTotalAmount">'. number_format($dTotalDebit, 0) . '</div></td>
				  <td align="center" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divCreditTotalAmount" name="divCreditTotalAmount">'. number_format($dTotalCredit, 0). '</div></td>
				 </tr>
				</table>
				<input type="hidden" name="hdnApplyRestriction" id="hdnApplyRestriction" value="' . $iApplyRestriction . '" /><input type="hidden" name="hdnRestrictionAmount" id="hdnRestrictionAmount" value="" />
				<input type="hidden" name="TotalDebit" id="TotalDebit" value="" /><input type="hidden" name="TotalCredit" id="TotalCredit" value="" />
				<input type="hidden" name="popup" id="popup" value="' . $bPopup . '" />
				<script language="JavaScript" type="text/javascript">
					var iRowCounter = ' . ($k+1) . ';
					var iRowVisible = ' . ($k+1) . ';
					UpdateTotal();				
				</script>';				
												
			}
			else if  ($sAction2 == "addnew")
			{
				$iQuickEntryId = "";
				$iEntryType = 2;
				$iPaymentType = 1;
				$iReceiptType = 1;
				$iStatus = 1;
				$sTitle = $objGeneral->fnGet("txtTitle");
				$iEntryOrder = $objGeneral->fnGet("txtEntryOrder");
				
				$sReturn .= '<form id="frmEntry" name="frmEntry" method="post" action="" onsubmit="return ValidateForm();">';
				/*
				$sStationName = '<select name="selStation" id="selStation" class="Tahoma14">
				<option value="0"> Select Station</option>';
				$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.STATUS='1' ORDER BY S.StationName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult, $i, "S.StationId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "S.StationId") . '">' . $objDatabase->Result($varResult, $i, "S.StationName") . '</option>';
				$sStationName .='</select>';
				
				$sDeparmentName = '<select class="Tahoma14" name="selDepartment" id="selDepartment">
				<option value="0"> Select Department</option>';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_departments AS D ORDER BY D.DepartmentName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sDeparmentName .= '<option ' . (($iDepartmentId == $objDatabase->Result($varResult2, $i, "D.DepartmentId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "D.DepartmentId") . '">' . $objDatabase->Result($varResult2, $i, "D.DepartmentName") . ' - ' . $objDatabase->Result($varResult2, $i, "D.DepartmentCode") . '</option>';
				$sDeparmentName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDepartment\'), \'../organization/departments.php?pagetype=details&id=\'+GetSelectedListBox(\'selDepartment\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Department Information" title="Department Information" border="0" /></a>';
				
				$sDonor = '<select name="selDonor" id="selDonor" onchange="GetDonorProjects(GetSelectedListBox(\'selDonor\'),0);" class="Tahoma14">
				<option value="0">Select Donor</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sDonor .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
				$sDonor .='</select>';
				
				$sDonorProject = '<div id="divDonorProject" style="display:none;"><select name="selDonorProject" id="selDonorProject" onchange="GetProjectActivities(GetSelectedListBox(\'selDonorProject\'),0);" class="Tahoma14"></select>&nbsp;&raquo;&nbsp;</div>';
				$sProjectActivity = '<div id="divProjectActivity" style="display:none;"><select name="selProjectActivity" id="selProjectActivity" class="Tahoma14"></select></div>';
				$sDonorRow = '<tr id="trDonors" style="display:visible;" bgcolor="#edeff1"><td valign="top">Donor:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sDonor  . '&nbsp;&raquo;&nbsp;</td><td>' . $sDonorProject . '</td><td>' . $sProjectActivity . '</td></tr></table></td></tr>';
				// Donor Projects
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.Status='1'");
				$iNoOfDonorProjects = $objDatabase->RowsNumber($varResult);
				$sDonorProjects = 'var aDonorProjects = MultiDimensionalArray(' . $iNoOfDonorProjects . ', 4);';
				for ($i=0; $i < $iNoOfDonorProjects; $i++)
				{
					$sDonorProjects .= 'aDonorProjects[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "DP.DonorId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '";';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '";';
				}
				
				// Project Activities
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.Status='1'");
				$iNoOfProjectActivities = $objDatabase->RowsNumber($varResult);
				$sProjectActivities = 'var aProjectActivities= MultiDimensionalArray(' . $iNoOfProjectActivities . ', 4);';
				for ($i=0; $i < $iNoOfProjectActivities; $i++)
				{
					$sProjectActivities .= 'aProjectActivities[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "PA.DonorProjectId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "PA.ActivityId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityCode") . '";';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityTitle") . '";';
				}
				*/
				$sBankAccount = '<div id="divBankAccount" style="display:none;"><select name="selBankAccount" id="selBankAccount" onchange="GetBankPaymentTypes();" class="Tahoma14"></select>';
				$sCheckBook = '<div id="divCheckBook" style="display:none;"><select name="selCheckBook" id="selCheckBook" class="Tahoma14"></select>';
				$sBankCheckBook = '<tr bgcolor="#ffffff" id="trCheckBook" style="display:none;"><td valign="top">Check Book:</td><td>' . $sCheckBook . '</td></tr>';
				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sEntryOrder = '<input type="text" name="txtEntryOrder" id="txtEntryOrder" class="form1" value="' . $iEntryOrder . '" size="10" />&nbsp;<span style="color:red;">*</span>';
								
				$sBank = '<div id="divBanks" style="display:inline;"><select name="selBank" id="selBank" onchange="GetBankAccounts(GetSelectedListBox(\'selBank\'),0);" class="Tahoma14">
				<option value="0">Select Bank </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B ORDER BY B.BankName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sBank .= '<option ' . (($iBnkId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . '</option>';
				$sBank .='</select></div>';				
				
				$sPaymentType = '<div id="divPaymentType" name="divPaymentType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selPaymentType" id="selPaymentType" class="Tahoma14" onchange="ChangePaymentType();">
				<option value="-1">Select Payment Type</option>';
				for($i = 0; $i < count($this->aPaymentType); $i++)
					$sPaymentType .= '<option value="' . $i . '">' . $this->aPaymentType[$i] . '</option>';
				$sPaymentType .='</select></div>';
				
				/*
				$sPaymentTo = '<div id="divPaymentTo" name="divPaymentTo" style="display:none;"><select name="selPaymentTo" id="selPaymentTo" class="Tahoma14" onchange="ChangePaymentTo();">
				<option value="-1">Select Payment To</option>';
				for($i = 0; $i < count($this->aPaymentTo); $i++)
					$sPaymentTo .= '<option value="' . $i . '">' . $this->aPaymentTo[$i] . '</option>';
				$sPaymentTo .='</select>&nbsp;&raquo;&nbsp;</div>';
				*/
				
				$sReceiptType = '<div id="divReceiptType" name="divReceiptType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selReceiptType" id="selReceiptType" class="Tahoma14" onchange="ChangeReceiptType();">
				<option value="-1">Select Receipt Type</option>';
				for($i = 0; $i < count($this->aReceiptType); $i++)
					$sReceiptType .= '<option value="' . $i . '">' . $this->aReceiptType[$i] . '</option>';
				$sReceiptType .='</select></div>';
				
				$sBankPaymentType = '<div id="divBankPaymentType" name="divBankPaymentType" style="display:none;">&nbsp;&raquo;&nbsp;<select name="selBankPaymentType" id="selBankPaymentType" class="Tahoma14" onchange="ChangeBankPaymentTypes(GetSelectedListBox(\'selBankPaymentType\'));">
				<option value="-1">Select Bank Payment Type</option>';
				for($i = 0; $i < count($this->aBankPaymentType); $i++)
					$sBankPaymentType .= '<option value="' . $i . '">' . $this->aBankPaymentType[$i] . '</option>';
				$sBankPaymentType .='</select></div>';
				
				$sBankRow = '<tr id="trBanks" style="display:none;" bgcolor="#edeff1"><td valign="top">Bank:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sBank  . '&nbsp;&raquo;&nbsp;</td><td>' . $sBankAccount . '</td><td>' . $sBankPaymentType . '</td></tr></table></td></tr>';

				// Bank Accounts
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.Status='1'");
				$iNoOfBankAccounts = $objDatabase->RowsNumber($varResult);
				$sBankAccounts = 'var aBankAccounts = MultiDimensionalArray(' . $iNoOfBankAccounts . ', 4);';
				for ($i=0; $i < $iNoOfBankAccounts; $i++)
				{
					$sBankAccounts .= 'aBankAccounts[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BA.BankId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . '";';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "BA.AccountTitle") . '";';
				}
				
				// Bank Check Books
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BC WHERE BC.Status='1'");
				$iNoOfBankCheckBooks = $objDatabase->RowsNumber($varResult);
				$sBankCheckBooks = 'var aBankCheckBooks = MultiDimensionalArray(' . $iNoOfBankCheckBooks . ', 5);';
				for ($i=0; $i < $iNoOfBankCheckBooks; $i++)
				{
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BC.BankAccountId") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BC.BankCheckBookId") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BC.CheckPrefix") . '";';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][3] = ' . $objDatabase->Result($varResult, $i, "BC.CheckNumberStart") . ';';
					$sBankCheckBooks .= 'aBankCheckBooks[' . $i . '][4] = ' . $objDatabase->Result($varResult, $i, "BC.CheckNumberEnd") . ';';
				}							
				
				$sEntryType = '<table border="0" cellspacing="0" cellpadding="0"><tr><td><select name="selEntryType" id="selEntryType" class="Tahoma14" onchange="ChangeEntryType();">
				<option value="-1">Select Entry Type</option>';
				for($i = 0; $i < count($this->aEntryType); $i++)
					$sEntryType .= '<option value="' . $i . '">' . $this->aEntryType[$i] . '</option>';
				$sEntryType .='</select></td><td>' . $sPaymentType . '</td><td>' . $sReceiptType . '</td></tr></table>				
				<script language="JavaScript" type="text/javascript">
				' . $sBankAccounts . '
				' . $sBankCheckBooks . '				
				function ChangeEntryType()
				{
					document.getElementById("selBank").value = 0;
					document.getElementById("selBankAccount").value = 0;
					
					document.getElementById("selPaymentType").value = -1;
					document.getElementById("selReceiptType").value = -1;			
					
					HideDiv("trBanks");
        			//HideDiv("divPaymentTo");
					//HideDiv("divReceiptFrom");
        			HideDiv("divBankAccount");
					HideDiv("divBankPaymentType");
					
					if (GetSelectedListBox("selEntryType") == 0)
	        		{
	        			//document.getElementById("divPaymentTo").style.display = "inline";
						document.getElementById("divPaymentType").style.display = "inline";	        			
						//HideDiv("divReceiptFrom");
						HideDiv("divReceiptType");
	        		}
	        		else if (GetSelectedListBox("selEntryType") == 1)
	        		{
						document.getElementById("divPaymentType").style.display = "none";
	        			//document.getElementById("divReceiptFrom").style.display = "inline";
						document.getElementById("divReceiptType").style.display = "inline";
						document.getElementById("divCheckBook").style.display = "none";
						document.getElementById("trCheckBook").style.display = "none";
	        		}
					else if (GetSelectedListBox("selEntryType") == 2)
	        		{
						// Fill Out Reference No & Series												
						document.getElementById("divPaymentType").style.display = "none";
	        			//document.getElementById("divPaymentFrom").style.display = "none";
						//document.getElementById("divReceiptFrom").style.display = "none";
						document.getElementById("divCheckBook").style.display = "none";
						document.getElementById("trCheckBook").style.display = "none";
						document.getElementById("divBankAccount").style.display = "none";
					}
				}
	
				function ChangePaymentType()
				{				
	        		if ((GetSelectedListBox("selEntryType") == 0 ) && (GetSelectedListBox("selPaymentType") == 0 ))
	        		{
						ShowDiv("trBanks");						
						document.getElementById("selBank").value = 0;						
					}
					else if ((GetSelectedListBox("selEntryType") == 0 ) && (GetSelectedListBox("selPaymentType") == 1 ))
	        		{
						HideDiv("trBanks");
						HideDiv("divBankAccount");
						HideDiv("divCheckBook");
						HideDiv("trCheckBook");						
					}					
				}
				
				function ChangeReceiptType()
				{					
	        		if ((GetSelectedListBox("selEntryType") == 1) && (GetSelectedListBox("selReceiptType") == 0))
	        		{
						ShowDiv("trBanks");						
						document.getElementById("selBank").value = 0;						
					}
					else if ((GetSelectedListBox("selEntryType") == 1) && (GetSelectedListBox("selReceiptType") == 1))
	        		{
						HideDiv("trBanks");
						HideDiv("divBankAccount");
						HideDiv("divCheckBook");
						HideDiv("trCheckBook");						
					}					
				}			
				
				function ChangeRecipient(selRecipient, iRecipientNumber)
				{
					HideDiv("divVendor" + iRecipientNumber);
					HideDiv("divCustomer" + iRecipientNumber);
					HideDiv("divEmployee" + iRecipientNumber);
					
					if (selRecipient == 0)	// Vendor
						ShowDiv("divVendor" + iRecipientNumber);
					else if (selRecipient == 1)	// Customer
						ShowDiv("divCustomer" + iRecipientNumber);
					if (selRecipient == 2)	// Vendor
						ShowDiv("divEmployee" + iRecipientNumber);						
				}
				
				function GetBankPaymentTypes()
				{
					if ((GetSelectedListBox("selEntryType") == 0) && (GetSelectedListBox("selPaymentType") == 0))
						ShowDiv("divBankPaymentType");
					SetVal("txtCheckNumber", "");

					// Get Bank Account Series
					iEntryType = GetSelectedListBox("selEntryType");
					iReceiptType = GetSelectedListBox("selReceiptType");
					iPaymentType = GetSelectedListBox("selPaymentType");
					iBankAccountId = GetSelectedListBox("selBankAccount");					
				}				
				function ChangeBankPaymentTypes(iBankPaymentType)
				{					
					if (iBankPaymentType == 0)
					{
						ShowDiv("trCheckBook");
						ShowDiv("divCheckBook");
						GetCheckBooks(GetSelectedListBox(\'selBankAccount\'),0);
					}
					else
					{
						HideDiv("trCheckBook");
						HideDiv("divCheckBook");
					}	
				}
				
				function GetCheckBooks(iBankAccountId, iDefaultBankCheckBookId)
				{					
					if ((GetSelectedListBox("selEntryType") == 0) && (GetSelectedListBox("selPaymentType") == 0))
					{
						OptionsList_RemoveAll("selCheckBook");						
						FillSelectBox("selCheckBook", "Select Check Book", 0);
						for (i=0; i < aBankCheckBooks.length; i++)
							if (aBankCheckBooks[i][0] == iBankAccountId)
	    						FillSelectBox("selCheckBook", aBankCheckBooks[i][2] + " (" + aBankCheckBooks[i][3] + " - " + aBankCheckBooks[i][4] + ")", aBankCheckBooks[i][1]);
			
						if (iDefaultBankCheckBookId > 0)
							document.getElementById("selCheckBook").value = iDefaultBankCheckBookId;

						//xajax_AJAX_FMS_Accounts_QuickEntry_FillCheckBooks(GetSelectedListBox("selBankAccount"), "selCheckBook", iDefaultBankCheckBookId);
					}
				}
				</script>';				
				
				$sAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				$sAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
                $sAddedBy = $sAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(str_replace('"', '', $sAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sStatus = '<select class="form1" id="selStatus" name="selStatus">';
				for($i=0; $i < count($this->aStatus); $i++)									
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aStatus[$i] . '</option>';				
				$sStatus .='</select>';
				
				$bFirst = true;
				$sDonor = $sDonorProject = $sProjectActivities = $sChartOfAccount = $sBudget = "";				
				for ($k=0; $k < 10; $k++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = (($k <= 4) ? 'table-row' : 'none');			
					
					if ($k > 0) $bFirst = false;
					
					$sChartOfAccount[$k] = $objQuery->AutoCompleteLocal("ChartOfAccounts", "hdnChartOfAccountId_" . ($k+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					$sStation = $objQueryStation->AutoCompleteLocal("Stations", "hdnStationId" . ($k+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					$sDepartment = $objQueryDepartment->AutoCompleteLocal("Departments", "hdnDepartmentId" . ($k+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					$sBudget = $objQueryBudget->AutoCompleteLocal("Budgets", "hdnBudgetId" . ($k+1), "width:100px; font-family:Tahoma,Arial; font-size:14px;", $bFirst);
					
					$sVendor = '<div id="divVendor' . ($k+1) . '" name="divVendor' . ($k+1) . '" style="display:none;">' . $objQueryVendor->AutoCompleteLocal("Vendors", "hdnVendorId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';
					$sCustomer = '<div id="divCustomer' . ($k+1) . '" name="divCustomer' . ($k+1) . '" style="display:none;">' . $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';
					$sEmployee = '<div id="divEmployee' . ($k+1) . '" name="divEmployee' . ($k+1) . '" style="display:none;">' . $objQueryEmployee->AutoCompleteLocal("Employees", "hdnEmployeeId" . ($k+1), "width:120px; font-family:Tahoma,Arial; font-size:14px;", $bFirst) . '</div>';

					$sRecipient[$k] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selRecipient' . ($k+1) . '" id="selRecipient' . ($k+1) . '" class="Tahoma14" onchange="ChangeRecipient(GetSelectedListBox(\'selRecipient' . ($k+1) . '\'), ' . ($k+1) . ');">
					<option value="-1">Select Recipient</option>';
					for($i = 0; $i < count($this->aRecipient); $i++)
						$sRecipient[$k] .= '<option value="' . $i . '">' . $this->aRecipient[$i] . '</option>';
					$sRecipient[$k] .='</select></td><td>&raquo;</td><td>' . $sVendor . '</td><td>' . $sCustomer . '</td><td>' . $sEmployee . '</td></tr></table>';
					
					$sDonorProject[$k] = '<div id="divDonorProject' . ($k+1) . '" style="display:none;"><select name="selDonorProject' . ($k+1) . '" id="selDonorProject' . ($k+1) . '" onchange="GetProjectActivities_QuickEntries(GetSelectedListBox(\'selDonorProject' . ($k+1) . '\'), \'selProjectActivity' . ($k+1) . '\', \'divProjectActivity' . ($k+1) . '\');" class="Tahoma14"></select></div>';
					$sProjectActivity[$k] = '<div id="divProjectActivity' . ($k+1) . '" style="display:none;"><select name="selProjectActivity' . ($k+1) . '" id="selProjectActivity' . ($k+1) . '" class="Tahoma14"></select></div>';
					
					$sDonor[$k] = '<table border="0" cellspacing="0" cellpaddign="0" width="80%" align="left"><tr><td width="10%"><select name="selDonor' . ($k+1) . '" id="selDonor' . ($k+1) . '" onchange="GetDonorProjects_QuickEntries(GetSelectedListBox(\'selDonor' . ($k+1) . '\'), \'selDonorProject' . ($k+1) . '\', \'selProjectActivity' . ($k+1) . '\', \'divDonorProject' . ($k+1) . '\');" class="Tahoma14">
					<option value="0">Select Donor</option>';					
					$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
						$sDonor[$k] .= '<option value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
					$sDonor[$k] .='</select></td><td>&raquo;</td><td>' . $sDonorProject[$k] . '</td><td>&raquo;</td><td>' . $sProjectActivity[$k] . '</td></tr></table>';
									
					$sQuickEntryParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sChartOfAccount[$k] . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sStation . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sDepartment . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sBudget . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sDonor[$k] . '</td>
					 <td valign="top" align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . $sRecipient[$k] . '</td>
 					 <td valign="top"><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtQuickEntryParticulars_Detail_' . ($k+1) . '" id="txtQuickEntryParticulars_Detail_' . ($k+1) . '" size="40%" /></td>
					 <td valign="top" align="center"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtQuickEntryParticulars_Debit_' . ($k+1) . '" id="txtQuickEntryParticulars_Debit_' . ($k+1) . '" size="10" /></td>
					 <td valign="top" align="center"><input onkeyup="UpdateTotal(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtQuickEntryParticulars_Credit_' . ($k+1) . '" id="txtQuickEntryParticulars_Credit_' . ($k+1) . '" size="10" /></td>
					 <td valign="top" align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
					</tr>';
				}
				
				$sQuickEntryEntries = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Chart of Accounts</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Station</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Department</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Budget</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Donor</span></td>
				  <td style="width:100px;"><span class="WhiteHeading">Recipient</span></td>
				  <td align="left"><span class="WhiteHeading">Description</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Debit</span></td>
				  <td style="width:80px;" align="center"><span class="WhiteHeading">Credit</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				 ' . $sQuickEntryParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="11">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="8" style="font-size:13px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
				  <td align="center" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divDebitTotalAmount" name="divDebitTotalAmount"></div></td>
				  <td align="center" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divCreditTotalAmount" name="divCreditTotalAmount"></div></td>
				 </tr>
				</table>
				<input type="hidden" name="hdnApplyRestriction" id="hdnApplyRestriction" value="' . $iApplyRestriction . '" /><input type="hidden" name="hdnRestrictionAmount" id="hdnRestrictionAmount" value="' . $dRestrictionAmount . '" />
				<input type="hidden" name="TotalDebit" id="TotalDebit" value="" /><input type="hidden" name="TotalCredit" id="TotalCredit" value="" />
				<input type="hidden" name="popup" id="popup" value="' . $bPopup . '" />
				<script language="JavaScript" type="text/javascript">
				var iRowCounter = ' . $k . ';
				var iRowVisible = 6;
				</script>';
			}
			
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			var iRowCounter = ' . $k . ';
			var iRowVisible = 6;
			function ValidateForm()
			{	
				if ((GetVal(\'txtTitle\') == "")) return(AlertFocus(\'Please enter valid Title\', \'txtTitle\'));
				if (!isNumeric(GetVal("txtEntryOrder"))) return(AlertFocus(\'Please enter a Numeric Value for Entry Order\', "txtEntryOrder"));
				//if(GetSelectedListBox(\'selStation\') <= 0) return(AlertFocus(\'Please select Station\', \'selStation\'));
				//if(GetSelectedListBox(\'selDepartment\') <= 0) return(AlertFocus(\'Please select Department\', \'selDepartment\'));
				if(GetSelectedListBox(\'selEntryType\') < 0) return(AlertFocus(\'Please select Entry Type\', \'selEntryType\'));
				if (GetVal(\'TotalDebit\') != GetVal(\'TotalCredit\')) return(AlertFocus(\'Debit and Creidt amount must be equal\', \'txtQuickEntryParticulars_Debit_1\'));
				if ((GetVal(\'txtQuickEntryParticulars_Debit_1\') == "") && (GetVal(\'txtQuickEntryParticulars_Credit_1\') == "")) return(AlertFocus(\'You can not do any transaction without any single entry\', \'txtQuickEntryParticulars_Debit_1\'));
				
				if ((GetVal(\'hdnChartOfAccountId_1\') == "") || (GetVal(\'hdnChartOfAccountId_2\') == "")) return(AlertFocus(\'You can not post transaction without balancing \', \'txtChartOfAccount_1\'));
				
				if(confirm(\'Are you sure, you want to add Quick Entry?\'))
					return(true);
				
				return(false);
			}
			
			function UpdateTotal()
			{
				var dTotalDebit = 0;
				var dTotalCredit = 0;
				for (i=0; i < iRowCounter; i++)
				{					
					dDebit = Number(GetVal("txtQuickEntryParticulars_Debit_" + i));
					dCredit = Number(GetVal("txtQuickEntryParticulars_Credit_" + i));
					
					// Only allow Numbers					
					if(dDebit != "")
						if (!isNumeric(GetVal("txtQuickEntryParticulars_Debit_" + i))) return(AlertFocus(\'Please enter a Numeric Value\', "txtQuickEntryParticulars_Debit_" + i));
					if(dCredit != "")
						if (!isNumeric(GetVal("txtQuickEntryParticulars_Credit_" + i))) return(AlertFocus(\'Please enter a Numeric Value\', "txtQuickEntryParticulars_Credit_" + i));	
					// End
						
					if((dDebit > 0 ) && (dCredit > 0))
					{
						dTotalDebit = 0;
						dTotalCredit = 0;
						alert("Please enter only debit or credit in one row, but both!");
						break;
					}
					dTotalDebit += dDebit;
					dTotalCredit += dCredit;					
				}		
				
				SetDivVal("divDebitTotalAmount", addCommas(dTotalDebit));
				SetDivVal("divCreditTotalAmount", addCommas(dTotalCredit));
				SetVal("TotalDebit", dTotalDebit);
				SetVal("TotalCredit", dTotalCredit);				
			}
			function LoadQuickEntry(iQuickEntryId)
			{
				xajax_AJAX_FMS_Accounts_QuickEntries_LoadQuickEntry(iQuickEntryId);
			}
			</script>			
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Quick Entry Information:</span></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top">Title:</td><td><strong>' . $sTitle . '</tr></td></tr>
			 <tr bgcolor="#edeff1"><td>Entry Order:</td><td><strong>' . $sEntryOrder . '</tr></td></tr>
			 <!--
			 <tr bgcolor="#ffffff"><td>Station:</td><td><strong>' . $sStationName . '</tr></td></tr>
			 <tr bgcolor="#edeff1"><td>Department:</td><td><strong>' . $sDeparmentName . '</tr></td></tr>
			 -->
			 <tr bgcolor="#ffffff"><td>Entry Type:</td><td><strong>' . $sEntryType . '</tr></td></tr>
			 ' . $sBankRow . ' '
			  . $sBankCheckBook
			  . '
			 <!--<tr bgcolor="#ffffff"><td valign="top">Source of Income:</td><td><strong>' . $sSourceOfIncome . '</tr></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Budget:</td><td><strong>' . $sBudget . '</tr></td></tr>
			 ' . $sDonorRow. '
			 -->
			 <tr bgcolor="#ffffff"><td valign="top">Status:</td><td><strong>' . $sStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Quick Entry Entries:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="2">' . $sQuickEntryEntries . '</td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top">Added On:</td><td><strong>' . $sAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Added By:</td><td><strong>' . $sAddedBy . '</strong></td></tr>
			</table>';
			
			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iQuickEntryId . '"><input type="hidden" name="action" id="action" value="UpdateQuickEntry"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>
				<script language="JavaScript" type="text/javascript">
				iQuickEntryId = GetVal("id");
				 LoadQuickEntry(iQuickEntryId)
				</script>';
				
			else if  ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add New" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewQuickEntry"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	/* AJAX Functions */
	
	function AJAX_LoadQuickEntry($iQuickEntryId)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_quickentries AS QE WHERE QE.QuickEntryId='$iQuickEntryId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$iQuickEntryId = $objDatabase->Result($varResult, 0, "QE.QuickEntryId");
			$iEntryType = $objDatabase->Result($varResult, 0, "QE.EntryType");
			$iPaymentType = $objDatabase->Result($varResult, 0, "QE.PaymentType");
			$iReceiptType = $objDatabase->Result($varResult, 0, "QE.ReceiptType");
			$iBankPaymentType = $objDatabase->Result($varResult, 0, "QE.BankPaymentType");
			$iBankId = $objDatabase->Result($varResult, 0, "QE.BankId");
			$iBankAccountId = $objDatabase->Result($varResult, 0, "QE.BankAccountId");
			$iBankCheckBookId = $objDatabase->Result($varResult, 0, "QE.BankCheckBookId");
			/*
			$iBudgetId = $objDatabase->Result($varResult, 0, "QE.BudgetId");
			$iStationId = $objDatabase->Result($varResult, 0, "QE.StationId");
			$iDonorId = $objDatabase->Result($varResult, 0, "QE.DonorId");
			$iDonorProjectId = $objDatabase->Result($varResult, 0, "QE.DonorProjectId");
			$iDonorProjectActivityId = $objDatabase->Result($varResult, 0, "QE.DonorProjectActivityId");
			$objResponse->addScript('document.getElementById("selBudget").value = ' . $iBudgetId . ';');
			$objResponse->addScript('document.getElementById("selStation").value = ' . $iStationId . ';');
			$objResponse->addScript('document.getElementById("selDonor").value = ' . $iDonorId . ';');
			$objResponse->addScript('GetDonorProjects(' . $iDonorId . ', ' . $iDonorProjectId . ');');
			$objResponse->addScript('GetProjectActivities(' . $iDonorProjectId . ', ' . $iDonorProjectActivityId . ');');
			*/
    		$objResponse->addScript('document.getElementById("selEntryType").value = ' . $iEntryType . ';');			
			$objResponse->addScript('ChangeEntryType();');
			// Payment
			if ($iEntryType == 0)
			{
				$objResponse->addScript('document.getElementById("selPaymentType").value = ' . $iPaymentType . ';');
				$objResponse->addScript('ChangePaymentType();');

				if ($iPaymentType == 0)
				{
					$objResponse->addScript('document.getElementById("selBank").value = ' . $iBankId . ';');
					$objResponse->addScript('GetBankAccounts(' . $iBankId . ', ' . $iBankAccountId . ');');
				}

				$objResponse->addScript('GetBankPaymentTypes();');
				$objResponse->addScript('document.getElementById("selBankPaymentType").value = ' . $iBankPaymentType . ';');
				$objResponse->addScript('ChangeBankPaymentTypes(' . $iBankPaymentType . ');');
				$objResponse->addScript('GetCheckBooks(' . $iBankAccountId . ', ' . $iBankCheckBookId . ');');
			}
			else		// Receipt
			{
				$objResponse->addScript('document.getElementById("selReceiptType").value = ' . $iReceiptType . ';');
				$objResponse->addScript('ChangeReceiptType();');

				if ($iPaymentType == 0)
				{
					$objResponse->addScript('document.getElementById("selBank").value = ' . $iBankId . ';');
					$objResponse->addScript('GetBankAccounts(' . $iBankId . ', ' . $iBankAccountId . ');');
				}

				$objResponse->addScript('GetBankPaymentTypes();');
			}
			/*
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_quickentries_entries AS QEE 
			INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = QEE.ChartOfAccountsId
			LEFT  JOIN fms_vendors AS V ON V.VendorId = QEE.VendorId
			LEFT  JOIN fms_customers AS C ON C.CustomerId = QEE.CustomerId
			LEFT  JOIN organization_employees AS E ON E.EmployeeId = QEE.EmployeeId
			WHERE QEE.QuickEntryId='$iQuickEntryId'");
			
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iRecipientType = $objDatabase->Result($varResult, $i, "QEE.Recipient");
				$sDetails = $objDatabase->Result($varResult, $i, "QEE.Detail");
				
				$sDetails = str_replace('[PREVIOUS_MONTH]', date("F Y", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"))), $sDetails);

				$objResponse->addScript('SetVal("txtQuickEntryParticulars_Detail_' . ($i+1) . '", "' . $sDetails . '");');
				$objResponse->addScript('SetVal("txtQuickEntryParticulars_Debit_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "QEE.Debit") . '");');
				$objResponse->addScript('SetVal("txtQuickEntryParticulars_Credit_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "QEE.Credit") . '");');
				
				$objResponse->addScript('SetVal("txtChartOfAccount_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "CA.AccountTitle") . ' ' . $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode") . '");');
				$objResponse->addScript('SetVal("hdnChartOfAccountId_' . ($i+1) . '", "' . $objDatabase->Result($varResult, $i, "QEE.ChartOfAccountsId") . '");');
				
				if ($iRecipientType >= 0)
				{
					$objResponse->addScript('document.getElementById("selRecipient' . ($i+1) . '").value = ' . $iRecipientType . ';');
					$objResponse->addScript('ChangeRecipient('. $iRecipientType . ', ' . ($i+1) . ');');
					
					if ($iRecipientType == 0)	// Vendor
					{
						$iRecipientId = $objDatabase->Result($varResult, $i, "QEE.VendorId");
						$sRecipientName = $objDatabase->Result($varResult, $i, "V.VendorName");
					}
					else if ($iRecipientType == 1) // Customer
					{
						$iRecipientId = $objDatabase->Result($varResult, $i, "QEE.CustomerId");
						$sRecipientName = $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") . ' (' . $objDatabase->Result($varResult, $i, "C.CompanyName") . ')';
					}
					else if ($iRecipientType == 2) // Employee
					{
						$iRecipientId = $objDatabase->Result($varResult, $i, "QEE.EmployeeId");
						$sRecipientName = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
					}


					
					$objResponse->addScript('SetVal("txt' . $this->aRecipient[$iRecipientType] . 'Name' . ($i+1) . '", "' . $sRecipientName . '");');
					$objResponse->addScript('SetVal("hdn' . $this->aRecipient[$iRecipientType] . 'Id' . ($i+1) . '", "' . $iRecipientId . '");');
				}
				
				$objResponse->addScript('UpdateTotal();');
			}
			*/
		}

		return($objResponse->getXML());
	}
	
	function AJAX_GetDonorProjects($iDonorId, $sTargetSelectBox)
	{
	   	$objResponse = new xajaxResponse();		
	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
	   	$aData = $this->GetDonorProjects($iDonorId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		return($objResponse->getXML());
	}
	
	function GetDonorProjects($iDonorId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_donors_projects AS DP
		WHERE DP.DonorId='$iDonorId'");		
		$aResult[0][0] = 0;
		$aResult[0][1] = "Select Project";
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{						
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "DP.DonorProjectId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
		}
		return($aResult);
	}
	
	function AJAX_GetProjectActivities($iDonorProjectId, $sTargetSelectBox)
	{
	   	$objResponse = new xajaxResponse();

	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
	   	$aData = $this->GetProjectActivities($iDonorProjectId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		return($objResponse->getXML());
	}
	
	function GetProjectActivities($iDonorProjectId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_donors_projects_activities AS PA
		WHERE PA.DonorProjectId='$iDonorProjectId'");		
		$aResult[0][0] = 0;
		$aResult[0][1] = "Select Activity";
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{						
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "PA.ActivityId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "PA.ActivityCode");
		}
		return($aResult);
	}
	
	function AddNewQuickEntry($sTitle, $iEntryOrder, $iEntryType, $iPaymentType, $iReceiptType, $iBankPaymentType, $iBankId, $iBankCheckBookId, $iBankAccountId, $iStatus)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[1] == 0)
			return(1010);
			
		$varNow = $objGeneral->fnNow();
		$iAddedBy = $objEmployee->iEmployeeId;		
		
		if($dTotalDebit != $dTotalCredit) return(7226);
		
		$sTitle = mysql_escape_string($sTitle);
		if($iEntryOrder == "") $iEntryOrder = 0;
				
		if($iEntryType == 2) $iPaymentType = $iReceiptType = 0;
		if($iBudgetId == "") $iBudgetId = 0;
		if($iBankId == "") $iBankId = 0;
		if($iBankCheckBookId == "") $iBankCheckBookId = 0;
		if($iBankAccountId == "") $iBankAccountId = 0;
		if($iDonorId == "") $iDonorId = 0;
		if($iDonorProjectId == "") $iDonorProjectId = 0;
		if($iActivityId == "") $iActivityId = 0;
				
		
		if(($iEntryType == 0)) 	
		{
			$iReceiptType = 0;
			if($iPaymentType == 1) 
			{
				$iBankCheckBookId = 0;
				$sCheckNumber = 0;
				$iBankAccountId = 0;
			}			
		}
		else if (($iEntryType == 1)) 
		{			
			$iPaymentType = 0;
			$iBankCheckBookId = 0;
			$sCheckNumber = 0;
			if($iReceiptType == 1) 
				$iBankAccountId = 0;
		}
		else
		{	
			$iBankCheckBookId = 0;
			$sCheckNumber = 0;
			$iBankAccountId = 0;
		}
		
		$sBankAccountCondition = "";		
				
		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_quickentries
		(Title, EntryOrder, EntryType, PaymentType, ReceiptType, BankPaymentType, BankId, BankCheckBookId, BankAccountId, QuickEntryAddedOn, QuickEntryAddedBy)
		VALUES ('$sTitle', '$iEntryOrder', '$iEntryType', '$iPaymentType', '$iReceiptType', '$iBankPaymentType', '$iBankId', '$iBankCheckBookId', '$iBankAccountId', '$varNow', '$iAddedBy')");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_quickentries AS QE WHERE QE.QuickEntryAddedOn='$varNow'");

			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iQuickEntryId = $objDatabase->Result($varResult, 0, "QE.QuickEntryId");
				
				for ($k=0; $k < 10; $k++)
				{
					$iChartOfAccountsId = $objGeneral->fnGet("hdnChartOfAccountId_" . ($k +1));
					$iStationId = $objGeneral->fnGet("hdnStationId" . ($k +1));
					$iDepartmentId = $objGeneral->fnGet("hdnDepartmentId" . ($k +1));
					$iBudgetId = $objGeneral->fnGet("hdnBudgetId" . ($k +1));
					$iDonorId = $objGeneral->fnGet("selDonor" . ($k +1));
					$iDonorProjectId = $objGeneral->fnGet("selDonorProject" . ($k +1));
					$iDonorProjectActivityId = $objGeneral->fnGet("selProjectActivity" . ($k +1));
					
					$sDetail = $objGeneral->fnGet("txtQuickEntryParticulars_Detail_" . ($k +1));
					$dDebit = $objGeneral->fnGet("txtQuickEntryParticulars_Debit_" . ($k +1));
					$dCredit = $objGeneral->fnGet("txtQuickEntryParticulars_Credit_" . ($k +1));
					
					$iRecipient = $objGeneral->fnGet("selRecipient" . ($k +1));
					$iVendorId = $objGeneral->fnGet("hdnVendorId" . ($k +1));
					$iCustomerId = $objGeneral->fnGet("hdnCustomerId" . ($k +1));
					$iEmployeeId = $objGeneral->fnGet("hdnEmployeeId" . ($k +1));				
					
					if(($dDebit != "") || ($dDebit > 0) || ($dCredit !="") || ($dCredit > 0))
						$this->AddQuickEntryEntries($iQuickEntryId, $iChartOfAccountsId, $iStationId, $iDepartmentId, $iBudgetId, $iDonorId, $iDonorProjectId, $iDonorProjectActivityId, $iRecipient, $iVendorId, $iCustomerId, $iEmployeeId, $sDetail, $dDebit, $dCredit);
				}				
				
				$objSystemLog->AddLog("Add New Quick Entry - " . $sTitle);				
				
				$objGeneral->fnRedirect('?pagetype=details&error=7220&id=' . $iQuickEntryId);
			}
		}

		return(7221);
	}
	
	function UpdateQuickEntry($iQuickEntryId, $sTitle, $iEntryOrder, $iEntryType, $iPaymentType, $iReceiptType, $iBankPaymentType, $iBankId, $iBankCheckBookId, $iBankAccountId, $iStatus)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_QuickEntries[2] == 0)
			return(1010);
		
		$sTitle = mysql_escape_string($sTitle);
		if($iEntryOrder == "") $iEntryOrder = 0;
		
		if($iEntryType == 2) $iPaymentType = $iReceiptType = 0;
		if($iBankId == "") $iBankId = 0;
		if($iBankCheckBookId == "") $iBankCheckBookId = 0;
		if($iBankAccountId == "") $iBankAccountId = 0;
		
		if(($iEntryType == 0)) 	
		{
			$iReceiptType = 0;
			if($iPaymentType == 1) 
			{
				$iBankCheckBookId = 0;
				$sCheckNumber = 0;
				$iBankAccountId = 0;
			}			
		}
		else if (($iEntryType == 1)) 
		{			
			$iPaymentType = 0;
			$iBankCheckBookId = 0;
			$sCheckNumber = 0;
			if($iReceiptType == 1) 
				$iBankAccountId = 0;
		}
		else
		{	
			$iBankCheckBookId = 0;
			$sCheckNumber = 0;
			$iBankAccountId = 0;
		}
		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_generaljournal_quickentries 
		SET	
			Title='$sTitle',			
			EntryOrder='$iEntryOrder',
			EntryType='$iEntryType', 
			BudgetId='$iBudgetId',
			PaymentType='$iPaymentType',
			ReceiptType='$iReceiptType',
			BankPaymentType='$iBankPaymentType',
			BankId='$iBankId',
			BankCheckBookId='$iBankCheckBookId',
			BankAccountId='$iBankAccountId'						
		WHERE QuickEntryId='$iQuickEntryId'");
		
		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_generaljournal_quickentries_entries WHERE QuickEntryId='$iQuickEntryId'");
						
		for ($k=0; $k < 10; $k++)
		{
			$iChartOfAccountsId = $objGeneral->fnGet("hdnChartOfAccountId_" . ($k +1));
			$iStationId = $objGeneral->fnGet("hdnStationId" . ($k +1));
			$iDepartmentId = $objGeneral->fnGet("hdnDepartmentId" . ($k +1));
			$iBudgetId = $objGeneral->fnGet("hdnBudgetId" . ($k +1));
			$iDonorId = $objGeneral->fnGet("selDonor" . ($k +1));
			$iDonorProjectId = $objGeneral->fnGet("selDonorProject" . ($k +1));
			$iDonorProjectActivityId = $objGeneral->fnGet("selProjectActivity" . ($k +1));
			
			$sDetail = $objGeneral->fnGet("txtQuickEntryParticulars_Detail_" . ($k +1));
			$dDebit = $objGeneral->fnGet("txtQuickEntryParticulars_Debit_" . ($k +1));
			$dCredit = $objGeneral->fnGet("txtQuickEntryParticulars_Credit_" . ($k +1));
			
			$iRecipient = $objGeneral->fnGet("selRecipient" . ($k +1));
			$iVendorId = $objGeneral->fnGet("hdnVendorId" . ($k +1));
			$iCustomerId = $objGeneral->fnGet("hdnCustomerId" . ($k +1));
			$iEmployeeId = $objGeneral->fnGet("hdnEmployeeId" . ($k +1));				
			
			if(($dDebit != "") || ($dDebit > 0) || ($dCredit !="") || ($dCredit > 0))
				$this->AddQuickEntryEntries($iQuickEntryId, $iChartOfAccountsId, $iStationId, $iDepartmentId, $iBudgetId, $iDonorId, $iDonorProjectId, $iDonorProjectActivityId, $iRecipient, $iVendorId, $iCustomerId, $iEmployeeId, $sDetail, $dDebit, $dCredit);
		}
				
		$objSystemLog->AddLog("Update Quick Entry - " . $sTitle);
		$objGeneral->fnRedirect('?pagetype=details&error=7222&id=' . $iQuickEntryId);
			
	}
		
	/* Add Quick Entry Entries */
	function AddQuickEntryEntries($iQuickEntryId, $iChartOfAccountsId, $iStationId, $iDepartmentId, $iBudgetId, $iDonorId, $iDonorProjectId, $iDonorProjectActivityId, $iRecipient, $iVendorId, $iCustomerId, $iEmployeeId, $sDetail, $dDebit, $dCredit)
	{
		global $objDatabase;
		global $objGeneral;	
		
		$sDetail = mysql_escape_string($sDetail);		
		if($dDebit == "" ) $dDebit = 0;
		if($dCredit == "" ) $dCredit = 0;
		if($iRecipient == "" ) $iRecipient = 0;
		if($iVendorId == "" ) $iVendorId = 0;
		if($iCustomerId == "" ) $iCustomerId = 0;
		if($iEmployeeId == "" ) $iEmployeeId = 0;
		if($iStationId == "") $iStationId = 0;
		if($iDepartmentId == "") $iDepartmentId = 0;
		if($iBudgetId == "") $iBudgetId = 0;
		if($iDonorId == "") $iDonorId = 0;
		if($iDonorProjectId == "") $iDonorProjectId = 0;
		if($iDonorProjectActivityId == "") $iDonorProjectActivityId = 0;
		
		
		
		$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_quickentries_entries
		(QuickEntryId, ChartOfAccountsId, StationId, DepartmentId, BudgetId, DonorId, DonorProjectId, DonorProjectActivityId, Recipient, VendorId, CustomerId, EmployeeId, Detail, Debit, Credit)
		VALUES ('$iQuickEntryId', '$iChartOfAccountsId', '$iStationId', '$iDepartmentId', '$iBudgetId', '$iDonorId', '$iDonorProjectId', '$iDonorProjectActivityId', '$iRecipient', '$iVendorId', '$iCustomerId', '$iEmployeeId', '$sDetail', '$dDebit', '$dCredit')");
		
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
	}	
}

?>