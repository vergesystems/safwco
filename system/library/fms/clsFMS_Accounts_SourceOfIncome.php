<?php

/* SourceOfIncome Class */
class clsAccounts_SourceOfIncome extends clsAccounts
{
    // Constructor
    function __construct()
    {
    }
	
	function ShowAllSourceOfIncomes($sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
				
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');			
			
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

        $sTitle = "Source Of Income";
        if ($sSortBy == "") $sSortBy = "SOI.SourceOfIncomeId DESC";

        if ($sSearch != "")
        {
        	$sSearch = mysql_escape_string($sSearch);
            $sSearchCondition = " AND ((SOI.SourceOfIncomeId LIKE '%$sSearch%') OR (SOI.Title LIKE '%$sSearch%')OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }
				
		$iTotalRecords = $objDatabase->DBCount("fms_accounts_sourceofincome AS SOI INNER JOIN organization_employees AS E ON E.EmployeeId = SOI.SourceOfIncomeAddedBy", "1=1 $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_sourceofincome AS SOI		
		INNER JOIN organization_employees AS E ON E.EmployeeId = SOI.SourceOfIncomeAddedBy
        WHERE 1=1 $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td width="10%" align="left"><span class="WhiteHeading">Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=SOI.SourceOfIncomeId&sortorder="><img src="../images/sort_up.gif" alt="Sort by SourceOfIncome Id in Ascending Order" title="Sort by SourceOfIncome Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=SOI.SourceOfIncomeId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Source Of Income Id in Descending Order" title="Sort by Id in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=SOI.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Source Of Income Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=SOI.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by Title in Descending Order" border="0" /></a></span></td>
		 <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iSourceOfIncomeId = $objDatabase->Result($varResult, $i, "SOI.SourceOfIncomeId");
			$sTitle = $objDatabase->Result($varResult, $i, "SOI.Title");
			
			$sEditSourceOfIncome = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' .mysql_escape_string(str_replace('"', '', $sTitle)) . '\', \'../accounts/sourceofincome_details.php?action2=edit&id=' . $iSourceOfIncomeId. '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Source Of Income Details" title="Edit this Source Of Income Details"></a></td>';
            $sDeleteSourceOfIncome = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this SourceOfIncome?\')) {window.location = \'?action=DeleteSourceOfIncome&id=' . $iSourceOfIncomeId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Source Of Income" title="Delete this Source Of Income"></a></td>';
			
			
       		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[2] == 0)
       			$sEditSourceOfIncome = '';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[3] == 0)
				$sDeleteSourceOfIncome = '';			

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $iSourceOfIncomeId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' .mysql_escape_string(str_replace('"', '', $sTitle)) . '\',  \'../accounts/sourceofincome_details.php?id=' . $iSourceOfIncomeId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Source Of Income Details" title="View this Source Of Income Details"></a></td>
			 ' . $sEditSourceOfIncome . '
             ' . $sDeleteSourceOfIncome . '
            </tr>';
		}

		$sAddNewSourceOfIncome = '<td width="10%" align="left"><input onclick="window.top.CreateTab(\'tabContainer\', \'Add\', \'../accounts/sourceofincome_details.php?action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add"></td>';
		
        if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[1] == 0) // Add Disabled
			$sAddNewSourceOfIncome = '';
		

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  ' . $sAddNewSourceOfIncome . '
          <form method="GET" action=""><td align="left" colspan="2">Search for a Source Of Income:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Source Of Income" title="Search for Source Of Income" border="0"></td></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Source Of Income Details" alt="View this Source Of Income Details"></td><td>View this Source Of Income Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Source Of Income Details" alt="Edit this Source Of Income Details"></td><td>Edit this Source Of Income Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Source Of Income" alt="Delete this Source Of Income"></td><td>Delete this Source Of Income</td></tr>
        </table>';

		return($sReturn);
    }

    function SourceOfIncomeDetails($iSourceOfIncomeId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_accounts_sourceofincome AS SOI
		LEFT JOIN fms_accounts_sourceofincome AS SOI2 ON SOI2.SourceOfIncomeId = SOI.ParentSourceOfIncomeId
		INNER JOIN organization_employees AS E ON E.EmployeeId = SOI.SourceOfIncomeAddedBy
		WHERE SOI.SourceOfIncomeId = '$iSourceOfIncomeId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iSourceOfIncomeId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this SourceOfIncome" title="Edit this SourceOfIncome" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this SourceOfIncome?\')) {window.location = \'?action=DeleteSourceOfIncome&id=' . $iSourceOfIncomeId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this SourceOfIncome" title="Delete this SourceOfIncome" /></a>';
		
		
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[2] == 0)
   			$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[3] == 0)
			$sButtons_Delete = '';
		

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">SourceOfIncome Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iSourceOfIncomeId = $objDatabase->Result($varResult, 0, "SOI.SourceOfIncomeId");
		        $sTitle = $objDatabase->Result($varResult, 0, "SOI.Title");
				$iParentSourceOfIncomeId = $objDatabase->Result($varResult, 0, "SOI.ParentSourceOfIncomeId");
				$sParentSourceOfIncome = $objDatabase->Result($varResult, 0, "SOI2.Title");
				if ($sParentSourceOfIncome == '') $sParentSourceOfIncome = 'No Parent';
		        else $sParentSourceOfIncome = $sParentSourceOfIncome . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' . mysql_escape_string(str_replace('"', '', $sParentSourceOfIncome)) .'\' , \'../accounts/sourceofincome_details.php?id=' . $iParentSourceOfIncomeId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Source Of Income Parent Details" title="Source Of Income Parent Details" /></a>';
		        
				$iSourceOfIncomeAddedBy = $objDatabase->Result($varResult, 0, "SOI.SourceOfIncomeAddedBy");
				
				$sSourceOfIncomeAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sSourceOfIncomeAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \''. mysql_escape_string(str_replace('"', '', $sSourceOfIncomeAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iSourceOfIncomeAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
		        $sNotes = $objDatabase->Result($varResult, 0, "SOI.Notes");
		        $sDescription = $objDatabase->Result($varResult, 0, "SOI.Description");
				$dSourceOfIncomeAddedOn = $objDatabase->Result($varResult, 0, "SOI.SourceOfIncomeAddedOn");
				$sSourceOfIncomeAddedOn = date("F j, Y", strtotime($dSourceOfIncomeAddedOn)) . ' at ' . date("g:i a", strtotime($dSourceOfIncomeAddedOn));
		    }

			if ($sAction2 == "edit")
			{ 
		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				
				$sParentSourceOfIncome = '<select class="form1" name="selSourceOfIncomeParent" id="selSourceOfIncomeParent">
				<option value="0"> No Parent </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_sourceofincome AS SOI2 WHERE SOI2.SourceOfIncomeId <> '$iSourceOfIncomeId' ORDER BY SOI2.Title");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sParentSourceOfIncome .= '<option ' . (($iParentSourceOfIncomeId == $objDatabase->Result($varResult, $i, "SOI2.SourceOfIncomeId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "SOI2.SourceOfIncomeId") . '">' . $objDatabase->Result($varResult, $i, "SOI2.Title") . '</option>';
				}
				$sParentSourceOfIncome .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selSourceOfIncomeParent\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selSourceOfIncomeParent\'), \'../accounts/sourceofincome_details.php?id=\'+GetSelectedListBox(\'selSourceOfIncomeParent\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Source Of Income Parent Information" title="Source Of Income Parent Information" border="0" /></a>';
				
			  	$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$iSourceOfIncomeId = '';
				
				$sSourceOfIncomeAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sSourceOfIncomeAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \''. mysql_escape_string(str_replace('"', '', $sSourceOfIncomeAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
		        $sTitle = $objGeneral->fnGet("txtTitle");		        
		        $sDescription = $objGeneral->fnGet("txtDescription");
		        $sNotes = $objGeneral->fnGet("txtNotes");

		       $sReturn .= '<form method="post" action="" enctype="multipart/form-data" onsubmit="return ValidateForm();">';
				
				$sParentSourceOfIncome = '<select class="form1" name="selSourceOfIncomeParent" id="selSourceOfIncomeParent">
				<option value="0"> No Parent </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_sourceofincome AS SOI2 WHERE SOI2.SourceOfIncomeId <> '$iSourceOfIncomeId' ORDER BY SOI2.Title");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sParentSourceOfIncome .= '<option ' . (($iParentSourceOfIncomeId == $objDatabase->Result($varResult, $i, "SOI2.SourceOfIncomeId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "SOI2.SourceOfIncomeId") . '">' . $objDatabase->Result($varResult, $i, "SOI2.Title") . '</option>';
				}
				$sParentSourceOfIncome .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selSourceOfIncomeParent\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selSourceOfIncomeParent\'), \'../accounts/sourceofincome_details.php?id=\'+GetSelectedListBox(\'selSourceOfIncomeParent\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Source Of Income Parent Information" title="Source Of Income Parent Information" border="0" /></a>';
				
                $sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
			  	$sSourceOfIncomeAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			  	$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}

			$sReturn .= '
			 <script language="JavaScript" type="text/javascript">
	         function ValidateForm()
	         {
	         	if (GetVal(\'txtTitle\') == "") return(AlertFocus(\'Please enter a valid Source Of Income Title\', \'txtTitle\'));
	         	return true;
	         }
	        </script> 
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="GridTR"><td colspan="2"><span class="Details_Title">SourceOfIncome Information:</span></td></tr>
			 <tr><td width="20%">SourceOfIncome Id:</td><td><strong>' . $iSourceOfIncomeId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Title:</td><td><strong>' . $sTitle . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Parent Source Of Income:</td><td><strong>' . $sParentSourceOfIncome . '</strong></td></tr>
		     <tr bgcolor="#edeff1"><td valign="top" colspan="2">Description:</td></tr>
			 <tr><td valign="top" colspan="4"><strong>' . $sDescription . '</strong></td></tr>
		     <tr class="GridTR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
		     <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
		     <tr bgcolor="#edeff1"><td>Source Of Income Added On:</td><td><strong>' . $sSourceOfIncomeAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Source Of Income Added By:</td><td><strong>' . $sSourceOfIncomeAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iSourceOfIncomeId . '"><input type="hidden" name="action" id="action" value="UpdateSourceOfIncome"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
	  		else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewSourceOfIncome"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

	function AddNewSourceOfIncome($iParentSourceOfIncomeId, $sTitle, $sDescription, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
				
		// Employee Roles
   		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[1] == 0) // Add Disabled
			return(1010);		
		
        $varNow = $objGeneral->fnNow();
		$iSourceOfIncomeAddedBy = $objEmployee->iEmployeeId;
		
		$sTitle = mysql_escape_string($sTitle);
		
		if($objDatabase->DBCount("fms_accounts_sourceofincome AS SOI", "SOI.Title='$sTitle'") > 0)
			return(7300);
			        
        $sDescription = mysql_escape_string($sDescription);
        $sNotes = mysql_escape_string($sNotes);

        $varResult = $objDatabase->Query("INSERT INTO fms_accounts_sourceofincome
        (ParentSourceOfIncomeId, Title, Description, Notes, SourceOfIncomeAddedOn, SourceOfIncomeAddedBy)
        VALUES ('$iParentSourceOfIncomeId', '$sTitle', '$sDescription', '$sNotes', '$varNow', '$iSourceOfIncomeAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_accounts_sourceofincome AS SOI WHERE SOI.Title='$sTitle' AND SOI.SourceOfIncomeAddedOn='$varNow'");

        if ($objDatabase->RowsNumber($varResult) > 0)
        {
        	$iSourceOfIncomeId = $objDatabase->Result($varResult, $i, "SOI.SourceOfIncomeId");
        	
  			$objSystemLog->AddLog("Add New Source Of Income - " . $sTitle);

            $objGeneral->fnRedirect('?error=7301&id=' . $objDatabase->Result($varResult, 0, "SOI.SourceOfIncomeId"));
        }
       	else
       		return(7302);
    }
		
    function UpdateSourceOfIncome($iSourceOfIncomeId, $iParentSourceOfIncomeId, $sTitle, $sDescription, $sNotes)
    {
        global $objDatabase;
        global $objEmployee;
        global $objGeneral;
		global $objSystemLog;
		
        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[2] == 0) // Update Disabled
			return(1010);			
		
		$sTitle = mysql_escape_string($sTitle);		
        $sDescription = mysql_escape_string($sDescription);
        $sNotes = mysql_escape_string($sNotes);
		
		if($objDatabase->DBCount("fms_accounts_sourceofincome AS SOI", "1=1 AND SOI.SourceOfIncomeId <> '$iSourceOfIncomeId' AND SOI.Title='$sTitle'") > 0)
			return(7300);
			
        $varResult = $objDatabase->Query("
	    UPDATE fms_accounts_sourceofincome
	    SET
			ParentSourceOfIncomeId='$iParentSourceOfIncomeId',
        	Title='$sTitle',
            Description='$sDescription',
            Notes='$sNotes'            
    	WHERE SourceOfIncomeId='$iSourceOfIncomeId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Update Source Of Income - " . $sTitle);

            $objGeneral->fnRedirect('../accounts/sourceofincome_details.php?error=7303&id=' . $iSourceOfIncomeId);
        }
        else
            return(7304);
    }

    function DeleteSourceOfIncome($iSourceOfIncomeId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
		
        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_SourceOfIncome[3] == 0) // Delete Disabled
			return(1010);

		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_sourceofincome AS SOI WHERE SOI.SourceOfIncomeId='$iSourceOfIncomeId'");

       	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Source Of Income Id');
		$sTitle = $objDatabase->Result($varResult, 0, "SOI.Title");

        $varResult = $objDatabase->Query("DELETE FROM fms_accounts_sourceofincome WHERE SourceOfIncomeId='$iSourceOfIncomeId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Delete Source Of Income - " . $sTitle);

            return(7305);
        }
       else
           return(7306);
    }

    

    
}
?>