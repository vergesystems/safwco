<?php

class clsBanking
{
    // Constructor
    function __construct()
    {
    }
    
    function ShowBanksMenu($sPage)
    {
    	global $objEmployee;
    	
    	$sBanks = '<a ' . (($sPage == "Banks") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../banking/?page=Banks"><img src="../images/banking/iconBanks.gif" alt="Banks" title="Banks" border="0" /><br />Banks</a>';
    	$sBankAccounts = '<a ' . (($sPage == "BankAccounts") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../banking/?page=BankAccounts"><img src="../images/banking/iconBankAccounts.gif" alt="Bank Accounts" title="Bank Accounts" border="0" /><br />Bank Accounts</a>';
		$sBankReconciliation = '<a ' . (($sPage == "BankReconciliation") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../banking/?page=BankReconciliation"><img src="../images/banking/iconBankReconciliation.gif" alt="Bank Reconciliation" title="Bank Reconciliation" border="0" /><br />Bank Reconciliation</a>';
    	$sBankCheckBooks = '<a ' . (($sPage == "BankCheckBooks") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../banking/?page=BankCheckBooks"><img src="../images/banking/iconBankCheckbooks.gif" alt="Bank Check Books" title="Bank Check Books" border="0" /><br />Bank Check Books</a>';
		$sBankDeposits = '<a ' . (($sPage == "BankDeposits") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../banking/?page=BankDeposits"><img src="../images/banking/iconBankDeposits.gif" alt="Bank Deposits" title="Bank Deposits" border="0" /><br />Bank Deposits</a>';
		$sBankWithdrawals = '<a ' . (($sPage == "BankWithdrawals") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../banking/?page=BankWithdrawals"><img src="../images/banking/iconBankWithdrawals.gif" alt="Bank Withdrawals" title="Bank Withdrawals" border="0" /><br />Bank Withdrawals</a>';
    	
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[0] == 0)
    		$sBanks = '<img src="../images/banking/iconBanks_disabled.gif" alt="Banks" title="Banks" border="0" /><br />Banks';
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[0] == 0)
    		$sBankAccounts = '<img src="../images/banking/iconBankAccounts_disabled.gif" alt="Bank Accounts" title="Bank Accounts" border="0" /><br />Bank Accounts';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[0] == 0)
    		$sBankCheckBooks = '<img src="../images/banking/iconBankCheckbooks_disabled.gif" alt="Bank Check Books" title="Bank Check Books" border="0" /><br />Bank Check Books';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankDeposits[0] == 0)
    		$sBankDeposits = '<img src="../images/banking/iconBankDeposits_disabled.gif" alt="Bank Deposits" title="Bank Check Books" border="0" /><br />Bank Deposits';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankWithdrawals[0] == 0)
    		$sBankWithdrawals = '<img src="../images/banking/iconBankWithdrawals_disabled.gif" alt="Bank Withdrawals" title="Bank Withdrawals" border="0" /><br />Bank Withdrawals';
		if ($objEmployee->objEmployeeRoles->iEmployeeRole_FMS_Banking_BankReconciliation[0] == 0)
    		$sBankReconciliation = '<img src="../images/banking/iconBankReconciliation_disabled.gif" alt="Bank Reconciliation" title="Bank Reconciliation" border="0" /><br />Bank Reconciliation';
							
    	$sReturn .= '
    	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
    	 <tr>
    	  <td valign="bottom">

           <table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
	    	<tr>
	    	 <td align="center" width="10%" valign="top">' . $sBankDeposits . '</td>
	    	 <td align="center" width="10%" valign="top">' . $sBankWithdrawals . '</td>
			 <td align="center" width="10%" valign="top">' . $sBankReconciliation . '</td>
	    	 <td align="center" width="10%" valign="top">' . $sBankCheckBooks . '</td>
	    	 <td align="center" width="10%" valign="top">' . $sBankAccounts . '</td>
	    	 <td align="center" width="10%" valign="top">' . $sBanks . '</td>
	      	</tr>
	       </table>
    	  </td></tr></table>';
    	
    	return($sReturn);    	
    }
	
	function ShowBankingPages($sPage)
	{
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		if ($sPage == "") $sPage = "BankDeposits";
		
		switch($sPage)
		{
			case "BankDeposits": 
				$aTabs[0][0] = 'Bank Deposits';
				$aTabs[0][1] = '../banking/bankdeposits.php';
				break;				
			case "BankWithdrawals": 
				$aTabs[0][0] = 'Bank Withdrawals';
				$aTabs[0][1] = '../banking/bankwithdrawals.php';
				break;
			case "BankReconciliation": 
				$aTabs[0][0] = 'Bank Reconciliation';
				$aTabs[0][1] = '../banking/bankreconciliation.php';
				break;
			case "BankCheckBooks": 
				$aTabs[0][0] = 'Bank Check Books';
				$aTabs[0][1] = '../banking/bankcheckbooks.php';
				$aTabs[1][0] = 'Issued Checks';
				$aTabs[1][1] = '../banking/bankcheckbooks.php?pagetype=issuedchecks';
				break;
			case "BankAccounts":
				$aTabs[0][0] = 'Bank Accounts';
				$aTabs[0][1] = '../banking/bankaccounts.php';
				break;
			case "Banks":
				$aTabs[0][0] = 'Banks';
				$aTabs[0][1] = '../banking/banks.php';
				break;
		}
		
		$sReturn = $objDHTMLSuite->TabBar($aTabs, $this->ShowBanksMenu($sPage));
		return($sReturn);
	}
    
	
}

?>