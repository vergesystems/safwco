<?php

/* Bank Accounts */
class clsBanking_BankAccounts
{
	public $aAccountStatus;
	public $aBankAccountType;
	
    // Constructor
    function __construct()
    {
    	$this->aAccountStatus = array("Inactive", "Active");    	
		$this->aBankAccountType= array("Current Account", "Savings Account");
    }

	function ShowAllBankAccounts($iBankId, $iStation, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		if ($iBankId == '') $iBankId = 0;

		if ($iBankId > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B WHERE B.OrganizationId='" . cOrganizationId . "' AND B.BankId='$iBankId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Id');
				$sBankAbbreviation = $objDatabase->Result($varResult, 0, "B.BankAbbreviation");

			$sBankCondition = "AND B.BankId='$iBankId' ";
			$sBankCondition2 = "AND B.BankId='$iBankId' ";
		}
		else
		{
			$sBankName = "All Banks";
			$sBankCondition = $sBankCondition2 = "AND 1=1";
		}

        if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess == 0) // Access on employee branch only
			$sEmployeeRestriction = " AND BA.StationId='$objEmployee->iEmployeeStationId'";

		$sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        if ($sSortBy == "") $sSortBy = "BA.BankAccountId DESC";

		$iPagingLimit = cPagingLimit;

		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = 'Bank Accounts';
        if ($sSearch != "")
        {
            $sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((BA.BankAccountNumber LIKE '%$sSearch%') OR (BA.BankAccountId LIKE '%$sSearch%') OR (BA.BranchName LIKE '%$sSearch%') OR (BA.BranchCode LIKE '%$sSearch%') OR (S.StationName LIKE '%$sSearch%') OR (B.BankName LIKE '%$sSearch%') OR (B.BankAbbreviation LIKE '%$sSearch'))";
            $sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
        }

		$iTotalRecords = $objDatabase->DBCount("fms_banking_bankaccounts AS BA INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId INNER JOIN organization_stations AS S ON S.StationId = BA.StationId INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= BA.ChartOfAccountsId", "BA.OrganizationId='" . cOrganizationId . "' $sBankCondition $sSearchCondition $sEmployeeRestriction");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_banking_bankaccounts AS BA
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		INNER JOIN organization_stations  AS S ON S.StationId = BA.StationId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= BA.ChartOfAccountsId
        WHERE BA.OrganizationId='" . cOrganizationId . "' $sBankCondition $sSearchCondition $sEmployeeRestriction
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
          <td align="left"><span class="WhiteHeading">Bank&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BankName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank in Ascending Order" title="Sort by Bank in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BankName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank in Descending Order" title="Sort by Bank in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Branch&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.BranchName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Branch Name in Ascending Order" title="Sort by Branch Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.BranchName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Branch Name in Descending Order" title="Sort by Branch Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Account Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.AccountTitle&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Title in Ascending Order" title="Sort by Account Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.AccountTitle&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Title in Descending Order" title="Sort by Account Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Account Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.BankAccountNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Number in Ascending Order" title="Sort by Account Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.BAccountNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Number in Descending Order" title="Sort by Account Number in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Chart Of Account Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Code in Ascending Order" title="Sort by Account Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CA.ChartOfAccountsCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Code in Descending Order" title="Sort by Account Code in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iCurrentBankId = $objDatabase->Result($varResult, $i, "BA.BankId");
			$iCurrentStationId = $objDatabase->Result($varResult, $i, "BA.StationId");
			$iCurrentAccountId = $objDatabase->Result($varResult, $i, "BA.BankAccountId");
            $sBranchName = $objDatabase->Result($varResult, $i, "BA.BranchName");
			$sBankName = $objDatabase->Result($varResult, $i, "B.BankName");
			$sStation = $objDatabase->Result($varResult, $i, "S.StationName");
			$sBankAccountNumber = $objDatabase->Result($varResult, $i, "BA.BankAccountNumber");
			$sAccountTitle = $objDatabase->Result($varResult, $i, "BA.AccountTitle");
			$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode");

			$sEditBankAccount = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(str_replace('"', '', $sBankAccountNumber)) . '\', \'../banking/bankaccounts.php?pagetype=details&action2=edit&id=' . $iCurrentAccountId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Bank Account Details" title="Edit Bank Account Details"></a></td>';
			$sDeleteBankAccount = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Bank Account?\')) {window.location = \'?action=DeleteBankAccount&id=' . $iCurrentAccountId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Account" title="Delete this Account"></a></td>';

			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[2] == 0)	$sEditBankAccount = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[3] == 0) $sDeleteBankAccount = '<td class="GridTD">&nbsp;</td>';

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sBankName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sBranchName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sAccountTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td> 
			 <td class="GridTD" align="left" valign="top">' . $sBankAccountNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sChartOfAccountsCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sBankAccountNumber)) . '\', \'../banking/bankaccounts.php?pagetype=details&id=' . $iCurrentAccountId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Account Details" title="View this Account Details"></a></td>
			 ' . $sEditBankAccount . ' ' . $sDeleteBankAccount . '</tr>';
		}

		$sBankSelect = '<select class="form1" name="selBank" onchange="window.location=\'?bankid=\'+GetSelectedListBox(\'selBank\');" id="selBank">
		<option value="0">All Banks</option>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B WHERE B.OrganizationId='" . cOrganizationId . "' ORDER BY B.BankName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sBankSelect .= '<option ' . (($iBankId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . ' (' . $objDatabase->Result($varResult, $i, "B.BankAbbreviation") . ')</option>';
		$sBankSelect .= '</select>';

		$sAddNewAccount = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add New Bank Account\', \'../banking/bankaccounts.php?pagetype=details&action2=addnew \', \'520px\', true);" type="button" class="AdminFormButton1" value="Add New Account">';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[1] == 0)
			$sAddNewAccount = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td align="left">' . $sAddNewAccount . '&nbsp;
          <form method="GET" action="">Search for an Account:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for an Account" title="Search for an Account" border="0"></td><input type="hidden" name="id" id="id" value="' . $iDonorId . '" /><input type="hidden" name="projectid" id="projectid" value="' . $iDonorProjectId . '" /></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>
		   ' . $sBankSelect . '
		  </td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Account Details" alt="View this Account Details"></td><td>View this Account Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Account Details" alt="Edit this Account Details"></td><td>Edit this Account Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Account Details" alt="Delete this Account Details"></td><td>Delete this Account Details</td></tr>
        </table>';

		return($sReturn);
    }

    function BankAccountDetails($iBankAccountId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

    	include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_banking_bankaccounts AS BA
		INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BankAccountAddedBy
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = BA.ChartOfAccountsId
		INNER JOIN organization_stations AS S ON S.StationId = BA.StationId
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		WHERE BA.OrganizationId='" . cOrganizationId . "'  AND BA.BankAccountId = '$iBankAccountId'");
		
		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' .  $iBankAccountId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Account" title="Edit this Account" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Bank Account?\')) {window.location = \'?pagetype=details&action=DeleteBankAccount&id=' . $iBankAccountId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Account" title="Delete this Account" /></a>';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[3] == 0)	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Account Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iBankId = $objDatabase->Result($varResult, 0, "BA.BankId");

            	$iAccountStatus = $objDatabase->Result($varResult, 0, "BA.Status");
            	$sAccountStatus = $this->aAccountStatus[$iAccountStatus];

            	$sNotes = $objDatabase->Result($varResult, 0, "BA.Notes");

            	$iEmployeeId = $objDatabase->Result($varResult, 0, "E.EmployeeId");

				$sBankAccountAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sBankAccountAddedBy = $sBankAccountAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sBankAccountAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

            	$iBankAccountType = $objDatabase->Result($varResult, 0, "BA.BankAccountType");
            	$sBankAccountType = $this->aBankAccountType[$iBankAccountType];

            	$iBankId = $objDatabase->Result($varResult, 0, "B.BankId");
            	$sBankName = $objDatabase->Result($varResult, 0, "B.BankName");
            	$sBankName = $sBankName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sBankName)) . '\', \'../banking/banks_details.php?id=' . $iBankId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Bank Information" title="Bank Information" border="0" /></a><br />';

				$sBankAccountNumber = $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");
				$sAccountTitle = $objDatabase->Result($varResult, 0, "BA.AccountTitle");

				$iStationId = $objDatabase->Result($varResult, $i, "BA.StationId");
				if($iStationId > 0)
				{
					$sStationName = $objDatabase->Result($varResult, $i, "S.StationName");
					$sStationName = $sStationName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' .  $objDatabase->RealEscapeString(str_replace('"', '', $sStationName)) .'\' , \'../organization/stations_details.php?id=' . $iStationId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Station Details" title="Station Details" /></a>';
				}

				// Branch Information
				$sBranchName = $objDatabase->Result($varResult, 0, "BA.BranchName");
				$sBranchCode = $objDatabase->Result($varResult, 0, "BA.BranchCode");
				$sBranchContactPerson = $objDatabase->Result($varResult, 0, "BA.BranchContactPerson");
				$sBranchAddress = $objDatabase->Result($varResult, 0, "BA.BranchAddress");
				$sBranchCity = $objDatabase->Result($varResult, 0, "BA.BranchCity");
				$sBranchState = $objDatabase->Result($varResult, 0, "BA.BranchState");
				$sBranchZipCode = $objDatabase->Result($varResult, 0, "BA.BranchZipCode");
				$sBranchCountry = $objDatabase->Result($varResult, 0, "BA.BranchCountry");
				$sBranchPhoneNumber = $objDatabase->Result($varResult, 0, "BA.BranchPhoneNumber");
				$sBranchEmailAddress = $objDatabase->Result($varResult, 0, "BA.BranchEmailAddress");
				

				/*
				$iSourceOfIncomeId = $objDatabase->Result($varResult, $i, "CA.SourceOfIncomeId");
				$sSourceOfIncome = "";
				if($iSourceOfIncomeId > 0)
				{
					$sSourceOfIncome = $objDatabase->Result($varResult, $i, "SOI.Title");
					$sSourceOfIncome = $sSourceOfIncome . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' .  $objDatabase->RealEscapeString(str_replace('"', '', $sSourceOfIncome)) .'\' , \'../accounts/sourceofincome_details.php?id=' . $iSourceOfIncomeId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Source of Income Details" title="Source of Income Details" /></a>';
				}
				*/

				$iChartOfAccountsId = $objDatabase->Result($varResult, 0, "BA.ChartOfAccountsId");
				//$sChartofAccountType = $objDatabase->Result($varResult, 0, "CAT.Title");
				//$sChartofAccountType = $sChartofAccountType . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sChartofAccountType)) . '\', \'../accounts/chartofaccounttypes_details.php?id=' . $iChartOfAccountsId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Chart of Accounts type information" title="Chart of Accounts type Information" border="0" /></a><br />';
                /*
                $sCategory = $objDatabase->Result($varResult, 0, "CAC.CategoryCode") . ' - ' . $objDatabase->Result($varResult, 0, "CAC.CategoryName");
				$sControlCode = $objDatabase->Result($varResult, 0, "CACT.ControlCode") . ' - ' . $objDatabase->Result($varResult, 0, "CACT.ControlName");
				$iParentChartOfAccountsId = $objDatabase->Result($varResult, 0, "CA.ParentChartOfAccountsId");
				$sParentChartOfAccountTitle = $objDatabase->Result($varResult, 0, "CA2.AccountTitle");
				if ($sParentChartOfAccountTitle == '') $sParentChartOfAccountTitle = 'No Parent';
		        else $sParentChartOfAccountTitle = $sParentChartOfAccountTitle . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' . $sParentChartOfAccountTitle .'\' , \'../accounts/chartofaccounts_details.php?id=' . $iParentChartOfAccountsId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Chart of Account Parent Details" title="Chart of Account Parent Details" /></a>';
                */

				$sChartOfAccount = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsCode") . ' - '. $objDatabase->Result($varResult, 0, "CA.AccountTitle");
				$sChartOfAccountsCode = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsCode");
				/*
				$iDonorId = $objDatabase->Result($varResult, $i, "CA.DonorId");
				$iDonorProjectId = $objDatabase->Result($varResult, $i, "CA.DonorProjectId");
				$iActivityId = $objDatabase->Result($varResult, $i, "CA.DonorProjectActivityId");
				if($iDonorId > 0)
				{
					$sDonor = $objDatabase->Result($varResult, $i, "D.DonorName");
					if($iDonorProjectId > 0 ) $sDonorProject = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
					if($iActivityId > 0 ) $sProjectActivities = $objDatabase->Result($varResult, $i, "PA.ActivityTitle");
					$sDonorRow = '<tr id="trDonors" style="display:visible;" bgcolor="#ffffff"><td valign="top">Donor:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sDonor. '&nbsp;&raquo;&nbsp;</td><td>' . $sDonorProject . '&nbsp;&raquo;&nbsp;</td><td>' . $sProjectActivities . '</td></tr></table></td></tr>';
				}
				*/
            	//$dOpeningBalance = $objDatabase->Result($varResult, 0, "BA.OpeningBalance");
            	//$sOpeningBalance = number_format($dOpeningBalance,2);
            	//$dOpeningBalanceAsOf = $objDatabase->Result($varResult, 0, "BA.OpeningBalanceAsOf");
            	//$sOpeningBalanceAsOf = date("F j, Y", strtotime($dOpeningBalanceAsOf));

            	$dBankAccountAddedOn = $objDatabase->Result($varResult, 0, "BA.BankAccountAddedOn");
            	$sBankAccountAddedOn = date("F j, Y", strtotime($dBankAccountAddedOn)) . ' at ' . date("g:i a", strtotime($dBankAccountAddedOn));
 		    }

			if ($sAction2 == "edit")
			{
		        $sReturn .= '<form method="post" onsubmit="return ValidateForm();">';
				
        		$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
        		$sAccountStatus = '<select name="selAccountStatus" id="selAccountStatus" class="form1">';
        		for ($i=0; $i < count($this->aAccountStatus); $i++)
        			$sAccountStatus .= '<option ' . (($i == $iAccountStatus) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aAccountStatus[$i] . '</option>';
        		$sAccountStatus .= '</select>&nbsp;<span style="color:red;">*</span>';

        		$sBankAccountType = '<select name="selBankAccountType" id="selBankAccountType" class="form1">';
        		for ($i=0; $i < count($this->aBankAccountType); $i++)
        			$sBankAccountType .= '<option ' . (($i == $iAccountType) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aBankAccountType[$i] . '</option>';
        		$sBankAccountType .= '</select>&nbsp;<span style="color:red;">*</span>';

        		$sBankName = '<select class="form1" name="selBank" id="selBank">';
        		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B WHERE B.OrganizationId='" . cOrganizationId . "'  ORDER BY B.BankName");
        		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
        			//$sBankName .= '<option ' . (($iBankId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . ' - ' . $objDatabase->Result($varResult, $i, "B.BranchName") . '</option>';
					$sBankName .= '<option ' . (($iBankId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . '</option>';
        		$sBankName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selBank\'), \'../banking/banks_details.php?id=\'+GetSelectedListBox(\'selBank\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Bank Details" title="Bank Details" /></a>';

                $sChartOfAccount = '<select class="form1" name="selChartOfAccount" id="selChartOfAccount">';
				$varResult = $objDatabase->Query("
				SELECT * FROM fms_accounts_chartofaccounts AS CA2
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA2.ChartOfAccountsControlId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CAT ON CAT.ChartOfAccountsCategoryId = CA2.ChartOfAccountsCategoryId
				WHERE CA2.OrganizationId='" . cOrganizationId . "' 
				ORDER BY CAT.ChartOfAccountsCategoryId, CC.ChartOfAccountsControlId, CA2.ChartOfAccountsCode");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					{
						if (($i == 0) || ($iTempChartOfAccountsCategoryId != $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId")))
						{
							$iTempChartOfAccountsCategoryId = $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId");
							$sChartOfAccount .= '<option style="color:blue;" value="0">' . $objDatabase->Result($varResult, $i, "CAT.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAT.CategoryName") . '</option>';
						}

						if (($i == 0) || ($iTempChartOfAccountsControlId != $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId")))
						{
							$iTempChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId");
							$sChartOfAccount .= '<option style="color:green;" value="0">&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CC.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CC.ControlName") . '</option>';
						}

						$sChartOfAccount .= '<option ' . (($iChartOfAccountsId == $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult, $i, "CA2.AccountTitle") . '</option>';
					}
				}
				$sChartOfAccount .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selChartOfAccount\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccount\'), \'../accounts/chartofaccounts_details.php?id=\'+GetSelectedListBox(\'selChartOfAccount\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Parent Information" title="Chart of Account Parent Information" border="0" /></a>';

        		$sStationName = '<select class="form1" name="selStation" id="selStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations  AS S WHERE S.OrganizationId='" . cOrganizationId . "'  AND S.Status='1' ORDER BY S.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess == 1)
						$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
					else
					{
						if ($objEmployee->iEmployeeStationId == $objDatabase->Result($varResult2, $i, "S.StationId"))
							$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
					}
				}
				$sStationName .= '</select>&nbsp;<span style="color:red;">*</span>';

        		$sBankAccountNumber = '<input type="text" name="txtBankAccountNumber" id="txtBankAccountNumber" value="' . $sBankAccountNumber . '" class="form1" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAccountTitle = '<input type="text" name="txtAccountTitle" id="txtAccountTitle" value="' . $sAccountTitle . '" class="form1" size="30" />&nbsp;<span style="color:red;">*</span>';

				$sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';

				// Branch Information
				$sBranchName = '<input type="text" name="txtBranchName" id="txtBranchName" class="form1" value="' . $sBranchName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBranchCode = '<input type="text" name="txtBranchCode" id="txtBranchCode" class="form1" value="' . $sBranchCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBranchContactPerson = '<input type="text" name="txtBranchContactPerson" id="txtBranchContactPerson" class="form1" value="' . $sBranchContactPerson . '" size="30" />';
				
				$sBranchAddress = '<input type="text" name="txtBranchAddress" id="txtBranchAddress" class="form1" value="' . $sBranchAddress . '" size="30" />';
				$sBranchCity = '<input type="text" name="txtBranchCity" id="txtBranchCity" class="form1" value="' . $sBranchCity . '" size="30" />';
				$sBranchState = '<input type="text" name="txtBranchState" id="txtBranchState" class="form1" value="' . $sBranchState . '" size="30" />';
				$sBranchZipCode = '<input type="text" name="txtBranchZipCode" id="txtBranchZipCode" class="form1" value="' . $sBranchZipCode . '" size="30" />';
				$sBranchCountry = $sCountryString;
				$sBranchPhoneNumber = '<input type="text" name="txtBranchPhoneNumber" id="txtBranchPhoneNumber" class="form1" value="' . $sBranchPhoneNumber . '" size="30" />';
				$sBranchEmailAddress = '<input type="text" name="txtBranchEmailAddress" id="txtBranchEmailAddress" class="form1" value="' . $sBranchEmailAddress . '" size="30" />';
				
				/*
				$sChartofAccountType = '<select class="form1" name="selChartOfAccountType" id="selChartOfAccountType">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_types AS CAT WHERE CAT.Title='Bank Accounts' ORDER BY CAT.Title");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sChartofAccountType .= '<option ' . (($iChartOfAccountsTypeId == $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsTypeId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsTypeId") . '">' . $objDatabase->Result($varResult, $i, "CAT.Title") . '</option>';
				}
				$sChartofAccountType .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccountType\'), \'../accounts/chartofaccounttypes_details.php?id=\'+GetSelectedListBox(\'selChartOfAccountType\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Char of Account Type Information" title="Char of Account Type Information" border="0" /></a>';
				*/
				//$sChartOfAccountsCode = '<input type="text" name="txtChartOfAccountsCode" id="txtChartOfAccountsCode" class="form1" value="' . $sChartOfAccountsCode . '" size="20" />&nbsp;<span style="color:red;">*</span>';
				//$sChartOfAccountTitle = '<input type="text" name="txtChartOfAccountTitle" id="txtChartOfAccountTitle" class="form1" value="' . $sChartOfAccountTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';


        		//$sOpeningBalance = '<input type="text" name="txtOpeningBalance" id="txtOpeningBalance" value="' . $dOpeningBalance . '" class="form1" size="15" />&nbsp;<span style="color:red;">*</span>';
        		//$sOpeningBalanceAsOf = '<input type="text" size="10" id="txtOpeningBalanceAsOf" name="txtOpeningBalanceAsOf" class="form1" value="' . $dOpeningBalanceAsOf . '" />' . $objjQuery->Calendar('txtOpeningBalanceAsOf') . '&nbsp;<span style="color:red;">*</span>';

			}
			else if ($sAction2 == "addnew")
			{
				$iAccountStatus = 1;
				$iAccountType = 0;
				$dAsOf = date("Y-m-d");

		        $sReturn .= '<form method="post" onsubmit="return ValidateForm();">';

		        $sBankAccountAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sBankAccountAddedBy = $sBankAccountAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sAccountAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

				$sBankAccountNumber = $objGeneral->fnGet("txtBankAccountNumber");
				$sAccountTitle = $objGeneral->fnGet("txtAccountTitle");
				$sBranchName = $objGeneral->fnGet("txtBranchName");
				$sBranchCode = $objGeneral->fnGet("txtBranchCode");
				$sBranchContactPerson = $objGeneral->fnGet("txtBranchContactPerson");
				
				$sBranchAddress = $objGeneral->fnGet("txtBranchAddress");
		        $sBranchCity = $objGeneral->fnGet("txtBranchCity");
		        $sBranchZipCode = $objGeneral->fnGet("txtBranchZipCode");
		        $sBranchState = $objGeneral->fnGet("txtBranchState");
		        $sBranchCountry = $objGeneral->fnGet("selCountry");
		        $sBranchPhoneNumber = $objGeneral->fnGet("txtBranchPhoneNumber");		        
		        $sBranchEmailAddress = $objGeneral->fnGet("txtBranchEmailAddress");

				$iBankId = $objGeneral->fnGet("selBank");
				$iStationId = $objGeneral->fnGet("selStation");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$iChartOfAccountsCategoryId = $iCategoryId;

        		$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

        		$sAccountStatus = '<select name="selAccountStatus" id="selAccountStatus" class="form1">';
        		for ($i=0; $i < count($this->aAccountStatus); $i++)
        			$sAccountStatus .= '<option ' . (($i == $iAccountStatus) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aAccountStatus[$i] . '</option>';
        		$sAccountStatus .= '</select>&nbsp;<span style="color:red;">*</span>';

        		$sBankAccountType = '<select name="selBankAccountType" id="selBankAccountType" class="form1">';
        		for ($i=0; $i < count($this->aBankAccountType); $i++)
        			$sBankAccountType .= '<option ' . (($i == $iAccountType) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aBankAccountType[$i] . '</option>';
        		$sBankAccountType .= '</select>&nbsp;<span style="color:red;">*</span>';

        		$sBankName = '<select class="form1" name="selBank" id="selBank">';
        		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B WHERE B.OrganizationId='" . cOrganizationId . "' ORDER BY B.BankName");
        		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
        			//$sBankName .= '<option ' . (($iBankId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . ' - ' . $objDatabase->Result($varResult, $i, "B.BranchName") . '</option>';
					$sBankName .= '<option ' . (($iBankId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . '</option>';
        		$sBankName .= '</select>&nbsp;<span style="color:red;">*</span>';

        		$sStationName = '<select name="selStation" id="selStation" class="form1">
				<option value="0"> Select Station</option>';
				$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' AND S.STATUS='1' ORDER BY S.StationName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sStationName .= '<option value="' . $objDatabase->Result($varResult, $i, "S.StationId") . '">' . $objDatabase->Result($varResult, $i, "S.StationName") . '</option>';
				$sStationName .='</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selStation\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStation\'), \'../organization/stations_details.php?id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Station Details" title="Station Details" border="0" /></a>';
				/*
				$sSourceOfIncome = '<select name="selSourceOfIncome" id="selSourceOfIncome" class="form1">
				<option value="0">Select Source of Income </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_sourceofincome AS SOI ORDER BY SOI.Title");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sSourceOfIncome .= '<option value="' . $objDatabase->Result($varResult, $i, "SOI.SourceOfIncomeId") . '">' . $objDatabase->Result($varResult, $i, "SOI.Title") . '</option>';
				$sSourceOfIncome .='</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selSourceOfIncome\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selSourceOfIncome\'), \'../accounts/sourceofincome_details.php?id=\'+GetSelectedListBox(\'selSourceOfIncome\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Source Of Income Information" title="Source Of Income Information" border="0" /></a>';
        		*/

        		$sBankAccountNumber = '<input type="text" name="txtBankAccountNumber" id="txtBankAccountNumber" value="' . $sAccountNumber . '" class="form1" size="30" />&nbsp;<span style="color:red;">*</span>';
        		$sAccountTitle = '<input type="text" name="txtAccountTitle" id="txtAccountTitle" value="' . $sAccountTitle . '" class="form1" size="30" />&nbsp;<span style="color:red;">*</span>';
				/*
				$sChartofAccountType = '<select class="form1" name="selChartOfAccountType" id="selChartOfAccountType">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_types AS CAT WHERE CAT.Title='Bank Accounts' ORDER BY CAT.Title");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
						$sChartofAccountType .= '<option ' . (($iChartOfAccountsTypeId == $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsTypeId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsTypeId") . '">' . $objDatabase->Result($varResult, $i, "CAT.Title") . '</option>';
				}
				$sChartofAccountType .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccountType\'), \'../accounts/chartofaccounttypes_details.php?id=\'+GetSelectedListBox(\'selChartOfAccountType\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Char of Account Type Information" title="Char of Account Type Information" border="0" /></a>';
				*/

				// Branch Information
				$sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';

				$sBranchName = '<input type="text" name="txtBranchName" id="txtBranchName" class="form1" value="' . $sBranchName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBranchCode = '<input type="text" name="txtBranchCode" id="txtBranchCode" class="form1" value="' . $sBranchCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBranchContactPerson = '<input type="text" name="txtBranchContactPerson" id="txtBranchContactPerson" class="form1" value="' . $sBranchContactPerson . '" size="30" />';
				$sBranchAddress = '<input type="text" name="txtBranchAddress" id="txtBranchAddress" class="form1" value="' . $sBranchAddress . '" size="30" />';
				$sBranchCity = '<input type="text" name="txtBranchCity" id="txtBranchCity" class="form1" value="' . $sBranchCity . '" size="30" />';
				$sBranchState = '<input type="text" name="txtBranchState" id="txtBranchState" class="form1" value="' . $sBranchState . '" size="30" />';
				$sBranchZipCode = '<input type="text" name="txtBranchZipCode" id="txtBranchZipCode" class="form1" value="' . $sBranchZipCode . '" size="30" />';
				$sBranchCountry = $sCountryString;
				$sBranchPhoneNumber = '<input type="text" name="txtBranchPhoneNumber" id="txtBranchPhoneNumber" class="form1" value="' . $sBranchPhoneNumber . '" size="30" />';
				$sBranchEmailAddress = '<input type="text" name="txtBranchEmailAddress" id="txtBranchEmailAddress" class="form1" value="' . $sBranchEmailAddress . '" size="30" />';
				

				$sCategory = '<select onchange="GetControlCode();" class="form1" name="selCategory" id="selCategory">
				<option value="0">Select Category Code</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCategory .= '<option ' . (($iChartOfAccountsCategoryId == $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsCategoryId")) ? 'selected="true"' : '')  . ' value="' . $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsCategoryId") . '">' . $objDatabase->Result($varResult, $i, "CAC.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAC.CategoryName") . '</option>';
				$sCategory .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="jsOpenWindow(\'../accounts/chartofaccounts_categorycodes.php?action2=' . $sAction2 . '\', 500, 500);"><img src="../images/icons/plus.gif" border="0" alt="Add New Category Code" title="Add New Category Code" /></a>';

				if ($iChartOfAccountsCategoryId == '') $iChartOfAccountsCategoryId = 1;

				$sControlCode = '<select class="form1" name="selControl" id="selControl" onChange="GetChartOfAccountCode();">
				<option value="0">Select Control Code</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CACT WHERE CACT.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId'");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sControlCode .= '<option value="' . $objDatabase->Result($varResult, $i, "CACT.ChartOfAccountsControlId") . '">' . $objDatabase->Result($varResult, $i, "CACT.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CACT.ControlName") . '</option>';
				$sControlCode .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="jsOpenWindow(\'../accounts/chartofaccounts_controlcodes.php?action2=' . $sAction2 . '&id=' . $iChartOfAccountsId . '&catid=\'+GetSelectedListBox(\'selCategory\'), \'500\', \'500\');"><img src="../images/icons/plus.gif" border="0" alt="Add New Control Code" title="Add New Control Code" /></a>';

				$sChartOfAccount = '<select class="form1" name="selChartOfAccount" id="selChartOfAccount">';
				$varResult = $objDatabase->Query("
				SELECT * FROM fms_accounts_chartofaccounts AS CA2
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA2.ChartOfAccountsControlId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CAT ON CAT.ChartOfAccountsCategoryId = CA2.ChartOfAccountsCategoryId
				WHERE CA2.OrganizationId='" . cOrganizationId . "' AND CA2.ChartOfAccountsId <> '$iChartOfAccountsId'
				ORDER BY CAT.ChartOfAccountsCategoryId, CC.ChartOfAccountsControlId, CA2.ChartOfAccountsCode");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					{
						if (($i == 0) || ($iTempChartOfAccountsCategoryId != $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId")))
						{
							$iTempChartOfAccountsCategoryId = $objDatabase->Result($varResult, $i, "CAT.ChartOfAccountsCategoryId");
							$sChartOfAccount .= '<option style="color:blue;" value="0">' . $objDatabase->Result($varResult, $i, "CAT.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAT.CategoryName") . '</option>';
						}

						if (($i == 0) || ($iTempChartOfAccountsControlId != $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId")))
						{
							$iTempChartOfAccountsControlId = $objDatabase->Result($varResult, $i, "CC.ChartOfAccountsControlId");
							$sChartOfAccount .= '<option style="color:green;" value="0">&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CC.ControlCode") . ' - ' . $objDatabase->Result($varResult, $i, "CC.ControlName") . '</option>';
						}

						$sChartOfAccount .= '<option ' . (($iParentChartOfAccountsId == $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") ? 'selected="true"' : '')) . ' value="' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsId") . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $objDatabase->Result($varResult, $i, "CA2.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult, $i, "CA2.AccountTitle") . '</option>';
					}
				}
				$sChartOfAccount .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selChartOfAccount\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccount\'), \'../accounts/chartofaccounts_details.php?id=\'+GetSelectedListBox(\'selChartOfAccount\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Parent Information" title="Chart of Account Parent Information" border="0" /></a>';

				$sChartOfAccountsCode = '<input type="text" name="txtChartOfAccountsCode" id="txtChartOfAccountsCode" class="form1" value="' . $sChartOfAccountsCode . '" size="20" />&nbsp;<span style="color:red;">*</span>';
				$sChartOfAccountTitle = '<input type="text" name="txtChartOfAccountTitle" id="txtChartOfAccountTitle" class="form1" value="' . $sChartOfAccountTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';

				$sDonor = '<select name="selDonor" id="selDonor" onchange="GetDonorProjects(GetSelectedListBox(\'selDonor\'),0);" class="form1">
				<option value="0">Select Donor</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sDonor .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
				$sDonor .='</select>';

				$sDonorProject = '<div id="divDonorProject" style="display:none;"><select name="selDonorProject" id="selDonorProject" onchange="GetProjectActivities(GetSelectedListBox(\'selDonorProject\'),0);" class="form1"></select>&nbsp;&raquo;&nbsp;</div>';
				$sProjectActivity = '<div id="divProjectActivity" style="display:none;"><select name="selProjectActivity" id="selProjectActivity" class="form1"></select></div>';

        		//$sOpeningBalance = '<input type="text" name="txtOpeningBalance" id="txtOpeningBalance" value="' . $sOpeningBalance . '" class="form1" size="15" />&nbsp;<span style="color:red;">*</span>';
        		//$sOpeningBalanceAsOf = '<input type="text" size="10" id="txtOpeningBalanceAsOf" name="txtOpeningBalanceAsOf" class="form1" value="' . $dOpeningBalanceAsOf . '" />' . $objjQuery->Calendar('txtOpeningBalanceAsOf') . '&nbsp;<span style="color:red;">*</span>';

        		$sDonorRow = '<tr id="trDonors" style="display:visible;" bgcolor="#ffffff"><td valign="top">Donor:</td><td><table border="0" cellspacing="0" cellpadding="0" align="left"><tr><td>' . $sDonor  . '&nbsp;&raquo;&nbsp;</td><td>' . $sDonorProject . '</td><td>' . $sProjectActivity . '</td></tr></table></td></tr>';
				// Donor Projects
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.Status='1'");
				$iNoOfDonorProjects = $objDatabase->RowsNumber($varResult);
				$sDonorProjects = 'var aDonorProjects = MultiDimensionalArray(' . $iNoOfDonorProjects . ', 4);';
				for ($i=0; $i < $iNoOfDonorProjects; $i++)
				{
					$sDonorProjects .= 'aDonorProjects[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "DP.DonorId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '";';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '";';
				}

				// Project Activities
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.Status='1'");
				$iNoOfProjectActivities = $objDatabase->RowsNumber($varResult);
				$sProjectActivities = 'var aProjectActivities= MultiDimensionalArray(' . $iNoOfProjectActivities . ', 4);';
				for ($i=0; $i < $iNoOfProjectActivities; $i++)
				{
					$sProjectActivities .= 'aProjectActivities[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "PA.DonorProjectId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "PA.ActivityId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityCode") . '";';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityTitle") . '";';
				}

        		$sBankAccountAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));

			}

            $sReturn .= '
    		<script language="JavaScript" type="text/javascript">
    		' . $sDonorProjects . '
    		' . $sProjectActivities . '
    		function ValidateForm()
    		{
                if(GetSelectedListBox(\'selStation\') <= 0) return(AlertFocus(\'Please select Station\', \'selStation\'));
                if (GetVal(\'txtBankAccountNumber\') == "") return(AlertFocus(\'Please enter a valid Bank Account Number\', \'txtBankAccountNumber\'));
    			if (GetVal(\'txtAccountTitle\') == "") return(AlertFocus(\'Please enter a valid Bank Account Title\', \'txtAccountTitle\'));
         		if (GetVal(\'txtBranchName\') == "") return(AlertFocus(\'Please enter a valid Branch Name\', \'txtBranchName\'));
				if (GetVal(\'txtBranchCode\') == "") return(AlertFocus(\'Please enter a valid Branch Code\', \'txtBranchCode\'));
    			if(GetSelectedListBox(\'selChartOfAccount\') <= 0) return(AlertFocus(\'Please select Chart of Account\', \'selChartOfAccount\'));
    			
				//if(GetSelectedListBox(\'selControl\') == 0) return(AlertFocus(\'Please select Control Code\', \'selControl\'));
            	//if (GetVal(\'txtChartOfAccountsCode\') == "") return(AlertFocus(\'Please enter a valid Chart of Account Code\', \'txtChartOfAccountsCode\'));
    			//if (GetVal(\'txtChartOfAccountTitle\') == "") return(AlertFocus(\'Please enter a valid Chart of Account Title\', \'txtChartOfAccountTitle\'));
    			//if (!isNumeric(GetVal("txtOpeningBalance"))) return(AlertFocus(\'Please enter a valid Opening Balance\', \'txtOpeningBalance\'));
    			//if (!isDate(GetVal("txtOpeningBalanceAsOf"))) return(AlertFocus(\'Please enter a valid Opening Balance As of Date\', \'txtOpeningBalanceAsOf\'));

    			return true;
    		}
    		function GetChartOfAccountCode()
    		{
    			iControlId = GetSelectedListBox("selControl");
    			SetVal("txtChartOfAccountsCode", "");

    			if(iControlId > 0) xajax_AJAX_FMS_Banking_BankAccounts_GetChartOfAccountCode(GetSelectedListBox("selControl"));
    		}
    		function GetControlCode()
    		{
    			SetVal("txtChartOfAccountsCode", "");
    			xajax_AJAX_FMS_Banking_BankAccounts_GetControlCode(GetSelectedListBox("selCategory"), "selControl");
    		}
    		</script>';

			$sReturn .= '
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Account Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top" style="width:200px;">Station:</td><td><strong>' . $sStationName . '</tr></td></tr>
			 <tr bgcolor="#ffffff"><td>Bank Name:</td><td><strong>' . $sBankName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Bank Account Type:</td><td><strong>' . $sBankAccountType . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Bank Account Number:</td><td><strong>' . $sBankAccountNumber . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Account Title:</td><td><strong>' . $sAccountTitle . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Bank Account Status:</td><td><strong>' . $sAccountStatus . '</strong></td></tr> 
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Branch Information:</span></td></tr>
			 <tr><td>Branch Name:</td><td><strong>' . $sBranchName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Branch Code:</td><td><strong>' . $sBranchCode . '</strong></td></tr>
			 <tr><td>Branch Contact Person:</td><td><strong>' . $sBranchContactPerson . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Branch Address:</td><td><strong>' . $sBranchAddress . '</strong></td></tr>
			 <tr><td>Branch City:</td><td><strong>' . $sBranchCity . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Branch State:</td><td><strong>' . $sBranchState . '</strong></td></tr>
			 <tr><td>Branch Zip Code:</td><td><strong>' . $sBranchZipCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Branch Country:</td><td><strong>' . $sBranchCountry . '</strong></td></tr>
			 <tr><td>Branch Phone Number:</td><td><strong>' . $sBranchPhoneNumber . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Branch Email Address:</td><td><strong>' . $sBranchEmailAddress . '</strong></td></tr>
		 	 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Chart of Account Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top">Chart of Account:</td><td><strong>' . $sChartOfAccount . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Account Added On:</td><td><strong>' . $sBankAccountAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Account Added by:</td><td><strong>' . $sBankAccountAddedBy . '</strong></td></tr>
			</table>';
			
			// <tr bgcolor="#edeff1"><td valign="top">Opening Balance:</td><td><strong>' . $objEmployee->aSystemSettings["CurrencySign"] . '&nbsp;' . $sOpeningBalance . '</strong></td></tr>
			// <tr bgcolor="#ffffff"><td valign="top">As Of:</td><td><strong>' .  $sOpeningBalanceAsOf. '</strong></td></tr>			 

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Account" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBankAccountId . '"><input type="hidden" name="action" id="action" value="UpdateAccount"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Account" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewAccount"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}
		
		if ($sAction2 == "addnew")
		$sReturn .= '
		<script language="JavaScript" type="text/javascript">
			GetChartOfAccountCode();
		</script>';

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }
	
    function AddNewBankAccount($iBankId, $iBankAccountType, $sBankAccountNumber, $sBankAccountTitle, $sBranchName, $sBranchCode, $sBranchContactPerson, $sBranchAddress, $sBranchCity, $sBranchState, $sBranchZipCode, $sBranchCountry, $sBranchPhoneNumber, $sBranchEmailAddress, $iStationId, $iChartOfAccountsId, $sNotes, $iAccountStatus)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[1] == 0) // Add Disabled
			return(1010);

        $varNow = $objGeneral->fnNow();
		$iBankAccountAddedBy = $objEmployee->iEmployeeId;

        $sBankAccountNumber = $objDatabase->RealEscapeString($sBankAccountNumber);
		$sBankAccountTitle = $objDatabase->RealEscapeString($sBankAccountTitle);
		$sChartOfAccountsCode = $objDatabase->RealEscapeString($sChartOfAccountsCode);
		$sChartOfAccountTitle = $objDatabase->RealEscapeString($sChartOfAccountTitle);
		$sBranchName = $objDatabase->RealEscapeString($sBranchName);
		$sBranchCode = $objDatabase->RealEscapeString($sBranchCode);
		$sBranchContactPerson = $objDatabase->RealEscapeString($sBranchContactPerson);
		$sBranchAddress = $objDatabase->RealEscapeString($sBranchAddress);
		$sBranchCity = $objDatabase->RealEscapeString($sBranchCity);
		$sBranchState = $objDatabase->RealEscapeString($sBranchState);
		$sBranchZipCode = $objDatabase->RealEscapeString($sBranchZipCode);
		$sBranchCountry = $objDatabase->RealEscapeString($sBranchCountry);
		$sBranchPhoneNumber = $objDatabase->RealEscapeString($sBranchPhoneNumber);
		$sBranchEmailAddress = $objDatabase->RealEscapeString($sBranchEmailAddress);		
        $sNotes = $objDatabase->RealEscapeString($sNotes);
        if($iDonorId == "") $iDonorId = 0;
		if($iDonorProjectId == "") $iDonorProjectId = 0;
		if($iActivityId == "") $iActivityId = 0;
		if($iStationId == "") $iStationId = 0;
		if($iSourceOfIncomeId == "") $iSourceOfIncomeId = 0;

		//if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CA", "CA.ChartOfAccountsCode='$sChartOfAccountsCode'") > 0) return(7106);

		// Check Chart of Account Code must lie under Control Codes Range
        /*
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CAC WHERE CAC.ChartOfAccountsControlId = '$iChartOfAccountsControlId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$iStartRange = $objDatabase->Result($varResult, 0, "CAC.StartRange");
			$iEndRange = $objDatabase->Result($varResult, 0, "CAC.EndRange");

			if( ($iChartOfAccountsCode < $iStartRange ) || ($iChartOfAccountsCode > $iEndRange) )
			return(7009);
		}

		if ($iParentChartOfAccountsId > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.ChartOfAccountsId='$iParentChartOfAccountsId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Request...');

			if ($objDatabase->Result($varResult, 0, "CA.ParentChartOfAccountsId") > 0)
				return(7010);
		}
        */
		// Check if chart of account is not assinged to existing Bank Account
		if ($objDatabase->DBCount("fms_banking_bankaccounts AS BA", "BA.OrganizationId='" . cOrganizationId . "' AND BA.ChartOfAccountsId='$iChartOfAccountsId'") > 0) return(3238);

		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.OrganizationId='" . cOrganizationId . "' AND CA.ChartOfAccountsId='$iChartOfAccountsId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$iChartOfAccountsId = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsId");

			$varResult = $objDatabase->Query("INSERT INTO fms_banking_bankaccounts
			(OrganizationId, ChartOfAccountsId, StationId, BankId, BankAccountType, BankAccountNumber, AccountTitle, BranchName, BranchCode, BranchContactPerson, BranchAddress, BranchCity, BranchState, BranchZipCode, BranchCountry, BranchPhoneNumber, BranchEmailAddress, Notes, Status, BankAccountAddedOn, BankAccountAddedBy)
			VALUES ('" . cOrganizationId . "', '$iChartOfAccountsId', '$iStationId', '$iBankId', '$iBankAccountType', '$sBankAccountNumber', '$sBankAccountTitle', '$sBranchName', '$sBranchCode', '$sBranchContactPerson', '$sBranchAddress', '$sBranchCity', '$sBranchState', '$sBranchZipCode', '$sBranchCountry', '$sBranchPhoneNumber', '$sBranchEmailAddress', '$sNotes', '$iAccountStatus', '$varNow', '$iBankAccountAddedBy')");

		}
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.OrganizationId='" . cOrganizationId . "' AND BA.BankId='$iBankId' AND BA.StationId='$iStationId' AND BA.BankAccountAddedOn='$varNow'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{			
			$objSystemLog->AddLog("Add New Bank Account - " . $sBankAccountNumber);
			$objGeneral->fnRedirect('?pagetype=details&error=3230&id=' . $objDatabase->Result($varResult, 0, "BA.BankAccountId"));
		}
		else
			return(3231);
	}

    function UpdateBankAccount($iBankAccountId, $iBankId, $iBankAccountType, $sBankAccountNumber, $sBankAccountTitle, $sBranchName, $sBranchCode, $sBranchContactPerson, $sBranchAddress, $sBranchCity, $sBranchState, $sBranchZipCode, $sBranchCountry, $sBranchPhoneNumber, $sBranchEmailAddress, $iStationId, $iChartOfAccountsId, $sNotes, $iAccountStatus)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[2] == 0) // Update Disabled
			return(1010);

        $sBankAccountNumber = $objDatabase->RealEscapeString($sBankAccountNumber);
		$sBankAccountTitle = $objDatabase->RealEscapeString($sBankAccountTitle);
		$sBranchName = $objDatabase->RealEscapeString($sBranchName);
		$sBranchCode = $objDatabase->RealEscapeString($sBranchCode);
		$sBranchContactPerson = $objDatabase->RealEscapeString($sBranchContactPerson);
		$sBranchAddress = $objDatabase->RealEscapeString($sBranchAddress);
		$sBranchCity = $objDatabase->RealEscapeString($sBranchCity);
		$sBranchState = $objDatabase->RealEscapeString($sBranchState);
		$sBranchZipCode = $objDatabase->RealEscapeString($sBranchZipCode);
		$sBranchCountry = $objDatabase->RealEscapeString($sBranchCountry);
		$sBranchPhoneNumber = $objDatabase->RealEscapeString($sBranchPhoneNumber);
		$sBranchEmailAddress = $objDatabase->RealEscapeString($sBranchEmailAddress);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		
		// Check if chart of account is not assinged to existing Bank Account
		if ($objDatabase->DBCount("fms_banking_bankaccounts AS BA", "BA.OrganizationId='" . cOrganizationId . "' AND BA.ChartOfAccountsId='$iChartOfAccountsId' AND BA.BankAccountId <> '$iBankAccountId'") > 0) return(3239);
		
        $varResult = $objDatabase->Query("
        UPDATE fms_banking_bankaccounts
        SET
            BankId='$iBankId',
            BankAccountType='$iBankAccountType',
            StationId='$iStationId',
            ChartOfAccountsId='$iChartOfAccountsId',
            BankAccountNumber='$sBankAccountNumber',
			AccountTitle='$sBankAccountTitle',
			BranchName = '$sBranchName',
			BranchCode = '$sBranchCode',
			BranchContactPerson = '$sBranchContactPerson',
			BranchAddress = '$sBranchAddress',
			BranchCity = '$sBranchCity',
			BranchState = '$sBranchState',
			BranchZipCode = '$sBranchZipCode',
			BranchCountry = '$sBranchCountry',
			BranchPhoneNumber = '$sBranchPhoneNumber',
			BranchEmailAddress = '$sBranchEmailAddress',
            Notes='$sNotes',
            Status='$iAccountStatus'
		WHERE OrganizationId='" . cOrganizationId . "' AND BankAccountId='$iBankAccountId'");

        if ( $varResult )
        {        	
  			$objSystemLog->AddLog("Update Bank Account - " . $sBankAccountNumber);
			$objGeneral->fnRedirect('../banking/bankaccounts.php?pagetype=details&error=3232&id=' . $iBankAccountId);
            //return(3232);
        }
        else
            return(3233);
    }

    function DeleteBankAccount($iBankAccountId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankAccounts[3] == 0) // Delete Disabled
		{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iBankAccountId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}			

		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.OrganizationId='" . cOrganizationId . "' AND BA.BankAccountId='$iBankAccountId'");       	
       	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Account Id');
		$sBankAccountNumber = $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");
		$iChartOfAccountsId = $objDatabase->Result($varResult, 0, "BA.ChartOfAccountsId");
		
		// Check Chart of Account usage in Transactions & Parent
		if ($objDatabase->DBCount("fms_accounts_generaljournal_entries AS GJE", "GJE.ChartOfAccountsId= '$iChartOfAccountsId'") > 0) return(3236);
		if ($objDatabase->DBCount("fms_accounts_chartofaccounts AS CA", "CA.OrganizationId='" . cOrganizationId . "' AND CA.ParentChartOfAccountsId= '$iChartOfAccountsId'") > 0) return(3237);
				
        $varResult = $objDatabase->Query("DELETE FROM fms_banking_bankaccounts WHERE BankAccountId='$iBankAccountId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {
			$varResult = $objDatabase->Query("DELETE FROM fms_accounts_chartofaccounts WHERE ChartOfAccountsId='$iChartOfAccountsId'");
			
  			$objSystemLog->AddLog("Delete Bank Account - " . $sBankAccountNumber);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iBankAccountId . '&error=3234');
			else
				$objGeneral->fnRedirect('?id=0&error=3234');
        }
        else
            return(3235);
    }
    
    function GetBankAccounts($iStationId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_banking_bankaccounts AS BA
		INNER JOIN organization_stations AS S ON S.StationId= BA.StationId
		INNER JOIN fms_banking_banks AS B ON B.BankId= BA.BankId
		WHERE A.StationId='$iStationId'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$aResult[$i][0] = $objDatabase->Result($varResult, $i, "S.StationId");
			$aResult[$i][1] = $objDatabase->Result($varResult, $i, "B.BankName") . '-' .$objDatabase->Result($varResult, $i, "B.BranchName") . '-' . $objDatabase->Result($varResult, $i, "B.Location") . '-' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . ')';
		}

		return($aResult);
	}
	
	function AJAX_GetChartOfAccountsCode($iChartOfAccountsControlId)
	{
		global $objDatabase;
	   	$objResponse = new xajaxResponse();
		
		$varResult = $objDatabase->Query("SELECT MAX(ChartOfAccountsCode) AS 'LastCode' FROM fms_accounts_chartofaccounts AS CAC WHERE CAC.ChartOfAccountsControlId = '$iChartOfAccountsControlId'");
		if (($objDatabase->RowsNumber($varResult) > 0) && ($objDatabase->Result($varResult, 0, "LastCode") != ""))
		{
			$iLastCode = $objDatabase->Result($varResult, 0, "LastCode");
			$iNextCode = $iLastCode + 1;
															
    		$objResponse->addScript('SetVal("txtChartOfAccountsCode", "' . $iNextCode . '");');
		}
		else
		{
			$varResult = $objDatabase->Query("SELECT CAC.StartRange AS 'LastCode' FROM fms_accounts_chartofaccounts_controls AS CAC WHERE CAC.ChartOfAccountsControlId='$iChartOfAccountsControlId'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iLastCode = $objDatabase->Result($varResult, 0, "LastCode");
				$iNextCode = $iLastCode;
    			$objResponse->addScript('SetVal("txtChartOfAccountsCode", "' . $iNextCode . '");');
			}
		}

		return($objResponse->getXML());
	}
	
	function AJAX_GetControlCode($iChartOfAccountsCategoryId, $sTargetSelectBox)
	{
	   	$objResponse = new xajaxResponse();

	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
	   	$aData = $this->GetControls($iChartOfAccountsCategoryId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		return($objResponse->getXML());
	}
	
	function GetControls($iChartOfAccountsCategoryId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_chartofaccounts_controls AS CAC		
		WHERE CAC.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId'");
		$aResult[0][0] = 0;
		$aResult[0][1] = "Select Control Code";
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{	
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsControlId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "CAC.ControlCode") . '-' .$objDatabase->Result($varResult, $i, "CAC.ControlName");
		}
		return($aResult);
	}
}

?>