<?php

/* Bank Checks */
class clsBankCheckBooks
{
	public $aCheckBookStatus;
	
    // Constructor
    function __construct()
    {
    	$this->aCheckBookStatus = array("Functional", "Expired", "Lost");
    }
	
	function ShowAllBankCheckBooks($iBankAccountId=0, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
		
        if ($sSortBy == "") $sSortBy = "BCB.BankCheckBookId DESC";
        
		$iPagingLimit = cPagingLimit;
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;
		
        if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess == 0) // Access on employee branch only
        {
			$sEmployeeRestriction = " AND BA.StationId='$objEmployee->iEmployeeStationId'";
			$sEmployeeRestriction2 = " WHERE S.StationId='$objEmployee->iEmployeeStationId'";
        }
		
        $sTitle = 'Bank Check Books';
		if ($iBankAccountId == '') $iBankAccountId = 0;
		
		if ($iBankAccountId > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.OrganizationId='" . cOrganizationId . "' AND BA.BankAccountId='$iBankAccountId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Account Id');
				$sBankAccount = $objDatabase->Result($varResult, 0, "BA.AccountTitle") . ' ' . $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");
			
			$sBankAccountCondition = "AND BA.BankAccountId='$iBankAccountId'";
			$sTitle = "Bank Checkbooks of " . $sBankAccount;
		}
		else
		{ 
			$sBankAccount = "All Bank Accounts";
			$sBankAccountCondition  = "";
		}      
		
        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((BCB.CheckPrefix LIKE '%$sSearch%') OR (BCB.BankCheckBookId LIKE '%$sSearch%') OR (BCB.CheckNumberStart LIKE '%$sSearch%') OR (BCB.CheckNumberEnd LIKE '%$sSearch%') OR (B.BankName LIKE '%$sSearch%') OR (BA.BankAccountNumber LIKE '%$sSearch%') OR (BA.BranchName LIKE '%$sSearch%'))";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }
            
		$iTotalRecords = $objDatabase->DBCount("fms_banking_bankcheckbooks AS BCB INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = BCB.BankAccountId INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId INNER JOIN organization_stations AS S ON S.StationId = BA.StationId", "BCB.OrganizationId='" . cOrganizationId . "' $sBankAccountCondition $sSearchCondition $sEmployeeRestriction");
		//die("fms_banking_bankcheckbooks AS BC INNER JOIN fms_banking_bankaccounts AS A ON A.AccountId = BCB.AccountId INNER JOIN fms_banking_banks AS B ON B.BankId = A.BankId INNER JOIN organization_stations AS S ON S.StationId = A.StationId" . "$sBankAccountCondition $sSearchCondition $sEmployeeRestriction");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

        $varResult = $objDatabase->Query("
		SELECT * FROM fms_banking_bankcheckbooks AS BCB
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = BCB.BankAccountId
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
        INNER JOIN organization_stations AS S ON S.StationId = BA.StationId
        WHERE BCB.OrganizationId='" . cOrganizationId . "' $sBankAccountCondition $sStationCondition $sSearchCondition $sEmployeeRestriction
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");		
        
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		 
		  <td align="left"><span class="WhiteHeading">Branch Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.BranchName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Branch Name in Ascending Order" title="Sort by Bank Branch Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.BranchName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Branch Name in Descending Order" title="Sort by Bank Branch Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Account Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.BankAccountNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Account Number in Ascending Order" title="Sort by Bank Account Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.BankAccountNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Account Number in Descending Order" title="Sort by Bank Account Number in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Check Prefix&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BCB.CheckPrefix&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Prefix in Ascending Order" title="Sort by Check Prefix in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BCB.CheckPrefix&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Prefix in Descending Order" title="Sort by Check Prefix in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Start&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BCB.CheckNumberStart&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Number Start in Ascending Order" title="Sort by Check Number Start in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BCB.CheckNumberStart&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Number Start in Descending Order" title="Sort by Check Number Start in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">End&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BCB.CheckNumberEnd&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Number End in Ascending Order" title="Sort by Check Number End in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BCB.CheckNumberEnd&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Number Start in Descending Order" title="Sort by Check Number Start in Descending Order" border="0" /></a></span></td>
		  <td width="4%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iBankCheckBookId = $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId");
			$iBankId = $objDatabase->Result($varResult, $i, "B.BankId");
			$sBankName = $objDatabase->Result($varResult, $i, "B.BankName");
			
			$sBranchName = $objDatabase->Result($varResult, $i, "BA.BranchName");
			$sAccountNumber = $objDatabase->Result($varResult, $i, "BA.BankAccountNumber");
			$iCurrentBankAccountId = $objDatabase->Result($varResult, $i, "BCB.BankAccountId");
			$iCurrentStationId = $objDatabase->Result($varResult, $i, "S.StationId");
			$sCurrentStationName = $objDatabase->Result($varResult, $i, "S.StationName");
						
			$sCheckPrefix = $objDatabase->Result($varResult, $i, "BCB.CheckPrefix");
			$sCheckNumberStart = $objDatabase->Result($varResult, $i, "BCB.CheckNumberStart");
			$sCheckNumberEnd = $objDatabase->Result($varResult, $i, "BCB.CheckNumberEnd");

			$sEditBankCheckBook = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Check Book' . $objDatabase->RealEscapeString(str_replace('"', '', $sCheckPrefix)) . '\', \'../banking/bankcheckbooks.php?pagetype=details&action2=edit&id=' . $iBankCheckBookId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Bank Check Book Details" title="Edit this Bank Check Book Details"></a></td>';
			$sDeleteBankCheckBook = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Bank Check Book?\')) {window.location = \'?action=DeleteBankCheckBook&id=' . $iBankCheckBookId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank Check Book" title="Delete this Bank Check Book"></a></td>';
			$sCancelledChecks = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Cancelled Checks\', \'../banking/bankcheckbooks.php?pagetype=cancelledchecks&id=' . $iBankCheckBookId . '\', \'520px\', true);"><img src="../images/banking/iconCancelledChecks.gif" border="0" alt="Cancelled Checks" title="Cancelled Checks"></a></td>';

			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[2] == 0)	$sEditBankCheckBook = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[3] == 0)	$sDeleteBankCheckBook = '<td class="GridTD">&nbsp;</td>';
										
            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sBranchName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sAccountNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCheckPrefix . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCheckNumberStart . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCheckNumberEnd . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View Check Book' . $objDatabase->RealEscapeString(str_replace('"', '', $sCheckPrefix)) . '\', \'../banking/bankcheckbooks.php?pagetype=details&id=' . $iBankCheckBookId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Check Details" title="View this Bank Check Details"></a></td>
			 ' . $sEditBankCheckBook . ' ' . $sCancelledChecks . '	' . $sDeleteBankCheckBook . '</tr>';
		}
		
		/*
		$sStationSelect = '<select class="form1" name="selStation" onchange="window.location=\'?stationid=\'+GetSelectedListBox(\'selStation\');" id="selStation">
		<option value="0">Bank Checks from All Stations</option>';
		$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S $sEmployeeRestriction2 ORDER BY S.StationName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sStationSelect .= '<option ' . (($iStationId == $objDatabase->Result($varResult, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "S.StationId") . '">' . $objDatabase->Result($varResult, $i, "S.StationName") . '</option>';
		$sStationSelect .= '</select>';
		*/
		$sAddNewBankCheckBook = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add New Bank Check\', \'../banking/bankcheckbooks.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add Check Book">';		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[1] == 0) // View Disabled
			$sAddNewBankCheckBook = '';
		
		$sBankAccountSelect = '<select class="form1" name="selBankAccount" onchange="window.location=\'?bankaccountid=\'+GetSelectedListBox(\'selBankAccount\');" id="selBankAccount">
		<option value="0">All Bank Accounts</option>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.OrganizationId='" . cOrganizationId . "' ORDER BY BA.AccountTitle");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sBankAccountSelect .= '<option ' . (($iBankAccountId == $objDatabase->Result($varResult, $i, "BA.BankAccountId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . '">' . $objDatabase->Result($varResult, $i, "BA.AccountTitle") . ' (' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . ')</option>';
		$sBankAccountSelect .= '</select>';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td align="left">
		  ' . $sAddNewBankCheckBook . '&nbsp;&nbsp;
          <form method="GET" action="">Search for a Bank Check:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Bank Check Book" title="Search for Bank Check Book" border="0"></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>
		   ' . $sBankAccountSelect . '
		  </td>
		 </tr>		 
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Bank Check Details" alt="View this Bank Check Details"></td><td>View this Bank Check Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Bank Check Details" alt="Edit this Bank Check Details"></td><td>Edit this Bank Check Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/banking/iconCancelledChecks.gif" border="0" alt="Cancelled Checks" title="Cancelled Checks"></td><td>Cancelled Checks</td></tr>         
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Bank Check Details" alt="Delete this Bank Check Details"></td><td>Delete this Bank Check Details</td></tr>
        </table>';

		return($sReturn);
    }

    function BankCheckBookDetails($iBankCheckBookId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;    	   	 	
    	
		$varResult = $objDatabase->Query("
		SELECT *,BCB.Notes AS Notes
		FROM fms_banking_bankcheckbooks AS BCB
		INNER JOIN organization_employees AS E ON E.EmployeeId = BCB.BankCheckBookAddedBy
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = BCB.BankAccountId		
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		WHERE BCB.OrganizationId='" . cOrganizationId . "' AND BCB.BankCheckBookId='$iBankCheckBookId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iBankCheckBookId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Bank Check" title="Edit this Bank Check" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Bank Check?\')) {window.location = \'?pagetype=details&action=DeleteBankCheckBook&id=' . $iBankCheckBookId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank Check" title="Delete this Bank Check" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[3] == 0)	$sButtons_Delete = '';		
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Bank Check Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#838274" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';
		
		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iBankCheckBookId = $objDatabase->Result($varResult, 0, "BCB.BankCheckBookId");
				
				$iCheckBookStatus = $objDatabase->Result($varResult, 0, "BCB.Status");
            	$sCheckBookStatus = $this->aCheckBookStatus[$iCheckBookStatus];
				
		        $iBankId = $objDatabase->Result($varResult, 0, "B.BankId");
		    	$iBankAccountId = $objDatabase->Result($varResult, 0, "BA.BankAccountId");
				
		    	$iBankCheckBookAddedBy = $objDatabase->Result($varResult, 0, "E.EmployeeId");			
				$sBankCheckBookAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sBankCheckBookAddedBy = $sBankCheckBookAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sBankCheckBookAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iBankCheckBookAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sBankName = $objDatabase->Result($varResult, 0, "B.BankName");
            	
            	$sBankBranchName = $objDatabase->Result($varResult, 0, "BA.BranchName");
            	$sAccountNumber = $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");
            	
            	$sCheckPrefix = $objDatabase->Result($varResult, 0, "BCB.CheckPrefix");
            	$sCheckNumberStart = $objDatabase->Result($varResult, 0, "BCB.CheckNumberStart");
            	$sCheckNumberEnd = $objDatabase->Result($varResult, 0, "BCB.CheckNumberEnd");
            	
            	$sBankAccount = $objDatabase->Result($varResult, $i, "B.BankName") . ' - ' . $objDatabase->Result($varResult, $i, "BA.BranchName") . ' - ' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") ;
            	$sBankAccount = $sBankAccount . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sAccountNumber)) . '\', \'../banking/bankaccounts.php?pagetype=details&id=' . $iBankAccountId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Bank Account Information" title="Bank Account Information" border="0" /></a><br />';
            	            	
            	$sNotes = $objDatabase->Result($varResult, 0, "Notes");
            	
            	$dBankCheckBookAddedOn = $objDatabase->Result($varResult, $i, "BCB.BankCheckBookAddedOn");
            	$sBankCheckBookAddedOn = date("F j, Y", strtotime($dBankCheckBookAddedOn)) . ' at ' . date("g:i a", strtotime($dBankCheckBookAddedOn));
            	
 		    }
		
			if ($sAction2 == "edit")
			{
		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
		        
				$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S");
				
				$sBankAccount = '<select class="form1" name="selBankAccount" id="selBankAccount">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA INNER JOIN fms_banking_banks AS B ON B.BankId=BA.BankId WHERE B.OrganizationId='" . cOrganizationId . "' ORDER BY B.BankName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sBankAccount .= '<option' . (($iBankAccountId == $objDatabase->Result($varResult, $i, "BA.BankAccountId")) ? ' selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . ' - ' . $objDatabase->Result($varResult, $i, "BA.BranchName") . ' - ' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . '</option>';
				$sBankAccount .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selBankAccount\'), \'../banking/bankaccounts.php?pagetype=details&id=\'+GetSelectedListBox(\'selBankAccount\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Bank Account Details" title="Bank Account Details" /></a>';
								
				$sCheckBookStatus = '<select name="selCheckBookStatus" id="selCheckBookStatus" class="form1">';
        		for ($i=0; $i < count($this->aCheckBookStatus); $i++)
        			$sCheckBookStatus .= '<option ' . (($i == $iCheckBookStatus) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aCheckBookStatus[$i] . '</option>';
        		$sCheckBookStatus .= '</select>&nbsp;<span style="color:red;">*</span>';
				
				$sCheckPrefix = '<input type="text" name="txtCheckPrefix" id="txtCheckPrefix" class="form1" value="' . $sCheckPrefix . '" size="20" />';
				$sCheckNumberStart = '<input type="text" name="txtCheckNumberStart" id="txtCheckNumberStart" class="form1" value="' . $sCheckNumberStart . '" size="20" />';
				$sCheckNumberEnd = '<input type="text" name="txtCheckNumberEnd" id="txtCheckNumberEnd" class="form1" value="' . $sCheckNumberEnd . '" size="20" />';

				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
 			}
			else if ($sAction2 == "addnew")
			{
		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sBankCheckBookAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sBankCheckBookAddedBy = $sBankCheckBookAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sBankCheckBookAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';			
				
				$sCheckPrefix = $objGeneral->fnGet("txtCheckPrefix");
				$sCheckNumberStart = $objGeneral->fnGet("txtCheckNumberStart");
				$sCheckNumberEnd = $objGeneral->fnGet("txtCheckNumberEnd");
				
				$sBankAccount = '<select class="form1" name="selBankAccount" id="selBankAccount">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA INNER JOIN fms_banking_banks AS B ON B.BankId=BA.BankId WHERE BA.OrganizationId='" . cOrganizationId . "' ORDER BY B.BankName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sBankAccount .= '<option' . (($iBankAccountId == $objDatabase->Result($varResult, $i, "BA.BankAccountId")) ? ' selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . ' - ' . $objDatabase->Result($varResult, $i, "BA.BranchName") . ' - ' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . '</option>';
				$sBankAccount .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selBankAccount\'), \'../banking/bankaccounts.php?pagetype=details&id=\'+GetSelectedListBox(\'selBankAccount\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Bank Account Details" title="Bank Account Details" /></a>';
				
				$sCheckBookStatus = '<select name="selCheckBookStatus" id="selCheckBookStatus" class="form1">';
        		for ($i=0; $i < count($this->aCheckBookStatus); $i++)
        			$sCheckBookStatus .= '<option ' . (($i == $iCheckBookStatus) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aCheckBookStatus[$i] . '</option>';
        		$sCheckBookStatus .= '</select>&nbsp;<span style="color:red;">*</span>';
				
				$sCheckPrefix = '<input type="text" name="txtCheckPrefix" id="txtCheckPrefix" class="form1" value="' . $sCheckPrefix . '" size="20" />';
				$sCheckNumberStart = '<input type="text" name="txtCheckNumberStart" id="txtCheckNumberStart" class="form1" value="' . $sCheckNumberStart . '" size="20" />';
				$sCheckNumberEnd = '<input type="text" name="txtCheckNumberEnd" id="txtCheckNumberEnd" class="form1" value="' . $sCheckNumberEnd . '" size="20" />';

				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
	
        		$sBankCheckBookAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}
		
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
	        function ValidateForm()
	        {
	        	if (GetVal(\'txtCheckPrefix\') == "") return(AlertFocus(\'Please enter a valid Check Prefix\', \'txtCheckPrefix\'));	         
	         	if (!isNumeric(GetVal(\'txtCheckNumberStart\'))) return(AlertFocus(\'Please enter a valid Check Start Number\', \'txtCheckNumberStart\'));	         	
	         	if (!isNumeric(GetVal(\'txtCheckNumberEnd\'))) return(AlertFocus(\'Please enter a valid Check End Number\', \'txtCheckNumberEnd\'));         		
         		return true;
	         }		         
		        
			function ChangeBankAccounts()
			{
				xajax_AJAX_FMS_Banking_BankAccounts_FillBankAccounts(GetSelectedListBox("selStation"), "selBankAccount");
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Bank Check Information:</span></td></tr>			 
			 <!--<tr bgcolor="#ffffff"><td>Station:</td><td><strong>' . $sStationName . '</strong></td></tr>-->
			 <tr bgcolor="#edeff1"><td valign="top" style="width:200px;">Bank Name / Branch / Account:</td><td><strong>' . $sBankAccount . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Check Book Prefix:</td><td><strong>' . $sCheckPrefix . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Check Numbers Start From:</td><td><strong>' . $sCheckNumberStart . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Check Numbers Start End:</td><td><strong>' . $sCheckNumberEnd . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Status:</td><td><strong>' . $sCheckBookStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr><td>Bank Check Book Added On:</td><td><strong>' . $sBankCheckBookAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Bank Check Book Added By:</td><td><strong>' . $sBankCheckBookAddedBy. '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="bankcheckid" id="bankcheckid" value="' . $iBankCheckBookId . '"><input type="hidden" name="action" id="action" value="UpdateBankCheckBook"></form></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Bank Check" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iStationId . '"><input type="hidden" name="action" id="action" value="AddNewBankCheckBook"></form></div>';
		}
		
		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }
    	
	function AddNewBankCheckBook($iBankAccountId, $sCheckPrefix, $sCheckNumberStart, $sCheckNumberEnd, $iStatus, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[1] == 0) // Add Disabled
			return(1010);
			
        $varNow = $objGeneral->fnNow();
        $iBankCheckBookAddedBy = $objEmployee->iEmployeeId;     
		
        //die($iBankAccountId);
        #if ($objDatabase->DBCount("organization_stations AS S", "S.StationId='$iStationId'") <= 0) die('Sorry, Invalid Station Id');
        if ($objDatabase->DBCount("fms_banking_bankaccounts AS BA", "BA.OrganizationId='" . cOrganizationId . "' AND BA.BankAccountId='$iBankAccountId'") <= 0) die('Sorry, Invalid Bank Account Id');
        
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		$sCheckPrefix = $objDatabase->RealEscapeString($sCheckPrefix);
		$sCheckNumberStart = $objDatabase->RealEscapeString($sCheckNumberStart);
		$sCheckNumberEnd = $objDatabase->RealEscapeString($sCheckNumberEnd);
		
        $varResult = $objDatabase->Query("INSERT INTO fms_banking_bankcheckbooks
        (OrganizationId, BankAccountId, CheckPrefix, CheckNumberStart, CheckNumberEnd, Status, Notes, BankCheckBookAddedOn, BankCheckBookAddedBy) 
        VALUES 
        ('" . cOrganizationId . "', '$iBankAccountId', '$sCheckPrefix', '$sCheckNumberStart', '$sCheckNumberEnd', '$iStatus', '$sNotes', '$varNow', '$iBankCheckBookAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BCB WHERE BCB.OrganizationId='" . cOrganizationId . "' AND BCB.BankAccountId='$iBankAccountId' AND BCB.BankCheckBookAddedOn='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
		{			
			$objSystemLog->AddLog("Add New Bank Check Book - " . $sCheckPrefix);
			
			$objGeneral->fnRedirect('?pagetype=details&error=5500&id=' . $objDatabase->Result($varResult, 0, "BCB.BankCheckBookId"));
		}
        else
            return(5501);
    }	
	
    function UpdateBankCheckBook($iBankCheckBookId, $iBankAccountId, $sCheckPrefix, $sCheckNumberStart, $sCheckNumberEnd, $iStatus, $sNotes)
    {
        global $objDatabase;
		global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[2] == 0) // Update Disabled
			return(1010);
		
        //if ($objDatabase->DBCount("organization_stations AS S", "S.StationId='$iStationId'") <= 0) die('Sorry, Invalid Station Id');
        if ($objDatabase->DBCount("fms_banking_bankaccounts AS BA", "BA.OrganizationId='" . cOrganizationId . "' AND BA.BankAccountId='$iBankAccountId'") <= 0) die('Sorry, Invalid Bank Account Id');
		
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		$sCheckPrefix = $objDatabase->RealEscapeString($sCheckPrefix);
		$sCheckNumberStart = $objDatabase->RealEscapeString($sCheckNumberStart);
		$sCheckNumberEnd = $objDatabase->RealEscapeString($sCheckNumberEnd);
        
        $varResult = $objDatabase->Query("
        UPDATE fms_banking_bankcheckbooks 
        SET
        	BankAccountId='$iBankAccountId',
            CheckPrefix='$sCheckPrefix',
            CheckNumberStart='$sCheckNumberStart',
            CheckNumberEnd='$sCheckNumberEnd',
			Status='$iStatus',
            Notes='$sNotes'
        WHERE OrganizationId='" . cOrganizationId . "' AND BankCheckBookId='$iBankCheckBookId'");

        if ($varResult)
		{			
			$objSystemLog->AddLog("Update Bank Check Book - " . $sCheckPrefix);
						
			$objGeneral->fnRedirect('../banking/bankcheckbooks.php?pagetype=details&error=5502&id=' . $iBankCheckBookId);
		}
            //return(5502);
        else
            return(5503);
    }

    function DeleteBankCheckBook($iBankCheckBookId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[3] == 0) // Delete Disabled
		{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iBankCheckBookId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}
		
		if ($objDatabase->DBCount("fms_accounts_generaljournal AS GJ", "GJ.OrganizationId='" . cOrganizationId . "' AND GJ.BankCheckBookId= '$iBankCheckBookId'") > 0) return(5507);
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BCB WHERE BCB.OrganizationId='" . cOrganizationId . "' AND BCB.BankCheckBookId='$iBankCheckBookId'");       	
       	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Book Id');
		$sCheckPrefix = $objDatabase->Result($varResult, 0, "BCB.CheckPrefix");
			
        $varResult = $objDatabase->Query("DELETE FROM fms_banking_bankcheckbooks WHERE BankCheckBookId='$iBankCheckBookId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Delete Bank Check Book - " . $sCheckPrefix);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iBankCheckBookId . '&error=5504');
			else
				$objGeneral->fnRedirect('?id=0&error=5504');			
		}
        else
            return(5505);
    }

	// To get Next Available Check
	function FindNextAvailableCheck($iBankCheckBookId)
	{
		global $objDatabase;
		$iNextCheckNumber = 0;
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BC WHERE BC.OrganizationId='" . cOrganizationId . "' AND BC.BankCheckBookId='$iBankCheckBookId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Book...');
		
		$iCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
		$iCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");
		
		$iNextCheckNumber = $iCheckNumberStart;
		
		$varResult = $objDatabase->Query("SELECT MAX(GJ.CheckNumber) AS 'BankCheckNumber' FROM fms_accounts_generaljournal AS GJ WHERE GJ.OrganizationId='" . cOrganizationId . "' AND GJ.BankCheckBookId='$iBankCheckBookId'");
		if ($objDatabase->RowsNumber($varResult) > 0) 
			$iLastCheckNumberUsed = $objDatabase->Result($varResult, 0, "BankCheckNumber");
		
		if (($iLastCheckNumberUsed >= $iCheckNumberStart) && ($iLastCheckNumberUsed <= $iCheckNumberEnd))
			$iNextCheckNumber = $iLastCheckNumberUsed+1;
		
		$varResult = $objDatabase->Query("SELECT MAX(BCC.CancelledCheckNumber) AS 'BankCheckNumber' FROM fms_banking_bankcheckbooks_cancelledchecks AS BCC WHERE BCC.OrganizationId='" . cOrganizationId . "' AND BCC.BankCheckBookId='$iBankCheckBookId'");
		if ($objDatabase->RowsNumber($varResult) > 0) 
			$iLastCheckNumberCancelled = $objDatabase->Result($varResult, 0, "BankCheckNumber");
			
		if ($iLastCheckNumberCancelled > $iLastCheckNumberUsed)
			$iNextCheckNumber = $iLastCheckNumberCancelled+1;
		
		return($iNextCheckNumber);
	}
	
    function AJAX_FillBankAccounts($iOfficeId, $sTargetSelectBox)
	{
	   	$objResponse = new xajaxResponse();

	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
	   	$aData = $this->GetBankAccounts($iOfficeId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		return($objResponse->getXML());
	}
	
	function _ShowAllIssuedChecks($iBankCheckBookId=0, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;		
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');		
		
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
		
        if ($sSortBy == "") $sSortBy = "BCB.BankCheckBookId DESC";
        
		$iPagingLimit = cPagingLimit;
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;
		
		$sTitle = 'Issued Checks';
		if ($iBankCheckBookId == '') $iBankCheckBookId = 0;
		
		if ($iBankCheckBookId > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BCB WHERE BCB.OrganizationId='" . cOrganizationId . "' AND BCB.BankCheckBookId='$iBankCheckBookId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Book Id');
				$sCheckPrefix = $objDatabase->Result($varResult, 0, "BCB.CheckPrefix");
			
			$sCheckBookCondition = "AND BCB.BankCheckBookId='$iBankCheckBookId'";
			$sTitle = "Issued Checks of " . $sCheckPrefix;
		}		
        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((BCB.CheckPrefix LIKE '%$sSearch%') OR (BCB.BankCheckBookId LIKE '%$sSearch%') OR (BCB.CheckNumberStart LIKE '%$sSearch%') OR (BCB.CheckNumberEnd LIKE '%$sSearch%') OR (B.BankName LIKE '%$sSearch%') OR (BA.BankAccountNumber LIKE '%$sSearch%') OR (BA.BranchName LIKE '%$sSearch%'))";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }
            
		$iTotalRecords = $objDatabase->DBCount("fms_accounts_generaljournal AS GJ INNER JOIN fms_banking_bankcheckbooks AS BCB ON BCB.BankCheckBookId = GJ.BankCheckBookId INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = BCB.BankAccountId INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId", " BCB.OrganizationId='" . cOrganizationId . "'  AND GJ.IsDeleted ='0' $sCheckBookCondition $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

        $varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankcheckbooks AS BCB ON BCB.BankCheckBookId = GJ.BankCheckBookId
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = BCB.BankAccountId
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId        
        WHERE BCB.OrganizationId='" . cOrganizationId . "'  AND GJ.IsDeleted ='0' $sCheckBookCondition $sSearchCondition 
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");		
        
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		 
		  <td align="left"><span class="WhiteHeading">Reference&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.Reference&sortorder="><img src="../images/sort_up.gif" alt="Sort by Reference in Ascending Order" title="Sort by Reference in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.Reference&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Reference in Descending Order" title="Sort by Reference in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Check Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.CheckNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Number in Ascending Order" title="Sort by Check Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.CheckNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Number in Descending Order" title="Sort by Check Number in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Check Prefix&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BCB.CheckPrefix&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Prefix in Ascending Order" title="Sort by Check Prefix in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BCB.CheckPrefix&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Prefix in Descending Order" title="Sort by Check Prefix in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Transaction Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Transaction Date in Ascending Order" title="Sort by Transaction Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Transaction Date in Descending Order" title="Sort by Transaction Date in Descending Order" border="0" /></a></span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			$iId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
			$sReference = $objDatabase->Result($varResult, $i, "GJ.Reference");
			$sCheckNumber = $objDatabase->Result($varResult, $i, "GJ.CheckNumber");
			$dTransactionDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
			$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
			$iCurrentCheckBookId = $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId");
			$sCheckPrefix = $objDatabase->Result($varResult, $i, "BCB.CheckPrefix");
			$sCheckNumberStart = $objDatabase->Result($varResult, $i, "BCB.CheckNumberStart");
			$sCheckNumberEnd = $objDatabase->Result($varResult, $i, "BCB.CheckNumberEnd");

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sReference . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCheckNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCheckPrefix . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sTransactionDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			</tr>';
		}
		
		$sCheckBookSelect = '<select class="form1" name="selCheckBook" onchange="window.location=\'?checkbookid=\'+GetSelectedListBox(\'selCheckBook\');" id="selCheckBook">
		<option value="0">All Check Books</option>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BCB WHERE BCB.OrganizationId='" . cOrganizationId . "' ORDER BY BCB.CheckPrefix");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sCheckBookSelect .= '<option ' . (($iBankCheckBookId == $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId") . '">' . $objDatabase->Result($varResult, $i, "BCB.CheckPrefix") . ' (' . $objDatabase->Result($varResult, $i, "BCB.CheckNumberStart") . ' - ' . $objDatabase->Result($varResult, $i, "BCB.CheckNumberEnd") . ')</option>';
		$sCheckBookSelect .= '</select>';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td align="left">
          <form method="GET" action="">Search for a Bank Check:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Bank Check Book" title="Search for Bank Check Book" border="0"></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>
		   ' . $sCheckBookSelect . '
		  </td>
		 </tr>		 
		</table>
        <br /><br />';

		return($sReturn);
    }

	function ShowAllIssuedChecks($iBankCheckBookId=0, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');		
		
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
		
        if ($sSortBy == "") $sSortBy = "GJ.TransactionDate DESC";
        
		$iPagingLimit = cPagingLimit;
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;
		
		$sTitle = 'Issued Checks';
		if ($iBankCheckBookId == '') $iBankCheckBookId = 0;
		
		if ($iBankCheckBookId > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BCB WHERE BCB.OrganizationId='" . cOrganizationId . "' AND BCB.BankCheckBookId='$iBankCheckBookId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Book Id');
				$sCheckPrefix = $objDatabase->Result($varResult, 0, "BCB.CheckPrefix");
			
			$sCheckBookCondition = "AND BCB.BankCheckBookId='$iBankCheckBookId'";
			$sTitle = "Issued Checks of " . $sCheckPrefix;
		}		
        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((BCB.CheckPrefix LIKE '%$sSearch%') OR (BCB.BankCheckBookId LIKE '%$sSearch%') OR (BCB.CheckNumberStart LIKE '%$sSearch%') OR (BCB.CheckNumberEnd LIKE '%$sSearch%') OR (B.BankName LIKE '%$sSearch%') OR (BA.BankAccountNumber LIKE '%$sSearch%') OR (BA.BranchName LIKE '%$sSearch%') OR (GJ.Reference LIKE '%$sSearch%') OR (GJ.CheckNumber LIKE '%$sSearch%') )";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }
            
		$iTotalRecords = $objDatabase->DBCount("fms_accounts_generaljournal AS GJ INNER JOIN fms_banking_bankcheckbooks AS BCB ON BCB.BankCheckBookId = GJ.BankCheckBookId INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = BCB.BankAccountId INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId", " BCB.OrganizationId='" . cOrganizationId . "' $sCheckBookCondition $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=issuedchecks&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);
		
        $varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankcheckbooks AS BCB ON BCB.BankCheckBookId = GJ.BankCheckBookId
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId        
        WHERE BCB.OrganizationId='" . cOrganizationId . "' $sCheckBookCondition $sSearchCondition 
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");		
		
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		 
		  <td align="left"><span class="WhiteHeading">Reference&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.Reference&sortorder="><img src="../images/sort_up.gif" alt="Sort by Reference in Ascending Order" title="Sort by Reference in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.Reference&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Reference in Descending Order" title="Sort by Reference in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Check Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.CheckNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Number in Ascending Order" title="Sort by Check Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.CheckNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Number in Descending Order" title="Sort by Check Number in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Check Prefix&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BCB.CheckPrefix&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Prefix in Ascending Order" title="Sort by Check Prefix in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BCB.CheckPrefix&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Prefix in Descending Order" title="Sort by Check Prefix in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Transaction Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Transaction Date in Ascending Order" title="Sort by Transaction Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Transaction Date in Descending Order" title="Sort by Transaction Date in Descending Order" border="0" /></a></span></td>
		  <td width="6%" colspan="6"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			$iId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
			$sReference = $objDatabase->Result($varResult, $i, "GJ.Reference");
			$sCheckNumber = $objDatabase->Result($varResult, $i, "GJ.CheckNumber");
			$dTransactionDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
			$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
			$iCurrentCheckBookId = $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId");
			$sCheckPrefix = $objDatabase->Result($varResult, $i, "BCB.CheckPrefix");
			$sCheckNumberStart = $objDatabase->Result($varResult, $i, "BCB.CheckNumberStart");
			$sCheckNumberEnd = $objDatabase->Result($varResult, $i, "BCB.CheckNumberEnd");
			
			$iBankCheckBookId = $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId");
			$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
			$sReference = $objDatabase->Result($varResult, $i, "GJ.Reference");
			$dTotalDebit = $objDatabase->Result($varResult, $i, "GJ.TotalDebit");
			$iBankId = $objDatabase->Result($varResult, $i, "B.BankId");

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sReference . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCheckNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCheckPrefix . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sTransactionDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Voucher Details\', \'../accounts/generaljournal_details.php?id=' . $iGeneralJournalId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this General Journal Entry Details" title="View this General Journal Entry Details"></a></td>
			</tr>';
		}
		
		/*
		$sCheckBookSelect = '<select class="form1" name="selCheckBook" onchange="window.location=\'?checkbookid=\'+GetSelectedListBox(\'selCheckBook\');" id="selCheckBook">
		<option value="0">All Check Books</option>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BCB WHERE BCB.OrganizationId='" . cOrganizationId . "' ORDER BY BCB.CheckPrefix");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sCheckBookSelect .= '<option ' . (($iBankCheckBookId == $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId") . '">' . $objDatabase->Result($varResult, $i, "BCB.CheckPrefix") . ' (' . $objDatabase->Result($varResult, $i, "BCB.CheckNumberStart") . ' - ' . $objDatabase->Result($varResult, $i, "BCB.CheckNumberEnd") . ')</option>';
		$sCheckBookSelect .= '</select>';
		*/
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td align="left">
          <form method="GET" action=""><td align="left" colspan="2">Search for a Issued Check:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Issued Check" title="Search for a Issued Check" border="0"><input type="hidden" name="id" id="id" value="'. $iBankCheckBookId . '" /><input type="hidden" name="pagetype" id="pagetype" value="issuedchecks" /></td></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>
		   ' . $sCheckBookSelect . '
		  </td>
		 </tr>		 
		</table>
        <br /><br />';

		return($sReturn);
    }

	function IssuedCheckBookDetails($iBankId, $sPayTo,  $iVoucherTitle, $dTransactionDate, $dTotalAmount)
	{
		global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;
	
		$sButtons_Print = '<div style="text-align:right;"><a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a></div>';

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#838274" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td valign="top">';
		$sReturn .= $sButtons_Print;
		
		$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
		
		
		if($sPayTo == '') $sPayTo = 'SELF';

		if($iBankId == 1) $background_bank_image = 'ubl.jpg';
		else if($iBankId==4 ) $background_bank_image = 'mcb.jpg';
		
		if($iVoucherTitle == 1) 
		{
			$sStars="******"; 
			$dProcessingFees = $dTotalAmount*2/100; // 2% Processing Fees
			$dHealthInssuranceFees = 125; // Fix Amount			
			$dTotalAmount = $dTotalAmount-($dProcessingFees+$dHealthInssuranceFees);
		}
		else $sStars='';

		$sTotalAmountInWords = $objGeneral->NumberToWord($dTotalAmount);
		
		$sReturn .= '
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr bgcolor="#ffffff"><td style="background-image:url(../../system/data/bankcheques/'.$background_bank_image.'); background-repeat:no-repeat; height: 287px;">
				<div style="font-size:14px; font-weight:bold; text-align:right; width: 560px; padding-bottom: 10px;">' . $sTransactionDate . '</div><div style="clear:both;"></div>
				<div style="float:left; font-size:14px; font-weight:bold; height: 25px; width: 400px; padding-left: 50px; margin-bottom: 6px;">'.$sPayTo.'</div><div style="float:left; width:120px; height:25px; text-align:right;padding-bottom:10px;"><span style="font-size:22px;font-weight:bold;">'.$sStars.'</span></div><div style="clear:both;"></div>
				<div style="float:left; width:340px; font-size:14px; font-weight:bold; height: 25px; padding-left: 60px; ">' . $sTotalAmountInWords . ' only.</div><div style="font-size:14px; font-weight:bold; text-align: right; width: 165px; height: 25px; margin-bottom: 6px; float:left;text-align:right;vertical-align:top"><span style="font-size:18px;" valign="bottom">***</span>' . number_format($dTotalAmount, 0) . '/=</div><div style="clear:both;"></div>
				<div style="float:left; font-size:14px; font-weight:bold; height: 25px; padding-left: 20px; padding-bottom: 5px;"></div>
			 </td></tr>
			</table>';
		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
	}
	
	
}

/* Bank Checks Cancelled */
class clsBankChecksCancelled extends clsBankCheckBooks
{
	function __construct()
	{

	}
	
	function ShowAllCancelledChecks($iBankCheckBookId, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;		
		global $objEmployee;	
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');		
		
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
		
        if ($sSortBy == "") $sSortBy = "BCB.BankCheckBookId DESC";
        
		$iPagingLimit = cPagingLimit;
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BCB WHERE BCB.OrganizationId='" . cOrganizationId . "' AND BCB.BankCheckBookId='$iBankCheckBookId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Book Id');
			$sCheckPrefix = $objDatabase->Result($varResult, 0, "BCB.CheckPrefix");			
		$sCheckBookCondition = "AND BCB.BankCheckBookId='$iBankCheckBookId'";
		
		$sTitle = "Cancelled Checks of " . $sCheckPrefix;
			
		if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((BCB.CheckPrefix LIKE '%$sSearch%') OR (BCC.CancelledCheckNumber LIKE '%$sSearch%'))";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }
            
		$iTotalRecords = $objDatabase->DBCount("fms_banking_bankcheckbooks_cancelledchecks AS BCC INNER JOIN fms_banking_bankcheckbooks AS BCB ON BCB.BankCheckBookId = BCC.BankCheckBookId INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = BCB.BankAccountId INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId", "BCC.OrganizationId='" . cOrganizationId . "' $sCheckBookCondition $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=cancelledchecks&id=' .$iBankCheckBookId . '&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

        $varResult = $objDatabase->Query("
		SELECT * FROM fms_banking_bankcheckbooks_cancelledchecks AS BCC
		INNER JOIN fms_banking_bankcheckbooks AS BCB ON BCB.BankCheckBookId = BCC.BankCheckBookId
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = BCB.BankAccountId
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId        
        WHERE BCC.OrganizationId='" . cOrganizationId . "' $sCheckBookCondition $sSearchCondition 
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");		
        
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		 <td align="left"><span class="WhiteHeading">Branch&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.BranchName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Branch Name in Ascending Order" title="Sort by Branch Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.BranchName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Branch Name in Descending Order" title="Sort by Branch Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Account Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.BankAccountNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Account Number in Ascending Order" title="Sort by Reference in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.BankAccountNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Account Number in Descending Order" title="Sort by Bank Account Number in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Check Prefix&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BCB.CheckPrefix&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Prefix in Ascending Order" title="Sort by Check Prefix in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BCB.CheckPrefix&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Prefix in Descending Order" title="Sort by Check Prefix in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Check Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BCC.CancelledCheckNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Number in Ascending Order" title="Sort by Check Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BCC.CancelledCheckNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Number in Descending Order" title="Sort by Check Number in Descending Order" border="0" /></a></span></td>
		 <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			$iCancelledCheckId= $objDatabase->Result($varResult, $i, "BCC.CancelledCheckId");
			$sBranchName = $objDatabase->Result($varResult, $i, "BA.BranchName");
			$sBankAccountNumber= $objDatabase->Result($varResult, $i, "BA.BankAccountNumber");
			$iCancelledCheckNumber = $objDatabase->Result($varResult, $i, "BCC.CancelledCheckNumber");
			$iCurrentCheckBookId = $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId");
			$sCheckPrefix = $objDatabase->Result($varResult, $i, "BCB.CheckPrefix");
			$sCheckNumberStart = $objDatabase->Result($varResult, $i, "BCB.CheckNumberStart");
			$sCheckNumberEnd = $objDatabase->Result($varResult, $i, "BCB.CheckNumberEnd");
			
			$sEditCancelledCheck = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Cancelled Cheqck\', \'../banking/bankcheckbooks.php?pagetype=cancelledchecks_details&action2=edit&id=' . $iBankCheckBookId . '&cancelledcheckid=' . $iCancelledCheckId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Cancelled Bank Check Details" title="Edit Cancelled Bank Check Details"></a></td>';
			$sDeleteCancelledCheck = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Cancelled Check?\')) {window.location = \'?action=DeleteCancelledCheck&id=' . $iBankCheckBookId. '&cancelledcheckid=' . $iCancelledCheckId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Cancelled Bank Check" title="Delete this Cancelled Bank Check"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[2] == 0)	$sEditCancelledCheck= '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[3] == 0)	$sDeleteCancelledCheck = '<td class="GridTD">&nbsp;</td>';

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			
			<td class="GridTD" align="left" valign="top">' . $sBranchName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			<td class="GridTD" align="left" valign="top">' . $sBankAccountNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>			
			<td class="GridTD" align="left" valign="top">' . $sCheckPrefix . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			<td class="GridTD" align="left" valign="top">' . $iCancelledCheckNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Cancelled Bank Check\', \'../banking/bankcheckbooks.php?pagetype=cancelledchecks_details&id=' . $iBankCheckBookId . '&cancelledcheckid=' . $iCancelledCheckId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Cancelled Bank Check Details" title="View this Cancelled Bank Check Details"></a></td> 
			' . $sEditCancelledCheck . ' ' . $sDeleteCancelledCheck . '
			</tr>';
		}
		
		$sAddNewCancelledCheck = '<td width="10%" align="left"><input onclick="window.top.CreateTab(\'tabContainer\', \'Cancel Check\', \'../banking/bankcheckbooks.php?pagetype=cancelledchecks_details&action2=addnew&id=' . $iBankCheckBookId . '\', \'520px\', true);" type="button" class="formbutton" value="Cancel Check"></td>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[1] == 0)
			$sAddNewCancelledCheque = '';		
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  ' . $sAddNewCancelledCheck . '
          <form method="GET" action=""><td align="left" colspan="2">Search for a Cancelled Check:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Cancelled Check" title="Search for a Cancelled Check" border="0"><input type="hidden" name="id" id="id" value="'. $iBankCheckBookId . '" /><input type="hidden" name="pagetype" id="pagetype" value="cancelledchecks" /></td></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>
		   ' . $sCheckBookSelect . '
		  </td>
		 </tr>		 
		</table>
        <br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Cancelled Check Details" alt="View this Cancelled Check Details"></td><td>View this Cancelled Check Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Cancelled Check Details" alt="Edit this Cancelled Check Details"></td><td>Edit this Cancelled Check Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Cancelled Check Details" alt="Delete this Cancelled Check Details"></td><td>Delete this Cancelled Check Details</td></tr>
        </table>';

		return($sReturn);
    }

    function CancelledCheckDetails($iBankCheckBookId, $iCancelledCheckId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
		global $objEmployee;		

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_banking_bankcheckbooks_cancelledchecks AS BCC
		INNER JOIN fms_banking_bankcheckbooks AS BC ON BC.BankCheckBookId = BCC.BankCheckBookId
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId= BC.BankAccountId
		INNER JOIN organization_employees AS E ON E.EmployeeId= BCC.CancelledCheckAddedBy
		WHERE BCC.BankCheckBookId = '$iBankCheckBookId' AND BCC.CancelledCheckId='$iCancelledCheckId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=cancelledchecks_details&action2=edit&id=' . $iBankCheckBookId . '&cancelledcheckid=' . $iCancelledCheckId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Cancelled Bank Check" title="Edit this Cancelled Bank Check" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Cancelled Bank Check?\')) {window.location = \'?pagetype=cancelledchecks_details&action=DeleteCancelledCheck&id=' . $iBankCheckBookId . '&cancelledcheckid=' .$iCancelledCheckId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Cancelled Bank Check" title="Delete this Cancelled Bank Check" /></a>';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[3] == 0)	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Cancelled Bank Check Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iBankCheckBookId = $objDatabase->Result($varResult, 0, "BCC.BankCheckBookId");
		    	$iCancelledCheckId = $objDatabase->Result($varResult, 0, "BCC.CancelledCheckId");

		    	$iCancelledCheckNumber = $objDatabase->Result($varResult, 0, "BCC.CancelledCheckNumber");
				$sCancelledCheckNumber = $iCancelledCheckNumber;
				
		    	$sNotes = $objDatabase->Result($varResult, 0, "BCC.Notes");

		    	$sBankAccountNumber = $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");

            	$sCheckPrefix = $objDatabase->Result($varResult, 0, "BC.CheckPrefix");
            	$sCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
            	$sCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");
				$sCheckBook = $sCheckPrefix . ' (' . $sCheckNumberStart . ' - ' . $sCheckNumberEnd . ')';
				$sCheckBook = $sCheckBook . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sCheckBook)) . '\', \'../banking/bankcheckbooks.php?pagetype=details&id=' . $iBankCheckBookId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Check Book Information" title="Check Book Information" border="0" /></a>';

            	$dCancelledCheckAddedOn = $objDatabase->Result($varResult, $i, "BCC.CancelledCheckAddedOn");
            	$sCancelledCheckAddedOn = date("F j, Y", strtotime($dCancelledCheckAddedOn)) . ' at ' . date("g:i a", strtotime($dCancelledCheckAddedOn));
				$iEmployeeId = $objDatabase->Result($varResult, 0, "E.EmployeeId");
				
				$sCancelledCheckAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sCancelledCheckAddedBy = $sCancelledCheckAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sCancelledCheckAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
 		    }

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sCancelledCheckNumber = '<input type="text" name="txtCancelledCheckNumber" id="txtCancelledCheckNumber" class="form1" value="' . $iCancelledCheckNumber . '" size="30" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$iCancelledCheckNumber = $objGeneral->fnGet("txtCancelledCheckNumber");

				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BC
				WHERE BC.OrganizationId='" . cOrganizationId . "' AND BC.BankCheckBookId = '$iBankCheckBookId'");
				if ($objDatabase->RowsNumber($varResult) <= 0) die("Sorry, Invalid Bank Check Id");

				$sCheckPrefix = $objDatabase->Result($varResult, 0, "BC.CheckPrefix");
            	$sCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
            	$sCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");
				$sCheckBook = $sCheckPrefix . ' (' . $sCheckNumberStart . ' - ' . $sCheckNumberEnd . ')';
				$sCheckBook = $sCheckBook . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sCheckBook)) . '\', \'../banking/bankcheckbooks.php?pagetype=details&id=' . $iBankCheckBookId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Check Book Information" title="Check Book Information" border="0" /></a>';

				
				//$sCancelledCheckNumber = $objGeneral->fnGet("txtCancelledCheckNumber");
				$iCancelledCheckNumber = $this->FindNextAvailableCheck($iBankCheckBookId);
				$sNotes = $objGeneral->fnGet("txtNotes");

				$sCancelledCheckNumber = '<input type="text" name="txtCancelledCheckNumber" id="txtCancelledCheckNumber" class="form1" value="' . $iCancelledCheckNumber . '" size="8" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

        		$sCancelledCheckAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				$sCancelledCheckAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sCancelledCheckAddedBy = $sCancelledCheckAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sCancelledCheckAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
			}
			

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
	         function ValidateForm()
	         {
	         	if (!isNumber(GetVal(\'txtCancelledCheckNumber\'))) return(AlertFocus(\'Please enter a valid Check Number\', \'txtCancelledCheckNumber\'));
         		return true;
	         }
	        </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Cacncelled Information:</span></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200px;">Check Book:</td><td><strong>' . $sCheckBook . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Cancelled Check Number:</td><td><strong>' . $sCancelledCheckNumber . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Check Added On:</td><td><strong>' . $sCancelledCheckAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Check Added by:</td><td><strong>' . $sCancelledCheckAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBankCheckBookId . '"><input type="hidden" name="cancelledcheckId" id="cancelledcheckId" value="' . $iCancelledCheckId. '"><input type="hidden" name="action" id="action" value="UpdateCancelledCheck"></form></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Cancel Check" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBankCheckBookId . '"><input type="hidden" name="action" id="action" value="AddNewCancelledCheck"></form></div>';
		}

		$sReturn .= '<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }
	
	function AddNewCancelledCheck($iBankCheckBookId, $iCancelledCheckNumber, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[1] == 0) // Add Disabled
			return(1010);

        $varNow = $objGeneral->fnNow();
		$iCancelledCheckAddedBy = $objEmployee->iEmployeeId;      
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BC WHERE BC.BankCheckBookId='$iBankCheckBookId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Id');

        $iCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
        $iCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");
				
        if (($iCancelledCheckNumber < $iCheckNumberStart) || ($iCancelledCheckNumber > $iCheckNumberEnd))
        	$objGeneral->fnRedirect('?error=5607&id=' . $iBankCheckBookId . '&action2=addnew');

        if ($objDatabase->DBCount("fms_banking_bankcheckbooks_cancelledchecks AS BCC", "BCC.BankCheckBookId='$iBankCheckBookId' AND BCC.CancelledCheckNumber='$iCancelledCheckNumber'") > 0)
        	$objGeneral->fnRedirect('?error=5600&id=' . $iBankCheckBookId . '&action2=addnew');

        if ($objDatabase->DBCount("fms_accounts_generaljournal AS GJ", "GJ.BankCheckBookId='$iBankCheckBookId' AND GJ.CheckNumber='$iCancelledCheckNumber'  AND GJ.IsDeleted ='0'") > 0)
        	$objGeneral->fnRedirect('?error=5608&id=' . $iBankCheckBookId . '&action2=addnew');
			
		$sNotes = $objDatabase->RealEscapeString($sNotes);		

        $varResult = $objDatabase->Query("INSERT INTO fms_banking_bankcheckbooks_cancelledchecks
        (BankCheckBookId, CancelledCheckNumber, Notes, CancelledCheckAddedOn, CancelledCheckAddedBy)
        VALUES
        ('$iBankCheckBookId', '$iCancelledCheckNumber', '$sNotes', '$varNow', '$iCancelledCheckAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks_cancelledchecks AS BCC WHERE BCC.BankCheckBookId='$iBankCheckBookId' AND BCC.CancelledCheckAddedOn='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
		{			
			$objSystemLog->AddLog("Add New Canclled Bank Check - " . $iCancelledCheckNumber);			
			
			$objGeneral->fnRedirect('?pagetype=cancelledchecks_details&error=5601&id=' . $objDatabase->Result($varResult, 0, "BCC.BankCheckBookId") . '&cancelledcheckid=' . $objDatabase->Result($varResult, 0, "BCC.CancelledCheckId"));
		}
        else
            return(5602);
    }
	
    function UpdateCancelledCheck($iBankCheckBookId, $iCancelledCheckId, $iCancelledCheckNumber, $sNotes)
    {
        global $objDatabase;
		global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[2] == 0) // Update Disabled
			return(1010);

		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks AS BC WHERE BC.OrganizationId='" . cOrganizationId . "' AND BC.BankCheckBookId='$iBankCheckBookId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Id');

        $iCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
        $iCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");

        if (($iCancelledCheckNumber < $iCheckNumberStart) || ($iCancelledCheckNumber > $iCheckNumberEnd))
        	return(5607); //$objGeneral->fnRedirect('?error=5556&id=' . $iBankCheckBookId . '&action2=addnew');

        if ($objDatabase->DBCount("fms_banking_bankcheckbooks_cancelledchecks AS BCC", "BCC.BankCheckBookId='$iBankCheckBookId' AND CancelledCheckId <> '$iCancelledCheckId' AND BCC.CancelledCheckNumber='$iCancelledCheckNumber'") > 0)
        	return(5600); //$objGeneral->fnRedirect('?error=5557&id=' . $iBankCheckBookId . '&action2=addnew');

        if ($objDatabase->DBCount("fms_accounts_generaljournal AS GJ", "GJ.BankCheckBookId='$iBankCheckBookId' AND GJ.CheckNumber='$iCancelledCheckNumber'  AND GJ.IsDeleted ='0'") > 0)
        	return(5608); //	$objGeneral->fnRedirect('?error=5558&id=' . $iBankCheckBookId . '&action2=addnew');


        $varResult = $objDatabase->Query("
        UPDATE fms_banking_bankcheckbooks_cancelledchecks SET
        	CancelledCheckNumber='$iCancelledCheckNumber',
            Notes='$sNotes'
        WHERE OrganizationId='" . cOrganizationId . "' AND BankCheckBookId='$iBankCheckBookId' AND CancelledCheckId='$iCancelledCheckId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Update Bank Check Canclled- " . $iCancelledCheckNumber);
						
			$objGeneral->fnRedirect('../banking/bankcheckbooks.php?pagetype=cancelledchecks_details&error=5603&id=' . $iBankCheckBookId . '&cancelledcheckid=' . $iCancelledCheckId);
			//return(5603);
		}
        else
            return(5604);
    }

    function DeleteCancelledCheck($iBankCheckBookId, $iCancelledCheckId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankCheckBooks_CancelledChecks[3] == 0) // Delete Disabled
		{
			if ($objGeneral->fnGet("pagetype") == "cancelledchecks_details")
				$objGeneral->fnRedirect('?pagetype=cancelledchecks_details&id=' . $iBankCheckBookId . '&cancelledcheckid='.$iCancelledCheckId . '&error=1010');
			else
				$objGeneral->fnRedirect('?pagetype=cancelledchecks&id=' . $iBankCheckBookId . '&error=1010');
		}
			
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckbooks_cancelledchecks AS BCC WHERE BCC.OrganizationId='" . cOrganizationId . "' AND BCC.CancelledCheckId='$iCancelledCheckId'");       	
       	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Cancelled Check Id');
		$iCancelledCheckNumber = $objDatabase->Result($varResult, 0, "BCC.CancelledCheckNumber");

        $varResult = $objDatabase->Query("DELETE FROM fms_banking_bankcheckbooks_cancelledchecks WHERE BankCheckBookId='$iBankCheckBookId' AND CancelledCheckId='$iCancelledCheckId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Delete Canclled Check" . $iCancelledCheckNumber);
			
			if ($objGeneral->fnGet("pagetype") == "cancelledchecks_details")
				$objGeneral->fnRedirect('?pagetype=cancelledchecks_details&id=' . $iBankCheckBookId . '&cancelledcheckid='.$iCancelledCheckId . '&error=5605');
			else
				$objGeneral->fnRedirect('?pagetype=cancelledchecks&id=' . $iBankCheckBookId . '&error=5605');
		}
        else
            return(5606);
    }   
    
}


?>