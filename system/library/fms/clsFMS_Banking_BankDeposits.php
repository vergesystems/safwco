<?php

// Class: BankDeposit
class clsBanking_BankDeposits
{
	// Class Constructor
	function __construct()
	{
	}
	
	function ShowAllBankDeposits($iBankAccountId=0, $sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_BankDeposits[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Bank Deposits";
		if ($sSortBy == "") $sSortBy = "GJ.TransactionDate DESC";

		if ($iBankAccountId == '') $iBankAccountId = 0;

		if ($iBankAccountId > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.BankAccountId='$iBankAccountId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Account Id');
				$sBankAccount = $objDatabase->Result($varResult, 0, "BA.AccountTitle") . ' ' . $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");

			$sBankAccountCondition = "AND BA.BankAccountId='$iBankAccountId'";
			$sTitle = "Bank Deposits in " . $sBankAccount;
		}
		else
		{
			$sBankAccount = "All Bank Accounts";
			$sBankAccountCondition  = "";
		}

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((B.BankName LIKE '%$sSearch%') OR (BA.BankAccountNumber LIKE'%$sSearch%') OR (GJE.Debit LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_generaljournal AS GJ INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId", "GJ.EntryType='1' AND GJ.ReceiptType='0'  AND GJ.IsDeleted ='0' $sSearchCondition $sBankAccountCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		WHERE GJ.EntryType='1' AND GJ.ReceiptType='0' AND GJ.IsDeleted ='0' $sSearchCondition $sBankAccountCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Deposit Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Deposited On in Ascending Order" title="Sort by Deposited On in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Deposited On in Descending Order" title="Sort by Deposited On in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Bank&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BankName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Name in Ascending Order" title="Sort by Bank Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BankName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Name in Descending Order" title="Sort by Bank Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Bank Account Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.AccountNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Account Number in Ascending Order" title="Sort by Bank Account Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.AccountNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Account Number in Descending Order" title="Sort by Bank Account Number in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Debit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Amount in Ascending Order" title="Sort by Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Debit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Amount in Descending Order" title="Sort by Amount in Descending Order" border="0" /></a></span></td>
		  <td width="2%" colspan="2"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
			$sReference = $objDatabase->Result($varResult, $i, "GJ.Reference");
			$iCurrentBankAccountId = $objDatabase->Result($varResult, $i, "BA.BankAccountId");

			$sBankName = $objDatabase->Result($varResult, $i, "B.BankName");
			$sBankName = $objDatabase->RealEscapeString($sBankName);
			$sBankName = stripslashes($sBankName);

			$sBranchName = $objDatabase->Result($varResult, $i, "BA.BranchName");
			$sBranchName = $objDatabase->RealEscapeString($sBranchName);
			$sBranchName = stripslashes($sBranchName);

			$sBankAccountNumber = $objDatabase->Result($varResult, $i, "BA.BankAccountNumber");
			$sBankAccountNumber = $objDatabase->RealEscapeString($sBankAccountNumber);
			$sBankAccountNumber = stripslashes($sBankAccountNumber);

			$dAmount = $objDatabase->Result($varResult, $i, "GJE.Debit");
			$sAmount = number_format($dAmount, 0);

			$dBankDepositDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
			$sBankDepositDate = date("F j, Y", strtotime($dBankDepositDate));
			$sDepositedOn = date(("F j, Y"), strtotime($dDepositedOn));

			//$sEditBankDeposit = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Bank Deposit\', \'../banking/bankdeposits_details.php?action2=edit&id=' . $iBankDepositId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Bank this Deposit Details" title="Edit this Bank Deposit Details"></a></td>';
			//$sDeleteBankDeposit = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Bank Deposit?\')) {window.location = \'?action=DeleteBankDeposit&id=' . $iBankDepositId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank Deposit" title="Delete this Bank Deposit"></a></td>';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sBankDepositDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sBankName . ' - ' . $sBranchName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sBankAccountNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . $sAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Transaction Details\', \'../accounts/generaljournal.php?pagetype=details&id=' . $iGeneralJournalId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Deposit Details" title="View this Bank Deposit Details"></a></td>
			</tr>';
		}

		$sBankAccountSelect = '<select class="form1" name="selBankAccount" onchange="window.location=\'?bankaccountid=\'+GetSelectedListBox(\'selBankAccount\');" id="selBankAccount">
		<option value="0">All Bank Accounts</option>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA ORDER BY BA.AccountTitle");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sBankAccountSelect .= '<option ' . (($iBankAccountId == $objDatabase->Result($varResult, $i, "BA.BankAccountId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . '">' . $objDatabase->Result($varResult, $i, "BA.AccountTitle") . ' (' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . ')</option>';
		$sBankAccountSelect .= '</select>';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>		   
 		   <form method="GET" action="">Search for a Bank Deposit:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Bank Deposit" title="Search for Bank Deposit" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>
		   ' . $sBankAccountSelect . '
		  </td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Transaction Details" alt="View this Transaction Details"></td><td>View this Transaction Details</td></tr>
		</table>';

		return($sReturn);
	}
}

?>