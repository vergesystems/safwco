<?php

// Class: BankReconciliation
class clsBanking_BankReconciliation
{
	// Class Constructor
	function __construct()
	{		
	}
	
	function ShowBankReconciliation($sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if($objEmployee->objEmployeeRoles->iEmployeeRole_FMS_Banking_BankReconciliation[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
			
		$iBankId = $objGeneral->fnGet("selBank");
		$iBankAccountId = $objGeneral->fnGet("selBankAccount");
		if ($iBankAccountId == "") $iBankAccountId = 0;
		$dStartDate = $objGeneral->fnGet("txtStartDate");
		$dEndDate = $objGeneral->fnGet("txtEndDate");
		
		if ($dStartDate == "") $dStartDate = date("Y-m-01");
		if ($dEndDate == "") $dEndDate = date("Y-m-d");
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();


		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		
		$sBank = '<div id="divBanks" style="display:inline;"><select name="selBank" id="selBank" onchange="BankAccounts(GetSelectedListBox(\'selBank\'),' . $iBankAccountId . ');" class="Tahoma14">
		<option value="0">Select Bank </option>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B ORDER BY B.BankName");
		for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
			$sBank .= '<option ' . (($iBankId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . '</option>';
		$sBank .='</select></div>';
		
		// Bank Accounts
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.Status='1'");
		$iNoOfBankAccounts = $objDatabase->RowsNumber($varResult);
		$sBankAccounts = 'var aBankAccounts = MultiDimensionalArray(' . $iNoOfBankAccounts . ', 4);';
		for ($i=0; $i < $iNoOfBankAccounts; $i++)
		{
			$sBankAccounts .= 'aBankAccounts[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BA.BankId") . ';';
			$sBankAccounts .= 'aBankAccounts[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . ';';
			$sBankAccounts .= 'aBankAccounts[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . '";';
			$sBankAccounts .= 'aBankAccounts[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "BA.AccountTitle") . '";';
		}
				
		$sBankAccount = '<select name="selBankAccount" id="selBankAccount" class="Tahoma14"><option value="-1">Select Bank Account</option></select>';
		
		$sStartDate = '<input type="text" size="10" id="txtStartDate" name="txtStartDate" class="Tahoma14" value="' . $dStartDate . '" />' . $objjQuery->Calendar('txtStartDate');
		$sEndDate = '<input type="text" size="10" id="txtEndDate" name="txtEndDate" class="Tahoma14" value="' . $dEndDate . '" />' . $objjQuery->Calendar('txtEndDate');
		$sShowReconciledTransactions = '<input type="checkbox" name="chkShowReconciled" id="chkShowReconciled" value="on" />';
		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Bank Reconciliation</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		$sReturn .= '<br />
		<script language="JavaScript" type="text/javascript">
		 ' . $sBankAccounts . '
		
		function BankAccounts(iBankId, iDefaultBankAccountId)
		{
			OptionsList_RemoveAll("selBankAccount");

			FillSelectBox("selBankAccount", "Select Bank Account", 0);
			for (i=0; i < aBankAccounts.length; i++)
			{
				if (iBankId == aBankAccounts[i][0])
					FillSelectBox("selBankAccount", aBankAccounts[i][3] + " (" + aBankAccounts[i][2] + ")", aBankAccounts[i][1]);
			}
		
			if (iDefaultBankAccountId > 0)	
				document.getElementById("selBankAccount").value = iDefaultBankAccountId;
		}
		
		function FindTransactions()
		{
			if(GetSelectedListBox(\'selBank\') <= 0) return(AlertFocus(\'Please select a Bank\', \'selBank\'));
			if(GetSelectedListBox(\'selBankAccount\') <= 0) return(AlertFocus(\'Please select a Bank Account\', \'selBankAccount\'));
			if (!isDate(GetVal(\'txtStartDate\'))) return(AlertFocus(\'Please select a valid Start Date\', \'txtStartDate\'));
			if (!isDate(GetVal(\'txtEndDate\'))) return(AlertFocus(\'Please select a valid End Date\', \'txtEndDate\'));
			
			return(true);
		}
		
		</script>
		<table border="0" cellspacing="0" cellpadding="3" style="width:550px;" align="center">
		 <tr>
		  <td>
		   <fieldset><legend style="font-size:14px; font-weight:bold;">Bank Reconciliation Criteria:</legend>
		    <form method="post" action="#ReconciliationTransactions" onsubmit="return FindTransactions();">
		    <table border="0" cellspacing="5" cellpadding="3" width="95%" align="center">
		     <tr><td width="30%" align="right">Select Bank:</td><td>' . $sBank . '</td></tr>
		     <tr><td align="right">Select Bank Account:</td><td>' . $sBankAccount . '</td></tr>
			 <tr><td align="right">Start Date:</td><td>' . $sStartDate . '</td></tr>
			 <tr><td align="right">End Date:</td><td>' . $sEndDate . '</td></tr>
			 <tr><td align="right">Show Reconciled Transactions:</td><td>' . $sShowReconciledTransactions . '</td></tr>
			 <tr><td></td><td><input type="submit" class="AdminFormButton1" value="Find Transactions" /></td></tr>
		    </table>
		    <input type="hidden" name="action2" id="action2" value="FindTransactions" />
		    </form>
		   </fieldset>
		   <br />
		  </td>
		 </tr>
		</table>';
		
		if ($sAction2 == "FindTransactions")
		{
			$iShowReconciledTransactions = $objGeneral->fnGet("chkShowReconciled");
			if($iShowReconciledTransactions == "")
			{
				$sReturn .= '<script language="JavaScript" type="text/javascript">' . (($iBankId > 0) ? 'BankAccounts(' . $iBankId . ', ' . $iBankAccountId . ');' : '') . '</script>';
				$sReturn .= $this->ShowTransactionsForReconciliation();
			}
			else
			{
				$sReturn .= '<script language="JavaScript" type="text/javascript">' . (($iBankId > 0) ? 'BankAccounts(' . $iBankId . ', ' . $iBankAccountId . ');' : '') . '</script>';
				$sReturn .= $this->ShowTransactionsForReconciliation_Undo();
			}
			
		}
		
		$sReturn .= '</td></tr></table>
		</td></tr></table>';
		
		return($sReturn);	
	}

	function ShowTransactionsForReconciliation()
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		
		$iBankId = $objGeneral->fnGet("selBank");
		$iBankAccountId = $objGeneral->fnGet("selBankAccount");
		$dStartDate = $objGeneral->fnGet("txtStartDate");
		$dEndDate = $objGeneral->fnGet("txtEndDate");
		
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr>
		  <td>
		  <a name="#ReconciliationTransactions"></a>
		   <form method="post" action="../banking/bankreconciliation_show.php">
		   <table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="70%" align="center">
		    <tr style="background-color:#ffffff;">
		     <td colspan="8"><span class="title2" style="color:#003366">Select transactions that settles with your bank account:</span></td>
   	        </tr>
   	        <tr class="GridTR">
   	         <td align="center" width="3%"><span class="WhiteHeading">S#</span></td>
			 <td style="width:100px;"><span class="WhiteHeading">Transaction Date</span></td>
			 <td style="width:110px;" align="left"><span class="WhiteHeading">Reconciliation Date</span></td>
			 <td style="width:200px;" align="left"><span class="WhiteHeading">Details</span></td>
			 <td style="width:100px;" align="left"><span class="WhiteHeading">Check Number</span></td>
   	         <td align="right" width="10%"><span class="WhiteHeading">Debit</span></td>
		     <td align="right" width="10%"><span class="WhiteHeading">Credit</span></td>
   	        </tr>';
			
		$varResult = $objDatabase->Query("
		(SELECT 
			'Deposit' AS 'TransactionType',
			GJE.GeneralJournalentryId AS 'GeneralJournalEntryId',
			GJE.Debit AS 'Amount',
			GJE.Detail AS 'Description',
			GJ.TransactionDate AS 'TransactionDate',
			GJ.CheckNumber AS 'CheckNumber'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		WHERE GJ.Status='0' AND GJ.EntryType='1' AND GJ.ReceiptType='0' AND GJ.BankAccountId='$iBankAccountId' AND (GJE.ReconciliationStatus='0' OR GJE.ReconciliationDateTime NOT BETWEEN '$dStartDate 00:00:00' AND  '$dEndDate 23:59:00' ) AND (GJ.TransactionDate BETWEEN '$dStartDate' AND '$dEndDate'))

		UNION 
		(SELECT 
			'Withdrawal' AS 'TransactionType',
			GJE.GeneralJournalentryId AS 'GeneralJournalEntryId',
			GJE.Credit AS 'Amount',
			GJE.Detail AS 'Description',
			GJ.TransactionDate AS 'TransactionDate',
			GJ.CheckNumber AS 'CheckNumber'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		WHERE GJ.Status='0' AND  GJ.EntryType='0' AND GJ.PaymentType='0' AND GJ.BankAccountId='$iBankAccountId' AND (GJE.ReconciliationStatus='0' OR GJE.ReconciliationDateTime NOT BETWEEN '$dStartDate 00:00:00' AND '$dEndDate 23:59:00' ) AND (GJ.TransactionDate BETWEEN '$dStartDate' AND '$dEndDate'))

		ORDER BY TransactionDate");
		
		$dTotalCredit = 0; 
		$dTotalDebit = 0;
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iGeneralJournalEntryId = $objDatabase->Result($varResult, $i, "GeneralJournalEntryId");
			$sTransactionType = $objDatabase->Result($varResult, $i, "TransactionType");
			$dTranactionDate = $objDatabase->Result($varResult, $i, "TransactionDate");
			$sDetails = $objDatabase->Result($varResult, $i, "Description");
			$dAmount = $objDatabase->Result($varResult, $i, "Amount");
			$sCheckNumber = $objDatabase->Result($varResult, $i, "CheckNumber");
			
			if ($sTransactionType == "Deposit")
			{
				$sDebit = number_format($dAmount,0);
				$sCredit = "";
				
				$dTotalDebit += $dAmount;
			}
			else
			{
				$sDebit = "";
				$sCredit = number_format($dAmount,0);
				
				$dTotalCredit += $dAmount;
			}
			
			$sReturn .= '<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td valign="top" align="center">' . ($i+1) . '</td>
			 <td valign="top" align="center"><input type="checkbox" name="chkReconciliation_' . $i . '" id="chkReconciliation_' . $i . '" value="on" /></td>
			 <td valign="top" align="left">' . date("F j, Y", strtotime($dTranactionDate)) . '</td>
			 <td valign="top">' . $sDetails . '</td>
			 <td valign="top">' . $sCheckNumber . '</td>
			 <td valign="top" align="right">
			 ' . $sDebit . '
			 </td>
			 <td valign="top" align="right">
 			 ' . $sCredit . '
			 <input type="hidden" name="hdnGeneralJournalEntryId_' . $i . '" id="hdnGeneralJournalEntryId_' . $i . '" value="' . $iGeneralJournalEntryId . '" />
 			 </td>
			</tr>';
		}

		$sReturn .= '
		    <tr style="background-color:#e7e7e7;"><td colspan="5" align="right">Total</td><td align="right">' . number_format($dTotalDebit,0) . '</td><td align="right">' . number_format($dTotalCredit, 0) . '</td></tr>
		   </table>
		  </td>
		 </tr>
	     <tr bgcolor="#ffffff">
		  <td colspan="3" align="center"><br /><input type="submit" class="AdminFormButton1" value="Save" /><br /><br /></td>
		 </tr>
		</table>
		<input type="hidden" name="hdnBankAccountId" id="hdnBankAccountId" value="' . $iBankAccountId . '" />
		<input type="hidden" name="hdnCounter" id="hdnCounter" value="' . $i . '" />
		<input type="hidden" name="action" id="action" value="SaveBankReconciliation" />
	    </form>';

		return($sReturn);
	}
	
	function ShowTransactionsForReconciliation_Undo()
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		
		$iBankId = $objGeneral->fnGet("selBank");
		$iBankAccountId = $objGeneral->fnGet("selBankAccount");
		$dStartDate = $objGeneral->fnGet("txtStartDate");
		$dEndDate = $objGeneral->fnGet("txtEndDate");
		
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr>
		  <td>
		  <a name="#ReconciliationTransactions"></a>
		   <form method="post" action="../banking/bankreconciliation_show.php">
		   <table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="70%" align="center">
		    <tr style="background-color:#ffffff;">
		     <td colspan="6"><span class="title2" style="color:#003366">Select transactions that settles with your bank account:</span></td>
   	        </tr>
   	        <tr class="GridTR">
   	         <td align="center" width="3%"><span class="WhiteHeading">S#</span></td>
			 <td width="3%"><span class="WhiteHeading"></span></td>
			 <td style="width:120px;" align="left"><span class="WhiteHeading">Date</span></td>
			 <td align="left"><span class="WhiteHeading">Details</span></td>
			 <td align="left"><span class="WhiteHeading">Check Number</span></td>
   	         <td align="right" width="10%"><span class="WhiteHeading">Debit</span></td>
		     <td align="right" width="10%"><span class="WhiteHeading">Credit</span></td>
   	        </tr>';
			
		$varResult = $objDatabase->Query("
		(SELECT 
			'Deposit' AS 'TransactionType',
			GJE.GeneralJournalentryId AS 'GeneralJournalEntryId',
			GJE.Debit AS 'Amount',
			GJE.Detail AS 'Description',
			GJ.TransactionDate AS 'TransactionDate',
			GJ.CheckNumber AS 'CheckNumber'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		WHERE GJ.Status='0' AND GJ.EntryType='1' AND GJ.ReceiptType='0' AND GJ.BankAccountId='$iBankAccountId' AND GJE.ReconciliationStatus='1' AND (GJ.TransactionDate BETWEEN '$dStartDate' AND '$dEndDate'))

		UNION 
		(SELECT 
			'Withdrawal' AS 'TransactionType',
			GJE.GeneralJournalentryId AS 'GeneralJournalEntryId',
			GJE.Credit AS 'Amount',
			GJE.Detail AS 'Description',
			GJ.TransactionDate AS 'TransactionDate',
			GJ.CheckNumber AS 'CheckNumber'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		WHERE GJ.Status='0' AND GJ.EntryType='0' AND GJ.PaymentType='0' AND GJ.BankAccountId='$iBankAccountId' AND GJE.ReconciliationStatus='1' AND (GJ.TransactionDate BETWEEN '$dStartDate' AND '$dEndDate'))

		ORDER BY TransactionDate");
		
		$dTotalCredit = 0; 
		$dTotalDebit = 0;
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iGeneralJournalEntryId = $objDatabase->Result($varResult, $i, "GeneralJournalEntryId");
			$sTransactionType = $objDatabase->Result($varResult, $i, "TransactionType");
			$dTranactionDate = $objDatabase->Result($varResult, $i, "TransactionDate");
			$sDetails = $objDatabase->Result($varResult, $i, "Description");
			$dAmount = $objDatabase->Result($varResult, $i, "Amount");
			$sCheckNumber = $objDatabase->Result($varResult, $i, "CheckNumber");
			
			if ($sTransactionType == "Deposit")
			{
				$sDebit = number_format($dAmount,0);
				$sCredit = "";
				
				$dTotalDebit += $dAmount;
			}
			else
			{
				$sDebit = "";
				$sCredit = number_format($dAmount,0);
				
				$dTotalCredit += $dAmount;
			}
			
			$sReturn .= '<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td valign="top" align="center">' . ($i+1) . '</td>
			 <td valign="top" align="center"><input type="checkbox" name="chkReconciliation_' . $i . '" id="chkReconciliation_' . $i . '" value="on" checked="true" /></td>
			 <td valign="top" align="left">' . date("F j, Y", strtotime($dTranactionDate)) . '</td>
			 <td valign="top">' . $sDetails . '</td>
			 <td valign="top">' . $sCheckNumber . '</td>
			 <td valign="top" align="right">
			 ' . $sDebit . '
			 </td>
			 <td valign="top" align="right">
 			 ' . $sCredit . '
			 <input type="hidden" name="hdnGeneralJournalEntryId_' . $i . '" id="hdnGeneralJournalEntryId_' . $i . '" value="' . $iGeneralJournalEntryId . '" />
 			 </td>
			</tr>';
		}

		$sReturn .= '
		    <tr style="background-color:#e7e7e7;"><td colspan="5" align="right">Total</td><td align="right">' . number_format($dTotalDebit,0) . '</td><td align="right">' . number_format($dTotalCredit, 0) . '</td></tr>
		   </table>
		  </td>
		 </tr>
	     <tr bgcolor="#ffffff">
		  <td colspan="3" align="center"><br /><input type="submit" class="AdminFormButton1" value="Save" /><br /><br /></td>
		 </tr>
		</table>
		<input type="hidden" name="hdnBankAccountId" id="hdnBankAccountId" value="' . $iBankAccountId . '" />
		<input type="hidden" name="hdnCounter" id="hdnCounter" value="' . $i . '" />
		<input type="hidden" name="action" id="action" value="SaveBankReconciliation_Undo" />
	    </form>';

		return($sReturn);
	}
	
	function SaveBankReconciliation()
	{
		global $objDatabase;
		global $objGeneral;
		$varNow = $objGeneral->fnNow();
		$iBankAccountId = $objGeneral->fnGet("hdnBankAccountId");
		$iCounter = $objGeneral->fnGet("hdnCounter");
		
		for ($i=0; $i < $iCounter; $i++)
		{
			$iGeneralJournalEntryId = $objGeneral->fnGet("hdnGeneralJournalEntryId_" . $i);
			$sReconsiliation = $objGeneral->fnGet("chkReconciliation_" . $i);
			
			if ($sReconsiliation == "on")
				$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_entries SET ReconciliationStatus='1',  ReconciliationDateTime = '$varNow' WHERE GeneralJournalEntryId='$iGeneralJournalEntryId'");
		}
		
		return(3270);
	}
	
	function SaveBankReconciliation_Undo()
	{
		global $objDatabase;
		global $objGeneral;
		
		$iBankAccountId = $objGeneral->fnGet("hdnBankAccountId");
		$iCounter = $objGeneral->fnGet("hdnCounter");
		
		for ($i=0; $i < $iCounter; $i++)
		{
			$iGeneralJournalEntryId = $objGeneral->fnGet("hdnGeneralJournalEntryId_" . $i);
			$sReconsiliation = $objGeneral->fnGet("chkReconciliation_" . $i);
			//print $i . ' * ' . $sReconsiliation . '<br />';
			//print("UPDATE fms_accounts_generaljournal_entries SET ReconciliationStatus='1' WHERE GeneralJournalEntryId='$iGeneralJournalEntryId'") . '<br />';
			if ($sReconsiliation == "on")
				$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_entries SET ReconciliationStatus='1' WHERE GeneralJournalEntryId='$iGeneralJournalEntryId'");
			else
				$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_entries SET ReconciliationStatus='0' WHERE GeneralJournalEntryId='$iGeneralJournalEntryId'");
		}
		
		return(3270);
	}
}

?>