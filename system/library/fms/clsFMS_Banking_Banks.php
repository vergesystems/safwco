<?php

class clsBanking_Banks
{
	public $aBankStatus;	
	function __construct()
	{
		$this->aBankStatus = array("Inactive", "Active");
		
	}
	
	function ShowAllBanks($sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        if ($sSortBy == "") $sSortBy = "B.BankId DESC";	
        
		$iPagingLimit = cPagingLimit;

		$iShow = $objGeneral->fnGet("show");		
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;


		$sTitle = 'Banks';
		if ($sSearch != "")
        {
			$sSearch = $objDatabase->RealEscapeString($sSearch);
            //$sSearchCondition = " AND ((B.BankName LIKE '%$sSearch%') OR (B.BankId LIKE '%$sSearch%') OR (B.BranchName LIKE '%$sSearch%') OR (B.BranchCode LIKE '%$sSearch%') OR (B.BankAbbreviation LIKE '%$sSearch%')) ";
			$sSearchCondition = " AND ((B.BankName LIKE '%$sSearch%') OR (B.BankId LIKE '%$sSearch%') OR (B.BankAbbreviation LIKE '%$sSearch%')) ";
			$sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

		$iTotalRecords = $objDatabase->DBCount("fms_banking_banks AS B", "B.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_banking_banks AS B
        WHERE B.OrganizationId='" . cOrganizationId . "' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td width="15%" align="left"><span class="WhiteHeading">Bank Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BankName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Name in Ascending Order" title="Sort by Bank Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BankName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Name in Descending Order" title="Sort by Bank Name in Descending Order" border="0" /></a></span></td>
		  <!--
		  <td width="15%" align="left"><span class="WhiteHeading">Branch Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BranchName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Branch Name in Ascending Order" title="Sort by Branch Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BranchName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Branch Name in Descending Order" title="Sort by Branch Name in Descending Order" border="0" /></a></span></td>
		  <td width="15%" align="left"><span class="WhiteHeading">Branch Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BranchCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Branch Code in Ascending Order" title="Sort by Branch Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BranchCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Branch Code in Descending Order" title="Sort by Branch Code in Descending Order" border="0" /></a></span></td>					
		  -->
		  <td width="15%" align="left"><span class="WhiteHeading">Bank Abbreviation&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BankAbbreviation&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Abbreviation in Ascending Order" title="Sort by Bank Abbreviation in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BankAbbreviation&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Abbreviation in Descending Order" title="Sort by Bank Abbreviation in Descending Order" border="0" /></a></span></td>
		  <td width="15%" align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iBankId = $objDatabase->Result($varResult, $i, "B.BankId");
			$sBankName = $objDatabase->Result($varResult, $i, "B.BankName");
			$sBankName = $objDatabase->RealEscapeString($sBankName);
			$sBankAbbreviation = $objDatabase->Result($varResult, $i, "B.BankAbbreviation");
			/*
			$sBranchName = $objDatabase->Result($varResult, $i, "B.BranchName");
			$sBranchName = $objDatabase->RealEscapeString($sBranchName);
			$sBranchCode = $objDatabase->Result($varResult, $i, "B.BranchCode");
			$sBranchCode = $objDatabase->RealEscapeString($sBranchCode);
			*/
			$iBankStatus = $objDatabase->Result($varResult, $i, "B.Status");
			$sBankStatus = $this->aBankStatus[$iBankStatus];			

			$sEditBank = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update' . $objDatabase->RealEscapeString(str_replace('"', '', $sBankName)) . '\', \'../banking/banks.php?pagetype=details&action2=edit&id=' . $iBankId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Bank Details" title="Edit this Bank Details"></a></td>';
			$sDeleteBank = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Bank?\')) {window.location = \'?action=DeleteBank&id=' . $iBankId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank" title="Delete this Bank"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[2] == 0)	$sEditBank = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[3] == 0)	$sDeleteBank = '<td class="GridTD">&nbsp;</td>';
			
            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sBankName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <!--
			 <td class="GridTD" align="left" valign="top">' . $sBranchName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sBranchCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 --> 
			 <td class="GridTD" align="left" valign="top">' . $sBankAbbreviation . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sBankStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View' . $objDatabase->RealEscapeString(str_replace('"', '', $sBankName)) . '\', \'../banking/banks.php?pagetype=details&id=' . $iBankId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Details" title="View this Bank Details"></a></td>
			 ' . $sEditBank . ' ' . $sDeleteBank . 
			'</tr>';
            
		}
		
		$sAddNewBank = '<td width="10%" align="left"><input onclick="window.top.CreateTab(\'tabContainer\', \'Add New Bank\', \'../banking/banks.php?pagetype=details&action2=addnew&id=' . $iStationId . '\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add New Bank"></td>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[1] == 0) // View Disabled
			$sAddNewBank = '';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  ' . $sAddNewBank . '
          <form method="GET" action=""><td align="left" colspan="2">Search for a Bank:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Bank" title="Search for Bank" border="0"></td></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Bank Details" alt="View this Bank Details"></td><td>View this Bank Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Bank Details" alt="Edit this Bank Details"></td><td>Edit this Bank Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Bank Details" alt="Delete this Bank Details"></td><td>Delete this Bank Details</td></tr>
        </table>';

		return($sReturn);
    }

    function BankDetails($iBankId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;
    	
    	$varResult = $objDatabase->Query("
		SELECT *,B.Notes AS Notes
		FROM fms_banking_banks AS B
		INNER JOIN organization_employees AS E ON E.EmployeeId = B.BankAddedBy
		WHERE B.OrganizationId='" . cOrganizationId . "' AND B.BankId='$iBankId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iBankId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Bank" title="Edit this Bank" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Bank?\')) {window.location = \'?pagetype=details&action=DeleteBank&id=' . $iBankId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank" title="Delete this Bank" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[3] == 0)	$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Bank Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#838274" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';
		
		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		        $iBankId = $objDatabase->Result($varResult, 0, "B.BankId");
            	$iBankAddedById = $objDatabase->Result($varResult, 0, "B.BankAddedBy");
				
				$sBankAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sBankAddedBy = $objDatabase->RealEscapeString($sBankAddedBy);
				$sBankAddedBy = stripslashes($sBankAddedBy);
				$sBankAddedBy = $sBankAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sGeneralJournalAddedByName)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iBankAddedById . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sBankName = $objDatabase->Result($varResult, 0, "B.BankName");
            	$sBankAbbreviation = $objDatabase->Result($varResult, 0, "B.BankAbbreviation");
				$dBankAddedOn = $objDatabase->Result($varResult, 0, "B.BankAddedOn");
				$sBankAddedOn = date('F j, Y', strtotime($dBankAddedOn)) . ' at ' . date('g:i a', strtotime($dBankAddedOn));
            	
				/*
				// Branch Information
				$sBranchName = $objDatabase->Result($varResult, 0, "B.BranchName");
				$sBranchCode = $objDatabase->Result($varResult, 0, "B.BranchCode");
				$sBranchContactPerson = $objDatabase->Result($varResult, 0, "B.BranchContactPerson");
				$sBranchAddress = $objDatabase->Result($varResult, 0, "B.BranchAddress");
				$sBranchCity = $objDatabase->Result($varResult, 0, "B.BranchCity");
				$sBranchState = $objDatabase->Result($varResult, 0, "B.BranchState");
				$sBranchZipCode = $objDatabase->Result($varResult, 0, "B.BranchZipCode");
				$sBranchCountry = $objDatabase->Result($varResult, 0, "B.BranchCountry");
				$sBranchPhoneNumber = $objDatabase->Result($varResult, 0, "B.BranchPhoneNumber");
				$sBranchEmailAddress = $objDatabase->Result($varResult, 0, "B.BranchEmailAddress");
				*/
				
				$iStatus = $objDatabase->Result($varResult, 0, "B.Status");
				$sBankStatus = $this->aBankStatus[$iStatus];
				
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
				
            	$sBankCheckSettings = $objDatabase->Result($varResult, 0, "BankCheckSettings");
            	$objBankCheckSettings = new clsBanking_BankCheckSettings();
            	
            	if ($sBankCheckSettings != "")
            		$objBankCheckSettings = unserialize($sBankCheckSettings);
 		    }
		
			if ($sAction2 == "edit")
			{
		        $sReturn .= '<form method="post" action="../banking/banks.php?pagetype=details" onsubmit="return ValidateForm();">';
				
				$sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';
				
				$sBankName = '<input type="text" name="txtBankName" id="txtBankName" class="form1" value="' . $sBankName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBankAbbreviation = '<input type="text" name="txtBankAbbreviation" id="txtBankAbbreviation" class="form1" value="' . $sBankAbbreviation . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				/*
				// Branch Information
				$sBranchName = '<input type="text" name="txtBranchName" id="txtBranchName" class="form1" value="' . $sBranchName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBranchCode = '<input type="text" name="txtBranchCode" id="txtBranchCode" class="form1" value="' . $sBranchCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBranchContactPerson = '<input type="text" name="txtBranchContactPerson" id="txtBranchContactPerson" class="form1" value="' . $sBranchContactPerson . '" size="30" />';
				
				$sBranchAddress = '<input type="text" name="txtBranchAddress" id="txtBranchAddress" class="form1" value="' . $sBranchAddress . '" size="30" />';
				$sBranchCity = '<input type="text" name="txtBranchCity" id="txtBranchCity" class="form1" value="' . $sBranchCity . '" size="30" />';
				$sBranchState = '<input type="text" name="txtBranchState" id="txtBranchState" class="form1" value="' . $sBranchState . '" size="30" />';
				$sBranchZipCode = '<input type="text" name="txtBranchZipCode" id="txtBranchZipCode" class="form1" value="' . $sBranchZipCode . '" size="30" />';
				$sBranchCountry = $sCountryString;
				$sBranchPhoneNumber = '<input type="text" name="txtBranchPhoneNumber" id="txtBranchPhoneNumber" class="form1" value="' . $sBranchPhoneNumber . '" size="30" />';
				$sBranchEmailAddress = '<input type="text" name="txtBranchEmailAddress" id="txtBranchEmailAddress" class="form1" value="' . $sBranchEmailAddress . '" size="30" />';
				*/
				
				$sBankStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aBankStatus); $i++)
					$sBankStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aBankStatus[$i];
				$sBankStatus .='</select>';
				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$objBankCheckSettings->iPosition_CheckAmount_Left = '<input type="text" name="txtPosition_CheckAmount_Left" id="txtPosition_CheckAmount_Left" class="form1" value="' . $objBankCheckSettings->iPosition_CheckAmount_Left . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmount_Top = '<input type="text" name="txtPosition_CheckAmount_Top" id="txtPosition_CheckAmount_Top" class="form1" value="' . $objBankCheckSettings->iPosition_CheckAmount_Top . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckDate_Left = '<input type="text" name="txtPosition_CheckDate_Left" id="txtPosition_CheckDate_Left" class="form1" value="' . $objBankCheckSettings->iPosition_CheckDate_Left . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckDate_Top = '<input type="text" name="txtPosition_CheckDate_Top" id="txtPosition_CheckDate_Top" class="form1" value="' . $objBankCheckSettings->iPosition_CheckDate_Top . '" size="5" />';
				$objBankCheckSettings->iPosition_CustomerName_Left = '<input type="text" name="txtPosition_CustomerName_Left" id="txtPosition_CustomerName_Left" class="form1" value="' . $objBankCheckSettings->iPosition_CustomerName_Left . '" size="5" />';
				$objBankCheckSettings->iPosition_CustomerName_Top = '<input type="text" name="txtPosition_CustomerName_Top" id="txtPosition_CustomerName_Top" class="form1" value="' . $objBankCheckSettings->iPosition_CustomerName_Top . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords_Left = '<input type="text" name="txtPosition_CheckAmountInWords_Left" id="txtPosition_CheckAmountInWords_Left" class="form1" value="' . $objBankCheckSettings->iPosition_CheckAmountInWords_Left . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords_Top = '<input type="text" name="txtPosition_CheckAmountInWords_Top" id="txtPosition_CheckAmountInWords_Top" class="form1" value="' . $objBankCheckSettings->iPosition_CheckAmountInWords_Top . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords_Break = '<input type="text" name="txtPosition_CheckAmountInWords_Break" id="txtPosition_CheckAmountInWords_Break" class="form1" value="' . $objBankCheckSettings->iPosition_CheckAmountInWords_Break . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords2_Left = '<input type="text" name="txtPosition_CheckAmountInWords2_Left" id="txtPosition_CheckAmountInWords2_Left" class="form1" value="' . $objBankCheckSettings->iPosition_CheckAmountInWords2_Left . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords2_Top = '<input type="text" name="txtPosition_CheckAmountInWords2_Top" id="txtPosition_CheckAmountInWords2_Top" class="form1" value="' . $objBankCheckSettings->iPosition_CheckAmountInWords2_Top . '" size="5" />';
 			}
			else if ($sAction2 == "addnew")
			{
		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sBankName = $objGeneral->fnGet("txtBankName");
				$sBankAbbreviation = $objGeneral->fnGet("txtBankAbbreviation");
				
				$sBranchName = $objGeneral->fnGet("txtBranchName");
				$sBranchCode = $objGeneral->fnGet("txtBranchCode");
				$sBranchContactPerson = $objGeneral->fnGet("txtBranchContactPerson");
				
				$sBranchAddress = $objGeneral->fnGet("txtBranchAddress");
		        $sBranchCity = $objGeneral->fnGet("txtBranchCity");
		        $sBranchZipCode = $objGeneral->fnGet("txtBranchZipCode");
		        $sBranchState = $objGeneral->fnGet("txtBranchState");
		        $sBranchCountry = $objGeneral->fnGet("selCountry");
		        $sBranchPhoneNumber = $objGeneral->fnGet("txtBranchPhoneNumber");		        
		        $sBranchEmailAddress = $objGeneral->fnGet("txtBranchEmailAddress");
				$iStatus = 1;
				
				$sNotes = $objGeneral->fnGet("txtNotes");
				
				$sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';
				
				$sBankName = '<input type="text" name="txtBankName" id="txtBankName" class="form1" value="' . $sBankName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBankAbbreviation = '<input type="text" name="txtBankAbbreviation" id="txtBankAbbreviation" class="form1" value="' . $sBankAbbreviation . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				
				// Branch Information
				/*
				$sBranchName = '<input type="text" name="txtBranchName" id="txtBranchName" class="form1" value="' . $sBranchName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBranchCode = '<input type="text" name="txtBranchCode" id="txtBranchCode" class="form1" value="' . $sBranchCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sBranchContactPerson = '<input type="text" name="txtBranchContactPerson" id="txtBranchContactPerson" class="form1" value="' . $sBranchContactPerson . '" size="30" />';
				
				$sBranchAddress = '<input type="text" name="txtBranchAddress" id="txtBranchAddress" class="form1" value="' . $sBranchAddress . '" size="30" />';
				$sBranchCity = '<input type="text" name="txtBranchCity" id="txtBranchCity" class="form1" value="' . $sBranchCity . '" size="30" />';
				$sBranchState = '<input type="text" name="txtBranchState" id="txtBranchState" class="form1" value="' . $sBranchState . '" size="30" />';
				$sBranchZipCode = '<input type="text" name="txtBranchZipCode" id="txtBranchZipCode" class="form1" value="' . $sBranchZipCode . '" size="30" />';
				$sBranchCountry = $sCountryString;
				$sBranchPhoneNumber = '<input type="text" name="txtBranchPhoneNumber" id="txtBranchPhoneNumber" class="form1" value="' . $sBranchPhoneNumber . '" size="30" />';
				$sBranchEmailAddress = '<input type="text" name="txtBranchEmailAddress" id="txtBranchEmailAddress" class="form1" value="' . $sBranchEmailAddress . '" size="30" />';
				*/
				
				$sBankStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aBankStatus); $i++)
					$sBankStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aBankStatus[$i];
				$sBankStatus .='</select>';
				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				
				$objBankCheckSettings = new clsBanking_BankCheckSettings();				
				$objBankCheckSettings->iPosition_CheckAmount_Left = '<input type="text" name="txtPosition_CheckAmount_Left" id="txtPosition_CheckAmount_Left" class="form1" value="' . $objGeneral->fnGet("txtPosition_CheckAmount_Left") . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmount_Top = '<input type="text" name="txtPosition_CheckAmount_Top" id="txtPosition_CheckAmount_Top" class="form1" value="' . $objGeneral->fnGet("txtPosition_CheckAmount_Top") . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckDate_Left = '<input type="text" name="txtPosition_CheckDate_Left" id="txtPosition_CheckDate_Left" class="form1" value="' . $objGeneral->fnGet("txtPosition_CheckDate_Left") . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckDate_Top = '<input type="text" name="txtPosition_CheckDate_Top" id="txtPosition_CheckDate_Top" class="form1" value="' . $objGeneral->fnGet("txtPosition_CheckDate_Top") . '" size="5" />';
				$objBankCheckSettings->iPosition_CustomerName_Left = '<input type="text" name="txtPosition_CustomerName_Left" id="txtPosition_CustomerName_Left" class="form1" value="' . $objGeneral->fnGet("txtPosition_CustomerName_Left") . '" size="5" />';
				$objBankCheckSettings->iPosition_CustomerName_Top = '<input type="text" name="txtPosition_CustomerName_Top" id="txtPosition_CustomerName_Top" class="form1" value="' . $objGeneral->fnGet("txtPosition_CustomerName_Top") . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords_Left = '<input type="text" name="txtPosition_CheckAmountInWords_Left" id="txtPosition_CheckAmountInWords_Left" class="form1" value="' . $objGeneral->fnGet("txtPosition_CheckAmountInWords_Left") . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords_Top = '<input type="text" name="txtPosition_CheckAmountInWords_Top" id="txtPosition_CheckAmountInWords_Top" class="form1" value="' . $objGeneral->fnGet("txtPosition_CheckAmountInWords_Top") . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords_Break = '<input type="text" name="txtPosition_CheckAmountInWords_Break" id="txtPosition_CheckAmountInWords_Break" class="form1" value="' . $objGeneral->fnGet("txtPosition_CheckAmountInWords_Break") . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords2_Left = '<input type="text" name="txtPosition_CheckAmountInWords2_Left" id="txtPosition_CheckAmountInWords2_Left" class="form1" value="' . $objGeneral->fnGet("txtPosition_CheckAmountInWords2_Left") . '" size="5" />';
				$objBankCheckSettings->iPosition_CheckAmountInWords2_Top = '<input type="text" name="txtPosition_CheckAmountInWords2_Top" id="txtPosition_CheckAmountInWords2_Top" class="form1" value="' . $objGeneral->fnGet("txtPosition_CheckAmountInWords2_Top") . '" size="5" />';
				
				$sBankAddedOn = date('F j, Y', strtotime($objGeneral->fnNow())) . ' at ' . date('g:i a', strtotime($objGeneral->fnNow()));
				$sBankAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $objEmployee->sEmployeeFirstName)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
			}
			
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
	        function ValidateForm()
	        {
	        	if (GetVal(\'txtBankName\') == "") return(AlertFocus(\'Please enter a valid Bank Name\', \'txtBankName\'));
	        	if (GetVal(\'txtBankAbbreviation\') == "") return(AlertFocus(\'Please enter a valid Bank Abbreviation\', \'txtBankAbbreviation\'));
         		//if (GetVal(\'txtBranchName\') == "") return(AlertFocus(\'Please enter a valid Branch Name\', \'txtBranchName\'));
				//if (GetVal(\'txtBranchCode\') == "") return(AlertFocus(\'Please enter a valid Branch Code\', \'txtBranchCode\'));
				
				
         		return true;
	        }		         
	        </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Bank Information:</span></td></tr>			 
			 <tr><td valign="top" style="width:200px;">Bank Name:</td><td><strong>' . $sBankName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Bank Abbreviation:</td><td><strong>' . $sBankAbbreviation . '</strong></td></tr>
			 <!--
				 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Branch Information:</span></td></tr>
				 <tr><td>Branch Name:</td><td><strong>' . $sBranchName . '</strong></td></tr>
				 <tr bgcolor="#edeff1"><td>Branch Code:</td><td><strong>' . $sBranchCode . '</strong></td></tr>
				 <tr><td>Branch Contact Person:</td><td><strong>' . $sBranchContactPerson . '</strong></td></tr>
				 <tr bgcolor="#edeff1"><td>Branch Address:</td><td><strong>' . $sBranchAddress . '</strong></td></tr>
				 <tr><td>Branch City:</td><td><strong>' . $sBranchCity . '</strong></td></tr>
				 <tr bgcolor="#edeff1"><td>Branch State:</td><td><strong>' . $sBranchState . '</strong></td></tr>
				 <tr><td>Branch Zip Code:</td><td><strong>' . $sBranchZipCode . '</strong></td></tr>
				 <tr bgcolor="#edeff1"><td>Branch Country:</td><td><strong>' . $sBranchCountry . '</strong></td></tr>
				 <tr><td>Branch Phone Number:</td><td><strong>' . $sBranchPhoneNumber . '</strong></td></tr>
				 <tr bgcolor="#edeff1"><td>Branch Email Address:</td><td><strong>' . $sBranchEmailAddress . '</strong></td></tr>
			 -->
			 <tr><td>Branch Status:</td><td><strong>' . $sBankStatus . '</strong></td></tr>
			 <!--
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Bank Check Settings:</span></td></tr>
		     <tr bgcolor="#edeff1"><td>Check Date</td><td><strong>Top: ' . $objBankCheckSettings->iPosition_CheckDate_Top . ' px&nbsp;&nbsp;&nbsp;Left: ' . $objBankCheckSettings->iPosition_CheckDate_Left . ' px</strong></td></tr>
		     <tr><td>Customer Name</td><td><strong>Top: ' . $objBankCheckSettings->iPosition_CustomerName_Top . ' px&nbsp;&nbsp;&nbsp;Left: ' . $objBankCheckSettings->iPosition_CustomerName_Left . ' px</strong></td></tr>
		     <tr bgcolor="#edeff1"><td>Check Amount</td><td><strong>Top: ' . $objBankCheckSettings->iPosition_CheckAmount_Top . ' px&nbsp;&nbsp;&nbsp;Left: ' . $objBankCheckSettings->iPosition_CheckAmount_Left . ' px</strong></td></tr>
		     <tr><td>Amount In Words</td><td><strong>Top: ' . $objBankCheckSettings->iPosition_CheckAmountInWords_Top . ' px&nbsp;&nbsp;&nbsp;Left: ' . $objBankCheckSettings->iPosition_CheckAmountInWords_Left . ' px</strong></td></tr>
		     <tr bgcolor="#edeff1"><td>Amount In Words Line 2</td><td><strong>Top: ' . $objBankCheckSettings->iPosition_CheckAmountInWords2_Top . ' px&nbsp;&nbsp;&nbsp;Left: ' . $objBankCheckSettings->iPosition_CheckAmountInWords2_Left . ' px</strong></td></tr>
		     <tr><td>Amount In Words Break</td><td><strong>' . $objBankCheckSettings->iPosition_CheckAmountInWords_Break . ' words</strong></td></tr>
			 -->
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Bank Added On</td><td><strong>' . $sBankAddedOn . '</strong></td></tr>
			 <tr><td>Bank Added By</td><td><strong>' . $sBankAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBankId . '" /><input type="hidden" name="action" id="action" value="UpdateBank"></form></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Bank" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewBank"></form></div>';
		}
		
		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }
	
	//function AddNewBank($sBankName, $sBankAbbreviation, $sBranchName, $sBranchCode, $sBranchContactPerson, $sBranchAddress, $sBranchCity, $sBranchState, $sBranchZipCode, $sBranchCountry, $sBranchPhoneNumber, $sBranchEmailAddress, $iStatus, $sNotes)
    function AddNewBank($sBankName, $sBankAbbreviation, $iStatus, $sNotes)
	{
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
		
		$varNow = $objGeneral->fnNow();
		$iBankAddedBy = $objEmployee->iEmployeeId;
		        
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[1] == 0) // Add Disabled
			return(1010);

		$sBankName = $objDatabase->RealEscapeString($sBankName);
		$sBankAbbreviation = $objDatabase->RealEscapeString($sBankAbbreviation);
		/*
		$sBranchName = $objDatabase->RealEscapeString($sBranchName);
		$sBranchCode = $objDatabase->RealEscapeString($sBranchCode);
		$sBranchContactPerson = $objDatabase->RealEscapeString($sBranchContactPerson);
		$sBranchAddress = $objDatabase->RealEscapeString($sBranchAddress);
		$sBranchCity = $objDatabase->RealEscapeString($sBranchCity);
		$sBranchState = $objDatabase->RealEscapeString($sBranchState);
		$sBranchZipCode = $objDatabase->RealEscapeString($sBranchZipCode);
		$sBranchCountry = $objDatabase->RealEscapeString($sBranchCountry);
		$sBranchPhoneNumber = $objDatabase->RealEscapeString($sBranchPhoneNumber);
		$sBranchEmailAddress = $objDatabase->RealEscapeString($sBranchEmailAddress);		
		*/
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		$objBankCheckSettings = new clsBanking_BankCheckSettings();

		$objBankCheckSettings->iPosition_CheckAmount_Left = $objGeneral->fnGet("txtPosition_CheckAmount_Left");
		$objBankCheckSettings->iPosition_CheckAmount_Top = $objGeneral->fnGet("txtPosition_CheckAmount_Top");
		$objBankCheckSettings->iPosition_CheckDate_Left = $objGeneral->fnGet("txtPosition_CheckDate_Left");
		$objBankCheckSettings->iPosition_CheckDate_Top = $objGeneral->fnGet("txtPosition_CheckDate_Top");
		$objBankCheckSettings->iPosition_CustomerName_Left = $objGeneral->fnGet("txtPosition_CustomerName_Left");
		$objBankCheckSettings->iPosition_CustomerName_Top = $objGeneral->fnGet("txtPosition_CustomerName_Top");
		$objBankCheckSettings->iPosition_CheckAmountInWords_Left = $objGeneral->fnGet("txtPosition_CheckAmountInWords_Left");
		$objBankCheckSettings->iPosition_CheckAmountInWords_Top = $objGeneral->fnGet("txtPosition_CheckAmountInWords_Top");
		$objBankCheckSettings->iPosition_CheckAmountInWords_Break = $objGeneral->fnGet("txtPosition_CheckAmountInWords_Break");
		$objBankCheckSettings->iPosition_CheckAmountInWords2_Left = $objGeneral->fnGet("txtPosition_CheckAmountInWords2_Left");
		$objBankCheckSettings->iPosition_CheckAmountInWords2_Top = $objGeneral->fnGet("txtPosition_CheckAmountInWords2_Top");
		
		$sBankCheckSettings = serialize($objBankCheckSettings);
		/*
        $varResult = $objDatabase->Query("INSERT INTO fms_banking_banks
        (BankName, BankAbbreviation, BranchName, BranchCode, BranchContactPerson, BranchAddress, BranchCity, BranchState, BranchZipCode, BranchCountry, BranchPhoneNumber, BranchEmailAddress, Status, BankCheckSettings, Notes, BankAddedOn, BankAddedBy) 
        VALUES ('$sBankName', '$sBankAbbreviation', '$sBranchName', '$sBranchCode', '$sBranchContactPerson', '$sBranchAddress', '$sBranchCity', '$sBranchState', '$sBranchZipCode', '$sBranchCountry', '$sBranchPhoneNumber', '$sBranchEmailAddress', '$iStatus', '$sBankCheckSettings', '$sNotes', '$varNow', '$iBankAddedBy')");
		*/
        $varResult = $objDatabase->Query("INSERT INTO fms_banking_banks
        (OrganizationId, BankName, BankAbbreviation, Status, BankCheckSettings, Notes, BankAddedOn, BankAddedBy) 
        VALUES ('" . cOrganizationId . "', '$sBankName', '$sBankAbbreviation', '$iStatus', '$sBankCheckSettings', '$sNotes', '$varNow', '$iBankAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B WHERE B.OrganizationId='" . cOrganizationId . "' AND B.BankName='$sBankName' AND B.BankAddedOn = '$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
		{			
  			$objSystemLog->AddLog("Add New Bank - " . $sBankName);			
			$objGeneral->fnRedirect('?pagetype=details&error=3250&id=' . $objDatabase->Result($varResult, 0, "B.BankId"));
		}
        else
            return(3251);
    }
	
    //function UpdateBank($iBankId, $sBankName, $sBankAbbreviation, $sBranchName, $sBranchCode, $sBranchContactPerson, $sBranchAddress, $sBranchCity, $sBranchState, $sBranchZipCode, $sBranchCountry, $sBranchPhoneNumber, $sBranchEmailAddress, $iStatus, $sNotes)
    function UpdateBank($iBankId, $sBankName, $sBankAbbreviation, $iStatus, $sNotes)
	{
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[2] == 0) // Update Disabled
			return(1010);		
		
		$sBankName = $objDatabase->RealEscapeString($sBankName);
		$sBankAbbreviation = $objDatabase->RealEscapeString($sBankAbbreviation);
		/*
		$sBranchName = $objDatabase->RealEscapeString($sBranchName);
		$sBranchCode = $objDatabase->RealEscapeString($sBranchCode);
		$sBranchContactPerson = $objDatabase->RealEscapeString($sBranchContactPerson);
		$sBranchAddress = $objDatabase->RealEscapeString($sBranchAddress);
		$sBranchCity = $objDatabase->RealEscapeString($sBranchCity);
		$sBranchState = $objDatabase->RealEscapeString($sBranchState);
		$sBranchZipCode = $objDatabase->RealEscapeString($sBranchZipCode);
		$sBranchCountry = $objDatabase->RealEscapeString($sBranchCountry);
		$sBranchPhoneNumber = $objDatabase->RealEscapeString($sBranchPhoneNumber);
		$sBranchEmailAddress = $objDatabase->RealEscapeString($sBranchEmailAddress);
		*/
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		$objBankCheckSettings = new clsBanking_BankCheckSettings();
		$objBankCheckSettings->iPosition_CheckAmount_Left = $objGeneral->fnGet("txtPosition_CheckAmount_Left");
		$objBankCheckSettings->iPosition_CheckAmount_Top = $objGeneral->fnGet("txtPosition_CheckAmount_Top");
		$objBankCheckSettings->iPosition_CheckDate_Left = $objGeneral->fnGet("txtPosition_CheckDate_Left");
		$objBankCheckSettings->iPosition_CheckDate_Top = $objGeneral->fnGet("txtPosition_CheckDate_Top");
		$objBankCheckSettings->iPosition_CustomerName_Left = $objGeneral->fnGet("txtPosition_CustomerName_Left");
		$objBankCheckSettings->iPosition_CustomerName_Top = $objGeneral->fnGet("txtPosition_CustomerName_Top");
		$objBankCheckSettings->iPosition_CheckAmountInWords_Left = $objGeneral->fnGet("txtPosition_CheckAmountInWords_Left");
		$objBankCheckSettings->iPosition_CheckAmountInWords_Top = $objGeneral->fnGet("txtPosition_CheckAmountInWords_Top");
		$objBankCheckSettings->iPosition_CheckAmountInWords_Break = $objGeneral->fnGet("txtPosition_CheckAmountInWords_Break");
		$objBankCheckSettings->iPosition_CheckAmountInWords2_Left = $objGeneral->fnGet("txtPosition_CheckAmountInWords2_Left");
		$objBankCheckSettings->iPosition_CheckAmountInWords2_Top = $objGeneral->fnGet("txtPosition_CheckAmountInWords2_Top");
		
		$sBankCheckSettings = serialize($objBankCheckSettings);
		/*
        $varResult = $objDatabase->Query("
        UPDATE fms_banking_banks SET
        	BankName='$sBankName',
        	BankAbbreviation='$sBankAbbreviation',
			BranchName = '$sBranchName',
			BranchCode = '$sBranchCode',
			BranchContactPerson = '$sBranchContactPerson',
			BranchAddress = '$sBranchAddress',
			BranchCity = '$sBranchCity',
			BranchState = '$sBranchState',
			BranchZipCode = '$sBranchZipCode',
			BranchCountry = '$sBranchCountry',
			BranchPhoneNumber = '$sBranchPhoneNumber',
			BranchEmailAddress = '$sBranchEmailAddress',
			Status = '$iStatus',
        	BankCheckSettings='$sBankCheckSettings',
			Notes='$sNotes'
        WHERE BankId='$iBankId'");
		*/

        $varResult = $objDatabase->Query("
        UPDATE fms_banking_banks SET
        	BankName='$sBankName',
        	BankAbbreviation='$sBankAbbreviation',
			Status = '$iStatus',
        	BankCheckSettings='$sBankCheckSettings',
			Notes='$sNotes'
        WHERE OrganizationId='" . cOrganizationId . "' AND BankId='$iBankId'");

        if ( $varResult )
		{			
  			$objSystemLog->AddLog("Update Bank - " . $sBankName);						
			$objGeneral->fnRedirect('../banking/banks.php?pagetype=details&error=3252&id=' . $iBankId);
			
		}
        else
            return(3253);
    }

    function DeleteBank($iBankId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Banking_Banks[3] == 0) // Delete Disabled
		{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iBankId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}		
        
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B WHERE B.OrganizationId='" . cOrganizationId . "' AND B.BankId='$iBankId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Id');
		
		$sBankName = $objDatabase->Result($varResult, 0, "B.BankName");		
			
        $varResult = $objDatabase->Query("DELETE FROM fms_banking_banks WHERE BankId='$iBankId'");
        if ( $varResult )
		{			
  			$objSystemLog->AddLog("Delete Bank - " . $sBankName);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iBankId . '&error=3254');
			else
				$objGeneral->fnRedirect('?id=0&error=3254');
		}
        else
            return(3255);
    }   
	
	function ChangeBankSettings()
    {
    	// MCB
    	$aBankCheckSettings['Position_CheckDate_Top'] = 30;
    	$aBankCheckSettings['Position_CheckDate_Left'] = 444;
		$aBankCheckSettings['Position_CustomerName_Top'] = 55;
		$aBankCheckSettings['Position_CustomerName_Left'] = 65;
		$aBankCheckSettings['Position_CheckAmount_Top'] = 100;
		$aBankCheckSettings['Position_CheckAmount_Left'] = 468;
		$aBankCheckSettings['Position_CheckAmountInWords_Top'] = 80;
		$aBankCheckSettings['Position_CheckAmountInWords_Left'] = 90;
		$aBankCheckSettings['Position_CheckAmountInWords2_Top'] = 146;
		$aBankCheckSettings['Position_CheckAmountInWords2_Left'] = 10;
		$aBankCheckSettings['Position_CheckAmountInWords_Break'] = 10;		
    	$this->SaveBankCheckSettings(1, $aBankCheckSettings);
    	
    	/*
    	// UBL
    	$aBankCheckSettings['Position_CheckDate_Top'] = 83;
    	$aBankCheckSettings['Position_CheckDate_Left'] = 450;
		$aBankCheckSettings['Position_CustomerName_Top'] = 110;
		$aBankCheckSettings['Position_CustomerName_Left'] = 50;
		$aBankCheckSettings['Position_CheckAmount_Top'] = 143;
		$aBankCheckSettings['Position_CheckAmount_Left'] = 415;
		$aBankCheckSettings['Position_CheckAmountInWords_Top'] = 148;
		$aBankCheckSettings['Position_CheckAmountInWords_Left'] = 76;
		$aBankCheckSettings['Position_CheckAmountInWords2_Top'] = 178;
		$aBankCheckSettings['Position_CheckAmountInWords2_Left'] = 30;
		$aBankCheckSettings['Position_CheckAmountInWords_Break'] = 6;
    	$this->SaveBankCheckSettings(2, $aBankCheckSettings);
    	*/
    }
    
    function SaveBankCheckSettings($iBankId, $aBankCheckSettings)
    {
    	global $objDatabase;
    	global $objEncryption;
    	
    	$objBankCheckSettings = new clsBankCheckSettings();
    	
    	$objBankCheckSettings->iPosition_CheckDate_Top = $aBankCheckSettings['Position_CheckDate_Top'];
    	$objBankCheckSettings->iPosition_CheckDate_Left = $aBankCheckSettings['Position_CheckDate_Left'];
		$objBankCheckSettings->iPosition_CustomerName_Top = $aBankCheckSettings['Position_CustomerName_Top'];
		$objBankCheckSettings->iPosition_CustomerName_Left = $aBankCheckSettings['Position_CustomerName_Left'];
		$objBankCheckSettings->iPosition_CheckAmount_Top = $aBankCheckSettings['Position_CheckAmount_Top'];
		$objBankCheckSettings->iPosition_CheckAmount_Left = $aBankCheckSettings['Position_CheckAmount_Left'];
		$objBankCheckSettings->iPosition_CheckAmountInWords_Top = $aBankCheckSettings['Position_CheckAmountInWords_Top'];
		$objBankCheckSettings->iPosition_CheckAmountInWords_Left = $aBankCheckSettings['Position_CheckAmountInWords_Left'];
		
		$objBankCheckSettings->iPosition_CheckAmountInWords2_Top = $aBankCheckSettings['Position_CheckAmountInWords2_Top'];
		$objBankCheckSettings->iPosition_CheckAmountInWords2_Left = $aBankCheckSettings['Position_CheckAmountInWords2_Left'];
		
		$objBankCheckSettings->iPosition_CheckAmountInWords_Break = $aBankCheckSettings['Position_CheckAmountInWords_Break'];
		
		$sBankCheckSettings = $objEncryption->fnEncrypt(cEncryptionKey, serialize($objBankCheckSettings));
		$varResult = $objDatabase->Query("UPDATE fms_banking_banks SET BankCheckSettings='$sBankCheckSettings' WHERE BankId='$iBankId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
			return(true);
		else 
			return(false);
    }	
}

/* Bank Checks Cancelled */
class clsBanking_BankChecksCancelled
{
	function __construct()
	{
		
	}
	
    function UpdateBankCheckCancelled($iBankCheckId, $iBankCheckCancelledId, $iCancelledCheckNumber, $sNotes)
    {
        global $objDatabase;
        global $objEmployee;
        global $objGeneral;
        /*
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_BankChecksCancelled[2] == 0) // Update Disabled
			return(1010);
		*/	
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankchecks AS BC WHERE BC.BankCheckId='$iBankCheckId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Id');
        
        $iCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
        $iCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");
        
        if (($iCancelledCheckNumber < $iCheckNumberStart) || ($iCancelledCheckNumber > $iCheckNumberEnd))
        	return(5607); //$objGeneral->fnRedirect('?error=5556&id=' . $iBankCheckId . '&action2=addnew');
        	
        if ($objDatabase->DBCount("fms_banking_bankcheckscancelled AS BCC", "BCC.BankCheckId='$iBankCheckId' AND BCC.CancelledCheckNumber='$iCancelledCheckNumber'") > 0)
        	return(5600); //$objGeneral->fnRedirect('?error=5557&id=' . $iBankCheckId . '&action2=addnew');
        	
        $varResult = $objDatabase->Query("
        UPDATE fms_banking_bankcheckscancelled SET
        	CancelledCheckNumber='$iCancelledCheckNumber',
            Notes='$sNotes'
        WHERE BankCheckId='$iBankCheckId' AND BankCheckCancelledId='$iBankCheckCancelledId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        	    $objGeneral->fnRedirect('../banking/bankchecks_cancelled_details.php?error=5603&id=' . $iBankCheckId . '&bankcheckcancelledid=' . $iBankCheckCancelledId);
        //return(5552);
        else
            return(5604);
    }

    function DeleteBankCheckCancelled($iBankCheckId, $iBankCheckCancelledId)
    {
        global $objDatabase;

        global $objEmployee;
		/*
        // Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_BankChecksCancelled[3] == 0) // Delete Disabled
			return(1010);
        */
        $varResult = $objDatabase->Query("DELETE FROM fms_banking_bankcheckscancelled WHERE BankCheckId='$iBankCheckId' AND BankCheckCancelledId='$iBankCheckCancelledId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
            return(5605);
        else
            return(5606);
    }

    function AddNewBankCheckCancelled($iBankCheckId, $iCancelledCheckNumber, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
        $varNow = $objGeneral->fnNow();
        
        /*
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_BankChecksCancelled[1] == 0) // Add Disabled
			return(1010);
		*/	
		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankchecks AS BC WHERE BC.BankCheckId='$iBankCheckId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Check Id');

        $iCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
        $iCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");
        
        if (($iCancelledCheckNumber < $iCheckNumberStart) || ($iCancelledCheckNumber > $iCheckNumberEnd))
        	$objGeneral->fnRedirect('?error=5607&id=' . $iBankCheckId . '&action2=addnew');

        if ($objDatabase->DBCount("fms_banking_bankcheckscancelled AS BCC", "BCC.BankCheckId='$iBankCheckId' AND BCC.CancelledCheckNumber='$iCancelledCheckNumber'") > 0)
        	return(5600);
        /*
        if ($objDatabase->DBCount("fms_banking_bankcheckscancelled AS BCC", "BCC.BankCheckId='$iBankCheckId' AND BCC.CancelledCheckNumber='$iCancelledCheckNumber'") > 0)
        	$objGeneral->fnRedirect('?error=5557&id=' . $iBankCheckId . '&action2=addnew');
        */ 	
        $sNotes = $objDatabase->RealEscapeString($sNotes);
        
        $varResult = $objDatabase->Query("INSERT INTO fms_banking_bankcheckscancelled
        (BankCheckId, CancelledCheckNumber, Notes, BankCheckCancelledDateTime) 
        VALUES 
        ('$iBankCheckId', '$iCancelledCheckNumber', '$sNotes', '$varNow')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankcheckscancelled AS BCC WHERE BCC.BankCheckId='$iBankCheckId' AND BCC.BankCheckCancelledDateTime='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
            $objGeneral->fnRedirect('../banking/bankchecks_cancelled_details.php?error=5601&id=' . $objDatabase->Result($varResult, 0, "BCC.BankCheckId") . '&bankcheckcancelledid=' . $objDatabase->Result($varResult, 0, "BCC.BankCheckCancelledId"));
        else
            return(5602);
    }

    function ShowAllBankChecksCancelled($iBankCheckId, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
        /*
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_BankChecksCancelled[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");

        if ($sSortBy == "") $sSortBy = "BCC.BankCheckCancelledId";

		$iPagingLimit = cPagingLimit;

		$iShow = $objGeneral->fnGet("show");

		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Bank Checks Cancelled";
        if ($sSearch != "")
        {
            $sSearchCondition = " AND ((BCC.CancelledCheckNumber LIKE '%$sSearch%')) ";
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        $objAccount = new clsBanking_BankAccounts();

		$iTotalRecords = $objDatabase->DBCount("fms_banking_bankcheckscancelled AS BCC INNER JOIN fms_banking_bankchecks AS BC ON BC.BankCheckId = BCC.BankCheckId INNER JOIN fms_banking_bankaccounts AS A ON A.AccountId=BC.AccountId INNER JOIN fms_banking_banks AS B ON B.BankId = A.BankId", " BCC.BankCheckId='$iBankCheckId' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '&id=' . $iBankCheckId . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_banking_bankcheckscancelled AS BCC
		INNER JOIN fms_banking_bankchecks AS BC ON BC.BankCheckId = BCC.BankCheckId
		INNER JOIN fms_banking_bankaccounts AS A ON A.AccountId = BC.AccountId
		INNER JOIN fms_banking_banks AS B ON B.BankId = A.BankId
		INNER JOIN organization_stations AS S ON S.StationId = A.StationId
        WHERE BCC.BankCheckId='$iBankCheckId' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table bordercolor="#E6E6E6" border=1 style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr  class="GridTR">
		  <td width="15%" align="left"><span class="WhiteHeading">Bank Name / Branch&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BankId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Name in Ascending Order" title="Sort by Bank Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BankId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Name in Descending Order" title="Sort by Bank Name in Descending Order" border="0" /></a></span></td>
		  <td width="15%" align="left"><span class="WhiteHeading">Account Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=A.AccountNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Account Number in Ascending Order" title="Sort by Bank Account Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=A.AccountNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Account Number in Descending Order" title="Sort by Bank Account Number in Descending Order" border="0" /></a></span></td>
		  <td width="15%" align="left"><span class="WhiteHeading">Bank Check Book&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BC.CheckPrefix&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Prefix in Ascending Order" title="Sort by Check Prefix in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BC.CheckPrefix&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Prefix in Descending Order" title="Sort by Check Prefix in Descending Order" border="0" /></a></span></td>
		  <td width="15%" align="left"><span class="WhiteHeading">Cancelled Check No.&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BCC.CancelledCheckNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Cancelled Check Number in Ascending Order" title="Sort by Cancelled Check Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BCC.CancelledCheckNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Cancelled Check Number in Descending Order" title="Sort by Cancelled Check Number in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>
		';
		
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iBankCheckId = $objDatabase->Result($varResult, $i, "BCC.BankCheckId");
			$iBankCheckCancelledId = $objDatabase->Result($varResult, $i, "BCC.BankCheckCancelledId");
			$iCancelledCheckNumber = $objDatabase->Result($varResult, $i, "BCC.CancelledCheckNumber");
			
			$iBankId = $objDatabase->Result($varResult, $i, "B.BankId");
			
			$sBankName = $objDatabase->Result($varResult, $i, "B.BankName");
			$sBankBranchName = $objDatabase->Result($varResult, $i, "A.BranchName");
			
			$sAccountNumber = $objDatabase->Result($varResult, $i, "A.AccountNumber");
			$iAccountType = $objDatabase->Result($varResult, $i, "A.AccountType");
			$sAccountType = $objAccount->aAccountType[$iAccountType];
			
			$sCheckPrefix = $objDatabase->Result($varResult, $i, "BC.CheckPrefix");
			$sCheckNumberStart = $objDatabase->Result($varResult, $i, "BC.CheckNumberStart");
			$sCheckNumberEnd = $objDatabase->Result($varResult, $i, "BC.CheckNumberEnd");
			
			$sEditCancelledCheck = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Cancelled Bank Check\', \'../banking/bankchecks_cancelled_details.php?action2=edit&id=' . $iBankCheckId . '&bankcheckcancelledid=' . $iBankCheckCancelledId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Cancelled Bank Check Details" title="Edit Cancelled Bank Check Details"></a></td>';
			$sDeleteCancelledCheck = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Cancelled Bank Check?\')) {window.location = \'?action=DeleteBankCheckCancelled&id=' . $iBankCheckId . '&bankcheckcancelledid=' . $iBankCheckCancelledId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Cancelled Bank Check" title="Delete this Cancelled Bank Check"></a></td>';

			/*
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_BankChecksCancelled[2] == 0)
				$sEditCancelledCheck = '';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_BankChecksCancelled[3] == 0)
				$sDeleteCancelledCheck = '';
            */
			
            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $varTRBGColor . '\';" bgcolor="' . $varTRBGColor . '">
			<td align="left" valign="top">' . $sBankName . ' / ' . $sBankBranchName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			<td align="left" valign="top">' . $sAccountNumber . ' (' . $sAccountType . ')' . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			<td align="left" valign="top">' . $sCheckPrefix . $sCheckNumberStart . ' - ' . $sCheckPrefix . $sCheckNumberEnd . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			<td align="left" valign="top">' . $sCheckPrefix . $iCancelledCheckNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Cancelled Bank Check\', \'../banking/bankchecks_cancelled_details.php?id=' . $iBankCheckId . '&bankcheckcancelledid=' . $iBankCheckCancelledId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Cancelled Bank Check Details" title="View this Cancelled Bank Check Details"></a></td>
			' . $sEditCancelledCheck . '
			' . $sDeleteCancelledCheck . '</tr>';

			if ($varTRBGColor == '#edeff1')
				$varTRBGColor = '#FFFFFF';
			else
				$varTRBGColor = '#edeff1';
		}

		$sAddNewCancelledCheck = '<td width="10%" align="left"><input onclick="window.top.CreateTab(\'tabContainer\', \'Cancel a new Check\', \'../banking/bankchecks_cancelled_details.php?action2=addnew&id=' . $iBankCheckId . '\', \'520px\', true);" type="button" class="AdminFormButton1" value="Cancel a new Check"></td>';
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_BankChecksCancelled[1] == 0)
			$sAddNewCancelledCheck = '';
		*/
				
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  ' . $sAddNewCancelledCheck . '
          <form method="GET" action=""><td align="left" colspan="2">Search for a Cancelled Check:&nbsp;<input type="text" class="form1" size="15" name="p" id="p" />&nbsp;<input type="hidden" name="id" id="id"' . 'value="' . $iBankCheckId . '"/>&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Cancelled Check" title="Search for Cancelled Check" border="0"></td></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Cancelled Check Details" alt="View this Cancelled Check Details"></td><td>View this Cancelled Check Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Cancelled Check Details" alt="Edit this Cancelled Check Details"></td><td>Edit this Cancelled Check Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Cancelled Check Details" alt="Delete this Cancelled Check Details"></td><td>Delete this Cancelled Check Details</td></tr>
        </table> 
		';

		return($sReturn);
    }

    function BankCheckCancelledDetails($iBankCheckId, $iBankCheckCancelledId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
		    	
		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_banking_bankcheckscancelled AS BCC
		INNER JOIN fms_banking_bankchecks AS BC ON BC.BankCheckId = BCC.BankCheckId
		INNER JOIN fms_banking_bankaccounts AS A ON A.AccountId = BC.AccountId
		INNER JOIN organization_stations AS S ON S.StationId = A.StationId
		INNER JOIN fms_banking_banks AS B ON B.BankId = B.BankId
		WHERE BCC.BankCheckId = '$iBankCheckId' AND BCC.BankCheckCancelledId='$iBankCheckCancelledId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iBankCheckId . '&bankcheckcancelledid=' . $iBankCheckCancelledId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Cancelled Bank Check" title="Edit this Cancelled Bank Check" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Cancelled Bank Check?\')) {window.location = \'?action=DeleteBankCheckCancelled&id=' . $iBankCheckId . '&bankcheckcancelledid=' . $iBankCheckCancelledId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Cancelled Bank Check" title="Delete this Cancelled Bank Check" /></a>';
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_BankChecksCancelled[2] == 0)
			$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_BankChecksCancelled[3] == 0)
			$sButtons_Delete = '';
		*/
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Cancelled Bank Check Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#838274" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';
		
		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iBankCheckId = $objDatabase->Result($varResult, 0, "BCC.BankCheckId");
		    	$iBankCheckCancelledId = $objDatabase->Result($varResult, 0, "BCC.BankCheckCancelledId");
		    	
		    	$sCancelledCheckNumber = $objDatabase->Result($varResult, 0, "BCC.CancelledCheckNumber");
		    	$sNotes = $objDatabase->Result($varResult, 0, "BCC.Notes");
				$iStationId = $objDatabase->Result($varResult, 0, "S.StationId");
				
		    	$iBankId = $objDatabase->Result($varResult, 0, "B.BankId");
            	$sBankName = $objDatabase->Result($varResult, 0, "B.BankName");
            	//$sBankName = $sBankName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sBankName)) . '\', \'../banking/banks.php?pagetype=details&id=' . $iBankId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Bank Information" title="Bank Information" border="0" /></a><br />';
				
            	$sBankBranchName = $objDatabase->Result($varResult, 0, "A.BranchName");
            	$sBankAccountNumber = $objDatabase->Result($varResult, 0, "A.AccountNumber");
            	
            	$sCheckPrefix = $objDatabase->Result($varResult, 0, "BC.CheckPrefix");
            	$sCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
            	$sCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");
            	
            	$sStationName = $objDatabase->Result($varResult, 0, "S.StationName");
				$sStationName = $sStationName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sStationName)) . '\', \'../organization/stations_details.php?id=' . $iStationId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
								
            	$dBankCheckCancelledDateTime = $objDatabase->Result($varResult, $i, "BCC.BankCheckCancelledDateTime");
            	$sBankCheckCancelledDateTime = date("F j, Y", strtotime($dBankCheckCancelledDateTime)) . ' at ' . date("g:i a", strtotime($dBankCheckCancelledDateTime));
 		    }
		
			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
		        				
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankchecks AS BC
				INNER JOIN fms_banking_bankaccounts AS A ON A.AccountId = BC.AccountId
				INNER JOIN organization_stations AS S ON S.StationId = A.StationId
				INNER JOIN fms_banking_banks AS B ON B.BankId = B.BankId
				WHERE BC.BankCheckId = '$iBankCheckId'
				");
				if ($objDatabase->RowsNumber($varResult) <= 0) die("Sorry, Invalid Bank Check Id");
				$iStationId = $objDatabase->Result($varResult, 0, "S.StationId");
				$sStationName = $objDatabase->Result($varResult, 0, "S.StationName");
				$sStationName = $sStationName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sStationName)) . '\', \'../organization/stations_details.php?id=' . $iStationId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
				$sCheckPrefix = $objDatabase->Result($varResult, 0, "BC.CheckPrefix");
				$sCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
				$sCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");

				$sCancelledCheckNumber = '<input type="text" name="txtCancelledCheckNumber" id="txtxtCancelledCheckNumber" class="form1" value="' . $sCancelledCheckNumber . '" size="30" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';        		
			}
			else if ($sAction2 == "addnew")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sEmployeeName = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sEmployeeName = $sEmployeeName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sEmployeeName)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankchecks AS BC
				INNER JOIN fms_banking_bankaccounts AS A ON A.AccountId = BC.AccountId
				INNER JOIN organization_stations AS S ON S.StationId = A.StationId
				INNER JOIN fms_banking_banks AS B ON B.BankId = A.BankId
				WHERE BC.BankCheckId = '$iBankCheckId'
				");
				if ($objDatabase->RowsNumber($varResult) <= 0) die("Sorry, Invalid Bank Check Id");
				$iStationId = $objDatabase->Result($varResult, 0, "S.StationId");
				$sStationName = $objDatabase->Result($varResult, 0, "S.StationName");
				$sStationName = $sStationName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sStationName)) . '\', \'../organization/stations_details.php?id=' . $iStationId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
				
				$sBankName = $objDatabase->Result($varResult, 0, "B.BankName") . ' (' . $objDatabase->Result($varResult, 0, "B.BankAbbreviation") . ')';
				$sBankBranchName = $objDatabase->Result($varResult, 0, "A.BranchName");
				$sBankAccountNumber = $objDatabase->Result($varResult, 0, "A.AccountNumber");
				$sCheckPrefix = $objDatabase->Result($varResult, 0, "BC.CheckPrefix");
				$sCheckNumberStart = $objDatabase->Result($varResult, 0, "BC.CheckNumberStart");
				$sCheckNumberEnd = $objDatabase->Result($varResult, 0, "BC.CheckNumberEnd");

				$sCancelledCheckNumber = $objGeneral->fnGet("txtCancelledCheckNumber");
				$sNotes = $objGeneral->fnGet("txtNotes");

				$sCancelledCheckNumber = '<input type="text" name="txtCancelledCheckNumber" id="txtxtCancelledCheckNumber" class="form1" value="' . $sCancelledCheckNumber . '" size="5" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';        		

        		$sBankCheckCancelledDateTime = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}
		
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
		         function ValidateForm()
		         {
		         	if (!isNumeric(GetVal(\'txtCancelledCheckNumber\')))
		         		return(AlertFocus(\'Please enter a valid Check Number\', \'txtCancelledCheckNumber\'));
		         	
	         		return true;
		         }		         
		        </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Cancelled Bank Check Information:</span></td></tr>
			 <tr><td width="20%">Bank Check Id:</td><td><strong>' . $iBankCheckId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Station:</td><td><strong>' . $sStationName . '</strong></td></tr>
			 <tr><td>Bank Name:</td><td><strong>' . $sBankName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Bank Branch:</td><td><strong>' . $sBankBranchName . '</strong></td></tr>
			 <tr><td>Bank Account Number:</td><td><strong>' . $sBankAccountNumber . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Checks Range:</td><td><strong>' . $sCheckPrefix . $sCheckNumberStart . ' - ' . $sCheckPrefix . $sCheckNumberEnd . '</strong></td></tr>
			 <tr><td>Cancelled Check Number:</td><td><strong>' . $sCheckPrefix . $sCancelledCheckNumber . '</strong></td></tr>
			  <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr><td>Bank Check Cancelled On:</td><td><strong>' . $sBankCheckCancelledDateTime . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBankCheckId . '"><input type="hidden" name="bankcheckcancelledid" id="bankcheckcancelledid" value="' . $iBankCheckCancelledId . '"><input type="hidden" name="action" id="action" value="UpdateBankCheckCancelled"></form></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Cancel Check" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBankCheckId . '"><input type="hidden" name="action" id="action" value="AddNewBankCheckCancelled"></form></div>';
		}
		
		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }
	
}


// Class: WriteCheck
class clsBanking_WriteChecks
{
	// Class Constructor
	function __construct()
	{
	}

	
	function ShowAllWriteChecks($iBankCheckId, $sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;
		//die($sSearch . ' ' . 'test' . ' ' . $iBankCheckId);
		$sTitle = "Write Checks";
		if ($sSortBy == "") $sSortBy = "BWC.WriteCheckId";
		
		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((BWC.BankCheckNo LIKE '%$sSearch%') OR (BWC.CheckWrittenDateTime LIKE '%$sSearch%') OR (BWC.Amount LIKE '%$sSearch%') OR(V.VendorName LIKE '%$sSearch%') OR (V.VendorCode LIKE '%$sSearch%')) ";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}
		
		$sBankCheckCondition = "BWC.BankCheckId = '$iBankCheckId'";
		//die($sSearchCondition);
		$iTotalRecords = $objDatabase->DBCount("fms_banking_writechecks AS BWC INNER JOIN fms_banking_bankchecks AS BC ON BC.BankCheckId = BWC.BankCheckId INNER JOIN fms_vendors AS V ON V.VendorId = BWC.VendorId", "$sBankCheckCondition $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '&bankcheckid=' . $iBankCheckId . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);
		//die($iBankCheckId);
		$varResult = $objDatabase->Query("
		SELECT * FROM fms_banking_writechecks AS BWC
		INNER JOIN fms_banking_bankchecks AS BC ON BC.BankCheckId = BWC.BankCheckId
		INNER JOIN fms_vendors AS V ON V.VendorId = BWC.VendorId
		WHERE BWC.BankCheckId='$iBankCheckId' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Vendor Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Name in Ascending Order" title="Sort by Vendor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BWC.Vendor Name&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Name in Descending Order" title="Sort by Vendor Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Bank Check No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BWC.BankCheckNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Bank Check No in Ascending Order" title="Sort by Bank Check No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BWC.BankCheckNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Bank Check No in Descending Order" title="Sort by Bank Check No in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BWC.Amount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Amount in Ascending Order" title="Sort by Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BWC.Amount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Amount in Descending Order" title="Sort by Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Check Written Date/Time&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BWC.CheckWrittenDateTime&sortorder="><img src="../images/sort_up.gif" alt="Sort by Check Written Date/Time in Ascending Order" title="Sort by Check Written Date/Time in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BWC.CheckWrittenDateTime&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Check Written Date/Time in Descending Order" title="Sort by Check Written Date/Time in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';


			$iWriteCheckId = $objDatabase->Result($varResult, $i, "BWC.WriteCheckId");
			$iVendorId = $objDatabase->Result($varResult, $i, "BWC.VendorId");

			$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");
			$sVendorName = $objDatabase->RealEscapeString($sVendorName);
			$sVendorName = stripslashes($sVendorName);

			$iBankCheckId = $objDatabase->Result($varResult, $i, "BWC.BankCheckId");

			$sBankCheckNo = $objDatabase->Result($varResult, $i, "BWC.BankCheckNo");
			$sBankCheckNo = $objDatabase->RealEscapeString($sBankCheckNo);
			$sBankCheckNo = stripslashes($sBankCheckNo);
			
			$dAmount = $objDatabase->Result($varResult, $i, "BWC.Amount");
			$sAmount = number_format($dAmount, 2);
			
			$dCheckWrittenDateTime = $objDatabase->Result($varResult, $i, "BWC.CheckWrittenDateTime");
			$sCheckWrittenDateTime = date("F j, Y", strtotime($dCheckWrittenDateTime)) . ' at ' . date("g:i a", strtotime($dCheckWrittenDateTime));
			
			$sEditWriteCheck = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sBankCheckNo . '\', \'../banking/writechecks_details.php?action2=edit&writecheckid=' . $iWriteCheckId . '&bankcheckid=' . $iBankCheckId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Write Check Details" title="Edit this Write Check Details"></a></td>';
			$sDeleteWriteCheck = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Write Check?\')) {window.location = \'?action=DeleteWriteCheck&writecheckid=' . $iWriteCheckId . '&bankcheckid=' . $iBankCheckId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Write Check" title="Delete this Write Check"></a></td>';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			
			  <td align="left" valign="top">' . $sVendorName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			  <td align="left" valign="top">' . $sBankCheckNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			  <td align="left" valign="top">' . $objEmployee->aSystemSettings["CurrencySign"] . ' ' . $sAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			  <td align="left" valign="top">' . $sCheckWrittenDateTime . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sBankCheckNo . '\', \'../banking/writechecks_details.php?writecheckid=' . $iWriteCheckId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Write Check Details" title="View this Write Check Details"></a></td>
			' . $sEditWriteCheck . $sDeleteWriteCheck . '</tr>';
		}

		$sAddWriteCheck = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Write Check\', \'../banking/writechecks_details.php?action2=addnew' . '&bankcheckid=' . $iBankCheckId .'\', \'520px\', true);" type="button" class="AdminFormButton1" value="Write Check">';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddWriteCheck . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Write Check:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="hidden" name="bankcheckid" id="bankcheckid"' . 'value="' . $iBankCheckId . '"/>&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Write Check" title="Search for Write Check" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Write Check Details" alt="View this Write Check Details"></td><td>View this Write Check Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Write Check Details" alt="Edit this Write Check Details"></td><td>Edit this Write Check Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Write Check" alt="Delete this Write Check"></td><td>Delete this Write Check</td></tr>
		</table>';

		return($sReturn);
	}
	
	function WriteCheckDetails($iWriteCheckId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_banking_writechecks AS BWC
		INNER JOIN fms_vendors AS V ON V.VendorId = BWC.VendorId
		WHERE BWC.WriteCheckId = '$iWriteCheckId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&writecheckid=' . $iWriteCheckId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Write Check" title="Edit this Write Check" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Write Check?\')) {window.location = \'?action=DeleteWriteCheck&writecheckid=' . $iWriteCheckId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Write Check" title="Delete this Write Check" /></a>';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Write Check Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				
				$iWriteCheckId = $objDatabase->Result($varResult, 0, "BWC.WriteCheckId");
				$iVendorId = $objDatabase->Result($varResult, 0, "BWC.VendorId");
				$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");
				$sVendorName = $objDatabase->RealEscapeString($sVendorName);
				$sVendorName = stripslashes($sVendorName);
				$sVendorName = $sVendorName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sVendorName . '\', \'../vendors/vendors_details.php?id=' . $iVendorId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Vendor Information" title="Vendor Information" border="0" /></a><br />';
				$iBankCheckId = $objDatabase->Result($varResult, 0, "BWC.BankCheckId");
				
				$sBankCheckNo = $objDatabase->Result($varResult, 0, "BWC.BankCheckNo");
				$sBankCheckNo = $objDatabase->RealEscapeString($sBankCheckNo);
				$sBankCheckNo = stripslashes($sBankCheckNo);
				
				$dAmount = $objDatabase->Result($varResult, 0, "BWC.Amount");
				$sAmount = number_format($dAmount,2);
				
				$sDescription = $objDatabase->Result($varResult, 0, "BWC.Description");
				$sDescription = $objDatabase->RealEscapeString($sDescription);
				$sDescription = stripslashes($sDescription);
				
				$sNotes = $objDatabase->Result($varResult, 0, "BWC.Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
				
				$dCheckWrittenDateTime = $objDatabase->Result($varResult, 0, "BWC.CheckWrittenDateTime");
				$sCheckWrittenDateTime = date("F j, Y", strtotime($dCheckWrittenDateTime)) . ' at ' . date("g:i a", strtotime($dCheckWrittenDateTime));
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sVendorName = '<select name="selVendor" id="selVendor" class="form1">';
				$varResult = $objDatabase->Query('SELECT * FROM fms_vendors AS V');
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
				{
					$sVendorName .='<option ' . (($iVendorId == $objDatabase->Result($varResult, $i, "V.VendorId")) ? ' selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult, $i, "V.VendorName") . '</option>';
				}
				$sVendorName .='</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selVendor\'), \'../vendors/vendors_details.php?id=\'+GetSelectedListBox(\'selVendor\'), \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" border="0" align="absmiddle" alt="Vendor Details" title="Vendor Details" /></a>';
				
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $sAmount . '" size="12" />&nbsp;<span style="color:red;">*</span>';
				$sBankCheckNo = '<input type="text" name="txtBankCheckNo" id="txtBankCheckNo" class="form1" value="' . $sBankCheckNo . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = '<textarea name="txtDescription" id="txtDescription" class="form1" rows="5" cols="60">' . $sDescription . '</textarea>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$iWriteCheckId = "";
				$sBankCheckNo = $objGeneral->fnGet("txtBankCheckNo");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sVendorName = '<select name="selVendor" id="selVendor" class="form1">';
				$varResult = $objDatabase->Query('SELECT * FROM fms_vendors AS V');
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
				{
					$sVendorName .='<option value="' . $objDatabase->Result($varResult, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult, $i, "V.VendorName") . '</option>';
				}
				$sVendorName .='</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selVendor\'), \'../vendors/vendors_details.php?id=\'+GetSelectedListBox(\'selVendor\'), \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" border="0" align="absmiddle" alt="Vendor Details" title="Vendor Details" /></a>';
				
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $sAmount . '" size="12" />&nbsp;<span style="color:red;">*</span>';
				$sBankCheckNo = '<input type="text" name="txtBankCheckNo" id="txtBankCheckNo" class="form1" value="' . $sBankCheckNo . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = '<textarea name="txtDescription" id="txtDescription" class="form1" rows="5" cols="60">' . $sDescription . '</textarea>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sCheckWrittenDateTime = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
				function ValidateForm()
				{
					if (GetVal(\'txtBankCheckNo\') == "") return(AlertFocus(\'Please enter a valid Bank Check No\', \'txtBankCheckNo\'));
					if (!isNumeric(GetVal(\'txtAmount\'))) return(AlertFocus(\'Please enter a valid Amount\', \'txtAmount\'));					
					
					return true;
				}
				</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			<tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Write Check Information:</span></td></tr>			
			  <tr bgcolor="#edeff1"><td valign="top" width="25%">Write Check Id:</td><td><strong>' . $iWriteCheckId . ' (Automatically Assigned)</strong></td></tr>
			  <tr bgcolor="#ffffff"><td valign="top">Vendor:</td><td><strong>' . $sVendorName . '</strong></td></tr>
			  <tr bgcolor="#edeff1"><td valign="top">Bank Check No:</td><td><strong>' . $sBankCheckNo . '</strong></td></tr>
			  <tr bgcolor="#ffffff"><td valign="top">Amount:</td><td><strong>' . $objEmployee->aSystemSettings['CurrencySign'] . $sAmount . '</strong></td></tr>
			  <tr bgcolor="#edeff1"><td valign="top">Description:</td><td><strong>' . $sDescription . '</strong></td></tr>
			  <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr> 
			  <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			  <tr bgcolor="#ffffff"><td valign="top">Check Written Date/Time:</td><td><strong>' . $dCheckWrittenDateTime . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iWriteCheckId . '"><input type="hidden" name="action" id="action" value="UpdateWriteCheck"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add New" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewWriteCheck"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewWriteCheck($iVendorId, $iBankCheckId, $sBankCheckNo, $dAmount, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
				
		$varNow = $objGeneral->fnNow();
		$iEmployeeId = $objEmployee->iEmployeeId;
	
		$sBankCheckNo = $objDatabase->RealEscapeString($sBankCheckNo);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_banking_bankcheckscancelled AS BCC ", "BCC.BankCheckId = '$iBankCheckId' AND BCC.CancelledCheckNumber = '$sBankCheckNo'") > 0) return(5708);
		if($objDatabase->DBCount("fms_banking_bankchecks AS BC ", "BC.BankCheckId = '$iBankCheckId' AND BC.CheckNumberStart <= '$sBankCheckNo' AND BC.CheckNumberEnd >= '$sBankCheckNo'") <= 0) return(5707);
		if($objDatabase->DBCount("fms_banking_writechecks AS BWC ", "BWC.BankCheckId = '$iBankCheckId' AND BWC.BankCheckNo = '$sBankCheckNo'") > 0) return(5700);
			
		$varResult = $objDatabase->Query("INSERT INTO fms_banking_writechecks
		(VendorId, BankCheckId, BankCheckNo, Amount, Description, Notes, CheckWrittenDateTime)
		VALUES ('$iVendorId', '$iBankCheckId', '$sBankCheckNo', '$dAmount', '$sDescription', '$sNotes', '$varNow')");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_writechecks AS BWC WHERE BWC.BankCheckNo='$sBankCheckNo' AND BWC.CheckWrittenDateTime='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$objSystemLog->AddLog("Add New Write Check - " . $sWriteCheckName);
				$objGeneral->fnRedirect('?error=5701&writecheckid=' . $objDatabase->Result($varResult, 0, "BWC.WriteCheckId"));
			}
		}

		return(5702);
	}
	
	function UpdateWriteCheck($iWriteCheckId, $iBankCheckId, $iVendorId, $sBankCheckNo, $dAmount, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		$sBankCheckNo = $objDatabase->RealEscapeString($sBankCheckNo);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_banking_bankcheckscancelled AS BCC ", "BCC.BankCheckId = '$iBankCheckId' AND BCC.CancelledCheckNumber = '$sBankCheckNo'") > 0) return(5708);
		if($objDatabase->DBCount("fms_banking_bankchecks AS BC ", "BC.BankCheckId = '$iBankCheckId' AND BC.CheckNumberStart <= '$sBankCheckNo' AND BC.CheckNumberEnd >= '$sBankCheckNo'") <= 0) return(5707);
		if($objDatabase->DBCount("fms_banking_writechecks AS BWC ", "BWC.BankCheckId = '$iBankCheckId' AND BWC.BankCheckNo = '$sBankCheckNo' AND BWC.WriteCheckId <> '$iWriteCheckId'") > 0) return(5700);
		
		$varResult = $objDatabase->Query("
		UPDATE fms_banking_writechecks SET	
			VendorId='$iVendorId', 
			BankCheckNo='$sBankCheckNo', 
			Amount='$dAmount', 
			Description='$sDescription', 
			Notes='$sNotes'			
		WHERE WriteCheckId='$iWriteCheckId'");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Update Write Check - " . $sBankCheckNo);
			$objGeneral->fnRedirect("../banking/writechecks_details.php?error=5703&writecheckid=" . $iWriteCheckId);
			//return(3002);
		}
		else
			return(5704);
	}
	
	function DeleteWriteCheck($iWriteCheckId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;

		$varResult = $objDatabase->Query("SELECT * FROM fms_banking_writechecks AS BWC WHERE BWC.WriteCheckId='$iWriteCheckId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(5709);

		$sBankCheckNo = $objDatabase->Result($varResult, 0, "BWC.BankCheckNo");
		$sBankCheckNo = $objDatabase->RealEscapeString($sBankCheckNo);

		$varResult = $objDatabase->Query("DELETE FROM fms_banking_writechecks WHERE WriteCheckId='$iWriteCheckId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{	
			$objSystemLog->AddLog("Delete Write Check - " . $sBankCheckNo);

			return(5705);
		}
		else
			return(5706);
	}

}


class clsBanking_BankCheckSettings
{
	public $iPosition_CheckDate_Top;
	public $iPosition_CheckDate_Left;
	
	public $iPosition_CustomerName_Top;
	public $iPosition_CustomerName_Left;
	
	public $iPosition_CheckAmount_Top;
	public $iPosition_CheckAmount_Left;
	
	public $iPosition_CheckAmountInWords_Top;
	public $iPosition_CheckAmountInWords_Left;

	public $iPosition_CheckAmountInWords2_Top;
	public $iPosition_CheckAmountInWords2_Left;
	
	public $iPosition_CheckAmountInWords_Break;		// Break on 6 words
		
}

?>