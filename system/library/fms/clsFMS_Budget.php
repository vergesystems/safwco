<?php

include('../../system/library/fms/clsFMS_Budget_Budget.php');

class clsFMS_Budget
{
    // Constructor
    function __construct()
    {
    }
    
    function ShowBudgetMenu()
    {
    	global $objEmployee;
    	
    	$sCurrentFile = $_SERVER['SCRIPT_NAME'];
    	$aCurrentFile = explode('/', $sCurrentFile);
    	$sCurrentFile = $aCurrentFile[count($aCurrentFile)-1];
    	
    	$sBudgets = '<a ' . (($sCurrentFile == "budget.php") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../accounts/budget.php"><img src="../images/budget/iconBudget.gif" alt="Budget" title="Budget" border="0" /><br />Budget</a>';
		$sBudgetAllocation = '<a ' . (($sCurrentFile == "allocation.php") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../accounts/allocation.php"><img src="../images/budget/iconBudgetAllocation.gif" alt="Budget Allocation" title="Budget Allocation" border="0" /><br />Budget Allocation</a>';
 
    	$sReturn .= '
    	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
    	 <tr>
    	  <td valign="bottom">

           <table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
	    	<tr>
	    	 <td align="center" width="10%" valign="top">' . $sBudgets . '</td>
			 <td align="center" width="10%" valign="top">' . $sBudgetAllocation . '</td>
	      	</tr>
	       </table>
    	  </td></tr></table>';
    	
    	return($sReturn);    	
    }
}

?>