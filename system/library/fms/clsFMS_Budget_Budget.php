<?php

// Class: clsFMS_Budget_Budgets
class clsFMS_Budget_Budget
{
	public $aStatus;
	// Class Constructor
	function __construct()
	{
		$this->aStatus = array("On Going", "Completed");
	}

	
	function ShowAllBudgets($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		
		$sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        if ($sSortBy == "") $sSortBy = "B.BudgetId DESC";
		
		$iPagingLimit = cPagingLimit;

		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Budgets";
		if ($sSortBy == "") $sSortBy = "B.BudgetId";

		if ($sSearch != "")
		{
			$sSearch = mysql_escape_string($sSearch);
			$sSearchCondition = " AND ((B.Title LIKE '%$sSearch%') OR (B.Description LIKE '%$sSearch%') OR (B.Notes LIKE '%$sSearch%') OR (E.FirstName) OR (E.LastName)) ";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_budget AS B INNER JOIN organization_employees AS E ON E.EmployeeId = B.BudgetAddedBy", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_budget AS B
		INNER JOIN organization_employees AS E ON E.EmployeeId = B.BudgetAddedBy
		WHERE 1=1 $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Budget Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BudgetId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Budget Id in Ascending Order" title="Sort by Budget Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BudgetId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Budget Id in Descending Order" title="Sort by Budget Id in Descending Order" border="0" /></a></span></td>		  
		  <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.Amount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Amount in Ascending Order" title="Sort by Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.Amount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Amount in Descending Order" title="Sort by Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">From&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BudgetStartDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Budget Start Date in Ascending Order" title="Sort by Budget Start Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BudgetStartDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Budget Start Date in Descending Order" title="Sort by Budget Start Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">To&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BudgetEndDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Budget End Date in Ascending Order" title="Sort by Budget End Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BudgetEndDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Budget End Date in Descending Order" title="Sort by Budget End Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>		  
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iBudgetId = $objDatabase->Result($varResult, $i, "B.BudgetId");
			$sTitle = $objDatabase->Result($varResult, $i, "B.Title");
			
			$dAmount = $objDatabase->Result($varResult, $i, "B.Amount");
			$sAmount = number_format($dAmount, 2);
			
			$dBudgetStartDate = $objDatabase->Result($varResult, $i, "B.BudgetStartDate");
			$sBudgetStartDate = date("F j, Y", strtotime($dBudgetStartDate));
			
			$dBudgetEndDate = $objDatabase->Result($varResult, $i, "B.BudgetEndDate");
			$sBudgetEndDate = date("F j, Y", strtotime($dBudgetEndDate));
			
			$iStatus = $objDatabase->Result($varResult, $i, "B.Status");
			$sStatus = $this->aStatus[$iStatus];
			
			$iBudgetAddedBy = $objDatabase->Result($varResult, $i, "B.BudgetAddedBy");
			$sBudgetAddedBy = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
			
			$dBudgetAddedOn = $objDatabase->Result($varResult, $i, "B.BudgetAddedOn");
			$sBudgetAddedOn = date("F j, Y", $dBudgetAddedOn);
			
			//$sEditBudget = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $sTitle . '\', \'../accounts/budgets_details.php?action2=edit&id=' . $iBudgetId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Details" title="Edit this Budget Details"></a></td>';
			$sDecreaseBudgetAmount = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Decrease ' . $sTitle . '\', \'../accounts/budget_details.php?action2=decrease&id=' . $iBudgetId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Decrease this Budget Amount" title="Decrease this Budget Amount"></a></td>';
			$sIncreaseBudgetAmount = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Increase ' . $sTitle . '\', \'../accounts/budget_details.php?action2=increase&id=' . $iBudgetId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Increase this Budget Amount" title="Increase this Budget Amount"></a></td>';
			//$sEditBudget = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $sTitle . '\', \'../accounts/budgets_details.php?action2=edit&id=' . $iBudgetId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Details" title="Edit this Budget Details"></a></td>';
			$sDeleteBudget = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Budget?\')) {window.location = \'?action=DeleteBudget&id=' . $iBudgetId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget" title="Delete this Budget"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[3] == 0) // Delete Disabled
				$sDeleteBudget = '';
			
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			
			 <td align="left" valign="top">' . $iBudgetId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>			 
			 <td align="left" valign="top">' . $sTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sBudgetStartDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sBudgetEndDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string($sTitle) . '\', \'../accounts/budget_details.php?id=' . $iBudgetId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Budget Details" title="View this Budget Details"></a></td>
			'. $sDecreaseBudgetAmount . $sIncreaseBudgetAmount . $sDeleteBudget . '</tr>';
			//' . $sEditBudget . $sDeleteBudget . '</tr>';
		}

		$sAddBudget = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Budget\', \'../accounts/budget_details.php?action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Budget">';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_Budgets[1] == 0) // Add Disabled
			$sAddBudget = '';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddBudget . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Budget:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Budget" title="Search for a Budget" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Budget Details" alt="View this Budget Details"></td><td>View this Budget Details</td></tr>
		 <!--
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Budget Details" alt="Edit this Budget Details"></td><td>Edit this Budget Details</td></tr>
		 -->
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Decrease this Budget Amount" alt="Decrease this Budget Amount"></td><td>Decrease this Budget Amount</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Increase this Budget Amount" alt="Increase this Budget Amount"></td><td>Increase this Budget Amount</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Budget" alt="Delete this Budget"></td><td>Delete this Budget</td></tr>
		</table>';

		return($sReturn);
	}
	
	function BudgetDetails($iBudgetId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_accounts_budget AS B
		LEFT JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId = B.DonorProjectId
		INNER JOIN organization_employees AS E ON E.EmployeeId = B.BudgetAddedBy
		WHERE B.BudgetId = '$iBudgetId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		//$sButtons_Edit = '<a href="?action2=edit&id=' . $iBudgetId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget" title="Edit this Budget" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Budget?\')) {window.location = \'?action=DeleteBudget&id=' . $iBudgetId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget" title="Delete this Budget" /></a>';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Budget Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				
				$iBudgetId = $objDatabase->Result($varResult, 0, "B.BudgetId");
				
				$iBudgetAddedBy = $objDatabase->Result($varResult, 0, "B.BudgetAddedBy");
				$sBudgetAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sBudgetAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(stripslashes($sBudgetAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iBudgetAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sTitle = $objDatabase->Result($varResult, 0, "B.Title");
				$iDonorProjectId = $objDatabase->Result($varResult, 0, "B.DonorProjectId");
				if($iDonorProjectId > 0)
				{
					$sProjectCode = $objDatabase->Result($varResult, 0, "DP.ProjectCode");
					$sProjectTitle = $objDatabase->Result($varResult, 0, "DP.ProjectTitle");
					$sDonorProject = $sProjectCode . ' - ' . $sProjectTitle;
					$sDonorProject = $sDonorProject . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' .  mysql_escape_string(str_replace('"', '', $sProjectTitle)) .'\' , \'../accounts/donorprojects_details.php?id=' . $iDonorProjectId . '\' , \'520px\', true);"><img align="absmiddle" src="../images/icons/iconContents.gif" border="0" alt="Project Details" title="Project Details" /></a>';
				}			
				
				$dAmount = $objDatabase->Result($varResult, 0, "B.Amount");
				$sAmount = number_format($dAmount, 2);
				
				$dRemainingAmount = $objDatabase->Result($varResult, 0, "B.RemainingAmount");
				$sRemainingAmount = number_format($dRemainingAmount, 2);
				
				$dRemainingAllocatedAmount = $objDatabase->Result($varResult, 0, "B.RemainingAllocatedAmount");
				$sRemainingAllocatedAmount = number_format($dRemainingAllocatedAmount, 2);
								
				$dBudgetStartDate = $objDatabase->Result($varResult, 0, "B.BudgetStartDate");
				$sBudgetStartDate = date("F j, Y", strtotime($dBudgetStartDate));
				
				$dBudgetEndDate = $objDatabase->Result($varResult, 0, "B.BudgetEndDate");
				$sBudgetEndDate = date("F j, Y", strtotime($dBudgetEndDate));
				
				$iStatus = $objDatabase->Result($varResult, 0, "B.Status");
				$sStatus = $this->aStatus[$iStatus];
				
				$sDescription = $objDatabase->Result($varResult, 0, "B.Description");
				$sNotes = $objDatabase->Result($varResult, 0, "B.Notes");
				
				$dBudgetAddedOn = $objDatabase->Result($varResult, 0, "B.BudgetAddedOn");
				$sBudgetAddedOn = date("F j, Y", strtotime($dBudgetAddedOn)) . ' at ' . date("g:i a", strtotime($dBudgetAddedOn));
				
				//Budget History
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_budgethistory AS BH WHERE BH.BudgetId = '$iBudgetId'");
				$sBudgetHistory = 
				'<tr class="Details_Title_TR">
				  <td colspan="4"><span class="Details_Title">Budget History</span></td>
				 </tr>
				 <tr>
				  <td colspan="4">
				   <table border="0" cellpadding="5" cellspacing="2">
				    <tr>
					 <strong><td><strong>Title</strong></td><td><strong>Amount</strong</td><td><strong>Description</strong</td></strong></tr>';
						   
				if($objDatabase->RowsNumber($varResult2) > 0)
				{
					for($j = 0; $j < $objDatabase->RowsNumber($varResult2); $j++)
					{
						$sTitle2 = $objDatabase->Result($varResult2, $j, "BH.Title");
						$sDescription2 = $objDatabase->Result($varResult2, $j, "BH.Description");
						$dAmount2 = $objDatabase->Result($varResult2, $j, "BH.Amount");
						$sAmount2 = number_format($dAmount2, 2);
						$sBudgetHistory .= '
						<tr>
						 <td>' . $sTitle2 . '</td><td>' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount2 . '</td>
						 <td>' . $sDescription2 . '</td></tr>
					     ';
					}
					$sBudgetHistory .= '</table></td></tr>';
				}
				
				//die($sBudgetHistory);
			}

			if ($sAction2 == "decrease")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->FCKEditor("txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			if ($sAction2 == "increase")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->FCKEditor("txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{	
				$iBudgetId = "";
				$sTitle = $objGeneral->fnGet("txtTitle");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$sAmount = $objGeneral->fnGet("txtAmount");
				$sBudgetStartDate = date('Y-m-d');
				$sBudgetEndDate = date('Y-m-d');

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sBudgetAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . mysql_escape_string(stripslashes($objEmployee->sEmployeeFirstName)) . ' ' . mysql_escape_string(stripslashes($objEmployee->sEmployeeLastName)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sDonorProject = '<select name="selDonorProject" id="selDonorProject" class="form1">
				<option value="0">Select Donor Project</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP ORDER BY DP.ProjectTitle");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)				
					$sDonorProject .= '<option ' . (($iDonorProjectId == $objDatabase->Result($varResult, $i, "DP.DonorProjectId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . '">' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . ' - ' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '</option>';
				$sDonorProject .='</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selDonorProject\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selDonorProject\'), \'../accounts/donorprojects_details.php?id=\'+GetSelectedListBox(\'selDonorProject\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Project Details" title="Project Details" border="0" /></a>';
								
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';
				for($i = 0; $i < count($this->aStatus); $i++)
					$sStatus .= '<option value = "' . $i . '">' . $this->aStatus[$i] . '</option>';
				$sStatus .= '</select>';
				
				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $sAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sBudgetStartDate = '<input type="text" name="txtBudgetStartDate" id="txtBudgetStartDate" class="form1" value="' . $sBudgetStartDate . '" size="10" />' . $objDHTMLSuite->Calendar('txtBudgetStartDate', true) . '&nbsp;<span style="color:red;">*</span>';
				$sBudgetEndDate = '<input type="text" name="txtBudgetEndDate" id="txtBudgetEndDate" class="form1" value="' . $sBudgetEndDate . '" size="10" />' . $objDHTMLSuite->Calendar('txtBudgetEndDate', true) . '&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->FCKEditor("txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sBudgetAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}
			
			
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if (GetVal(\'txtTitle\') == "") return(AlertFocus(\'Please enter a valid Title\', \'txtTitle\'));
				//if (!isDate(GetVal(\'txtBudgetStartDate\'))) return(AlertFocus(\'Please enter a valid Budget Start Date\', \'txtBudgetStartDate\'));
				//if (!isDate(GetVal(\'txtBudgetEndDate\'))) return(AlertFocus(\'Please enter a valid Budget End Date\', \'txtBudgetEndDate\'));
				if (!isNumeric(GetVal(\'txtAmount\'))) return(AlertFocus(\'Please enter a valid Amount\', \'txtAmount\'));
					
				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Budget Information:</span></td></tr>			
			 <tr bgcolor="#edeff1"><td valign="top" width="20%">Budget Id:</td><td><strong>' . $iBudgetId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Donor Project:</td><td><strong>' . $sDonorProject . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Title:</td><td><strong>' . $sTitle . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Budget Start Date (From):</td><td><strong>' . $sBudgetStartDate . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Budget End Date (To):</td><td><strong>' . $sBudgetEndDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Amount:</td><td><strong>' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Remaining Amount:</td><td><strong>' . $objEmployee->aSystemSettings["CurrencySign"] . $sRemainingAmount . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Remaining Allocated Amount:</td><td><strong>' . $objEmployee->aSystemSettings["CurrencySign"] . $sRemainingAllocatedAmount . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="4">Description:</td></tr>
			 <tr><td colspan="4"><strong>' . $sDescription . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Status:</td><td><strong>' . $sStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Budget Added By:</td><td><strong>' . $sBudgetAddedBy . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Added On:</td><td><strong>' . $sBudgetAddedOn . '</strong></td></tr>
			 ' . $sBudgetHistory . '
			</table>';

			//if ($sAction2 == "edit")
			//	$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Budget" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBudgetId . '"><input type="hidden" name="action" id="action" value="UpdateBudget"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Budget" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewBudget"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "decrease")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Decrease Budget" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="decrease"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "increase")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Increase Budget" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="increase"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewBudget($iDonorProjectId, $sTitle, $dAmount, $dBudgetStartDate, $dBudgetEndDate, $iStatus, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		$varNow = $objGeneral->fnNow();
		$iEmployeeId = $objEmployee->iEmployeeId;
	
		$sTitle = mysql_escape_string($sTitle);
		$sDescription = mysql_escape_string($sDescription);
		$sNotes = mysql_escape_string($sNotes);

		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_budget
		(DonorProjectId, Title, Amount, RemainingAmount, RemainingAllocatedAmount, BudgetStartDate, BudgetEndDate, Status, Description, Notes, BudgetAddedBy, BudgetAddedOn)
		VALUES ('$iDonorProjectId', '$sTitle', '$dAmount', '$dAmount', '$dAmount', '$dBudgetStartDate', '$dBudgetEndDate', '$iStatus', '$sDescription', '$sNotes', '$iEmployeeId', '$varNow')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.Title='$sTitle' AND B.BudgetAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iBudgetId = $objDatabase->Result($varResult, 0, "B.BudgetId");
				
				$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgethistory
				(BudgetId, Title, Amount, Description, Notes, BudgetHistoryAddedBy, BudgetHistoryAddedOn)
				VALUES ('$iBudgetId', '$sTitle', '$dAmount', '$sDescription', '$sNotes', '$iEmployeeId', '$varNow')");
				
				include('../../system/library/clsOrganization_SystemLog.php');
				$objSystemLog = new clsSystemLog();
				$objSystemLog->AddLog("Add New Budget - " . $sBudgetName);

				$objGeneral->fnRedirect('?error=13000&id=' . $objDatabase->Result($varResult, 0, "B.BudgetId"));
			}
		}

		return(13001);
	}
	
	function IncreaseOrDecreaseBudget($iBudgetId, $sTitle, $dAmount, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		
		if($objEmployee->objEmployeeRoles->iEmployeeRole_FMS_Accounts_Budgets_Increase[0] == 0)
			return(1010);
			
		if($objDatabase->DBCount("fms_accounts_budget AS B", " B.BudgetId = '$iBudgetId'") <= 0)
			return(13007);
		
		$sTitle = mysql_escape_string($sTitle);
		$sDescription = mysql_escape_string($sDescription);
		$sNotes = mysql_escape_string($sNotes);
		$iEmployeeId = $objEmployee->iEmployeeId;
		$varNow = $objGeneral->fnNow();
		
		if($objGeneral->fnGet("action") == 'decrease')
		{	
			$varResult = $objDatabase->Query("SELECT B.Amount, B.RemainingAmount, B.RemainingAllocatedAmount FROM fms_accounts_budget AS B WHERE B.BudgetId = '$iBudgetId'");
			$dBudgetAmount = $objDatabase->Result($varResult, 0, "B.Amount");
			$dRemainingAmount = $objDatabase->Result($varResult, 0, "B.RemainingAmount");
			$dRemainingAllocatedAmount = $objDatabase->Result($varResult, 0, "B.RemainingAllocatedAmount");
			// If Remaining Amount is less than Descreasing Amount than System should not allow to decrease
			//if($dRemainingAmount < $dAmount) return(13006);
			if($dRemainingAllocatedAmount < $dAmount) return(13006);
			
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budget
			SET	
				Title = '$sTitle', 
				Amount = Amount - '$dAmount',
				RemainingAmount = RemainingAmount - '$dAmount',
				Description = '$sDescription',
				Notes = '$sNotes'
			WHERE BudgetId = '$iBudgetId'");
				
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgethistory
			(BudgetId, Title, Amount, BudgetHistoryAddedOn, BudgetHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetId', '$sTitle', '$dAmount', '$varNow', '$iEmployeeId', '$sDescription', '$sNotes')");
		}
		if($objGeneral->fnGet("action") == 'increase')	
		{
			$varResult3 = $objDatabase->Query("
			UPDATE fms_accounts_budget 
			SET	
				Title='$sTitle', 
				Amount= Amount + '$dAmount',
				RemainingAmount = RemainingAmount + '$dAmount',
				RemainingAllocatedAmount = RemainingAllocatedAmount + '$dAmount',
				Description = '$sDescription',
				Notes = '$sNotes'
			WHERE BudgetId='$iBudgetId'");
				
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgethistory
			(BudgetId, Title, Amount, BudgetHistoryAddedOn, BudgetHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetId', '$sTitle', '$dAmount', '$varNow', '$iEmployeeId', '$sDescription', '$sNotes')");
		}
		if ($objDatabase->AffectedRows($varResult2) > 0)
		{
			include('../../system/library/clsOrganization_SystemLog.php');
			$objSystemLog = new clsSystemLog();
			$objSystemLog->AddLog("Update Budget - " . $sTitle);
			$objGeneral->fnRedirect('../accounts/budget_details.php?id='. $iBudgetId . ' &error=13002');
			//return(13002);
		}
		else
			return(13003);
	}
	
	function DeleteBudget($iBudgetId)
	{
		global $objDatabase;
		global $objEmployee;

		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.BudgetId = '$iBudgetId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(13005);

		$sTitle = $objDatabase->Result($varResult, 0, "B.Title");
		
		// Check Budgets usage in General Journal
		if ($objDatabase->DBCount("fms_accounts_generaljournalAS GJ", "GJ.BudgetId= '$iBudgetId'") > 0) return(13008);

		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_budget WHERE BudgetId = '$iBudgetId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			include('../../system/library/clsOrganization_SystemLog.php');
			$objSystemLog = new clsSystemLog();
			$objSystemLog->AddLog("Delete Budget - " . $sTitle);

			return(13004);
		}
		else
			return(13005);
	}

}

?>