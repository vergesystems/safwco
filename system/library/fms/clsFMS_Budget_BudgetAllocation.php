<?php

// Class: clsFMS_Budget_BudgetAllocation
class clsFMS_Budget_BudgetAllocation
{	
	// Class Constructor
	function __construct()
	{		
	}

	
	function ShowAllBudgetAllocation($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		
		$sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        if ($sSortBy == "") $sSortBy = "BA.BudgetAllocationId DESC";
		
		$iPagingLimit = cPagingLimit;

		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Budget Allocation";
		if ($sSortBy == "") $sSortBy = "BA.BudgetAllocationId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((BA.Title LIKE '%$sSearch%') OR (BA.Description LIKE '%$sSearch%') OR (BA.Notes LIKE '%$sSearch%') OR (E.FirstName) OR (E.LastName)) ";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_accounts_budgetallocation AS BA INNER JOIN fms_accounts_budget AS B ON B.BudgetId = BA.BudgetId INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = BA.ChartOfAccountId INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BudgetAllocationAddedBy", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_accounts_budgetallocation AS BA
		INNER JOIN fms_accounts_budget AS B ON B.BudgetId = BA.BudgetId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = BA.ChartOfAccountId
		INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BudgetAllocationAddedBy
		WHERE 1=1 $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.BudgetId&sortorder="><img src="../images/sort_up.gif" alt="Sort by BudgetId in Ascending Order" title="Sort by BudgetId in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.BudgetId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by BudgetId in Descending Order" title="Sort by BudgetId in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Budget&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=B.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Budget in Ascending Order" title="Sort by Budget in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=B.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Budget in Descending Order" title="Sort by Budget in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Chart of Account&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CA.AccountTitle&sortorder="><img src="../images/sort_up.gif" alt="Sort by Account Title in Ascending Order" title="Sort by Account Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CA.AccountTitle&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Account Title in Descending Order" title="Sort by Account Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=BA.Amount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Amount in Ascending Order" title="Sort by Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=BA.Amount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Amount in Descending Order" title="Sort by Amount in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iBudgetAllocationId = $objDatabase->Result($varResult, $i, "BA.BudgetAllocationId");
			$sBudgetTitle = $objDatabase->Result($varResult, $i, "B.Title");
			$sTitle = $objDatabase->Result($varResult, $i, "BA.Title");
			$sChartOfAccount = $objDatabase->Result($varResult, $i, "CA.AccountTitle");			
			$dAmount = $objDatabase->Result($varResult, $i, "BA.Amount");
			$sAmount = number_format($dAmount, 2);
												
			$sDecreaseBudgetAmount = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Decrease ' . $sTitle . '\', \'../accounts/allocation_details.php?action2=decrease&id=' . $iBudgetAllocationId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Decrease this Allocation Amount" title="Decrease this Allocation Amount"></a></td>';
			$sIncreaseBudgetAmount = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Increase ' . $sTitle . '\', \'../accounts/allocation_details.php?action2=increase&id=' . $iBudgetAllocationId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Increase this Allocation Amount" title="Increase this Allocation Amount"></a></td>';
			$sEditBudget = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $sTitle . '\', \'../accounts/allocation_details.php?action2=edit&id=' . $iBudgetAllocationId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Details" title="Edit this Budget Details"></a></td>';
			//$sDeleteBudgetAllocation = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Budget Allocation?\')) {window.location = \'?action=DeleteBudgetAllocation&id=' . $iBudgetAllocationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget Allocation" title="Delete this Budget Allocation"></a></td>';

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			
			 <td align="left" valign="top">' . $iBudgetAllocationId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sBudgetTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sChartOfAccount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sTitle) . '\', \'../accounts/allocation_details.php?id=' . $iBudgetAllocationId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Budget Allocation Details" title="View this Budget Allocation Details"></a></td>
			'. $sEditBudget . $sDecreaseBudgetAmount . $sIncreaseBudgetAmount . '</tr>';
			//' . $sEditBudget . $sDeleteBudget . '</tr>';
		}

		$sAddBudgetAllocation = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Allocate Budget\', \'../accounts/allocation_details.php?action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Allocate Budget">';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddBudgetAllocation . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Budget Allocation:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Budget Allocation" title="Search for a Budget Allocation" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Budget Allocation Details" alt="View this Budget Allocation Details"></td><td>View this Budget Allocation Details</td></tr>
		 <!--
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Budget Details" alt="Edit this Budget Details"></td><td>Edit this Budget Details</td></tr>
		 -->
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Decrease this Budget Allocation Amount" alt="Decrease this Budget Allocation Amount"></td><td>Decrease this Budget Allocation Amount</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Increase this Budget Allocation Amount" alt="Increase this Budget Allocation Amount"></td><td>Increase this Budget Allocation Amount</td></tr>		 
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Budget Allocation" alt="Delete this Budget Allocation"></td><td>Delete this Budget Allocation</td></tr>
		</table>';

		return($sReturn);
	}
	
	function BudgetAllocationDetails($iBudgetAllocationId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *,BA.Notes AS Notes,BA.Description AS Description
		FROM fms_accounts_budgetallocation AS BA
		INNER JOIN fms_accounts_budget AS B ON B.BudgetId = BA.BudgetId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = BA.ChartOfAccountId
		INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BudgetAllocationAddedBy		
		WHERE BA.BudgetAllocationId = '$iBudgetAllocationId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		//$sButtons_Edit = '<a href="?action2=edit&id=' . $iBudgetId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget" title="Edit this Budget" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Budget Allocation?\')) {window.location = \'?action=DeleteBudgetAllocation&id=' . $iBudgetAllocationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget" title="Delete this Budget" /></a>';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Budget Allocation Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iBudgetId = $objDatabase->Result($varResult, 0, "B.BudgetId");
				$iBudgetAllocationId = $objDatabase->Result($varResult, 0, "BA.BudgetAllocationId");
				$iChartOfAccountId = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsId");
								
				$iBudgetAllocationAddedBy = $objDatabase->Result($varResult, 0, "BA.BudgetAllocationAddedBy");
				$sBudgetAllocationAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sBudgetAllocationAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sBudgetAllocationAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iBudgetAllocationAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sBudgetTitle = $objDatabase->Result($varResult, 0, "B.Title");
				$sBudgetTitle .='&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sBudgetTitle)) . '\', \'../accounts/budget_details.php?id=' . $iBudgetId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Budget Information" title="Budget Information" border="0" /></a>';
				
				$sBudgetAllocationTitle = $objDatabase->Result($varResult, 0, "BA.Title");
				$sChartOfAccount = $objDatabase->Result($varResult, 0, "CA.AccountTitle");
				$sChartOfAccount .='&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sChartOfAccount)) . '\', \'../accounts/chartofaccounts_details.php?id=' . $iChartOfAccountId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Information" title="Chart of Account Information" border="0" /></a>';
				
				$dAmount = $objDatabase->Result($varResult, 0, "BA.Amount");
				$sAmount = number_format($dAmount, 2);
				
				$dRemainingAmount = $objDatabase->Result($varResult, 0, "BA.RemainingAmount");
				$sRemainingAmount = number_format($dRemainingAmount, 2);
				
				$sDescription = $objDatabase->Result($varResult, 0, "Description");
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
				
				$dBudgetAllocationAddedOn = $objDatabase->Result($varResult, 0, "BA.BudgetAllocationAddedOn");
				$sBudgetAllocationAddedOn = date("F j, Y", strtotime($dBudgetAllocationAddedOn)) . ' at ' . date("g:i a", strtotime($dBudgetAllocationAddedOn));
				
				//Budget Allocation History
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_budgetallocationhistory AS BAH WHERE BAH.BudgetAllocationId = '$iBudgetAllocationId'");
				$sBudgetAllocationHistory = 
				'<tr class="Details_Title_TR">
				  <td colspan="3"><span class="Details_Title">Budget Allocation History</span></td>
				 </tr>
				 <tr>
				  <td colspan="10">
				   <table border="0" bordercolor="#E6E6E6" border="1" style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;"  cellpadding="3" cellspacing="0" width="100%">
				    <tr class="Details_Title_TR">
					 <td align="left"><span class="WhiteHeading">Title</span></td>
					 <td align="left"><span class="WhiteHeading">Amount</span></td>
					<td align="left"><span class="WhiteHeading">Description</span></td>
					</tr>';
						   
				if($objDatabase->RowsNumber($varResult2) > 0)
				{
					for($j = 0; $j < $objDatabase->RowsNumber($varResult2); $j++)
					{
						$sTitle2 = $objDatabase->Result($varResult2, $j, "BAH.Title");
						$sDescription2 = $objDatabase->Result($varResult2, $j, "BAH.Description");
						$dAmount2 = $objDatabase->Result($varResult2, $j, "BAH.Amount");
						$sAmount2 = number_format($dAmount2, 2);
					
						$sBudgetAllocationHistory .= '
						<tr>
						 <td>' . $sTitle2 . '</td>
						 <td>' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount2 . '</td>
						 <td>' . $sDescription2 . '</td>
						</tr>
					     ';
					}
					$sBudgetAllocationHistory .= '</table></td></tr>';
				}
				
				//die($sBudgetHistory);
			}

			if ($sAction2 == "decrease")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sBudgetAllocationTitle = '<input type="text" name="txtBudgetAllocationTitle" id="txtBudgetAllocationTitle" class="form1" value="' . $sBudgetAllocationTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $sAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->FCKEditor("txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';							
			}
			if ($sAction2 == "increase")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sBudgetAllocationTitle = '<input type="text" name="txtBudgetAllocationTitle" id="txtBudgetAllocationTitle" class="form1" value="' . $sBudgetAllocationTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $sAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->FCKEditor("txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';								
			}
			else if ($sAction2 == "edit")
			{					
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';				
				$sChartOfAccount = '<select name="selChartOfAccount" id="selChartOfAccount" class="form1">';				
				$sChartOfAccount .= '<option value="0">Please Select Chart Of Account</option>';
				$varResultX = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.ChartOfAccountsCategoryId = '1' OR CA.ChartOfAccountsCategoryId = '4' ORDER BY CA.AccountTitle");
				if($objDatabase->RowsNumber($varResultX) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResultX); $i++)
					{
						$sChartOfAccount .= '<option ' . (($iChartOfAccountId == $objDatabase->Result($varResultX, $i, "CA.ChartOfAccountsId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResultX, $i, "CA.ChartOfAccountsId") . '">' . $objDatabase->Result($varResultX, $i, "CA.AccountTitle") . '</option>';
					}
				}
				$sChartOfAccount .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selChartOfAccount\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccount\'), \'../accounts/chartofaccounts_details.php?id=\'+GetSelectedListBox(\'selChartOfAccount\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Details" title="Chart of Account Details" border="0" /></a>';
				//End Chart of Accounts
				
				//Budget
				$sBudgetTitle = '<select name="selBudget" id="selBudget" class="form1">';
				$sBudgetTitle .= '<option value="0">Please Select Budget </option>';
				$varResultX = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B ORDER BY B.Title");
				if($objDatabase->RowsNumber($varResultX) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResultX); $i++)
					{
						$sBudgetTitle .= '<option ' . (($iBudgetId == $objDatabase->Result($varResultX, $i, "B.BudgetId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResultX, $i, "B.BudgetId") . '">' . $objDatabase->Result($varResultX, $i, "B.Title") . '</option>';
					}
				}
				$sBudgetTitle .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selBudget\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selBudget\'), \'../accounts/budget_details.php?id=\'+GetSelectedListBox(\'selBudget\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Budget Details" title="Budget Details" border="0" /></a>';
				//Budget
				
				$sBudgetAllocationTitle = '<input type="text" name="txtBudgetAllocationTitle" id="txtBudgetAllocationTitle" class="form1" value="' . $sBudgetAllocationTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				//$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->FCKEditor("txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';				
			}
			else if ($sAction2 == "addnew")
			{	
				$iBudgetAllocationId = "";
				$sBudgetAllocationTitle = $objGeneral->fnGet("txtBudgetAllocationTitle");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$dAmount = $objGeneral->fnGet("txtAmount");
				$dRemainingAmount = $objGeneral->fnGet("txtRemainingAmount");
				$iBudgetId = $objGeneral->fnGet("selBudget");
				$iChartOfAccountId = $objGeneral->fnGet("selChartOfAccount");
				
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sBudgetAllocationAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeFirstName)) . ' ' . $objDatabase->RealEscapeString(stripslashes($objEmployee->sEmployeeLastName)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				//Chart of Accounts
				// Only display Assets & Expenses, ChartOfAccountsCategoryId 1= Assets, 4= Expenses
				$sChartOfAccount = '<select name="selChartOfAccount" id="selChartOfAccount" class="form1">';
				$sChartOfAccount .= '<option value="0">Please Select Chart Of Account</option>';
				$varResultX = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.ChartOfAccountsCategoryId = '1' OR CA.ChartOfAccountsCategoryId = '4' ORDER BY CA.AccountTitle");
				if($objDatabase->RowsNumber($varResultX) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResultX); $i++)
					{
						$sChartOfAccount .= '<option' . (($iChartOfAccountId == $objDatabase->Result($varResultX, $i, "CA.ChartOfAccountsId")) ? 'selected="true"' : '')  . ' value="' . $objDatabase->Result($varResultX, $i, "CA.ChartOfAccountsId") . '">' . $objDatabase->Result($varResultX, $i, "CA.AccountTitle") . '</option>';
					}
				}
				$sChartOfAccount .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selChartOfAccount\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selChartOfAccount\'), \'../accounts/chartofaccounts_details.php?id=\'+GetSelectedListBox(\'selChartOfAccount\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Chart of Account Details" title="Chart of Account Details" border="0" /></a>';
				//End Chart of Accounts
				
				//Budget
				$sBudgetTitle = '<select name="selBudget" id="selBudget" class="form1">';
				$sBudgetTitle .= '<option value="0">Please Select Budget </option>';
				$varResultX = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B ORDER BY B.Title");
				if($objDatabase->RowsNumber($varResultX) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResultX); $i++)
					{
						$sBudgetTitle .= '<option' . (($iBudgetId == $objDatabase->Result($varResultX, $i, "B.BudgetId")) ? 'selected="true"' : '')  . ' value="' . $objDatabase->Result($varResultX, $i, "B.BudgetId") . '">' . $objDatabase->Result($varResultX, $i, "B.Title") . '</option>';
					}
				}
				$sBudgetTitle .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selBudget\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selBudget\'), \'../accounts/budget_details.php?id=\'+GetSelectedListBox(\'selBudget\'), \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="Budget Details" title="Budget Details" border="0" /></a>';
				//Budget
				
				$sBudgetAllocationTitle = '<input type="text" name="txtBudgetAllocationTitle" id="txtBudgetAllocationTitle" class="form1" value="' . $sBudgetAllocationTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAmount = '<input type="text" name="txtAmount" id="txtAmount" class="form1" value="' . $dAmount . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->FCKEditor("txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sBudgetAllocationAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}
						
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if(GetSelectedListBox(\'selBudget\') == "0") return(AlertFocus(\'Please select a Budget\', \'selBudget\'));
				if(GetSelectedListBox(\'selChartOfAccount\') == "0") return(AlertFocus(\'Please select a Chart of Account\', \'selChartOfAccount\'));				
				if (GetVal(\'txtBudgetAllocationTitle\') == "") return(AlertFocus(\'Please enter a valid Budget Allocation Title\', \'txtBudgetAllocationTitle\'));
				//if (!isDate(GetVal(\'txtBudgetStartDate\'))) return(AlertFocus(\'Please enter a valid Budget Start Date\', \'txtBudgetStartDate\'));
				//if (!isDate(GetVal(\'txtBudgetEndDate\'))) return(AlertFocus(\'Please enter a valid Budget End Date\', \'txtBudgetEndDate\'));
				if (!isNumeric(GetVal(\'txtAmount\'))) return(AlertFocus(\'Please enter a valid Amount\', \'txtAmount\'));				
				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Budget Allocation Information:</span></td></tr>			
			 <tr bgcolor="#edeff1"><td valign="top" width="20%">Budget Allocation Id:</td><td><strong>' . $iBudgetAllocationId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Budget Title:</td><td><strong>' . $sBudgetTitle . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Chart of Accounts:</td><td><strong>' . $sChartOfAccount . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Budget Allocation Title:</td><td><strong>' . $sBudgetAllocationTitle . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Amount:</td><td><strong>' . $objEmployee->aSystemSettings["CurrencySign"] . $sAmount . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Remaining Amount:</td><td><strong>' . $objEmployee->aSystemSettings["CurrencySign"] . $sRemainingAmount . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="3">Description:</td></tr>
			 <tr><td colspan="4"><strong>' . $sDescription . '</strong></td></tr>
			 ' . $sBudgetAllocationHistory . '
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Budget Added By:</td><td><strong>' . $sBudgetAllocationAddedBy . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Added On:</td><td><strong>' . $sBudgetAllocationAddedOn . '</strong></td></tr>			 
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iBudgetAllocationId . '"><input type="hidden" name="action" id="action" value="UpdateBudgetAllocation"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Allocate" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewBudgetAllocation"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "decrease")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Decrease" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="decrease"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "increase")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Increase" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="increase"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewBudgetAllocation($iBudgetId, $iChartOfAccountId, $sBudgetAllocationTitle, $dAmount, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		$varNow = $objGeneral->fnNow();
		$iEmployeeId = $objEmployee->iEmployeeId;
		//die($iBudgetId . ' - ' . $iChartOfAccountId . ' - ' . $sBudgetAllocationTitle . ' - ' . $dAmount . ' - ' . $sDescription . ' - ' . $sNotes);
		$sBudgetAllocationTitle = $objDatabase->RealEscapeString($sBudgetAllocationTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_accounts_budget AS B", " B.BudgetId = '$iBudgetId'") <= 0)
			return(13057);
		
		$varResult = $objDatabase->Query("SELECT B.RemainingAllocatedAmount FROM fms_accounts_budget AS B WHERE B.BudgetId = '$iBudgetId'");
		$dBudgetAmount = $objDatabase->Result($varResult, 0, "B.RemainingAllocatedAmount");
		$varResult = $objDatabase->Query("SELECT SUM(BA.Amount) AS 'AllocatedAmount' FROM fms_accounts_budgetallocation AS BA WHERE BA.BudgetId = '$iBudgetId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$dAllocatedAmount = $objDatabase->Result($varResult, 0, "AllocatedAmount");
		}
		//$dExpectedAllocatedAmount = $dAllocatedAmount + $dAmount;
		$dExpectedAllocatedAmount = $dAmount;
		if($dExpectedAllocatedAmount > $dBudgetAmount) return(13056);
		
		$varResult = $objDatabase->Query("INSERT INTO fms_accounts_budgetallocation
		(BudgetId, ChartOfAccountId, Title, Amount, RemainingAmount, Description, Notes, BudgetAllocationAddedBy, BudgetAllocationAddedOn)
		VALUES ('$iBudgetId', '$iChartOfAccountId', '$sBudgetAllocationTitle', '$dAmount', '$dAmount', '$sDescription', '$sNotes', '$iEmployeeId', '$varNow')");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budget
			SET
				RemainingAllocatedAmount = RemainingAllocatedAmount - '$dAmount'
			WHERE BudgetId = '$iBudgetId'
			");
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budgetallocation AS BA WHERE BA.Title='$sBudgetAllocationTitle' AND BA.BudgetAllocationAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iBudgetAllocationId = $objDatabase->Result($varResult, 0, "BA.BudgetAllocationId");
				
				$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgetallocationhistory
				(BudgetAllocationId, Title, Amount, Description, Notes, BudgetAllocationHistoryAddedBy, BudgetAllocationHistoryAddedOn)
				VALUES ('$iBudgetAllocationId', '$sBudgetAllocationTitle', '$dAmount', '$sDescription', '$sNotes', '$iEmployeeId', '$varNow')");
				
				include('../../system/library/clsOrganization_SystemLog.php');
				$objSystemLog = new clsSystemLog();
				$objSystemLog->AddLog("Add New Budget - " . $sBudgetAllocationTitle);

				$objGeneral->fnRedirect('?error=13050&id=' . $iBudgetAllocationId);
			}
		}

		return(13051);
	}
	
	// Update Budget Allocation
	function UpdateNewBudgetAllocation($iBudgetAllocationId, $iBudgetId, $iChartOfAccountId, $sBudgetAllocationTitle, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		$sBudgetAllocationTitle = $objDatabase->RealEscapeString($sBudgetAllocationTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_accounts_budget AS B", " B.BudgetId = '$iBudgetId'") <= 0)
			return(13057);
		
		$varResult = $objDatabase->Query("SELECT B.RemainingAllocatedAmount FROM fms_accounts_budget AS B WHERE B.BudgetId = '$iBudgetId'");
		$dBudgetAmount = $objDatabase->Result($varResult, 0, "B.RemainingAllocatedAmount");
		$varResult = $objDatabase->Query("SELECT SUM(BA.Amount) AS 'AllocatedAmount' FROM fms_accounts_budgetallocation AS BA WHERE BA.BudgetId = '$iBudgetId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$dAllocatedAmount = $objDatabase->Result($varResult, 0, "AllocatedAmount");
		}		
		
		$varResult = $objDatabase->Query("
		UPDATE fms_accounts_budgetallocation 
		SET						
			BudgetId='$iBudgetId',			
			ChartOfAccountId='$iChartOfAccountId',			
			Description='$sDescription', 
			Notes='$sNotes'
		WHERE BudgetAllocationId='$iBudgetAllocationId'");
		if ($varResult)
		{
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budget
			SET
				RemainingAllocatedAmount = RemainingAllocatedAmount - '$dAmount'
			WHERE BudgetId = '$iBudgetId'
			");
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budgetallocation AS BA WHERE BA.Title='$sBudgetAllocationTitle' AND BA.BudgetAllocationAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iBudgetAllocationId = $objDatabase->Result($varResult, 0, "BA.BudgetAllocationId");
				
				$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgetallocationhistory
				(BudgetAllocationId, Title, Amount, Description, Notes, BudgetAllocationHistoryAddedBy, BudgetAllocationHistoryAddedOn)
				VALUES ('$iBudgetAllocationId', '$sBudgetAllocationTitle', '$dAmount', '$sDescription', '$sNotes', '$iEmployeeId', '$varNow')");
				
				include('../../system/library/clsOrganization_SystemLog.php');
				$objSystemLog = new clsSystemLog();
				$objSystemLog->AddLog("Add New Budget - " . $sBudgetAllocationTitle);

				$objGeneral->fnRedirect('?error=13050&id=' . $iBudgetAllocationId);
			}
		}

		return(13051);
	}
	
	function IncreaseOrDecreaseBudgetAllocation($iBudgetAllocationId, $dAmount, $sBudgetAllocationTitle)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
				
		$sBudgetAllocationTitle = $objDatabase->RealEscapeString($sBudgetAllocationTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		$iEmployeeId = $objEmployee->iEmployeeId;
		$varNow = $objGeneral->fnNow();
		
		$varResult = $objDatabase->Query("
		SELECT B.BudgetId, B.RemainingAllocatedAmount, BA.RemainingAmount 
		FROM fms_accounts_budgetallocation AS BA
		INNER JOIN fms_accounts_budget AS B ON B.BudgetId = BA.BudgetId
		WHERE BA.BudgetAllocationId = '$iBudgetAllocationId'");		
		$iBudgetId = $objDatabase->Result($varResult, 0, "B.BudgetId");
		$dRemainingAmount = $objDatabase->Result($varResult, 0, "BA.RemainingAmount");
		$dRemainingAllocatedAmount = $objDatabase->Result($varResult, 0, "B.RemainingAllocatedAmount");
		
		if($objGeneral->fnGet("action") == 'decrease')
		{	
			if($dRemainingAmount < $dAmount) return(13058);
			
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budget
			SET	
				RemainingAllocatedAmount= RemainingAllocatedAmount + '$dAmount'				
			WHERE BudgetId='$iBudgetId'");
			
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budgetallocation
			SET	
				Title='$sBudgetAllocationTitle', 
				Amount=Amount - '$dAmount',
				RemainingAmount = RemainingAmount - '$dAmount',
				Description='$sDescription',
				Notes='$sNotes'
			WHERE 
				BudgetAllocationId='$iBudgetAllocationId'");
				
			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_budgetallocationhistory
			(BudgetAllocationId, Title, Amount, BudgetAllocationHistoryAddedOn, BudgetAllocationHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetAllocationId', '$sBudgetAllocationTitle', '$dAmount', '$varNow', '$iEmployeeId', '$sDescription', '$sNotes')");
		}		
		if($objGeneral->fnGet("action") == 'increase')	
		{
			//if($dRemainingAmount < $dAmount) return(13056);
			if($dRemainingAllocatedAmount < $dAmount) return(13058);
			
			$varResult = $objDatabase->Query("
			UPDATE fms_accounts_budget
			SET	
				RemainingAllocatedAmount= RemainingAllocatedAmount - '$dAmount'				
			WHERE BudgetId='$iBudgetId'");
						
			$varResult3 = $objDatabase->Query("
			UPDATE fms_accounts_budgetallocation
			SET	
				Title='$sBudgetAllocationTitle', 
				Amount= Amount + '$dAmount',
				RemainingAmount= RemainingAmount + '$dAmount',
				Description='$sDescription',
				Notes='$sNotes'
			WHERE 
				BudgetAllocationId='$iBudgetAllocationId'");
				
			$varResult2 = $objDatabase->Query("
			INSERT INTO fms_accounts_budgetallocationhistory(BudgetAllocationId, Title, Amount, BudgetAllocationHistoryAddedOn, BudgetAllocationHistoryAddedBy, Description, Notes)			
			VALUES('$iBudgetAllocationId', '$sBudgetAllocationTitle', '$dAmount', '$varNow', '$iEmployeeId', '$sDescription', '$sNotes')");
		}
		if ($varResult)
		{
			include('../../system/library/clsOrganization_SystemLog.php');
			$objSystemLog = new clsSystemLog();
			$objSystemLog->AddLog("Update Budget - " . $sTitle);
			$objGeneral->fnRedirect('../accounts/allocation_details.php?id='. $iBudgetAllocationId . ' &error=13052');
			//return(13002);
		}
		else
			return(13053);
	}
	
	function DeleteBudgetAllocation($iBudgetAllocationId)
	{
		global $objDatabase;
		global $objEmployee;

		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budgetallocation AS BA WHERE BA.BudgetAllocationId = '$iBudgetAllocationId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(13057);

		$sBudgetAllocationTitle = $objDatabase->Result($varResult, 0, "BA.Title");

		$varResult = $objDatabase->Query("DELETE FROM fms_accounts_budgetallocation WHERE BudgetAllocationId = '$iBudgetAllocationId'");
		if ($varResult)
		{
			include('../../system/library/clsOrganization_SystemLog.php');
			$objSystemLog = new clsSystemLog();
			$objSystemLog->AddLog("Delete Budget Allocation - " . $sBudgetAllocationTitle);

			return(13054);
		}
		else
			return(13055);
	}

}
?>