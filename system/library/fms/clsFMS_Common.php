<?php

class clsFMS_Common
{			
	/* Types */
	function ShowTypes($sComponentName, $sAction = "", $sTypeId = 0, $sNewTypeName = "")
	{
		global $objDatabase;
		
		switch ($sComponentName)
		{
			case "Organization_StationTypes": 
				$iNumberOfItems = $objDatabase->DBCount("organization_stations AS S", "S.StationType='$sTypeId'");
				$sTitle = "Station Type";
				break;
			case "Vendors_VendorTypes": 
				$iNumberOfItems = $objDatabase->DBCount("fms_vendors AS V", "V.VendorType='$sTypeId'");
				$sTitle = "Vendor Type";
				break;				
		}
		

		if ($sAction == "AddType")
		{
			$varResult = $objDatabase->Query("INSERT INTO fms_common_types (ComponentName, TypeName) VALUES ('" . $sComponentName . "', '" . $sNewTypeName . "')");
			$sMessage = "New Type has been added successfully...";
		}
		else if ($sAction == "EditType")
		{
			$varResult = $objDatabase->Query("UPDATE fms_common_types SET TypeName='$sNewTypeName' WHERE ComponentName='$sComponentName' AND TypeId='$sTypeId'");
			$sMessage = "Type has updated added successfully...";
		}
		else if ($sAction == "DeleteType")
		{
			if ($iNumberOfItems <= 0)
			{
				$varResult = $objDatabase->Query("DELETE FROM fms_common_types WHERE ComponentName='$sComponentName' AND TypeId='$sTypeId'");
				$sMessage = "Type has been deleted successfully...";
			}
			else
				$sMessage = "Sorry, Cannot delete this Type, it contains one or more items...";
		}
		
		if ($sMessage != "")
			$sMessage = '<div align="center" style="font-family:Tahoma, Arial; font-size:12px; color:blue;"><br />' . $sMessage . '</div>';
		
		$sReturn = $sMessage . '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td><span style="font-family:Tahoma, Arial; font-size:22px;">' . $sTitle . 's</span></td></tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr class="GridTR">
		 <td class="GridTD" width="5%" align="center" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">S#</td>
		 <td class="GridTD" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Type Name</td>
		 <td class="GridTD" width="2%" colspan="2" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Operations</td>
		</tr>';
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_common_types AS T WHERE T.ComponentName='$sComponentName'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iTypeId = $objDatabase->Result($varResult, $i, "T.TypeId");
			$sTypeName = $objDatabase->Result($varResult, $i, "T.TypeName");

			$sEdit = '<td class="GridTD" align="center"><a href="#noanchor" onclick="ShowDiv(\'divEditType\');SetVal(\'hdnTypeNumber\', \'' . $iTypeId . '\');SetVal(\'txtEditTypeName\', \'' . $sTypeName . '\');"><img src="../images/icons/iconEdit.gif" border="0" title="Edit" alt="Edit" /></a></td>';
			$sDelete = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to Delete this ' . $sTitle . '?\')) {window.top.MOOdalBox.open(\'../common/types.php?componentname=' . $sComponentName . '&action=DeleteType&id=' . $iTypeId . '\', \'' . $sTitle . '\', \'700 420\');}"><img src="../images/icons/iconDelete.gif" border="0" title="Delete" alt="Delete" /></a></td>';
			
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			$sReturn .= '<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="center" style="font-size:11px; font-family:Tahoma, Arial;">' . ($i+1) . '</td>
			 <td class="GridTD">' . $sTypeName . '</td>
			 ' . $sEdit . '
			 ' . $sDelete . '
			</tr>';
		}
		
		$sReturn .= '</table>
		<br />';
		
		$sReturn .= '
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td><a href="#noanchor" onclick="ShowHideDiv(\'divAddNewType\');" style="font-family:Arial; font-size:14px; font-weight:bold; color:green;"><img src="../images/icons/plus.gif" align="absmiddle" border="0" />&nbsp;Add New ' . $sTitle . '</a></td></tr>
		</table>
		<div id="divAddNewType" style="display:none;" align="center">
		 <br />
		 <form id="frmAddNewType" method="post" action="" onsubmit="window.top.MOOdalBox.open(\'../common/types.php?componentname=' . $sComponentName . '&action=AddType&txtTypeName=\'+GetVal(\'txtTypeName\'), \'' . $sTitle . 's\', \'700 420\');return false;">
		 <table border="0" class="GridTD" cellspacing="0" cellpadding="5" width="80%" align="center">
		  <tr style="background-color:#cdcdcd;"><td style="font-family: Tahoma, Arial; font-size:18px; color:#000000;">Add New ' . $sTitle . '</td><td align="right"><a href="#noanchor" onclick="HideDiv(\'divAddNewType\');">X</a></td></tr>
		  <tr>
		   <td colspan="2">
		    <table border="0" cellspacing="0" cellpadding="5" width="95%" align="center">
			 <tr><td align="right"><span class="Tahoma16">' . $sTitle . ':</span></td><td><input type="text" name="txtTypeName" id="txtTypeName" class="Tahoma16" style="width:200px;" /></td></tr>
			 <tr><td colspan="2" align="center"><br /><br />
			 <div align="center"><input type="submit" class="AdminFormButton1" value="Add ' . $sTitle . '" /></div>
			 </td></tr>
			</table>
		   </td>
		  </tr>
		 </table>
		 <br />
		</form>
		</div>
		<div id="divEditType" style="display:none;" align="center">
		 <br />
		 <form id="frmEditType" method="post" action="" onsubmit="window.top.MOOdalBox.open(\'../common/types.php?componentname=' . $sComponentName . '&action=EditType&id=\'+GetVal(\'hdnTypeNumber\')+\'&txtTypeName=\'+GetVal(\'txtEditTypeName\'), \'' . $sTitle . '\', \'700 420\');return false;">
		 <table border="0" class="GridTD" cellspacing="0" cellpadding="5" width="80%" align="center">
		  <tr style="background-color:#cdcdcd;"><td style="font-family: Tahoma, Arial; font-size:18px; color:#000000;">Edit ' . $sTitle . '</td><td align="right"><a href="#noanchor" onclick="HideDiv(\'divEditType\');">X</a></td></tr>
		  <tr>
		   <td colspan="2">
			<table border="0" cellspacing="0" cellpadding="5" width="95%" align="center">
			 <tr><td align="right"><span class="Tahoma16">' . $sTitle . ':</span></td><td><input type="text" name="txtEditTypeName" id="txtEditTypeName" value="" class="Tahoma16" style="width:200px;" /></td></tr>
			 <tr><td colspan="2" align="center"><br /><br />
			 <div align="center"><input type="submit" class="AdminFormButton1" value="Update" /></div>
			 </td></tr>
			</table>
		   </td>
		  </tr>
		 </table>
		 <br />
		<input type="hidden" name="hdnTypeNumber" id="hdnTypeNumber" value="" />
		</form>
		</div>';
		
		return($sReturn);
	}
}

?>