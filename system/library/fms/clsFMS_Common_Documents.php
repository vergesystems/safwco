<?php

class clsFMS_Common_Documents
{
	public $sComponentName;
	public $sDocumentsPath;
	public $aRoles;
	
	// Constructor
	function __construct()
	{
		$this->sDocumentsPath = cDataFolder . '/vf/documents/';
	}
	
	function SetRoles($sComponentName)
	{
		global $objEmployee;
		$this->sComponentName = $sComponentName;
		
		// Verify
		if ($sComponentName == "Accounts_GeneralJournal")
			$this->aRoles = $objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournal_Documents;
		else if($sComponentName == "Customers_Quotations")
			$this->aRoles = $objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations_Documents;
		else if($sComponentName == "Customers_SalesOrders")
			$this->aRoles = $objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders_Documents;
		else if($sComponentName == "Customers_Invoices")
			$this->aRoles = $objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices_Documents;	
		else if($sComponentName == "Customers_Receipts")
			$this->aRoles = $objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts_Documents;
		else if($sComponentName == "Vendors_PurchaseRequests")
			$this->aRoles = $objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests_Documents;
		else if($sComponentName == "Vendors_Quotations")
			$this->aRoles = $objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations_Documents;
		else if($sComponentName == "Vendors_PurchaseOrders")
			$this->aRoles = $objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders_Documents;			
						
	}

	function ShowDocuments($sComponentName, $iComponentId)
	{
		global $objDatabase;
		global $objEmployee;

		$this->SetRoles($sComponentName);

		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td colspan="5"><span style="font-family:Tahoma, Arial; font-size:22px;">Documents</span></td></tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr class="GridTR">
		 <td width="5%" align="center" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">S#</td>
		 <td width="25%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Date / Time</td>
		 <td width="20%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Employee</td>
		 <td width="20%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Title</td>
		 <td width="20%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">File Name</td>
		 <td width="1%" colspan="2" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Operations</td>
		</tr>';

		$iCounter = 1;
		$varResult = $objDatabase->Query("SELECT *
		FROM fms_common_documents AS D
		INNER JOIN organization_employees AS E ON E.EmployeeId = D.DocumentAddedBy
		WHERE D.ComponentName='$sComponentName' AND D.ComponentId='$iComponentId'
		ORDER BY D.DocumentId DESC");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			for ($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$sEmployeeName = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
				$sTitle = $objDatabase->Result($varResult, $i, "D.Title");
				$sFileName = $objDatabase->Result($varResult, $i, "D.FileName");
				$iDocumentId = $objDatabase->Result($varResult, $i, "D.DocumentId");
				$iAccessLevel = $objDatabase->Result($varResult, $i, "D.AccessLevel");
				$iDocumentAddedBy = $objDatabase->Result($varResult, $i, "D.DocumentAddedBy");
				$dDocumentDateTime = $objDatabase->Result($varResult, $i, "D.DocumentAddedOn");
				$sDocumentDateTime = date("F j, Y", strtotime($dDocumentDateTime)) . ' at ' . date("g:i a", strtotime($dDocumentDateTime));

				// Private Files
				if (($iAccessLevel == 1) && ($iDocumentAddedBy != $objEmployee->iEmployeeId))
					continue;
					
				$sDeleteDocument = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to Delete this Document?\')) {window.top.MOOdalBox.open(\'../common/documents_show.php?action=DeleteDocument&componentname=' . $sComponentName . '&id=' . $iComponentId . '&documentid=' . $iDocumentId . '\', \'Documents of ' .  str_replace("'", "\'", $sTitle) . '\', \'700 420\');}"><img src="../images/icons/iconDelete.gif" border="0" title="Delete Document" alt="Delete Document" /></a></td>';
				
				if ($iDocumentAddedBy != $objEmployee->iEmployeeId)
					$sDeleteDocument = '<td>&nbsp;</td>';
				
				$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
				$sReturn .= '<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
				 <td align="center" style="font-size:11px; font-family:Tahoma, Arial;">' . ($iCounter) . '</td>
				 <td>' . $sDocumentDateTime . '</td>
				 <td>' . $sEmployeeName . '</td>
				 <td>' . $sTitle . '</td>
				 <td>' . $sFileName . '</td>
				 <td align="center"><a href="../common/documents_download.php?componentname=' . $sComponentName . '&id=' . $iComponentId . '&documentid=' . $iDocumentId . '"><img src="../images/icons/save.gif" border="0" title="Download Document" alt="Download Document" /></a></td>
				 ' . $sDeleteDocument . '
				</tr>';
				
				$iCounter++;
			}
		}

		$sReturn .= '</table>
		<br />
		&nbsp;&nbsp;<img src="../images/icons/plus.gif" align="absmiddle" alt="Add New Document" title="Add New Document" border="0" /><a href="#noanchor" style="font-size:14px; font-weight:bold; font-family:Arial, Tahoma; color:green;" onclick="ShowHideDiv(\'divAddNewDocument\');">&nbsp;Add New Document</a>&nbsp;&nbsp;
		<div id="divAddNewDocument" style="display:none;" align="center">
		 <br />
		 <form id="frmAddDocument" method="post" action="" onsubmit="return VF_Common_Documents_AddNewDocument(\'' . $sComponentName . '\', ' . $iComponentId . ', GetVal(\'txtTitle\'), GetCheckedValue(document.forms[\'frmAddDocument\'].elements[\'radAccessLevel\']), window.frmUpload.sFileName);">
		 <table border="1" bordercolor="#cdcdcd" cellspacing="0" cellpadding="5" width="80%" align="center">
		  <tr>
		   <td>
		    <table border="0" cellspacing="0" cellpadding="5" width="95%" align="center">
			 <tr><td align="right" style="width:100px;" class="Tahoma16">Employee:</td><td class="Tahoma16Bold">' . $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '</td></tr>
			 <tr><td align="right" class="Tahoma16">Date / Time:</td><td class="Tahoma16Bold">' . date("F j, Y g:i a") . '</td></tr>
			 <tr><td align="right"><span class="Tahoma16">Title:</span></td><td><input type="text" name="txtTitle" id="txtTitle" style="font-size:14px; font-family:Tahoma, Arial; width:200px;" /></td></tr>
			 <tr><td align="right"><span class="Tahoma16">Access Level:</span></td><td><input type="radio" name="radAccessLevel" checked="true" id="radAccessLevel1" value="0"/>&nbsp;<span style="font-size:14px; font-family:Tahoma, Arial;">Public</span>&nbsp;&nbsp;<input type="radio" name="radAccessLevel" id="radAccessLevel2" value="1" />&nbsp;<span style="font-size:14px; font-family:Tahoma, Arial;">Private</span></td></tr>
			 <tr><td></td><td>
			  <iframe id="frmUpload" name="frmUpload" src="../common/documents_upload.php" style="width:380px; height:100px;" frameborder="0" marginheight="0" marginwidth="0" scrolling="no">
			  </iframe>
			 </td></tr>
			 <tr><td colspan="2" align="center"><br /><br />
			 <div align="center"><input type="submit" class="AdminFormButton1" value="Add Document" /></div>
			 </td></tr>
			</table>
		   </td>
		  </tr>
		 </table>
		 <br />
		</form>
		</div>';

		return($sReturn);
	}
	
	function ShowUpload()
	{
		$sReturn = '
		<link rel="stylesheet" href="' . cVSFFolder . '/components/jQuery/Uploadify/uploadify.css" type="text/css" media="screen" />
		<script type="text/javascript" src="' . cVSFFolder . '/components/jQuery/jQuery.js"></script>
		<script type="text/javascript" src="' . cVSFFolder . '/components/jQuery/Uploadify/swfobject.js"></script>
		<script type="text/javascript" src="' . cVSFFolder . '/components/jQuery/Uploadify/Uploadify.js"></script>

		<div align="left" id="divBrowse">
		<input id="fileInput1" name="fileInput1" type="file" />
		</div>
		<div id="divFile" style="font-family:Tahoma, Arial; font-size:12px;"></div>

		<script type="text/javascript" language="JavaScript">
		var bFileUploaded = false;
		var sFileName = "";
		
		function Completed(event, queueId, fObject)
		{
			document.getElementById("divFile").innerHTML = fObject.name + " uploaded successfully...";
			document.getElementById("divBrowse").style.display = "none";
			
			bFileUploaded = true;
			sFileName = fObject.name;
		}
		
       	$("#fileInput1").uploadify({
		\'uploader\'       : \'' . cVSFFolder . '/components/jQuery/Uploadify/uploadify.swf\',
		\'script\'         : \'../common/documents_upload.php\',
		\'cancelImg\'      : \'' . cVSFFolder . '/components/jQuery/Uploadify/cancel.png\',
		\'auto\'           : true,
		\'onComplete\'	   : Completed,
		\'multi\'          : false
		});
		</script>';
		
		return($sReturn);
	}
	
	function UploadFile()
	{
		$sTempFolder = '../../system/data/temp/'; 
		$sTempFileName = $_FILES['Filedata']['tmp_name'];
		
		$sFileName =  $sTempFolder . $_FILES['Filedata']['name'];
		if (file_exists($sFileName)) unlink($sFileName);
		
		move_uploaded_file($sTempFileName, $sFileName);
		echo(1);
	}

	function AddDocument($sComponentName, $iComponentId, $sTitle, $iAccessLevel, $sFileName)
	{
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
        $varNow = $objGeneral->fnNow();
		
		$this->SetRoles($sComponentName);
		
        if ($this->aRoles[1] == 0) // Add Disabled
        	return(1010);

        $sTitle = $objDatabase->RealEscapeString($sTitle);

        $sUploadPath = $this->sDocumentsPath;
		$sFileUniqueName = $objGeneral->fnRandomPasswordGenerator(16);

		$sTempFile = '../../system/data/temp/' . $sFileName;
      	if (file_exists($sTempFile))
			rename($sTempFile, $sUploadPath . $sFileUniqueName);

        $varResult = $objDatabase->Query("INSERT INTO fms_common_documents
        (ComponentName, ComponentId, Title, AccessLevel, FileName, FileUniqueName, DocumentAddedOn, DocumentAddedBy)
        VALUES ('$sComponentName', '$iComponentId', '$sTitle', '$iAccessLevel', '$sFileName', '$sFileUniqueName', '$varNow', '$objEmployee->iEmployeeId')");
	}
	
    function DocumentDownload($sComponentName, $iComponentId, $iDocumentId)
    {
        global $objDatabase;
        global $objGeneral;
		global $objEmployee;
		
		$this->SetRoles($sComponentName);

        if ($this->aRoles[0] == 0) // Access Disabled
        	return(1010);

        $varResult = $objDatabase->Query("SELECT * FROM fms_common_documents AS D WHERE D.ComponentName='$sComponentName' AND D.ComponentId='$iComponentId' AND D.DocumentId='$iDocumentId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, invalid document id');
        
		$sFileName = $objDatabase->Result($varResult, 0, "D.FileName");
		$sFileUniqueName = $objDatabase->Result($varResult, 0, "D.FileUniqueName");
		$iAccessLevel = $objDatabase->Result($varResult, 0, "D.AccessLevel");
		$iDocumentAddedBy = $objDatabase->Result($varResult, 0, "D.DocumentAddedBy");
		
		// Access Level
		if (($iAccessLevel == 1) && ($iDocumentAddedBy != $objEmployee->iEmployeeId))
			die('Access Denied...');

		$sDownloadFileName = $this->sDocumentsPath . $sFileUniqueName;
		
		$fHandle = fopen($sDownloadFileName, "r");
		$contents = fread($fHandle, filesize($sDownloadFileName));
		fclose($fHandle);

		// Download file
		if(isset($HTTP_SERVER_VARS['HTTP_USER_AGENT']) and strpos($HTTP_SERVER_VARS['HTTP_USER_AGENT'],'MSIE'))
			Header('Content-Type: application/force-download');
		else
			Header('Content-Type: application/octet-stream');
		if(headers_sent())
			die('Some data has already been output to browser, can\'t send the file');
		Header('Content-Length: ' . strlen($contents));
		Header('Content-disposition: attachment; filename='.$sFileName);
		print($contents);
    }

    function DeleteDocument($sComponentName, $iComponentId, $iDocumentId)
    {
        global $objDatabase;
        global $objEmployee;
		
		$iEmployeeId = $objEmployee->iEmployeeId;
		
		$this->SetRoles($sComponentName);

        if ($this->aRoles[3] == 0) // Delete Disabled
        	return(1010);
 
        $varResult = $objDatabase->Query("SELECT * FROM fms_common_documents AS D WHERE D.ComponentName='$sComponentName' AND D.ComponentId='$iComponentId' AND D.DocumentId='$iDocumentId' AND D.DocumentAddedBy='$iEmployeeId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) return(1010);
        $sFileUniqueName = $objDatabase->Result($varResult, 0, "D.FileUniqueName");

        $sFileName = $this->sDocumentsPath . $sFileUniqueName;
        if (file_exists($sFileName)) unlink($sFileName);

        $varResult = $objDatabase->Query("DELETE FROM fms_common_documents WHERE ComponentName='$sComponentName' AND ComponentId='$iComponentId' AND DocumentId='$iDocumentId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
            return(2304);
        else
            return(2305);
    }
}
?>