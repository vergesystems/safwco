<?php

class clsFMS_Common_Status
{
	public $aStatus;
	public $aStatusType;
	
	// Constructor
	function __construct()
	{
	}

	function ShowStatus($sComponentName, $iComponentId)
	{
		global $objDatabase;
		global $objEmployee;
		
		// Verify
		switch ($sComponentName)
		{
			case "Accounts_GeneralJournal": 
				include('../../system/library/fms/clsFMS_Accounts_GeneralJournal.php');
				$objComponent = new clsAccounts_GeneralJournal();
				break;
		}
		
		$this->aStatus = $objComponent->aStatus;
		$this->aStatusType = $objComponent->aStatusType;

		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr><td colspan="5"><span style="font-family:Tahoma, Arial; font-size:22px;">Status Update History</span></td></tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="98%" align="center">
		<tr class="GridTR">
		 <td width="5%" align="center" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">S#</td>
		 <td width="25%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Date / Time</td>
		 <td width="20%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Employee Name</td>
		 <td width="20%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Status</td>
		 <td width="30%" style="color:#ffffff; font-size:11px; font-family:Tahoma, Arial;">Comments</td>
		</tr>';
		
		$bAllowUpdate = true;
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_common_status AS S
		INNER JOIN organization_employees AS E ON E.EmployeeId = S.StatusAddedBy
		WHERE S.ComponentName='$sComponentName' AND S.ComponentId='$iComponentId'");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			for ($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$sEmployeeName = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
				$sComments = $objDatabase->Result($varResult, $i, "S.Comments");
				$iStatus = $objDatabase->Result($varResult, $i, "S.Status");
				$iStatusType = $objDatabase->Result($varResult, $i, "S.StatusType");
				$dStatusDateTime = $objDatabase->Result($varResult, $i, "S.StatusAddedOn");
				$sStatusDateTime = date("F j, Y", strtotime($dStatusDateTime)) . ' at ' . date("g:i a", strtotime($dStatusDateTime));
				
				$sReturn .= '<tr>
				 <td align="center" style="font-size:11px; font-family:Tahoma, Arial;">' . ($i+1) . '</td>
				 <td>' . $sStatusDateTime . '</td>
				 <td>' . $sEmployeeName . '</td>
				 <td>' . $this->aStatus[$iStatus] . '</td>
				 <td>' . $sComments . '</td>
				</tr>';
				
				if (($i+1) == $objDatabase->RowsNumber($varResult) && ($iStatusType == 1))
					$bAllowUpdate = false;
			}
		}

		$sReturn .= '</table>';

		if ($bAllowUpdate)
		{
			$sStatusList = '<select name="selStatus" id="selStatus" style="font-size:14px; font-family:Tahoma, Arial;">';
			for ($i=0; $i < count($this->aStatus); $i++)
				$sStatusList .= '<option value="' . $i . ',' . $this->aStatusType[$i] . '">' . $this->aStatus[$i] . '</option>';
			$sStatusList .= '</select>';
		
			$sReturn .= '
			<br />
			&nbsp;&nbsp;<img src="../images/icons/plus.gif" align="absmiddle" alt="Update Status" title="Update Status" border="0" /><a href="#noanchor" style="font-size:18px; font-family:Tahoma, Arial; color:green;" onclick="ShowHideDiv(\'divUpdateStatus\');">&nbsp;Update Status</a>&nbsp;&nbsp;
			<div id="divUpdateStatus" style="display:none;" align="center">
			 <br />
			 <table border="1" bordercolor="#cdcdcd" cellspacing="0" cellpadding="5" width="80%" align="center">
			  <tr>
			   <td>
				<form method="post" action="" onsubmit="return window.top.MOOdalBox.open(\'../common/status_show.php?componentname=' . $sComponentName . '&id=' . $iComponentId . '&action=AddStatus&selStatus=\'+GetSelectedListBox(\'selStatus\')+\'&txtComments=\'+GetVal(\'txtComments\'), \'' . $sTitle . '\', \'700 420\');">
				<span style="font-size:14px; color:blue;">' . $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '</span>:&nbsp;<span style="font-size:11px; color:green;">[ ' . date("F j, Y g:i a") . ' ]</span>&nbsp;<br />
				<br />
				<div align="center">
				' . $sStatusList . '
				<br /><br />
				<span class="Tahoma16">Comments:</span><br />
				<textarea name="txtComments" id="txtComments" style="font-size:14px; font-family:Tahoma, Arial; width:500px; height:50px;"></textarea>
				</div>
				<br /><br />
				<div align="center"><input type="submit" class="AdminFormButton1" value="Submit" /></div>
				</form>
			   </td>
			  </tr>
			 </table>
			</div>';
		}
		
		return($sReturn);		
	}
	
	function AddStatus($sComponentName, $iComponentId, $iStatus, $sComments)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		$iEmployeeId = $objEmployee->iEmployeeId;
		
		$aTemp = explode(',', $iStatus);
		$iStatus = $aTemp[0];
		$iStatusType = $aTemp[1];
		
		$varNow = $objGeneral->fnNow();
		$sMessage = $objDatabase->RealEscapeString($sMessage);

		$objDatabase->Query("INSERT INTO fms_common_status
		(ComponentName, ComponentId, Status, StatusType, Comments, StatusAddedOn, StatusAddedBy) 
		VALUES ('$sComponentName', '$iComponentId', '$iStatus', '$iStatusType', '$sComments', '$varNow', '$iEmployeeId')");
		
		// Update Status of Component table
		switch ($sComponentName)
		{
			case "Accounts_GeneralJournal":
				$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal SET Status = '$iStatus' WHERE GeneralJournalId='$iComponentId'");
				break;
		}
		
		return(0);
	}
	
}
?>