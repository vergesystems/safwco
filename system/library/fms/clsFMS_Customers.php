<?php

include('../../system/library/fms/clsFMS_Customers_Customers.php');
include('../../system/library/fms/clsFMS_Customers_Jobs.php');
include('../../system/library/fms/clsFMS_Customers_Receipts.php');
include('../../system/library/fms/clsFMS_Customers_Invoices.php');
include('../../system/library/fms/clsFMS_Customers_Invoices_RecurringInvoices.php');
include('../../system/library/fms/clsFMS_Customers_SalesOrders.php');

class clsCustomers
{
    // Constructor
    function __construct()
    {
    }
    
    function ShowCustomersMenu($sPage)
    {
    	global $objEmployee;
    	
    	$sCustomers = '<a ' . (($sPage == "Customers") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../customers/?page=Customers"><img src="../images/customers/iconCustomers.gif" alt="Customers" title="Customer" border="0" /><br />Customers</a>';
    	$sJobs = '<a ' . (($sPage == "Jobs") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../customers/?page=Jobs"><img src="../images/customers/iconJobs.gif" alt="Jobs" title="Customer" border="0" /><br />Jobs</a>';
    	$sQuotations = '<a ' . (($sPage == "Quotations") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../customers/?page=Quotations"><img src="../images/customers/iconQuotations.gif" alt="Quotations" title="Customer" border="0" /><br />Quotations</a>';
    	$sSalesOrders = '<a ' . (($sPage == "SalesOrders") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../customers/?page=SalesOrders"><img src="../images/customers/iconSalesOrders.gif" alt="Sales Orders" title="Sales Orders" border="0" /><br />Sales Orders</a>';
    	$sReceipts = '<a ' . (($sPage == "Receipts") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../customers/?page=Receipts"><img src="../images/customers/iconReceipts.gif" alt="Receipts" title="Receipts" border="0" /><br />Receipts</a>';
    	$sInvoices = '<a ' . (($sPage == "Invoices") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../customers/?page=Invoices"><img src="../images/customers/iconInvoices.gif" alt="Invoices" title="Invoices" border="0" /><br />Invoices</a>';
		$sStatements = '<a ' . (($sPage == "Statements") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../customers/?page=Statements"><img src="../images/customers/iconStatements.gif" alt="Statements" title="Statements" border="0" /><br />Statements</a>';
    		
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[0] == 0)    		
    		$sCustomers = '<img src="../images/customers/iconCustomers_disabled.gif" alt="Customers" title="Customers" border="0" /><br />Customers';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[0] == 0)
    		$sQuotations = '<img src="../images/customers/iconQuotations_disabled.gif" alt="Quotations" title="Quotations" border="0" /><br />Quotations';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[0] == 0)
    		$sSalesOrders = '<img src="../images/customers/iconSalesOrders_disabled.gif" alt="Sales Orders" title="Sales Orders" border="0" /><br />Sales Orders';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[0] == 0)
    		$sInvoices = '<img src="../images/customers/iconInvoices_disabled.gif" alt="Invoices" title="Invoices" border="0" /><br />Invoices';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[0] == 0)
    		$sReceipts = '<img src="../images/customers/iconReceipts_disabled.gif" alt="Receipts" title="Receipts" border="0" /><br />Receipts';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[0] == 0)
    		$sJobs = '<img src="../images/customers/iconJobs_disabled.gif" alt="Jobs" title="Jobs" border="0" /><br />Jobs';

    	$sReturn .= '
    	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
    	 <tr>
    	  <td valign="bottom">

           <table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
	    	<tr>
	    	 <td align="center" width="10%" valign="top">' . $sCustomers . '</td>	    	 
	    	 <td align="center" width="10%" valign="top">' . $sQuotations . '</td>
	    	 <td align="center" width="10%" valign="top">' . $sSalesOrders . '</td>
	    	 <td align="center" width="10%" valign="top">' . $sInvoices . '</td>
	    	 <td align="center" width="10%" valign="top">' . $sReceipts . '</td>
			 <td align="center" width="10%" valign="top">' . $sJobs . '</td>
	      	</tr>
	       </table>
    	  </td></tr></table>';
    	
    	return($sReturn);    	
    }
	
	function ShowCustomersPages($sPage)
	{
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		if ($sPage == "") $sPage = "Customers";
		
		switch($sPage)
		{
			case "Customers": 
				$aTabs[0][0] = 'Customers';
				$aTabs[0][1] = '../customers/customers.php';
				break;				
			case "Quotations": 
				$aTabs[0][0] = 'Quotations';
				$aTabs[0][1] = '../customers/quotations.php';
				break;
			case "SalesOrders": 
				$aTabs[0][0] = 'Sales Orders';
				$aTabs[0][1] = '../customers/salesorders.php';
				break;
			case "Invoices": 
				$aTabs[0][0] = 'Invoices';
				$aTabs[0][1] = '../customers/invoices.php';
				$aTabs[1][0] = 'Recurring Invoices';
				$aTabs[1][1] = '../customers/invoices.php?pagetype=recurringinvoices';
				break;
			case "Receipts":
				$aTabs[0][0] = 'Receipts';
				$aTabs[0][1] = '../customers/receipts.php';
				break;
			case "Jobs":
				$aTabs[0][0] = 'Jobs';
				$aTabs[0][1] = '../customers/jobs.php';
				break;	
			case "Budget":
				$aTabs[0][0] = 'Budget';
				$aTabs[0][1] = '../accounts/budget.php';
				break;
			case "BudgetAllocation":
				$aTabs[0][0] = 'Budget Allocation';
				$aTabs[0][1] = '../accounts/allocation.php';
				break;	
		}
		
		$sReturn = $objDHTMLSuite->TabBar($aTabs, $this->ShowCustomersMenu($sPage));
		return($sReturn);
	}
}

?>