<?php

/* Customer Class */
class clsCustomers_Customers extends clsCustomers
{
	public $aCustomerStatus;
    // Constructor
    function __construct()
    {
    	$this->aCustomerStatus = array("Inactive", "Active");
    }

    function ShowAllCustomers($iStationid=0, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
/*
		$varResult = $objDatabase->Query("
		SELECT 
			
			distinct(GJE.GeneralJournalId), 
			GJ.Reference, 
			GJ.TransactionDate,
			GJ.TotalDebit,
			
			GJ.DonorId,
			GJ.DonorProjectId,
			
			GJE.DonorId,
			GJE.DonorProjectId,
			DP.ProjectTitle
			
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId = GJE.DonorProjectId
		WHERE 1=1 AND GJ.DonorProjectId = '0' order by GJ.GeneralJournalId, GJE.GeneralJournalEntryId ");
		
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{	
			$iGJId				= $objDatabase->Result($varResult, $i, "GJE.GeneralJournalId");
			$sReference			= $objDatabase->Result($varResult, $i, "GJ.Reference");
			$dTransactionDate	= $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
			$dTotalDebit		= $objDatabase->Result($varResult, $i, "GJ.TotalDebit");
			
			$iDonorId			= $objDatabase->Result($varResult, $i, "GJ.DonorId");
			$iDonorProjectId	= $objDatabase->Result($varResult, $i, "GJ.DonorProjectId");

			$iDonorId1			= $objDatabase->Result($varResult, $i, "GJE.DonorId");
			$iDonorProjectId1	= $objDatabase->Result($varResult, $i, "GJE.DonorProjectId");
			$sProjectTitle		= $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
			
			$varResult1 = $objDatabase->Query("
			SELECT * FROM fms_accounts_generaljournal_entries AS GJE		
			WHERE GJE.GeneralJournalId ='$iGJId' AND GJE.DonorProjectId <> '$iDonorProjectId' ");
			if ($objDatabase->RowsNumber($varResult1) > 0) 
			{
				echo '<br><Br>'.$iGJId.', '.$sReference.', '.$dTransactionDate.', '.$dTotalDebit.', '.$iDonorId1.', '.$iDonorProjectId1;
				/* echo '<Br>'."
				UPDATE fms_accounts_generaljournal
				SET
					DonorId='$iDonorId',
					DonorProjectId='$iDonorProjectId'					
				WHERE GeneralJournalId='$iGJId'";
				
				$varResult99 = $objDatabase->Query("
				UPDATE fms_accounts_generaljournal
				SET
					DonorId='$iDonorId1',
					DonorProjectId='$iDonorProjectId1'					
				WHERE GeneralJournalId='$iGJId'");
				
				/*
				echo '<Br>'."
				UPDATE fms_accounts_generaljournal_entries
				SET
					DonorId='$iDonorId',
					DonorProjectId='$iDonorProjectId'					
				WHERE GeneralJournalId='$iGJId'";
				
				
				$varResult88 = $objDatabase->Query("
				UPDATE fms_accounts_generaljournal_entries
				SET
					DonorId='$iDonorId',
					DonorProjectId='$iDonorProjectId'					
				WHERE GeneralJournalId='$iGJId'");
						
			}
		
		}
	
		
		die("test....");
*/
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = "Customers";
        if ($sSortBy == "") $sSortBy = "C.CustomerId DESC";
		
		if ($iStationid > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' AND S.StationId='$iStationid'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid StationId Id');
				$sStationName = $objDatabase->Result($varResult, 0, "S.StationName");			
			$sTitle = "Customers from " . $sStationName;
			
			$sStationCondition = "AND S.StationId='$iStationid' ";		
		}
		else $sStationCondition =  "";
		
        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((C.CustomerId LIKE '%$sSearch%') OR (C.FirstName LIKE '%$sSearch%') OR (C.LastName LIKE '%$sSearch%') OR (C.Designation LIKE '%$sSearch%') OR (C.CompanyName LIKE '%$sSearch%') OR (S.StationName LIKE '%$sSearch%')) ";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

		$iTotalRecords = $objDatabase->DBCount("fms_customers AS C INNER JOIN organization_employees AS E ON E.EmployeeId = C.CustomerAddedBy INNER JOIN organization_stations AS S ON S.StationId = C.StationId", "C.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sStationCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_customers AS C
		INNER JOIN organization_employees AS E ON E.EmployeeId = C.CustomerAddedBy
		INNER JOIN organization_stations AS S ON S.StationId = C.StationId
        WHERE C.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sStationCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">		 
		 <td align="left"><span class="WhiteHeading">Customer Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Customer Name in Ascending Order" title="Sort by Customer Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Customer Name in Descending Order" title="Sort by Customer Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Company&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Company Name in Ascending Order" title="Sort by Company Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Company Name in Descending Order" title="Sort by Company Name in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Designation&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.Designation&sortorder="><img src="../images/sort_up.gif" alt="Sort by Designation in Ascending Order" title="Sort by Designation in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.Designation&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Designation in Descending Order" title="Sort by Designation in Descending Order" border="0" /></a></span></td>
		 <td align="left"><span class="WhiteHeading">Email Address&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.EmailAddress&sortorder="><img src="../images/sort_up.gif" alt="Sort by Email Address in Ascending Order" title="Sort by Email Address in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.EmailAddress&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Email Address in Descending Order" title="Sort by Email Address in Descending Order" border="0" /></a></span></td>
		 <td width="4%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$sFirstName = $objDatabase->Result($varResult, $i, "C.FirstName");
			$sFirstName = $objDatabase->RealEscapeString($sFirstName);
			$sFirstName = stripslashes($sFirstName);			
			$sLastName = $objDatabase->Result($varResult, $i, "C.LastName");
			$sLastName = $objDatabase->RealEscapeString($sLastName);
			$sLastName = stripslashes($sLastName);
			$iCustomerId = $objDatabase->Result($varResult, $i, "C.CustomerId");
			$iCurrentStationId = $objDatabase->Result($varResult, $i, "C.StationId");
			$sStationName = $objDatabase->Result($varResult, $i, "S.StationName");			
            $sPhoneNumber = $objDatabase->Result($varResult, $i, "C.PhoneNumber");
            $sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");
			$sDesgignation = $objDatabase->Result($varResult, $i, "C.Designation");		
			
			$sEmailAddress = $objDatabase->Result($varResult, $i, "C.EmailAddress");		
			
            $sEditCustomer = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Edit Customer ' . $objDatabase->RealEscapeString($sFirstName). '\', \'../customers/customers.php?pagetype=details&action2=edit&id=' . $iCustomerId. '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Customer Details" title="Edit this Customer Details"></a></td>';
            $sDeleteCustomer = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Customer?\')) {window.location = \'?action=DeleteCustomer&id=' . $iCustomerId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Customer" title="Delete this Customer"></a></td>';
			$sCustomersLedger = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Customers Ledger ' . $objDatabase->RealEscapeString(stripslashes($sVendorCode)) . '\', \'../customers/customers.php?pagetype=customersledger&id=' . $iCustomerId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" alt="Customers Ledger" title="Customers Ledger"></a></td>';

       		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[2] == 0)	$sEditCustomer = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[3] == 0)	$sDeleteCustomer = '<td class="GridTD">&nbsp;</td>';

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
             <td class="GridTD" align="left" valign="top">' . $sFirstName . ' ' . $sLastName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCompanyName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sDesgignation . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sEmailAddress . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'' . $objDatabase->RealEscapeString($sFirstName) . '\',  \'../customers/customers.php?pagetype=details&id=' . $iCustomerId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Customer Details" title="View this Customer Details"></a></td>
			 ' . $sEditCustomer . '
			 ' . $sCustomersLedger . '
             ' . $sDeleteCustomer . '
            </tr>';
		}
		
		
		$sStationSelect = '<select class="form1" name="selStation" onchange="window.location=\'?stationid=\'+GetSelectedListBox(\'selStation\');" id="selStation">
		<option value="0">All Stations</option>';
		$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' ORDER BY S.StationId");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sStationSelect .= '<option ' . (($iStationid == $objDatabase->Result($varResult, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "S.StationId") . '">' . $objDatabase->Result($varResult, $i, "S.StationName") . '</option>';
		$sStationSelect .= '</select>';	

		$sAddNewCustomer = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Customer\', \'../customers/customers.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Customer">';
       	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[1] == 0) // Add Disabled
			$sAddNewCustomer = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		  ' . $sAddNewCustomer . '&nbsp;<form method="GET" action="">Search for a Customer:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Customer" title="Search for Customer" border="0"></form>
		  </td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td>
		   ' . $sStationSelect . '
		  </td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Customer Details" alt="View this Customer Details"></td><td>View this Customer Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Customer Details" alt="Edit this Customer Details"></td><td>Edit this Customer Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconContents.gif" title="Customers Ledger" alt="Customers Ledger"></td><td>Customers Ledger</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Customer" alt="Delete this Customer"></td><td>Delete this Customer</td></tr>
        </table>';

		return($sReturn);
     }
    
    
     function CustomerDetails($iCustomerId, $sAction2)
     {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_customers AS C
		INNER JOIN organization_employees AS E ON E.EmployeeId = C.CustomerAddedBy
		INNER JOIN organization_stations AS S ON S.StationId = C.StationId
		WHERE C.OrganizationId='" . cOrganizationId . "' AND S.OrganizationId='" . cOrganizationId . "' AND C.CustomerId = '$iCustomerId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iCustomerId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Customer" title="Edit this Customer" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Customer?\')) {window.location = \'?pagetype=details&action=DeleteCustomer&id=' . $iCustomerId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Customer" title="Delete this Customer" /></a>';

    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[3] == 0)	$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Customer Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
		    	$iStationId = $objDatabase->Result($varResult, 0, "C.StationId");
		    	
		    	$sStationName= $objDatabase->Result($varResult, 0, "S.StationName");		    	
		    	$sStationName = $sStationName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $objDatabase->RealEscapeString(str_replace('"', '', $sStationName)) . '\', \'../organization/stations.php?pagetype=details&id=' . $iStationId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
		    	
		        $sFirstName = $objDatabase->Result($varResult, 0, "C.FirstName");		       
		        $sLastName = $objDatabase->Result($varResult, 0, "C.LastName");		       
		        $sNICNumber = $objDatabase->Result($varResult, 0, "C.NICNumber");
				$cGender = $objDatabase->Result($varResult, 0, "C.Gender");
		        $sGender = ($cGender == 'M') ? 'Male' : 'Female';
								
		        // Customer Address
		        $sAddress = $objDatabase->Result($varResult, 0, "C.Address");		       
		        $sCity = $objDatabase->Result($varResult, 0, "C.City");		       
		        $sState = $objDatabase->Result($varResult, 0, "C.State");		        
		        $sZipCode = $objDatabase->Result($varResult, 0, "C.ZipCode");		        
		        $sCountry = $objDatabase->Result($varResult, 0, "C.Country");				
		        $sPhoneNumber = $objDatabase->Result($varResult, 0, "C.PhoneNumber");
				$sMobileNumber = $objDatabase->Result($varResult, 0, "C.MobileNumber");
				$sEmailAddress = $objDatabase->Result($varResult, 0, "C.EmailAddress");
				$sFaxNumber = $objDatabase->Result($varResult, 0, "C.FaxNumber");
				$iStatus = $objDatabase->Result($varResult, 0, "C.Status");
		        $sStatus = $this->aCustomerStatus[$iStatus];
				// Customer Company info
				$sCompanyName = $objDatabase->Result($varResult, 0, "C.CompanyName");
				$sDesignation = $objDatabase->Result($varResult, 0, "C.Designation");
				$sCompanyWebsite = $objDatabase->Result($varResult, 0, "C.CompanyWebsite");
				
		        $sCustomerAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName"). ' ' . $objDatabase->Result($varResult, 0, "E.LastName");				
		        $sCustomerAddedBy = $sCustomerAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sCustomerAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

				$sNotes = $objDatabase->Result($varResult, 0, "C.Notes");

				$dCustomerAddedOn = $objDatabase->Result($varResult, 0, "C.CustomerAddedOn");				
				$sCustomerAddedOn = date("F j, Y", strtotime($dCustomerAddedOn)) . ' at ' . date("g:i a", strtotime($dCustomerAddedOn));
				
				$sCustomerPhoto = '<img src="' . $this->FindCustomerPhoto($iCustomerId) . '" border="0" alt="' . $sFirstName . ' ' . $sLastName . '" title="' . $sFirstName . ' ' . $sLastName . '" />';
		    }

			if ($sAction2 == "edit")
			{
				
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();" enctype="multipart/form-data">';
				
				$sCustomerPhotoUpload = '<tr bgcolor="#edeff1"><td>Customer Photo:</td><td><input class="form1" type="file" name="fileField1" accept="text/*" size="30" maxlength="' . cUploadFileMaxLimit . '"></td></tr>';
				
				$sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';

		        $sStationName = '<select class="form1" name="selStation" id="selStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' ORDER BY S.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess == 1)
						$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
					else
					{
						if ($objEmployee->iEmployeeStationId == $objDatabase->Result($varResult2, $i, "S.StationId"))
							$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
					}
				}
				$sStationName .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selStation\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
				
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aCustomerStatus); $i++)
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . 'value="' . $i . '">' . $this->aCustomerStatus[$i];
				$sStatus .='</select>';
				
				$sFirstName = '<input type="text" name="txtFirstName" id="txtFirstName" class="form1" value="' . $sFirstName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sLastName = '<input type="text" name="txtLastName" id="txtLastName" class="form1" value="' . $sLastName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNICNumber = '<input type="text" name="txtNICNumber" id="txtNICNumber" class="form1" value="' . $sNICNumber . '" size="30" />';
				
  		        $sGender = '<select class="form1" name="selGender" id="selGender">';
		        $sGender .= '<option ' . (($cGender == 'M') ? 'selected="true"' : '') . ' value="M">Male</option>';
		        $sGender .= '<option ' . (($cGender == 'F') ? 'selected="true"' : '') . ' value="F">Female</option>';
		        $sGender .= '</select>';
				
				/* Customer address */
		        $sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sCountry = $sCountryString;
				/* Customer contact details */
				
				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />';
				$sMobileNumber = '<input type="text" name="txtMobileNumber" id="txtMobileNumber" class="form1" value="' . $sMobileNumber . '" size="30" />';
				$sEmailAddress = '<input type="text" name="txtEmailAddress" id="txtEmailAddress" class="form1" value="' . $sEmailAddress . '" size="30" />';
				$sFaxNumber = '<input type="text" name="txtFaxNumber" id="txtFaxNumber" class="form1" value="' . $sFaxNumber . '" size="30" />';
				
				// Customers company info
				$sCompanyName = '<input type="text" name="txtCompanyName" id="txtCompanyName" class="form1" value="' . $sCompanyName . '" size="30" />';
				$sDesignation = '<input type="text" name="txtDesignation" id="txtDesignation" class="form1" value="' . $sDesignation . '" size="30" />';
				$sCompanyWebsite = '<input type="text" name="txtCompanyWebsite" id="txtCompanyWebsite" class="form1" value="' . $sCompanyWebsite . '" size="30" />';
				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{				
				$iCustomerId = "";
				$iEmployeeId = "";
		        $sFirstName = $objGeneral->fnGet("txtFirstName");
		        $sLastName = $objGeneral->fnGet("txtLastName");
		        $sNICNumber = $objGeneral->fnGet("txtNICNumber");
		        $sAddress = $objGeneral->fnGet("txtAddress");
		        $sCity = $objGeneral->fnGet("txtCity");
		        $sZipCode = $objGeneral->fnGet("txtZipCode");
		        $sState = $objGeneral->fnGet("txtState");
		        $sCountry = $objGeneral->fnGet("selCountry");
		        $sPhoneNumber = $objGeneral->fnGet("txtPhoneNumber");
		        $sMobileNumber = $objGeneral->fnGet("txtMobileNumber");
		        $sEmailAddress = $objGeneral->fnGet("txtEmailAddress");
				$sFaxNumber = $objGeneral->fnGet("txtFaxNumber");
		        $sNotes = $objGeneral->fnGet("txtNotes");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sCompanyName = $objGeneral->fnGet("txtCompanyName");
				$sDesignation = $objGeneral->fnGet("txtDesignation");
				$sCompanyWebsite = $objGeneral->fnGet("txtCompanyWebsite");
				$iStatus = 1;		
		        
                $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();" enctype="multipart/form-data">';
				
				$sCustomerPhoto = '<img src="../images/customers/NoImage.jpg" alt="No Customer Photo" title="No Customer Photo" border="0" />';
				$sCustomerPhotoUpload = '<tr bgcolor="#edeff1"><td>Customer Photo:</td><td><input class="form1" type="file" name="fileField1" accept="text/*" size="30" maxlength="' . cUploadFileMaxLimit . '"></td></tr>';
				
				$sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';
				
		       $sStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aCustomerStatus); $i++)
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aCustomerStatus[$i];
				$sStatus .='</select>';
		        
                $sFirstName = '<input type="text" name="txtFirstName" id="txtFirstName" class="form1" value="' . $sFirstName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sLastName = '<input type="text" name="txtLastName" id="txtLastName" class="form1" value="' . $sLastName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNICNumber = '<input type="text" name="txtNICNumber" id="txtNICNumber" class="form1" value="' . $sNICNumber . '" size="30" />';
  		        
				$sGender = '<select class="form1" name="selGender" id="selGender">';
		        $sGender .= '<option ' . (($cGender == 'M') ? 'selected="true"' : '') . ' value="M">Male</option>';
		        $sGender .= '<option ' . (($cGender == 'F') ? 'selected="true"' : '') . ' value="F">Female</option>';
		        $sGender .= '</select>';

		        $sStationName = '<select class="form1" name="selStation" id="selStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' ORDER BY S.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess == 1)
						$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
					else
					{
						if ($objEmployee->iEmployeeStationId == $objDatabase->Result($varResult2, $i, "S.StationId"))
							$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
					}
				}
				$sStationName .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selStation\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
								
				/* Customer address */
				$sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sCountry = $sCountryString;
				/* Customer contact details */
				
				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />';
				$sMobileNumber = '<input type="text" name="txtMobileNumber" id="txtMobileNumber" class="form1" value="' . $sMobileNumber . '" size="30" />';
				$sEmailAddress = '<input type="text" name="txtEmailAddress" id="txtEmailAddress" class="form1" value="' . $sEmailAddress . '" size="30" />';
				$sFaxNumber = '<input type="text" name="txtFaxNumber" id="txtFaxNumber" class="form1" value="' . $sFaxNumber . '" size="30" />';
				
				/* Customers company info */
				$sCompanyName = '<input type="text" name="txtCompanyName" id="txtCompanyName" class="form1" value="' . $sCompanyName . '" size="30" />';
				$sDesignation = '<input type="text" name="txtDesignation" id="txtDesignation" class="form1" value="' . $sDesignation . '" size="30" />';
				$sCompanyWebsite = '<input type="text" name="txtCompanyWebsite" id="txtCompanyWebsite" class="form1" value="' . $sCompanyWebsite . '" size="30" />';
				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

			   	$sCustomerAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				$sCustomerAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
                $sCustomerAddedBy = $sCustomerAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sCustomerAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';				
			}

			$sReturn .= '
            <script language="JavaScript" type="text/javascript">
		    function ValidateForm()
		    {
		       	if (GetVal(\'txtFirstName\') == "") return(AlertFocus(\'Please enter a valid Customer First Name\', \'txtFirstName\'));
		       	if (GetVal(\'txtLastName\') == "") return(AlertFocus(\'Please enter a valid Last Name\', \'txtLastName\'));
				return true;
		    }
		    </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="Details_Title_TR"><td colspan="3"><span class="Details_Title">Customer Information:</span></td></tr>
			 <tr bgcolor="#ffffff">
			  <td width="20%">Station:</td><td><strong>' . $sStationName . '</strong></td>
			  <td rowspan="6" valign="top">
			   <table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			    <tr>
			     <td align="right" valign="top">' . $sCustomerPhoto . '</td>
			    </tr>
			   </table>
			  </td>
			 </tr>
			 <!--<tr bgcolor="#edeff1"><td valign="top">Station:</td><td><strong>' . $sStationName . '</strong></td></tr>-->
			 <tr bgcolor="#ffffff"><td>First Name:</td><td><strong>' . $sFirstName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Last Name:</td><td><strong>' . $sLastName . '</strong></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top">NIC Number:</td><td><strong>' . $sNICNumber . '</strong></td></tr>			 
			 <tr bgcolor="#edeff1"><td>Gender: </td><td><strong>' . $sGender . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Status: </td><td><strong>' . $sStatus . '</strong></td></tr>
			 ' . $sCustomerPhotoUpload . '
			 <tr class="Details_Title_TR"><td colspan="3"><span class="Details_Title">Company Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td>Company Name:</td><td colspan="2"><strong>' . $sCompanyName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Designation:</td><td colspan="2"><strong>' . $sDesignation . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Company Website:</td><td colspan="2"><strong>' . $sCompanyWebsite . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="3"><span class="Details_Title">Customer Address:</span></td></tr>
		     <tr bgcolor="#edeff1"><td>Address:</td><td colspan="2"><strong>' . $sAddress . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>City:</td><td colspan="2"><strong>' . $sCity . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>State/Province:</td><td colspan="2"><strong>' . $sState . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Zip Code:</td><td colspan="2"><strong>' . $sZipCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Country:</td><td colspan="2"><strong>' . $sCountry . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="3"><span class="Details_Title">Contact Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td>Phone Number:</td><td colspan="2"><strong>' . $sPhoneNumber . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Mobile Number:</td><td colspan="2"><strong>' . $sMobileNumber . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Email Address:</td><td colspan="2"><strong>' . $sEmailAddress . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Fax Number:</td><td colspan="2"><strong>' . $sFaxNumber . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="3"><span class="Details_Title">Extra Information:</span></td></tr>
		     <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td colspan="2"><strong>' . $sNotes . '</strong></td></tr>			 
		     <tr bgcolor="#edeff1"><td>Customer Added On:</td><td colspan="2"><strong>' . $sCustomerAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Customer Added By:</td><td colspan="2"><strong>' . $sCustomerAddedBy . '</strong></td></tr>
	        </table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iCustomerId . '"><input type="hidden" name="action" id="action" value="UpdateCustomer"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Customer" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewCustomer"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';

		}

		$sReturn .= '<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

	function ShowAdvancedSearch($sWidth = "70%", $iFormId)
    {
    	global $objDatabase;
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();

		$sStationString = '<select class="form1" name="selStation" id="selStation">
		<option value="0">Any Station</option>';
		
		$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S 
		INNER JOIN fms_common_types AS ST ON ST.TypeId = S.StationType
		WHERE S.OrganizationId='" . cOrganizationId . "' AND ST.ComponentName = 'Organization_StationTypes'
		ORDER BY S.StationName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
			$sStationString .= '<option value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . ' (' . $objDatabase->Result($varResult2, $i, "ST.TypeName") . ')</option>';
		$sStationString .= '</select>';
		
		$dCustomerAddeDateTime = date("Y-m-d", mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-15));
		$dCustomerAddeDateTime2 = date("Y-m-d");
		
		$sCustomerAddeDateTime = '<input size="10" type="text" id="txtCustomerAddedDate" name="txtCustomerAddedDate" class="form1" value="' . $dCustomerAddeDateTime . '" />' . $objjQuery->Calendar('txtCustomerAddedDate');
		$sCustomerAddeDateTime2 = '<input size="10" type="text" id="txtCustomerAddedDate2" name="txtCustomerAddedDate2" class="form1" value="' . $dCustomerAddeDateTime2 . '" />' . $objjQuery->Calendar('txtCustomerAddedDate2');
		    	
    	$sReturn = '<div id="divAdvancedSearch" style="display:none;">
		 <form method="GET" action="">
		 <table border="0" cellspacing="0" cellpadding="3" width="' . $sWidth . '" align="center">
		  <tr>
		   <td>
		    <fieldset><legend style="font-size:12px; font-weight:bold;">Advanced Search</legend>
		     <br />
		      <table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		       <tr><td align="right">First Name:</td><td><input type="text" class="form1" name="txtFirstName" id="txtFirstName" /></td></tr>
		       <tr><td align="right">Last Name:</td><td><input type="text" class="form1" name="txtLastName" id="txtLastName" /></td></tr>
		       <tr><td align="right">CNIC Number:</td><td><input type="text" class="form1" name="txtCNICNumber" id="txtCNICNumber" /></td></tr>		       
		       <tr><td align="right">Station:</td><td>' . $sStationString . '</td></tr>
		       <tr><td align="right">Customer Added Between:</td><td>' . $sCustomerAddeDateTime . ' and ' . $sCustomerAddeDateTime2 . '</td></tr>
		      </table>
		     <br />
		     <div align="center"><input type="submit" value="Search" class="AdminFormButton1" /></div>
		    </fieldset>
		   </td>
		  </tr>
		 </table>
		 <input type="hidden" name="action" id="action" value="SearchCustomer" />
		 <input type="hidden" name="view" id="view" value="1" />
		 </form>
		</div>';

    	return($sReturn);
    }
	
	function ShowCustomersDirectory($iCustomerId = 0, $sAction, $iView = 0)
	{
    	global $objDatabase;
    	global $objGeneral;    	
    	
    	$sCustomerSearch = $objGeneral->fnGet("txtCustomerSearch");
    	$iPagingLimit = 6;
    	
    	$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
    	$sButtons = $sButtons_Print;
    	
    	$sReturn = '<table border="0" width="825" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Customers Directory</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="825" cellpadding="1" cellspacing="0" bgcolor="#838274" align="center"><tr><td><table width="825" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">
		<br />

		<table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
		 <tr><td colspan="2"><span style="font-size:16px; font-weight:bold;">Customer Search:</span></td></tr>
		 <tr>
		  <td>
		  <form method="POST" action="../customers/customersdirectory.php">
		  <input type="text" name="txtCustomerSearch" id="txtCustomerSearch" size="27" class="form2" value="' . $sCustomerSearch . '" />
		  </td>
		  <td>
		   <input type="submit" class="AdminFormButton1" value="Search" /><input type="hidden" name="action" id="action" value="SearchCustomer" /></form>
		  </td>
		 </tr>
		 <tr>
		  <td></td>
		  <td>[ <a href="#noanchor" onclick="ShowHideDiv(\'divAdvancedSearch\');">Advanced Search</a> ]</td>
		 </tr>
		</table>
		' . $this->ShowAdvancedSearch(' ', 1) . '
		<br /><br />
		';
    	    	
    	if (($sAction == "SearchCustomer") && ($iCustomerId == 0))
    	{
    		
    		if ($iView == 0)
    		{
	    		// If there is a space in customer search
	    		if (strpos($sCustomerSearch, ' ') !== false)
	    		{
	    			$aCustomerSearch = explode(' ', $sCustomerSearch);
	    			if (count($aCustomerSearch) > 0)
	    				$sCustomerSearch2 = $aCustomerSearch[0];
	    		}
	    		else 
	    			$sCustomerSearch2 = $sCustomerSearch;
    		}
    		else 
    		{
            	$sAdvancedSearch_FirstName = $objGeneral->fnGet("txtFirstName");
            	$sAdvancedSearch_FirstName = $objDatabase->RealEscapeString($sAdvancedSearch_FirstName);
            	$sAdvancedSearch_LastName = $objGeneral->fnGet("txtLastName");
            	$sAdvancedSearch_LastName = $objDatabase->RealEscapeString($sAdvancedSearch_LastName);
            	
	        	$sAdvancedSearch_CNICNumber = $objGeneral->fnGet("txtCNICNumber");
	        	$sAdvancedSearch_CNICNumber = $objDatabase->RealEscapeString($sAdvancedSearch_CNICNumber);
	        	$cAdvancedSearch_Gender = $objGeneral->fnGet("selGender");
	        	$iAdvancedSearch_StationId = $objGeneral->fnGet("selStation");
	        	$iAdvancedSearch_StationId = $objDatabase->RealEscapeString($iAdvancedSearch_StationId);
	        	
	        	$dAdvancedSearch_StartDate = $objGeneral->fnGet("txtCustomerAddedDate");
	        	$dAdvancedSearch_StartDate = $objDatabase->RealEscapeString($dAdvancedSearch_StartDate);
	        	$dAdvancedSearch_EndDate = $objGeneral->fnGet("txtCustomerAddedDate2");
	        	$dAdvancedSearch_EndDate = $objDatabase->RealEscapeString($dAdvancedSearch_EndDate);
	        	
	        	$sAdvancedSearchString = "";
	        	if ($sAdvancedSearch_FirstName != '') $sAdvancedSearchString .= " AND (C.FirstName LIKE '%$sAdvancedSearch_FirstName%')";
	        	if ($sAdvancedSearch_LastName != '') $sAdvancedSearchString .= " AND (C.LastName LIKE '%$sAdvancedSearch_LastName%')";
				if ($sAdvancedSearch_CNICNumber != '') $sAdvancedSearchString .= " AND (C.CNICNumber LIKE '%$sAdvancedSearch_CNICNumber%')";
				if ($cAdvancedSearch_Gender != '0') $sAdvancedSearchString .= " AND (C.Gender LIKE '%$cAdvancedSearch_Gender%')";
				if ($iAdvancedSearch_StationId > 0) $sAdvancedSearchString .= " AND (C.StationId = '$iAdvancedSearch_StationId')";			

				
				$sAdvancedSearchString .= " AND (C.CustomerAddedOn BETWEEN '$dAdvancedSearch_StartDate 00:00:00' AND '$dAdvancedSearch_EndDate 23:59:59')";
    		}
    		    		
    		if ($sCustomerSearch2 != "")
	        {
	        	$sCustomerSearch2 = $objDatabase->RealEscapeString($sCustomerSearch2);
	            $sSearchCondition = " AND ((C.CustomerId LIKE '%$sCustomerSearch2%') OR (C.FirstName LIKE '%$sCustomerSearch2%') OR (C.LastName LIKE '%$sCustomerSearch2%') OR (C.CompanyName LIKE '%$sCustomerSearch2%') OR (C.CompanyWebsite LIKE '%$sCustomerSearch2%') OR (C.PhoneNumber LIKE '%$sCustomerSearch2%') OR (C.EmailAddress LIKE '%$sCustomerSearch2%') OR (C.MobileNumber LIKE '%$sCustomerSearch2%') OR (C.NICNumber LIKE '%$sCustomerSearch2%')) ";
	        }
	        else 
	        	$sSearchCondition = $sAdvancedSearchString;

			$sQuery = "SELECT * FROM fms_customers AS C WHERE C.OrganizationId='" . cOrganizationId . "' $sSearchCondition ";
			$varResult = $objDatabase->Query($sQuery);
			$iNumberOfRecords = $objDatabase->RowsNumber($varResult);
    		if ($iNumberOfRecords <= 0)
    		{
	    		$sReturn .= '
	    		<table boder="0" cellspacing="0" cellpadding="3" width="90%" align="center">
	    		 <tr><td align="center"><span style="font-size:12px; color:red;">Sorry, your search returned no results...<br /><br /></span></td></tr>
	    		</table>';
    		}
			else
    		{
    			if ($iNumberOfRecords == 1)
    			{
    				$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
					$objGeneral->fnRedirect('../customers/customersdirectory.php?txtCustomerSearch=' . urlencode($sCustomerSearch) . '&id=' . $iCustomerId);
    			}
    			else
    			{
					$sReturn .= '<br />
					<div align="center">
					 <span style="font-size:12px; color:green;">Your search returned more than one results,<br />please select a customer from the following list:<br /><br /></span>
					</div>
		    		<table boder="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		    		 <tr>
		    		  <td>
				       <table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">

						<table class="GridTable"  border="1" style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;" cellspacing="0" cellpadding="3" width="100%" align="center">
				         <tr class="GridTR"><td colspan="3"><span class="title2">Select a Customer:</span></td></tr>
						 <tr class="GridTR"><td class="WhiteHeading">Customer</td><td class="WhiteHeading">Company Name</td><td class="WhiteHeading">Company Website</td></tr>';

					for ($i=0; $i < $iNumberOfRecords; $i++)
					{
						$sReturn .= '<tr bgcolor="#ffffff">
						 <td><a style="font-size:12px; text-decoration:underline; color:blue;" href="../customers/customersdirectory.php?txtCustomerSearch=' . $sCustomerSearch . '&id=' . $objDatabase->Result($varResult, $i, "C.CustomerId") . '">' . $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") . '</a></td>
						  <td style="font-size:12px;">' . $objDatabase->Result($varResult, $i, "C.CompanyName") . '</td>
						  <td style="font-size:12px;">' . $objDatabase->Result($varResult, $i, "C.CompanyWebsite") . '</td>
						 </tr>';
					}

					$sReturn .= '
						</table>
				       </td></tr></table></td></tr></table>
		    		  </td>
		    		 </tr>
		    		</table>
		    		<br />';
    			}
    		}
    	}

    	if ($iCustomerId > 0)
    	{
    		$varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C WHERE C.OrganizationId='" . cOrganizationId . "' AND C.CustomerId='$iCustomerId'");

    		if ($objDatabase->RowsNumber($varResult) <= 0) die('Invalid Customer Id');

    		$sFirstName = $objDatabase->Result($varResult, 0, "C.FirstName");
    		$sLastName = $objDatabase->Result($varResult, 0, "C.LastName");
    		$sCustomerFullName = $sFirstName . ' ' . $sLastName;
			
    		$sGender = ($objDatabase->Result($varResult, 0, "C.Gender") == 'M') ? 'Male' : 'Female';
    		$sCNICNumber = $objDatabase->Result($varResult, 0, "C.NICNumber");
			$sCompanyName = $objDatabase->Result($varResult, 0, "C.CompanyName");
			$sCompanyWebsite = $objDatabase->Result($varResult, 0, "C.CompanyWebsite");
			$sPhoneNumber = $objDatabase->Result($varResult, 0, "C.PhoneNumber");
			$sMobileNumber = $objDatabase->Result($varResult, 0, "C.MobileNumber");
			$sEmailAddress = $objDatabase->Result($varResult, 0, "C.EmailAddress");
			
			$iStatus = $objDatabase->Result($varResult, 0, "C.Status");
    		$sStatus = ($iStatus == 1) ? 'Active' : 'Inactive';
			
			$sAddress = $objDatabase->Result($varResult, 0, "C.Address");
			$sCity = $objDatabase->Result($varResult, 0, "C.City");
			$sState = $objDatabase->Result($varResult, 0, "C.State");
			$sZipCode = $objDatabase->Result($varResult, 0, "C.ZipCode");
			$sCountry = $objDatabase->Result($varResult, 0, "C.Country");
			
			$sReturn .= '
    		<table boder="0" cellspacing="0" cellpadding="3" width="90%" align="center">
    		 <tr>
    		  <td>
				<table border="0" style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;" cellspacing="0" cellpadding="8" width="100%" align="center">
		         <tr class="GridTR"><td colspan="4"><span class="title2">' . $sCustomerFullName . '</span></td></tr>
				 <tr>
				  <td width="20%" class="Tahoma16">First Name:</td><td width="30%" style="text-decoration:underline;" class="Tahoma16">' . $sFirstName . '</td>
				  <td rowspan="6" colspan="2" align="right" valign="top"><img src="' . $this->FindCustomerPhoto($iCustomerId) . '" /></td>
		         </tr>
				 <tr><td class="Tahoma16">Last Name:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sLastName . '</td></tr>
				 <tr><td class="Tahoma16">Gender:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sGender . '</td></tr>
				 <tr><td class="Tahoma16">Company Name:</td><td colspan="3" style="text-decoration:underline;" class="Tahoma16">' . $sCompanyName . '</td></tr>
				 <tr><td class="Tahoma16">Website:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sCompanyWebsite . '</td></tr>
				 <tr><td class="Tahoma16">E-Mail Address:</td><td colspan="3" style="text-decoration:underline;" class="Tahoma16">' . $sEmailAddress . '</td></tr>
				 <tr>
				  <td valign="top" class="Tahoma16">Phone Number:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sPhoneNumber . '</td>
				  <td valign="top" width="20%" class="Tahoma16">Mobile Number:</td><td valign="top" width="30%" style="text-decoration:underline;" class="Tahoma16">' . $sMobileNumber . '</td>
				 </tr>
				 <tr>
				  <td class="Tahoma16" valign="top">Address:</td><td colspan="3" style="text-decoration:underline;" class="Tahoma16">' . $sAddress . '</td>
				 </tr>
				 <tr>
				  <td class="Tahoma16" valign="top">City:</td><td valign="top" style="text-decoration:underline;" class="Tahoma16">' . $sCity . '</td>
				  <td class="Tahoma16">State:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sState . '</td>
				 </tr>
				 <tr>
				  <td class="Tahoma16">Zip Code:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sZipCode . '</td>
				  <td class="Tahoma16">Country:</td><td style="text-decoration:underline;" class="Tahoma16">' . $objGeneral->aCountriesList[$sCountry] . '</td>
				 </tr>
				</table>
    		  </td>
    		 </tr>
    		</table>
    		<br />';
    	}

    	$sReturn .= '</td></tr></table></td></tr></table><br />
    	<div align="center"><input type="button" class="AdminFormButton1" onclick="window.close();" value="Close" /></div>';			

		return($sReturn);
	}
	
	function FindCustomerPhoto($iCustomerId)
	{
		$sPhotoFolder = cDataFolder . '/vf/customerphotos/';

    	if (file_exists($sPhotoFolder . $iCustomerId . '.jpg'))
    		return($sPhotoFolder . $iCustomerId . '.jpg');
    	else if (file_exists($sPhotoFolder . $iCustomerId . '.gif'))
    		return($sPhotoFolder . $iCustomerId . '.gif');
    	else
    		return('../images/customers/NoImage.jpg');
	}
    
	function AddNewCustomer($sFirstName, $sLastName, $sNICNumber, $sGender, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sMobileNumber, $sEmailAddress, $sFaxNumber, $iStatus, $iStationId, $sCompanyName, $sDesignation, $sCompanyWebsite, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[1] == 0) // Add Disabled
			return(1010);
			
        $iCustomerAddedBy = $objEmployee->iEmployeeId;
        $varNow = $objGeneral->fnNow();
		

		$sFirstName = $objDatabase->RealEscapeString($sFirstName);
        $sLastName = $objDatabase->RealEscapeString($sLastName);
        $sNICNumber = $objDatabase->RealEscapeString($sNICNumber);
        $sGender = $objDatabase->RealEscapeString($sGender);
        //$sDateOfBirth = $objDatabase->RealEscapeString($sDateOfBirth);
        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
		$sCountry = $objDatabase->RealEscapeString($sCountry);
		$sPhoneNumber = $objDatabase->RealEscapeString($sPhoneNumber);
		$sMobileNumber = $objDatabase->RealEscapeString($sMobileNumber);
		$sEmailAddress = $objDatabase->RealEscapeString($sEmailAddress);
		$sFaxNumber = $objDatabase->RealEscapeString($sFaxNumber);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		$sCompanyName = $objDatabase->RealEscapeString($sCompanyName);
		$sDesignation = $objDatabase->RealEscapeString($sDesignation);
		$sCompanyWebsite = $objDatabase->RealEscapeString($sCompanyWebsite);

        $varResult = $objDatabase->Query("INSERT INTO fms_customers
        (OrganizationId, FirstName, LastName, NICNumber, Gender, Address, City, State, ZipCode, Country, PhoneNumber, MobileNumber, EmailAddress, FaxNumber, Status, StationId, CompanyName, Designation, CompanyWebsite, Notes, CustomerAddedOn, CustomerAddedBy)
        VALUES ('" . cOrganizationId . "', '$sFirstName', '$sLastName', '$sNICNumber', '$sGender', '$sAddress', '$sCity', '$sState', '$sZipCode', '$sCountry', '$sPhoneNumber', '$sMobileNumber', '$sEmailAddress', '$sFaxNumber', '$iStatus', '$iStationId', '$sCompanyName', '$sDesignation', '$sCompanyWebsite', '$sNotes', '$varNow', '$iCustomerAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C WHERE C.OrganizationId='" . cOrganizationId . "' AND C.FirstName='$sFirstName' AND C.CustomerAddedOn='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {
			$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
			// Add Customer Photo
	       	include(cVSFFolder . '/classes/clsUploader.php');
			$up = new clsUploader();
			$sUploadPath = cDataFolder . '/vf/customerphotos/';
			$sAllowedTypes = array("image/jpeg", "image/pjpeg", "image/gif");
			$varPath = $objGeneral->dumpAssociativeArray($up->uploadTo($sUploadPath, true, $sAllowedTypes));
			if ($varPath != '')
			{
				$sFileName = explode('/', $varPath);
	   			$sFileName = $sFileName[count($sFileName)-1];
				$sExt = explode('.', $sFileName);
				$sExt = strtolower($sExt[count($sExt)-1]);

				$sNewFileName = $sUploadPath . $iCustomerId . '.' . $sExt;
				if (file_exists($sNewFileName)) unlink($sNewFileName);

	    		if (!rename($varPath, $sNewFileName))
	        		return(4001);

	        	// Make Thumbnail
		    	$iThumbnailHeight = $objEmployee->aSystemSettings["FMS_Customers_CustomerPhoto_Height"];
	    		$iThumbnailWidth = $objEmployee->aSystemSettings["FMS_Customers_CustomerPhoto_Width"];

				list($iImageWidth, $iImageHeight, $sImageType, $aImageAttr) = getimagesize($sNewFileName);
				$iImageRatio = $iImageWidth / $iImageHeight;
				
				if ($iImageRatio > 1)
					$iThumbnailHeight = number_format(($iThumbnailHeight * $iImageHeight) / $iImageWidth);
				else
					$iThumbnailWidth = number_format(($iThumbnailWidth * $iImageWidth) / $iImageHeight);

				include(cVSFFolder . '/classes/clsThumbnails.php');
		        $objThumbnail = new thumbs($sUploadPath);
		        $objThumbnail->fnCreateThumbnail($sUploadPath, $sUploadPath, $iCustomerId . '.' . strtolower($sExt), $iThumbnailWidth, $iThumbnailHeight, $iCustomerId . '.' . strtolower($sExt));
		    }
			
  			$objSystemLog->AddLog("Add New Customer - " . $sFirstName);

            $objGeneral->fnRedirect('?pagetype=details&error=8516&id=' . $iCustomerId);
        }
        else
            return(8517);
    }
	   
    function UpdateCustomer($iCustomerId, $sFirstName, $sLastName, $sNICNumber, $sGender, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sMobileNumber, $sEmailAddress, $sFaxNumber, $iStatus, $iStationId, $sCompanyName, $sDesignation, $sCompanyWebsite, $sNotes)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;

        // Employee Roles
	  	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[2] == 0) // Update Disabled
			return(1010);

        $sFirstName = $objDatabase->RealEscapeString($sFirstName);
        $sLastName = $objDatabase->RealEscapeString($sLastName);
        $sNICNumber = $objDatabase->RealEscapeString($sNICNumber);
        $sGender = $objDatabase->RealEscapeString($sGender);
        //$sDateOfBirth = $objDatabase->RealEscapeString($sDateOfBirth);
        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
		$sCountry = $objDatabase->RealEscapeString($sCountry);
		$sPhoneNumber = $objDatabase->RealEscapeString($sPhoneNumber);
		$sMobileNumber = $objDatabase->RealEscapeString($sMobileNumber);
		$sEmailAddress = $objDatabase->RealEscapeString($sEmailAddress);
		$sFaxNumber = $objDatabase->RealEscapeString($sFaxNumber);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		$sCompanyName = $objDatabase->RealEscapeString($sCompanyName);
		$sDesignation = $objDatabase->RealEscapeString($sDesignation);
		$sCompanyWebsite = $objDatabase->RealEscapeString($sCompanyWebsite);

       	$varResult = $objDatabase->Query("
        UPDATE fms_customers
        SET
            FirstName='$sFirstName',
            LastName='$sLastName',
            NICNumber='$sNICNumber',
            Gender='$sGender',			
			Address='$sAddress',
			City='$sCity',
            State='$sState',
            ZipCode='$sZipCode',
            Country='$sCountry',
            PhoneNumber='$sPhoneNumber',
			MobileNumber='$sMobileNumber',
            EmailAddress='$sEmailAddress',
			FaxNumber='$sFaxNumber',
            Status='$iStatus',
            StationId='$iStationId',
			CompanyName='$sCompanyName',
			Designation='$sDesignation',
			CompanyWebsite='$sCompanyWebsite',
            Notes='$sNotes'
        WHERE OrganizationId='" . cOrganizationId . "' AND CustomerId='$iCustomerId'");
		
		// Update Customer Photo
   		include(cVSFFolder . '/classes/clsUploader.php');
		$up = new clsUploader();
		$sUploadPath = cDataFolder . '/vf/customerphotos/';
		$sAllowedTypes = array("image/jpeg", "image/pjpeg", "image/gif");
		$varPath = $objGeneral->dumpAssociativeArray($up->uploadTo($sUploadPath, true, $sAllowedTypes));
		if ($varPath != '')
		{
			$sFileName = explode('/', $varPath);
			$sFileName = $sFileName[count($sFileName)-1];
			$sExt = explode('.', $sFileName);
			$sExt = strtolower($sExt[count($sExt)-1]);

			$sNewFileName = $sUploadPath . $iCustomerId . '.' . $sExt;
			if (file_exists($sNewFileName)) unlink($sNewFileName);

			if (!rename($varPath, $sNewFileName))
    			return(4001);

    		// Make Thumbnail
    		$iThumbnailHeight = $objEmployee->aSystemSettings["FMS_Customers_CustomerPhoto_Height"];
			$iThumbnailWidth = $objEmployee->aSystemSettings["FMS_Customers_CustomerPhoto_Width"];
			
            list($iImageWidth, $iImageHeight, $sImageType, $aImageAttr) = getimagesize($sNewFileName);
            $iImageRatio = $iImageWidth / $iImageHeight;
			
            if ($iImageRatio > 1)
                $iThumbnailHeight = number_format(($iThumbnailHeight * $iImageHeight) / $iImageWidth);
            else
                $iThumbnailWidth = number_format(($iThumbnailWidth * $iImageWidth) / $iImageHeight);
			
			include(cVSFFolder . '/classes/clsThumbnails.php');
			$objThumbnail = new thumbs($sUploadPath);
			$objThumbnail->fnCreateThumbnail($sUploadPath, $sUploadPath, $iCustomerId . '.' . strtolower($sExt), $iThumbnailWidth, $iThumbnailHeight, $iCustomerId . '.' . strtolower($sExt));
		}
		       	
		$objSystemLog->AddLog("Update Customer - " . $sCustomerName);
		$objGeneral->fnRedirect('../customers/customers.php?pagetype=details&error=8518&id=' . $iCustomerId);            
    }

    function DeleteCustomer($iCustomerId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
        
        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[3] == 0) // Delete Disabled
	    {
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iCustomerId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}
			return(1010);
		
		// Check Customers Transactions		
		if ($objDatabase->DBCount("fms_accounts_generaljournal_entries AS GJE", "GJE.CustomerId= '$iCustomerId'") > 0) return(8523);
		
       	$varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C WHERE C.OrganizationId='" . cOrganizationId . "' AND C.CustomerId='$iCustomerId'");
       	
       	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Customer Id');
		$sCustomerName = $objDatabase->Result($varResult, 0, "C.FirstName") . '' . $objDatabase->Result($varResult, 0, "C.LastName");

        $varResult = $objDatabase->Query("DELETE FROM fms_customers WHERE CustomerId='$iCustomerId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Delete Customer - " . $sCustomerName);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iCustomerId . '&error=8250');
			else
				$objGeneral->fnRedirect('?id=0&error=8250');
        }
        else
            return(8521);
    }
	
	function CustomersList()
	{
		global $objDatabase;

		$aCustomers = array();
		$varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C WHERE C.OrganizationId='" . cOrganizationId . "' AND C.Status='1' ");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sFirstName = $objDatabase->Result($varResult, $i, "C.FirstName");
			$sLastName = $objDatabase->Result($varResult, $i, "C.LastName");
			$iCustomerId = $objDatabase->Result($varResult, $i, "C.CustomerId");
			$sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");
			
			$aCustomers[$i][0] = $iCustomerId;
			$aCustomers[$i][1] = $sFirstName . ' ' . $sLastName . ' (' . $sCompanyName . ')';
			$aCustomers[$i][2] = $sFirstName . ' ' . $sLastName;
			
			//$sReturn .= $sFirstName . ' ' . $sLastName . ' (' . $sCompanyName . ')' . '| ' . $iCustomerId . "\n";
		}
		
		return($aCustomers);
	} 
	
	// Customers Ledger
	function ShowAllCustomersLedger($iCustomerId, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Customers[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$varResult = $objDatabase->Query("
		SELECT * FROM fms_customers AS C
		WHERE C.OrganizationId='" . cOrganizationId . "' AND C.CustomerId = '$iCustomerId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found...</div><br /><br />');
		$sCustomerName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName");
		
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = 'Ledger Details Of ' . $sCustomerName;
		
        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((GJ.GeneralJournalId LIKE '%$sSearch%') OR (GJ.TransactionDate LIKE '%$sSearch%') OR (GJE.Detail LIKE '%$sSearch%') OR (GJE.Debit LIKE '%$sSearch%') OR (GJE.Credit LIKE '%$sSearch%'))";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        if ($sSortBy == "") $sSortBy = "C.FirstName";

		$iTotalRecords = $objDatabase->DBCount("fms_customers AS C INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.CustomerId = C.CustomerId INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId", "C.CustomerId = '$iCustomerId' $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?id='. $iCustomerId . '&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_customers AS C
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.CustomerId = C.CustomerId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
        WHERE C.OrganizationId='" . cOrganizationId . "' AND C.CustomerId = '$iCustomerId' AND CA.ChartOfAccountsCategoryId !='5' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">		 
		 <td width="20%" align="left"><span class="WhiteHeading">Transaction Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Transaction Date in Ascending Order" title="Sort by Transaction Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Transaction Date in Descending Order" title="Sort by Transaction Date in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Detail&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Detail&sortorder="><img src="../images/sort_up.gif" alt="Sort by Detail in Ascending Order" title="Sort by Vendor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Detail&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Detail in Descending Order" title="Sort by Detail in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Debit&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Debit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Debit in Ascending Order" title="Sort by Debit in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Debit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Debit in Descending Order" title="Sort by Debit in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Credit&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Credit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Credit in Ascending Order" title="Sort by Debit in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Credit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Credit in Descending Order" title="Sort by Credit in Descending Order" border="0" /></a></span></td>		 
		</tr>
		';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iCustomerId = $objDatabase->Result($varResult, $i, "C.CustomerId");
			$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
			$dTransactionDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
			$sDetail = $objDatabase->Result($varResult, $i, "GJE.Detail");
			$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
			$dDebit = $objDatabase->Result($varResult, $i, "GJE.Debit");
			$dCredit = $objDatabase->Result($varResult, $i, "GJE.Credit");
						
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td align="left" valign="top">' . $sTransactionDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sDetail . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td> 
			 <td align="left" valign="top">' . number_format($dDebit, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . number_format($dCredit, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			</tr>';            
		}

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>		  
          <form method="GET" action=""><td align="left" colspan="2">Search for a Transaction:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" /><input type="hidden" name="id" id="id" value="' . $iCustomerId . '" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Vendor" title="Search for a Vendor" border="0"></td> </form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>';

		return($sReturn);
    }

}

?>