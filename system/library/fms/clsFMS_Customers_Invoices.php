<?php
// Class: Invoice
class clsCustomers_Invoices extends clsCustomers
{
	public $aPaymentTerms;
	public $aTaxGroup;
	public $aInvoiceStatus;
	// Class Constructor
	function __construct()
	{
		$this->aPaymentTerms = array("Monthly", "Quarterly", "Half-Yearly", "Annualy");
		$this->aTaxGroup = array("Non-Taxable Sales", "Out of State Sales", "Taxable Sales");
		$this->aInvoiceStatus = array("Pending", "Outstanding", "Cleared", "Cancelled");
	}
		
	function ShowAllInvoices($sSearch, $iInvoiceStatus = -1)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
			
		if ($iInvoiceStatus == '') $iInvoiceStatus = -1;
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Invoices";
		if ($sSortBy == "") $sSortBy = "CI.InvoiceId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((CI.TaxGroup LIKE '%$sSearch%') OR (CI.InvoiceId LIKE '%$sSearch%') OR (C.FirstName LIKE '%$sSearch%') OR (C.LastName LIKE '%$sSearch%') OR (CI.PaymentTerm LIKE '%$sSearch%') OR (CI.ShippingTerm LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (C.CompanyName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}
		if ($iInvoiceStatus >= 0) $sStatusCondition = " AND CI.STATUS='$iInvoiceStatus'";

		$iTotalRecords = $objDatabase->DBCount("fms_customers_invoices AS CI INNER JOIN fms_customers AS C ON C.CustomerId = CI.CustomerId INNER JOIN organization_employees AS E ON E.EmployeeId = CI.InvoiceAddedBy", "CI.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sStatusCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_customers_invoices AS CI
		INNER JOIN fms_customers AS C ON C.CustomerId = CI.CustomerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = CI.InvoiceAddedBy
		WHERE CI.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sStatusCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Invoice Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CI.InvoiceDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Invoice Date in Ascending Order" title="Sort by Invoice Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CI.InvoiceDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Invoice Date in Descending Order" title="Sort by Invoice Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Customer&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Customer Name in Ascending Order" title="Sort by Customer Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Customer Name in Descending Order" title="Sort by Customer Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Company&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Company Name in Ascending Order" title="Sort by Company Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Company Name in Descending Order" title="Sort by Company Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Invoice Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CI.InvoiceNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Invoice Number in Ascending Order" title="Sort by Invoice Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CI.InvoiceNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Invoice Number in Descending Order" title="Sort by Invoice Number in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CI.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CI.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Total Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CI.TotalAmount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Total amount in Ascending Order" title="Sort by Total amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CI.TotalAmount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Total amount in Descending Order" title="Sort by Total amount in Descending Order" border="0" /></a></span></td>
		  <td width="6%" colspan="6"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						
			$iInvoiceId = $objDatabase->Result($varResult, $i, "CI.InvoiceId");
			$sInvoiceNo = $objDatabase->Result($varResult, $i, "CI.InvoiceNo");
			$iCustomerId = $objDatabase->Result($varResult, $i, "CI.CustomerId");
			$sCustomerFirstName = $objDatabase->Result($varResult, $i, "C.FirstName");
			$sCustomerLastName = $objDatabase->Result($varResult, $i, "C.LastName");
			$sCustomerName = $sCustomerFirstName . ' ' . $sCustomerLastName;			
			$iQuotationId = $objDatabase->Result($varResult, $i, "CI.QuotationId");
			$dTotalAmount = $objDatabase->Result($varResult, $i, "CI.TotalAmount");
			$sTotalAmount = number_format($dTotalAmount, 0);
			$dTotalPayment = $objDatabase->Result($varResult, $i, "CI.TotalPayment");
			$sTotalPayment = number_format($dTotalPayment, 0);
			
			$sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");
			
			$iStatus = $objDatabase->Result($varResult, $i, "CI.Status");
			$sStatus = $this->aInvoiceStatus[$iStatus];
			
			$dBalance = $objDatabase->Result($varResult, $i, "CI.Balance");			
			$sBalance = number_format($dBalance, 0);
			
			$dInvoiceDate = $objDatabase->Result($varResult, $i, "CI.InvoiceDate");
			$sInvoiceDate = date("F j, Y", strtotime($dInvoiceDate));
			
			$sEditInvoice = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Invoice\', \'../customers/invoices.php?pagetype=details&action2=edit&id=' . $iInvoiceId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Invoice Details" title="Edit this Invoice Details"></a></td>';
			$sDeleteInvoice = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Invoice?\')) {window.location = \'?action=DeleteInvoice&id=' . $iInvoiceId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Invoice" title="Delete this Invoice"></a></td>';
			$sInvoiceReceipt = '<td class="GridTD" align="center"><a href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=CustomerReports&reporttype=Invoice&selEmployee=-1&selStation=-1&txtId=' . $iInvoiceId . '\', 800,600);"><img src="../images/icons/iconPrint2.gif" border="0" alt="Invoice Receipt" title="Invoice Receipt"></a></td>';			
			$sDocuments = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/documents_show.php?componentname=Customers_Invoices&id=' . $iInvoiceId . '\', \'Documents of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sInvoiceNo)) . '\', \'700 420\');"><img src="../images/icons/save.gif" border="0" alt="Invoice Documents" title="Invoice Documents"></a></td>';
			$sMakeReceipt = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Make Receipt of ' . $objDatabase->RealEscapeString(stripslashes($sInvoiceNo)) . '\', \'../customers/receipts.php?pagetype=details&action2=addnew&invoiceid=' . $iInvoiceId . '\', \'520px\', true);"><img src="../images/icons/iconRight.gif" border="0" alt="Make Receipt" title="Make Receipt"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[2] == 0)	$sEditQuotation = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[3] == 0)	$sDeleteQuotation = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices_Documents[0] == 0)	$sDocuments = '<td class="GridTD">&nbsp;</td>';	
				
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sInvoiceDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCustomerName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCompanyName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sInvoiceNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . $sTotalAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \' View Invoice details\', \'../customers/invoices.php?pagetype=details&id=' . $iInvoiceId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Invoice Details" title="View this Invoice Details"></a></td>
			 ' . $sEditInvoice . $sMakeReceipt. $sInvoiceReceipt. $sDocuments . $sDeleteInvoice . '</tr>';
		}

		$sAddInvoice = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Invoice\', \'../customers/invoices.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Invoice">';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[1] == 0) // Add Disabled
			$sAddInvoice = '';
		
		$sInvoiceStatusSelect = '<select class="form1" name="selInvoiceStatus" onchange="window.location=\'?status=\'+GetSelectedListBox(\'selInvoiceStatus\');" id="selInvoiceStatus">
		<option value="-1">Invoices with All Status</option>';
		for ($i=0; $i < count($this->aInvoiceStatus); $i++)
			$sInvoiceStatusSelect .= '<option ' . (($iInvoiceStatus== $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aInvoiceStatus[$i] . '</option>';
		$sInvoiceStatusSelect .= '</select>';
			
			
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddInvoice . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for an Invoice:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for an Invoice" title="Search for an Invoice" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td colspan="2">' .  $sInvoiceStatusSelect . '</td>
		 </tr>	
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Invoice Details" alt="View this Invoice Details"></td><td>View this Invoice Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Invoice Details" alt="Edit this Invoice Details"></td><td>Edit this Invoice Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconRight.gif" border="0" alt="Make Receipt" title="Make Receipt"></td><td>Make Receipt</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconPrint2.gif" border="0" alt="Print Invoice" title="Print Invoice"></td><td>Print Invoice</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconFolder.gif" border="0" alt="Invoice Documents" title="Invoice Documents"></td><td>Invoice Documents</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Invoice" alt="Delete this Invoice"></td><td>Delete this Invoice</td></tr>
		</table>';

		return($sReturn);
	}
	
	function InvoiceDetails($iInvoiceId, $iSalesOrderId=0, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;		
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
				
		$objjQuery = new clsjQuery();
		$objCustomers = new clsCustomers_Customers();
		$aCustomers = $objCustomers->CustomersList();
				
		$objQueryCustomer = new clsjQuery($aCustomers);
				
		$k = 0;
				
		$varResult = $objDatabase->Query("
		SELECT *,CI.TotalAmount AS TotalAmount,CI.Description AS Description, CI.Notes AS Notes
		FROM fms_customers_invoices AS CI
		INNER JOIN organization_employees AS E ON E.EmployeeId = CI.InvoiceAddedBy
		LEFT JOIN fms_customers_salesorders AS SO ON SO.SalesOrderId = CI.SalesOrderId
		INNER JOIN fms_customers AS C ON C.CustomerId = CI.CustomerId
		WHERE CI.OrganizationId='" . cOrganizationId . "' AND CI.InvoiceId = '$iInvoiceId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iInvoiceId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Invoice" title="Edit this Invoice" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Invoice?\')) {window.location = \'?pagetype=details&action=DeleteInvoice&id=' . $iInvoiceId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Invoice" title="Delete this Invoice" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[3] == 0)	$sButtons_Delete = '';
			
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Invoice Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				
				$iInvoiceId = $objDatabase->Result($varResult, 0, "CI.InvoiceId");
				$iSalesOrderId = $objDatabase->Result($varResult, 0, "CI.SalesOrderId");
				$iCustomerId = $objDatabase->Result($varResult, 0, "CI.CustomerId");
				$iInvoiceAddedBy = $objDatabase->Result($varResult, 0, "CI.InvoiceAddedBy");
								
				$sInvoiceAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0 , "E.LastName");
				$sInvoiceAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sInvoiceAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iInvoiceAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sCustomerFirstName = $objDatabase->Result($varResult, 0, "C.FirstName");
				$sCustomerLastName = $objDatabase->Result($varResult, 0, "C.LastName");				
				$sCustomerName = $sCustomerFirstName . ' ' . $sCustomerLastName;
				$sCustomerFullName = $sCustomerName;				
				$sCustomerName = $sCustomerName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sCustomerName)) . '\', \'../customers/customers.php?pagetype=details&id=' . $iCustomerId . '\', \'520px\', true);">&nbsp;<img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Customer Information" title="Customer Information" border="0" /></a><br />';
				
				$sInvoiceNo = $objDatabase->Result($varResult, 0, "CI.InvoiceNo");
				$sInvoiceNo = $objDatabase->RealEscapeString($sInvoiceNo);
				$sInvoiceNo = stripslashes($sInvoiceNo);
				
				if($iSalesOrderId > 0)
				{					
					$sSalesOrderNo = $objDatabase->Result($varResult, 0, "SO.OrderNo");					
					$sSalesOrderNo = $objDatabase->RealEscapeString($sSalesOrderNo);
					$sSalesOrderNo = stripslashes($sSalesOrderNo);
					$sSalesOrderNo = $sSalesOrderNo . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View ' . $sSalesOrderNo . '\', \'../customers/salesorders.php?pagetype=details&salesorderid=' . $iSalesOrderId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Sales Order Information" title="Sales Order Information" border="0" /></a><br />';
				}
				else $sSalesOrderNo = "No Sales Order";
								
				$dTotalPayment = $objDatabase->Result($varResult, 0, "CI.TotalPayment");
				$sTotalPayment = number_format($dTotalPayment, 0);
				
				$dTotalAmount = $objDatabase->Result($varResult, 0, "TotalAmount");
				$sTotalAmount = number_format($dTotalAmount, 0);
				
				$dInvoiceDate = $objDatabase->Result($varResult, 0, "CI.InvoiceDate");
				$sInvoiceDate = date("F j, Y", strtotime($dInvoiceDate));
				
				$iStatus = $objDatabase->Result($varResult, 0, "CI.Status");
				$sStatus = $this->aInvoiceStatus[$iStatus];
				
				$sDescription = $objDatabase->Result($varResult, 0, "Description");
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
								
				$dInvoiceAddedOn = $objDatabase->Result($varResult, 0, "CI.InvoiceAddedOn");
				$sInvoiceAddedOn = date("F j, Y", strtotime($dInvoiceAddedOn)) . ' at ' . date("g:i a", strtotime($dInvoiceAddedOn));
				
				// Data Grid Items
				$varResult2 = $objDatabase->Query("
				SELECT *
				FROM fms_customers_invoices_items AS II
				WHERE II.InvoiceId = '$iInvoiceId'");
				$aGridData = '';
				if ($objDatabase->RowsNumber($varResult2) > 0)
				{
					$iGridRows = $objDatabase->RowsNumber($varResult2);
					for ($i=0; $i < $iGridRows; $i++)
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						$sRowVisible = 'visible';
										
						$sProductName = $objDatabase->Result($varResult2, $i, "II.ProductName");
						$iQuantity = $objDatabase->Result($varResult2, $i, "II.Quantity");
						$dUnitPrice = $objDatabase->Result($varResult2, $i, "II.UnitPrice");
						$dAmount = $objDatabase->Result($varResult2, $i, "II.Amount");
						
						$sOrderParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . ($i+1) . '</td>					 
 						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;">' . $sProductName . '</td>					 					 
						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . $iQuantity . '</td>
						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>
						</tr>';
					}
					$sInvoiceItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
					 <tr class="GridTR">
					  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
					  <td><span class="WhiteHeading">Item</span></td>				  				  
					  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
					  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
					  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
					 </tr>
					' . $sOrderParticulars_Rows . '
					 <tr bgcolor="#ffffff">
					  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #000000;">Total:</td>
					  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #000000; font-family:Tahoma, Arial; font-size:18px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</td>
					 </tr>
					</table>';					
					//$sInvoiceItems = $objDHTMLSuite->DataGrid($aGridColumns, $aGridColumns_Width, $aGridColumns_Alignment, $aGridColumns_CellFormat, $aGridData, $iGridRows, 5, "98%", "", false, false);								
				}			
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';				
				
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
		        
				$sInvoiceDate = '<input type="text" size="10" id="txtInvoiceDate" name="txtInvoiceDate" class="form1" value="' . $dInvoiceDate . '" />' . $objjQuery->Calendar('txtInvoiceDate') . '&nbsp;<span style="color:red;">*</span>';
				$sInvoiceNo = '<input type="text" name="txtInvoiceNo" id="txtInvoiceNo" class="form1" value="' . $sInvoiceNo . '" size="30" />';
				
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aInvoiceStatus); $i++)
				{
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aInvoiceStatus[$i];
				}
				$sStatus .='</select>';
							
		        $sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				// Data Grid				
				//$sInvoiceItems = $objDHTMLSuite->DataGrid($aGridColumns, $aGridColumns_Width, $aGridColumns_Alignment, $aGridColumns_CellFormat, $aGridData, $iGridRows, 5, "98%", "", true, false);
				//$sInvoiceItems .= '<br /><a href="#noanchor" onclick="AddNewRow();"><img align="absmiddle" src="../images/icons/plus.gif" border="0" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a>&nbsp;&nbsp;<a href="#noanchor" onclick="DeleteRow();"><img align="absmiddle" src="../images/icons/minus.gif" border="0" alt="Delete Row" title="Delete Row" />&nbsp;Delete Row</a>';
				//$sInvoiceItems .= '<input type="hidden" name="hdnInvoiceItems" id="hdnInvoiceItems" value="" />';
				
				$sOrderParticulars_Rows = "";
				$varResult2 = $objDatabase->Query("SELECT * 
				FROM fms_customers_invoices_items AS II
				WHERE II.InvoiceId= '$iInvoiceId'");
						       	
		        for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';			
					
					$sProductName = $objDatabase->Result($varResult2, $k, "II.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $k, "II.Quantity");
					$dUnitPrice = $objDatabase->Result($varResult2, $k, "II.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $k, "II.Amount");					
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%"  /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');"  type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">' . number_format($dAmount, 0) . '</div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				for ($j = $k; $j < 50; $j++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'none';
										
					$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="4" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td>
					</tr>';
				}
				
				$sInvoiceItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				  ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>				  
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #000000;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</div></td>
				 </tr>				 
				</table>
				<input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="' . $dTotalAmount . '" />
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $j . ';
				 var iRowVisible = ' . ($k +1) . ';
				</script>';
			}
			else if ($sAction2 == "addnew")
			{
				$iInvoiceId = "";
				$iSalesOrderId = $objGeneral->fnGet("salesorderid");
				$sTaxGroup = $objGeneral->fnGet("txtTaxGroup");
				$sPaymentTerm = $objGeneral->fnGet("txtPaymentTerm");
				$sShippingTerm = $objGeneral->fnGet("txtShippingTerm");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$sInvoiceNo = $objGeneral->fnGet("txtInvoiceNo");
								
				$iStatus = 1;
								
				$dInvoiceDate = date("Y-m-d");
                $dDeliveryDate= date("Y-m-d");

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sInvoiceAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sInvoiceAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sInvoiceAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sInvoiceDate = '<input type="text" size="10" id="txtInvoiceDate" name="txtInvoiceDate" class="form1" value="' . $dInvoiceDate . '" />' . $objjQuery->Calendar('txtInvoiceDate') . '&nbsp;<span style="color:red;">*</span>';				
				$sSalesOrderNo = '<select name="selSalesOrder" class="form1" onchange="window.location=\'?pagetype=details&action2=addnew&salesorderid=\'+GetSelectedListBox(\'selSalesOrder\');" id="selSalesOrder">
				<option value="0"> Select Sales Order </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_customers_salesorders AS SO WHERE SO.OrganizationId='" . cOrganizationId . "' AND SO.Status='2' ORDER BY SO.OrderNo");
				if($objDatabase->RowsNumber($varResult) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sSalesOrderNo .='<option ' . (($iSalesOrderId == $objDatabase->Result($varResult, $i, "SO.SalesOrderId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "SO.SalesOrderId") . '" >' . $objDatabase->Result($varResult, $i, "SO.OrderNo") . '</option>';
				}				
				$sSalesOrderNo .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selSalesOrder\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selSalesOrder\'), \'../customers/salesorders.php?pagetype=details&salesorderid=\'+GetSelectedListBox(\'selSalesOrder\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Sales Order Details" title="Sales Order Details" /></a>';
				
				if($iSalesOrderId > 0)
				{
					$varResult = $objDatabase->Query("
					SELECT *
					FROM fms_customers_salesorders AS SO
					INNER JOIN fms_customers AS C ON C.CustomerId = SO.CustomerId
					WHERE SO.OrganizationId='" . cOrganizationId . "' AND SO.SalesOrderId= '$iSalesOrderId'");
					$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
					$sCustomerFullName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName");
				}
				
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
				
				// Get Last Invoice Id
				$varResult = $objDatabase->Query("SELECT * FROM fms_customers_invoices AS CI ORDER BY CI.InvoiceId DESC LIMIT 1");
				if ($objDatabase->RowsNumber($varResult) > 0)
					$iInvoiceId = $objDatabase->Result($varResult, 0, "CI.InvoiceId");
				$iInvoiceId++;
								
		        $sInvoiceNo = $objEmployee->aSystemSettings['FMS_Customers_Invoice'];
				$sInvoiceNo = str_replace('[Year]', date("Y"), $sInvoiceNo);
				$sInvoiceNo = str_replace('[Month]', date("m"), $sInvoiceNo);
				$sInvoiceNo = str_replace('[Id]', $iInvoiceId, $sInvoiceNo);
				
				$sInvoiceNo = '<input type="text" name="txtInvoiceNo" id="txtInvoiceNo" class="form1" value="' . $sInvoiceNo . '" size="30" />';
				
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';
				for($i = 0; $i < count($this->aInvoiceStatus); $i++)
				{
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . 'value="' . $i . '">' . $this->aInvoiceStatus[$i];
				}
				$sStatus .='</select>';
				
		        //$sDescription = '<textarea name="txtDescription" id="txtDescription" class="form1" rows="5" cols="60">' . $sDescription . '</textarea>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sInvoiceAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				// Data Grid
				//$sInvoiceItems = $objDHTMLSuite->DataGrid($aGridColumns, $aGridColumns_Width, $aGridColumns_Alignment, $aGridColumns_CellFormat, $aGridData, 6, 5, "98%", "", true, false);
				//$sInvoiceItems .= '<br /><a href="#noanchor" onclick="AddNewRow();"><img align="absmiddle" src="../images/icons/plus.gif" border="0" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a>&nbsp;&nbsp;<a href="#noanchor" onclick="DeleteRow();"><img align="absmiddle" src="../images/icons/minus.gif" border="0" alt="Delete Row" title="Delete Row" />&nbsp;Delete Row</a>';
				//$sInvoiceItems .= '<input type="hidden" name="hdnInvoiceItems" id="hdnInvoiceItems" value="" />';
				
				if($iSalesOrderId > 0)
				{
					$varResult = $objDatabase->Query("
					SELECT *
					FROM fms_customers_salesorders AS SO
					INNER JOIN fms_customers_salesorders_items AS SOI ON SOI.SalesOrderId = SO.SalesOrderId
					WHERE SO.SalesOrderId= '$iSalesOrderId'");
					for($k = 0; $k < $objDatabase->RowsNumber($varResult); $k++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';					
						$sRowVisible = 'visible';						
						$sProductName = $objDatabase->Result($varResult, $k, "SOI.ProductName");					
						$iQuantity= $objDatabase->Result($varResult, $k, "SOI.Quantity");
						$dUnitPrice = $objDatabase->Result($varResult, $k, "SOI.UnitPrice");
						$dAmount = $objDatabase->Result($varResult, $k, "SOI.Amount");
						$dTotalAmount += $dAmount;
												
						$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td> 
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">'. $dAmount . '</div></td>
						 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
						</tr>';
					}
					for ($j = $k; $j < 50; $j++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						$sRowVisible = 'none';
						
						$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="3" /></td>
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
						 <!--<td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td>-->
						</tr>';
					}
					
					$sOrderParticulars_Rows .= '
					<!--
					<tr bgcolor="#ffffff">
					 <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
					</tr>
					-->
					<tr bgcolor="#ffffff">
					 <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
					 <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">'. number_format($dTotalAmount, 0) . '</div></td>
					</tr>
					</table>
					<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="'. $dTotalAmount . '" />
					<script type="text/javascript" language="JavaScript">
					 var iRowCounter = ' . $j . ';
					 var iRowVisible = ' . ($k +1) . ';
					</script>';
					
				}
				else
				{
					for ($k=0; $k < 50; $k++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						$sRowVisible = (($k <= 4) ? 'visible' : 'none');
						
						$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" size="3" /></td> 
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '"></div></td>
						 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td>
						</tr>';
					}
					
					$sOrderParticulars_Rows .= '
					<tr bgcolor="#ffffff">
					 <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
					</tr>
					<tr bgcolor="#ffffff">
					 <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
					 <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount"></div></td>
					</tr>
					</table>
					<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="" />
					<script type="text/javascript" language="JavaScript">
					 var iRowCounter = ' . $k . ';
					 var iRowVisible = 6;	
					</script>';
				}
					
					$sInvoiceItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
					 <tr class="GridTR">
					  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
					  <td><span class="WhiteHeading">Item</span></td>				  				  
					  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
					  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
					  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
					  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
					 </tr>
					 ' . $sOrderParticulars_Rows . '';
				
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">			
			function ValidateForm()
			{
				if (GetVal(\'hdnCustomerId\') == "") return(AlertFocus(\'Please enter a valid Customer Name\', \'txtCustomerName\'));
				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Invoice Information:</span></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200px;">Sales Order No:</td><td><strong>' . $sSalesOrderNo . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Customer Name:</td><td><strong>' . $sCustomerName . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Invoice No:</td><td><strong>' . $sInvoiceNo . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Invoice Date:</td><td><strong>' . $sInvoiceDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Status:</td><td><strong>' . $sStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Invoice Items:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">' . $sInvoiceItems . '</td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Invoice Added on:</td><td><strong>' . $sInvoiceAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Invoice Added By:</td><td><strong>' . $sInvoiceAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Invoice" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iInvoiceId . '"><input type="hidden" name="action" id="action" value="UpdateInvoice"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Invoice" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewInvoice"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewInvoice ($iCustomerId, $iSalesOrderId, $sInvoiceNo, $dInvoiceDate, $dTotalAmount, $sDescription, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[1] == 0) // Add Disabled
			return(1010);

		$varNow = $objGeneral->fnNow();
		$iInvoiceAddedBy = $objEmployee->iEmployeeId;
		
		/*
		if($dTotalPayment == "") $dTotalPayment = 0;		
		if($dTotalAmount == "") $dTotalAmount = 0;		
		if($dTotalPayment > $dTotalAmount);	
		{
			$dTotalPayment_diff = $dTotalPayment - $dTotalAmount;
			$dTotalPayment = $dTotalPayment - $dTotalPayment_diff;
		}
		*/
		
		$sInvoiceNo = $objDatabase->RealEscapeString($sInvoiceNo);		
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		//if($objDatabase->DBCount("fms_customers_invoices AS CI", "CI.InvoiceNo= '$sInvoiceNo'") > 0) return(8606);
		
		// Check for Incoice against specified Sales Order
		if($iSalesOrderId > 0)
		{	
			if($objDatabase->DBCount("fms_customers_invoices AS CI", "CI.OrganizationId='" . cOrganizationId . "' AND CI.SalesOrderId = '$iSalesOrderId'") > 0) return(8607);			
			// Get Customer Id from Quotation			
			$varResult = $objDatabase->Query("
			SELECT *
			FROM fms_customers_salesorders AS SO
			INNER JOIN fms_customers AS C ON C.CustomerId = SO.CustomerId
			WHERE SO.SalesOrderId= '$iSalesOrderId'");
			$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
		}	

		$varResult = $objDatabase->Query("INSERT INTO fms_customers_invoices
		(OrganizationId, CustomerId, SalesOrderId, InvoiceNo, TotalAmount, InvoiceDate, Description, Status, Notes, InvoiceAddedOn, InvoiceAddedBy)
		VALUES ('" . cOrganizationId . "', '$iCustomerId', '$iSalesOrderId', '$sInvoiceNo', '$dTotalAmount', '$dInvoiceDate', '$sDescription', '$iStatus', '$sNotes', '$varNow', '$iInvoiceAddedBy')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_customers_invoices AS CI WHERE CI.OrganizationId='" . cOrganizationId . "' AND CI.CustomerId='$iCustomerId' AND CI.InvoiceAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iInvoiceId = $objDatabase->Result($varResult, 0, "CI.InvoiceId");
				// Add Invoice Items				
				for ($k=0; $k < 50; $k++)
				{					
					$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));
					$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
					$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
					$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
					
					if(($sProductName != "") && ($iProductQty > 0))
						$this->AddInvoiceItems($iInvoiceId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
					
				}							
				
				$objSystemLog->AddLog("Add New Invoice  - " . $iCustomerId);

				$objGeneral->fnRedirect('?pagetype=details&error=8600&id=' . $objDatabase->Result($varResult, 0, "CI.InvoiceId"));
			}
		}

		return(8601);
	}
	
	function UpdateInvoice ($iInvoiceId, $iCustomerId, $iSalesOrderId, $sInvoiceNo, $dInvoiceDate, $dTotalAmount, $sDescription, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[2] == 0) // Update Disabled
			return(1010);
		/*
		if($dTotalPayment == "") $dTotalPayment = 0;		
		if($dTotalAmount == "") $dTotalAmount = 0;		
		if($dTotalPayment > $dTotalAmount);	
		{
			$dTotalPayment_diff = $dTotalPayment - $dTotalAmount;
			$dTotalPayment = $dTotalPayment - $dTotalPayment_diff;
		}
		*/
		
		$sInvoiceNo = $objDatabase->RealEscapeString($sInvoiceNo);		
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_customers_invoices AS CI", "CI.OrganizationId='" . cOrganizationId . "' AND CI.InvoiceNo= '$sInvoiceNo' AND CI.InvoiceId <> '$iInvoiceId'") > 0) return(8608);
		// Check for Incoice against specified Sales Order
		if($iSalesOrderId > 0)
		{	
			if($objDatabase->DBCount("fms_customers_invoices AS CI", "CI.OrganizationId='" . cOrganizationId . "' AND CI.InvoiceId <> '$iInvoiceId' AND CI.SalesOrderId = '$iSalesOrderId'") > 0) return(8607);			
			// Get Customer Id from Quotation			
			$varResult = $objDatabase->Query("
			SELECT *
			FROM fms_customers_salesorders AS SO
			INNER JOIN fms_customers AS C ON C.CustomerId = SO.CustomerId
			WHERE SO.SalesOrderId= '$iSalesOrderId'");
			$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
		}

		$varResult = $objDatabase->Query("
		UPDATE fms_customers_invoices 
		SET	
			CustomerId='$iCustomerId',			
			TotalAmount='$dTotalAmount',
			InvoiceNo='$sInvoiceNo', 
			InvoiceDate='$dInvoiceDate', 
			Description='$sDescription',
			Status='$iStatus',
			Notes='$sNotes' 			
		WHERE OrganizationId='" . cOrganizationId . "' AND InvoiceId='$iInvoiceId'");
				
		$varResult = $objDatabase->Query("DELETE FROM fms_customers_invoices_items WHERE InvoiceId='$iInvoiceId'");
		// Adding Invoice Items
		for ($k=0; $k < 50; $k++)
		{					
			$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));
			$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
			$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
			$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
			
			if(($sProductName != "") && ($iProductQty > 0))
				$this->AddInvoiceItems($iInvoiceId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
			
		}
		
		$objSystemLog->AddLog("Update Invoice - " . $iInvoiceId);		
		$objGeneral->fnRedirect('../customers/invoices.php?pagetype=details&error=8602&id=' . $iInvoiceId);		
	}
	
	function DeleteInvoice($iInvoiceId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Invoices[3] == 0) // Delete Disabled
	    {
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iInvoiceId . '&error=1010');
			else
				$objGeneral->fnRedirect('?pagetype=invoices&id=0&error=1010');
		}
		$varResult = $objDatabase->Query("SELECT * FROM fms_customers_invoices AS CI WHERE CI.OrganizationId='" . cOrganizationId . "' AND CI.InvoiceId='$iInvoiceId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(8605);		

		$varResult = $objDatabase->Query("DELETE FROM fms_customers_invoices WHERE InvoiceId='$iInvoiceId'");
		if ($varResult)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_customers_invoices_items WHERE InvoiceId='$iInvoiceId'");
			
			$objSystemLog->AddLog("Delete Invoice - " . $iInvoiceId);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iInvoiceId . '&error=8604');
			else
				$objGeneral->fnRedirect('?id=0&error=8604');							
		}
		else
			return(8605);
	}
	
	// Add Invoice Items
	function AddInvoiceItems($iInvoiceId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount)
	{
		global $objDatabase;
		global $objGeneral;
		
		$sProductName = $objDatabase->RealEscapeString($sProductName);
		if($iProductQty == "" ) $iProductQty = 0;
		if($dProductUnitPrice == "" ) $dProductUnitPrice = 0;
		if($dProductTotalAmount == "" ) $dProductTotalAmount = 0;
		
		$varResult2 = $objDatabase->Query("INSERT INTO fms_customers_invoices_items
		(InvoiceId, ProductName, Quantity, UnitPrice, Amount)
		VALUES ('$iInvoiceId', '$sProductName', '$iProductQty', '$dProductUnitPrice', '$dProductTotalAmount')");
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
		
		
	}
	
	function SendInvoices()
	{
		global $objDatabase;
		global $objGeneral;
		
		$varToday = date("Y-m-d");
		
		include('../../system/library/fms/clsFMS_Reports.php');
		$objReports = new clsFMS_Reports();
		$objCustomerReports = new clsFMS_Reports_CustomerReports();
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_customers_invoices AS I 
		INNER JOIN fms_customers AS C ON C.CustomerId = I.CustomerId
		WHERE CI.OrganizationId='" . cOrganizationId . "' AND I.Status='1' AND I.InvoiceDate <= '$varToday'
		ORDER BY I.InvoiceId DESC");
		
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sCustomerName = $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName");
			$sCustomerEmail = $objDatabase->Result($varResult, $i, "C.EmailAddress");
			
			$iInvoiceId = $objDatabase->Result($varResult, $i, "I.InvoiceId");
			$sInvoiceNumber = $objDatabase->Result($varResult, $i, "I.InvoiceNo");
			
			$sSubject = "Your pending invoice from " . cCompanyName;
			$sInvoice = $objReports->GenerateReportHeader("Job") . $objCustomerReports->Invoice(false, $sTemp, $iInvoiceId, true);
			
			$sInvoice = $sInvoice . '<br /><br /><hr size="1" /><div align="center" style="font-family:Tahoma, Arial; font-size:11px; color:#777777">This is an auto-generated invoice - This is not a subscription e-mail, it cannot be unsubscribed.<br />The only way to stop this email, is by making full payment to the service provider.</div>';
						
			if ($sCustomerEmail != "")
			{
				$objGeneral->fnSendMail(cFromEmailName, cFromEmail, $sCustomerName, $sCustomerEmail, $sSubject, $sInvoice);
				$sResult .= "Invoice #$sInvoiceNumber sent to $sCustomerName &lt;$sCustomerEmail&gt;<br />";
				$sResultLog = date("F j, Y g:i a") . " - Invoice #$sInvoiceNumber sent to $sCustomerName <$sCustomerEmail>\n";
			}
			else
			{
				$sResult .= "Invoice #$sInvoiceNumber was not sent to $sCustomerName [ No E-mail Address Found ]<br />";
				$sResultLog = date("F j, Y g:i a") . " - Invoice #$sInvoiceNumber was not sent to $sCustomerName [ No E-mail Address Found ]\n";
			}

			$varFileHandle = @fopen('../../system/logs/WeeklyJobs.log','a');
			$varBytes = @fwrite($varFileHandle, $sResultLog);
			fclose($varFileHandle);
		}

		print($sResult);
		$objGeneral->fnSendMail(cFromEmailName, cFromEmail, cFromEmail, cFromEmail, "Automatic Invoices Job", $sResult);
	}
}

class clsCustomers_RecurringInvoices extends clsCustomers
{
	public $aPaymentTerms;
	public $aTaxGroup;
	public $aRecurringInvoiceStatus;
	// Class Constructor
	function __construct()
	{
		$this->aPaymentTerms = array("Monthly", "Quarterly", "Half-Yearly", "Annualy");
		$this->aTaxGroup = array("Non-Taxable Sales", "Out of State Sales", "Taxable Sales");
		$this->aRecurringInvoiceStatus = array("In-active", "Active");
	}
		
	function ShowAllRecurringInvoices($sSearch, $iRecurringInvoiceStatus = -1)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
			
		if ($iRecurringInvoiceStatus == '') $iRecurringInvoiceStatus = -1;

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Recurring Invoices";
		if ($sSortBy == "") $sSortBy = "RI.RecurringInvoiceId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((RI.RecurringInvoiceId LIKE '%$sSearch%') OR (RI.Title LIKE '%$sSearch%')OR (C.FirstName LIKE '%$sSearch%') OR (C.LastName LIKE '%$sSearch%')OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (C.CompanyName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}
		if ($iRecurringInvoiceStatus >= 0) $sStatusCondition = " AND RI.STATUS='$iRecurringInvoiceStatus'";

		$iTotalRecords = $objDatabase->DBCount("fms_customers_invoices_recurringinvoices AS RI INNER JOIN fms_customers AS C ON C.CustomerId = RI.CustomerId INNER JOIN organization_employees AS E ON E.EmployeeId = RI.RecurringInvoiceAddedBy", "1=1 $sSearchCondition $sStatusCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=recurringinvoices&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_customers_invoices_recurringinvoices AS RI
		INNER JOIN fms_customers AS C ON C.CustomerId = RI.CustomerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = RI.RecurringInvoiceAddedBy
		WHERE 1=1 $sSearchCondition $sStatusCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=RI.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=RI.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by RecurringInvoice Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Customer&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Customer Name in Ascending Order" title="Sort by Customer Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Customer Name in Descending Order" title="Sort by Customer Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Company&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Company Name in Ascending Order" title="Sort by Company Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Company Name in Descending Order" title="Sort by Company Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=RI.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=RI.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="6%" colspan="6"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						
			$iRecurringInvoiceId = $objDatabase->Result($varResult, $i, "RI.RecurringInvoiceId");
			$sTitle = $objDatabase->Result($varResult, $i, "RI.Title");
			$iCustomerId = $objDatabase->Result($varResult, $i, "RI.CustomerId");
			$sCustomerFirstName = $objDatabase->Result($varResult, $i, "C.FirstName");
			$sCustomerLastName = $objDatabase->Result($varResult, $i, "C.LastName");
			$sCustomerName = $sCustomerFirstName . ' ' . $sCustomerLastName;			
			$sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");			
			$iStatus = $objDatabase->Result($varResult, $i, "RI.Status");
			$sStatus = $this->aRecurringInvoiceStatus[$iStatus];
			
			$sEditRecurringInvoice = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Recurring Invoice\', \'../customers/invoices.php?pagetype=recurringinvoices_details&action2=edit&id=' . $iRecurringInvoiceId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Recurring Invoice Details" title="Edit this Recurring Invoice Details"></a></td>';
			$sDeleteRecurringInvoice = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Recurring Invoice?\')) {window.location = \'?action=DeleteRecurringInvoice&id=' . $iRecurringInvoiceId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Recurring Invoice" title="Delete this Recurring Invoice"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[2] == 0)	$sEditQuotation = '';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[3] == 0)	$sDeleteQuotation = '';
			
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCustomerName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCompanyName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>			 
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \' View Recurring Invoice details\', \'../customers/invoices.php?pagetype=recurringinvoices_details&id=' . $iRecurringInvoiceId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Recurring Invoice Details" title="View this Recurring Invoice Details"></a></td>
			 ' . $sEditRecurringInvoice . $sDeleteRecurringInvoice . '</tr>';
		}

		$sAddRecurringInvoice = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Recurring Invoice\', \'../customers/invoices.php?pagetype=recurringinvoices_details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add">';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[1] == 0) // Add Disabled
			$sAddRecurringInvoice = '';
		
		$sRecurringInvoiceStatusSelect = '<select class="form1" name="selRecurringInvoiceStatus" onchange="window.location=\'?pagetype=recurringinvoices&status=\'+GetSelectedListBox(\'selRecurringInvoiceStatus\');" id="selRecurringInvoiceStatus">
		<option value="-1">Recurring Invoices with All Status</option>';
		for ($i=0; $i < count($this->aRecurringInvoiceStatus); $i++)
			$sRecurringInvoiceStatusSelect .= '<option ' . (($iRecurringInvoiceStatus== $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aRecurringInvoiceStatus[$i] . '</option>';
		$sRecurringInvoiceStatusSelect .= '</select>';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddRecurringInvoice . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for an RecurringInvoice:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Recurring Invoice" title="Search for a Recurring Invoice" border="0"><input type="hidden" name="pagetype" id="pagetype" value="recurringinvoices" /></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td colspan="2">' .  $sRecurringInvoiceStatusSelect . '</td>
		 </tr>	
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Recurring Invoice Details" alt="View this Recurring Invoice Details"></td><td>View this Recurring Invoice Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Recurring Invoice Details" alt="Edit this Recurring Invoice Details"></td><td>Edit this Recurring Invoice Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Recurring Invoice" alt="Delete this Recurring Invoice"></td><td>Delete this Recurring Invoice</td></tr>
		</table>';

		return($sReturn);
	}
	
	function RecurringInvoiceDetails($iRecurringInvoiceId, $iSalesOrderId=0, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;		
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objCustomers = new clsCustomers_Customers();
		$aCustomers = $objCustomers->CustomersList();
						
		$objQueryCustomer = new clsjQuery($aCustomers);
		
		$k = 0;
		
		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_customers_invoices_recurringinvoices AS RI
		INNER JOIN organization_employees AS E ON E.EmployeeId = RI.RecurringInvoiceAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = RI.CustomerId
		WHERE RI.RecurringInvoiceId = '$iRecurringInvoiceId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=recurringinvoices_details&action2=edit&id=' . $iRecurringInvoiceId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this RecurringInvoice" title="Edit this RecurringInvoice" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this RecurringInvoice?\')) {window.location = \'?action=DeleteRecurringInvoice&id=' . $iRecurringInvoiceId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this RecurringInvoice" title="Delete this RecurringInvoice" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[2] == 0)
   			$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[3] == 0)
			$sButtons_Delete = '';
			
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Recurring Invoice Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iRecurringInvoiceId = $objDatabase->Result($varResult, 0, "RI.RecurringInvoiceId");				
				$iCustomerId = $objDatabase->Result($varResult, 0, "RI.CustomerId");
				$iRecurringInvoiceAddedBy = $objDatabase->Result($varResult, 0, "RI.RecurringInvoiceAddedBy");								
				$sRecurringInvoiceAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0 , "E.LastName");
				$sRecurringInvoiceAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sRecurringInvoiceAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iRecurringInvoiceAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sCustomerFirstName = $objDatabase->Result($varResult, 0, "C.FirstName");
				$sCustomerLastName = $objDatabase->Result($varResult, 0, "C.LastName");				
				$sCustomerName = $sCustomerFirstName . ' ' . $sCustomerLastName;
				$sCustomerFullName = $sCustomerName;				
				$sCustomerName = $sCustomerName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sCustomerName)) . '\', \'../customers/customers.php?pagetype=details&id=' . $iCustomerId . '\', \'520px\', true);">&nbsp;<img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Customer Information" title="Customer Information" border="0" /></a><br />';
				
				$sTitle = $objDatabase->Result($varResult, 0, "RI.Title");
				$sTitle = $objDatabase->RealEscapeString($sTitle);
				$sTitle = stripslashes($sTitle);
				$iStatus = $objDatabase->Result($varResult, 0, "RI.Status");
				$sStatus = $this->aRecurringInvoiceStatus[$iStatus];
				
				$sDescription = $objDatabase->Result($varResult, 0, "RI.Description");
								
				$dRecurringInvoiceAddedOn = $objDatabase->Result($varResult, 0, "RI.RecurringInvoiceAddedOn");
				$sRecurringInvoiceAddedOn = date("F j, Y", strtotime($dRecurringInvoiceAddedOn)) . ' at ' . date("g:i a", strtotime($dRecurringInvoiceAddedOn));
				$iInvoiceDateOfMonth = $objDatabase->Result($varResult, 0, "RI.InvoiceDateOfMonth");
				$sInvoiceDateOfMonth = $iInvoiceDateOfMonth;
				$iInvoiceMonthOfYear_1 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_1");				
				$sInvoiceMonthOfYear_1 = (($iInvoiceMonthOfYear_1 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');				
				$iInvoiceMonthOfYear_2 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_2");
				$sInvoiceMonthOfYear_2 = (($iInvoiceMonthOfYear_2 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_3 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_3");
				$sInvoiceMonthOfYear_3 = (($iInvoiceMonthOfYear_3 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_4 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_4");
				$sInvoiceMonthOfYear_4 = (($iInvoiceMonthOfYear_4 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_5 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_5");
				$sInvoiceMonthOfYear_5 = (($iInvoiceMonthOfYear_5 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_6 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_6");
				$sInvoiceMonthOfYear_6 = (($iInvoiceMonthOfYear_6 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_7 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_7");
				$sInvoiceMonthOfYear_7 = (($iInvoiceMonthOfYear_7 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_8 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_8");
				$sInvoiceMonthOfYear_8 = (($iInvoiceMonthOfYear_8 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_9 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_9");
				$sInvoiceMonthOfYear_9 = (($iInvoiceMonthOfYear_9 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_10 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_10");
				$sInvoiceMonthOfYear_10 = (($iInvoiceMonthOfYear_10 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_11 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_11");
				$sInvoiceMonthOfYear_11 = (($iInvoiceMonthOfYear_11 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				$iInvoiceMonthOfYear_12 = $objDatabase->Result($varResult, 0, "RI.InvoiceMonthOfYear_12");
				$sInvoiceMonthOfYear_12 = (($iInvoiceMonthOfYear_12 == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				
				// Data Grid Items
				$varResult2 = $objDatabase->Query("
				SELECT *
				FROM fms_customers_invoices_recurringinvoices_items AS II
				WHERE II.RecurringInvoiceId = '$iRecurringInvoiceId'");
				$aGridData = '';
				if ($objDatabase->RowsNumber($varResult2) > 0)
				{
					$iGridRows = $objDatabase->RowsNumber($varResult2);
					for ($i=0; $i < $iGridRows; $i++)
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						$sRowVisible = 'visible';
																
						$sProductName = $objDatabase->Result($varResult2, $i, "II.ProductName");
						$iQuantity = $objDatabase->Result($varResult2, $i, "II.Quantity");
						$dUnitPrice = $objDatabase->Result($varResult2, $i, "II.UnitPrice");
						$dAmount = $objDatabase->Result($varResult2, $i, "II.Amount");						
						$dTotalAmount += $dAmount;
						
						$sOrderParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . ($i+1) . '</td>					 
 						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;">' . $sProductName . '</td>					 					 
						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . $iQuantity . '</td>
						 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>
						</tr>';
					}
					$sRecurringInvoiceItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
					 <tr class="GridTR">
					  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
					  <td><span class="WhiteHeading">Item</span></td>				  				  
					  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
					  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
					  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
					 </tr>
					' . $sOrderParticulars_Rows . '
					 <tr bgcolor="#ffffff">
					  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #000000;">Total:</td>
					  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #000000; font-family:Tahoma, Arial; font-size:18px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</td>
					 </tr>
					</table>';					
					//$sRecurringInvoiceItems = $objDHTMLSuite->DataGrid($aGridColumns, $aGridColumns_Width, $aGridColumns_Alignment, $aGridColumns_CellFormat, $aGridData, $iGridRows, 5, "98%", "", false, false);								
				}			
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';				
				
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
		        				
				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aRecurringInvoiceStatus); $i++)
				{
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aRecurringInvoiceStatus[$i];
				}
				$sStatus .='</select>';
				
				$sInvoiceDateOfMonth = '<input type="text" name="txtInvoiceDateOfMonth" id="txtInvoiceDateOfMonth" class="form1" value="' . $sInvoiceDateOfMonth . '" size="5" />&nbsp;<span style="color:red;">*</span>';
				$sInvoiceMonthOfYear_1 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_1 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_1" id="chkInvoiceMonthOfYear_1" value="on" />';
				$sInvoiceMonthOfYear_2 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_2 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_2" id="chkInvoiceMonthOfYear_2" value="on" />';
				$sInvoiceMonthOfYear_3 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_3 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_3" id="chkInvoiceMonthOfYear_3" value="on" />';
				$sInvoiceMonthOfYear_4 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_4 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_4" id="chkInvoiceMonthOfYear_4" value="on" />';
				$sInvoiceMonthOfYear_5 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_5 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_5" id="chkInvoiceMonthOfYear_5" value="on" />';
				$sInvoiceMonthOfYear_6 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_6 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_6" id="chkInvoiceMonthOfYear_6" value="on" />';
				$sInvoiceMonthOfYear_7 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_7 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_7" id="chkInvoiceMonthOfYear_7" value="on" />';
				$sInvoiceMonthOfYear_8 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_8 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_8" id="chkInvoiceMonthOfYear_8" value="on" />';
				$sInvoiceMonthOfYear_9 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_9 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_9" id="chkInvoiceMonthOfYear_9" value="on" />';
				$sInvoiceMonthOfYear_10 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_10 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_10" id="chkInvoiceMonthOfYear_10" value="on" />';
				$sInvoiceMonthOfYear_11 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_11 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_11" id="chkInvoiceMonthOfYear_11" value="on" />';
				$sInvoiceMonthOfYear_12 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_12 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_12" id="chkInvoiceMonthOfYear_12" value="on" />';
								
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				
				$sOrderParticulars_Rows = "";
				$varResult2 = $objDatabase->Query("SELECT * 
				FROM fms_customers_invoices_recurringinvoices_items AS II
				WHERE II.RecurringInvoiceId= '$iRecurringInvoiceId'");
						       	
		        for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';			
					
					$sProductName = $objDatabase->Result($varResult2, $k, "II.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $k, "II.Quantity");
					$dUnitPrice = $objDatabase->Result($varResult2, $k, "II.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $k, "II.Amount");					
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%"  /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');"  type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">' . number_format($dAmount, 0) . '</div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				for ($j = $k; $j < 50; $j++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'none';
										
					$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="4" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td>
					</tr>';
				}
				
				$sRecurringInvoiceItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				  ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>				  
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #000000;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</div></td>
				 </tr>				 
				</table>
				<input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="' . $dTotalAmount . '" />
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $j . ';
				 var iRowVisible = ' . ($k +1) . ';
				</script>';
			}
			else if ($sAction2 == "addnew")
			{
				$iRecurringInvoiceId = "";
				$sDescription = $objGeneral->fnGet("txtDescription");								
				$iStatus = 1;
								
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sRecurringInvoiceAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sRecurringInvoiceAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sRecurringInvoiceAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
				
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';
				for($i = 0; $i < count($this->aRecurringInvoiceStatus); $i++)
				{
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . 'value="' . $i . '">' . $this->aRecurringInvoiceStatus[$i];
				}
				$sStatus .='</select>';
				
				$sInvoiceDateOfMonth = '<input type="text" name="txtInvoiceDateOfMonth" id="txtInvoiceDateOfMonth" class="form1" value="' . $sInvoiceDateOfMonth . '" size="5" />&nbsp;<span style="color:red;">*</span>';
				$sInvoiceMonthOfYear_1 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_1 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_1" id="chkInvoiceMonthOfYear_1" value="on" />';
				$sInvoiceMonthOfYear_2 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_2 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_2" id="chkInvoiceMonthOfYear_2" value="on" />';
				$sInvoiceMonthOfYear_3 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_3 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_3" id="chkInvoiceMonthOfYear_3" value="on" />';
				$sInvoiceMonthOfYear_4 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_4 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_4" id="chkInvoiceMonthOfYear_4" value="on" />';
				$sInvoiceMonthOfYear_5 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_5 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_5" id="chkInvoiceMonthOfYear_5" value="on" />';
				$sInvoiceMonthOfYear_6 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_6 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_6" id="chkInvoiceMonthOfYear_6" value="on" />';
				$sInvoiceMonthOfYear_7 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_7 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_7" id="chkInvoiceMonthOfYear_7" value="on" />';
				$sInvoiceMonthOfYear_8 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_8 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_8" id="chkInvoiceMonthOfYear_8" value="on" />';
				$sInvoiceMonthOfYear_9 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_9 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_9" id="chkInvoiceMonthOfYear_9" value="on" />';
				$sInvoiceMonthOfYear_10 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_10 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_10" id="chkInvoiceMonthOfYear_10" value="on" />';
				$sInvoiceMonthOfYear_11 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_11 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_11" id="chkInvoiceMonthOfYear_11" value="on" />';
				$sInvoiceMonthOfYear_12 = '<input type="checkbox" ' . (($iInvoiceMonthOfYear_12 == 1) ? 'checked="true"' : '') . ' name="chkInvoiceMonthOfYear_12" id="chkInvoiceMonthOfYear_12" value="on" />';
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sRecurringInvoiceAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));				
				
				for ($k=0; $k < 50; $k++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = (($k <= 4) ? 'table-row' : 'none');
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" size="3" /></td> 
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
					</tr>';
				}				
				
				$sRecurringInvoiceItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				 ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount"></div></td>
				 </tr>
				</table>
				<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="" />
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $k . ';
				 var iRowVisible = 6;	
				</script>';
			
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">			
			function ValidateForm()
			{
				if (GetVal(\'txtTitle\') == "") return(AlertFocus(\'Please enter a valid Title\', \'txtTitle\'));
				if (GetVal(\'hdnCustomerId\') == "") return(AlertFocus(\'Please enter a valid Customer Name\', \'txtCustomerName\'));
				if (!isNumeric(GetVal("txtInvoiceDateOfMonth"))) return(AlertFocus(\'Please enter a Valid Date of Month\', \'txtInvoiceDateOfMonth\'));
				iInvoiceDateOfMonth = GetVal("txtInvoiceDateOfMonth");
				if(iInvoiceDateOfMonth <=0 || iInvoiceDateOfMonth > 31) return(AlertFocus(\'Date of Month must be between 1 and 31\', \'txtInvoiceDateOfMonth\'));
				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Recurring Invoice Information:</span></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200px;">Title:</td><td><strong>' . $sTitle . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Customer Name:</td><td><strong>' . $sCustomerName . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Date Of Month:</td><td><strong>' . $sInvoiceDateOfMonth . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">January:</td><td><strong>' . $sInvoiceMonthOfYear_1 . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Feburary:</td><td><strong>' . $sInvoiceMonthOfYear_2 . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">March:</td><td><strong>' . $sInvoiceMonthOfYear_3 . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">April:</td><td><strong>' . $sInvoiceMonthOfYear_4 . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">May:</td><td><strong>' . $sInvoiceMonthOfYear_5 . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">June:</td><td><strong>' . $sInvoiceMonthOfYear_6 . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">July:</td><td><strong>' . $sInvoiceMonthOfYear_7 . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">August:</td><td><strong>' . $sInvoiceMonthOfYear_8 . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">September:</td><td><strong>' . $sInvoiceMonthOfYear_9 . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">October:</td><td><strong>' . $sInvoiceMonthOfYear_10 . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">November:</td><td><strong>' . $sInvoiceMonthOfYear_11 . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">December:</td><td><strong>' . $sInvoiceMonthOfYear_12 . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Status:</td><td><strong>' . $sStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">RecurringInvoice Items:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">' . $sRecurringInvoiceItems . '</td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top">RecurringInvoice Added on:</td><td><strong>' . $sRecurringInvoiceAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">RecurringInvoice Added By:</td><td><strong>' . $sRecurringInvoiceAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iRecurringInvoiceId . '"><input type="hidden" name="action" id="action" value="UpdateRecurringInvoice"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add " class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewRecurringInvoice"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewRecurringInvoice ($sTitle, $iCustomerId, $iInvoiceDateOfMonth, $iInvoiceMonthOfYear_1, $iInvoiceMonthOfYear_2, $iInvoiceMonthOfYear_3, $iInvoiceMonthOfYear_4, $iInvoiceMonthOfYear_5, $iInvoiceMonthOfYear_6, $iInvoiceMonthOfYear_7, $iInvoiceMonthOfYear_8, $iInvoiceMonthOfYear_9, $iInvoiceMonthOfYear_10, $iInvoiceMonthOfYear_11, $iInvoiceMonthOfYear_12, $sDescription, $iStatus)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[1] == 0) // Add Disabled
			return(1010);

		$varNow = $objGeneral->fnNow();
		$iRecurringInvoiceAddedBy = $objEmployee->iEmployeeId;
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);		
		
		if($iInvoiceDateOfMonth <= 0 || $iInvoiceDateOfMonth > 31) return(8616);
		if($iCustomerId == "") $iCustomerId = 0;
		$iInvoiceMonthOfYear_1 = ( ($iInvoiceMonthOfYear_1 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_2 = (($iInvoiceMonthOfYear_2 == 'on' )? 1 : 0);
		$iInvoiceMonthOfYear_3 = (($iInvoiceMonthOfYear_3 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_4 = (($iInvoiceMonthOfYear_4 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_5 = (($iInvoiceMonthOfYear_5 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_6 = (($iInvoiceMonthOfYear_6 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_7 = (($iInvoiceMonthOfYear_7 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_8 = (($iInvoiceMonthOfYear_8 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_9 = (($iInvoiceMonthOfYear_9 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_10 = (($iInvoiceMonthOfYear_10 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_11 = (($iInvoiceMonthOfYear_11 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_12 = (($iInvoiceMonthOfYear_12 == 'on') ? 1 : 0);
		
		$varResult = $objDatabase->Query("INSERT INTO fms_customers_invoices_recurringinvoices
		(Title, CustomerId, Description, InvoiceDateOfMonth, InvoiceMonthOfYear_1, InvoiceMonthOfYear_2, InvoiceMonthOfYear_3, InvoiceMonthOfYear_4, InvoiceMonthOfYear_5, InvoiceMonthOfYear_6, InvoiceMonthOfYear_7, InvoiceMonthOfYear_8, InvoiceMonthOfYear_9, InvoiceMonthOfYear_10, InvoiceMonthOfYear_11, InvoiceMonthOfYear_12, Status, RecurringInvoiceAddedOn, RecurringInvoiceAddedBy)
		VALUES ('$sTitle', '$iCustomerId', '$sDescription', '$iInvoiceDateOfMonth', '$iInvoiceMonthOfYear_1', '$iInvoiceMonthOfYear_2', '$iInvoiceMonthOfYear_3', '$iInvoiceMonthOfYear_4', '$iInvoiceMonthOfYear_5', '$iInvoiceMonthOfYear_6', '$iInvoiceMonthOfYear_7', '$iInvoiceMonthOfYear_8', '$iInvoiceMonthOfYear_9', '$iInvoiceMonthOfYear_10', '$iInvoiceMonthOfYear_11', '$iInvoiceMonthOfYear_12','$iStatus', '$varNow', '$iRecurringInvoiceAddedBy')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_customers_invoices_recurringinvoices AS RI WHERE RI.CustomerId='$iCustomerId' AND RI.RecurringInvoiceAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iRecurringInvoiceId = $objDatabase->Result($varResult, 0, "RI.RecurringInvoiceId");
				// Add RecurringInvoice Items				
				for ($k=0; $k < 50; $k++)
				{					
					$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));
					$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
					$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
					$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
					
					if(($sProductName != "") && ($iProductQty > 0))
						$this->AddRecurringInvoiceItems($iRecurringInvoiceId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
					
				}
				
				$objSystemLog->AddLog("Add New RecurringInvoice  - " . $sTitle);

				$objGeneral->fnRedirect('?pagetype=recurringinvoices_details&error=8610&id=' . $objDatabase->Result($varResult, 0, "RI.RecurringInvoiceId"));
			}
		}

		return(8611);
	}
	
	function UpdateRecurringInvoice ($iRecurringInvoiceId, $sTitle, $iCustomerId, $iInvoiceDateOfMonth, $iInvoiceMonthOfYear_1, $iInvoiceMonthOfYear_2, $iInvoiceMonthOfYear_3, $iInvoiceMonthOfYear_4, $iInvoiceMonthOfYear_5, $iInvoiceMonthOfYear_6, $iInvoiceMonthOfYear_7, $iInvoiceMonthOfYear_8, $iInvoiceMonthOfYear_9, $iInvoiceMonthOfYear_10, $iInvoiceMonthOfYear_11, $iInvoiceMonthOfYear_12, $sDescription, $iStatus)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[2] == 0) // Update Disabled
			return(1010);
		
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		
		if($iInvoiceDateOfMonth <= 0 || $iInvoiceDateOfMonth > 31) return(8616);
		if($iCustomerId == "") $iCustomerId = 0;
		$iInvoiceMonthOfYear_1 = ( ($iInvoiceMonthOfYear_1 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_2 = (($iInvoiceMonthOfYear_2 == 'on' )? 1 : 0);
		$iInvoiceMonthOfYear_3 = (($iInvoiceMonthOfYear_3 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_4 = (($iInvoiceMonthOfYear_4 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_5 = (($iInvoiceMonthOfYear_5 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_6 = (($iInvoiceMonthOfYear_6 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_7 = (($iInvoiceMonthOfYear_7 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_8 = (($iInvoiceMonthOfYear_8 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_9 = (($iInvoiceMonthOfYear_9 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_10 = (($iInvoiceMonthOfYear_10 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_11 = (($iInvoiceMonthOfYear_11 == 'on') ? 1 : 0);
		$iInvoiceMonthOfYear_12 = (($iInvoiceMonthOfYear_12 == 'on') ? 1 : 0);		
		
		$varResult = $objDatabase->Query("
		UPDATE fms_customers_invoices_recurringinvoices 
		SET	
			Title='$sTitle',
			CustomerId='$iCustomerId',			
			Description='$sDescription',
			InvoiceDateOfMonth='$iInvoiceDateOfMonth',
			InvoiceMonthOfYear_1='$iInvoiceMonthOfYear_1',
			InvoiceMonthOfYear_2='$iInvoiceMonthOfYear_2',
			InvoiceMonthOfYear_3='$iInvoiceMonthOfYear_3',
			InvoiceMonthOfYear_4='$iInvoiceMonthOfYear_4',
			InvoiceMonthOfYear_5='$iInvoiceMonthOfYear_5',
			InvoiceMonthOfYear_6='$iInvoiceMonthOfYear_6',
			InvoiceMonthOfYear_7='$iInvoiceMonthOfYear_7',
			InvoiceMonthOfYear_8='$iInvoiceMonthOfYear_8',
			InvoiceMonthOfYear_9='$iInvoiceMonthOfYear_9',
			InvoiceMonthOfYear_10='$iInvoiceMonthOfYear_10',
			InvoiceMonthOfYear_11='$iInvoiceMonthOfYear_11',
			InvoiceMonthOfYear_12='$iInvoiceMonthOfYear_12',
			Status='$iStatus'			
		WHERE RecurringInvoiceId='$iRecurringInvoiceId'");
		
		$varResult = $objDatabase->Query("DELETE FROM fms_customers_invoices_recurringinvoices_items WHERE RecurringInvoiceId='$iRecurringInvoiceId'");
		// Adding RecurringInvoice Items
		for ($k=0; $k < 50; $k++)
		{					
			$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));
			$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
			$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
			$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
			
			if(($sProductName != "") && ($iProductQty > 0))
				$this->AddRecurringInvoiceItems($iRecurringInvoiceId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
			
		}
		
		$objSystemLog->AddLog("Update RecurringInvoice - " . $sTitle);		
		$objGeneral->fnRedirect('../customers/invoices.php?pagetype=recurringinvoices_details&error=8612&id=' . $iRecurringInvoiceId);		
	}
	
	function DeleteRecurringInvoice($iRecurringInvoiceId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		global $objGeneral;
		
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_RecurringInvoices[3] == 0) // Delete Disabled
			return(1010);

		$varResult = $objDatabase->Query("SELECT * FROM fms_customers_invoices_recurringinvoices AS RI WHERE RI.RecurringInvoiceId='$iRecurringInvoiceId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(8605);		

		$varResult = $objDatabase->Query("DELETE FROM fms_customers_invoices_recurringinvoices WHERE RecurringInvoiceId='$iRecurringInvoiceId'");
		if ($varResult)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_customers_invoices_recurringinvoices_items WHERE RecurringInvoiceId='$iRecurringInvoiceId'");
			
			$objSystemLog->AddLog("Delete RecurringInvoice - " . $iRecurringInvoiceId);
			
			$objGeneral->fnRedirect('../customers/invoices.php?pagetype=recurringinvoices&error=8614&id=' . $iRecurringInvoiceId);
			//return(8614);
		}
		else
			return(8615);
	}
	
	// Add RecurringInvoice Items
	function AddRecurringInvoiceItems($iRecurringInvoiceId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount)
	{
		global $objDatabase;
		global $objGeneral;
		
		$sProductName = $objDatabase->RealEscapeString($sProductName);
		if($iProductQty == "" ) $iProductQty = 0;
		if($dProductUnitPrice == "" ) $dProductUnitPrice = 0;
		if($dProductTotalAmount == "" ) $dProductTotalAmount = 0;
		
		$varResult2 = $objDatabase->Query("INSERT INTO fms_customers_invoices_recurringinvoices_items
		(RecurringInvoiceId, ProductName, Quantity, UnitPrice, Amount)
		VALUES ('$iRecurringInvoiceId', '$sProductName', '$iProductQty', '$dProductUnitPrice', '$dProductTotalAmount')");
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
		
		
	}
}

?>