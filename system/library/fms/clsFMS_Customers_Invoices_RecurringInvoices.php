<?php

// Class: Invoice
class clsCustomers_Invoices_RecurringInvoices extends clsCustomers
{
	
	function GenerateInvoices($dDate)
	{
		global $objDatabase;
		global $objGeneral;

        $varResult = $objDatabase->Query("SELECT * FROM organization_settings AS S WHERE S.SettingName='FMS_Customers_Invoice'");
		if ($objDatabase->RowsNumber($varResult) > 0)
        	$sInvoiceNoFromSettings = $objDatabase->Result($varResult, 0, "S.SettingValue");

		$iInvoiceAddedBy = 1;			// System Administrator;
		$varNow = $objGeneral->fnNow();
		$iInvoiceStatus = 1;			// Outstanding Invoice
		
		$iDateOfMonth = date("d", strtotime($dDate));
		$iMonthOfYear = date("n", strtotime($dDate));
		
		$sMonthName = date("F", strtotime($dDate));
		$sYear = date("Y", strtotime($dDate));
		$sNextYear = $sYear + 1;

		// RI.InvoiceDateOfMonth='$iDateOfMonth' AND 
				
		$varResult = $objDatabase->Query("
		SELECT * FROM fms_customers_invoices_recurringinvoices RI 
		WHERE 
			RI.InvoiceMonthOfYear_" . $iMonthOfYear . "='1' AND
			RI.Status='1'");
		
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iRecurringInvoiceId = $objDatabase->Result($varResult, $i, "RI.RecurringInvoiceId");
			$iCustomerId = $objDatabase->Result($varResult, $i, "RI.CustomerId");
			$sTitle = $objDatabase->Result($varResult, $i, "RI.Title");
			$sDescription = $objDatabase->Result($varResult, $i, "RI.Description");
			$iInvoiceDateOfMonth = $objDatabase->Result($varResult, $i, "RI.InvoiceDateOfMonth");
			
			// Get Last Invoice Id
			$varResult2 = $objDatabase->Query("SELECT * FROM fms_customers_invoices AS CI ORDER BY CI.InvoiceId DESC LIMIT 1");
			if ($objDatabase->RowsNumber($varResult2) > 0)
				$iInvoiceId = $objDatabase->Result($varResult2, 0, "CI.InvoiceId");
			
			$iInvoiceId++;

			$sInvoiceNo = $sInvoiceNoFromSettings;
			$sInvoiceNo = str_replace('[Year]', date("Y"), $sInvoiceNo);
			$sInvoiceNo = str_replace('[Month]', date("m"), $sInvoiceNo);
			$sInvoiceNo = str_replace('[Id]', $iInvoiceId, $sInvoiceNo);
			
			$dInvoiceDate = date("Y-m-" . $iInvoiceDateOfMonth, strtotime($dDate));	// Date
			
			$dTotalAmount = 0;
	
			$varResult2 = $objDatabase->Query("INSERT INTO fms_customers_invoices 
			(CustomerId, InvoiceNo, TotalAmount, InvoiceDate, Description, Status, InvoiceAddedOn, InvoiceAddedBy) VALUES 
			('$iCustomerId', '$sInvoiceNo', '$dTotalAmount', '$dInvoiceDate', '$sDescription', '$iInvoiceStatus', '$varNow', '$iInvoiceAddedBy')");
			
			$varResult2 = $objDatabase->Query("SELECT * FROM fms_customers_invoices AS I WHERE I.CustomerId='$iCustomerId' AND I.InvoiceNo='$sInvoiceNo' AND I.InvoiceAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult2) > 0) 
			{
				$iInvoiceId = $objDatabase->Result($varResult2, 0, "I.InvoiceId");
				
				// Items
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_customers_invoices_recurringinvoices_items AS RII WHERE RII.RecurringInvoiceId='$iRecurringInvoiceId'");
				for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
				{
					$sProductName = $objDatabase->Result($varResult2, $j, "RII.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $j, "RII.Quantity");
					$dUnitPrice = $objDatabase->Result($varResult2, $j, "RII.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $j, "RII.Amount");
					
					$sProductName = str_replace("[CURRENT_MONTH]", $sMonthName, $sProductName);
					$sProductName = str_replace("[CURRENT_YEAR]", $sYear, $sProductName);
					$sProductName = str_replace("[NEXT_YEAR]", $sNextYear, $sProductName);
				
					$dTotalAmount += $dAmount;
					$varResult3 = $objDatabase->Query("INSERT INTO fms_customers_invoices_items 
					(InvoiceId, ProductName, Quantity, UnitPrice, Amount) 
					VALUES 
					('$iInvoiceId', '$sProductName', '$iQuantity', '$dUnitPrice', '$dAmount')");
				}
	
				$varResult2 = $objDatabase->Query("UPDATE fms_customers_invoices SET TotalAmount='$dTotalAmount' WHERE InvoiceId='$iInvoiceId'");
				$sContents = 'Invoice Created on ' . date("F j, Y g:i a") . ' [ Invoice Id: ' . $iInvoiceId . ' ] [ Total Amount: ' . $dTotalAmount . ' ]';
				
				print($sContents . '<br />');
				// Update Daily Jobs Log
				$varFileHandle = @fopen('../../system/logs/DailyJobs.job','a');
				$varBytes = @fwrite($varFileHandle, $sContents);
				fclose($varFileHandle);
			}
		}
	}
}
?>