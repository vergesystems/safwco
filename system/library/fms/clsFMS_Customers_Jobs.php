<?php

// Class: Job
class clsCustomers_Jobs
{
	public $aJobStatus;
	// Class Constructor
	function __construct()
	{
		$this->aJobStatus = array("Not-Started", "In-Progress", "Completed");
	}
	
	function ShowAllJobs($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
			
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Jobs";
		if ($sSortBy == "") $sSortBy = "CJ.JobId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((CJ.JobId LIKE '%$sSearch%') OR (CJ.Title LIKE '%$sSearch%') OR (CJ.Supervisor LIKE '%$sSearch%') OR (CJ.Description LIKE '%$sSearch%') OR (CJ.Notes LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_customers_jobs AS CJ INNER JOIN organization_employees AS E ON E.EmployeeId = CJ.JobAddedBy", "CJ.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_customers_jobs AS CJ
		INNER JOIN organization_employees AS E ON E.EmployeeId = CJ.JobAddedBy
		WHERE CJ.OrganizationId='" . cOrganizationId . "' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Supervisor&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CJ.Supervisor&sortorder="><img src="../images/sort_up.gif" alt="Sort by Supervisor in Ascending Order" title="Sort by Supervisor in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CJ.Supervisor&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Supervisor in Descending Order" title="Sort by Supervisor in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CJ.Title&sortorder="><img src="../images/sort_up.gif" alt="Sort by Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CJ.Title&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Start Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CJ.StartDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Start Date in Ascending Order" title="Sort by Start Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CJ.StartDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Start Date in Descending Order" title="Sort by Start Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">End Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CJ.EndDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by End Date in Ascending Order" title="Sort by End Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CJ.EndDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by End Date in Descending Order" title="Sort by End Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=CJ.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=CJ.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';			
			$iJobId = $objDatabase->Result($varResult, $i, "CJ.JobId");
			$sSupervisor = $objDatabase->Result($varResult, $i, "CJ.Supervisor");
			$sTitle = $objDatabase->Result($varResult, $i, "CJ.Title");
			$dStartDate = $objDatabase->Result($varResult, $i, "CJ.StartDate");
			$sStartDate = date("F j, Y", strtotime($dStartDate));
			
			$dEndDate = $objDatabase->Result($varResult, $i, "CJ.EndDate");
			$sEndDate = date("F j, Y", strtotime($dEndDate));
			
			$iJobStatus = $objDatabase->Result($varResult, $i, "CJ.Status");
			$sJobStatus = $this->aJobStatus[$iJobStatus];
						
			$sEditJob = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sTitle)) . '\', \'../customers/jobs.php?pagetype=details&action2=edit&id=' . $iJobId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Job Details" title="Edit this Job Details"></a></td>';
			$sDeleteJob = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Job?\')) {window.location = \'?action=DeleteJob&id=' . $iJobId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Job" title="Delete this Job"></a></td>';
			
			if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[2] == 0)	$sEditJob = '<td class="GridTD">&nbsp;</td>';				
			if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[3] == 0)	$sDeleteJob = '<td class="GridTD">&nbsp;</td>';
				
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sSupervisor . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStartDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sEndDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sJobStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $objDatabase->RealEscapeString(stripslashes($sTitle)) . '\', \'../customers/jobs.php?pagetype=details&id=' . $iJobId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Job Details" title="View this Job Details"></a></td>
			' . $sEditJob . $sDeleteJob . '</tr>';
		}

		$sAddJob = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Job\', \'../customers/jobs.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Job">';
		
		if($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[1] == 0)
			$sAddJob = '';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddJob . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Job:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Job" title="Search for a Job" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Job Details" alt="View this Job Details"></td><td>View this Job Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Job Details" alt="Edit this Job Details"></td><td>Edit this Job Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Job" alt="Delete this Job"></td><td>Delete this Job</td></tr>
		</table>';

		return($sReturn);
	}
	
	function JobDetails($iJobId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();
		
		$objCustomers = new clsCustomers_Customers();
		$aCustomers = $objCustomers->CustomersList();
				
		$objQueryCustomer = new clsjQuery($aCustomers);
		

		$varResult = $objDatabase->Query("
		SELECT *,CJ.Notes AS Notes
		FROM fms_customers_jobs AS CJ
		INNER JOIN fms_customers AS C ON C.CustomerId = CJ.CustomerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = CJ.JobAddedBy		
		WHERE CJ.OrganizationId='" . cOrganizationId . "' AND CJ.JobId = '$iJobId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iJobId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Job" title="Edit this Job" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Job?\')) {window.location = \'?pagetype=details&action=DeleteJob&id=' . $iJobId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Job" title="Delete this Job" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[3] == 0)	$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Job Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iJobId = $objDatabase->Result($varResult, 0, "CJ.JobId");				
				$iCustomerId = $objDatabase->Result($varResult, 0, "CJ.CustomerId");
				
				$sFirstName = $objDatabase->Result($varResult, 0, "C.FirstName");
				$sFirstName = $objDatabase->RealEscapeString($sFirstName);
				$sFirstName = stripslashes($sFirstName);
				
				$sLastName = $objDatabase->Result($varResult, 0, "C.LastName");
				$sLastName = $objDatabase->RealEscapeString($sLastName);
				$sLastName = stripslashes($sLastName);
				$sCustomerFullName = $sFirstName . ' ' . $sLastName;				
				$sCustomerName = $sCustomerFullName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sCustomerName)) . '\', \'../customers/customers_details.php?id=' . $iCustomerId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Customer Information" title="Customer Information" border="0" /></a><br />';
				
				$iJobAddedBy = $objDatabase->Result($varResult, 0, "CJ.JobAddedBy");
				$sJobAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sJobAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sJobAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iJobAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a><br />';
								
				$sTitle = $objDatabase->Result($varResult, 0, "CJ.Title");
				$sSupervisor = $objDatabase->Result($varResult, 0, "CJ.Supervisor");
				
				$dStartDate = $objDatabase->Result($varResult, 0, "CJ.StartDate");
				$sStartDate = date("F j, Y", strtotime($dStartDate));
				
				$dEndDate = $objDatabase->Result($varResult, 0, "CJ.EndDate");
				$sEndDate = date("F j, Y", strtotime($dEndDate));
				
				$sDescription = $objDatabase->Result($varResult, 0, "CJ.Description");
				$iStatus = $objDatabase->Result($varResult, 0, "CJ.Status");
				$sJobStatus = $this->aJobStatus[$iStatus];
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
				
				$dJobAddedOn = $objDatabase->Result($varResult, 0, "CJ.JobAddedOn");
				$sJobAddedOn = date("F j, Y", strtotime($dJobAddedOn)) . ' at ' . date("g:i a", strtotime($dJobAddedOn));
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
				
				$sJobStatus = '<select class="form1" name="selJobStatus" id="selJobStatus">';
		        for ($i=0; $i < count($this->aJobStatus); $i++)
		        	$sJobStatus .= '<option ' . (($iStatus== $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aJobStatus[$i] . '</option>';
		        $sJobStatus .= '</select>';
		        				
				$sSupervisor = '<input type="text" name="txtSupervisor" id="txtSupervisor" class="form1" value="' . $sSupervisor . '" size="30" />&nbsp;<span style="color:red;">*</span>';				
				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				
				$sStartDate = '<input type="text" size="10" id="txtStartDate" name="txtStartDate" class="form1" value="' . $dStartDate . '" />' . $objjQuery->Calendar('txtStartDate') . '&nbsp;<span style="color:red;">*</span>';
				$sEndDate= '<input type="text" size="10" id="txtEndDate" name="txtEndDate" class="form1" value="' . $dEndDate. '" />' . $objjQuery->Calendar('txtEndDate') . '&nbsp;<span style="color:red;">*</span>';
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$iJobId = "";
				$sTitle = $objGeneral->fnGet("txtTitle");
				$sSupervisor = $objGeneral->fnGet("txtSupervisor");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				
				$dStartDate = date("Y-m-d");
                $dEndDate= date("Y-m-d");

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				
				$sJobAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sJobAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View' . $objDatabase->RealEscapeString(stripslashes($sJobAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';	
				
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
				
				$sJobStatus = '<select class="form1" name="selJobStatus" id="selJobStatus">';
		        for ($i=0; $i < count($this->aJobStatus); $i++)
		        	$sJobStatus .= '<option ' . (($iStatus== $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aJobStatus[$i] . '</option>';
		        $sJobStatus .= '</select>';
	        
				$sSupervisor = '<input type="text" name="txtSupervisor" id="txtSupervisor" class="form1" value="' . $sSupervisor . '" size="30" />&nbsp;<span style="color:red;">*</span>';				
				$sTitle = '<input type="text" name="txtTitle" id="txtTitle" class="form1" value="' . $sTitle . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				
				$sStartDate = '<input type="text" size="10" id="txtStartDate" name="txtStartDate" class="form1" value="' . $dStartDate . '" />' . $objjQuery->Calendar('txtStartDate', true) . '&nbsp;<span style="color:red;">*</span>';
				$sEndDate= '<input type="text" size="10" id="txtEndDate" name="txtEndDate" class="form1" value="' . $dEndDate. '" />' . $objjQuery->Calendar('txtEndDate', true) . '&nbsp;<span style="color:red;">*</span>';				
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sJobAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if (GetVal(\'txtSupervisor\') == "") return(AlertFocus(\'Please enter a valid Supervisor Name\', \'txtSupervisor\'));
				if (GetVal(\'txtTitle\') == "") return(AlertFocus(\'Please enter a valid Job Title\', \'txtTitle\'));
				if (GetVal(\'hdnCustomerId\') == "") return(AlertFocus(\'Please enter a valid Customer Name\', \'txtCustomerName\'));

				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Job Information:</span></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200px;">Supervisor:</td><td><strong>' . $sSupervisor . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Customer:</td><td><strong>' . $sCustomerName . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Title:</td><td><strong>' . $sTitle . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Start Date:</td><td><strong>' . $sStartDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">End Date:</td><td><strong>' . $sEndDate . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="2">Description:</td></tr>
			 <tr><td valign="top" colspan="4"><strong>' . $sDescription . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Status:</td><td><strong>' . $sJobStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Job Added On:</td><td><strong>' . $sJobAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Job Added By:</td><td><strong>' . $sJobAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Job" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iJobId . '"><input type="hidden" name="action" id="action" value="UpdateJob"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Job" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewJob"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewJob($sSupervisor, $iCustomerId, $sTitle, $dStartDate, $dEndDate, $sDescription, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[1] == 0)
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		$iJobAddedBy = $objEmployee->iEmployeeId;
		$sSupervisor = $objDatabase->RealEscapeString($sSupervisor);	
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);		

		$varResult = $objDatabase->Query("INSERT INTO fms_customers_jobs
		(OrganizationId, Supervisor, CustomerId, Title, StartDate, EndDate, Description, Status, Notes, JobAddedOn, JobAddedBy)
		VALUES ('" . cOrganizationId . "', '$sSupervisor', '$iCustomerId', '$sTitle', '$dStartDate', '$dEndDate', '$sDescription', '$iStatus', '$sNotes', '$varNow', '$iJobAddedBy')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_customers_jobs AS CJ WHERE CJ.OrganizationId='" . cOrganizationId . "' CJ.Title='$sTitle' AND CJ.JobAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$objSystemLog->AddLog("Add New Job - " . $sTitle);
				$objGeneral->fnRedirect('?pagetype=details&error=8700&id=' . $objDatabase->Result($varResult, 0, "CJ.JobId"));
			}
		}

		return(8701);
	}
	
	function UpdateJob($iJobId, $sSupervisor, $iCustomerId, $sTitle, $dStartDate, $dEndDate, $sDescription, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[2] == 0)
			return(1010);
			
		$sSupervisor = $objDatabase->RealEscapeString($sSupervisor);
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);

		$varResult = $objDatabase->Query("
		UPDATE fms_customers_jobs 
		SET
			Supervisor='$sSupervisor', 
			CustomerId='$iCustomerId', 
			Title='$sTitle', 
			StartDate='$dStartDate', 
			EndDate='$dEndDate', 
			Description='$sDescription', 
			Status='$iStatus', 
			Notes='$sNotes'	
		WHERE OrganizationId='" . cOrganizationId . "' AND JobId='$iJobId'");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Update Job - " . $sTitle);
			$objGeneral->fnRedirect('../customers/jobs.php?pagetype=details&error=8702&id=' . $iJobId);			
		}
		else
			return(8703);
	}
	
	function DeleteJob($iJobId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Jobs[3] == 0)
		{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iJobId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_customers_jobs AS CJ WHERE CJ.OrganizationId='" . cOrganizationId . "' AND CJ.JobId='$iJobId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
		{
			return(8705);
		}

		$sTitle = $objDatabase->Result($varResult, 0, "CJ.Title");
		$sTitle = $objDatabase->RealEscapeString($sTitle);
		
		$varResult = $objDatabase->Query("DELETE FROM fms_customers_jobs WHERE JobId='$iJobId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Delete Job - " . $sTitle);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iJobId . '&error=8704');
			else
				$objGeneral->fnRedirect('?id=0&error=8704');
		}
		else
			return(8705);
	}

}

?>