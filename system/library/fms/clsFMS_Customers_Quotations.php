<?php

// Class: Quotation
class clsQuotations
{
	// Class Constructor
	public $aQuotationStatus;
	function __construct()
	{
		$this->aQuotationStatus = array("Generated", "Processed", "Approved", "Rejected");
	}
	
	function ShowAllQuotations($sSearch, $iQuotationStatus = -1)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		if ($iQuotationStatus== '') $iQuotationStatus = -1;
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Quotations";
		if ($sSortBy == "") $sSortBy = "Q.QuotationId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((Q.QuotationId LIKE '%$sSearch%') OR (C.FirstName LIKE '%$sSearch%') OR (C.LastName LIKE '%$sSearch%') OR (Q.QuotationNo LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (C.FirstName LIKE '%$sSearch%') OR (C.LastName LIKE '%$sSearch%') OR (C.CompanyName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}
		if ($iQuotationStatus >= 0) $sStatusCondition = " AND Q.STATUS='$iQuotationStatus'";

		$iTotalRecords = $objDatabase->DBCount("fms_customers_quotations AS Q INNER JOIN fms_customers AS C ON C.CustomerId = Q.CustomerId INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy", "Q.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sStatusCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_customers_quotations AS Q 
		INNER JOIN fms_customers AS C ON C.CustomerId = Q.CustomerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy
		WHERE Q.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sStatusCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");
		
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Quotation Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Quotation Date in Ascending Order" title="Sort by Quotation Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Quotation Date in Descending Order" title="Sort by Quotation Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Customer&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Customer Name in Ascending Order" title="Sort by Customer Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Customer Name in Descending Order" title="Sort by Customer Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Company&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Company Name in Ascending Order" title="Sort by Company Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Company Name in Descending Order" title="Sort by Company Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Quotation No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Quotation No in Ascending Order" title="Sort by Quotation No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Quotation No in Descending Order" title="Sort by Quotation No in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Expiry Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.ExpiryDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Expiry Date in Ascending Order" title="Sort by Expiry Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.ExpiryDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Expiry Date in Descending Order" title="Sort by Expiry Date in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Total Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.TotalAmount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Total Amount in Ascending Order" title="Sort by Total Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.TotalAmount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Total Amount in Descending Order" title="Sort by Total Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="6%" colspan="6"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iQuotationId = $objDatabase->Result($varResult, $i, "Q.QuotationId");
			$iCustomerId = $objDatabase->Result($varResult, $i, "Q.CustomerId");
			
			$sFirstName = $objDatabase->Result($varResult, $i, "C.FirstName");
			$sFirstName = $objDatabase->RealEscapeString($sFirstName);
			$sFirstName = stripslashes($sFirstName);
			
			$sLastName = $objDatabase->Result($varResult, $i, "C.LastName");
			$sLastName = $objDatabase->RealEscapeString($sLastName);
			$sLastName = stripslashes($sLastName);
			$sCustomerName = $sFirstName . ' ' . $sLastName;
			
			$dExpiryDate = $objDatabase->Result($varResult, $i, "Q.ExpiryDate");
			$sExpiryDate = date("F j, Y", strtotime($dExpiryDate));
			
			$sQuotationNo = $objDatabase->Result($varResult, $i, "Q.QuotationNo");
			$sQuotationNo = $objDatabase->RealEscapeString($sQuotationNo);
			$sQuotationNo = stripslashes($sQuotationNo);
			
			$dQuotationDate = $objDatabase->Result($varResult, $i, "Q.QuotationDate");
			$sQuotationDate = date("F j, Y", strtotime($dQuotationDate));
			
			$dTotalAmount = $objDatabase->Result($varResult, $i, "Q.TotalAmount");
			$sTotalAmount = number_format($dTotalAmount, 0);
			
			$iStatus = $objDatabase->Result($varResult, $i, "Q.Status");
			$sStatus = $this->aQuotationStatus[$iStatus];
			
			$sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");
			
			$sEditQuotation = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Quotation ' . $sQuotationNo . '\', \'../customers/quotations.php?pagetype=details&action2=edit&quotationid=' . $iQuotationId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quotation Details" title="Edit this Quotation Details"></a></td>';
			$sDeleteQuotation = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Quotation?\')) {window.location = \'?action=DeleteQuotation&quotationid=' . $iQuotationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quotation" title="Delete this Quotation"></a></td>';
			$sQuotationReceipt = '<td class="GridTD" align="center"><a href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=CustomerReports&reporttype=Quotation&selEmployee=-1&selStation=-1&txtId=' . $iQuotationId . '\', 800,600);"><img src="../images/icons/iconPrint2.gif" border="0" alt="Quotation Receipt" title="Quotation Receipt"></a></td>';
			$sMakeSalesOrder = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Sales Order ' . $objDatabase->RealEscapeString(stripslashes($sQuotationNo)) . '\', \'../customers/salesorders.php?pagetype=details&action2=addnew&quotationid=' . $iQuotationId . '\', \'520px\', true);"><img src="../images/icons/tick.gif" border="0" alt="Make Sales Order" title="Make Sales Order"></a></td>';
			
			$sDocuments = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/documents_show.php?componentname=Customers_Quotations&id=' . $iQuotationId . '\', \'Documents of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sQuotationNo)) . '\', \'700 420\');"><img src="../images/icons/save.gif" border="0" alt="Quotation Documents" title="Quotation Documents"></a></td>';
						
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[2] == 0)		$sEditQuotation = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[3] == 0)		$sDeleteQuotation = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations_Documents[0] == 0)	$sDocuments = '<td class="GridTD">&nbsp;</td>';
			
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sQuotationDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCustomerName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sCompanyName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sQuotationNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sExpiryDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>			 
			 <td class="GridTD" align="right" valign="top">' . $sTotalAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View Quotation ' . $sQuotationNo . '\', \'../customers/quotations.php?pagetype=details&quotationid=' . $iQuotationId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quotation Details" title="View this Quotation Details"></a></td>
			 ' . $sEditQuotation . $sMakeSalesOrder. $sQuotationReceipt. $sDocuments. $sDeleteQuotation  . '</tr>';
		}

		$sAddQuotation = '<input onclick="window.top.CreateTab(\'tabContainer\', \'New Quotation\', \'../customers/quotations.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="New Quotation">';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[1] == 0) // Add Disabled
			$sAddQuotation = '';
		
		$sQuotationStatusSelect = '<select class="form1" name="selQuotationStatus" onchange="window.location=\'?status=\'+GetSelectedListBox(\'selQuotationStatus\');" id="selQuotationStatus">
		<option value="-1">Quotations with All Status</option>';
		for ($i=0; $i < count($this->aQuotationStatus); $i++)
			$sQuotationStatusSelect .= '<option ' . (($iQuotationStatus== $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aQuotationStatus[$i] . '</option>';
		$sQuotationStatusSelect .= '</select>';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddQuotation . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Quotation:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Quotation" title="Search for a Quotation" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td colspan="2">' .  $sQuotationStatusSelect . '</td>
		 </tr>	
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Quotation Details" alt="View this Quotation Details"></td><td>View this Quotation Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Quotation Details" alt="Edit this Quotation Details"></td><td>Edit this Quotation Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Quotation" alt="Delete this Quotation"></td><td>Delete this Quotation</td></tr>
		</table>';

		return($sReturn);
	}
	
	function QuotationDetails($iQuotationId, $sAction2)
	{		
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		$k = 0;
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();		
		include(cVSFFolder . '/classes/clsjQuery.php');		
		$objjQuery = new clsjQuery();
		
		include('../../system/library/fms/clsFMS_Customers.php');
		$objCustomers = new clsCustomers_Customers();
		$aCustomers = $objCustomers->CustomersList();
		
		$objQueryCustomer = new clsjQuery($aCustomers);
		
		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_customers_quotations AS Q
		INNER JOIN fms_customers AS C ON C.CustomerId = Q.CustomerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy
		WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationId = '$iQuotationId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&quotationid=' . $iQuotationId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quotation Details" title="Edit this Quotation Details" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Quotation?\')) {window.location = \'?pagetype=details&action=DeleteQuotation&quotationid=' . $iQuotationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quotation" title="Delete this Quotation" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[3] == 0)	$sButtons_Delete = '';
					
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Quotation Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iQuotationId = $objDatabase->Result($varResult, 0, "Q.QuotationId");
				$iCustomerId = $objDatabase->Result($varResult, 0, "Q.CustomerId");
				
				$sFirstName = $objDatabase->Result($varResult, 0, "C.FirstName");
				$sFirstName = $objDatabase->RealEscapeString($sFirstName);
				$sFirstName = stripslashes($sFirstName);				
				$sLastName = $objDatabase->Result($varResult, 0, "C.LastName");
				$sLastName = $objDatabase->RealEscapeString($sLastName);
				$sLastName = stripslashes($sLastName);
				$sCustomerName = $sFirstName . ' ' . $sLastName;
				$sCustomerFullName = $sCustomerName;
				$sCustomerName = $sCustomerName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sCustomerName)) . '\', \'../customers/customers.php?pagetype=details&id=' . $iCustomerId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Customer Information" title="Customer Information" border="0" /></a><br />';
				$dExpiryDate = $objDatabase->Result($varResult, 0, "Q.ExpiryDate");
				$sExpiryDate = date("F j, Y", strtotime($dExpiryDate));				
				$dQuotationDate = $objDatabase->Result($varResult, 0, "Q.QuotationDate");
				$sQuotationDate = date("F j, Y", strtotime($dQuotationDate));				
				$sQuotationNo = $objDatabase->Result($varResult, 0, "Q.QuotationNo");
				$sQuotationNo = $objDatabase->RealEscapeString($sQuotationNo);
				$sQuotationNo = stripslashes($sQuotationNo);				
				$dTotalAmount = $objDatabase->Result($varResult, 0, "Q.TotalAmount");
				$sTotalAmount = number_format($dTotalAmount, 0);
				
				$iQuotationAddedBy = $objDatabase->Result($varResult, 0, "Q.QuotationAddedBy");
				$sQuotationAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sQuotationAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sQuotationAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iQuotationAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a><br />';
				
				$sDescription = $objDatabase->Result($varResult, 0, "Q.Description");				
				$sNotes = $objDatabase->Result($varResult, 0, "Q.Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
			    
				$iQuotationStatus = $objDatabase->Result($varResult, 0, "Q.Status");
				$sQuotationStatus = $this->aQuotationStatus[$iQuotationStatus];
				
				$dQuotationAddedOn = $objDatabase->Result($varResult, 0, "Q.QuotationAddedOn");
				$sQuotationAddedOn = date("F j, Y", strtotime($dQuotationAddedOn)) . ' at ' . date("g:i a", strtotime($dQuotationAddedOn));
				
				$varResult2 = $objDatabase->Query("SELECT * 
				FROM fms_customers_quotations_items AS QI 
				WHERE QI.QuotationId = '$iQuotationId'");
						       	
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';
										
					$sProductName = $objDatabase->Result($varResult2, $i, "QI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $i, "QI.Quantity");
					$dUnitPrice = $objDatabase->Result($varResult2, $i, "QI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $i, "QI.Amount");				
					
					$sOrderParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . ($i+1) . '</td>					 
 					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;">' . $sProductName . '</td>					 					 
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . $iQuantity . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>
					</tr>';
				}
				
				$sQuotationItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="center"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="center"><span class="WhiteHeading">Amount</span></td>
				 </tr>
				 ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:18px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</td>
				 </tr>				 
				</table>';
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
				
				$sQuotationDate = '<input size="12" type="text" id="txtQuotationDate" name="txtQuotationDate" class="form1" value="' . $dQuotationDate . '" />' . $objjQuery->Calendar('txtQuotationDate');
				$sExpiryDate = '<input size="12" type="text" id="txtExpiryDate" name="txtExpiryDate" class="form1" value="' . $dExpiryDate . '" />' . $objjQuery->Calendar('txtExpiryDate');
				$sQuotationNo = '<input type="text" name="txtQuotationNo" id="txtQuotationNo" class="form1" value="' . $sQuotationNo . '" size="15" />&nbsp;<span style="color:red;">*</span>';
				$sQuotationStatus = '<select class="form1" name="selQuotationtStatus" id="selQuotationtStatus">';
		        for ($i=0; $i < count($this->aQuotationStatus); $i++)
		        	$sQuotationStatus .= '<option ' . (($iQuotationtStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aQuotationStatus[$i] . '</option>';
		        $sQuotationStatus .= '</select>';
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sOrderParticulars_Rows = "";
				$varResult2 = $objDatabase->Query("SELECT * 
				FROM fms_customers_quotations_items AS QI				
				WHERE QI.QuotationId= '$iQuotationId'");
						       	
		        for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';			
					
					$sProductName = $objDatabase->Result($varResult2, $k, "QI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $k, "QI.Quantity");					
					$dUnitPrice = $objDatabase->Result($varResult2, $k, "QI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $k, "QI.Amount");					
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');"  type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">' . number_format($dAmount, 0) . '</div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				for ($j = $k; $j < 50; $j++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'none';
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				$sQuotationItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				  ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</div></td>
				 </tr>
				</table>
				<input type="hidden" name="TotalPayment" id="TotalPayment" value="" />
				<input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="' . $dTotalAmount . '" />
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $j . ';
				 var iRowVisible = ' . ($k +1) . ';
				</script>';
			}
			else if ($sAction2 == "addnew")
			{
				$iQuotationId = "";				
				$sQuotationNo = $objGeneral->fnGet("txtQuotationNo");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
								
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sStationName = '<select class="form1" name="selStation" id="selStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S ORDER BY S.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess == 1)
						$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
					else
					{
						if ($objEmployee->iEmployeeStationId == $objDatabase->Result($varResult2, $i, "S.StationId"))
							$sStationName .= '<option ' . (($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
					}
				}
				$sStationName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
				
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true);
				
				$sAddNewCustomer = '<a style="color:blue; font-size:14px;" href="#noanchor" onclick="AddNewCustomer();"><img align="absmiddle" src="../images/icons/plus.gif" border="0" alt="Add New Customer" title="Add New Customer" /></a>';
				$sAddNewCustomer .= '
				<tr id="trAddNewCustomer1" style="display:none;"><td valign="top">Station:</td><td><strong>' . $sStationName . '</strong></td></tr>
    			<tr id="trAddNewCustomer2" style="display:none;"><td valign="top">First Name:</td><td><input type="text" name="txtFirstName" id="txtFirstName" class="form1" size="30"/></td></tr>
    			<tr id="trAddNewCustomer3" style="display:none;"><td valign="top">Last Name:</td><td><input type="text" name="txtLastName" id="txtLastName" class="form1" size="30" /></td></tr>
    			<tr id="trAddNewCustomer4" style="display:none;"><td valign="top">Phone Number:</td><td ><input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" size="30" /></td></tr>
    			<tr id="trAddNewCustomer5" style="display:none;"><td valign="top">Mobile Number:</td><td><input type="text" name="txtMobileNumber" id="txtMobileNumber" class="form1" size="30" /></td></tr>';
		    			
				$dQuotationDate = date("Y-m-d");
				$dExpiryDate = date("Y-m-d", mktime(0, 0, 0, date("m")+1, date("d"),   date("Y"))); // Next Month
				
				$sQuotationDate = '<input size="12" type="text" id="txtQuotationDate" name="txtQuotationDate" class="form1" value="' . $dQuotationDate . '" />' . $objjQuery->Calendar('txtQuotationDate');				
				$sExpiryDate = '<input size="12" type="text" id="txtExpiryDate" name="txtExpiryDate" class="form1" value="' . $dExpiryDate . '" />' . $objjQuery->Calendar('txtExpiryDate');
				
				$sTax = '<input type="text" name="txtTax" id="txtTax" class="form1" value="' . $dTax . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sQuotationNo = '<input type="text" name="txtQuotationNo" id="txtQuotationNo" class="form1" value="' . $sQuotationNo . '" size="15" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sQuotationStatus = '<select class="form1" name="selQuotationtStatus" id="selQuotationtStatus">';
		        for ($i=0; $i < count($this->aQuotationStatus); $i++)
		        	$sQuotationStatus .= '<option ' . (($iQuotationtStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aQuotationStatus[$i] . '</option>';
		        $sQuotationStatus .= '</select>';
				
				$sQuotationAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				$sQuotationAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;				
				$sQuotationAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sQuotationAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a><br />';
				
				for ($k=0; $k < 50; $k++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = (($k <= 4) ? 'table-row' : 'none');
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" size="3" /></td> 
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
					</tr>';
				}				
				
				$sQuotationItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				 ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount"></div></td>
				 </tr>
				</table>
				<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="" />
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $k . ';
				 var iRowVisible = 6;	
				</script>';
			}
			
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">			
			function AddNewCustomer()
			{
				for (i = 1; i <= 5; i++)
					document.getElementById("trAddNewCustomer" + i).style.display = (document.getElementById("trAddNewCustomer" + i).style.display == "none") ? "block" : "none";
			}
			function ValidateForm()
			{	
				if (GetVal(\'hdnCustomerId\') == "") return(AlertFocus(\'Please enter a valid Customer Name\', \'txtCustomerName\'));
				if (GetVal(\'txtQuotationNo\') == "") return(AlertFocus(\'Please enter a valid Quotation No\', \'txtQuotationNo\'));
				if (!isDate(GetVal(\'txtQuotationDate\'))) return(AlertFocus(\'Please enter a valid value for Quotation Date\', \'txtQuotationDate\'));				
				if (!isDate(GetVal(\'txtExpiryDate\'))) return(AlertFocus(\'Please enter a valid value for Expiry Date\', \'txtExpiryDate\'));
				
				return (true);
			}
			</script>				
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Quotation Information:</span></td></tr>			 
			 <tr bgcolor="#ffffff"><td style="width:200px;" valign="top">Customer:</td><td><strong>' . $sCustomerName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Quotation No:</td><td><strong>' . $sQuotationNo . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Quotation Date:</td><td><strong>' . $sQuotationDate . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Expiry Date:</td><td><strong>' . $sExpiryDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Status:</td><td><strong>' . $sQuotationStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Quotation Items:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">' . $sQuotationItems . '</td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Quotation Added On:</td><td><strong>' . $sQuotationAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Quotation Added By:</td><td><strong>' . $sQuotationAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align="center"><input type="submit" value="Update Quotation" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iQuotationId . '"><input type="hidden" name="action" id="action" value="UpdateQuotation"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align="center"><input type="submit" value="Add Quotation" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewQuotation"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewQuotation($iCustomerId, $dExpiryDate, $dQuotationDate, $sQuotationNo, $sQuotationItems, $sDescription, $dTotalAmount, $sNotes, $iStatus)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[1] == 0) // Add Disabled
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		$iQuotationAddedBy = $objEmployee->iEmployeeId;
				
		if($dTotalAmount == "") $dTotalAmount = 0;

		$sQuotationNo = $objDatabase->RealEscapeString($sQuotationNo);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		$sFirstName = $objDatabase->RealEscapeString($sFirstName);
		$sLastName = $objDatabase->RealEscapeString($sLastName);
		$sPhoneNumber = $objDatabase->RealEscapeString($sPhoneNumber);
		$sMobileNumber = $objDatabase->RealEscapeString($sMobileNumber);
				
		if($objDatabase->DBCount("fms_customers_quotations AS Q", "Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationNo = '$sQuotationNo'") > 0)
			return(10000);
		
		if (($iCustomerId == 0) || ($iCustomerId == ""))
			return(10008);		
		
		$bCheck = $objDatabase->Query("INSERT INTO fms_customers_quotations
		(OrganizationId, CustomerId, ExpiryDate, QuotationDate, QuotationNo, TotalAmount, Description, Notes, STATUS, QuotationAddedOn, QuotationAddedBy)
		VALUES ('" . cOrganizationId . "', '$iCustomerId', '$dExpiryDate', '$dQuotationDate', '$sQuotationNo', '$dTotalAmount', '$sDescription', '$sNotes', '$iStatus', '$varNow', '$iQuotationAddedBy')");

		if ($bCheck)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_customers_quotations AS Q WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationNo='$sQuotationNo' AND Q.QuotationAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iQuotationId = $objDatabase->Result($varResult, 0, "Q.QuotationId");				
				for ($k=0; $k < 50; $k++)
				{					
					$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));					
					
					$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
					$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
					$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
					
					if(($sProductName != "") && ($iProductQty > 0))
						$this->AddQuotationItems($iQuotationId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
					
				}
				
				$objSystemLog->AddLog("Add New Quotation - " . $sQuotationNo);
				$objGeneral->fnRedirect('?pagetype=details&error=10001&quotationid=' . $objDatabase->Result($varResult, 0, "Q.QuotationId"));
			}
		}

		return(10002);
	}
	
	function UpdateQuotation($iQuotationId, $iCustomerId, $dExpiryDate, $dQuotationDate, $sQuotationNo, $sQuotationItems, $sDescription, $dTotalAmount,$sNotes, $iStatus)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[2] == 0) // Update Disabled
			return(1010);			
		
		if($dTotalAmount == "") $dTotalAmount = 0;		
		$sQuotationNo = $objDatabase->RealEscapeString($sQuotationNo);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if ($iCustomerId == 0) return(10008);
			
		if($objDatabase->DBCount("fms_customers_quotations AS Q", "Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationNo = '$sQuotationNo' AND Q.QuotationId <> '$iQuotationId'") > 0)
			return(10000);
		
		$varResult = $objDatabase->Query("
		UPDATE fms_customers_quotations 
		SET	
			CustomerId='$iCustomerId', 
			ExpiryDate='$dExpiryDate', 
			QuotationDate='$dQuotationDate',
			QuotationNo='$sQuotationNo',
			TotalAmount='$dTotalAmount',			
			Description='$sDescription',
			Status='$iStatus',
			Notes='$sNotes'
		WHERE OrganizationId='" . cOrganizationId . "' AND QuotationId='$iQuotationId'");

		$varResult = $objDatabase->Query("DELETE FROM fms_customers_quotations_items WHERE QuotationId='$iQuotationId'");
		
		for ($k=0; $k < 50; $k++)
		{					
			$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));					
			
			$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
			$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
			$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
			
			if(($sProductName != "") && ($iProductQty > 0))
				$this->AddQuotationItems($iQuotationId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
			
		}
		
		$objSystemLog->AddLog("Update Quotation - " . $sQuotationNo);
		$objGeneral->fnRedirect('../customers/quotations.php?pagetype=details&error=10003&quotationid=' . $iQuotationId . '&id=' . $iCustomerId);		
		
	}
	
	function DeleteQuotation($iQuotationId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Quotations[2] == 0) // Delete Disabled
	    {
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iQuotationId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_customers_quotations AS Q WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationId='$iQuotationId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(10007);

		$sQuotationNo = $objDatabase->Result($varResult, 0, "Q.QuotationNo");
		$sQuotationNo = $objDatabase->RealEscapeString($sQuotationNo);
		
		$varResult = $objDatabase->Query("DELETE FROM fms_customers_quotations WHERE QuotationId='$iQuotationId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_customers_quotations_items WHERE QuotationId='$iQuotationId'");
			
			$objSystemLog->AddLog("Delete Quotation - " . $sQuotationNo);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iQuotationId . '&error=10005');
			else
				$objGeneral->fnRedirect('?id=0&error=10005');
		}
		else
			return(10006);
	}
	
	function AddQuotationItems($iQuotationId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount)
	{
		global $objDatabase;
		global $objGeneral;
		
		$sProductName = $objDatabase->RealEscapeString($sProductName);
		if($iProductQty == "" ) $iProductQty = 0;
		if($dProductUnitPrice == "" ) $dProductUnitPrice = 0;
		if($dProductTotalAmount == "" ) $dProductTotalAmount = 0;
		
		$varResult2 = $objDatabase->Query("INSERT INTO fms_customers_quotations_items
		(QuotationId, ProductName, Quantity, UnitPrice, Amount)
		VALUES ('$iQuotationId', '$sProductName', '$iProductQty', '$dProductUnitPrice', '$dProductTotalAmount')");
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
		
		
	}
	
}

?>