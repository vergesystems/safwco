<?php

// Class: Receipt
class clsReceipts
{
	public $aPaymentMethod;
	// Class Constructor
	function __construct()
	{
		$this->aPaymentMethod = array("Received By Check", "Received By Cash", "Received By Online Transfer");
	}
	
	function ShowAllReceipts($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		if ($iReceiptStatus== '') $iReceiptStatus = -1;
		
		$sTitle = "Receipts";
		if ($sSortBy == "") $sSortBy = "R.ReceiptId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((R.Reference LIKE '%$sSearch%') OR (R.ReceiptId LIKE '%$sSearch%') OR (C.FirstName LIKE '%$sSearch%') OR (C.LastName LIKE '%$sSearch%') OR (C.CompanyName LIKE '%$sSearch%') OR (R.ReceiptNo LIKE '%$sSearch%')) ";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}
		//if ($iReceiptStatus>= 0) $sStatusCondition = " AND R.STATUS='$iReceiptStatus'";

		$iTotalRecords = $objDatabase->DBCount("fms_customers_receipts AS R LEFT JOIN fms_customers_invoices AS I ON I.InvoiceId = R.InvoiceId INNER JOIN fms_customers AS C ON C.CustomerId = R.CustomerId INNER JOIN organization_employees AS E ON E.EmployeeId = R.ReceiptAddedBy", "R.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT *,R.TotalAmount AS TotalAmount FROM fms_customers_receipts AS R 
		LEFT JOIN fms_customers_invoices AS I ON I.InvoiceId = R.InvoiceId
		INNER JOIN fms_customers AS C ON C.CustomerId = R.CustomerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = R.ReceiptAddedBy
		WHERE R.OrganizationId='" . cOrganizationId . "' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Receipt Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=R.ReceiptDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Receipt Date in Ascending Order" title="Sort by Receipt Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=R.ReceiptDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Receipt Date in Descending Order" title="Sort by Receipt Date in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Customer Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Customer Name in Ascending Order" title="Sort by Customer Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Customer Name in Descending Order" title="Sort by Customer Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Company Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Company Name in Ascending Order" title="Sort by Company Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CompanyName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Company Name in Descending Order" title="Sort by Company Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Invoice No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=I.InvoiceNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Invoice No in Ascending Order" title="Sort by Invoice No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=I.InvoiceNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Invoice No in Descending Order" title="Sort by Invoice No in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Receipt No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=R.ReceiptNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Receipt No in Ascending Order" title="Sort by Receipt No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=R.ReceiptNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Receipt No in Descending Order" title="Sort by Receipt No in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=R.TotalAmount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Total Amount in Ascending Order" title="Sort by Total Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=R.TotalAmount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Total Amount in Descending Order" title="Sort by Total Amount in Descending Order" border="0" /></a></span></td>
		  <td width="5%" colspan="5"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iReceiptId = $objDatabase->Result($varResult, $i, "R.ReceiptId");
			$iInvoiceId = $objDatabase->Result($varResult, $i, "R.InvoiceId");
			
			$sInvoiceNo = $objDatabase->Result($varResult, $i, "I.InvoiceNo");
			
			$sReference = $objDatabase->Result($varResult, $i, "R.Reference");
			$sReference = $objDatabase->RealEscapeString($sReference);
			
			$sReceiptNo = $objDatabase->Result($varResult, $i, "R.ReceiptNo");
			$sReceiptNo = $objDatabase->RealEscapeString($sReceiptNo);
			
			$sFirstName = $objDatabase->Result($varResult, $i, "C.FirstName");
			$sFirstName = $objDatabase->RealEscapeString($sFirstName);
			$sFirstName = stripslashes($sFirstName);
			
			$sLastName = $objDatabase->Result($varResult, $i, "C.LastName");
			$sLastName = $objDatabase->RealEscapeString($sLastName);
			$sLastName = stripslashes($sLastName);
			$sCustomerName = $sFirstName . ' ' . $sLastName;
		
			$dTotalAmount = $objDatabase->Result($varResult, $i, "TotalAmount");
			$sTotalAmount = number_format($dTotalAmount, 0);
			
			$dReceiptDate = $objDatabase->Result($varResult, $i, "R.ReceiptDate");
			$sReceiptDate = date("F j, Y", strtotime($dReceiptDate));
			$sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");
			
			$sEditReceipt = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Receipt' . $objDatabase->RealEscapeString(str_replace('"', '', $sReceiptNo)) . '\', \'../customers/receipts.php?pagetype=details&action2=edit&receiptid=' . $iReceiptId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Receipt Details" title="Edit Receipt Details"></a></td>';
			$sDeleteReceipt = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Receipt?\')) {window.location = \'?action=DeleteReceipt&receiptid=' . $iReceiptId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete Receipt" title="Delete Receipt"></a></td>';
			$sReceipt = '<td class="GridTD" align="center"><a href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=CustomerReports&reporttype=Receipt&selEmployee=-1&selStation=-1&txtId=' . $iReceiptId . '\', 800,600);"><img src="../images/icons/iconPrint2.gif" border="0" alt="Payment Receipt" title="Payment Receipt"></a></td>';
			$sDocuments = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/documents_show.php?componentname=Customers_Receipts&id=' . $iReceiptId . '\', \'Documents of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sOrderNo)) . '\', \'700 420\');"><img src="../images/icons/save.gif" border="0" alt="Receipt Documents" title="Receipt Documents"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[2] == 0)	$sEditReceipt = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[3] == 0)	$sDeleteReceipt = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts_Documents[0] == 0)	$sDocuments = '<td class="GridTD">&nbsp;</td>';
							
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sReceiptDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>			 
			 <td class="GridTD" align="left" valign="top">' . $sCustomerName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td> 
			 <td class="GridTD" align="left" valign="top">' . $sCompanyName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td> 
			 <td class="GridTD" align="left" valign="top">' . $sInvoiceNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sReceiptNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>			 
			 <td class="GridTD" align="right" valign="top">' . $sTotalAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $objDatabase->RealEscapeString(str_replace('"', '', $sReceiptNo)) . '\', \'../customers/receipts.php?pagetype=details&receiptid=' . $iReceiptId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Receipt Details" title="View this Receipt Details"></a></td>
			 ' . $sEditReceipt . $sReceipt. $sDocuments . $sDeleteReceipt . '
			</tr>';
		}

		$sAddReceipt = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Receipt\', \'../customers/receipts.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Receipt">';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[1] == 0) // Add Disabled
			$sAddReceipt = '';	
			
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddReceipt . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Receipt:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Receipt" title="Search for Receipt" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Receipt Details" alt="View this Receipt Details"></td><td>View this Receipt Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Receipt Details" alt="Edit this Receipt Details"></td><td>Edit this Receipt Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconPrint2.gif" title="Print the Recipt" alt="Print the Recipt"></td><td>Print the Receipt</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconFolder.gif" title="Recipt Documents" alt="Recipt Documents"></td><td>Recipt Documents</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Receipt" alt="Delete this Receipt"></td><td>Delete this Receipt</td></tr>
		</table>';

		return($sReturn);
	}
	
	function ReceiptDetails($iReceiptId, $iInvoiceId=0, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;	
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();
		
		$objCustomers = new clsCustomers_Customers();
		$aCustomers = $objCustomers->CustomersList();
				
		$objQueryCustomer = new clsjQuery($aCustomers);
		
		$varResult = $objDatabase->Query("
		SELECT *,R.TotalAmount AS TotalAmount,R.Description AS Description,R.Notes AS Notes
		FROM fms_customers_receipts AS R
		LEFT JOIN fms_customers_invoices AS I ON I.InvoiceId = R.InvoiceId
		INNER JOIN fms_customers AS C ON C.CustomerId = R.CustomerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = R.ReceiptAddedBy
		WHERE R.OrganizationId='" . cOrganizationId . "' AND R.ReceiptId = '$iReceiptId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&receiptid=' . $iReceiptId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Receipt Details" title="Edit this Receipt Details" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Receipt?\')) {window.location = \'?pagetype=details&action=DeleteReceipt&receiptid=' . $iReceiptId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Receipt" title="Delete this Receipt" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[3] == 0)	$sButtons_Delete = '';
			
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Receipt Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iReceiptId = $objDatabase->Result($varResult, 0, "R.ReceiptId");
				$iInvoiceId = $objDatabase->Result($varResult, 0, "R.InvoiceId");
		
				if($iInvoiceId > 0)
				{
					$sInvoiceNo = $objDatabase->Result($varResult, 0, "I.InvoiceNo");
					$sInvoiceNo = $objDatabase->RealEscapeString($sInvoiceNo);
					$sInvoiceNo = stripslashes($sInvoiceNo);
					$sInvoiceNo .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View ' . $sInvoiceNo . '\', \'../customers/invoices_details.php?id=' . $iInvoiceId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Invoice Information" title="Invoice Information" border="0" /></a><br />';
				}
				else
					$sInvoiceNo = "No Invoice";	
				
				$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
				$sCustomerName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName");
				$sCustomerName = $objDatabase->RealEscapeString($sCustomerName);
				$sCustomerFullName = $sCustomerName;
				$sCustomerName .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View' . $sCustomerName . '\', \'../customers/customers_details.php?id=' . $iCustomerId . '\', \'520px\', true);">&nbsp;<img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Customer Information" title="Customer Information" border="0" /></a><br />';
								
				$sReceiptNo = $objDatabase->Result($varResult, 0, "R.ReceiptNo");
				$sReceiptNo = $objDatabase->RealEscapeString($sReceiptNo);
				
				$dTotalAmount = $objDatabase->Result($varResult, 0, "TotalAmount");
				$sTotalAmount = number_format($dTotalAmount, 0);
				
				$iPaymentMethod = $objDatabase->Result($varResult, 0, "R.PaymentMethod");				
				$sPaymentMethod = $this->aPaymentMethod[$iPaymentMethod];
				
				$dReceiptDate = $objDatabase->Result($varResult, 0, "R.ReceiptDate");
				$sReceiptDate = date("F j, Y", strtotime($dReceiptDate));
				
				$sDescription = $objDatabase->Result($varResult, 0, "Description");
				$sDescription = $objDatabase->RealEscapeString($sDescription);
				$sDescription = stripslashes($sDescription);
				
				$sNotes = $objDatabase->Result($varResult, 0, "Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
			
				$dReceiptAddedOn = $objDatabase->Result($varResult, 0, "R.ReceiptAddedOn");
				$sReceiptAddedOn = date("F j, Y", strtotime($dReceiptAddedOn)) . ' at ' . date("g:i a", strtotime($dReceiptAddedOn));
				
				$iReceiptAddedBy = $objDatabase->Result($varResult, 0, "E.EmployeeId");				
				$sReceiptAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sReceiptAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View' . $objDatabase->RealEscapeString(stripslashes($sReceiptAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iReceiptAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$varResult2 = $objDatabase->Query("
				SELECT *
				FROM fms_customers_receipts_items AS RI				
				WHERE RI.ReceiptId = '$iReceiptId'");
								
				 for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';
										
					$sProductName = $objDatabase->Result($varResult2, $i, "RI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $i, "RI.Quantity");
					$dUnitPrice = $objDatabase->Result($varResult2, $i, "RI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $i, "RI.Amount");				
					
					$sOrderParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . ($i+1) . '</td>					 
 					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;">' . $sProductName . '</td>					 					 
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . $iQuantity . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>
					</tr>';
				}
				
				$sReceiptItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		          <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>
				  <td><span class="WhiteHeading">Item</span></td>
				  <td width="14%" align="center"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="center"><span class="WhiteHeading">Amount</span></td>
				 </tr>
				 ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:18px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</td>
				 </tr>				 
				</table>';
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
				
				$sPaymentMethod = '<select name="selPaymentMethod" id="selPaymentMethod" class="form1">';
				for($i = 0; $i < count($this->aPaymentMethod); $i++)
				{
					$sPaymentMethod .= '<option ' . (($iPaymentMethod ==  $i) ? 'selected="true"' : '') . '' . 'value="' . $i . '">' . $this->aPaymentMethod[$i];
				}
				$sPaymentMethod .= '</select>';
				
				$sReceiptDate = '<input size="18" type="text" id="txtReceiptDate" name="txtReceiptDate" class="form1" value="' . $dReceiptDate . '" />' . $objjQuery->Calendar('txtReceiptDate');
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sReceiptNo = '<input type="text" name="txtReceiptNo" id="txtReceiptNo" class="form1" value="' . $sReceiptNo . '" size="30" />';
				
				//$sDescription = '<textarea name="txtDescription" id="txtDescription" class="form1" rows="5" cols="60">' . $sDescription . '</textarea>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sOrderParticulars_Rows = "";				
				$varResult2 = $objDatabase->Query("
				SELECT *
				FROM fms_customers_receipts_items AS RI				
				WHERE RI.ReceiptId = '$iReceiptId'");
						       	
		        for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';			
					
					$sProductName = $objDatabase->Result($varResult2, $k, "RI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $k, "RI.Quantity");					
					$dUnitPrice = $objDatabase->Result($varResult2, $k, "RI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $k, "RI.Amount");					
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');"  type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">' . number_format($dAmount, 0) . '</div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				for ($j = $k; $j < 50; $j++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'none';
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="4" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				$sReceiptItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				  ' . $sOrderParticulars_Rows . '
				 <!--
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 -->
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</div></td>
				 </tr>
				</table>
				<input type="hidden" name="TotalPayment" id="TotalPayment" value="" />
				<input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="' . $dTotalAmount . '" />
				<input type="hidden" name="hdnCustomerId" id="hdnCustomerId" value="' . $iCustomerId . '" /> 
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $j . ';
				 var iRowVisible = ' . ($k +1) . ';
				</script>';			
				
			}
			else if ($sAction2 == "addnew")
			{
				$iReceiptId = "";
				$iInvoiceId = $objGeneral->fnGet("invoiceid");
				$sReference = $objGeneral->fnGet("txtReference");
				//$sReceiptNo = $objGeneral->fnGet("txtReceiptNo");
				$dAppliedCredit = $objGeneral->fnGet("txtAppliedCredit");				
				$sDescription = $objGeneral->fnGet("txtDescription");				
				$sNotes = $objGeneral->fnGet("txtNotes");
								
				$dReceiptDateTime = date("Y-m-d");
				$dPaymentDateTime = date("Y-m-d");
				$dDiscountDateTime = date("Y-m-d");			
				
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sReceiptAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sReceiptAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View' . $objDatabase->RealEscapeString(stripslashes($sReceiptAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sInvoiceNo = '<select name="selInvoice" class="form1" onchange="window.location=\'?pagetype=details&action2=addnew&invoiceid=\'+GetSelectedListBox(\'selInvoice\');" id="selInvoice">
				<option value="0">Select Invoice</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_customers_invoices AS CI WHERE CI.OrganizationId='" . cOrganizationId . "' AND CI.Status='2' ORDER BY CI.InvoiceNo");
				if($objDatabase->RowsNumber($varResult) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sInvoiceNo .='<option ' . (($iInvoiceId == $objDatabase->Result($varResult, $i, "CI.InvoiceId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "CI.InvoiceId") . '">' . $objDatabase->Result($varResult, $i, "CI.InvoiceNo") . '</option>';
				}				
				$sInvoiceNo .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selInvoice\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selInvoice\'), \'../customers/invoices_details.php?id=\'+GetSelectedListBox(\'selInvoice\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Invoice Details" title="Invoice Details" /></a>';
				
				if($iInvoiceId > 0)
				{
					$varResult = $objDatabase->Query("
					SELECT *
					FROM fms_customers_invoices AS CI
					INNER JOIN fms_customers AS C ON C.CustomerId = CI.CustomerId
					WHERE CI.InvoiceId= '$iInvoiceId'");
					$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
					$sCustomerFullName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName");
				}
								
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
				
				$sPaymentMethod = '<select name="selPaymentMethod" id="selPaymentMethod" class="form1">';
				for($i = 0; $i < count($this->aPaymentMethod); $i++)
				{
					$sPaymentMethod .= '<option value="' . $i . '">' . $this->aPaymentMethod[$i];
				}
				$sPaymentMethod .= '</select>';
				$sReceiptDate = '<input size="18" type="text" id="txtReceiptDate" name="txtReceiptDate" class="form1" value="' . $dReceiptDateTime . '" />' . $objjQuery->Calendar('txtReceiptDate');
								
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				//$sDescription = '<textarea name="txtDescription" id="txtDescription" class="form1" rows="5" cols="60">' . $sDescription . '</textarea>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';				
				$sReceiptAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				// Get Last Receipt Id
				$varResult = $objDatabase->Query("
				SELECT *
				FROM fms_customers_receipts AS CR
				WHERE CR.OrganizationId='" . cOrganizationId . "'
				ORDER BY CR.ReceiptId DESC LIMIT 1");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					$iReceiptId = $objDatabase->Result($varResult, 0, "CR.ReceiptId");
				}
				$iReceiptId++;
								
		        $sReceiptNo = $objEmployee->aSystemSettings['FMS_Customers_Receipt'];
				$sReceiptNo = str_replace('[Year]', date("Y"), $sReceiptNo);
				$sReceiptNo = str_replace('[Month]', date("m"), $sReceiptNo);				
				$sReceiptNo = str_replace('[Id]', $iReceiptId, $sReceiptNo);
				
				$sReceiptNo = '<input type="text" name="txtReceiptNo" id="txtReceiptNo" class="form1" value="' . $sReceiptNo . '" size="30" />';
				
				if($iInvoiceId > 0)
				{
					$varResult = $objDatabase->Query("
					SELECT *
					FROM fms_customers_invoices AS CI
					INNER JOIN fms_customers_invoices_items AS CII ON CII.InvoiceId = CI.InvoiceId
					WHERE CI.OrganizationId='" . cOrganizationId . "' AND CI.InvoiceId= '$iInvoiceId'");
					for($k = 0; $k < $objDatabase->RowsNumber($varResult); $k++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';					
						$sRowVisible = 'visible';
						
						$sProductName = $objDatabase->Result($varResult, $k, "CII.ProductName");					
						$iQuantity= $objDatabase->Result($varResult, $k, "CII.Quantity");
						$dUnitPrice = $objDatabase->Result($varResult, $k, "CII.UnitPrice");
						$dAmount = $objDatabase->Result($varResult, $k, "CII.Amount");
						$dTotalAmount += $dAmount;
												
						$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%" readonly="true"/></td>
						 <td align="right"><input style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' .$dUnitPrice . '" size="10" readonly="true"/></td>
						 <td align="center"><input style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" readonly="true" /></td> 
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">'. number_format($dAmount, 0) . '</div></td>
						 <td>&nbsp;</td>
						 <!--<td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td>-->
						</tr>';
					}					
					
					$sOrderParticulars_Rows .= '
					<!--
					<tr bgcolor="#ffffff">
					 <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
					</tr>
					-->
					<tr bgcolor="#ffffff">
					 <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
					 <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">'. number_format($dTotalAmount, 0) . '</div></td>
					</tr>
					</table>
					<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="'. $dTotalAmount . '" />';
					
				}
				else
				{
				
					for ($k=0; $k < 50; $k++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						/*
						$br = strtolower($_SERVER['HTTP_USER_AGENT']); // what browser they use.										
						if(ereg("msie", $br)) $sStyle = "visible";
						else $sStyle = "table-row";
						*/
						$sStyle = 'visible';
						$sRowVisible = $sStyle;
						if($k > 4) $sRowVisible = 'none';
						
						$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" size="3" /></td> 
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '"></div></td>
						 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
						</tr>';
					}
					
					$sOrderParticulars_Rows .= '
					<tr bgcolor="#ffffff">
					 <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
					</tr>
					<tr bgcolor="#ffffff">
					 <td align="right" colspan="4" style="font-size:13px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
					 <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderTotalAmount" name="divOrderTotalAmount"></div></td>
					</tr>
					</table>
					<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="" />
					<script type="text/javascript" language="JavaScript">
					 var iRowCounter = ' . $k . ';
					 var iRowVisible = 6;	
					</script>';
				}
				
				$sReceiptItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				 ' . $sOrderParticulars_Rows;
				
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">			
			function ValidateForm()
			{
				if (GetVal(\'hdnCustomerId\') == "") return(AlertFocus(\'Please enter a valid Customer Name\', \'txtCustomerName\'));				
				if (!isDate(GetVal(\'txtReceiptDate\'))) return(AlertFocus(\'Please enter a valid Due Date\', \'txtReceiptDate\'));	
				//if(GetSelectedListBox(\'selInvoice\') <= 0)	return(AlertFocus(\'Please select Invoice No\', \'selInvoice\'));
									
				return true;
			}
			</script>				
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Receipt Information:</span></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200px;">Customer Name:</td><td><strong>' . $sCustomerName . '</strong></td></tr> 
			 <tr bgcolor="#edeff1"><td valign="top">Invoice No:</td><td><strong>' . $sInvoiceNo . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Receipt No:</td><td><strong>' . $sReceiptNo . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Receipt Date:</td><td><strong>' . $sReceiptDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Payment Method:</td><td><strong>' . $sPaymentMethod . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Receipt Items:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="2">' . $sReceiptItems . '</td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Receipt Added On:</td><td><strong>' . $sReceiptAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Receipt Added By:</td><td><strong>' . $sReceiptAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Receipt" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iReceiptId . '"><input type="hidden" name="action" id="action" value="UpdateReceipt"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Receipt" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewReceipt"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewReceipt($iCustomerId, $iInvoiceId, $sReceiptNo, $dTotalAmount, $iPaymentMethod, $dReceiptDate, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[1] == 0) // Add Disabled
			return(1010);

		$varNow = $objGeneral->fnNow();
		$iReceiptAddedBy = $objEmployee->iEmployeeId;	
		
		$sReceiptNo = $objDatabase->RealEscapeString($sReceiptNo); 
		$sReceiptNo = $objDatabase->RealEscapeString($sReceiptNo);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($dTotalAmount == "") $dTotalAmount = 0;		
		
		if($objDatabase->DBCount("fms_customers_receipts AS R", "R.OrganizationId='" . cOrganizationId . "' AND R.ReceiptNo = '$sReceiptNo'") > 0)
			return(11000);
		
		$varResult = $objDatabase->Query("INSERT INTO fms_customers_receipts
		(OrganizationId, CustomerId, InvoiceId, ReceiptNo, TotalAmount, PaymentMethod, ReceiptDate, Description, Notes, ReceiptAddedOn, ReceiptAddedBy)
		VALUES ('" . cOrganizationId . "', '$iCustomerId', '$iInvoiceId', '$sReceiptNo', '$dTotalAmount', '$iPaymentMethod', '$dReceiptDate', '$sDescription', '$sNotes', '$varNow', '$iReceiptAddedBy')");

		if ($varResult)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_customers_receipts AS R WHERE R.OrganizationId='" . cOrganizationId . "' AND R.ReceiptNo='$sReceiptNo' AND R.ReceiptAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iReceiptId = $objDatabase->Result($varResult, 0, "R.ReceiptId");
				
				for ($k=0; $k < 50; $k++)
				{					
					$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));					
					
					$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
					$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
					$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
					
					if(($sProductName != "") && ($iProductQty > 0))
						$this->AddReceiptItems($iReceiptId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
					
				}
				
				$objSystemLog->AddLog("Add New Receipt - " . $sReceiptNo);
				$objGeneral->fnRedirect('?pagetype=details&error=11001&receiptid=' . $objDatabase->Result($varResult, 0, "R.ReceiptId"));
			}
		}

		return(11002);
	}
	
	function UpdateReceipt($iReceiptId, $sReceiptNo, $dTotalAmount, $iPaymentMethod, $dReceiptDate, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[2] == 0) // Update Disabled
			return(1010);
		
		$sReference = $objDatabase->RealEscapeString($sReference);
		$sReceiptNo = $objDatabase->RealEscapeString($sReceiptNo);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		//if($objDatabase->DBCount("fms_customers_receipts AS R", "R.OrganizationId='" . cOrganizationId . "' AND R.ReceiptNo = '$sReceiptNo' AND R.ReceiptId <> '$iReceiptId'") > 0)
			//return(11000);
			
		if($dTotalAmount == "") $dTotalAmount = 0;
		if($dAppliedCredit == "") $dAppliedCredit = 0;
		
		$varResult = $objDatabase->Query("
		UPDATE fms_customers_receipts 
		SET						
			ReceiptNo='$sReceiptNo',			
			TotalAmount='$dTotalAmount',
			PaymentMethod='$iPaymentMethod',
			ReceiptDate='$dReceiptDate',
			Description='$sDescription', 
			Notes='$sNotes'
		WHERE OrganizationId='" . cOrganizationId . "' AND ReceiptId='$iReceiptId'");

		$varResult = $objDatabase->Query("DELETE FROM fms_customers_receipts_items WHERE ReceiptId='$iReceiptId'");
		// Add SalesOrder Items
		for ($k=0; $k < 50; $k++)
		{					
			$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));
			$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
			$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
			$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);
			
			if(($sProductName != "") && ($iProductQty > 0))
				$this->AddReceiptItems($iReceiptId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
			
		}	
		
		$objSystemLog->AddLog("Update Receipt - " . $sReceiptNo);
		$objGeneral->fnRedirect('../customers/receipts.php?pagetype=details&error=11003&receiptid=' . $iReceiptId . '&id=' . $iInvoiceId);
		//return(3002);
		
	}
	
	function DeleteReceipt($iReceiptId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_Receipts[3] == 0) // Delete Disabled
	    {
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iReceiptId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_customers_receipts AS R WHERE R.OrganizationId='" . cOrganizationId . "' AND R.ReceiptId='$iReceiptId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(11007);

		$sReceiptNo = $objDatabase->Result($varResult, 0, "R.ReceiptNo");
		$sReceiptNo = $objDatabase->RealEscapeString($sReceiptNo);
		
		$varResult = $objDatabase->Query("DELETE FROM fms_customers_receipts WHERE ReceiptId='$iReceiptId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_customers_receipts_items WHERE ReceiptId='$iReceiptId'");
			
			$objSystemLog->AddLog("Delete Receipt No - " . $sReceiptNo);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iSalesOrderId . '&error=11005');
			else
				$objGeneral->fnRedirect('?id=0&error=11005');
		}
		else
			return(11006);
	}
	
	function AddReceiptItems($iReceiptId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount)
	{
		global $objDatabase;
		global $objGeneral;		
		$sProductName = $objDatabase->RealEscapeString($sProductName);
		if($iProductQty == "" ) $iProductQty = 0;
		if($dProductUnitPrice == "" ) $dProductUnitPrice = 0;
		if($dProductTotalAmount == "" ) $dProductTotalAmount = 0;
		
		$varResult2 = $objDatabase->Query("INSERT INTO fms_customers_receipts_items
		(ReceiptId, ProductName, Quantity, UnitPrice, Amount)
		VALUES ('$iReceiptId', '$sProductName', '$iProductQty', '$dProductUnitPrice', '$dProductTotalAmount')");
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
		
		
	}
	

}

?>