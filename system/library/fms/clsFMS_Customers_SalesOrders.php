<?php

// Class: SalesOrder
class clsCustomers_SalesOrders
{
	public $aShippingMethod;
	public $aSalesOrderStatus;
	// Class Constructor
	function __construct()
	{
		$this->aShippingMethod = array("By Road", "By Plane");
		$this->aSalesOrderStatus = array("Postponed", "Cancelled", "Approved");
	}
	
	function ShowAllSalesOrders($sSearch, $iSalesOrderStatus = -1)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		if ($iSalesOrderStatus== '') $iSalesOrderStatus = -1;
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Sales Orders";
		if ($sSortBy == "") $sSortBy = "SO.SalesOrderId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((SO.OrderNo LIKE '%$sSearch%') OR (SO.SalesOrderId LIKE '%$sSearch%')OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (Q.QuotationNo LIKE '%$sSearch%')) ";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}
		if ($iSalesOrderStatus >= 0) $sStatusCondition = " AND SO.STATUS='$iSalesOrderStatus'";

		$iTotalRecords = $objDatabase->DBCount("fms_customers_salesorders AS SO LEFT JOIN fms_customers_quotations AS Q ON Q.QuotationId = SO.QuotationId INNER JOIN fms_customers AS C ON C.CustomerId = SO.CustomerId INNER JOIN organization_employees AS E ON E.EmployeeId = SO.SalesOrderAddedBy", "SO.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sStatusCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_customers_salesorders AS SO
		LEFT JOIN fms_customers_quotations AS Q ON Q.QuotationId = SO.QuotationId
		INNER JOIN fms_customers AS C ON C.CustomerId = SO.CustomerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = SO.SalesOrderAddedBy
		WHERE SO.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sStatusCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Customer Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Customer Name in Ascending Order" title="Sort by Customer Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.FirstName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Customer Name in Descending Order" title="Sort by Customer Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Order No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=SO.OrderNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Order No in Ascending Order" title="Sort by Order No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=SO.OrderNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Order No in Descending Order" title="Sort by Order No in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Quotation No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Quotation No in Ascending Order" title="Sort by Quotation No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Quotation No in Descending Order" title="Sort by Quotation No in Descending Order" border="0" /></a></span></td>		  
		  <td align="left"><span class="WhiteHeading">Delivery Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=SO.DeliveryDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Delivery Date in Ascending Order" title="Sort by Delivery Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=SO.DeliveryDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Delivery Date in Descending Order" title="Sort by Delivery Date in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=SO.TotalAmount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Amount in Ascending Order" title="Sort by Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=SO.TotalAmount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Amount in Descending Order" title="Sort by Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=SO.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=SO.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iSalesOrderId = $objDatabase->Result($varResult, $i, "SO.SalesOrderId");
			$iQuotationId = $objDatabase->Result($varResult, $i, "SO.QuotationId");
			
			if($iQuotationId > 0)
				$sQuotationNo = $objDatabase->Result($varResult, $i, "Q.QuotationNo");
			else
				$sQuotationNo = "";
			
			$sOrderNo = $objDatabase->Result($varResult, $i, "SO.OrderNo");
			$sOrderNo = $objDatabase->RealEscapeString($sOrderNo);
			$sOrderNo = stripslashes($sOrderNo);
			
			$sFirstName = $objDatabase->Result($varResult, $i, "C.FirstName");
			$sFirstName = $objDatabase->RealEscapeString($sFirstName);
			$sFirstName = stripslashes($sFirstName);
			
			$sLastName = $objDatabase->Result($varResult, $i, "C.LastName");
			$sLastName = $objDatabase->RealEscapeString($sLastName);
			$sLastName = stripslashes($sLastName);
			$sCustomerName = $sFirstName . ' ' . $sLastName;
			
			$dDeliveryDate = $objDatabase->Result($varResult, $i, "SO.DeliveryDate");
			$sDeliveryDate = date("F j, Y", strtotime($dDeliveryDate));

			$iStatus = $objDatabase->Result($varResult, $i, "SO.Status");
			$sStatus = $this->aSalesOrderStatus[$iStatus];
			
			//$dSalesOrderDate = $objDatabase->Result($varResult, $i, "SO.SalesOrderDate");
			//$sSalesOrderDate = date("F j, Y", strtotime($dSalesOrderDate));
						
			$sBackOrder = $objDatabase->Result($varResult, $i, "SO.BackOrder");
			$sBackOrder = $objDatabase->RealEscapeString($sBackOrder);
			$sBackOrder = stripslashes($sBackOrder);
			
			$dAmount = $objDatabase->Result($varResult, $i, "SO.TotalAmount");
			$sAmount = number_format($dAmount, 0);
			
			$dSalesOrderAddedOn = $objDatabase->Result($varResult, $i, "SO.SalesOrderAddedOn");
			$sSalesOrderAddedOn = date("F j, Y", strtotime($dSalesOrderAddedOn)) . ' at ' . date("g:i a", strtotime($dSalesOrderAddedOn));
			
			$sEditSalesOrder = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update Sales order' . $objDatabase->RealEscapeString(str_replace('"', '', $sOrderNo))  . '\', \'../customers/salesorders.php?pagetype=details&action2=edit&salesorderid=' . $iSalesOrderId. '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Sales Order Details" title="Edit this Sales Order Details"></a></td>';
			$sDeleteSalesOrder = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Sales Order?\')) {window.location = \'?action=DeleteSalesOrder&salesorderid=' . $iSalesOrderId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Sales Order" title="Delete this Sales Order"></a></td>';
			$sSalesOrderReceipt = '<td class="GridTD" align="center"><a href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=CustomerReports&reporttype=SalesOrder&selEmployee=-1&selStation=-1&txtId=' . $iSalesOrderId . '\', 800,600);"><img src="../images/icons/iconPrint2.gif" border="0" alt="Sales Order Receipt" title="Sales Order Receipt"></a></td>';			
			$sDocuments = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/documents_show.php?componentname=Customers_SalesOrders&id=' . $iSalesOrderId . '\', \'Documents of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sOrderNo)) . '\', \'700 420\');"><img src="../images/icons/save.gif" border="0" alt="Sales Order Documents" title="Sales Order Documents"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[2] == 0)	$sEditSalesOrder = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[3] == 0)	$sDeleteSalesOrder = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders_Documents[0] == 0)	$sDocuments = '<td class="GridTD">&nbsp;</td>';	
				
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sCustomerName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sOrderNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>			 
			 <td class="GridTD" align="left" valign="top">' . $sQuotationNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sDeliveryDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . $sAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View' . $objDatabase->RealEscapeString(str_replace('"', '', $sOrderNo)) . '\', \'../customers/salesorders.php?pagetype=details&salesorderid=' . $iSalesOrderId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Sales Order Details" title="View this Sales Order Details"></a></td>
			 ' . $sEditSalesOrder . $sSalesOrderReceipt. $sDocuments . $sDeleteSalesOrder . '</tr>';
		}

		$sAddSalesOrder = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Sales Order\', \'../customers/salesorders.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add Sales Order">';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[1] == 0) // Add Disabled
			$sAddSalesOrder = '';
		
		$sSalesOrderStatusSelect = '<select class="form1" name="selSalesOrderStatus" onchange="window.location=\'?status=\'+GetSelectedListBox(\'selSalesOrderStatus\');" id="selSalesOrderStatus">
		<option value="-1">Sales Orders with All Status</option>';
		for ($i=0; $i < count($this->aSalesOrderStatus); $i++)
			$sSalesOrderStatusSelect .= '<option ' . (($iSalesOrderStatus== $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aSalesOrderStatus[$i] . '</option>';
		$sSalesOrderStatusSelect .= '</select>';
			
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddSalesOrder . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Sales Order:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Sales Order" title="Search for a Sales Order" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		  <tr>
		  <td colspan="2">' .  $sSalesOrderStatusSelect . '</td>
		 </tr>	
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Sales Order Details" alt="View this Sales Order Details"></td><td>View this Sales Order Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Sales Order Details" alt="Edit this Sales Order Details"></td><td>Edit this Sales Order Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Sales Order" alt="Delete this Sales Order"></td><td>Delete this Sales Order</td></tr>
		</table>';

		return($sReturn);
	}
	
	function SalesOrderDetails($iSalesOrderId, $iQuotationId = 0, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		$k = 0;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
				
		include(cVSFFolder . '/classes/clsjQuery.php');
		
		$objjQuery = new clsjQuery();
		
		$objCustomers = new clsCustomers_Customers();
		$aCustomers = $objCustomers->CustomersList();
				
		$objQueryCustomer = new clsjQuery($aCustomers);
				
		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_customers_salesorders AS SO
		LEFT JOIN fms_customers_quotations AS Q ON Q.QuotationId = SO.QuotationId
		INNER JOIN fms_customers AS C ON C.CustomerId = SO.CustomerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = SO.SalesOrderAddedBy
		WHERE SO.OrganizationId='" . cOrganizationId . "' AND SO.SalesOrderId = '$iSalesOrderId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&salesorderid=' . $iSalesOrderId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this SalesOrder" title="Edit this SalesOrder" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this SalesOrder?\')) {window.location = \'?pagetype=details&action=DeleteSalesOrder&salesorderid=' . $iSalesOrderId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Sales Order" title="Delete this Sales Order" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[3] == 0)	$sButtons_Delete = '';
					
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Sales Order Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iSalesOrderId = $objDatabase->Result($varResult, 0, "SO.SalesOrderId");
				$iQuotationId = $objDatabase->Result($varResult, 0, "SO.QuotationId");
				
				if($iQuotationId > 0)
				{
					$sQuotationNo = $objDatabase->Result($varResult, 0, "Q.QuotationNo");
					$sQuotationNo = $objDatabase->RealEscapeString($sQuotationNo);
					$sQuotationNo = stripslashes($sQuotationNo);
					$sQuotationNo .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View ' . $sQuotationNo . '\', \'../customers/quotations.php?pagetype=details&quotationid=' . $iQuotationId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Quotation Information" title="Quotation Information" border="0" /></a><br />';
				}
				else $sQuotationNo = "No Quotation";
				
				$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
				$sCustomerName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName");
				$sCustomerName = $objDatabase->RealEscapeString($sCustomerName);
				$sCustomerFullName = $sCustomerName;
				$sCustomerName .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View ' . $sCustomerName . '\', \'../customers/customers.php?pagetype=details&id=' . $iCustomerId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Customer Information" title="Customer Information" border="0" /></a><br />';					
				
				$sOrderNo = $objDatabase->Result($varResult, 0, "SO.OrderNo");
				$sOrderNo = $objDatabase->RealEscapeString($sOrderNo);
				$sOrderNo = stripslashes($sOrderNo);
					
				$dDeliveryDate = $objDatabase->Result($varResult, 0, "SO.DeliveryDate");
				$sDeliveryDate = date("F j, Y", strtotime($dDeliveryDate));
				
				$iStatus = $objDatabase->Result($varResult, $i, "SO.Status");
				$sStatus = $this->aSalesOrderStatus[$iStatus];
					
				$dSalesOrderDate = $objDatabase->Result($varResult, 0, "SO.SalesOrderDate");
				$sSalesOrderDate = date("F j, Y", strtotime($dSalesOrderDate));
				/*
				$sBackOrder = $objDatabase->Result($varResult, 0, "SO.BackOrder");
				$sBackOrder = $objDatabase->RealEscapeString($sBackOrder);
				$sBackOrder = stripslashes($sBackOrder);
				
				$iShippingMethod = $objDatabase->Result($varResult, 0, "SO.ShippingMethod");
				$sShippingMethod = $this->aShippingMethod[$iShippingMethod];
				*/
				$dTotalAmount = $objDatabase->Result($varResult, 0, "SO.TotalAmount");
				$sTotalAmount = number_format($dTotalAmount, 0);
				
				$sDescription = $objDatabase->Result($varResult, 0, "SO.Description");
				$sDescription = $objDatabase->RealEscapeString($sDescription);
				$sDescription = stripslashes($sDescription);
				
				$sNotes = $objDatabase->Result($varResult, 0, "SO.Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
				
				$dSalesOrderAddedOn = $objDatabase->Result($varResult, 0, "SO.SalesOrderAddedOn");
				$sSalesOrderAddedOn = date("F j, Y", strtotime($dSalesOrderAddedOn)) . ' at ' . date("g:i a", strtotime($dSalesOrderAddedOn));			
				
				$iSalesOrderAddedBy = $objDatabase->Result($varResult, 0, "E.EmployeeId");
				
				$sSalesOrderAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sSalesOrderAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sSalesOrderAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iSalesOrderAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				
				$varResult2 = $objDatabase->Query("
				SELECT *
				FROM fms_customers_salesorders_items AS SOI				
				WHERE SOI.SalesOrderId = '$iSalesOrderId'");
								
				 for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';
										
					$sProductName = $objDatabase->Result($varResult2, $i, "SOI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $i, "SOI.Quantity");
					$dUnitPrice = $objDatabase->Result($varResult2, $i, "SOI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $i, "SOI.Amount");				
					
					$sOrderParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . ($i+1) . '</td>					 
 					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;">' . $sProductName . '</td>					 					 
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . $iQuantity . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>
					</tr>';
				}
				
				$sSalesOrderItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="center"><span class="WhiteHeading">Amount</span></td>
				 </tr>
				 ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:18px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</td>
				 </tr>				 
				</table>';
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sQuotationNo = '<select name="selQuotation" class="form1" id="selQuotation">
				<option value="0"> No Quotation </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_customers_quotations AS Q WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.Status='2' ORDER BY Q.QuotationNo");
				if($objDatabase->RowsNumber($varResult) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sQuotationNo .='<option ' . (($iQuotationId == $objDatabase->Result($varResult, $i, "Q.QuotationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "Q.QuotationId") . '" >' . $objDatabase->Result($varResult, $i, "Q.QuotationNo") . '</option>';
				}				
				$sQuotationNo .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selQuotation\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selQuotation\'), \'../customers/quotations.php?pagetype=details&quotationid=\'+GetSelectedListBox(\'selQuotation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Quotation Details" title="Quotation Details" /></a>';
				
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
				
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';				
				for($i = 0; $i < count($this->aSalesOrderStatus); $i++)
				{
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aSalesOrderStatus[$i];
				}
				$sStatus .='</select>';
											
				$sOrderNo = '<input type="text" name="txtOrderNo" id="txtOrderNo" class="form1" value="' . $sOrderNo . '" size="15" />&nbsp;<span style="color:red;">*</span>';				
				$sDeliveryDate = '<input size="18" type="text" id="txtDeliveryDate" name="txtDeliveryDate" class="form1" value="' . $dDeliveryDate . '" />' . $objjQuery->Calendar('txtDeliveryDate');
				$sSalesOrderDate = '<input size="18" type="text" id="txtSalesOrderDate" name="txtSalesOrderDate" class="form1" value="' . $dSalesOrderDate . '" />' . $objjQuery->Calendar('txtSalesOrderDate');
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sOrderParticulars_Rows = "";				
				$varResult2 = $objDatabase->Query("
				SELECT *
				FROM fms_customers_salesorders_items AS SOI				
				WHERE SOI.SalesOrderId = '$iSalesOrderId'");
						       	
		        for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';			
					
					$sProductName = $objDatabase->Result($varResult2, $k, "SOI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $k, "SOI.Quantity");					
					$dUnitPrice = $objDatabase->Result($varResult2, $k, "SOI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $k, "SOI.Amount");					
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');"  type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">' . number_format($dAmount, 0) . '</div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				for ($j = $k; $j < 50; $j++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'none';
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				$sSalesOrderItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="center"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				  ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="center" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</div></td>
				 </tr>
				</table>
				<input type="hidden" name="TotalPayment" id="TotalPayment" value="" />
				<input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="' . $dTotalAmount . '" />
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $j . ';
				 var iRowVisible = ' . ($k +1) . ';
				</script>';
				
			}
			else if ($sAction2 == "addnew")
			{
				$iSalesOrderId = "";
				$iQuotationId = $objGeneral->fnGet("quotationid");
				$sOrderNo = $objGeneral->fnGet("txtOrderNo");
				$sBackOrder = $objGeneral->fnGet("txtBackOrder");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$dAmount_SO = $objGeneral->fnGet("txtAmount");
				$dDeliveryDate = date("Y-m-d");
				$dSalesOrderDate = date("Y-m-d");
				$iStatus = 2;
				
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sSalesOrderAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sSalesOrderAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sSalesOrderAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sQuotationNo = '<select name="selQuotation" class="form1" onchange="window.location=\'?pagetype=details&action2=addnew&quotationid=\'+GetSelectedListBox(\'selQuotation\');" id="selQuotation">
				<option value="0"> No Quotation </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_customers_quotations AS Q WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.Status='2' ORDER BY Q.QuotationNo");
				if($objDatabase->RowsNumber($varResult) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sQuotationNo .='<option ' . (($iQuotationId == $objDatabase->Result($varResult, $i, "Q.QuotationId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "Q.QuotationId") . '" >' . $objDatabase->Result($varResult, $i, "Q.QuotationNo") . '</option>';
				}				
				$sQuotationNo .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selQuotation\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selQuotation\'), \'../customers/quotations.php?pagetype=details&quotationid=\'+GetSelectedListBox(\'selQuotation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Quotation Details" title="Quotation Details" /></a>';
				
				if($iQuotationId > 0)
				{
					$varResult = $objDatabase->Query("
					SELECT *
					FROM fms_customers_quotations AS Q
					INNER JOIN fms_customers AS C ON C.CustomerId = Q.CustomerId					
					WHERE Q.OrganizationId='" . cOrganizationId . "' Q.QuotationId= '$iQuotationId'");
					$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
					$sCustomerFullName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName");					
				}
								
				$sCustomerName = $objQueryCustomer->AutoCompleteLocal("Customers", "hdnCustomerId", "width:200px; font-family:Tahoma,Arial; font-size:14px;", true, $sCustomerFullName, $iCustomerId);
				
				$sStatus = '<select name="selStatus" id="selStatus" class="form1">';
				for($i = 0; $i < count($this->aSalesOrderStatus); $i++)
				{
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . 'value="' . $i . '">' . $this->aSalesOrderStatus[$i];
				}
				$sStatus .='</select>';			
				
				
				$sOrderNo = '<input type="text" name="txtOrderNo" id="txtOrderNo" class="form1" value="' . $sOrderNo . '" size="15" />&nbsp;<span style="color:red;">*</span>';
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sDeliveryDate = '<input size="18" type="text" id="txtDeliveryDate" name="txtDeliveryDate" class="form1" value="' . $dDeliveryDate . '" />' . $objjQuery->Calendar('txtDeliveryDate');
				$sSalesOrderDate = '<input size="18" type="text" id="txtSalesOrderDate" name="txtSalesOrderDate" class="form1" value="' . $dSalesOrderDate . '" />' . $objjQuery->Calendar('txtSalesOrderDate');
				$sSalesOrderAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));				
				$dTotalAmount = 0;
				
				if($iQuotationId > 0)
				{
					$varResult = $objDatabase->Query("
					SELECT *
					FROM fms_customers_quotations AS Q
					INNER JOIN fms_customers_quotations_items AS QI ON QI.QuotationId = Q.QuotationId
					WHERE Q.QuotationId= '$iQuotationId'");
					for($k = 0; $k < $objDatabase->RowsNumber($varResult); $k++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';					
						$sRowVisible = 'visible';
						
						$sProductName = $objDatabase->Result($varResult, $k, "QI.ProductName");
						$iQuantity= $objDatabase->Result($varResult, $k, "QI.Quantity");
						$dUnitPrice = $objDatabase->Result($varResult, $k, "QI.UnitPrice");
						$dAmount = $objDatabase->Result($varResult, $k, "QI.Amount");
						$dTotalAmount += $dAmount;						
						$iAvailabeQuantity = $iQuantityRemaining + $iWarehouseQuantity;
						
						$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td> 
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">'. $dAmount . '</div></td>
						 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
						</tr>';
					}
					for ($j = $k; $j < 50; $j++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						$sRowVisible = 'none';
						
						$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="3" /></td>
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
						 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
						</tr>';
					}
					
					$sOrderParticulars_Rows .= '
					<tr bgcolor="#ffffff">
					 <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
					</tr>
					<tr bgcolor="#ffffff">
					 <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
					 <td align="center" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">'. number_format($dTotalAmount, 0) . '</div></td>
					</tr>
					</table>
					<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="'. $dTotalAmount . '" />
					<script type="text/javascript" language="JavaScript">
					 var iRowCounter = ' . $j . ';
					 var iRowVisible = ' . ($k +1) . ';
					</script>';
					
				}				
				else
				{
					for ($k=0; $k < 50; $k++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						$sRowVisible = (($k <= 4) ? 'visible' : 'none');
						
						$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" size="3" /></td> 
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '"></div></td>
						 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
						</tr>';
					}
					
					$sOrderParticulars_Rows .= '
					<tr bgcolor="#ffffff">
					 <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
					</tr>
					<tr bgcolor="#ffffff">
					 <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
					 <td align="center" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount"></div></td>
					</tr>
					</table>
					<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="" />
					<script type="text/javascript" language="JavaScript">
					 var iRowCounter = ' . $k . ';
					 var iRowVisible = 6;	
					</script>';
				}
				
				$sSalesOrderItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				 ' . $sOrderParticulars_Rows;
				
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">			
			function ValidateForm()
			{
				if (GetVal(\'hdnCustomerId\') == "") return(AlertFocus(\'Please enter a valid Customer Name\', \'txtCustomerName\'));
				if (GetVal(\'txtOrderNo\') == "") return(AlertFocus(\'Please enter a valid Order No\', \'txtOrderNo\'));
				if(!isDate(GetVal(\'txtSalesOrderDate\'))) return(AlertFocus(\'Please Enter a valid Sales Order date\',\'txtSalesOrderDate\'));
				if(!isDate(GetVal(\'txtDeliveryDate\'))) return(AlertFocus(\'Please Enter a valid delivery date\',\'txtDeliveryDate\'));
				
				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Sales Order Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" style="width:200px;">Customer Name:</td><td><strong>' . $sCustomerName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Quotation No:</td><td><strong>' . $sQuotationNo . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Order No:</td><td><strong>' . $sOrderNo . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Order Date:</td><td><strong>' . $sSalesOrderDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Delivery Date:</td><td><strong>' . $sDeliveryDate . '</strong></td></tr>			 
			 <tr bgcolor="#ffffff"><td valign="top">Status:</td><td><strong>' . $sStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Sales Order Items:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">' . $sSalesOrderItems . '</td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Sales Order Added On:</td><td><strong>' . $sSalesOrderAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Sales Order Added By:</td><td><strong>' . $sSalesOrderAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Sales Order" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iSalesOrderId . '"><input type="hidden" name="action" id="action" value="UpdateSalesOrder"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Sales Order" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewSalesOrder"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewSalesOrder($iCustomerId, $iQuotationId, $sOrderNo, $dDeliveryDate, $dSalesOrderDate, $dTotalAmount, $sDescription, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[1] == 0) // Add Disabled
			return(1010);

		$varNow = $objGeneral->fnNow();
		$iSalesOrderAddedBy = $objEmployee->iEmployeeId;
		
		$sOrderNo = $objDatabase->RealEscapeString($sOrderNo);
		$sBackOrder = $objDatabase->RealEscapeString($sBackOrder);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_customers_salesorders AS SO", "SO.OrganizationId='" . cOrganizationId . "' AND SO.OrderNo = '$sOrderNo'") > 0) return(12000);
		// Check for Sale Order against specified Quotation
		if($iQuotationId > 0)
		{			
			if($objDatabase->DBCount("fms_customers_salesorders AS SO", "SO.OrganizationId='" . cOrganizationId . "' AND SO.QuotationId = '$iQuotationId'") > 0) return(12008);			
			// Get Customer Id from Quotation			
			$varResult = $objDatabase->Query("
			SELECT *
			FROM fms_customers_quotations AS Q
			INNER JOIN fms_customers AS C ON C.CustomerId = Q.CustomerId
			WHERE Q.QuotationId= '$iQuotationId'");
			$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
		}
		
		
		$bCheck = $objDatabase->Query("INSERT INTO fms_customers_salesorders
		(OrganizationId, CustomerId, QuotationId, OrderNo, DeliveryDate, SalesOrderDate, TotalAmount, Description, Status, Notes, SalesOrderAddedOn, SalesOrderAddedBy)
		VALUES ('" . cOrganizationId . "', '$iCustomerId', '$iQuotationId', '$sOrderNo', '$dDeliveryDate', '$dSalesOrderDate', '$dTotalAmount', '$sDescription', '$iStatus', '$sNotes', '$varNow', '$iSalesOrderAddedBy')");

		if ( $bCheck )
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_customers_salesorders AS SO WHERE SO.OrganizationId='" . cOrganizationId . "' AND SO.OrderNo='$sOrderNo' AND SO.SalesOrderAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iSalesOrderId = $objDatabase->Result($varResult, 0, "SO.SalesOrderId");
				
				for ($k=0; $k < 50; $k++)
				{					
					$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));					
					
					$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
					$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
					$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
					
					if(($sProductName != "") && ($iProductQty > 0))
						$this->AddSalesOrderItems($iSalesOrderId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
					
				}
				
				$objSystemLog->AddLog("Add New Sales Order - " . $sOrderNo);
				$objGeneral->fnRedirect('?pagetype=details&error=12001&salesorderid=' . $objDatabase->Result($varResult, 0, "SO.SalesOrderId"));
			}
		}

		return(12002);
	}
	
	function UpdateSalesOrder ($iSalesOrderId, $iCustomerId, $iQuotationId, $sOrderNo, $dDeliveryDate, $dSalesOrderDate, $dTotalAmount, $sDescription, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[2] == 0) // Update Disabled
			return(1010);		
		
		$sOrderNo = $objDatabase->RealEscapeString($sOrderNo);
		$sBackOrder = $objDatabase->RealEscapeString($sBackOrder);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);

		if($objDatabase->DBCount("fms_customers_salesorders AS SO", "SO.OrganizationId='" . cOrganizationId . "' AND SO.OrderNo = '$sOrderNo' AND SO.SalesOrderId <> '$iSalesOrderId'") > 0) return(12000);
		// Check for Sale Order against specified Quotation
		if($iQuotationId > 0)
		{
			
			if($objDatabase->DBCount("fms_customers_salesorders AS SO", "SO.OrganizationId='" . cOrganizationId . "' AND SO.SalesOrderId <> '$iSalesOrderId' AND SO.QuotationId = '$iQuotationId'") > 0) return(12009);
			
			// Get Customer Id from Quotation			
			$varResult = $objDatabase->Query("
			SELECT *
			FROM fms_customers_quotations AS Q
			INNER JOIN fms_customers AS C ON C.CustomerId = Q.CustomerId
			WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationId= '$iQuotationId'");
			$iCustomerId = $objDatabase->Result($varResult, 0, "C.CustomerId");
		}
		
		$varResult = $objDatabase->Query("
		UPDATE fms_customers_salesorders 
		SET
			CustomerId = '$iCustomerId',	
			QuotationId='$iQuotationId', 
			OrderNo='$sOrderNo', 
			DeliveryDate='$dDeliveryDate', 
			SalesOrderDate='$dSalesOrderDate',			
			TotalAmount='$dTotalAmount', 
			Description='$sDescription',
			Status='$iStatus',
			Notes='$sNotes'			
		WHERE OrganizationId='" . cOrganizationId . "' AND SalesOrderId='$iSalesOrderId'");

		$varResult = $objDatabase->Query("DELETE FROM fms_customers_salesorders_items WHERE SalesOrderId='$iSalesOrderId'");
		// Add SalesOrder Items
		for ($k=0; $k < 50; $k++)
		{					
			$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));					
			
			$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
			$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
			$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
			
			if(($sProductName != "") && ($iProductQty > 0))
				$this->AddSalesOrderItems($iSalesOrderId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
			
		}
		
		$objSystemLog->AddLog("Update Sales Order - " . $sOrderNo);
		$objGeneral->fnRedirect("../customers/salesorders.php?pagetype=details&error=12003&salesorderid=" . $iSalesOrderId);			
		
	}
	
	function DeleteSalesOrder($iSalesOrderId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Customers_SalesOrders[3] == 0) // Delete Disabled
	    {
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iSalesOrderId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}		

		$varResult = $objDatabase->Query("SELECT * FROM fms_customers_salesorders AS SO WHERE SO.OrganizationId='" . cOrganizationId . "' AND SO.SalesOrderId='$iSalesOrderId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(12007);

		$sOrderNo = $objDatabase->Result($varResult, 0, "SO.OrderNo");
		$sOrderNo = $objDatabase->RealEscapeString($sOrderNo);

		$varResult = $objDatabase->Query("DELETE FROM fms_customers_salesorders WHERE SalesOrderId='$iSalesOrderId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_customers_salesorders_items WHERE SalesOrderId='$iSalesOrderId'");
			
			$objSystemLog->AddLog("Delete Sales Order - " . $sOrderNo);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iSalesOrderId . '&error=12005');
			else
				$objGeneral->fnRedirect('?id=0&error=12005');
		}
		else
			return(12006);
	}
	
	function AddSalesOrderItems($iSalesOrderId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount)
	{
		global $objDatabase;
		global $objGeneral;
		
		$sProductName = $objDatabase->RealEscapeString($sProductName);
		if($iProductQty == "" ) $iProductQty = 0;
		if($dProductUnitPrice == "" ) $dProductUnitPrice = 0;
		if($dProductTotalAmount == "" ) $dProductTotalAmount = 0;
		
		$varResult2 = $objDatabase->Query("INSERT INTO fms_customers_salesorders_items
		(SalesOrderId, ProductName, Quantity, UnitPrice, Amount)
		VALUES ('$iSalesOrderId', '$sProductName', '$iProductQty', '$dProductUnitPrice', '$dProductTotalAmount')");
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
		
		
	}
	
}

?>