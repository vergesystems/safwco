<?php

class clsFMS_Help
{
	
    function ShowFMSHelpMenu($Page)
    {
    	global $objEmployee;

    	$sIntroduction = '<a ' . (($Page == "Introduction") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../help/?page=Introduction"><img src="../images/help/iconHelp_Introduction.gif" alt="Introduction" title="Introduction" border="0" /><br />Introduction</a>';
    	$sTheOrganization = '<a ' . (($Page == "Organization") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../help/?page=Organization"><img src="../images/help/iconHelp_TheOrganization.gif" alt="The Organization" title="The Organization" border="0" /><br />The Organization</a>';
		$sAccounts = '<a ' . (($Page == "Accounts") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../help/?page=Accounts"><img src="../images/help/iconHelp_Accounts.gif" alt="Accounts" title="Accounts" border="0" /><br />Accounts</a>';
		$sVendors = '<a ' . (($Page == "Vendors") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../help/?page=Vendors"><img src="../images/help/iconHelp_Vendors.gif" alt="Vendors" title="Vendors" border="0" /><br />Vendors</a>';
		$sCustomers = '<a ' . (($Page == "Customers") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../help/?page=Customers"><img src="../images/help/iconHelp_Customers.gif" alt="Customers" title="Customers" border="0" /><br />Customers</a>';
		$sBanking = '<a ' . (($Page == "Banking") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../help/?page=Banking"><img src="../images/help/iconHelp_Banking.gif" alt="Banking" title="Banking" border="0" /><br />Banking</a>';
    	$sReports = '<a ' . (($Page == "Reports") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../help/?page=Reports"><img src="../images/help/iconHelp_Reports.gif" alt="Reports" title="Reports" border="0" /><br />Reports</a>';

    	$sReturn .= '
    	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
    	 <tr>
    	  <td valign="bottom">

    	<table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
    	 <tr>
    	  <td align="center" width="10%">' . $sIntroduction . '</td>
    	  <td align="center" width="10%">' . $sTheOrganization . '</td>
		  <td align="center" width="10%">' . $sAccounts . '</td>
		  <td align="center" width="10%">' . $sVendors . '</td>
		  <td align="center" width="10%">' . $sCustomers . '</td>
		  <td align="center" width="10%">' . $sBanking . '</td>
    	  <td align="center" width="10%">' . $sReports . '</td>
    	 </tr>
    	</table>

    	</td></tr></table>';

    	return($sReturn);
    }

    function ShowHelpPages($sPage)
	{
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		if ($sPage == "") $sPage = "Introduction";
		
		switch($sPage)
		{
			case "Introduction": 
				$aTabs[0][0] = 'Introduction';
				$aTabs[0][1] = '../help/introduction.php';
				break;
			case "Organization": 
				$aTabs[0][0] = 'The Organization Help';
				$aTabs[0][1] = '../help/theorganization.php';
				break;
			case "Accounts": 
				$aTabs[0][0] = 'Accounts Help';
				$aTabs[0][1] = '../help/accounts.php';
				break;	
			case "Vendors": 
				$aTabs[0][0] = 'Vendors Help';
				$aTabs[0][1] = '../help/vendors.php';
				break;
			case "Customers": 
				$aTabs[0][0] = 'Customers Help';
				$aTabs[0][1] = '../help/customers.php';
				break;
			case "Banking": 
				$aTabs[0][0] = 'Banking Help';
				$aTabs[0][1] = '../help/banking.php';
				break;
			case "Reports": 
				$aTabs[0][0] = 'Reports Help';
				$aTabs[0][1] = '../help/reports.php';
				break;			
		}
		
		$sReturn = $objDHTMLSuite->TabBar($aTabs, $this->ShowFMSHelpMenu($sPage));
		return($sReturn);
	}
	
	function Introduction()
    {		
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
		<div class="Help_Contents" align="justify">

		<span class="Help_Title">Introduction to Financial Management Systems</span><br />
		<br />
		Financial Management Systems are software applications that record and process accounting transactions within functional modules such as accounts payable, accounts receivable, payroll, and trial balance, thus functioning as finance/accounting information system. 
		<br /><br />
		Financial Management Systems enable companies to integrate and streamline their financial resource management processes from end to end. By doing so, they achieve a reliable view of financial performance across the entire organization, as well as the flexibility and control necessary for adapting to the demands of even the most challenging business environment.
		<br /><br />
		Financial Management Systems are used to:
		<br /><br />
		<ul>
		 <li>Improve financial transparency</li><br /><br />
		 <li>Reduce transaction costs</li><br /><br />
		 <li>Shorten process cycle times</li><br /><br />
		 <li>Achieve data consistency</li><br /><br />
		 <li>Enforce global financial standards and processes</li><br />
		</ul>
		<br />
		<span class="Help_Title">Advantages of Computerized MIS for Financial Management System (FMS)</span><br />
		<br />
		Financial Management Systems help to maintain control over multiple streams of data and provide the basis for reporting and control mechanisms required by regulating agencies. The benefits of financial management information systems usually outweigh the high cost and time requirements to implement and maintain these complex systems.
		<br /><br />
		Following are some of the specific benefits to an organization after successfully deploying an FMS:
		<br />
		<ul>
		 <li><span style="text-decoration:underline; font-weight:bold;">Control and Oversight:</span> FMS systems create control and security on financial information and data through built-in security and management controls.</li><br /><br />
		 <li><span style="text-decoration:underline; font-weight:bold;">Accounting Benefits:</span> Reporting for accounting reporting cycles such as month-end, and quarter-end requirements are simpler with FMS that can automatically generate reports, data and analysis. </li><br /><br />
		 <li><span style="text-decoration:underline; font-weight:bold;">Reporting:</span> Routine reports and ad-hoc reporting are streamlined with an FMS. The ease of data access and reliability is systematically increased.</li><br /><br />
		 <li><span style="text-decoration:underline; font-weight:bold;">Executives:</span> Executives benefit from FMS through increased reliability in data, faster reporting, and decreased possibility of fraudulent or misleading information.</li><br /><br />
		 <li><span style="text-decoration:underline; font-weight:bold;">Audit Compliance:</span> FMS can help maintain compliance of the auditors.
		</ul>

		<br /><br />
		<span class="Help_Title">Introduction to VergeFinancials:</span><br />
		<br />
		VergeFinancials is a Financial Management System (FMS) for managing Accounts and Finance of a small, medium or large organization. VergeFinancials can be used to track an organization�s income and expenses and use those figures to evaluate its financial status. VergeFinancials converts information related to accounts and finance department into a digital format, allowing that information to be added to the knowledge management systems of the organization. 
		<br /><br />
		VergeFinancials accumulates and analyzes financial data in order to make good financial management decisions in running the organization. The basic objective of VergeFinancials is to meet the organization�s financial obligations as they come due, using the minimal amount of financial resources consistent with an established margin of safety. Outputs generated by the system include accounting reports, operating and capital budgets, working capital reports, cash flow statements, and various other reports and graphs. 
		<br /><br /><br />
		<span class="Help_Title">Key Features of VergeFinancials:</span><br /><br />
		Following are some of the key features of VergeHRMS:
		<br /><br />
		<ul>
		 <li>Online web based system (easily accessible from everywhere)</li><br /><br />
		 <li>Easy to use � user friendly system</li><br /><br />
		 <li>Double entry accounting system</li><br /><br />
		 <li>Donors Project / Activity based accounting</li><br /><br />
		 <li>Flexible chart of accounts</li><br /><br />
		 <li>Branch wise accounting</li><br /><br />
		 <li>Detailed in-depth financial analysis</li><br /><br />
		 <li>Auto invoices to customers</li><br /><br />
		 <li>Single entry will auto post to Ledgers, Transactions, Financial Reports, Graphs, etc.</li><br /><br />
		 <li>Centralized online database � interactive on-the-fly reporting from each branch</li><br /><br />
		 <li>No downloading / installation needed � ready to use on any computer</li><br /><br />
		 <li>Comprehensive reporting</li><br /><br />
		 <li>Customizable reporting based on your organization�s needs</li><br /><br />
		 <li>Auto data backups to minimize data loss</li><br /><br />
		</ul>

		<hr size="1" />
		</div>
		</td></tr></table>';
		
		return($sReturn);
		
    }

    function Help_TheOrganization()
    {		
    	$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
		 <a name="#Top"></a>
		 <span class="Help_Title">The Organization Module</span><br /><br />
		<div class="Help_Contents" align="justify">
		This module deals with managing the organization / company settings, that will help in operating the entire FM System. Most of the actions in this module are to be performed once, usually when setting up the system. A certain minor changes may be required here and there, especially when there is a change in organizational structure. Components in this module are mostly for administrative purposes only, users can have the role to view information on a certain components. 
		<br /><br />
		The Organization module comprises of the following components (Click for details):
		<br /><br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		 <tr>
		  <td width="10%" align="center"><a href="#RegionalHierarchy"><img src="../images/organization/iconRegionalHierarchy.gif" title="Regional Hierarchy" alt="Regional Hierarchy" border="0" /><br />Regional Hierarchy</a></td>
		  <td width="10%" align="center"><a href="#Stations"><img src="../images/organization/iconStations.gif" title="Stations" alt="Stations" border="0" /><br />Stations</a></td>
		  <td width="10%" align="center"><a href="#Departments"><img src="../images/organization/iconDepartments.gif" title="Departments" alt="Departments" border="0" /><br />Departments</a></td>
		  <td width="10%" align="center"><a href="#DataBackup"><img src="../images/organization/iconDataBackup.gif" title="Data Backup" alt="Data Backup" border="0" /><br />Data Backup</a></td>
		  <td width="10%" align="center"><a href="#SystemSettings"><img src="../images/organization/iconSettings.gif" title="System Settings" alt="System Settings" border="0" /><br />System Settings</a></td>
		  <td width="10%" align="center"><a href="#SystemLog"><img src="../images/organization/iconSystemLog.gif" title="System Log" alt="System Log" border="0" /><br />System Log</a></td>
		 </tr>
		</table>
		<br /><br />
		<hr size="1" />

		<!-- Regional Hierarchy -->
		<a name="#RegionalHierarchy"></a><span class="Help_Title">Regional Hierarchy</span><br /><br />
		This component manages the regional hierarchy of the regions / areas where the organization works. A typical example of regional hiearchy would be an organization working in different regions, e.g. Lahore, Karachi, Sukkur and Queeta, would add the following entries:<br />
		<ul>
		 <li>Pakistan as a Country</li>
		 <li>Punjab as a Province</li>
		 <li>Balochistan as a Province</li>
		 <li>Sindh as a Province</li>
		 <li>Lahore as a City</li>
		 <li>Quetta as a City</li>
		 <li>Karachi as a City</li> 
		 <li>Sukkur as a City</li> 
		</ul>
		Regional hierarchy helps mostly in reporting, where the report is to be generated, lets say on Sindh Province, would display results from all branches in Sindh. The reason for the report displaying all branches in Sindh when province Sindh is selected is because Regional Hiearchy works like a tree, derived from top to bottom. In our example, the top branch of the tree would be Country Pakistan, then, the next level of hierarchy would be Provinces and finally the Cities at the bottom. Regional hieararchy is completely controllable by the user to allow any number of levels in the hierarchy. This will give flexibility to add further levels of regions, for example, if the user wants to add Districts, Tehsils, Talukas and even Villages.
		<br /><br />
		Following is the screenshot of a user view of Regional Hierarchy component:
		<br /><br />
		<div align="center"><img src="../images/help/Organization/RegionalHierarchy1.gif" /></div>
		<br /><br />
		Regional hierarchy component is divided in two main sections, Regions and Region Types. Region Types must be defined first, in our example following were the region types that were added in the system: Country, Province, City. After adding Region Types, the user will add different regions by selecting its region type, in our example Karachi is listed as a City, and so on.
		<br /><br />
		<span class="Help_Title2">How to add a new Region Type:</span><br />
		To add a new region type, click on Region Types tab, then click on Add Region Type button <img src="../images/help/Organization/RegionalHierarchy2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Region Type <img src="../images/help/Organization/RegionalHierarchy2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new region type cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Region Types:</span><br />
		Following operations can be performed on a selected region type:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Region Type Details" title="View this Region Type Details" /></td>
		  <td><strong>View this Region Type Details:</strong> This will open a window that will display all of the details of the selected region type.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Region Type Details" title="Edit this Region Type Details" /></td>
		  <td><strong>Edit this Region Type Details:</strong> This will open a window that will allow modifying/updating the selected region type.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Region Type" title="Delete this Region Type" /></td>
		  <td><strong>Delete this Region Type:</strong> This action will delete the selected region type. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<span class="Help_Title2">How to add a new Region:</span><br />
		To add a new region, click on Add Region button <img src="../images/help/Organization/RegionalHierarchy3.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Region <img src="../images/help/Organization/RegionalHierarchy3.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new region cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Region:</span><br />
		Following operations can be performed on a selected region:
		<br /><br />
		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Region Details" title="View this Region Details" /></td>
		  <td><strong>View this Region Details:</strong> This will open a window that will display all of the details of the selected region.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Region Details" title="Edit this Region Details" /></td>
		  <td><strong>Edit this Region Details:</strong> This will open a window that will allow modifying/updating the selected region.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Region" title="Delete this Region" /></td>
		  <td><strong>Delete this Region:</strong> This action will delete the selected region. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />		

		<!-- Stations -->
		<a name="#Stations"></a><span class="Help_Title">Stations</span><br /><br />
		This component manages different stations / branches / offices of the organization. 
		<br /><br />
		Following is the screenshot of a user view of Stations component:
		<br /><br />
		<div align="center"><img src="../images/help/Organization/Stations1.gif" /></div>
		<br /><br />
		Just like regional hierarchy, stations component is divided in two main sections, Stations and Station Types. Station Types must be added before adding any station.
		<br /><br />
		<span class="Help_Title2">How to add a new Station Type:</span><br />
		To add a new station type, click on Station Types tab, then click on Add Station Type button <img src="../images/help/Organization/Stations2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Station Type <img src="../images/help/Organization/Stations2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new station type cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Station Types:</span><br />
		Following operations can be performed on a selected station type:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Station Type Details" title="View this Station Type Details" /></td>
		  <td><strong>View this Station Type Details:</strong> This will open a window that will display all of the details of the selected station type.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Station Type Details" title="Edit this Station Type Details" /></td>
		  <td><strong>Edit this Station Type Details:</strong> This will open a window that will allow modifying/updating the selected station type.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Station Type" title="Delete this Station Type" /></td>
		  <td><strong>Delete this Station Type:</strong> This action will delete the selected station type. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<span class="Help_Title2">How to add a new Station:</span><br />
		To add a new station, click on Add Station button <img src="../images/help/Organization/Stations3.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Station <img src="../images/help/Organization/Stations3.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new station cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Stations:</span><br />
		Following operations can be performed on a selected station:
		<br /><br />
		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Stations Details" title="View this Stations Details" /></td>
		  <td><strong>View this Stations Details:</strong> This will open a window that will display all of the details of the selected station.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Stations Details" title="Edit this Stations Details" /></td>
		  <td><strong>Edit this Stations Details:</strong> This will open a window that will allow modifying/updating the selected station.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Stations" title="Delete this Stations" /></td>
		  <td><strong>Delete this Stations:</strong> This action will delete the selected station. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />		

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Departments -->
		<a name="#Departments"></a><span class="Help_Title">Departments</span><br /><br />
		Every organization is divided in several different departments, each department usually have a department head, who manages every employee working under his supervision. This component helps in defining the departments and their department heads in the organization.
		<br /><br />
		Following is the screenshot of a user view of departments component:
		<br /><br />
		<div align="center"><img src="../images/help/Organization/Departments1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to add a new department:</span><br />
		To add a new department, click on Add Department button <img src="../images/help/Organization/Departments2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Department <img src="../images/help/Organization/Departments2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new department cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Departments:</span><br />
		Following operations can be performed on a selected department:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Department Details" title="View this Department Details" /></td>
		  <td><strong>View this Department Details:</strong> This will open a window that will display all of the details of the selected department.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Department Details" title="Edit this Department Details" /></td>
		  <td><strong>Edit this Department Details:</strong> This will open a window that will allow modifying/updating the selected department.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Department" title="Delete this Department" /></td>
		  <td><strong>Delete this Department:</strong> This action will delete the selected department. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />		

		<!-- Data Backup -->
		<a name="#DataBackup"></a><span class="Help_Title">Data Backup</span><br /><br />
		This comoponent is used to take complete backup of the complete database. This is a purely technical function and must be used only by the authorized personnel. In information technology, backup refers to making copies of data so that these additional copies may be used to restore the original after a data loss event. These additional copies are typically called "backups." Backups are useful primarily for two purposes. The first is to restore a state following a disaster (called disaster recovery). The second is to restore small numbers of files after they have been accidentally deleted or corrupted.
		<br /><br />
		Please note that only one data backup is allowed per day. Backup files save as the current date. In a particular day, if three data backups are taken, the first two will be overwritten by the third attempt.
		<br /><br />
		It is recommended to take data backup atleast once a week, most organizations prefer taking their data backup on Sundays, so it won\'t interfere with the regular operations of the system. Data backup files generated through the system can be copied on any external media.
		<br /><br />
		Following is the screenshot of a user view of Data Backup component:
		<br /><br />
		<div align="center"><img src="../images/help/Organization/DataBackup1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to take a new data backup:</span><br />
		To take a new data backup, click on Backup Data button <img src="../images/help/Organization/DataBackup2.gif" />. 
		<br /><br />
		<span class="Help_Title2">Operations on Data Backup:</span><br />
		Following operations can be performed on a selected data backup file:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconBackup.gif" border="0" alt="Download this Backup File" title="Download this Backup File" /></td>
		  <td><strong>Download this Backup File:</strong> This will download the selected data backup file.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Backup File" title="Delete this Backup File" /></td>
		  <td><strong>Delete this Backup File:</strong> This action will delete the selected data backup file. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- System Settings -->
		<a name="#SystemSettings"></a><span class="Help_Title">System Settings</span><br /><br />
		This comoponent is used to define a certain settings that affect the whole FM System. Access to this area must be restricted to the administrator only, as any unnecessary change may create problems in the system. 
		<br /><br />
		Following is the screenshot of a user view of System Settings component:
		<br /><br />
		<div align="center"><img src="../images/help/Organization/SystemSettings1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to save system settings:</span><br />
		To save settings, click on Save button <img src="../images/help/Organization/SystemSettings2.gif" />. 
		<br /><br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />		

		<!-- System Log -->
		<a name="#SystemLog"></a><span class="Help_Title">System Log</span><br /><br />
		This component displays log of each action users perform. The exact server date/time, along with the user name and IP address is recorded. 
		<br /><br />
		Following is the screenshot of a user view of System Log component:
		<br /><br />
		<div align="center"><img src="../images/help/Organization/SystemLog1.gif" /></div>
		<br /><br /><br />
		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />
		</div>		
		</td></tr></table>';
		
		return($sReturn);
	}
	
	function Help_Accounts()
    {		
    	$sReturn = '		
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
		 <a name="#Top"></a>
		 <span class="Help_Title">Accounts Module</span><br /><br />
		<div class="Help_Contents" align="justify">
		This module deals with managing the accounts settings, that will help in operating the entire FM System. Most of the actions in this module are to be performed once, usually when setting up the system. A certain minor changes may be required here and there, especially when there is a change in organizational structure. Components in this module are mostly for administrative purposes only, users can have the role to view information on a certain components.
		<br /><br />
		The Organization module comprises of the following components (Click for details):
		<br /><br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		 <tr>
		  <td width="10%" align="center"><a href="#GeneralJournal"><img src="../images/accounts/iconGeneralJournal.gif" title="General Journal" alt="General Journal" border="0" /><br />General Journal</a></td>
		  <td width="10%" align="center"><a href="#GeneralLedger"><img src="../images/accounts/iconGeneralLedger.gif" title="General Ledger" alt="General Ledger" border="0" /><br />General Ledger</a></td>
		  <td width="10%" align="center"><a href="#ChartofAccounts"><img src="../images/accounts/iconChartofAccounts.gif" title="Chart of Accounts" alt="Chart of Accounts" border="0" /><br />Chart of Accounts</a></td>
		  <td width="10%" align="center"><a href="#QuickEntries"><img src="../images/accounts/iconQuickEntries.gif" title="Quick Entries" alt="Quick Entries" border="0" /><br />Quick Entries</a></td>
		  <td width="10%" align="center"><a href="#Donors"><img src="../images/accounts/iconDonors.gif" title="Donors" alt="Donors" border="0" /><br />Donors</a></td>
		  <td width="10%" align="center"><a href="#CloseFinancialYear"><img src="../images/accounts/iconCloseFinancialYear.gif" title="Close Financial Year" alt="Close Financial Year" border="0" /><br />Close Financial Year</a></td>
		  <td width="10%" align="center"><a href="#Budget"><img src="../images/accounts/iconBudget.gif" title="Budget" alt="Budget" border="0" /><br />Budget</a></td>
		  <td width="10%" align="center"><a href="#BudgetAllocation"><img src="../images/accounts/iconBudgetAllocation.gif" title="Budget Allocation" alt="Budget Allocation" border="0" /><br />Budget Allocation</a></td>
		 </tr>
		</table>
		<br /><br />
		<hr size="1" />

		<!-- General Journal-->
		<a name="#GeneralJournal"></a><span class="Help_Title">General Journal</span><br /><br />
		This component manages the regional hierarchy of the regions / areas where the organization works. A typical example of regional hiearchy would be an organization working in different regions, e.g. Lahore, Karachi, Sukkur and Queeta, would add the following entries:<br />
		Regional hierarchy helps mostly in reporting, where the report is to be generated, lets say on Sindh Province, would display results from all branches in Sindh. The reason for the report displaying all branches in Sindh when province Sindh is selected is because Regional Hiearchy works like a tree, derived from top to bottom. In our example, the top branch of the tree would be Country Pakistan, then, the next level of hierarchy would be Provinces and finally the Cities at the bottom. Regional hieararchy is completely controllable by the user to allow any number of levels in the hierarchy. This will give flexibility to add further levels of regions, for example, if the user wants to add Districts, Tehsils, Talukas and even Villages.
		<br /><br />
		Following is the screenshot of a user view of Gnral Journal component:
		<br /><br />
		<div align="center"><img src="../images/help/Accounts/GenralJournal1.gif" /></div>
		<br /><br />
		Regional hierarchy component is divided in two main sections, Regions and Region Types. Region Types must be defined first, in our example following were the region types that were added in the system: Country, Province, City. After adding Region Types, the user will add different regions by selecting its region type, in our example Karachi is listed as a City, and so on.
		<br /><br />
		<span class="Help_Title2">How to add a new General Journal:</span><br />
		To add a new General Journal, click on General Journal tab, then click on Add New button <img src="../images/help/Accounts/GeneralJournal2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add New <img src="../images/help/Accounts/GeneralJournal2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new General Journal cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on General Journal:</span><br />
		Following operations can be performed on a selected General Journal:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this General Journal Details" title="View this General Journal Details" /></td>
		  <td><strong>View this General Journal Details:</strong> This will open a window that will display all of the details of the selected General Journal.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this General Journal Details" title="Edit this General Journal Details" /></td>
		  <td><strong>Edit this General Journal Details:</strong> This will open a window that will allow modifying/updating the selected General Journal.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this General Journal" title="Delete this General Journal" /></td>
		  <td><strong>Delete this General Journal:</strong> This action will delete the selected General Journal. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconPrint2.gif" border="0" alt="Receipt" title="Receipt" /></td>
		  <td><strong>Receipt:</strong> This action will show the Receipt of selected General Journal.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconSave.gif" border="0" alt="General Journal Documents" title="General Journal Documents" /></td>
		  <td><strong>General Journal Documents:</strong> This action is used to manage the selected General Journal Documents.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/tick.gif" border="0" alt="Update Status" title="Update Status" /></td>
		  <td><strong>Update Status:</strong> This action is used to Update Status of the selected General Journal.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Genral Ledger -->
		<a name="#GeneralLedger"></a><span class="Help_Title">General Ledger</span><br /><br />
		This component shows all the General Journal Entries. 
		<br /><br />
		Following is the screenshot of a user view of General Ledger component:
		<br /><br />
		<div align="center"><img src="../images/help/Accounts/GenralLedger1.gif" /></div>
		<br /><br />
		Just like regional hierarchy, stations component is divided in two main sections, Stations and Station Types. Station Types must be added before adding any station.
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Station Type Details" title="View this Station Type Details" /></td>
		  <td><strong>View this General Ledger Details:</strong> This will open a window that will display all of the details of the selected General Ledger Entry.</td>
		 </tr> 
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Chart of Accounts -->
		<a name="#ChartofAccounts"></a><span class="Help_Title">Chart of Accounts</span><br /><br />
		Every organization is divided in several different departments, each department usually have a department head, who manages every employee working under his supervision. This component helps in defining the departments and their department heads in the organization.
		<br /><br />
		Following is the screenshot of a user view of Chart of Accounts component:
		<br /><br />
		<div align="center"><img src="../images/help/Organization/ChartofAccounts1.gif" /></div>
		<br /><br />
		Just like regional hierarchy, stations component is divided in two main sections, Stations and Station Types. Station Types must be added before adding any station.
		<br /><br />
		<span class="Help_Title2">How to add a new Chart of Account:</span><br />
		To add a new Chart of Account, click on Chart of Account, tab, then click on Add Account button <img src="../images/help/Accounts/ChartofAccount2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add <img src="../images/help/Accounts/ChartofAccount2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Chart of Account cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Chart of Account:</span><br />
		Following operations can be performed on a selected Chart of Account:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Chart of Accounts Details" title="View this Chart of Accounts Details" /></td>
		  <td><strong>View this Chart Of Account Details:</strong> This will open a window that will display all of the details of the selected Chart of Accounts.</td>
		 </tr>
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Chart of Accounts Details" title="Edit this Chart of Accounts Details" /></td>
		  <td><strong>Edit this Chart of Account Details:</strong> This will open that will allow modifying/updating the selected Chart of Accounts.</td>
		 </tr>
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconExport.gif" border="0" alt="View Transaction Details" title="View Transaction Details" /></td>
		  <td><strong>View Transaction Details:</strong> This will open a window that will display all of the details of the selected Chart of Account\'s Transaction Details.</td>
		 </tr> 
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Chart Of Account" title="Delete this Chart Of Account" /></td>
		  <td><strong>Delete this Chart Of Account:</strong> This action will delete the selected Chart Of Account. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />		

		<!-- Quick Entries -->
		<a name="#QuickEntries"></a><span class="Help_Title">Quick Entries</span><br /><br />
		This comoponent is used to take complete backup of the complete database. This is a purely technical function and must be used only by the authorized personnel. In information technology, backup refers to making copies of data so that these additional copies may be used to restore the original after a data loss event. These additional copies are typically called "backups." Backups are useful primarily for two purposes. The first is to restore a state following a disaster (called disaster recovery). The second is to restore small numbers of files after they have been accidentally deleted or corrupted.
		<br /><br />
		Please note that only one data backup is allowed per day. Backup files save as the current date. In a particular day, if three data backups are taken, the first two will be overwritten by the third attempt.
		<br /><br />
		It is recommended to take data backup atleast once a week, most organizations prefer taking their data backup on Sundays, so it won\'t interfere with the regular operations of the system. Data backup files generated through the system can be copied on any external media.
		<br /><br />
		Following is the screenshot of a user view of Quick Entries component:
		<br /><br />
		<div align="center"><img src="../images/help/Organization/QuickEntries.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to add a new Quick Entry:</span><br />
		To add a new Quick Entry, click on Quick Entry, tab, then click on Add New button <img src="../images/help/Accounts/QuickEntry2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add <img src="../images/help/Accounts/QuickEntry2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Quick Entry cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Quick Entry:</span><br />
		Following operations can be performed on a selected Quick Entry:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quick Entry Details" title="View this Quick Entry Details" /></td>
		  <td><strong>View this Quick Entry Details:</strong> This will open a window that will display all of the details of the selected Quick Entry.</td>
		 </tr>
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quick Entry Details" title="Edit this Quick Entry Details" /></td>
		  <td><strong>Edit this Quick Entry Details:</strong> This will open that will allow modifying/updating the selected Quick Entry.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quick Entry" title="Delete this Quick Entry" /></td>
		  <td><strong>Delete this Quick Entry:</strong> This action will delete the selected Quick Entry. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Donors -->
		<a name="#Donors"></a><span class="Help_Title">Donors</span><br /><br />
		This component manages the Donors, Projects and Project Activities:<br />

		<br /><br />
		Following is the screenshot of a user view of Donors component:
		<br /><br />
		<div align="center"><img src="../images/help/Organization/Donors1.gif" /></div>
		<br /><br />
		Donors component is divided in three main sections, Donors, donor Projects and Project Activities. Donors must be defined first.
		<br /><br />
		<span class="Help_Title2">How to add a new Donor:</span><br />
		To add a new Donor, click on Donor tab, then click on Add Donor button <img src="../images/help/Organization/Donors2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Donor <img src="../images/help/Accounts/Donors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Donor cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Donors:</span><br />
		Following operations can be performed on a selected Donor:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Donor Details" title="View this Donor Details" /></td>
		  <td><strong>View this Donor Details:</strong> This will open a window that will display all of the details of the selected Donor.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Donor Details" title="Edit this Donor Details" /></td>
		  <td><strong>Edit this Donor Details:</strong> This will open a window that will allow modifying/updating the selected Donor.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Donor" title="Delete this Donor" /></td>
		  <td><strong>Delete this Donor:</strong> This action will delete the selected Donor. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<span class="Help_Title2">How to add a new Donor Project:</span><br />
		To add a new Donor Project, click on Donor Projects tab, then click on Add Donor button <img src="../images/help/Accounts/DonorProjects2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Project <img src="../images/help/Accounts/DonorProjects2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Donor Project cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Donor Projects:</span><br />
		Following operations can be performed on a selected Donor Project:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Donor Project Details" title="View this Donor Project Details" /></td>
		  <td><strong>View this Donor Project Details:</strong> This will open a window that will display all of the details of the selected Donor Project.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Donor Project Details" title="Edit this Donor Project Details" /></td>
		  <td><strong>Edit this Donor Project Details:</strong> This will open a window that will allow modifying/updating the selected Donor Project.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Donor Project" title="Delete this Donor Project" /></td>
		  <td><strong>Delete this Donor Project:</strong> This action will delete the selected Donor Project. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<span class="Help_Title2">How to add a new Project Activity:</span><br />
		To add a new Project Activity, click on Project Activities tab, then click on Add Activity button <img src="../images/help/Accounts/ProjectActivity2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Activity <img src="../images/help/Accounts/ProjectActivity2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Project Activity cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Project Activity:</span><br />
		Following operations can be performed on a selected Project Activity:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Project Activity Details" title="View this Project Activity Details" /></td>
		  <td><strong>View this Project Activity Details:</strong> This will open a window that will display all of the details of the selected Project Activity.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Project Activity Details" title="Edit this Project Activity Details" /></td>
		  <td><strong>Edit this Project Activity Details:</strong> This will open a window that will allow modifying/updating the selected Project Activity.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Project Activity" title="Delete this Project Activity" /></td>
		  <td><strong>Delete this Project Activity:</strong> This action will delete the selected Project Activity. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Close Financial Year -->
		<a name="#CloseFinancialYear"></a><span class="Help_Title">Close Financial Year</span><br /><br />
		This component manages the regional hierarchy of the regions / areas where the organization works. A typical example of regional hiearchy would be an organization working in different regions, e.g. Lahore, Karachi, Sukkur and Queeta, would add the following entries:<br />
		<br /><br />
		Following is the screenshot of a user view of Close Financial Year module:
		<br /><br />
		<div align="center"><img src="../images/help/Accounts/CloseFinancialYear1.gif" /></div>
		<br /><br />
		Regional hierarchy component is divided in two main sections, Regions and Region Types. Region Types must be defined first, in our example following were the region types that were added in the system: Country, Province, City. After adding Region Types, the user will add different regions by selecting its region type, in our example Karachi is listed as a City, and so on.
		<br /><br />
		<span class="Help_Title2">How to Close a Financial Year:</span><br />
		To add a new region type, click on Region Types tab, then click on Add Region Type button <img src="../images/help/Organization/RegionalHierarchy2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Region Type <img src="../images/help/Organization/RegionalHierarchy2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new region type cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Close Financial Year:</span><br />
		Following operations can be performed on a selected Close Financial Year:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Region Type Details" title="View this Region Type Details" /></td>
		  <td><strong>View this Region Type Details:</strong> This will open a window that will display all of the details of the selected region type.</td>
		 </tr>
		 </table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Budget -->
		<a name="#Budget"></a><span class="Help_Title">Budget</span><br /><br />
		This component manages the Budget of the organization.<br />
		<br /><br />
		Following is the screenshot of a user view of Budget component:
		<br /><br />
		<div align="center"><img src="../images/help/Accounts/Budget1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to add a new Budget:</span><br />
		To add a new Budget, click on Budget tab, then click on Add Budget button <img src="../images/help/Accounts/Budget2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Budget <img src="../images/help/Accounts/Budget2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new region type cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Budget:</span><br />
		Following operations can be performed on a selected Budget:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Budget Details" title="View this Budget Details" /></td>
		  <td><strong>View this Budget Details:</strong> This will open a window that will display all of the details of the selected Budget.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Details" title="Edit this Budget Details" /></td>
		  <td><strong>Edit this Budget Details:</strong> This will open a window that will allow modifying/updating the selected Budget.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/tick.gif" border="0" alt="Change Budget Amount" title="Change Budget Amount" /></td>
		  <td><strong>Change Budget Amount:</strong> This will open a window that will allow modifying/updating the selected Budget Amount.</td>
		 </tr> 
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget" title="Delete this Budget" /></td>
		  <td><strong>Delete this Budget:</strong> This action will delete the selected Budget. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Budget Allocation-->
		<a name="#BudgetAllocation"></a><span class="Help_Title">Budget Allocation</span><br /><br />
		This component is used to Allocate the Budget.
		<br /><br />
		Following is the screenshot of a user view of Budget Allocation component:
		<br /><br />
		<div align="center"><img src="../images/help/Accounts/BudgetAllocation1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to Allocate Budget:</span><br />
		To Allocate Budget, click on Budget Allocation tab, then click on Allocate Budget button <img src="../images/help/Accounts/BudgetAllocation2.gif" />. This will open a new tab, fill in the required information in the new window, then click Allocate Type <img src="../images/help/Accounts/BudgetAllocation2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Budget cannot be allocated until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Budget Allocation:</span><br />
		Following operations can be performed on a selected Allocated Budget:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Budget Allocation Details" title="View this Budget Allocation Details" /></td>
		  <td><strong>View this Budget Allocation Details:</strong> This will open a window that will display all of the details of the selected Budget Allocation.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Allocation Details" title="Edit this Budget Allocation Details" /></td>
		  <td><strong>Edit this Budget Allocation Details:</strong> This will open a window that will allow modifying/updating the selected Budget Allocation.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/tick.gif" border="0" alt="Change Budget Allocation Amount" title="Change Budget Allocation Amount" /></td>
		  <td><strong>Change Budget Allocation Amount:</strong> This action will change the allocated budget amount.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />
		</div>		
		</td></tr></table>';
		
		return($sReturn);
	}
	
	function Help_Vendors()
    {
    	$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
		 <a name="#Top"></a>
		 <span class="Help_Title">Vendors Module</span><br /><br />
		<div class="Help_Contents" align="justify">
		This module deals with managing the vendors settings, that will help in operating the entire FM System. Most of the actions in this module are to be performed once, usually when setting up the system. A certain minor changes may be required here and there, especially when there is a change in organizational structure. Components in this module are mostly for administrative purposes only, users can have the role to view information on a certain components.
		<br /><br />
		The Vendor module comprises of the following components (Click for details):
		<br /><br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		 <tr>
		  <td width="10%" align="center"><a href="#Vendors"><img src="../images/vendors/iconVendors.gif" title="Vendors" alt="Vendors" border="0" /><br />Vendors</a></td>
		  <td width="10%" align="center"><a href="#Products"><img src="../images/vendors/iconProducts.gif" title="Products" alt="Products" border="0" /><br />Products</a></td>
		  <td width="10%" align="center"><a href="#PurchaseRequests"><img src="../images/vendors/iconPurchaseRequests.gif" title="Purchase Requests" alt="Purchase Requests" border="0" /><br />Purchase Requests</a></td>
		  <td width="10%" align="center"><a href="#Quotations"><img src="../images/vendors/iconQuotations.gif" title="Quotations" alt="Quotations" border="0" /><br />Quotations</a></td>
		  <td width="10%" align="center"><a href="#PurchaseOrders"><img src="../images/vendors/iconPurchaseOrders.gif" title="Purchase Orders" alt="Purchase Orders" border="0" /><br />Purchase Orders</a></td>  
		 </tr>
		</table>
		<br /><br />
		<hr size="1" />

		<!-- Vendors -->
		<a name="#Vendors"></a><span class="Help_Title">Vendors</span><br /><br />
		This component manages the Vendors.
		<br /><br />
		Following is the screenshot of a user view of Vendors component:
		<br /><br />
		<div align="center"><img src="../images/help/Vendors/Vendors1.gif" /></div>
		<br /><br />
		Vendor component is divided in two main sections, Vendors and Vendor Types. Vendor Types must be defined first.
		<br /><br />
		<span class="Help_Title2">How to add a new Vendor Type:</span><br />
		To add a new Vendor Type, click on Vendor Types tab, then click on Add Vendor Type button <img src="../images/help/Vendors/Vendors2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Vendor Type <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Vendor Type cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Vendor Types:</span><br />
		Following operations can be performed on a selected Vendor Type:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Vendor Type Details" title="View this Vendor Type Details" /></td>
		  <td><strong>View this Vendor Type Details:</strong> This will open a window that will display all of the details of the selected Vendor Type.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Vendor Type Details" title="Edit this Vendor Type Details" /></td>
		  <td><strong>Edit this Vendor Type Details:</strong> This will open a window that will allow modifying/updating the selected Vendor Type.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Vendor Type" title="Delete this Vendor Type" /></td>
		  <td><strong>Delete this Vendor Type:</strong> This action will delete the selected Vendor Type. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<span class="Help_Title2">How to add a new Vendor:</span><br />
		To add a new Vendor, click on Vendors tab, then click on Add Vendor button <img src="../images/help/Vendors/Vendors2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Vendor <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Vendor cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Vendors:</span><br />
		Following operations can be performed on a selected Vendor:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Vendor Details" title="View this Vendor Details" /></td>
		  <td><strong>View this Vendor Details:</strong> This will open a window that will display all of the details of the selected Vendor.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Vendor Details" title="Edit this Vendor Details" /></td>
		  <td><strong>Edit this Vendor Details:</strong> This will open a window that will allow modifying/updating the selected Vendor.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Vendor" title="Delete this Vendor" /></td>
		  <td><strong>Delete this Vendor:</strong> This action will delete the selected Vendor. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />		

		<!-- Products -->
		<a name="#Products"></a><span class="Help_Title">Products</span><br /><br />
		This component manages different Products. 
		<br /><br />
		Following is the screenshot of a user view of Products component:
		<br /><br />
		<div align="center"><img src="../images/help/Vendors/Products1.gif" /></div>
		<br /><br />
		Products component is divided in two main sections, Products and Categories. Categories must be added before adding any Product.
		<br /><br />

		<span class="Help_Title2">How to add a new Category:</span><br />
		To add a new Category, click on Categorys tab, then click on Add Category button <img src="../images/help/Categorys/Categorys2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Category <img src="../images/help/Categorys/Categorys2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Category cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Categorys:</span><br />
		Following operations can be performed on a selected Category:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Category Details" title="View this Category Details" /></td>
		  <td><strong>View this Category Details:</strong> This will open a window that will display all of the details of the selected Category.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Category Details" title="Edit this Category Details" /></td>
		  <td><strong>Edit this Category Details:</strong> This will open a window that will allow modifying/updating the selected Category.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Category" title="Delete this Category" /></td>
		  <td><strong>Delete this Category:</strong> This action will delete the selected Category. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<span class="Help_Title2">How to add a new Product:</span><br />
		To add a new Product, click on Products tab, then click on Add Product button <img src="../images/help/Products/Products2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Product <img src="../images/help/Products/Products2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Product cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Products:</span><br />
		Following operations can be performed on a selected Product:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Product Details" title="View this Product Details" /></td>
		  <td><strong>View this Product Details:</strong> This will open a window that will display all of the details of the selected Product.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Product Details" title="Edit this Product Details" /></td>
		  <td><strong>Edit this Product Details:</strong> This will open a window that will allow modifying/updating the selected Product.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconFolder.gif" border="0" alt="Product Documents" title="Product Documents" /></td>
		  <td><strong>Product Documents:</strong> This will open a window that will allow uploading/deleting/downloading the selected Product Documents.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconImages.gif" border="0" alt="Product Images" title="Product Images" /></td>
		  <td><strong>Product Images:</strong> This will open a window that will allow uploading/deleting/downloading the selected Product Images.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Product" title="Delete this Product" /></td>
		  <td><strong>Delete this Product:</strong> This action will delete the selected Product. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Purchase Requests -->
		<a name="#PurchaseRequests"></a><span class="Help_Title">Purchase Requests</span><br /><br />
		Every organization is divided in several different departments, each department usually have a department head, who manages every employee working under his supervision. This component helps in defining the departments and their department heads in the organization.
		<br /><br />
		Following is the screenshot of a user view of Purchase Requests component:
		<br /><br />
		<div align="center"><img src="../images/help/Vendors/PurchaseRequests1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to add a new Purchase Request:</span><br />
		To add a new Purchase Request, click on Purchase Requests tab, then click on Add Purchase Request button <img src="../images/help/Purchase Requests/Purchase Requests2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Purchase Request <img src="../images/help/Purchase Requests/Purchase Requests2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Purchase Request cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Purchase Requests:</span><br />
		Following operations can be performed on a selected Purchase Request:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Purchase Request Details" title="View this Purchase Request Details" /></td>
		  <td><strong>View this Purchase Request Details:</strong> This will open a window that will display all of the details of the selected Purchase Request.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Purchase Request Details" title="Edit this Purchase Request Details" /></td>
		  <td><strong>Edit this Purchase Request Details:</strong> This will open a window that will allow modifying/updating the selected Purchase Request.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconContents.gif" border="0" alt="View Quotations list against this Purchase Request" title="View Quotations list against this Purchase Request" /></td>
		  <td><strong>View Quotations list against this Purchase Request:</strong> This will open a window that will allow to view Quotations against the select Purchase Request.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Purchase Request" title="Delete this Purchase Request" /></td>
		  <td><strong>Delete this Purchase Request:</strong> This action will delete the selected Purchase Request. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />		

		<!-- Quotations -->
		<a name="#Quotations"></a><span class="Help_Title">Quotations</span><br /><br />
		This component is used for Quotations.
		<br /><br />
		Following is the screenshot of a user view of Quotation component:
		<br /><br />
		<div align="center"><img src="../images/help/Vendors/Quotations.gif" /></div>
		<br /><br />

		<span class="Help_Title2">How to add a new Quotation:</span><br />
		To add a new Quotation, click on Quotations tab, then click on Add Quotation button <img src="../images/help/Quotations/Quotations2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Quotation <img src="../images/help/Quotations/Quotations2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Quotation cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Quotations:</span><br />
		Following operations can be performed on a selected Quotation:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quotation Details" title="View this Quotation Details" /></td>
		  <td><strong>View this Quotation Details:</strong> This will open a window that will display all of the details of the selected Quotation.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quotation Details" title="Edit this Quotation Details" /></td>
		  <td><strong>Edit this Quotation Details:</strong> This will open a window that will allow modifying/updating the selected Quotation.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quotation" title="Delete this Quotation" /></td>
		  <td><strong>Delete this Quotation:</strong> This action will delete the selected Quotation. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Purchase Orders -->
		<a name="#PurchaseOrders"></a><span class="Help_Title">Purchase Orders</span><br /><br />
		This component is used for Purchase Orders.
		<br /><br />
		Following is the screenshot of a user view of Purchase Orders component:
		<br /><br />
		<div align="center"><img src="../images/help/Vendors/PurchaseOrders.gif" /></div>
		<br /><br />

		<span class="Help_Title2">How to add a new Purchase Order:</span><br />
		To add a new Purchase Order, click on Purchase Orders tab, then click on Add New button <img src="../images/help/Vendors/PurchaseOrders2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add New <img src="../images/help/Purchase Orders/Purchase Orders2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Purchase Order cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Purchase Orders:</span><br />
		Following operations can be performed on a selected Purchase Order:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Purchase Order Details" title="View this Purchase Order Details" /></td>
		  <td><strong>View this Purchase Order Details:</strong> This will open a window that will display all of the details of the selected Purchase Order.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Purchase Order Details" title="Edit this Purchase Order Details" /></td>
		  <td><strong>Edit this Purchase Order Details:</strong> This will open a window that will allow modifying/updating the selected Purchase Order.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Purchase Order" title="Delete this Purchase Order" /></td>
		  <td><strong>Delete this Purchase Order:</strong> This action will delete the selected Purchase Order. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		</div>		
		</td></tr></table>';
				
		return($sReturn);
	}
	
	function Help_Customers()
    {		
    	$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
		 <a name="#Top"></a>
		 <span class="Help_Title">Customers Module</span><br /><br />
		<div class="Help_Contents" align="justify">
		This module deals with managing the cstomers.
		<br /><br />
		The Customers module comprises of the following components (Click for details):
		<br /><br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		 <tr>
		  <td width="10%" align="center"><a href="#Customers"><img src="../images/customers/iconCustomers.gif" title="Customers" alt="Customers" border="0" /><br />Customers</a></td>
		  <td width="10%" align="center"><a href="#Quotations"><img src="../images/customers/iconQuotations.gif" title="Quotations" alt="Quotations" border="0" /><br />Quotations</a></td>
		  <td width="10%" align="center"><a href="#SalesOrders"><img src="../images/customers/iconSalesOrders.gif" title="Sales Orders" alt="Sales Orders" border="0" /><br />Sales Orders</a></td>
		  <td width="10%" align="center"><a href="#Invoices"><img src="../images/customers/iconInvoices.gif" title="Invoices" alt="Invoices" border="0" /><br />Invoices</a></td>
		  <td width="10%" align="center"><a href="#Receipts"><img src="../images/customers/iconReceipts.gif" title="Receipts" alt="Receipts" border="0" /><br />Receipts</a></td>
		  <td width="10%" align="center"><a href="#Jobs"><img src="../images/customers/iconJobs.gif" title="Jobs" alt="Jobs" border="0" /><br />Jobs</a></td>  
		 </tr>
		</table>
		<br /><br />
		<hr size="1" />

		<!-- Customers -->
		<a name="#Customers"></a><span class="Help_Title">Customers</span><br /><br />
		This component manages the Customers.
		<br /><br />
		Following is the screenshot of a user view of Customers component:
		<br /><br />
		<div align="center"><img src="../images/help/Customers/Customers1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to add a new Customers:</span><br />
		To add a new Customer, click on Customers tab, then click on Add Customer button <img src="../images/help/Customers/Customers2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Customer<img src="../images/help/Customers/Customers2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Customer cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Customers:</span><br />
		Following operations can be performed on a selected Customer:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Customer Details" title="View this Customer Details" /></td>
		  <td><strong>View this Customer Details:</strong> This will open a window that will display all of the details of the selected Customer.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Customer Details" title="Edit this Customer Details" /></td>
		  <td><strong>Edit this Customer Details:</strong> This will open a window that will allow modifying/updating the selected Customer.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconContents.gif" border="0" alt="Customers Ledger" title="Customers Ledger" /></td>
		  <td><strong>Customers Ledger:</strong> This will open a window that will allow to view the selected Customer\'s Ledger.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Customer" title="Delete this Customer" /></td>
		  <td><strong>Delete this Customer:</strong> This action will delete the selected Customer. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />		

		<!-- Quotations -->
		<a name="#Quotations"></a><span class="Help_Title">Quotations</span><br /><br />
		This component is used for Quotations.
		<br /><br />
		Following is the screenshot of a user view of Quotation component:
		<br /><br />
		<div align="center"><img src="../images/help/Vendors/Quotations.gif" /></div>
		<br /><br />

		<span class="Help_Title2">How to add a new Quotation:</span><br />
		To add a new Quotation, click on Quotations tab, then click on Add Quotation button <img src="../images/help/Quotations/Quotations2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Quotation <img src="../images/help/Quotations/Quotations2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Quotation cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Quotations:</span><br />
		Following operations can be performed on a selected Quotation:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quotation Details" title="View this Quotation Details" /></td>
		  <td><strong>View this Quotation Details:</strong> This will open a window that will display all of the details of the selected Quotation.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quotation Details" title="Edit this Quotation Details" /></td>
		  <td><strong>Edit this Quotation Details:</strong> This will open a window that will allow modifying/updating the selected station type.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quotation" title="Delete this Quotation" /></td>
		  <td><strong>Delete this Quotation:</strong> This action will delete the selected Quotation. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Sales Orders -->
		<a name="#SalesOrders"></a><span class="Help_Title">Sales Orders</span><br /><br />
		This component is used for Sales Orders.
		<br /><br />
		Following is the screenshot of a user view of Sales Orders component:
		<br /><br />
		<div align="center"><img src="../images/help/Vendors/SalesOrders.gif" /></div>
		<br /><br />

		<span class="Help_Title2">How to add a new Sales Order:</span><br />
		To add a new Sales Order, click on Sales Orders tab, then click on Add New button <img src="../images/help/Vendors/SalesOrders2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add New <img src="../images/help/Sales Orders/Sales Orders2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Sales Order cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Sales Orders:</span><br />
		Following operations can be performed on a selected Sales Order:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Sales Order Details" title="View this Sales Order Details" /></td>
		  <td><strong>View this Sales Order Details:</strong> This will open a window that will display all of the details of the selected Sales Order.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Sales Order Details" title="Edit this Sales Order Details" /></td>
		  <td><strong>Edit this Sales Order Details:</strong> This will open a window that will allow modifying/updating the selected Sales Order.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Sales Order" title="Delete this Sales Order" /></td>
		  <td><strong>Delete this Sales Order:</strong> This action will delete the selected Sales Order. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Invoices -->
		<a name="#Invoices"></a><span class="Help_Title">Invoices</span><br /><br />
		This component is used for Invoices.
		<br /><br />
		Following is the screenshot of a user view of Invoices component:
		<br /><br />

		Invoices component is divided in two main sections, Invoices and Recurring Invoices.
		<br /><br />
		<div align="center"><img src="../images/help/Customers/Invoices.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to add a new Recurring Invoices:</span><br />
		To add a new Recurring Invoice, click on Recurring Invoices tab, then click on Add button <img src="../images/help/Custoemrs/RecurringInvoices2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add <img src="../images/help/Customers/RecurringInvoices2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Recurring Invoice cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Recurring Invoices:</span><br />
		Following operations can be performed on a selected Recurring Invoices:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Recurring Invoices Details" title="View this Recurring Invoices Details" /></td>
		  <td><strong>View this Recurring Invoices Details:</strong> This will open a window that will display all of the details of the selected Recurring Invoices.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Recurring Invoices Details" title="Edit this Recurring Invoices Details" /></td>
		  <td><strong>Edit this Recurring Invoices Details:</strong> This will open a window that will allow modifying/updating the selected Recurring Invoices.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Recurring Invoices" title="Delete this Recurring Invoices" /></td>
		  <td><strong>Delete this Recurring Invoices:</strong> This action will delete the selected Recurring Invoices. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<span class="Help_Title2">How to add a new Invoice:</span><br />
		To add a new Invoice, click on Invoices tab, then click on Add Invoice button <img src="../images/help/Custoemrs/Invoices2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Invoice <img src="../images/help/Vendors/Invoices2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Invoice cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Invoices:</span><br />
		Following operations can be performed on a selected Invoice:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Invoice Details" title="View this Invoice Details" /></td>
		  <td><strong>View this Invoice Details:</strong> This will open a window that will display all of the details of the selected Invoice.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Invoice Details" title="Edit this Invoice Details" /></td>
		  <td><strong>Edit this Invoice Details:</strong> This will open a window that will allow modifying/updating the selected Invoice.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconRight.gif" border="0" alt="Make Receipt" title="Make Receipt" /></td>
		  <td><strong>Make Receipt:</strong> This will open a window that will allow to Make Receipt against the selected Invoice.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconPrint2.gif" border="0" alt="Print Invoice" title="Print Invoice" /></td>
		  <td><strong>Print Invoice:</strong> This will open a window that will allow to print Invoice.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconFolder.gif" border="0" alt="Invoice Documents" title="Invoice Documents" /></td>
		  <td><strong>Invoice Documents:</strong> This will open a window that will allow modifying/uploading/deleting the selected Invoice Documents.</td>
		 </tr> 
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Invoice" title="Delete this Invoice" /></td>
		  <td><strong>Delete this Invoice:</strong> This action will delete the selected Invoice. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Receipts -->
		<a name="#Receipts"></a><span class="Help_Title">Receipts</span><br /><br />
		This component is used for Receipts.
		<br /><br />
		Following is the screenshot of a user view of Receipts component:
		<br /><br />

		<span class="Help_Title2">How to add a new Receipt:</span><br />
		To add a new Receipt, click on Receipts tab, then click on Add Receipt button <img src="../images/help/Custoemrs/Receipts2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Receipt <img src="../images/help/Vendors/Receipts2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Receipt cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Receipts:</span><br />
		Following operations can be performed on a selected Receipt:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Receipt Details" title="View this Receipt Details" /></td>
		  <td><strong>View this Receipt Details:</strong> This will open a window that will display all of the details of the selected Receipt.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Receipt Details" title="Edit this Receipt Details" /></td>
		  <td><strong>Edit this Receipt Details:</strong> This will open a window that will allow modifying/updating the selected Receipt.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconPrint2.gif" border="0" alt="Print Receipt" title="Print Receipt" /></td>
		  <td><strong>Print Receipt:</strong> This will open a window that will allow to print Receipt.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconFolder.gif" border="0" alt="Receipt Documents" title="Receipt Documents" /></td>
		  <td><strong>Receipt Documents:</strong> This will open a window that will allow modifying/uploading/deleting the selected Receipt Documents.</td>
		 </tr> 
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Receipt" title="Delete this Receipt" /></td>
		  <td><strong>Delete this Receipt:</strong> This action will delete the selected Receipt. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Jobs -->
		<a name="#Jobs"></a><span class="Help_Title">Jobs</span><br /><br />
		This component is used for Jobs.
		<br /><br />
		Following is the screenshot of a user view of Jobs component:
		<br /><br />

		<span class="Help_Title2">How to add a new Job:</span><br />
		To add a new Job, click on Jobs tab, then click on Add Job button <img src="../images/help/Custoemrs/Jobs2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Job <img src="../images/help/Vendors/Jobs2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Job cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Jobs:</span><br />
		Following operations can be performed on a selected Job:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Job Details" title="View this Job Details" /></td>
		  <td><strong>View this Job Details:</strong> This will open a window that will display all of the details of the selected Job.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Job Details" title="Edit this Job Details" /></td>
		  <td><strong>Edit this Job Details:</strong> This will open a window that will allow modifying/updating the selected Job.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Job" title="Delete this Job" /></td>
		  <td><strong>Delete this Job:</strong> This action will delete the selected Job. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />
		</div>		
		</td></tr></table>';
		
		return($sReturn);
		
	}
	
	function Help_Banking()
    {		
    	$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
		 <a name="#Top"></a>
		 <span class="Help_Title">Banking Module</span><br /><br />
		<div class="Help_Contents" align="justify">
		This module deals with managing the accounts settings, that will help in operating the entire FM System. Most of the actions in this module are to be performed once, usually when setting up the system. A certain minor changes may be required here and there, especially when there is a change in organizational structure. Components in this module are mostly for administrative purposes only, users can have the role to view information on a certain components.
		<br /><br />
		The Organization module comprises of the following components (Click for details):
		<br /><br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		 <tr>
		  <td width="10%" align="center"><a href="#BankDeposits"><img src="../images/banking/iconBankDeposits.gif" title="Bank Deposits" alt="Bank Deposits" border="0" /><br />Bank Deposits</a></td>
		  <td width="10%" align="center"><a href="#BankWithdrawals"><img src="../images/banking/iconBankWithdrawals.gif" title="Bank Withdrawals" alt="Bank Withdrawals" border="0" /><br />Bank Withdrawals</a></td>
		  <td width="10%" align="center"><a href="#BankReconciliation"><img src="../images/banking/iconBankReconciliation.gif" title="Bank Reconciliation" alt="Bank Reconciliation" border="0" /><br />Bank Reconciliation</a></td>
		  <td width="10%" align="center"><a href="#BankCheckBooks"><img src="../images/banking/iconBankCheckBooks.gif" title="Bank Check Books" alt="Bank Check Books" border="0" /><br />Bank Check Books</a></td>
		  <td width="10%" align="center"><a href="#BankAccounts"><img src="../images/banking/iconBankAccounts.gif" title="Bank Accounts" alt="Bank Accounts" border="0" /><br />Bank Accounts</a></td>
		  <td width="10%" align="center"><a href="#Banks"><img src="../images/banking/iconBanks.gif" title="Banks" alt="Banks" border="0" /><br />Banks</a></td>  
		 </tr>
		</table>
		<br /><br />
		<hr size="1" />

		<!-- BankDeposits -->
		<a name="#BankDeposits"></a><span class="Help_Title">Bank Deposits</span><br /><br />
		This component manages the Bank Deposits.
		<br /><br />
		Following is the screenshot of a user view of Bank Deposits component:
		<br /><br />
		<div align="center"><img src="../images/help/Banking/BankDeposits1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">Operations on Bank Deposits:</span><br />
		Following operations can be performed on a selected Bank Deposits:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Deposit Details" title="View this Bank Deposit Details" /></td>
		  <td><strong>View this Bank Deposit Details:</strong> This will open a window that will display all of the details of the selected Bank Deposit.</td>
		 </tr> 
		</table>
		<br /><br />
		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />	

		<!-- Bank Withdrawals -->
		<a name="#BankWithdrawals"></a><span class="Help_Title">Bank Withdrawals</span><br /><br />
		This component manages the Bank Withdrawals.
		<br /><br />
		Following is the screenshot of a user view of Bank Withdrawals component:
		<br /><br />
		<div align="center"><img src="../images/help/Banking/BankWithdrawals1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">Operations on Bank Withdrawals:</span><br />
		Following operations can be performed on a selected Bank Withdrawals:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Withdrawal Details" title="View this Bank Withdrawal Details" /></td>
		  <td><strong>View this Bank Withdrawal Details:</strong> This will open a window that will display all of the details of the selected Bank Withdrawal.</td>
		 </tr> 
		</table>
		<br /><br />
		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />	

		<!-- Bank Reconciliation -->
		<a name="#BankReconciliation"></a><span class="Help_Title">Bank Reconciliation</span><br /><br />
		This component manages the Bank Reconciliation.
		<br /><br />
		Following is the screenshot of a user view of Bank Reconciliation component:
		<br /><br />
		<div align="center"><img src="../images/help/Banking/BankReconciliation1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">Operations on Bank Reconciliation:</span><br />
		Following operations can be performed on a selected Bank Reconciliation:
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />	

		<!-- Bank Check Books -->
		<a name="#BankCheckBooks"></a><span class="Help_Title">Bank Check Books</span><br /><br />
		This component manages the Bank Check Books.
		<br /><br />
		Following is the screenshot of a user view of Bank Check Books component:
		<br /><br />
		<div align="center"><img src="../images/help/Vendors/Vendors1.gif" /></div>
		<br /><br />
		Bank Check Books component is divided in two main sections, Bank Check Books and Issued Checks.
		<br /><br />
		Issued Checks shows just the Checks used/issued for a trnasaction.
		<br /><br />
		<span class="Help_Title2">How to add a new Bank Check Book:</span><br />
		To add a new Bank Check Book, click on Bank Check Books tab, then click on Add Bank Check Book button <img src="../images/help/Vendors/Vendors2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Bank Check Book <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Bank Check Book cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Bank Check Books:</span><br />
		Following operations can be performed on a selected Bank Check Book:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Check Book Details" title="View this Bank Check Book Details" /></td>
		  <td><strong>View this Bank Check Book Details:</strong> This will open a window that will display all of the details of the selected Bank Check Book.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Bank Check Book Details" title="Edit this Bank Check Book Details" /></td>
		  <td><strong>Edit this Bank Check Book Details:</strong> This will open a window that will allow modifying/updating the selected Bank Check Book.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/banking/iconCancelledChecks.gif" border="0" alt="Cancelled Checks" title="Cancelled Checks" /></td>
		  <td><strong>Cancelled Checks:</strong> This will open a window that will allow to Cancel the Checks from selected Bank Check Book.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank Check Book" title="Delete this Bank Check Book" /></td>
		  <td><strong>Delete this Bank Check Book:</strong> This action will delete the selected Bank Check Book. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />		

		<!-- Bank Accounts -->
		<a name="#BankAccounts"></a><span class="Help_Title">Bank Accounts</span><br /><br />
		This component manages the Bank Accounts.
		<br /><br />
		Following is the screenshot of a user view of Bank Accounts component:
		<br /><br />
		<div align="center"><img src="../images/help/Banking/BankAccounts1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to add a new Bank Account:</span><br />
		To add a new Bank Account, click on Bank Accounts tab, then click on Add Bank Account button <img src="../images/help/Banking/BankAccounts2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Bank Account <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Bank Account cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Bank Accounts:</span><br />
		Following operations can be performed on a selected Bank Account:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Account Details" title="View this Bank Account Details" /></td>
		  <td><strong>View this Bank Account Details:</strong> This will open a window that will display all of the details of the selected Bank Account.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Bank Account Details" title="Edit this Bank Account Details" /></td>
		  <td><strong>Edit this Bank Account Details:</strong> This will open a window that will allow modifying/updating the selected Bank Account.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank Account" title="Delete this Bank Account" /></td>
		  <td><strong>Delete this Bank Account:</strong> This action will delete the selected Bank Account. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />

		<!-- Banks -->
		<a name="#Banks"></a><span class="Help_Title">Banks</span><br /><br />
		This component manages the Banks.
		<br /><br />
		Following is the screenshot of a user view of Banks component:
		<br /><br />
		<div align="center"><img src="../images/help/Banking/BankAccounts1.gif" /></div>
		<br /><br />
		<span class="Help_Title2">How to add a new Bank:</span><br />
		To add a new Bank, click on Banks tab, then click on Add New Bank button <img src="../images/help/Banking/Banks2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Bank <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Bank cannot be created until all required fields are filled in. 
		<br /><br />
		<span class="Help_Title2">Operations on Banks:</span><br />
		Following operations can be performed on a selected Bank:
		<br /><br />

		<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
		 <tr>
		  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Details" title="View this Bank Details" /></td>
		  <td><strong>View this Bank Details:</strong> This will open a window that will display all of the details of the selected Bank.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Bank Details" title="Edit this Bank Details" /></td>
		  <td><strong>Edit this Bank Details:</strong> This will open a window that will allow modifying/updating the selected Bank.</td>
		 </tr>
		 <tr>
		  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank" title="Delete this Bank" /></td>
		  <td><strong>Delete this Bank:</strong> This action will delete the selected Bank. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
		 </tr>
		</table>
		<br /><br />

		<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
		<hr size="1" />
		</div>		
		</td></tr></table>';

		return($sReturn);

	}
		
    function Help_Reports()
    {
		return('<div align="center" style="font-family:Tahoma; font-size:16px;" >Page Under Construction </div>');
    }
}
?>