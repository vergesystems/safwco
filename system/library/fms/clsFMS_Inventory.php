<?php

include('../../system/library/fms/clsFMS_Inventory_Categories.php');
include('../../system/library/fms/clsFMS_Inventory_Products.php');
include('../../system/library/fms/clsFMS_Inventory_Services.php');


class clsInventory
{
    // Constructor
    function __construct()
    {
    }
    
    function ShowInventoryMenu()
    {
    	global $objEmployee;
    	
    	$sCurrentFile = $_SERVER['SCRIPT_NAME'];
    	$aCurrentFile = explode('/', $sCurrentFile);
    	$sCurrentFile = $aCurrentFile[count($aCurrentFile)-1];
		
    	$sServices = '<a ' . (($sCurrentFile == "services.php") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../inventory/services.php"><img src="../images/inventory/iconServices.gif" alt="Services" title="Services" border="0" /><br />Services</a>';
    	$sProducts = '<a ' . (($sCurrentFile == "products.php") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../inventory/products.php"><img src="../images/inventory/iconProducts.gif" alt="Products" title="Products" border="0" /><br />Products</a>';
		$sCategories = '<a ' . (($sCurrentFile == "categories.php") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../inventory/categories.php"><img src="../images/inventory/iconCategories.gif" alt="Categories" title="Categories" border="0" /><br />Categories</a>';
		
    	$sReturn .= '
    	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
    	 <tr>
    	  <td valign="bottom">
           <table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
	    	<tr>
			 <td align="center" width="10%">' . $sProducts . '</td>
			 <td align="center" width="10%">' . $sCategories . '</td>    	     
			 <!--<td align="center" width="10%">' . $sServices . '</td>-->
    	 	</tr>
	       </table>
    	  </td></tr></table>';
    	
    	return($sReturn);    	
    }
    
	
}

?>