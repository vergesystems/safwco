<?php

/* Category Class */
class clsInventory_Categories
{
    // Constructor
    function __construct()
    {
    }
	
	function ShowAllCategories($sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
				
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Categories[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');			
			
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

        $sTitle = "Categories";
        if ($sSortBy == "") $sSortBy = "C.CategoryId DESC";

        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((C.CategoryId LIKE '%$sSearch%') OR (C.CategoryName LIKE '%$sSearch%') OR (C.CategoryCode LIKE '%$sSearch%') OR (C2.CategoryName LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }
				
		$iTotalRecords = $objDatabase->DBCount("fms_inventory_categories AS C LEFT JOIN fms_inventory_categories AS C2 ON C2.CategoryId = C.ParentCategoryId INNER JOIN organization_employees AS E ON E.EmployeeId = C.CategoryAddedBy", "1=1 $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_inventory_categories AS C
		LEFT JOIN fms_inventory_categories AS C2 ON C2.CategoryId = C.ParentCategoryId
		INNER JOIN organization_employees AS E ON E.EmployeeId = C.CategoryAddedBy
        WHERE 1=1 $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td width="15%" align="left"><span class="WhiteHeading">Category Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CategoryId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Category Id in Ascending Order" title="Sort by Category Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CategoryId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Category Id in Descending Order" title="Sort by Category Id in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Category Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CategoryName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Category Name in Ascending Order" title="Sort by Category Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CategoryName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Category Name in Descending Order" title="Sort by Category Name in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Category Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CategoryCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Category Code in Ascending Order" title="Sort by Category Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CategoryCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Category Code in Descending Order" title="Sort by Category Code in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Parent Category Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C2.CategoryName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Parent Category Name in Ascending Order" title="Sort by Parent Category Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C2.CategoryName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Parent Category Name in Descending Order" title="Sort by Parent Category Name in Descending Order" border="0" /></a></span></td>
		 <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iCategoryId = $objDatabase->Result($varResult, $i, "C.CategoryId");
			$sCategoryName = $objDatabase->Result($varResult, $i, "C.CategoryName");
			$sCategoryCode = $objDatabase->Result($varResult, $i, "C.CategoryCode");
			$sCategoryParentName = $objDatabase->Result($varResult, $i, "C2.CategoryName");
            $sCategoryDescription = $objDatabase->Result($varResult, $i, "C.CategoryDescription");

            if ($sCategoryParentName == '') $sCategoryParentName = "No Parent";

			$sEditCategory = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sCategoryCode)) . '\', \'../inventory/categories_details.php?action2=edit&id=' . $iCategoryId. '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Category Details" title="Edit this Category Details"></a></td>';
            $sDeleteCategory = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Category?\')) {window.location = \'?action=DeleteCategory&id=' . $iCategoryId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Category" title="Delete this Category"></a></td>';
			
			
       		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Categories[2] == 0)
       			$sEditCategory = '';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Categories[3] == 0)
				$sDeleteCategory = '';			

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td align="left" valign="top">' . $iCategoryId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sCategoryName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sCategoryCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sCategoryParentName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'' . $objDatabase->RealEscapeString($sCategoryName) . '\',  \'../inventory/categories_details.php?id=' . $iCategoryId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Category Details" title="View this Category Details"></a></td>
			 ' . $sEditCategory . '
             ' . $sDeleteCategory . '
            </tr>';
		}

		$sAddNewCategory = '<td width="10%" align="left"><input onclick="window.top.CreateTab(\'tabContainer\', \'Add Category\', \'../inventory/categories_details.php?action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Category"></td>';
		
        if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Categories[1] == 0) // Add Disabled
			$sAddNewCategory = '';
		

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  ' . $sAddNewCategory . '
          <form method="GET" action=""><td align="left" colspan="2">Search for a Category:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Category" title="Search for Category" border="0"></td></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Category Details" alt="View this Category Details"></td><td>View this Category Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Category Details" alt="Edit this Category Details"></td><td>Edit this Category Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Category" alt="Delete this Category"></td><td>Delete this Category</td></tr>
        </table>';

		return($sReturn);
    }

    function CategoryDetails($iCategoryId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_inventory_categories AS C
		LEFT JOIN fms_inventory_categories AS C2 ON C2.CategoryId = C.ParentCategoryId
		INNER JOIN organization_employees AS E ON E.EmployeeId = C.CategoryAddedBy
		WHERE C.CategoryId = '$iCategoryId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iCategoryId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Category" title="Edit this Category" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Category?\')) {window.location = \'?action=DeleteCategory&id=' . $iCategoryId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Category" title="Delete this Category" /></a>';
		
		
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Categories[2] == 0)
   			$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Categories[3] == 0)
			$sButtons_Delete = '';
		

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Category Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iCategoryId = $objDatabase->Result($varResult, 0, "C.CategoryId");
		        $sCategoryName = $objDatabase->Result($varResult, 0, "C.CategoryName");
		        $sCategoryCode = $objDatabase->Result($varResult, 0, "C.CategoryCode");
		        $iCategoryParentId = $objDatabase->Result($varResult, 0, "C.ParentCategoryId");
		        $sCategoryParentName = $objDatabase->Result($varResult, 0, "C2.CategoryName");
				$iCategoryAddedBy = $objDatabase->Result($varResult, 0, "C.CategoryAddedBy");
				$sCategoryAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sCategoryAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \''. $objDatabase->RealEscapeString(str_replace('"', '', $sCategoryAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iCategoryAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
		        $sNotes = $objDatabase->Result($varResult, 0, "C.Notes");

		        if ($sCategoryParentName == '') $sParentStation = "No Parent";
				else $sCategoryParentName  = $sCategoryParentName  . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sCategoryParentName  . '\', \'../inventory/categories_details.php?id=' . $iCategoryParentId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Category Information" title="Parent Category Information" border="0" /></a>';

		        $sDescription = $objDatabase->Result($varResult, 0, "C.CategoryDescription");
		        if($sCategoryParentName=='') $sCategoryParentName="No Parent";

				$dCategoryAddedOn = $objDatabase->Result($varResult, 0, "C.CategoryAddedOn");
				$sCategoryAddedOn = date("F j, Y", strtotime($dCategoryAddedOn)) . ' at ' . date("g:i a", strtotime($dCategoryAddedOn));
		    }

			if ($sAction2 == "edit")
			{
				$sCategoryParentName = '<select class="form1" name="selCategoryParentId" id="selCategoryParentId">
		        <option value="0">No Parent</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_inventory_categories AS C Order by CategoryName");

		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iCategoryParentId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		               $sCategoryParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' .  $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		            else if ($iCategoryId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		            	$sCategoryParentName .= '';
		            else
		               $sCategoryParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' . $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		        }
		        $sCategoryParentName .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selCategoryParentId\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selCategoryParentId\'), \'../inventory/categories_details.php?id=\'+GetSelectedListBox(\'selCategoryParentId\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Category Information" title="Parent Category Information" border="0" /></a>';

		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">
		        <script language="JavaScript" type="text/javascript">
		         function ValidateForm()
		         {
		         	if (GetVal(\'txtCategoryName\') == "") return(AlertFocus(\'Please enter a valid Category Name\', \'txtCategoryName\'));
		         	if (GetVal(\'txtCategoryCode\') == "") return(AlertFocus(\'Please enter a valid Category Code\', \'txtCategoryCode\'));

		         	return true;
		         }
		        </script>';

				$sCategoryName = '<input type="text" name="txtCategoryName" id="txtCategoryName" class="form1" value="' . $sCategoryName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sCategoryCode = '<input type="text" name="txtCategoryCode" id="txtCategoryCode" class="form1" value="' . $sCategoryCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				
			  	$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$iCategoryId = '';
				$sCategoryImageName = '';
				$sCategoryCode = '';
				
				$sCategoryAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sCategoryAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \''. $objDatabase->RealEscapeString(str_replace('"', '', $sCategoryAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
		        $sCategoryName = $objGeneral->fnGet("txtCategoryName");
		        $sCategoryCode = $objGeneral->fnGet("txtCategoryCode");
		        $sDescription = $objGeneral->fnGet("txtDescription");
		        $sNotes = $objGeneral->fnGet("txtNotes");

		        $sCategoryParentName = '<select class="form1" name="selCategoryParentId" id="selCategoryParentId">
		        <option value="0">No Parent</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_inventory_categories AS C Order by CategoryName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iCategoryParentId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		               $sCategoryParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' .  $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		            else if ($iCategoryId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		            	$sCategoryParentName .= '';
		            else
		               $sCategoryParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' . $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		        }
		        $sCategoryParentName .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selCategoryParentId\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selCategoryParentId\'), \'../inventory/categories_details.php?id=\'+GetSelectedListBox(\'selCategoryParentId\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Category Information" title="Parent Category Information" border="0" /></a>';

                $sReturn .= '<form method="post" action="" enctype="multipart/form-data" onsubmit="return ValidateForm();">
		        <script language="JavaScript" type="text/javascript">
		         function ValidateForm()
		         {
		         	if (GetVal(\'txtCategoryName\') == "") return(AlertFocus(\'Please enter a valid Category Name\', \'txtCategoryName\'));
		         	if (GetVal(\'txtCategoryCode\') == "") return(AlertFocus(\'Please enter a valid Category Code\', \'txtCategoryCode\'));

		         	return true;
		         }
		        </script>';

                $sCategoryName = '<input type="text" name="txtCategoryName" id="txtCategoryName" class="form1" value="' . $sCategoryName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sCategoryCode = '<input type="text" name="txtCategoryCode" id="txtCategoryCode" class="form1" value="' . $sCategoryCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
			  	$sCategoryAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			  	$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}

			$sReturn .= '
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="GridTR"><td colspan="2"><span class="Details_Title">Category Information:</span></td></tr>
			 <tr><td width="20%">Category Id:</td><td><strong>' . $iCategoryId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Category Name:</td><td><strong>' . $sCategoryName . '</strong></td></tr>
			 <tr><td>Category Code:</td><td><strong>' . $sCategoryCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Parent Category:</td><td><strong>' . $sCategoryParentName . '</strong></td></tr>
		     <tr bgcolor="#ffffff"><td valign="top" colspan="2">Description:</td></tr>
			 <tr><td valign="top" colspan="4"><strong>' . $sDescription . '</strong></td></tr>
		     <tr class="GridTR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
		     <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
		     <tr bgcolor="#ffffff"><td>Category Added On:</td><td><strong>' . $sCategoryAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Category Added By:</td><td><strong>' . $sCategoryAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Category" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iCategoryId . '"><input type="hidden" name="action" id="action" value="UpdateCategory"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
	  		else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Category" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewCategory"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

	function AddNewCategory($sCategoryName, $sCategoryCode, $sDescription, $sNotes, $iParentCategoryId)
    {
        global $objDatabase;
        global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
				
		// Employee Roles
   		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Categories[1] == 0) // Add Disabled
			return(1010);		
		
        $varNow = $objGeneral->fnNow();
		$iCategoryAddedBy = $objEmployee->iEmployeeId;
		
		$sCategoryCode = $objDatabase->RealEscapeString($sCategoryCode);
		
		if($objDatabase->DBCount("fms_inventory_categories AS C", "C.CategoryCode='$sCategoryCode'") > 0)
			return(5008);
		
		
        $sCategoryName = $objDatabase->RealEscapeString($sCategoryName);
        $sDescription = $objDatabase->RealEscapeString($sDescription);
        $sNotes = $objDatabase->RealEscapeString($sNotes);

        $varResult = $objDatabase->Query("INSERT INTO fms_inventory_categories
        (CategoryName, CategoryCode, CategoryDescription, Notes, ParentCategoryId, CategoryAddedOn, CategoryAddedBy)
        VALUES ('$sCategoryName', '$sCategoryCode', '$sDescription', '$sNotes', '$iParentCategoryId', '$varNow', '$iCategoryAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_inventory_categories AS C WHERE C.CategoryName='$sCategoryName' AND C.CategoryCode='$sCategoryCode' AND C.CategoryAddedOn='$varNow'");

        if ($objDatabase->RowsNumber($varResult) > 0)
        {
        	$iCategoryId = $objDatabase->Result($varResult, $i, "C.CategoryId");
        	
			$objSystemLog->AddLog("Add New Category - " . $sCategoryName);
			
            $objGeneral->fnRedirect('?error=5001&id=' . $objDatabase->Result($varResult, 0, "C.CategoryId"));
        }
       	else
       		return(5002);
    }
		
    function UpdateCategory($iCategoryId, $sCategoryName, $sCategoryCode, $sDescription, $sNotes, $iParentCategoryId)
    {
        global $objDatabase;
        global $objEmployee;
        global $objGeneral;
		global $objSystemLog;
		
        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Categories[2] == 0) // Update Disabled
			return(1010);			
		
		$sCategoryCode = $objDatabase->RealEscapeString($sCategoryCode);
		$sCategoryName = $objDatabase->RealEscapeString($sCategoryName);        
        $sDescription = $objDatabase->RealEscapeString($sDescription);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		
		
		if($objDatabase->DBCount("fms_inventory_categories AS C", "1=1 AND C.CategoryId <> '$iCategoryId' AND C.CategoryCode='$sCategoryCode'") > 0)
			return(5008);
			
        $varResult = $objDatabase->Query("
	    UPDATE fms_inventory_categories
	    SET
        	CategoryName='$sCategoryName',
            CategoryCode='$sCategoryCode',
            CategoryDescription='$sDescription',
            Notes='$sNotes',
            ParentCategoryId='$iParentCategoryId'
    	WHERE CategoryId='$iCategoryId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {
        	$objSystemLog->AddLog("Update Category - " . $sCategoryName);
            $objGeneral->fnRedirect('../inventory/categories_details.php?error=5003&id=' . $iCategoryId);
        }
        else
            return(5004);
    }

    function DeleteCategory($iCategoryId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
		
		
        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Categories[3] == 0) // Delete Disabled
			return(1010);

		if ($objDatabase->DBCount("fms_inventory_products AS P", "P.CategoryId='$iCategoryId'") > 0) return(5007);

       	$varResult = $objDatabase->Query("SELECT * FROM fms_inventory_categories AS C WHERE C.CategoryId='$iCategoryId'");

       	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Category Id');
		$sCategoryName = $objDatabase->Result($varResult, 0, "C.CategoryName");

        $varResult = $objDatabase->Query("DELETE FROM fms_inventory_categories WHERE CategoryId='$iCategoryId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Delete Category - " . $sCategoryName);

            return(5005);
        }
       else
           return(5006);
    }

    

    
}
?>