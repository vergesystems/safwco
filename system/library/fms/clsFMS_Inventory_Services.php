<?php

class clsInventory_Services
{
	public $aServiceStatus;
	// Constructor
    function __construct() 
    {
		$this->aServiceStatus = array("Active", "Inactive");   
    }

    function ShowAllServices($sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Services[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = "Services";
        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((S.ServiceId LIKE '%$sSearch%') OR (S.ServiceName LIKE '%$sSearch%') OR (S.ServiceCode LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (S.ServiceFrequency LIKE '%$sSearch%') OR (S.ServicePrice LIKE '%$sSearch%'))";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        if ($sSortBy == "") $sSortBy = "S.ServiceName";

		$iTotalRecords = $objDatabase->DBCount("fms_inventory_services AS S INNER JOIN organization_employees AS E ON E.EmployeeId = S.ServiceAddedBy", "1=1 $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_inventory_services AS S
		INNER JOIN organization_employees AS E ON E.EmployeeId = S.ServiceAddedBy
		WHERE 1=1 $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td width="10%" align="left"><span class="WhiteHeading">Service Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=S.ServiceId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Service Id in Ascending Order" title="Sort by Service Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=S.ServiceId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Service Id in Descending Order" title="Sort by Service Id in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Service Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=S.ServiceName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Service Name in Ascending Order" title="Sort by Service Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=S.ServiceName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Service Name in Descending Order" title="Sort by Service Name in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Service Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=S.ServiceCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Service Code in Ascending Order" title="Sort by Service Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=S.ServiceCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Service Code in Descending Order" title="Sort by Service Code in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Frequency&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=S.ServiceFrequency&sortorder="><img src="../images/sort_up.gif" alt="Sort by Service Frequency in Ascending Order" title="Sort by Service Frequency in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=S.ServiceFrequency&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Service Frequency in Descending Order" title="Sort by Service Frequency in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=S.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=S.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		 <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iServiceId = $objDatabase->Result($varResult, $i, "S.ServiceId");
			$sServiceFrequency = $objDatabase->Result($varResult, $i, "S.ServiceFrequency");			
			$sServiceName = $objDatabase->Result($varResult, $i, "S.ServiceName");
			$sServiceName = $objDatabase->RealEscapeString($sServiceName);
			$sServiceName = stripslashes($sServiceName);
			$sServiceCode = $objDatabase->Result($varResult, $i, "S.ServiceCode");
			$sServiceCode = $objDatabase->RealEscapeString($sServiceCode);
			$sServiceCode = stripslashes($sServiceCode);
			$iStatus = $objDatabase->Result($varResult, $i, "S.Status");
			$sServiceStatus = $this->aServiceStatus[$iStatus];
            
            $sEditService = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sServiceName)) . '\', \'../inventory/services_details.php?action2=edit&id=' . $iServiceId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Service Details" title="Edit this Service Details"></a></td>';
            $sDeleteService = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to Delete this Service?\')) {window.location = \'?action=DeleteService&id=' . $iServiceId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Service" title="Delete this Service"></a></td>';
						
       		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Services[2] == 0)
       			$sEditService = '';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Services[3] == 0)
				$sDeleteService = '';
			
            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td align="left" valign="top">' . $iServiceId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sServiceName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sServiceCode  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sServiceFrequency . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sServiceStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sServiceName)) . '\', \'../inventory/services_details.php?id=' . $iServiceId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Service Details" title="View this Service Details"></a></td>
			 ' . $sEditService . '
			 ' . $sDeleteService . '
			</tr>';
		}

		$sAddNewService = '<td width="10%" align="left"><input onclick="window.top.CreateTab(\'tabContainer\', \'Add Service\', \'../inventory/services_details.php?action2=addnew\', \'520px\', true);" type="button" class="AdminFormButton1" value="Add New Service"></td>';
				
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Services[1] == 0) // Add Disabled
			$sAddNewService = '';
		
		$sReturn .= '</table>

		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  ' . $sAddNewService . '
          <form method="GET" action=""><td align="left" colspan="2">Search for a Service:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Service" title="Search for Service" border="0"></td> </form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Service Details" alt="View this Service Details"></td><td>View this Service Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Service Details" alt="Edit this Service Details"></td><td>Edit this Service Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Service" alt="Delete this Service"></td><td>Delete this Service</td></tr>
        </table>';

		return($sReturn);
    }

    function ServiceDetails($iServiceId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

    	$varResult = $objDatabase->Query("
    	SELECT * FROM fms_inventory_services AS S
    	INNER JOIN organization_employees AS E ON E.EmployeeId = S.ServiceAddedBy
    	WHERE S.ServiceId = '$iServiceId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iServiceId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Service" title="Edit this Service" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Service?\')) {window.location = \'?action=DeleteService&id=' . $iServiceId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Service" title="Delete this Service" /></a>';
		
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Services[2] == 0)
   			$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Services[3] == 0)
			$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Service Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iServiceId = $objDatabase->Result($varResult, 0, "S.ServiceId");
		    	$iServiceAddedBy = $objDatabase->Result($varResult, 0, "E.EmployeeId");
		        $sServiceName = $objDatabase->Result($varResult, $i, "S.ServiceName");
				$sServiceName = $objDatabase->RealEscapeString($sServiceName);				
				$sServiceName = stripslashes($sServiceName);
				
				$sServiceAddedBy = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
				$sServiceAddedBy = $sServiceAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sServiceAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iServiceAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sServiceCode = $objDatabase->Result($varResult, $i, "S.ServiceCode");
				$sServiceCode = $objDatabase->RealEscapeString($sServiceCode);
				$sServiceCode = stripslashes($sServiceCode);
	            
				$sDescription = $objDatabase->Result($varResult, $i, "S.Description");
            	$sDescription = $objDatabase->RealEscapeString($sDescription);
            	$sDescription = stripslashes($sDescription);
         		
				$iServiceStatus = $objDatabase->Result($varResult, 0, "S.Status");
				$sServiceStatus = $this->aServiceStatus[$iServiceStatus];
				
				$sServiceFrequency = $objDatabase->Result($varResult, 0, "S.ServiceFrequency");
				
				$dServicePrice = $objDatabase->Result($varResult, 0, "S.ServicePrice");
				$sServicePrice = number_format($dServicePrice, 2);
				
				$sNotes = $objDatabase->Result($varResult, 0, "S.Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
				
				$dServiceAddedOn = $objDatabase->Result($varResult, 0, "S.ServiceAddedOn");
				$sServiceAddedOn = date("F j, Y", strtotime($dServiceAddedOn )) . ' at ' . date("g:i a", strtotime($dServiceAddedOn));
		    }

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
		        
				$sServiceStatus = '<select class="form1" name="selServiceStatus" id="selServiceStatus">';
				for ($i=0; $i < count($this->aServiceStatus); $i++)
				{
					$sServiceStatus .= '<option ' . (($iServiceStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aServiceStatus[$i] . '</option>';					
				}
				$sServiceStatus .= '</select>';
				
				$sServicePrice = '<input type="text" name="txtServicePrice" id="txtServicePrice" class="form1" value="' . number_format($dServicePrice, 2) . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sServiceFrequency = '<input type="text" name="txtServiceFrequency" id="txtServiceFrequency" class="form1" value="' . $sServiceFrequency . '" size="30" />&nbsp;<span style="color:red;">*</span>';
		        $sServiceName = '<input type="text" name="txtServiceName" id="txtServiceName" class="form1" value="' . $sServiceName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sServiceCode = '<input type="text" name="txtServiceCode" id="txtServiceCode" class="form1" value="' . $sServiceCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$iServiceId = "";
		        $sServiceName = $objGeneral->fnGet("txtServiceName");
		        $sServiceCode = $objGeneral->fnGet("txtServiceCode");
				$sServiceFrequency = $objGeneral->fnGet("txtServiceFrequency");
				$sServicePrice = $objGeneral->fnGet("txtServicePrice");
		        $sNotes = $objGeneral->fnGet("txtNotes");		        
		        $sDescription = $objGeneral->fnGet("txtDescription");

                $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
                
               	$sServiceAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
                $sServiceAddedBy = $sServiceAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sServiceAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
		        
				$sServiceStatus = '<select class="form1" name="selServiceStatus" id="selServiceStatus">';
				for ($i=0; $i < count($this->aServiceStatus); $i++)
				{
					$sServiceStatus .= '<option value="' . $i . '">' . $this->aServiceStatus[$i] . '</option>';					
				}
				$sServiceStatus .= '</select>';
				
				$sServicePrice = '<input type="text" name="txtServicePrice" id="txtServicePrice" class="form1" value="' . $sServicePrice . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sServiceFrequency = '<input type="text" name="txtServiceFrequency" id="txtServiceFrequency" class="form1" value="' . $sServiceFrequency . '" size="30" />&nbsp;<span style="color:red;">*</span>';
		        $sServiceName = '<input type="text" name="txtServiceName" id="txtServiceName" class="form1" value="' . $sServiceName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sServiceCode = '<input type="text" name="txtServiceCode" id="txtServiceCode" class="form1" value="' . $sServiceCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

			   	$sServiceAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
	     		if (GetVal(\'txtServiceName\') == "") return(AlertFocus(\'Please enter a valid Service Name\', \'txtServiceName\'));
	     		if (GetVal(\'txtServiceCode\') == "") return(AlertFocus(\'Please enter a valid Service Code\', \'txtServiceCode\'));
				if (GetVal(\'txtServiceFrequency\') == "") return(AlertFocus(\'Please enter a valid Service Frequency\', \'txtServiceFrequency\'));
				if (!isNumeric(GetVal(\'txtServicePrice\'))) return(AlertFocus(\'Please enter a valid Service Price\', \'txtServicePrice\'));

	     		return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Service Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td width="20%">Service Id:</td><td><strong>' . $iServiceId . ' (Automatically Assigned)</strong></td></tr>			 
			 <tr bgcolor="#ffffff"><td>Service Name:</td><td><strong>' . $sServiceName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Service Code:</td><td><strong>' . $sServiceCode . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Service Frequency:</td><td><strong>' . $sServiceFrequency . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Service Price:</td><td><strong>' . $sServicePrice . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">Description:</td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="4"><strong>' . $sDescription . '</strong></td></tr>
			 <tr><td>Service Status:</td><td><strong>' . $sServiceStatus. '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
		     <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Service Added On:</td><td><strong>' . $sServiceAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Service Added by:</td><td><strong>' . $sServiceAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Service" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iServiceId . '"><input type="hidden" name="action" id="action" value="UpdateService"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Service" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewService"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

    function UpdateService($iServiceId, $sServiceName, $sServiceCode, $sServiceFrequency, $dServicePrice, $sDescription, $iServiceStatus, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
        
        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Services[2] == 0) // Update Disabled
			return(1010);
		
		
	    // Service Code already added in the System
	  	//if ($objDatabase->DBCount("fms_organization_services AS S", "S.ServiceId <> '$iServiceId' AND S.ServiceCode='$sServiceCode'") > 0) return(7550);

        $sServiceName = $objDatabase->RealEscapeString($sServiceName);
		$sServiceFrequency = $objDatabase->RealEscapeString($sServiceFrequency);
        $sServiceCode = $objDatabase->RealEscapeString($sServiceCode);
        $sDescription = $objDatabase->RealEscapeString($sDescription);
        $sNotes = $objDatabase->RealEscapeString($sNotes);

        if ($objDatabase->DBCount("fms_inventory_services AS S", "S.ServiceId <> '$iServiceId' AND S.ServiceCode='$sServiceCode'") > 0) return(9000);
        if ($objDatabase->DBCount("fms_inventory_services AS S", "S.ServiceId <> '$iServiceId'" ) < 0) return(9007);
        $varResult = $objDatabase->Query("
        UPDATE fms_inventory_services 
		SET
            ServiceName='$sServiceName',
	        ServiceCode='$sServiceCode',
			ServiceFrequency='$sServiceFrequency',
			ServicePrice='$dServicePrice',
			Description='$sDescription',
	        Notes='$sNotes',
			Status='$iServiceStatus'
        WHERE ServiceId='$iServiceId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Update Service - " . $sServiceName);
			$objGeneral->fnRedirect('../inventory/services_details.php?error=9003&id=' . $iServiceId);
            
        }
        else
            return(9004);
    }

    function DeleteService($iServiceId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objSystemLog;
		
        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Services[3] == 0) // Delete Disabled
			return(1010);
		
       if ($objDatabase->DBCount("fms_inventory_services AS S", "S.ServiceId <> '$iServiceId'" ) < 0) return(9007);
		$varResult = $objDatabase->Query("
		SELECT * FROM fms_inventory_services AS S
		WHERE S.ServiceId = '$iServiceId'
		");
		
		$sServiceName = $objDatabase->Result($varResult, 0, "S.ServiceName");
		$sServiceName = $objDatabase->RealEscapeString($sServiceName);

        $varResult = $objDatabase->Query("DELETE FROM fms_inventory_services WHERE ServiceId='$iServiceId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Delete Service - " . $sServiceName);

            return(9005);
        }
        else
            return(9006);
    }

    function AddNewService($sServiceName, $sServiceCode, $sServiceFrequency, $dServicePrice, $sDescription, $iServiceStatus, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
		
        // Employee Roles
   		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_Services[1] == 0) // Add Disabled
			return(1010);
				
		$varNow = $objGeneral->fnNow();
		$iServiceAddedBy = $objEmployee->iEmployeeId;
		
   		// Service Code already added in the System
		$sServiceCode = $objDatabase->RealEscapeString($sServiceCode);
   		if ($objDatabase->DBCount("fms_inventory_services AS S", "S.ServiceCode = '$sServiceCode' ") > 0) return(9000);

        $sServiceName = $objDatabase->RealEscapeString($sServiceName);
		$sServiceFrequency = $objDatabase->RealEscapeString($sServiceFrequency);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
        $sNotes = $objDatabase->RealEscapeString($sNotes);

        $varResult = $objDatabase->Query("INSERT INTO fms_inventory_services
        (ServiceName, ServiceCode, ServiceFrequency, ServicePrice, Description, Notes, Status, ServiceAddedOn, ServiceAddedBy)
        VALUES
        ('$sServiceName', '$sServiceCode', '$sServiceFrequency', '$dServicePrice', '$sDescription', '$sNotes', '$iServiceStatus', '$varNow', '$iServiceAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_inventory_services AS S WHERE S.ServiceName='$sServiceName' AND S.ServiceAddedOn='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Add New Service - " . $sServiceName);
            $objGeneral->fnRedirect('?error=9001&id=' . $objDatabase->Result($varResult, 0, "S.ServiceId"));
        }
        else
            return(9002);
    }

    function GetServiceInfo($iServiceId)
    {
    	global $objDatabase;

    	$varResult = $objDatabase->Query("SELECT * FROM fms_inventory_services AS S WHERE S.ServiceId = '$iServiceId'");

		if ($objDatabase->RowsNumber($varResult) <= 0) return("");

    	$iServiceId = $objDatabase->Result($varResult, 0, "S.ServiceId");
         $sServiceName = $objDatabase->Result($varResult, $i, "S.ServiceName");
    	 $sServiceName = $objDatabase->RealEscapeString($sServiceName);
		 $sServiceName = stripslashes($sServiceName);
		 $sServiceCode = $objDatabase->Result($varResult, $i, "S.ServiceCode");
		 $sServiceCode = $objDatabase->RealEscapeString($sServiceCode);
		 $sServiceCode = stripslashes($sServiceCode);
	     
		$sReturn = '
	     <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Service Information:</span></td></tr>
		 <tr bgcolor="#edeff1"><td width="20%">Service Id:</td><td><strong>' . $iServiceId . ' (Automatically Assigned)</strong></td></tr>
		 <tr bgcolor="#edeff1"><td>Service Name:</td><td><strong>' . $sServiceName . '</strong></td></tr>
		 <tr><td>Service Code:</td><td><strong>' . $sServiceCode . '</strong></td></tr>';

		return($sReturn);
    }

}

// Class: Services Type
class clsServices_Types
{
	// Class Constructor
	function __construct()
	{
	}
	
	function ShowAllServiceTypes($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_ServiceTypes[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Service Types";
		if ($sSortBy == "") $sSortBy = "ST.ServiceTypeId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((ST.ServiceTypeId LIKE '%$sSearch%') OR (ST.ServiceTypeName LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_inventory_services_types AS ST", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_inventory_services_types AS ST
		INNER JOIN organization_employees AS E ON E.EmployeeId = ST.ServiceTypeAddedBy
		WHERE 1=1 $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td width="20%" align="left"><span class="WhiteHeading">Service Type Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=ST.ServiceTypeId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Service Type Id in Ascending Order" title="Sort by Service Type Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=ST.ServiceTypeId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Service Type Id in Descending Order" title="Sort by Service Type Id in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Service Type Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=ST.ServiceTypeName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Service Type Name in Ascending Order" title="Sort by Service Type Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=ST.ServiceTypeName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Service Type Name in Descending Order" title="Sort by Service Type Name in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iServiceTypeId = $objDatabase->Result($varResult, $i, "ST.ServiceTypeId");			
			$sServiceTypeName = $objDatabase->Result($varResult, $i, "ST.ServiceTypeName");
			$sServiceTypeName = $objDatabase->RealEscapeString($sServiceTypeName);
			$sServiceTypeName = stripslashes($sServiceTypeName);						
			$dServiceTypeAddedOn = $objDatabase->Result($varResult, $i, "ST.ServiceTypeAddedOn");
						
			$sEditServiceType = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sServiceTypeName)) . '\', \'../inventory/services_servicetypes_details.php?action2=edit&id=' . $iServiceTypeId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Service Type Details" title="Edit this Service Type Details"></a></td>';
			$sDeleteServiceType = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Service Type?\')) {window.location = \'?action=DeleteServiceType&id=' . $iServiceTypeId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Service Type" title="Delete this Service Type"></a></td>';
						
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_ServiceTypes[2] == 0) // Edit Disabled
				$sEditServiceType = '';
				
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_ServiceTypes[3] == 0) // Edit Disabled
				$sDeleteServiceType = '';
			
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			
			  <td align="left" valign="top">' . $iServiceTypeId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			  <td align="left" valign="top">' . $sServiceTypeName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			  <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sServiceTypeName)) . '\', \'../inventory/services_servicetypes_details.php?id=' . $iServiceTypeId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Service Type Details" title="View this Service Type Details"></a></td>
			' . $sEditServiceType . $sDeleteServiceType . '</tr>';
		}

		$sAddServicesType = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Service Type\', \'../inventory/services_servicetypes_details.php?action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Service Type">';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_ServiceTypes[1] == 0) // Add Disabled
			$sAddServicesType = '';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddServicesType . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Service Type:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Service Type" title="Search for a Service Type" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Service Type Details" alt="View this Service Type Details"></td><td>View this Service Type Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Service Type Details" alt="Edit this Service Type Details"></td><td>Edit this Service Type Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Service Type" alt="Delete this Service Type"></td><td>Delete this Service Type</td></tr>
		</table>';

		return($sReturn);
	}
	
	function ServiceTypeDetails($iServiceTypeId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();	

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_inventory_services_types AS ST
		INNER JOIN organization_employees AS E ON E.EmployeeId = ST.ServiceTypeAddedBy
		WHERE ST.ServiceTypeId = '$iServiceTypeId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?action2=edit&id=' . $iServiceTypeId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Service Type Details" title="Edit this Service Type Details" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Service Type?\')) {window.location = \'?action=DeleteServiceType&id=' . $iServiceTypeId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Service Type" title="Delete this Service Type" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_ServiceTypes[2] == 0)
   			$sButtons_Edit = '';
			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_ServiceTypes[3] == 0)
			$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">ServicesType Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iServiceTypeId = $objDatabase->Result($varResult, 0, "ST.ServiceTypeId");
				
				$sServiceTypeName = $objDatabase->Result($varResult, 0, "ST.ServiceTypeName");
				$sServiceTypeName = $objDatabase->RealEscapeString($sServiceTypeName);
				$sServiceTypeName = stripslashes($sServiceTypeName);
				
				$iServiceTypeAddedBy = $objDatabase->Result($varResult, 0, "ST.ServiceTypeAddedBy");
				$sServiceTypeAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sServiceTypeAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sServiceTypeAddedBy)) . '\', \'../organization/employees_details.php?id=' . $iServiceTypeAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sDescription = $objDatabase->Result($varResult, $i, "ST.Description");
				
				$sNotes = $objDatabase->Result($varResult, $i, "ST.Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
					
				$dServiceTypeAddedOn = $objDatabase->Result($varResult, 0, "ST.ServiceTypeAddedOn");
				$sServiceTypeAddedOn = date("F j, Y", strtotime($dServiceTypeAddedOn)) . ' at ' . date("g:i a", strtotime($dServiceTypeAddedOn));
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sServiceTypeName = '<input type="text" name="txtServiceTypeName" id="txtServiceTypeName" class="form1" value="' . $sServiceTypeName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
			}
			else if ($sAction2 == "addnew")
			{
				$iServiceTypeId = "";
				$sServiceTypeName = $objGeneral->fnGet("txtServiceTypeName");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$sDescription = $objGeneral->fnGet("txtDescription");
								
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sServiceTypeAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sServiceTypeAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sServiceTypeAddedBy)) . '\', \'../organization/employees_details.php?id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sServiceTypeName = '<input type="text" name="txtServiceTypeName" id="txtServiceTypeName" class="form1" value="' . $sServiceTypeName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				
				$sServiceTypeAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if (GetVal(\'txtServiceTypeName\') == "") return(AlertFocus(\'Please enter a valid Service Type Name\', \'txtServiceTypeName\'));
					
				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Service Type Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" width="25%">Service Type Id:</td><td><strong>' . $iServiceTypeId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Service Type Name:</td><td><strong>' . $sServiceTypeName . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Service Type Added On:</td><td><strong>' . $sServiceTypeAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Service Type Added By:</td><td><strong>' . $sServiceTypeAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iServiceTypeId . '"><input type="hidden" name="action" id="action" value="UpdateServiceType"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Service Type" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewServiceType"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewServiceType($sServiceTypeName, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_ServiceTypes[1] == 0) // Add Disabled
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		
		$sServiceTypeName = $objDatabase->RealEscapeString($sServiceTypeName);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		
		$iServiceTypeAddedBy = $objEmployee->iEmployeeId;
		
		if($objDatabase->DBCount("fms_inventory_services_types AS ST", "ST.ServiceTypeName = '$sServiceTypeName'") > 0) return(9100);
		
		$varResult = $objDatabase->Query("INSERT INTO fms_inventory_services_types
		(ServiceTypeName, Description, Notes, ServiceTypeAddedOn, ServiceTypeAddedBy)
		VALUES ('$sServiceTypeName', '$sDescription', '$sNotes', '$varNow', '$iServiceTypeAddedBy')");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_inventory_services_types AS ST WHERE ST.ServiceTypeName='$sServiceTypeName' AND ST.ServiceTypeAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$objSystemLog->AddLog("Add New Service Type - " . $sServiceTypeName);
				$objGeneral->fnRedirect('?error=9101&id=' . $objDatabase->Result($varResult, 0, "ST.ServiceTypeId"));
			}
		}

		return(9102);
	}
	
	function UpdateServiceType($iServiceTypeId, $sServiceTypeName, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_ServiceTypes[2] == 0) // Edit Disabled
			return(1010);
		
		$sServiceTypeName = $objDatabase->RealEscapeString($sServiceTypeName);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		
		if($objDatabase->DBCount("fms_inventory_services_types AS ST", "ST.ServiceTypeName = '$sServiceTypeName' AND ST.ServiceTypeId <> '$iServiceTypeId'") > 0) return(5220);
		
		$varResult = $objDatabase->Query("
		UPDATE fms_inventory_services_types
		SET	
			ServiceTypeName='$sServiceTypeName', 
			Description='$sDescription',
			Notes='$sNotes'			
		WHERE ServiceTypeId='$iServiceTypeId'");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Update Service Type - " . $sServiceTypeName);
			$objGeneral->fnRedirect('../inventory/services_servicetypes_details.php?error=9103' . '&id=' . $iServiceTypeId);
			//return(3002);
		}
		else
			return(9104);
	}
	
	function DeleteServiceType($iServiceTypeId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Inventory_ServiceTypes[3] == 0) // Delete Disabled
			return(1010);
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_inventory_services_types AS ST WHERE ST.ServiceTypeId='$iServiceTypeId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(9107);

		$sServiceTypeName = $objDatabase->Result($varResult, 0, "ST.ServiceTypeName");
		$sServiceTypeName = $objDatabase->RealEscapeString($sServiceTypeName);

		$varResult = $objDatabase->Query("DELETE FROM fms_inventory_services_types WHERE ServiceTypeId='$iServiceTypeId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$objSystemLog->AddLog("Delete Service Type - " . $sServiceTypeName);
			return(9105);
		}
		else
			return(9106);
	}

}

?>