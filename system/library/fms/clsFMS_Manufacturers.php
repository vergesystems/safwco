<?php

include('../../system/library/fms/clsFMS_Manufacturers_Manufacturers.php');

class clsManufacturers
{
    // Constructor
    function __construct()
    {
    }

    function ShowManufacturersMenu()
    {
    	global $objEmployee;

    	$sCurrentFile = $_SERVER['SCRIPT_NAME'];
    	$aCurrentFile = explode('/', $sCurrentFile);
    	$sCurrentFile = $aCurrentFile[count($aCurrentFile)-1];

    	$sManufacturers = '<a ' . (($sCurrentFile == "manufacturers.php") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../manufacturers/manufacturers.php"><img src="../images/manufacturers/iconManufacturers.gif" alt="Manufacturers" title="Manufacturers" border="0" /><br />Manufacturers</a>';
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Manufacturers_Manufacturers[0] == 0)
    		$sManufacturers = '<img src="../images/manufacturers/iconManufacturers_disabled.gif" alt="Manufacturers" title="Manufacturers" border="0" /><br />Manufacturers';

    	$sReturn .= '
    	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
    	 <tr>
    	  <td valign="bottom">

         <table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
	    	<tr>
	    	 <td align="center" width="10%" valign="top">' . $sManufacturers . '</td>
	    	</tr>
	       </table>
    	  </td></tr></table>';
    	
    	return($sReturn);    	
    }
    
	
}

?>