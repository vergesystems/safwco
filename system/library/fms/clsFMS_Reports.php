<?php
include('../../system/library/fms/clsFMS_Reports_CustomerReports.php');
include('../../system/library/fms/clsFMS_Reports_VendorReports.php');
include('../../system/library/fms/clsFMS_Reports_BankingReports.php');
include('../../system/library/fms/clsFMS_Reports_AccountReports.php');
include('../../system/library/fms/clsFMS_Reports_FinancialStatements.php');
include('../../system/library/fms/clsFMS_Reports_Graphs.php');

class clsFMS_Reports
{
    // Constructor
    function __construct()
    {
		
    }

    function ShowFMSReportsMenu($sPage)
    {
    	global $objEmployee;
		
    	$sCustomerReports = '<a ' . (($sPage == "Customers") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../reports/?page=Customers"><img src="../images/customers/iconCustomers.gif" alt="Customer Reports" title="Customer Reports" border="0" /><br />Customer Reports</a>';
		$sVendorReports = '<a ' . (($sPage == "Vendors") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../reports/?page=Vendors"><img src="../images/vendors/iconVendors.gif" alt="Vendor Reports" title="Vendor Reports" border="0" /><br />Vendor Reports</a>';
		$sBankingReports = '<a ' . (($sPage == "Banking") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../reports/?page=Banking"><img src="../images/banking/iconBanks.gif" alt="Banking Reports" title="Banking Reports" border="0" /><br />Banking Reports</a>';
		$sAccountReports = '<a ' . (($sPage == "Accounts") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../reports/?page=Accounts"><img src="../images/reports/iconAccountsReports.gif" alt="Accounts Reports" title="Accounts Reports" border="0" /><br />Accounts Reports</a>';
		$sFinancialStatements = '<a ' . (($sPage == "FinancialStatements") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../reports/?page=FinancialStatements"><img src="../images/reports/iconFinancialStatements.gif" alt="Financial Statements" title="Financial Statements" border="0" /><br />Financial Statements</a>';
		$sGraphs = '<a ' . (($sPage == "Graphs") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../reports/?page=Graphs"><img src="../images/reports/iconReports_Graphs.gif" alt="Graphs" title="Graphs" border="0" /><br />Graphs</a>';

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports[0] == 0)
    		$sCustomerReports = '<img src="../images/customers/iconCustomers_disabled.gif" alt="Customer Reports" title="Customer Reports" border="0" /><br />Customer Reports';
			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports[0] == 0)
    		$sVendorReports = '<img src="../images/reports/iconVendorReport_disabled.gif" alt="Vendor Reports" title="Vendor Reports" border="0" /><br />Vendor Reports';
				
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports[0] == 0)
    		$sBankingReports = '<img src="../images/reports/iconBankingReport_disabled.gif" alt="Banking Reports" title="Banking Reports" border="0" /><br />Banking Reports';
			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports[0] == 0)
    		$sAccountReports = '<img src="../images/accounts/iconAccountsRegister_disabled.gif" alt="Accounts Reports" title="Accounts Reports" border="0" /><br />Accounts Reports';
			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_FinancialReports[0] == 0)
    		$sFinancialStatements = '<img src="../images/accounts/iconAccountsRegister_disabled.gif" alt="Financial Reports" title="Financial Reports" border="0" /><br />Financial Reports';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Graphs[0] == 0)
			$sGraphs = '<img src="../images/reports/iconReports_Graphs_disabled.gif" alt="Graphs" title="Graphs" border="0" /><br />Graphs';
		
    	$sReturn .= '
     	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
          <td align="right">

           <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
    	    <tr>
			 <td valign="top" align="center" width="10%">' . $sBudgetReports . '</td>
    	     <td valign="top" align="center" width="10%">' . $sAccountReports . '</td>
			 <td valign="top" align="center" width="10%">' . $sFinancialStatements . '</td>
			 <td valign="top" align="center" width="10%">' . $sVendorReports . '</td>
    	     <td valign="top" align="center" width="10%">' . $sCustomerReports . '</td>
			 <td valign="top" align="center" width="10%">' . $sBankingReports . '</td>
			 <td valign="top" align="center" width="10%">' . $sGraphs . '</td>
    	    </tr>
    	   </table>

    	   </td>
         </tr>
        </table>';

    	return($sReturn);
    }
	
	function ShowReportsPages($sPage)
	{
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		if ($sPage == "") $sPage = "Accounts";
		
		switch($sPage)
		{
			case "Accounts": 
				$aTabs[0][0] = 'Accounts Reports';
				$aTabs[0][1] = '../reports/accountreports.php';
				break;
			case "FinancialStatements": 
				$aTabs[0][0] = 'Financial Statemetns';
				$aTabs[0][1] = '../reports/financialstatements.php';
				break;
			case "Vendors": 
				$aTabs[0][0] = 'Vendors Reports';
				$aTabs[0][1] = '../reports/vendorreports.php';
				break;
			case "Customers": 
				$aTabs[0][0] = 'Customers Reports';
				$aTabs[0][1] = '../reports/customerreports.php';
				break;
			case "Banking": 
				$aTabs[0][0] = 'Banking Reports';
				$aTabs[0][1] = '../reports/bankingreports.php';
				break;
			case "Graphs": 
				$aTabs[0][0] = 'Graphs';
				$aTabs[0][1] = '../reports/graphs.php';
				break;
		}
		
		$sReturn = $objDHTMLSuite->TabBar($aTabs, $this->ShowFMSReportsMenu($sPage));
		return($sReturn);
	}
	
    function GenerateReportCriteria($sCriteriaName, $sFieldName, $bCriteriaAll_Employees = true, $bCriteriaAll_Customers = true, $bCriteriaAll_Categories = true, $bCriteriaAll_ControlCodes=true, $bCriteriaAll_ChartOfAccounts=true, $bCriteriaAll_Vendors = true, $bCriteriaAll_Donors = true, $bCriteriaAll_DonorProjects = true, $bCriteriaAll_ProjectActivities = true, $bCriteriaAll_Banks = true, $bCriteriaAll_BankAccounts = true, $bCriteriaAll_Manufacturers = true)
    {
    	global $objDatabase;
    	global $objEmployee;
		
		include_once(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();
		include_once('../../system/library/clsOrganization.php');
		$objStations = new clsStations();
		
		$iEmployeeId = $objEmployee->iEmployeeId;
		
		$sEmployeeStationAccess = $objStations->GetStationAccess();
		
    	$dDateRangeStart = date("Y-m-d", mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-1));
    	$dDateRangeEnd = date("Y-m-d");
		
		$dDate = date("Y-m-d");

		$aMonths = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		$aYears = array("2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012");

    	switch ($sCriteriaName)
    	{
			case "Station":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				$sCriteriaAll = '<option value="-1">All Stations</option>';

				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE 1=1 AND S.STATUS='1' $sEmployeeStationAccess ORDER BY S.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option value="' . $objDatabase->Result($varResult, $i, "S.StationId") . '">' . $objDatabase->Result($varResult, $i, "S.StationName") . '</option>';
				$sCriteria .= '</select>';
				break;
			case "Vendor":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				//$sCriteriaAll = '<option value="-1">All Vendors</option>';

				if ($bCriteriaAll_Vendors) $sCriteriaAll .= '<option value="-1">All Vendors</option>';

				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE 1=1 ORDER BY V.VendorName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option value="' . $objDatabase->Result($varResult, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult, $i, "V.VendorName") . '</option>';
				$sCriteria .= '</select>';
				break;
			case "Manufacturer":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				if ($bCriteriaAll_Manufacturers) $sCriteriaAll .= '<option value="-1">All Manufacturers</option>';
				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_manufacturers AS M WHERE 1=1 ORDER BY M.ManufacturerName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option value="' . $objDatabase->Result($varResult, $i, "M.ManufacturerId") . '">' . $objDatabase->Result($varResult, $i, "M.ManufacturerName") . '</option>';
				$sCriteria .= '</select>';
				break;
			case "Category":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				if ($bCriteriaAll_Manufacturers) $sCriteriaAll .= '<option value="-1">All Categories</option>';
				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_categories AS C WHERE 1=1 ORDER BY C.CategoryName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option value="' . $objDatabase->Result($varResult, $i, "C.CategoryId") . '">' . $objDatabase->Result($varResult, $i, "C.CategoryName") . '</option>';
				$sCriteria .= '</select>';
				break;
				
			case "ReportOption_DonorLogo":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				if ($bCriteriaAll_Donors) $sCriteriaAll .= '<option value="-1">No Donor</option>';
				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
				$sCriteria .='</select>';
				break;					
			case "Donor":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '" onchange="GetDonorProjects_Reports(GetSelectedListBox(\'selDonor\'),0);" >';	
				if ($bCriteriaAll_Donors) $sCriteriaAll .= '<option value="-1">All Donors</option>';
				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
				$sCriteria .='</select>';				
				
				if ($objEmployee->objEmployeeRoles->iEmployeeRole_FMS_Accounts_AccessAllProjects == 1) // Can access All Projectts...
				{
					$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.Status='1'");
					$iNoOfDonorProjects = $objDatabase->RowsNumber($varResult);
					$sDonorProjects = 'var aDonorProjects = MultiDimensionalArray(' . $iNoOfDonorProjects . ', 4);';
					for ($i=0; $i < $iNoOfDonorProjects; $i++)
					{
						$sDonorProjects .= 'aDonorProjects[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "DP.DonorId") . ';';
						$sDonorProjects .= 'aDonorProjects[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . ';';
						$sDonorProjects .= 'aDonorProjects[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '";';
						$sDonorProjects .= 'aDonorProjects[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '";';
					}
				}else
				{					
					$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP 						
					INNER JOIN organization_employees_donorprojects AS EDP ON EDP.DonorProjectId = DP.DonorProjectId
					INNER JOIN organization_employees AS E ON E.EmployeeId = EDP.EmployeeId 
					WHERE E.EmployeeId='$iEmployeeId' ORDER BY DP.DonorProjectId");					
					$iNoOfDonorProjects = $objDatabase->RowsNumber($varResult);
					$sDonorProjects = 'var aDonorProjects = MultiDimensionalArray(' . $iNoOfDonorProjects . ', 4);';
					for ($i=0; $i < $iNoOfDonorProjects; $i++)
					{
						$sDonorProjects .= 'aDonorProjects[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "DP.DonorId") . ';';
						$sDonorProjects .= 'aDonorProjects[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . ';';
						$sDonorProjects .= 'aDonorProjects[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '";';
						$sDonorProjects .= 'aDonorProjects[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '";';
					}
					
				}			
	
					

				// Project Activities
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.Status='1'");
				$iNoOfProjectActivities = $objDatabase->RowsNumber($varResult);
				$sProjectActivities = 'var aProjectActivities= MultiDimensionalArray(' . $iNoOfProjectActivities . ', 4);';
				for ($i=0; $i < $iNoOfProjectActivities; $i++)
				{
					$sProjectActivities .= 'aProjectActivities[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "PA.DonorProjectId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "PA.ActivityId") . ';';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityCode") . '";';
					$sProjectActivities .= 'aProjectActivities[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "PA.ActivityTitle") . '";';
				}
				$sCriteria .='
				<script language="JavaScript" type="text/javascript">
				' . $sDonorProjects . '
				' . $sProjectActivities . '
				</script>';
				break;
			case "DonorProject":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '" onchange="GetProjectActivities_Reports(GetSelectedListBox(\'selDonorProject\'),0);">';
				if ($bCriteriaAll_DonorProjects) $sCriteriaAll .= '<option value="-1">All Projects</option>';
				$sCriteria .= $sCriteriaAll;
				$sCriteria .= '</select>';
				break;
			case "ProjectActivity":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				if ($bCriteriaAll_ProjectActivities) $sCriteriaAll .= '<option value="-1">All Activities</option>';
				$sCriteria .= $sCriteriaAll;
				$sCriteria .= '</select>';
				break;
			case "Bank":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '" onchange="GetBankAccountsReports(GetSelectedListBox(\'selBank\'),0);">';
				if ($bCriteriaAll_Banks) $sCriteriaAll .= '<option value="-1">All Banks</option>';
				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B ORDER BY B.BankName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option ' . (($iBnkId == $objDatabase->Result($varResult, $i, "B.BankId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "B.BankId") . '">' . $objDatabase->Result($varResult, $i, "B.BankName") . '</option>';
				$sCriteria .='</select>';
				// Bank Accounts
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.Status='1'");
				$iNoOfBankAccounts = $objDatabase->RowsNumber($varResult);
				$sBankAccounts = 'var aBankAccounts = MultiDimensionalArray(' . $iNoOfBankAccounts . ', 4);';
				for ($i=0; $i < $iNoOfBankAccounts; $i++)
				{
					$sBankAccounts .= 'aBankAccounts[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "BA.BankId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . ';';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . '";';
					$sBankAccounts .= 'aBankAccounts[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "BA.AccountTitle") . '";';
				}
				$sCriteria .='
				<script language="JavaScript" type="text/javascript">
				' . $sBankAccounts. '
				</script>';
				break;
			case "BankAccount":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				if ($bCriteriaAll_BankAccounts) $sCriteriaAll .= '<option value="-1">All Bank Accounts</option>';
				$sCriteria .= $sCriteriaAll;
				$sCriteria .= '</select>';
				break;
			case "Employee":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';

				if ($bCriteriaAll_Employees) $sCriteriaAll .= '<option value="-1">All Employees</option>';

				if ($objEmployee->objEmployeeRoles->iEmployeeRole_Employees_EmployeeStationAccess == 0) // Access on employee branch only
					$sEmployeeRestriction = " AND E.StationId='$objEmployee->iEmployeeStationId'";

				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.Status='1' $sEmployeeRestriction $sEmployeeCustomerRestriction ORDER BY E.FirstName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option value="' . $objDatabase->Result($varResult, $i, "E.EmployeeId") . '">' . $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName") . '</option>';
				$sCriteria .= '</select>';
				break;
			case "Customer":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';

				if ($bCriteriaAll_Customers) $sCriteriaAll .= '<option value="-1">All Customers</option>';

				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C WHERE C.Status='1' ORDER BY C.FirstName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option value="' . $objDatabase->Result($varResult, $i, "C.CustomerId") . '">' . $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") . '</option>';
				$sCriteria .= '</select>';
				break;
			case "BankAccount":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';

				if ($bCriteriaAll_Customers) $sCriteriaAll .= '<option value="-1">All Bank Accounts</option>';

				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.Status='1'  ORDER BY BA.AccountTitle");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option value="' . $objDatabase->Result($varResult, $i, "BA.BankAccountId") . '">' . $objDatabase->Result($varResult, $i, "BA.AccountTitle") . ' - ' . $objDatabase->Result($varResult, $i, "BA.BankAccountNumber") . '</option>';
				$sCriteria .= '</select>';
				break;
			case "ChartOfAccountCategory";	
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '" onchange="GetChartOfAccountControl(GetSelectedListBox(\'selChartOfAccountCategory\'));">';
				if ($bCriteriaAll_Categories) $sCriteriaAll .= '<option value="-1">All Categories</option>';
				$sCriteria .= $sCriteriaAll;				
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option ' . (($iChartOfAccountsCategoryId == $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsCategoryId")) ? 'selected="true"' : '')  . ' value="' . $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsCategoryId") . '">' . $objDatabase->Result($varResult, $i, "CAC.CategoryCode") . ' - ' . $objDatabase->Result($varResult, $i, "CAC.CategoryName") . '</option>';
				$sCriteria .='</select>';
			break;
			case "ChartOfAccountControl":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '" onchange="GetChartOfAccounts(GetSelectedListBox(\'selChartOfAccountControl\'));">';
				if ($bCriteriaAll_ControlCodes) $sCriteriaAll .= '<option value="-1">All Control Codes</option>';
				$sCriteria .= $sCriteriaAll;
				$sCriteria .= '</select>';
				break;
			case "ChartOfAccount":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				if ($bCriteriaAll_ChartOfAccounts) $sCriteriaAll .= '<option value="-1">All Chart of Accounts</option>';				
				$sCriteria .= $sCriteriaAll;				
				$sCriteria .= '</select>';
				
				$sCriteria .='
				<script language="JavaScript" type="text/javascript">
				function GetChartOfAccountControl(iCategoryId)
				{
					OptionsList_RemoveAll("selChartOfAccount");
					FillSelectBox("selChartOfAccount", "All Chart Of Accounts", -1);
										
					if(iCategoryId > 0)
						xajax_AJAX_FMS_Reports_GetControlCode(iCategoryId, "selChartOfAccountControl");
				}
				function GetChartOfAccounts(iControlId)
				{	
					if(iControlId > 0) xajax_AJAX_FMS_Reports_GetChartOfAccounts(iControlId, "selChartOfAccount");
				}				
				</script>';
				break;					
			case "Budget":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				if ($bCriteriaAll_Manufacturers) $sCriteriaAll .= '<option value="-1">All Budgets</option>';
				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE 1=1 ORDER BY B.Title");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option value="' . $objDatabase->Result($varResult, $i, "B.BudgetId") . '">' . $objDatabase->Result($varResult, $i, "B.Title") . '</option>';
				$sCriteria .= '</select>';
				break;
				case "Donor_Budget":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '" onchange="GetDonorProjects_Budget(GetSelectedListBox(\'selDonor\'));" >';
				if ($bCriteriaAll_Donors) $sCriteriaAll .= '<option value="-1">All Donors</option>';
				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D ORDER BY D.DonorName");
				for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option ' . (($iDonorId == $objDatabase->Result($varResult, $i, "D.DonorId")) ? 'selected="true"' : '') . 'value="' . $objDatabase->Result($varResult, $i, "D.DonorId") . '">' . $objDatabase->Result($varResult, $i, "D.DonorCode") . '</option>';
				$sCriteria .='</select>';
				// Donor Projects
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.Status='1'");
				$iNoOfDonorProjects = $objDatabase->RowsNumber($varResult);
				$sDonorProjects = 'var aDonorProjects = MultiDimensionalArray(' . $iNoOfDonorProjects . ', 4);';
				for ($i=0; $i < $iNoOfDonorProjects; $i++)
				{
					$sDonorProjects .= 'aDonorProjects[' . $i . '][0] = ' . $objDatabase->Result($varResult, $i, "DP.DonorId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][1] = ' . $objDatabase->Result($varResult, $i, "DP.DonorProjectId") . ';';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][2] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectCode") . '";';
					$sDonorProjects .= 'aDonorProjects[' . $i . '][3] = "' . $objDatabase->Result($varResult, $i, "DP.ProjectTitle") . '";';
				}
				$sCriteria .='
				<script language="JavaScript" type="text/javascript">
				' . $sDonorProjects . '				
				</script>';
				break;
			case "DonorProject_Budget":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				if ($bCriteriaAll_DonorProjects) $sCriteriaAll .= '<option value="-1">All Projects</option>';
				$sCriteria .= $sCriteriaAll;
				$sCriteria .= '</select>';
				break;	
			case "Date":
				// Calendar Control				
				$sDate = '<input size="10" type="text" id="' . $sFieldName . '" name="' . $sFieldName . '" class="Tahoma16" value="' . $dDate . '" />' . $objjQuery->Calendar($sFieldName);

				$sCriteria = $sDate;
				break;
				/*$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '_Month" id="' . $sFieldName . '_Month">';
				for ($i=0; $i < count($aMonths); $i++)
					$sCriteria .= '<option ' . ((($i+1) == date("n")) ? 'selected="true"' : '') . ' value="' . ($i+1) . '">' . $aMonths[$i] . '</option>';

				$sCriteria .= '</select>&nbsp;<select class="Tahoma16" name="' . $sFieldName . '_Year" id="' . $sFieldName . '_Year">';
				for ($i=0; $i < count($aYears); $i++)
					$sCriteria .= '<option ' . (($aYears[$i] == date("Y")) ? 'selected="true"' : '') . ' value="' . $aYears[$i] . '">' . $aYears[$i] . '</option>';
				$sCriteria .= '</select>';
				break;*/

			case "Year":
				$sCriteria .= '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				for ($i=0; $i < count($aYears); $i++)
					$sCriteria .= '<option ' . (($aYears[$i] == date("Y")) ? 'selected="true"' : '') . ' value="' . $aYears[$i] . '">' . $aYears[$i] . '</option>';
				$sCriteria .= '</select>';

				break;
			/*
			case "SourceOfIncome":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				$sCriteriaAll = '<option value="-1">Any Source Of Income</option>';

				$sCriteria .= $sCriteriaAll;
				$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_sourceofincome AS SOI WHERE 1=1 ORDER BY SOI.Title");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
					$sCriteria .= '<option value="' . $objDatabase->Result($varResult, $i, "SOI.SourceOfIncomeId") . '">' . $objDatabase->Result($varResult, $i, "SOI.Title") . '</option>';
				$sCriteria .= '</select>';
				break;
			*/
			case "DateRange":
				// Calendar Control
				$objDHTMLSuite = new clsDHTMLSuite();
				$sDateRangeStart = '<input size="10" type="text" id="' . $sFieldName . '_Start" name="' . $sFieldName . '_Start" class="Tahoma16" value="' . $dDateRangeStart . '" />' . $objjQuery->Calendar($sFieldName . '_Start');
				$sDateRangeEnd = '<input size="10" type="text" id="' . $sFieldName . '_End" name="' . $sFieldName . '_End" class="Tahoma16" value="' . $dDateRangeEnd . '" />&nbsp;' . $objjQuery->Calendar($sFieldName . '_End');

				$sCriteria = $sDateRangeStart . '&nbsp;&nbsp; to &nbsp;&nbsp;' . $sDateRangeEnd;
				break;
			
			case "VoucherType":
				$sCriteria = '<select class="Tahoma16" name="' . $sFieldName . '" id="' . $sFieldName . '">';
				if ($bCriteriaAll_DonorProjects) $sCriteriaAll .= '<option value="-1">All Vouchers</option>';
				$sCriteria .= $sCriteriaAll;
				$sCriteria .= '<option value="BPV">BPV</option>';
				$sCriteria .= '<option value="BRV">BRV</option>';
				$sCriteria .= '<option value="CPV">CPV</option>';
				$sCriteria .= '<option value="CRV">CRV</option>';
				$sCriteria .= '<option value="JV">JV</option>';
				$sCriteria .= '</select>';
				break;
				
    	}

		return($sCriteria);
    }

    function ShowReport($sReport, $sReportType, $bExportToExcel, &$aExcelData, $sAction = "")
    {
    	switch ($sReport)
    	{
    		case "CustomerReports":
    			$objCustomerReports = new clsFMS_Reports_CustomerReports();
    			return($objCustomerReports->GenerateCustomerReports($sReportType, $bExportToExcel, $aExcelData, $sAction));
    			break;
			case "VendorReports":
    			$objCustomerReports = new clsFMS_Reports_VendorReports();
    			return($objCustomerReports->GenerateVendorReports($sReportType, $bExportToExcel, $aExcelData, $sAction));
    			break;
			case "BankingReports":
    			$objBankingReports = new clsFMS_Reports_BankingReports();
    			return($objBankingReports->GenerateBankingReports($sReportType, $bExportToExcel, $aExcelData, $sAction));
    			break;
			case "AccountsReports":
    			$objAccountsReports = new clsFMS_Reports_AccountReports();
    			return($objAccountsReports->GenerateAccountsReports($sReportType, $bExportToExcel, $aExcelData, $sAction));
    			break;
			case "FinancialStatements":
    			$objFinancialStatements = new clsFMS_Reports_FinancialStatements();
    			return($objFinancialStatements->GenerateFinancialStatements($sReportType, $bExportToExcel, $aExcelData, $sAction));
    			break;
			case "BudgetReports":
    			$objBudgetReports = new clsFMS_Reports_BudgetReports();
    			return($objBudgetReports->GenerateBudgetReports($sReportType, $bExportToExcel, $aExcelData, $sAction));
    			break;
			case "Graphs":
    			$objGraphs = new clsFMS_Reports_Graphs();
    			return($objGraphs->GenerateGraphs($sReportType, $bExportToExcel, $aExcelData, $sAction));
    			break;
    	}
    }

    function ExportReport($sReportString, $sReportType, $aExcelData)
    {
    	global $objGeneral;

    	switch ($sReportType)
    	{
    		case "pdf":
    			$sFileName = '../../system/data/temp/test1.html';
				//include(cVSFFolder . "/components/dompdf-0.5.1/clsExcelWriter.php");
				/*
				$url = cVSFFolder . "/components/dompdf-0.5.1/dompdf.php?input_file=" . rawurlencode($sFileName) .
				"&paper=letter&output_file=" . rawurlencode("Report.pdf");
				header("Location: http://localhost/vergesystems/VSF-PHP/" .  "/$url");
				*/
    			$varFileHandle = @fopen($sFileName ,'w'); $varBytes = @fwrite($varFileHandle, $sReportString); fclose($varFileHandle);
    			$objGeneral->fnURL2PDF(str_replace('../../', cServerURL . '/', $sFileName), "Report.pdf");
    			break;
    		case "html":
				//Download file
				if(isset($HTTP_SERVER_VARS['HTTP_USER_AGENT']) and strpos($HTTP_SERVER_VARS['HTTP_USER_AGENT'],'MSIE'))
					Header('Content-Type: application/force-download');
				else
					Header('Content-Type: application/octet-stream');
				if(headers_sent())
					die('Some data has already been output to browser, can\'t send the file');
				header('Content-Length: ' . strlen($sReportString));
				header('Content-disposition: attachment; filename=Report.html');
				print($sReportString);
				die();
    			break;
			case "word":
				include(cVSFFolder . '/classes/clsMSWord.php');
				$objDoc = new HTML_TO_DOC();
				$objDoc->createDoc($sReportString, "Report", true);
				break;
    		case "excel":
				include(cVSFFolder . '/classes/clsExcel.php');
				$objDoc = new HTML_TO_DOC();
				$objDoc->createDoc($sReportString, "Report", true);
				break;
				/*include(cVSFFolder . "/classes/clsExcelWriter.php");
				$objExcelWriter = new ExcelWriter(cDataFolder . "/temp/Report.xls");

				if($objExcelWriter == false) print($excel->error);

				for ($i=0; $i < count($aExcelData); $i++)
					$objExcelWriter->writeLine($aExcelData[$i]);

				$objExcelWriter->close();

				header("Location: " . cDataFolder . '/temp/report.xls');
				break;*/
    	}
    }

    function EmailReport($sReportString, $sYourName, $sYourEmail, $sRecipientName, $sRecipientEmail, $sMessage)
    {
    	global $objGeneral;
    	global $objEmployee;

    	$sReturn = "";

    	if ($sYourName == "") $sReturn = "Please enter your name";
        if ($sYourEmail == "") $sReturn = "Please enter your e-mail address";
        if ($sRecipientName == "") $sReturn = "Please enter recipient\'s name";
        if ($sRecipientEmail == "") $sReturn = "Please enter recipient\'s e-mail address";

        if ($sReturn != "") return($sReturn);

        $sEmailMessage = "<html><head></head><body>
		<span style=\"font-family:Tahoma, font-size:12px;\">
        Hello $sRecipientName!
        <br /><br />
        $sYourName &lt;$sYourEmail&gt; has sent you the following report from " . $objEmployee->aSystemSettings['SystemTitle'] . "<br />
        <br />
        <blockquote>$sMessage</blockquote>
        <br />
		<hr size=\"1\" />
		</span>
        $sReportString
        </body>
        </html>";

        $sSubject = 'Report from ' . $objEmployee->aSystemSettings['SystemTitle'];

        $objGeneral->fnSendMail($sYourName, $sYourEmail, $sRecipientName, $sRecipientEmail, $sSubject, $sEmailMessage);
		return('Report has been Emailed to ' . $sRecipientEmail);
    }

	function ShowEmailReport()
	{
		$sReturn = '
		<form method="post" action="?' . $_SERVER['QUERY_STRING'] . '">
		<table border="0" cellspacing="0" cellpadding="3" width="94%" align="center">
		 <tr><td colspan="2" style="font-size:24px; font-family: Tahoma, Arial;">E-Mail This Report<br /></td></tr>
		 <tr><td width="40%" class="Tahoma16">Your Name:</td><td><input type="text" name="EmailDetails_YourName" size="30" class="Tahoma16" id="EmailDetails_YourName" /></td></tr>
		 <tr><td class="Tahoma16">Your E-Mail:</td><td><input type="text" name="EmailDetails_YourEmailAddress" size="30" class="Tahoma16" id="EmailDetails_YourEmailAddress" /></td></tr>
		 <tr><td class="Tahoma16">Recipient\'s Name:</td><td><input type="text" name="EmailDetails_RecipientName" size="30" class="Tahoma16" id="EmailDetails_RecipientName" /></td></tr>
		 <tr><td class="Tahoma16">Recipient\'s E-Mail:</td><td><input type="text" name="EmailDetails_RecipientEmailAddress" size="30" class="Tahoma16" id="EmailDetails_RecipientEmailAddress" /></td></tr>
		 <tr><td class="Tahoma16" colspan="2">Your Message:<br /><textarea style="width:450px; height:120px;" class="Tahoma16" name="EmailDetails_Message" id="EmailDetails_Message"></textarea></td></tr>
		 <tr><td colspan="2" align="center"><input type="submit" class="AdminFormButton1" value="Send E-Mail" /></td></tr>
		</table>
		<input type="hidden" name="action" id="action" value="EmailReport" />
		</form>';

		return($sReturn);
	}

	function ShowExportReport()
	{
		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td class="Tahoma16" colspan="2">Select export format:<br /><br /></td></tr>
		 <tr>
		  <td align="center" valign="top" width="25%"><a href="?action=ExportReport&exporttype=pdf&' . $_SERVER['QUERY_STRING'] . '"><img src="../images/reports/iconExport_PDF.gif" border="0" /><br />Export as PDF File</a></td>
		  <td align="center" valign="top" width="25%"><a href="?action=ExportReport&exporttype=html&' . $_SERVER['QUERY_STRING'] . '"><img src="../images/reports/iconExport_HTML.gif" border="0" /><br />Export as HTML File</a></td>
		  <td align="center" valign="top" width="25%"><a href="?action=ExportReport&exporttype=word&' . $_SERVER['QUERY_STRING'] . '"><img src="../images/reports/iconExport_DOC.gif" border="0" /><br />Export as MS Word File</a></td>
		  <td align="center" valign="top" width="25%"><a href="?action=ExportReport&exporttype=excel&exportxls=true&' . $_SERVER['QUERY_STRING'] . '"><img src="../images/reports/iconExport_XLS.gif" border="0" /><br />Export as Excel File</a></td>
		 </tr>
		</table>';
		
		return($sReturn);
	}
	
	function GenerateReportHeader($sAction)
	{
		global $objEmployee;
		global $objGeneral;
		
		$iDonorLogo = $objGeneral->fnGet("selReportOption_DonorLogo");
		
		$sButtons = '<table border="0" cellspacing="0" cellpadding="1" align="right" id="DontPrint">
		    <tr>
		     <td><a href="javascript:window.print();"><img align="middle" src="../images/icons/iconPrint2.gif" border="0" alt="Print this Report" title="Print this Report"></a></td><td>&nbsp;<a href="javascript:window.print();">Print</a></td>
		     <td>&nbsp;&nbsp;</td><td><a href="#top" onclick="MOOdalBox.open(\'../reports/emailreport.php?' . $_SERVER["QUERY_STRING"] . '\', \'E-Mail Report\', \'500 350\');"><img align="middle" src="../images/icons/iconEmail.gif" border="0" alt="E-Mail this Report" title="E-Mail this Report"></a></td><td>&nbsp;<a href="#noanchor" onclick="MOOdalBox.open(\'../reports/emailreport.php?' . $_SERVER["QUERY_STRING"] . '\', \'E-Mail Report\', \'500 350\');">E-Mail</a></td>
		     <td>&nbsp;&nbsp;</td><td><a href="#top" onclick="MOOdalBox.open(\'../reports/exportreport.php?' . $_SERVER["QUERY_STRING"] . '\', \'Export Report\', \'400 200\');"><img align="middle" src="../images/icons/iconSave.gif" border="0" alt="Export this Report" title="Export this Report"></a></td><td>&nbsp;<a href="#noanchor" onclick="MOOdalBox.open(\'../reports/exportreport.php?' . $_SERVER["QUERY_STRING"] . '\', \'Export Report\', \'400 200\');">Export</a></td>
		    </tr>
		   </table>';
		
		if ($sAction != "") $sButtons = '';
		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="1" width="98%" align="center">
		 <tr>
		  <td>
		   <img src="' . cServerURL . '/system/config/reportheader.jpg" border="0" />
		   ' . (($iDonorLogo > 0) ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="' . cServerURL . '/system/data/vf/donors/' . $iDonorLogo . '.jpg' . '" />' : '') . '
		  </td>
		  <td align="right" valign="top">
		   <span style="font-family:Tahoma, Arial; font-size:12px;">
		    Report Generated on: ' . date("F j, Y"). ' at ' . date("g:i a") . '
		   </span>
		   <br /><br />
		   ' . $sButtons . '
		  </td>
		 </tr>
		</table>';
		return($sReturn);
	}
	
	function GenerateReportFooter()
	{
		global $objEmployee;

		$sReturn = $objEmployee->aSystemSettings['ReportFooter'];
		return($sReturn);
	}

	function ReportStyleSheet()
	{
		$sReturn = '<style type="text/css" media="print">
		DIV.tableContainer { OVERFLOW: visible }
		TABLE > TBODY { OVERFLOW: visible }
		TD { HEIGHT: 14pt }
		THEAD { DISPLAY: table-header-group }
		THEAD TH { POSITION: static }
		THEAD TD { POSITION: static }
		TABLE TFOOT TR { POSITION: static }

		@media print { #DontPrint { display: none; } }
		</style>';

		return($sReturn);
	}

	function ReportFilter(&$sReportCriteria, &$sReportCriteriaSQL)
	{
		global $objGeneral;
		global $objDatabase;
		include_once('../../system/library/clsOrganization.php');
		$objStations = new clsStations();
		
		// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_ChartOfAccountId = $objGeneral->fnGet("selChartOfAccount");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		$iCriteria_CustomerId = $objGeneral->fnGet("selCustomer");
		$iCriteria_CategoryId = $objGeneral->fnGet("selCategory");
		$iCriteria_FixedAssetId = $objGeneral->fnGet("selFixedAsset");
		$iCriteria_VendorId = $objGeneral->fnGet("selVendor");
		$iCriteria_ManufacturerId = $objGeneral->fnGet("selManufacturer");
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		//if ($iCriteria_EmployeeId == '') $iCriteria_EmployeeId = -1;
		if ($iCriteria_StationId == '') $iCriteria_StationId = -1;
		if ($iCriteria_CustomerId == '') $iCriteria_CustomerId = -1;
		if ($iCriteria_CategoryId == '') $iCriteria_CategoryId = -1;
		if ($iCriteria_FixedAssetId == '') $iCriteria_FixedAssetId = -1;
		if ($iCriteria_VendorId == '') $iCriteria_VendorId = -1;
		if($iCriteria_ManufacturerId == '') $iCriteria_ManufacturerId = -1; 
		if ($iCriteria_ChartOfAccountId == '') $iCriteria_ChartOfAccountId = -1;
		
		//$sReportCriteriaSQL = ($iCriteria_EmployeeId != -1) ? " AND E.EmployeeId='$iCriteria_EmployeeId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_StationId != -1) ? " AND S.StationId='$iCriteria_StationId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_CustomerId != -1) ? " AND C.CustomerId='$iCriteria_CustomerId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_CategoryId != -1) ? " AND PC.CategoryId='$iCriteria_CategoryId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_FixedAssetId != -1) ? " AND P.FixedAssetId='$iCriteria_FixedAssetId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_VendorId != -1) ? " AND V.VendorId='$iCriteria_VendorId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_ManufacturerId != -1) ? " AND M.ManufacturerId='$iCriteria_ManufacturerId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_ChartOfAccountId != -1) ? " AND CA.ChartOfAccountsId='$iCriteria_ChartOfAccountId'" : '';
		
		// New Restriction 
		if($iCriteria_StationId == -1)
		{
			$sReportCriteriaSQL .= $objStations->GetStationAccess();
		}
		
		// Date Range
		$sCriteria_DateRange = date("F j, Y", strtotime($dCriteria_StartDate)) . ' - ' . date("F j, Y", strtotime($dCriteria_EndDate));

		// Employees
		if ($iCriteria_EmployeeId == -1 || !$iCriteria_EmployeeId) $sCriteria_Employee = "All Employees";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId = '$iCriteria_EmployeeId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Id...');
			$sCriteria_Employee = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
		}

		// Stations
		if ($iCriteria_StationId == -1 || !$iCriteria_StationId) $sCriteria_Station = "All Stations";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.StationId = '$iCriteria_StationId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Station Id...');
			$sCriteria_Station = $objDatabase->Result($varResult, 0, "S.StationName");
		}		
		// Customers
		if ($iCriteria_CustomerId == -1 || !$iCriteria_CustomerId) $sCriteria_Customer = "All Customer";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C WHERE C.CustomerId = '$iCriteria_CustomerId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Customer Id...');
			$sCriteria_Customer= $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName");
		}
		
		
		// Vendors
		if ($iCriteria_VendorId == -1 || !$iCriteria_VendorId) $sCriteria_Vendor = "All Vendors";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.VendorId = '$iCriteria_VendorId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Vendor Id...');
			$sCriteria_Vendor = $objDatabase->Result($varResult, 0, "V.VendorName");
		}
		
		// Manufacturers
		if ($iCriteria_ManufacturerId == -1 || !$iCriteria_ManufacturerId) $sCriteria_Manufacturer = "All Manufacturers";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_manufacturers AS M WHERE M.ManufacturerId = '$iCriteria_ManufacturerId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Manufacturer Id...');
			$sCriteria_Manufacturer = $objDatabase->Result($varResult, 0, "M.ManufacturerName");
		}
		// Categories
		if ($iCriteria_CategoryId == -1 || !$iCriteria_CategoryId) $sCriteria_Category = "All Categories";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_categories AS C WHERE C.CategoryId = '$iCriteria_CategoryId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Category Id...');
			$sCriteria_Category = $objDatabase->Result($varResult, 0, "C.CategoryName");
		}
		
		$sReportCriteria .= '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>
		 <fieldset><legend style="font-size:13px; font-weight:bold;">Report Criteria:</legend>
		  <table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		   <tr>
		    <!--<td width="33%">Employee:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Employee . '</span></td>-->			
			<td width="33%">Manufacturer:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Manufacturer . '</span></td>
		   <td width="33%">Category:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Category . '</span></td> 
		   </tr>
		   <tr>
		    <td width="33%">Customer:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Customer . '</span></td>
			<td width="33%">Vendor:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Vendor . '</span></td> 
		   </tr>
		   <tr>		    
		    <td>Date Range:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DateRange . '</span></td>
		   </tr>		   
		  </table>
		 </fieldset>
		 </td></tr>
		</table>';

		return(true);
	}

}

?>