<?php

class clsFMS_Reports_AccountReports extends clsFMS_Reports
{
	public $aAccountType;
	public $aAmountType;
	function __construct()
	{		
		$this->aAccountType = array('Payment', 'Receipt', 'Journal Entry');
		$this->aAmountType= array("Actual", "Increase", "Decrease");
	}
	
	function ReportFilter(&$sReportCriteria, &$sReportCriteriaSQL)
	{
		global $objGeneral;
		global $objDatabase;
		global $objEmployee;
		$iEmployeeId = $objEmployee->iEmployeeId;
		// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		//$iCriteria_SourceOfIncomeId = $objGeneral->fnGet("selSourceOfIncome");
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		$iCriteria_DonorId = $objGeneral->fnGet("selDonor");
		$iCriteria_DonorProjectId = $objGeneral->fnGet("selDonorProject");
		$iCriteria_BudgetId = $objGeneral->fnGet("selBudget");
		$iCriteria_ProjectActivityId = $objGeneral->fnGet("selProjectActivity");
		$iCriteria_ChartOfAccountsCategoryId = $objGeneral->fnGet("selChartOfAccountCategory");
		$iCriteria_ChartOfAccountsControlId = $objGeneral->fnGet("selChartOfAccountControl");		
		$iCriteria_ChartOfAccountId = $objGeneral->fnGet("selChartOfAccount");
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		//if ($iCriteria_EmployeeId == '') $iCriteria_EmployeeId = -1;
		if ($iCriteria_BankId == '' || $iCriteria_BankId == 0) $iCriteria_BankId = -1;
		if ($iCriteria_BankAccountId == '' || $iCriteria_BankAccountId == 0) $iCriteria_BankAccountId = -1;
		if ($iCriteria_DonorId == '' || $iCriteria_DonorId == 0) $iCriteria_DonorId = -1;
		if ($iCriteria_DonorProjectId == '' || $iCriteria_DonorProjectId == '0') $iCriteria_DonorProjectId = -1;
		if ($iCriteria_BudgetId == '' || $iCriteria_BudgetId == 0) $iCriteria_BudgetId = -1;
		if ($iCriteria_ProjectActivityId == '' || $iCriteria_ProjectActivityId == '0') $iCriteria_ProjectActivityId= -1;
		if ($iCriteria_ChartOfAccountsCategoryId == '') $iCriteria_ChartOfAccountsCategoryId = -1;
		if ($iCriteria_ChartOfAccountsControlId == '') $iCriteria_ChartOfAccountsControlId = -1;
		if ($iCriteria_ChartOfAccountId == '') $iCriteria_ChartOfAccountId = -1;
		
		if ($iCriteria_StationId == '') $iCriteria_StationId = -1;
		//if ($iCriteria_SourceOfIncomeId == '') $iCriteria_SourceOfIncomeId = -1;

		//$sReportCriteriaSQL = ($iCriteria_EmployeeId != -1) ? " AND E.EmployeeId='$iCriteria_EmployeeId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_BankId != -1) ? " AND BA.BankId='$iCriteria_BankId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_BankAccountId != -1) ? " AND BA.BankAccountId='$iCriteria_BankAccountId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_DonorId != -1) ? " AND GJE.DonorId='$iCriteria_DonorId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_BudgetId != -1) ? " AND GJE.BudgetId='$iCriteria_BudgetId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_DonorProjectId != -1) ? " AND GJE.DonorProjectId='$iCriteria_DonorProjectId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_ProjectActivityId != -1) ? " AND GJE.DonorProjectActivityId='$iCriteria_ProjectActivityId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_ChartOfAccountsCategoryId != -1) ? " AND CA.ChartOfAccountsCategoryId='$iCriteria_ChartOfAccountsCategoryId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_ChartOfAccountsControlId != -1) ? " AND CA.ChartOfAccountsControlId='$iCriteria_ChartOfAccountsControlId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_ChartOfAccountId != -1) ? " AND CA.ChartOfAccountsId='$iCriteria_ChartOfAccountId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_StationId != -1) ? " AND GJE.StationId='$iCriteria_StationId'" : '';
		//$sReportCriteriaSQL .= ($iCriteria_SourceOfIncomeId != -1) ? " AND CA.SourceOfIncomeId='$iCriteria_SourceOfIncomeId'" : '';
		if($iCriteria_StationId == -1)
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_employees_stations AS ES WHERE ES.EmployeeId= '$iEmployeeId'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{
					$iNewStationId = $objDatabase->Result($varResult, $i, "ES.StationId");
					if($i == 0)	$sReportCriteriaSQL .= " AND ((GJE.StationId = '$iNewStationId')";
					else
						$sReportCriteriaSQL .= " OR (GJE.StationId = '" . $iNewStationId . "')";
				}
				
				$sReportCriteriaSQL .= ")";
			}
		}	
			
		// Date Range
		$sCriteria_DateRange = date("F j, Y", strtotime($dCriteria_StartDate)) . ' - ' . date("F j, Y", strtotime($dCriteria_EndDate));

		// Employees
		if ($iCriteria_EmployeeId == -1 || !$iCriteria_EmployeeId) $sCriteria_Employee = "All Employees";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId = '$iCriteria_EmployeeId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Id...');
			$sCriteria_Employee = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
		}
		// Banks
		if ( ($iCriteria_BankId == -1) || !($iCriteria_BankId)) $sCriteria_Banks= "All Banks";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B WHERE B.BankId = '$iCriteria_BankId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Id...');
			$sCriteria_Banks = $objDatabase->Result($varResult, 0, "B.BankName");
		}

		// Bank Accounts
		if ( ($iCriteria_BankAccountId == -1) || !($iCriteria_BankAccountId)) $sCriteria_BankAccounts = "All Bank Accounts";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.BankAccountId = '$iCriteria_BankAccountId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Account Id...');
			$sCriteria_BankAccounts = $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");
		}

		// Donors
		if ( ($iCriteria_DonorId == -1) || !($iCriteria_DonorId)) $sCriteria_Donors = "All Donors";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D WHERE D.DonorId = '$iCriteria_DonorId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Donor Id...');
			$sCriteria_Donors = $objDatabase->Result($varResult, 0, "D.DonorCode");
		}

		// Donor Projects
		if ( ($iCriteria_DonorProjectId == -1) || !($iCriteria_DonorProjectId )) $sCriteria_DonorProjects = "All Donor Projects";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.DonorProjectId= '$iCriteria_DonorProjectId '");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Donor Project Id...');
			$sCriteria_DonorProjects = $objDatabase->Result($varResult, 0, "DP.ProjectCode");
		}

		// Budget
		if ( ($iCriteria_BudgetId == -1) || !($iCriteria_BudgetId)) $sCriteria_Budget = "All Budgets";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.BudgetId = '$iCriteria_BudgetId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Budget Id...');
			$sCriteria_Budget = $objDatabase->Result($varResult, 0, "B.BudgetCode");
		}
		// Project Activities
		if ( ($iCriteria_ProjectActivityId == -1) || !($iCriteria_ProjectActivityId)) $sCriteria_ProjectActivities = "All Projects Activities";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.ActivityId= '$iCriteria_ProjectActivityId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Project Activity Id...');
			$sCriteria_ProjectActivities = $objDatabase->Result($varResult, 0, "PA.ActivityCode");
		}
		
		// Chart of Accounts Categories
		if ($iCriteria_ChartOfAccountsCategoryId == -1 || !$iCriteria_ChartOfAccountsCategoryId) $sCriteria_ChartOfAccountsCategory = "All Chart of Accounts Categories";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_categories AS CAC WHERE CAC.ChartOfAccountsCategoryId= '$iCriteria_ChartOfAccountsCategoryId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Chart of Account Category Id...');
			$sCriteria_ChartOfAccountsCategory = $objDatabase->Result($varResult, 0, "CAC.CategoryCode") . ' - ' . $objDatabase->Result($varResult, 0, "CAC.CategoryName");
		}
		
		// Chart of Accounts Controls
		if ($iCriteria_ChartOfAccountsControlId == -1 || !$iCriteria_ChartOfAccountsControlId) $sCriteria_ChartOfAccountsControl = "All Chart of Accounts Controls";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts_controls AS CAC WHERE CAC.ChartOfAccountsControlId= '$iCriteria_ChartOfAccountsControlId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Chart of Account Category Id...');
			$sCriteria_ChartOfAccountsControl = $objDatabase->Result($varResult, 0, "CAC.ControlCode") . ' - ' . $objDatabase->Result($varResult, 0, "CAC.ControlName");
		}
		
		// Chart of Accounts
		if ($iCriteria_ChartOfAccountId == -1 || !$iCriteria_ChartOfAccountId) $sCriteria_ChartOfAccounts = "All Chart of Accounts";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.ChartOfAccountsId = '$iCriteria_ChartOfAccountId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Chart of Account Id...');
			$sCriteria_ChartOfAccounts = $objDatabase->Result($varResult, 0, "CA.ChartOfAccountsCode") . ' - ' . $objDatabase->Result($varResult, 0, "CA.AccountTitle");
		}
		// Stations
		if ($iCriteria_StationId == -1 || !$iCriteria_StationId) $sCriteria_Station = "All Stations";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.StationId = '$iCriteria_StationId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Station Id...');
			$sCriteria_Station = $objDatabase->Result($varResult, 0, "S.StationName");
		}
		/*
		// Source Of Income
		if ($iCriteria_SourceOfIncomeId == -1 || !$iCriteria_SourceOfIncomeId) $sCriteria_SourceOfIncome = "Any Source of Income";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_sourceofincome AS SOI WHERE SOI.SourceOfIncomeId= '$iCriteria_SourceOfIncomeId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Source of Income Id...');
			$sCriteria_SourceOfIncome = $objDatabase->Result($varResult, 0, "SOI.Title");
		}
		*/
		$sReportCriteria .= '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>
		 <fieldset><legend style="font-size:13px; font-weight:bold;">Report Criteria:</legend>
		  <table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		   <tr>
		    <td width="33%">Station:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Station . '</span></td>
			<td width="33%">Category Code:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_ChartOfAccountsCategory . '</span></td>
		   </tr>
		   <tr>	
			<td width="33%">Control Code:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_ChartOfAccountsControl . '</span></td> 
			<td width="33%">Chart Of Account:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_ChartOfAccounts . '</span></td>
		   </tr>
		   <tr>		    
			<td width="33%">Bank:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Banks . '</span></td>
			 <td width="33%">Bank Account:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_BankAccounts . '</span></td>
		   </tr>
		   <tr>			
			<td width="33%">Donor:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Donors . '</span></td>
			<td width="33%">Donor Projects:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DonorProjects. '</span></td> 
		   </tr>
		   <tr>			
			<td>Project Activities:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_ProjectActivities . '</span></td>
			<td width="33%" colspan="2">Budget:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Budget . '</span></td> 
		   </tr>		   
		   <tr>
			<td>Date Range:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DateRange . '</span></td>
		   </tr>
		  </table>
		 </fieldset>
		 </td></tr>
		</table>';

		return(true);
	}


	function ShowAccountReports($sReportName)
    {
    	global $objDatabase;
    	global $objEmployee;
    	$iEmployeeId = $objEmployee->iEmployeeId;

		/*
    	// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports[0] == 0)
    		return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Accounts Reports</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		$sChartOfAccounts = '<a ' . (($sReportName == "ChartOfAccounts") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=ChartOfAccounts"><img src="../images/accounts/iconChartOfAccounts.gif" border="0" alt="Chart of Accounts Report" title="Chart of Accounts Report" /><br />Chart of Accounts Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ChartOfAccountsReport[0] == 0)
			$sChartOfAccounts = '<img src="../images/accounts/iconChartOfAccounts_Disabled.gif" border="0" alt="Chart of Accounts Report" title="Chart of Accounts Report" /><br />Chart of Accounts Report';

		$sServices = '<a ' . (($sReportName == "Services") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=Services"><img src="../images/accounts/iconServices.gif" border="0" alt="Services Report" title="Services Report" /><br />Services Report</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountReports_ServicesReport[0] == 0)
			//$sServices = '<img src="../images/accounts/iconServices_disabled.gif" border="0" alt="Services Report" title="Services Report" /><br />Services Report';

		$sProducts = '<a ' . (($sReportName == "Products") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=Products"><img src="../images/accounts/iconProducts.gif" border="0" alt="Products Report" title="Products Report" /><br />Products Report</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountReports_ProductsReport[0] == 0)
			//$sProducts = '<img src="../images/accounts/iconProducts_disabled.gif" border="0" alt="Products Report" title="Products Report" /><br />Products Report';

		$sGeneralJournalReceipt = '<a ' . (($sReportName == "GeneralJournalReceipt") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=GeneralJournalReceipt"><img src="../images/accounts/iconGeneralJournal.gif" border="0" alt="General Journal Receipt" title="General Journal Receipt" /><br />General Journal Receipt</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralJournalReceipt[0] == 0)
			$sGeneralJournalReceipt = '<img src="../images/accounts/iconGeneralJournal_disabled.gif" border="0" alt="General Journal Receipt" title="General Journal Receipt" /><br />General Journal Receipt';
			
		$sGeneralJournal = '<a ' . (($sReportName == "GeneralJournal") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=GeneralJournal"><img src="../images/accounts/iconGeneralJournal.gif" border="0" alt="General Journal Report" title="General Journal Report" /><br />General Journal Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralJournalReport[0] == 0)
			$sGeneralJournal = '<img src="../images/accounts/iconGeneralJournal_disabled.gif" border="0" alt="General Journal Report" title="General Journal Report" /><br />General Journal Report';

		$sGeneralLedger = '<a ' . (($sReportName == "GeneralLedger") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=GeneralLedger"><img src="../images/accounts/iconGeneralLedger.gif" border="0" alt="General Ledger Report" title="General Ledger Report" /><br />General Ledger Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerReport[0] == 0)
			$sGeneralLedger = '<img src="../images/accounts/iconGeneralLedger_disabled.gif" border="0" alt="General Ledger Report" title="General Ledger Report" /><br />General Ledger Report';

		$sGeneralLedgerActivity = '<a ' . (($sReportName == "GeneralLedgerActivity") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=GeneralLedgerActivity"><img src="../images/accounts/iconGeneralLedger.gif" border="0" alt="GL Activity Report" title="GL Activity Report" /><br />GL Activity Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerReport[0] == 0)
			$sGeneralLedgerActivity = '<img src="../images/accounts/iconGeneralLedger_disabled.gif" border="0" alt="General Ledger Report" title="General Ledger Report" /><br />General Ledger Activity Report';

		$sGeneralLedgerActivityDateWise = '<a ' . (($sReportName == "GeneralLedgerActivityDateWise") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=GeneralLedgerActivityDateWise"><img src="../images/accounts/iconGeneralLedger.gif" border="0" alt="GL Activity Date Wise Report" title="GL Activity Date Wise Report" /><br />GL Activity Date Wise Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralLedgerActivityDateWiseReport[0] == 0)
			$sGeneralLedgerActivityDateWise = '<img src="../images/accounts/iconGeneralLedger_disabled.gif" border="0" alt="General Ledger Report" title="General Ledger Report" /><br />General Ledger Activity Report';

		$sBudgetList = '<a ' . (($sReportName == "BudgetList") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=BudgetList"><img src="../images/accounts/iconBudget.gif" border="0" alt="Budget List Report" title="Budget List Report" /><br />Budget List Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetListReport[0] == 0)
			$sBudgetList = '<img src="../images/accounts/iconBugdteList_disabled.gif" border="0" alt="Budget List Report" title="Budget List Report" /><br />Budget List Report';

		$sBudgetAllocation = '<a ' . (($sReportName == "BudgetAllocation") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=BudgetAllocation"><img src="../images/accounts/iconBudgetAllocation.gif" border="0" alt="Budget Allocation Report" title="Budget Allocation Report" /><br />Budget Allocation Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetAllocationReport[0] == 0)
			$sBudgetAllocation = '<img src="../images/accounts/iconBudgetAllocation_disabled.gif" border="0" alt="Budget Allocation Report" title="Budget Allocation Report" /><br />Budget Allocation Report';

		$sBudgetUtilization = '<a ' . (($sReportName == "BudgetUtilization") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=BudgetUtilization"><img src="../images/accounts/iconBudgetAllocation.gif" border="0" alt="Budget Utilization Report" title="Budget Utilization Report" /><br />Budget Utilization Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetUtilizationReport[0] == 0)
			$sBudgetUtilization = '<img src="../images/accounts/iconBudgetAllocation_disabled.gif" border="0" alt="Budget Utilization Report" title="Budget Utilization Report" /><br />Budget Utilization Report';

		$sDeletedTransactions = '<a ' . (($sReportName == "DeletedTransactions") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/accountreports.php?reportname=DeletedTransactions"><img src="../images/reports/iconDeletedTransactions.png" border="0" alt="Deleted Transactions Report" title="Deleted Transactions Report" /><br />Deleted Transactions Report</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_GeneralJournalReport[0] == 0)
		//	$sDeletedTransactions = '<img src="../images/accounts/iconGeneralJournal_disabled.gif" border="0" alt="General Journal Report" title="General Journal Report" /><br />General Journal Report';


		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
		 <tr>
		  <td width="10%" valign="top" align="center">' . $sChartOfAccounts . '</td>
		  <td width="10%" valign="top" align="center">' . $sGeneralJournal . '</td>
		  <td width="10%" valign="top" align="center">' . $sGeneralJournalReceipt . '</td>
		  <td width="10%" valign="top" align="center">' . $sGeneralLedger . '</td>
		  <td width="10%" valign="top" align="center">' . $sGeneralLedgerActivity . '</td>
		  <td width="10%" valign="top" align="center">' . $sGeneralLedgerActivityDateWise . '</td>
		  <td width="10%" valign="top" align="center">' . $sBudgetList . '</td>
		  <td width="10%" valign="top" align="center">' . $sBudgetAllocation . '</td>
		 </tr>
		 <tr>
		  <td width="10%" valign="top" align="center">' . $sBudgetUtilization . '</td>
		  <td width="10%" valign="top" align="center">' . $sDeletedTransactions . '</td>		 
		</tr>
		</table>
		<br />';

        $sReturn .= $this->ShowReportCriteria($sReportName);

		$sReturn .= '</td></tr></table></td></tr></table>';
		return($sReturn);
    }

    function ShowReportCriteria($sReportName)
    {
    	switch($sReportName)
    	{
			case "ChartOfAccounts": $sReturn = $this->ChartOfAccountsCriteria(); break;
			case "Services": $sReturn = $this->ServicesCriteria(); break;
			case "Products": $sReturn = $this->ProductsCriteria(); break;
			case "GeneralJournal": $sReturn = $this->GeneralJournalCriteria(); break;
			case "GeneralJournalReceipt": $sReturn = $this->GeneralJournalReceiptCriteria(); break;
			case "GeneralLedger": $sReturn = $this->GeneralLedgerCriteria(); break;
			case "GeneralLedgerActivity": $sReturn = $this->GeneralLedgerActivityCriteria(); break;
			case "GeneralLedgerActivityDateWise": $sReturn = $this->GeneralLedgerActivityDateWiseCriteria(); break;
			case "BudgetList": $sReturn = $this->BudgetListCriteria(); break;
			case "BudgetAllocation": $sReturn = $this->BudgetAllocationCriteria(); break;
			case "BudgetUtilization": $sReturn = $this->BudgetUtilizationCriteria(); break;
			case "DeletedTransactions": $sReturn = $this->DeletedTransactionsCriteria(); break;

    	}

    	$sReturn .= '
    	<script type="text/javascript" language="JavaScript">
		function GenerateReport(sReportName, sReportType)
		{
			var aReportCriteria;
			var sReportFilter;

			aReportCriteria = new Array();

			//iEmployee = (GetVal("selEmployee") == "") ? -1 : GetVal("selEmployee");
			iStation = (GetVal("selStation") == "") ? -1 : GetVal("selStation");
			//iSourceOfIncome = (GetVal("selSourceOfIncome") == "") ? -1 : GetVal("selSourceOfIncome");
			iChartOfAccount = (GetVal("selChartOfAccount") == "") ? -1 : GetVal("selChartOfAccount");
			iChartOfAccountCategory = (GetVal("selChartOfAccountCategory") == "") ? -1 : GetVal("selChartOfAccountCategory");
			iChartOfAccountControl = (GetVal("selChartOfAccountControl") == "") ? -1 : GetVal("selChartOfAccountControl");			
			iChartOfAccount = (GetVal("selChartOfAccount") == "") ? -1 : GetVal("selChartOfAccount");
			iDonor = (GetVal("selDonor") == "") ? -1 : GetVal("selDonor");
			iDonorProject = (GetVal("selDonorProject") == "") ? -1 : GetVal("selDonorProject");
			iBudget = (GetVal("selBudget") == "") ? -1 : GetVal("selBudget");
			iProjectActivity = (GetVal("selProjectActivity") == "") ? -1 : GetVal("selProjectActivity");
			iBank = (GetVal("selBank") == "") ? -1 : GetVal("selBank");
			iBankAccount = (GetVal("selBankAccount") == "") ? -1 : GetVal("selBankAccount");

			dDateRange_Start = (GetVal("txtDateRange_Start") == "") ? -1 : GetVal("txtDateRange_Start");
			dDateRange_End = (GetVal("txtDateRange_End") == "") ? -1 : GetVal("txtDateRange_End");
			
			ReportOption_DonorLogo = (GetVal("selReportOption_DonorLogo") == "") ? -1 : GetVal("selReportOption_DonorLogo");

			//sReportFilter = "&selEmployee=" + iEmployee;
			//sReportFilter += "&selSourceOfIncome=" + iSourceOfIncome;
			sReportFilter = "&selStation=" + iStation;			
			sReportFilter += "&selChartOfAccountCategory=" + iChartOfAccountCategory;
			sReportFilter += "&selChartOfAccountControl=" + iChartOfAccountControl;
			sReportFilter += "&selChartOfAccount=" + iChartOfAccount;			
			sReportFilter += "&selDonor=" + iDonor;
			sReportFilter += "&selDonorProject=" + iDonorProject;
			sReportFilter += "&selBudget=" + iBudget;
			sReportFilter += "&selProjectActivity=" + iProjectActivity;
			sReportFilter += "&selBank=" + iBank;
			sReportFilter += "&selBankAccount=" + iBankAccount;
			sReportFilter += "&txtDateRangeStart=" + dDateRange_Start;
			sReportFilter += "&txtDateRangeEnd=" + dDateRange_End;			
			sReportFilter += "&selReportOption_DonorLogo=" + ReportOption_DonorLogo;
			//alert(sReportFilter);

			jsOpenWindow("../reports/showreport.php?report=" + sReportName + "&reporttype=" + sReportType + sReportFilter, 800,600);
		}
		function GenerateReceiptReport(sReportName, sReportType)
		{
			var aReportCriteria;
			var sReportFilter;

			aReportCriteria = new Array();

			
			sReference = GetVal("txtReference");
			if (GetVal(\'txtReference\') == "") return(AlertFocus(\'Please enter valid Reference No\', \'txtReference\'));
			
			sReportFilter = "&txtReference=" + sReference;
			jsOpenWindow("../reports/showreport.php?report=" + sReportName + "&reporttype=" + sReportType + sReportFilter, 800,600);
		}
		</script>';

    	return($sReturn);
    }

	function ChartOfAccountsCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ChartOfAccountsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Chart of Accounts Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td>Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td>Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>
			   <!--<tr><td>Source Of Income:</td><td>' . $this->GenerateReportCriteria("SourceOfIncome", "selSourceOfIncome") . '</td></tr>-->
			   <tr><td>Category Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountCategory", "selChartOfAccountCategory") . '</td></tr>
			   <tr><td>Control Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountControl", "selChartOfAccountControl") . '</td></tr> 
			   <tr><td>Chart of Account:</td><td>' . $this->GenerateReportCriteria("ChartOfAccount", "selChartOfAccount") . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td colspan="2"><br /><hr /><span class="Tahoma16">Report Options:</span></td></tr>
			   <tr><td class="Tahoma16">Donor Logo:</td><td>' . $this->GenerateReportCriteria("ReportOption_DonorLogo", "selReportOption_DonorLogo") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'AccountsReports\', \'ChartOfAccounts\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }

	function GeneralJournalCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">General Journal Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td>Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td>Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>
			   <!--<tr><td>Source Of Income:</td><td>' . $this->GenerateReportCriteria("SourceOfIncome", "selSourceOfIncome") . '</td></tr>-->
			   <tr><td>Category Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountCategory", "selChartOfAccountCategory") . '</td></tr>
			   <tr><td>Control Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountControl", "selChartOfAccountControl") . '</td></tr> 
			   <tr><td>Chart of Account:</td><td>' . $this->GenerateReportCriteria("ChartOfAccount", "selChartOfAccount") . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'AccountsReports\', \'GeneralJournal\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function GeneralJournalReceiptCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">General Journal Receipt:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <tr><td>Reference:</td><td><input type="text" name="txtReference" id="txtReference" /></td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReceiptReport(\'AccountsReports\', \'GeneralJournalReceipt\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function GeneralLedgerCriteria()
    {
		global $objEmployee;
		/*		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
							
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">General Ledger Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Budget:</td><td>' . $this->GenerateReportCriteria("Budget", "selBudget") . '</td></tr>
			   <tr><td>Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td>Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td>Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>			   
			   <!--<tr><td>Source Of Income:</td><td>' . $this->GenerateReportCriteria("SourceOfIncome", "selSourceOfIncome") . '</td></tr>-->
			   <tr><td>Category Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountCategory", "selChartOfAccountCategory") . '</td></tr>
			   <tr><td>Control Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountControl", "selChartOfAccountControl") . '</td></tr> 
			   <tr><td>Chart of Account:</td><td>' . $this->GenerateReportCriteria("ChartOfAccount", "selChartOfAccount") . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td colspan="2"><br /><hr /><span class="Tahoma16">Report Options:</span></td></tr>
			   <tr><td class="Tahoma16">Donor Logo:</td><td>' . $this->GenerateReportCriteria("ReportOption_DonorLogo", "selReportOption_DonorLogo") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'AccountsReports\', \'GeneralLedger\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
	
	function GeneralLedgerActivityCriteria()
    {
		global $objEmployee;
		/*		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
							
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">General Ledger Activity Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Budget:</td><td>' . $this->GenerateReportCriteria("Budget", "selBudget") . '</td></tr>			  
			   <tr><td>Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td>Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td>Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>			   
			   <!--<tr><td>Source Of Income:</td><td>' . $this->GenerateReportCriteria("SourceOfIncome", "selSourceOfIncome") . '</td></tr>-->
			   <tr><td>Category Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountCategory", "selChartOfAccountCategory") . '</td></tr>
			   <tr><td>Control Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountControl", "selChartOfAccountControl") . '</td></tr> 
			   <tr><td>Chart of Account:</td><td>' . $this->GenerateReportCriteria("ChartOfAccount", "selChartOfAccount") . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'AccountsReports\', \'GeneralLedgerActivity\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function GeneralLedgerActivityDateWiseCriteria()
    {
		global $objEmployee;
		/*		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
							
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">General Ledger Activity Date Wise Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Budget:</td><td>' . $this->GenerateReportCriteria("Budget", "selBudget") . '</td></tr> 
			   <tr><td>Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td>Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td>Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>			   
			   <!--<tr><td>Source Of Income:</td><td>' . $this->GenerateReportCriteria("SourceOfIncome", "selSourceOfIncome") . '</td></tr>-->
			   <tr><td>Category Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountCategory", "selChartOfAccountCategory") . '</td></tr>
			   <tr><td>Control Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountControl", "selChartOfAccountControl") . '</td></tr> 
			   <tr><td>Chart of Account:</td><td>' . $this->GenerateReportCriteria("ChartOfAccount", "selChartOfAccount") . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'AccountsReports\', \'GeneralLedgerActivityDateWise\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function BudgetListCriteria()
    {
		global $objEmployee;
				
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] == 0)
			//return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
							
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Budget List Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
               <!--<tr><td>Donor:</td><td>' . $this->GenerateReportCriteria("Donor_Budget", "selDonor") . '</td></tr>
			   <tr><td>Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject_Budget", "selDonorProject") . '</div></td></tr>
			   <tr><td>Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>-->
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td colspan="2"><br /><hr /><span class="Tahoma16">Report Options:</span></td></tr>
			   <tr><td class="Tahoma16">Donor Logo:</td><td>' . $this->GenerateReportCriteria("ReportOption_DonorLogo", "selReportOption_DonorLogo") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'AccountsReports\', \'BudgetList\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }

	function BudgetAllocationCriteria()
    {
		global $objEmployee;

		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] == 0)
			//return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Budget Allocation Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
               <tr><td>Budget:</td><td>' . $this->GenerateReportCriteria("Budget", "selBudget") . '</td></tr>
			   <tr><td colspan="2"><br /><hr /><span class="Tahoma16">Report Options:</span></td></tr>
			   <tr><td class="Tahoma16">Donor Logo:</td><td>' . $this->GenerateReportCriteria("ReportOption_DonorLogo", "selReportOption_DonorLogo") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'AccountsReports\', \'BudgetAllocation\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
	
	function BudgetUtilizationCriteria()
    {
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetUtilizationReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Budget Utilization Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
               <tr><td>Budget:</td><td>' . $this->GenerateReportCriteria("Budget", "selBudget") . '</td></tr>
               <tr><td colspan="2"><br /><hr /><span class="Tahoma16">Report Options:</span></td></tr>
			   <tr><td class="Tahoma16">Donor Logo:</td><td>' . $this->GenerateReportCriteria("ReportOption_DonorLogo", "selReportOption_DonorLogo") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'AccountsReports\', \'BudgetUtilization\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function DeletedTransactionsCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Deleted Transactions Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td>Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td>Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>
			   <!--<tr><td>Source Of Income:</td><td>' . $this->GenerateReportCriteria("SourceOfIncome", "selSourceOfIncome") . '</td></tr>-->
			   <tr><td>Category Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountCategory", "selChartOfAccountCategory") . '</td></tr>
			   <tr><td>Control Code:</td><td>' . $this->GenerateReportCriteria("ChartOfAccountControl", "selChartOfAccountControl") . '</td></tr> 
			   <tr><td>Chart of Account:</td><td>' . $this->GenerateReportCriteria("ChartOfAccount", "selChartOfAccount") . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'AccountsReports\', \'DeletedTransactions\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }		
	
    function GenerateAccountsReports($sReportType, $bExportToExcel, &$aExcelData, $sAction = "")
    {
    	$sReport = $this->GenerateReportHeader($sAction);
    	switch ($sReportType)
    	{   
			case "ChartOfAccounts": $sReport .= $this->GenerateChartOfAccountsReport($bExportToExcel, $aExcelData); break;			
			case "GeneralJournal": $sReport .= $this->GenerateGeneralJournalReport($bExportToExcel, $aExcelData); break;
			case "GeneralJournalReceipt": $sReport .= $this->GenerateGeneralJournalReceiptlReport($bExportToExcel, $aExcelData); break;
			case "GeneralLedger": $sReport .= $this->GenerateGeneralLedgerReport($bExportToExcel, $aExcelData); break;
			case "GeneralLedgerActivity": $sReport .= $this->GenerateGeneralLedgerActivityReport($bExportToExcel, $aExcelData); break;
			case "GeneralLedgerActivityDateWise": $sReport .= $this->GenerateGeneralLedgerActivityDateWiseReport($bExportToExcel, $aExcelData); break;
			case "BudgetList": $sReport .= $this->GenerateBudgetListReport($bExportToExcel, $aExcelData); break;
			case "BudgetAllocation": $sReport .= $this->GenerateBudgetAllocationReport($bExportToExcel, $aExcelData); break;
			case "BudgetUtilization": $sReport .= $this->GenerateBudgetUtilizationReport($bExportToExcel, $aExcelData); break;
			case "DeletedTransactions": $sReport .= $this->GenerateDeletedTransactionsReport($bExportToExcel, $aExcelData); break;

    	}
    	//$sReport .= $this->GenerateReportFooter();
    	return($sReport);
    }   
	
	/* Chart Of Accounts Report*/
	function GenerateChartOfAccountsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ChartOfAccountsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
				
		// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		//$iCriteria_SourceOfIncomeId = $objGeneral->fnGet("selSourceOfIncome");
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		$iCriteria_DonorId = $objGeneral->fnGet("selDonor");
		$iCriteria_DonorProjectId = $objGeneral->fnGet("selDonorProject");
		$iCriteria_ProjectActivityId = $objGeneral->fnGet("selProjectActivity");
		$iCriteria_BudgetId = $objGeneral->fnGet("selBudget");		
		$iCriteria_ChartOfAccountsCategoryId = $objGeneral->fnGet("selChartOfAccountCategory");
		$iCriteria_ChartOfAccountsControlId = $objGeneral->fnGet("selChartOfAccountControl");		
		$iCriteria_ChartOfAccountId = $objGeneral->fnGet("selChartOfAccount");
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		//$sReportDateTimeCriteria .= " AND (CA.ChartOfAccountsAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";
		$sReportDateTimeCriteria .= " AND (GJ.GeneralJournalAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">Chart of Accounts Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);		
		$sReturn .= $sReportCriteria;
		
		$sBankTable = "";
		$iCriteria_BankId = $objGeneral->fnGet("selBank");		
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		if($iCriteria_BankId != -1)	$sBankTable = "INNER JOIN fms_banking_bankaccounts AS BA ON BA.ChartOfAccountsId = CA.ChartOfAccountsId";
		
		$sQuery = "
		SELECT
		 CACT.CategoryName AS 'CategoryName',
		 CACT.CategoryCode AS 'CategoryCode',
		 CACT.ChartOfAccountsCategoryId AS 'CategoryId',
		 IF(CACT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_chartofaccounts_categories AS CACT
		INNER JOIN fms_accounts_chartofaccounts_controls AS CAC ON CAC.ChartOfAccountsCategoryId = CACT.ChartOfAccountsCategoryId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CAC.ChartOfAccountsControlId
		INNER  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		$sBankTable
		WHERE 1=1 AND GJ.IsDeleted='0' $sReportCriteriaSQL $sReportDateTimeCriteria
		GROUP BY CACT.ChartOfAccountsCategoryId";
		
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		
		$sReturn .= '<br />
		<table border="0" bordercolor="#cecece" cellspacing="0" cellpadding="5" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" width="1%">&nbsp;</td>
		  <td align="center" width="1%">&nbsp;</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Chart of Accounts</td>
		  <td align="right" style="width:100px; font-weight:bold; font-size:12px;">Balance</td>
	     </tr>
	     </thead>';
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{				
				$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
				$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");
				$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");				
				$dBalance = $objDatabase->Result($varResult, $i, "Balance");
				
				$sReturn .= '<tr style="background-color:#d9e5ef;">
				<td style="border-top:1px solid; font-family:Tahoma, Arial; font-size:16px;" colspan="3"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selBank=' . $iCriteria_BankId . '&selBankAccount=' .$iCriteria_BankAccountId . '&selDonor=' .$iCriteria_DonorId . '&selDonorProject=' .$iCriteria_DonorProjectId . '&selProjectActivity=' .$iCriteria_ProjectActivityId . '&selChartOfAccountCategory='. $iCategoryId . '&selChartOfAccountControl='. $iCriteria_ChartOfAccountsControlId . '&selChartOfAccount='. $iCriteria_ChartOfAccountId. '\', 800,600);" />' . $sCategoryCode . ' - '.  $sCategoryName . '</a></td>
				<td align="right" style="border-top:1px solid; font-family:Tahoma, Arial; font-size:16px;">' . number_format($dBalance, 0) . '</td> 
				</tr>';
				
				// Get Controls
				$varResult2 = $objDatabase->Query("SELECT 
				 CC.ChartOfAccountsControlId AS 'ChartOfAccountsControlId',
				 CC.ControlName AS 'ControlName',		 
				 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
				FROM fms_accounts_chartofaccounts_categories AS CCAT
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
				INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
				$sBankTable
				WHERE CCAT.CategoryCode='$sCategoryCode'  AND GJ.IsDeleted='0' $sReportCriteriaSQL $sReportDateTimeCriteria
				GROUP BY CC.ChartOfAccountsControlId");	
				
				for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
				{
					$iChartOfAccountsControlId = $objDatabase->Result($varResult2, $j, "ChartOfAccountsControlId");
					$sControlName = $objDatabase->Result($varResult2, $j, "ControlName");
					$dBalance = $objDatabase->Result($varResult2, $j, "Balance");
										
					$sReturn .='<tr style="background-color:#fff8cf;">
					<td>&nbsp;</td>
					<td style="font-family:Tahoma, Arial; font-size:14px;" colspan="2"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selBank=' . $iCriteria_BankId . '&selBankAccount=' .$iCriteria_BankAccountId . '&selDonor=' .$iCriteria_DonorId . '&selDonorProject=' .$iCriteria_DonorProjectId . '&selProjectActivity=' .$iCriteria_ProjectActivityId . '&selChartOfAccountCategory='. $iCategoryId . '&selChartOfAccountControl='. $iChartOfAccountsControlId . '&selChartOfAccount='. $iCriteria_ChartOfAccountId. '\', 800,600);" />' . $sControlName . '</a></td>
					<td align="right">' . number_format($dBalance, 0) . '</td> 
					</tr>';
					
					// Chart of Accounts 
					//$varResult3 = $objDatabase->Query("SELECT 
					$varResult3 = $objDatabase->Query("SELECT 
					 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
					 CA.AccountTitle AS 'AccountTitle',
					 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
					 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
					FROM fms_accounts_chartofaccounts_categories AS CCAT
					INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
					INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
					INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
					INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
					$sBankTable
					WHERE CA.ChartOfAccountsControlId='$iChartOfAccountsControlId'  AND GJ.IsDeleted='0' $sReportCriteriaSQL $sReportDateTimeCriteria
					GROUP BY CA.ChartOfAccountsId");
					for ($k=0; $k < $objDatabase->RowsNumber($varResult3); $k++)
					{
						$dBalance = $objDatabase->Result($varResult3, $k, "Balance");
						$sChartOfAccountsCode = $objDatabase->Result($varResult3, $k, "ChartOfAccountsCode");
						$sChartOfAccountsTitle = $objDatabase->Result($varResult3, $k, "AccountTitle");
						$iChartOfAccountsId = $objDatabase->Result($varResult3, $k, "ChartOfAccountsId");
						
						$dChildBalance = 0;
						$sChildControls = "";
						/*
						//$varResult4 = $objDatabase->Query("
						$varResult4 = $objDatabase->Query("
						SELECT 
						 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
						 CA.AccountTitle AS 'AccountTitle',
						 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
						 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
						FROM fms_accounts_chartofaccounts_categories AS CCAT
						INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsCategoryId = CCAT.ChartOfAccountsCategoryId
						INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsControlId = CC.ChartOfAccountsControlId
						INNER  JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = CA.ChartOfAccountsId
						INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
						$sBankTable
						WHERE CA.ParentChartOfAccountsId='$iChartOfAccountsId' $sReportCriteriaSQL $sReportDateTimeCriteria
						GROUP BY CA.ChartOfAccountsId");
						if ($objDatabase->RowsNumber($varResult4) > 0)
						{
							for ($l=0; $l < $objDatabase->RowsNumber($varResult4); $l++)
							{
								$sChildChartOfAccountsCode = $objDatabase->Result($varResult4, $l, "ChartOfAccountsCode");
								$sChildChartOfAccountTitle = $objDatabase->Result($varResult4, $l, "AccountTitle");
								$iChildChartOfAccountsId = $objDatabase->Result($varResult4, $l, "ChartOfAccountsId");
								$dTempChildBalance = $objDatabase->Result($varResult4, $l, "Balance");
								
								$sChildControls .='<tr>
								 <td>&nbsp;</td>
								 <td>&nbsp;</td>
								 <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selBank=' . $iCriteria_BankId . '&selBankAccount=' .$iCriteria_BankAccountId . '&selDonor=' .$iCriteria_DonorId . '&selDonorProject=' .$iCriteria_DonorProjectId . '&selProjectActivity=' .$iCriteria_ProjectActivityId . '&selChartOfAccountCategory='. $iCategoryId . '&selChartOfAccountControl='. $iCriteria_ChartOfAccountsControlId . '&selChartOfAccount='. $iChildChartOfAccountsId. '\', 800,600);" />' . $sChildChartOfAccountsCode . ' - ' . $sChildChartOfAccountTitle . '</a></td>
								 <td align="right">' . number_format($dTempChildBalance, 0) . '</td>
								</tr>';
								
								$dChildBalance += $dTempChildBalance;
							}
						}
						
						*/
						//echo '<Br># '.$dChildBalance;
						$sReturn .='<tr>
						 <td>&nbsp;</td>
						 <td>&nbsp;</td>
						 <td align="left"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selBank=' . $iCriteria_BankId . '&selBankAccount=' .$iCriteria_BankAccountId . '&selDonor=' .$iCriteria_DonorId . '&selDonorProject=' .$iCriteria_DonorProjectId . '&selProjectActivity=' .$iCriteria_ProjectActivityId . '&selChartOfAccountCategory='. $iCategoryId . '&selChartOfAccountControl='. $iCriteria_ChartOfAccountsControlId . '&selChartOfAccount='. $iChartOfAccountsId . '\', 800,600);" />' . $sChartOfAccountsCode . ' - ' . $sChartOfAccountsTitle . '</a></td>
						 <td align="right">' . number_format($dBalance + $dChildBalance, 0) . '</td>
						</tr>' . $sChildControls;
					}
					
				}
			}
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma;font-size:16px;" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
	/* Services Report */
	function GenerateServicesReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ServicesReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (S.ServiceAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">Services Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_accounts_services AS S		
		INNER JOIN organization_employees AS E ON E.EmployeeId = S.ServiceAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Service Name</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Service Code</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Service Frequency</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Price</td>		  
	     </tr>
	     </thead>';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iServiceId = $objDatabase->Result($varResult, $i, "S.ServiceId");
				$sServiceName = $objDatabase->Result($varResult, $i, "S.ServiceName");
				$sServiceCode = $objDatabase->Result($varResult, $i, "S.ServiceCode");
				$sServiceFrequency = $objDatabase->Result($varResult, $i, "S.ServiceFrequency");
				$dServicePrice = $objDatabase->Result($varResult, $i, "S.ServicePrice");
				$sServicePrice = number_format($dServicePrice, 0);
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sServiceName . '</td>
				 <td align="left">' . $sServiceCode . '</td>
				 <td align="left">' . $sServiceFrequency . '</td>
				 <td align="right">' .  $objEmployee->aSystemSettings['CurrencySign'] . $sServicePrice . '</td>				 				 
				</tr>';
			}
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
	/* Products Report */
	function GenerateProductsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (P.ProductAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">Products Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_accounts_products AS P		
		INNER JOIN organization_employees AS E ON E.EmployeeId = P.ProductAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Employee Name </td> 
          <td align="left" style="font-weight:bold; font-size:12px;">Product Name </td>
          <td align="left" style="font-weight:bold; font-size:12px;">Product Code</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Description</td>
	     </tr>
	     </thead>';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iProductId = $objDatabase->Result($varResult, $i, "P.ProductId");
				$sProductName = $objDatabase->Result($varResult, $i, "P.ProductName");
				$sProductCode = $objDatabase->Result($varResult, $i, "P.ProductCode");
				$sDescription = $objDatabase->Result($varResult, $i, "P.Description");
				$sEmployeeName = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sEmployeeName . '</td>
				 <td align="left">' . $sProductName . '</td>
				 <td align="left">' . $sProductCode . '</td>
				 <td align="left">' . $sDescription . '</td>				 
				</tr>';
			}
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}

	/* General Journal Report */
	function GenerateGeneralJournalReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankAccountsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		//$iCriteria_SourceOfIncomeId = $objGeneral->fnGet("selSourceOfIncome");
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		$iCriteria_DonorId = $objGeneral->fnGet("selDonor");
		$iCriteria_DonorProjectId = $objGeneral->fnGet("selDonorProject");
		$iCriteria_ProjectActivityId = $objGeneral->fnGet("selProjectActivity");
		$iCriteria_BudgetId = $objGeneral->fnGet("selBudget");		
		$iCriteria_ChartOfAccountsCategoryId = $objGeneral->fnGet("selChartOfAccountCategory");
		$iCriteria_ChartOfAccountsControlId = $objGeneral->fnGet("selChartOfAccountControl");		
		$iCriteria_ChartOfAccountId = $objGeneral->fnGet("selChartOfAccount");
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">General Journal Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sBankTable = "";
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		if($iCriteria_BankId != -1 && $iCriteria_BankId != "")	$sBankTable = "INNER JOIN fms_banking_bankaccounts AS BA ON BA.ChartOfAccountsId = CA.ChartOfAccountsId";
		
		//AND CA.ChartOfAccountsCategoryId != '5' 
		$sQuery = "
		SELECT
		 GJ.GeneralJournalId AS 'Id',
		 GJ.Reference AS 'Reference'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
		$sBankTable
		WHERE 1=1 AND GJ.IsDeleted='0' $sReportDateTimeCriteria $sReportCriteriaSQL
        GROUP BY GJ.GeneralJournalId";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">Id</td>
		  <td align="center" style="font-weight:bold; font-size:12px;">Reference</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Account Code</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Account Title</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Detail</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Check No#</td>
		  <td align="right" style="font-weight:bold; font-size:12px;">Debit</td>
          <td align="right" style="font-weight:bold; font-size:12px;">Credit</td>
	     </tr>
	     </thead>';
		 $dTotalDebit = 0;
		 $dTotalCredit = 0;
		 $iTempId = 0;

	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iId = $objDatabase->Result($varResult, $i, "Id");
				$sReference = $objDatabase->Result($varResult, $i, "Reference");
				//AND CA.ChartOfAccountsCategoryId != '5'
                $sQuery = "
        		SELECT
        		 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
        		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
        		 CA.AccountTitle AS 'AccountTitle',
                 GJE.Detail AS 'Detail',
                 GJ.CheckNumber AS 'CheckNumber',
                 GJE.Debit AS 'Debit',
        		 GJE.Credit AS 'Credit'
        		FROM fms_accounts_generaljournal AS GJ
        		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
        		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
        		WHERE 1=1  AND GJ.IsDeleted='0' AND GJE.GeneralJournalId = '$iId'";

                $varResult2 = $objDatabase->Query($sQuery);
                for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
			    {
                    $iChartOfAccountsId = $objDatabase->Result($varResult2, $k, "ChartOfAccountsId");
					$sAccountTitle = $objDatabase->Result($varResult2, $k, "AccountTitle");
    				$sChartOfAccountsCode = $objDatabase->Result($varResult2, $k, "ChartOfAccountsCode");
                    $sDetail = $objDatabase->Result($varResult2, $k, "Detail");
                    $sCheckNumber = $objDatabase->Result($varResult2, $k, "CheckNumber");
                    if($sCheckNumber == 0) $sCheckNumber = "";
    				$dDebit = $objDatabase->Result($varResult2, $k, "Debit");
    				$sDebit = number_format($dDebit, 0);
    				$dCredit = $objDatabase->Result($varResult2, $k, "Credit");
    				$sCredit = number_format($dCredit, 0);
    				$dTotalCredit += $dCredit;
    				$dTotalDebit += $dDebit;

                    $iTransactionId = $iId;
    				if($iTempId == $iId)
    				{
    					$iTransactionId = '';
    					$sReference = '';
    				}
    				$iTempId = $iId;

                    $sReturn .= '
    				<tr>
    				 <td align="center">' . $iTransactionId . '&nbsp;</td>
    				 <td align="center">' . $sReference . '&nbsp;</td>
    				 <td align="left"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selBank=' . $iCriteria_BankId . '&selBankAccount=' .$iCriteria_BankAccountId . '&selDonor=' .$iCriteria_DonorId . '&selDonorProject=' .$iCriteria_DonorProjectId . '&selProjectActivity=' .$iCriteria_ProjectActivityId . '&selChartOfAccountCategory='. $iCategoryId . '&selChartOfAccountControl='. $iCriteria_ChartOfAccountsControlId . '&selChartOfAccount='. $iChartOfAccountsId . '&txtDateRangeStart='. $dCriteria_StartDate . '&txtDateRangeEnd='. $dCriteria_EndDate . '\', 800,600);" />' . $sChartOfAccountsCode . '&nbsp;</td>
    				 <td align="left">' . $sAccountTitle . '&nbsp;</td>
                     <td align="left">' . $sDetail . '&nbsp;</td>
                     <td align="left">' . $sCheckNumber . '&nbsp;</td>
    				 <td align="right">' . $sDebit . '&nbsp;</td>
    				 <td align="right">' . $sCredit . '&nbsp;</td>
    				</tr>';
                    }

			}

			$sReturn .= '
			<tr>
			 <td style="font-weight:bold;font-size:14px;" align="right" colspan="6">Total</td>
			 <td style="font-weight:bold;font-size:14px;" align="right">' . number_format($dTotalDebit, 0) . '&nbsp;</td>
			 <td style="font-weight:bold;font-size:14px;" align="right">' . number_format($dTotalCredit, 0). '&nbsp;</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma;font-size:16px;"  align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}

	/* General Ledger Report */
	function GenerateGeneralLedgerReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		/*
		$dCurrentDate = $objGeneral->fnGet("txtAsOfDate");
		if($dCurrentDate == "")	$dCurrentDate = date("Y-m-d");
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		if($dCriteria_StartDate != "" && $dCriteria_EndDate != "")
			$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";
		else
			$sReportDateTimeCriteria .= " AND (GJ.TransactionDate <= '$dCurrentDate')";	
		*/

	// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		//$iCriteria_SourceOfIncomeId = $objGeneral->fnGet("selSourceOfIncome");
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		$iCriteria_DonorId = $objGeneral->fnGet("selDonor");
		$iCriteria_DonorProjectId = $objGeneral->fnGet("selDonorProject");
		$iCriteria_ProjectActivityId = $objGeneral->fnGet("selProjectActivity");
		$iCriteria_BudgetId = $objGeneral->fnGet("selBudget");		
		$iCriteria_ChartOfAccountsCategoryId = $objGeneral->fnGet("selChartOfAccountCategory");
		$iCriteria_ChartOfAccountsControlId = $objGeneral->fnGet("selChartOfAccountControl");		
		$iCriteria_ChartOfAccountId = $objGeneral->fnGet("selChartOfAccount");
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		//$sReportDateTimeCriteria .= " AND (CA.ChartOfAccountsAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";
		//$sReportDateTimeCriteria .= " AND (GJ.GeneralJournalAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";
		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";



		$sReturn = '<div align="center"><span class="ReportTitle">General Ledger Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sBankTable = "";
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
				
		if($iCriteria_BankId != -1 && $iCriteria_BankId != "")
			$sBankTable = "INNER JOIN fms_banking_bankaccounts AS BA ON BA.ChartOfAccountsId = CA.ChartOfAccountsId";
					
		//AND CA.ChartOfAccountsCategoryId !='5' 
		// ChartOfAccountsCategoryId 5 = Equity
		$sQuery = "
		SELECT
		 CA.ChartOfAccountsCategoryId AS 'ChartOfAccountsCategoryId',
		 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 CA.AccountTitle AS 'AccountTitle',
		 CCAT.DebitIncrease AS 'DebitIncrease'		 
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
		$sBankTable
		WHERE 1=1 AND GJ.IsDeleted='0' $sReportCriteriaSQL $sReportDateTimeCriteria
		GROUP BY CA.ChartOfAccountsId
		ORDER BY GJ.TransactionDate";		
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{				
				$iChartOfAccountsCategoryId = $objDatabase->Result($varResult, $i, "ChartOfAccountsCategoryId");
				$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
				$iDebitIncrease = $objDatabase->Result($varResult, $i, "DebitIncrease");
				
				$dTotal_Credit = 0;
				$dTotal_Debit = 0;
				$dBalance = 0;
				
				if ($i > 0)
					$sReturn .= '<br /><br /><div style="page-break-after:always"></div>';

				// Get Closing Balance
				$varResult2 = $objDatabase->Query("
				SELECT
					IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
				FROM fms_accounts_generaljournal AS GJ
				INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
				INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
				$sBankTable
				WHERE GJE.ChartOfAccountsId= '$iChartOfAccountsId' AND GJ.IsDeleted='0' $sReportCriteriaSQL AND GJ.TransactionDate <'$dCriteria_StartDate' ORDER BY GJ.TransactionDate");
				if ($objDatabase->RowsNumber($varResult2) > 0)
				{
					$dBalance = $objDatabase->Result($varResult2, 0, "Balance");
				}

				$sReturn .= '<br />
				<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
				<tr>
				 <td style="font-family:Tahoma, Arial; font-size:22px;" align="left" colspan="6">General Ledger - ' . $sAccountTitle . '</td>
				</tr>
				<tr>
				 <td style="font-family:Tahoma, Arial; font-size:22px;" align="left" colspan="6">Opening Balance :' . number_format($dBalance, 2) . '</td>
				</tr>
				<tr>
				 <td align="center" style="border-bottom:1px solid; width:25px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">S#</td>
				 <td align="left" style="border-bottom:1px solid; width:120px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Date</td>
				 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Ref No.</td>
				 <td align="left" style="border-bottom:1px solid; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Description</td>
                 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Check No#</td>
				 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Debit</td>
				 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Credit</td>
				 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Balance</td>
				</tr>';

				// ChartOfAccountsCategoryId  1 = Assets, 2= Liabilities, 3= Income, 4= Expenses, 5=Equity
				if($iChartOfAccountsCategoryId == 1 || $iChartOfAccountsCategoryId == 4)
					$sBalanceColumn = "SUM((GJE.Debit - GJE.Credit)) AS 'Balance'";
				else
					$sBalanceColumn = "SUM((GJE.Credit - GJE.Debit)) AS 'Balance'";

				// Show Transactions
				$varResult2 = $objDatabase->Query("
				SELECT
					GJ.GeneralJournalId AS 'GeneralJournalId',
					GJ.TransactionDate AS 'TransactionDate',
					GJ.Reference AS 'Reference',
                    GJ.CheckNumber AS 'CheckNumber',
					GJ.EntryType, 
					GJ.PaymentType, 
					GJ.ReceiptType,
					GJ.Series,
					GJE.Detail AS 'Detail',
					GJE.Debit AS 'Debit',
					GJE.Credit AS 'Credit'
				FROM fms_accounts_generaljournal AS GJ
				INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CACT ON CACT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
				INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
				$sBankTable
				WHERE GJ.IsDeleted='0' AND GJE.ChartOfAccountsId= '$iChartOfAccountsId' $sReportCriteriaSQL $sReportDateTimeCriteria
				ORDER BY GJ.TransactionDate, GJ.EntryType, GJ.PaymentType, GJ.ReceiptType, GJ.Series");
				//ORDER BY GJ.TransactionDate, GJ.GeneralJournalId");
				
				for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$iGeneralJournalId = $objDatabase->Result($varResult2, $k, "GeneralJournalId");
					$sReference = $objDatabase->Result($varResult2, $k, "Reference");
					$dTransactionDate = $objDatabase->Result($varResult2, $k, "TransactionDate");
					$sDetail = $objDatabase->Result($varResult2, $k, "Detail");
                    $sCheckNumber = $objDatabase->Result($varResult2, $k, "CheckNumber");
                    if($sCheckNumber == 0) $sCheckNumber = '';
					$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
					$dDebit = $objDatabase->Result($varResult2, $k, "Debit");
					$dCredit = $objDatabase->Result($varResult2, $k, "Credit");
					//$dBalance += $objDatabase->Result($varResult2, $k, "Balance");
					if($iDebitIncrease == 1)
					{
						 $dTransBalance = $dDebit - $dCredit;
					}
					else
					{
						$dTransBalance = $dCredit - $dDebit;
					}

					$dBalance += $dTransBalance;

					$dTotal_Debit += $dDebit;
					$dTotal_Credit += $dCredit;

					$sDebit = number_format($dDebit, 0);
					$sCredit = number_format($dCredit, 0);

					if ($dDebit == 0) $sDebit = '';
					if ($dCredit == 0) $sCredit = '';
					if($iDebitIncrease == '1') $dClosingBalance = $dTotal_Debit - $dTotal_Credit;
					else $dClosingBalance = $dTotal_Credit - $dTotal_Debit;
					
					//$sReceipt = '<td class="GridTD" align="center"><img src="../images/icons/iconPrint2.gif" border="0" alt="Receipt" title="Receipt"></a></td>';
					
					$sReturn .= '
					<tr>
					 <td valign="top" align="center" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . ($k+1) . '&nbsp;</td>
					 <td valign="top" align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sTransactionDate . '&nbsp;</td>
					 <td valign="top" align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;"><a style="font-family:Tahoma, Arial; font-size:12px; text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&id=' . $iGeneralJournalId. '&txtReference=' . $sReference . '\', 800,600);">' . $sReference . '&nbsp;</a></td>
					 <td valign="top" align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;"><a style="font-family:Tahoma, Arial; font-size:12px; text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&id=' . $iGeneralJournalId. '&txtReference=' . $sReference . '\', 800,600);">' . $sDetail . '&nbsp;</a></td>
                     <td valign="top" align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sCheckNumber . '&nbsp;</td>
					 <td valign="top" align="right" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sDebit. '&nbsp;</td>
					 <td valign="top" align="right" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sCredit . '&nbsp;</td>
					 <td valign="top" align="right" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . number_format($dBalance, 0) . '&nbsp;</td>
					</tr>';
				}

				$sReturn .= '<tr>
				 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" colspan="5" align="right">Total</td>
				 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Debit, 0) . '</td>
				 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Credit, 0) . '</td>
				 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format(($dBalance), 0) . '</td>
				</tr>';

				$sReturn .= '</table>';
			}
	    }
	    else
	    {
				if($iCriteria_ChartOfAccountsCategoryId != '-1') $sChartOfAccountsCategory =  "AND CCAT.ChartOfAccountsCategoryId='$iCriteria_ChartOfAccountsCategoryId'"; 
				if($iCriteria_ChartOfAccountsControlId != '-1') $sChartOfAccountsControl =  "AND CC.ChartOfAccountsControlId='$iCriteria_ChartOfAccountsControlId'"; 
				if($iCriteria_ChartOfAccountId != '-1') $sChartOfAccount =  "AND CA.ChartOfAccountsId='$iCriteria_ChartOfAccountId'"; 
				
				//die($sReportDateTimeCriteria);
				//AND GJE.DonorId='4' AND GJE.DonorProjectId='46' AND CA.ChartOfAccountsCategoryId='1' AND CA.ChartOfAccountsControlId='39' AND CA.ChartOfAccountsId='331'
				$sQuery = "
				SELECT
				 CA.ChartOfAccountsCategoryId AS 'ChartOfAccountsCategoryId',
				 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
				 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
				 CA.AccountTitle AS 'AccountTitle'				 		 
				FROM fms_accounts_chartofaccounts AS CA				
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId= CA.ChartOfAccountsControlId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId= CC.ChartOfAccountsCategoryId
				$sBankTable
				WHERE 1=1 $sChartOfAccountsCategory  $sChartOfAccountsControl $sChartOfAccount";		
				//die($sQuery);
				$varResult = $objDatabase->Query($sQuery);
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{				
					$iChartOfAccountsCategoryId = $objDatabase->Result($varResult, $i, "ChartOfAccountsCategoryId");
					$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");
					$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
					$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
					//$iDebitIncrease = $objDatabase->Result($varResult, $i, "DebitIncrease");				
					
					// Get Closing Balance
					$varResult2 = $objDatabase->Query("
					SELECT
						IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
					FROM fms_accounts_generaljournal AS GJ
					INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
					INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
					INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
					INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
					$sBankTable
					WHERE GJE.ChartOfAccountsId= '$iChartOfAccountsId' AND GJ.IsDeleted='0' $sReportCriteriaSQL AND GJ.TransactionDate <'$dCriteria_StartDate' ORDER BY GJ.TransactionDate");
					if ($objDatabase->RowsNumber($varResult2) > 0)
					{
						$dBalance = $objDatabase->Result($varResult2, 0, "Balance");
						//if($dBalance > 0 )
						//{
							$sReturn .= '<br />
							<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
							<tr>
							 <td style="font-family:Tahoma, Arial; font-size:22px;" align="left" colspan="6">General Ledger - ' . $sAccountTitle . '</td>
							</tr>
							<tr>
							 <td style="font-family:Tahoma, Arial; font-size:22px;" align="left" colspan="6">Opening Balance :' . number_format($dBalance, 2) . '</td>
							</tr>
							<tr>
							 <td align="center" style="border-bottom:1px solid; width:25px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">S#</td>
							 <td align="left" style="border-bottom:1px solid; width:120px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Date</td>
							 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Ref No.</td>
							 <td align="left" style="border-bottom:1px solid; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Description</td>
							 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Check No#</td>
							 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Debit</td>
							 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Credit</td>
							 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Balance</td>
							</tr>';
							
							$sReturn .= '<tr>
							 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" colspan="9" align="center">No records found...</td></tr>';

							$sReturn .= '</table>';
						//}	
					}
				
				}			
		}	
			
			//$sReturn .= '<div align="center" style="font-family:Tahoma;font-size:16px;">No records found...</div>';
		return($sReturn);
	}

	/* General Ledger Activity Report */
	function GenerateGeneralLedgerActivityReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">General Ledger Activity Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
		$sBankTable = "";
		$iCriteria_BankId = $objGeneral->fnGet("selBank");		
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		if($iCriteria_BankId != -1)
			$sBankTable = "INNER JOIN fms_banking_bankaccounts AS BA ON BA.ChartOfAccountsId = CA.ChartOfAccountsId";

		$sQuery = "
		SELECT
		 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 CACT.DebitIncrease AS 'DebitIncrease',
		 CA.AccountTitle AS 'AccountTitle',
		 GJ.GeneralJournalId AS 'No',
		 GJ.Reference AS 'Reference',
		 GJ.EntryType AS 'Type',		 
		 GJ.TransactionDate AS 'Date',
		 GJE.Detail AS 'Detail',
		 GJE.Debit AS 'Debit',
		 GJE.Credit AS 'Credit'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CACT ON CACT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		$sBankTable
		WHERE 1=1 AND GJ.IsDeleted='0' $sReportDateTimeCriteria $sReportCriteriaSQL
		ORDER BY CA.ChartOfAccountsId";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br /><table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">';

		$dTotal_Credit = 0;
		$dTotal_Debit = 0;
		$dBalance = 0;
		$iTemp_ChartOfAccountsId = 0;
			
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				$iDebitIncrease = $objDatabase->Result($varResult, $i, "DebitIncrease");
				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");				
				$dOpeningBalance = 0;
				// Check for Account Head change				
				if(($iTemp_ChartOfAccountsId != $iChartOfAccountsId) && ($i == 0))
				{
					// Get Closing Balance				
					$varResult2 = $objDatabase->Query("
					SELECT
						IF(CACT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
					FROM fms_accounts_generaljournal AS GJ
					INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
					INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
					INNER JOIN fms_accounts_chartofaccounts_categories AS CACT ON CACT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
					$sBankTable
					WHERE GJE.ChartOfAccountsId= '$iChartOfAccountsId'  AND GJ.IsDeleted='0' $sReportCriteriaSQL AND GJ.TransactionDate <'$dCriteria_StartDate'");
					if ($objDatabase->RowsNumber($varResult2) > 0)
					{
						$dOpeningBalance = $objDatabase->Result($varResult2, 0, "Balance");
					}
					$sOpeningBalance = number_format($dOpeningBalance, 0);
					$dBalance = $dOpeningBalance;
					
					$sReturn .='					
					<tr style="background-color:#e1e1e1;">
					 <td align="left" style="font-weight:bold; font-size:12px;">Account Code</td>
					 <td><strong>' . $sChartOfAccountsCode . '</strong></td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Account Title</td>
					 <td><strong>' . $sAccountTitle . '</strong></td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Opening Balance</td>
					 <td colspan="2" align="right" style="font-weight:bold; font-size:12px;">' . $sOpeningBalance . '</td>
					</tr>					
					<tr style="background-color:#e1e1e1;">
					 <td align="center" style="font-weight:bold; font-size:12px;">No. </td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Type</td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Date</td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Ref</td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Detail</td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Debit</td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Credit</td>
					</tr>';
				}
								
				else if(($iTemp_ChartOfAccountsId != $iChartOfAccountsId) && ($i > 0))
				{
					// Get Closing Balance				
					$varResult2 = $objDatabase->Query("
					SELECT
						IF(CACT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
					FROM fms_accounts_generaljournal AS GJ
					INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
					INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
					INNER JOIN fms_accounts_chartofaccounts_categories AS CACT ON CACT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
					$sBankTable
					WHERE GJE.ChartOfAccountsId= '$iChartOfAccountsId'  AND GJ.IsDeleted='0' $sReportCriteriaSQL AND GJ.TransactionDate <'$dCriteria_StartDate'");
					if ($objDatabase->RowsNumber($varResult2) > 0)
					{
						$dOpeningBalance = $objDatabase->Result($varResult2, 0, "Balance");
					}
					$sOpeningBalance = number_format($dOpeningBalance, 0);
				
					$sReturn .='
					<tr class="GridReport">
					 <td colspan="5" align="left" style="font-weight:bold; font-size:12px;">Current Closing Balance : ' .  $objEmployee->aSystemSettings['CurrencySign']. ' ' . $sClosingBalance .'</td>
					 <td align="right"><strong>' . $objEmployee->aSystemSettings['CurrencySign']. ' ' . number_format($dTotal_Debit, 0) . '</strong></td>
					 <td align="right"><strong>' . $objEmployee->aSystemSettings['CurrencySign']. ' ' . number_format($dTotal_Credit, 0) . '</strong></td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr style="background-color:#e1e1e1;">
					 <td align="left" style="font-weight:bold; font-size:12px;">Account Code</td>
					 <td><strong>' . $sChartOfAccountsCode . '</strong></td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Account Title</td>
					 <td><strong>' . $sAccountTitle . '</strong></td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Opening Balance</td>
					 <td colspan="2" align="right" style="font-weight:bold; font-size:12px;">' . $sOpeningBalance . '</td>
					</tr>					
					<tr>
					 <td align="center" style="font-weight:bold; font-size:12px;">No. </td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Type</td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Date</td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Ref</td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Detail</td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Debit</td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Credit</td>
					</tr>';
					$dTotal_Debit = 0;
					$dTotal_Credit = 0;
					$dBalance = $dOpeningBalance;
				}
				// End here				
				$iNo = $objDatabase->Result($varResult, $i, "No");
				$sReference = $objDatabase->Result($varResult, $i, "Reference");
				$iType = $objDatabase->Result($varResult, $i, "Type");
				$sType = $this->aAccountType[$iType];
				
				$dDate = $objDatabase->Result($varResult, $i, "Date");
				$sDate = date("F j, Y", strtotime($dDate));				
				$sDetail = $objDatabase->Result($varResult, $i, "Detail");
				
				$dDebit = $objDatabase->Result($varResult, $i, "Debit");
				$dCredit = $objDatabase->Result($varResult, $i, "Credit");
				//$dClosingBalance = $objDatabase->Result($varResult, $i, "ClosingBalance");
				
				$sCredit = number_format($dCredit, 0);
				$sDebit = number_format($dDebit, 0);
				$dTotal_Credit += $dCredit;
				$dTotal_Debit += $dDebit;
				
				if($iDebitIncrease == 1)
				{
					if($dDebit > 0)
					 $dBalance = $dDebit + $dBalance;
					else
						$dBalance = $dBalance - $dCredit;
				}
				else 
				{
					if($dCredit > 0)
					 $dBalance = $dCredit + $dBalance;
					else
						$dBalance = $dBalance - $dDebit;						
				}
				
				//$dBalance += $dTransBalance;				
				//if($iDebitIncrease == '1') $dClosingBalance = $dTotal_Debit - $dTotal_Credit;
				//else $dClosingBalance = $dTotal_Credit - $dTotal_Debit;	
				$sClosingBalance = number_format($dBalance, 2);
				
				$sReturn .= '
				<tr>
				 <td align="center">' . $iNo . '</td>
				 <td align="left">' . $sType . '</td>
				 <td align="left">' . $sDate . '</td>
				 <td align="left"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&id=' . $iNo. '&txtReference=' . $sReference . '\', 800,600);">' . $sReference . '</a></td> 
				 <td align="left"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&id=' . $iNo. '&txtReference=' . $sReference . '\', 800,600);">' . $sDetail . '</a></td> 
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign']. ' ' . $sDebit . '</td>
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign']. ' ' . $sCredit . '</td>
				</tr>';
				
				$iTemp_ChartOfAccountsId = $iChartOfAccountsId;
			}
			
			$sReturn .= '
			<tr class="GridReport">
			 <td colspan="5" align="left" style="font-weight:bold; font-size:12px;">Current Closing Balance : ' .  $objEmployee->aSystemSettings['CurrencySign']. ' ' . $sClosingBalance .'</td>
			 <td align="right"><strong>' . $objEmployee->aSystemSettings['CurrencySign']. ' ' . number_format($dTotal_Debit, 0) . '</strong></td>
			 <td align="right"><strong>' . $objEmployee->aSystemSettings['CurrencySign']. ' ' . number_format($dTotal_Credit, 0) . '</strong></td>
		    </tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center" style="font-family:Tahoma; Font-size:16px;"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
	/* General Ledger Activity Date Wise Report */
	function GenerateGeneralLedgerActivityDateWiseReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		//$iCriteria_SourceOfIncomeId = $objGeneral->fnGet("selSourceOfIncome");
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		$iCriteria_DonorId = $objGeneral->fnGet("selDonor");
		$iCriteria_DonorProjectId = $objGeneral->fnGet("selDonorProject");
		$iCriteria_ProjectActivityId = $objGeneral->fnGet("selProjectActivity");
		$iCriteria_BudgetId = $objGeneral->fnGet("selBudget");		
		$iCriteria_ChartOfAccountsCategoryId = $objGeneral->fnGet("selChartOfAccountCategory");
		$iCriteria_ChartOfAccountsControlId = $objGeneral->fnGet("selChartOfAccountControl");		
		$iCriteria_ChartOfAccountId = $objGeneral->fnGet("selChartOfAccount");
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">General Ledger Activity Date Wise Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
		$sBankTable = "";
		$iCriteria_BankId = $objGeneral->fnGet("selBank");		
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		if($iCriteria_BankId != -1)
		{
			$sBankTable = "INNER JOIN fms_banking_bankaccounts AS BA ON BA.ChartOfAccountsId = CA.ChartOfAccountsId";
		}
		//AND CA.ChartOfAccountsCategoryId !='5'

		$sQuery = "
		SELECT
		 DATE_FORMAT(GJ.TransactionDate, '%d %M %Y') AS Date,
		 GJ.TransactionDate AS 'TransactionDate',
		 SUM(GJE.Debit) AS 'Debit',
		 SUM(GJE.Credit) AS 'Credit'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
		$sBankTable
		WHERE 1=1   AND GJ.IsDeleted='0' $sReportDateTimeCriteria $sReportCriteriaSQL
		GROUP BY Date";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="5" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="left" style="font-weight:bold; font-size:12px;">Date</td>
		  <td align="right" style="font-weight:bold; font-size:12px;">Debit</td>
          <td align="right" style="font-weight:bold; font-size:12px;">Credit</td>
	     </tr>
	     </thead>';

		$dTotal_Credit = 0;
		$dTotal_Debit = 0;
		$dBalance = 0;

	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				/*
				$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
				*/

				$dDate = $objDatabase->Result($varResult, $i, "Date");
				$dTransactionDate = $objDatabase->Result($varResult, $i, "TransactionDate");				
				$sDate = date("F j, Y", strtotime($dDate));
				
				$dDebit = $objDatabase->Result($varResult, $i, "Debit");
				$sDebit = number_format($dDebit, 0);
				$dCredit = $objDatabase->Result($varResult, $i, "Credit");
				$sCredit = number_format($dCredit, 0);

				$dTotal_Credit += $dCredit;
				$dTotal_Debit += $dDebit;

				$sReturn .= '
				<tr>
				 <td align="left"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selBank=' . $iCriteria_BankId . '&selBankAccount=' .$iCriteria_BankAccountId . '&selDonor=' .$iCriteria_DonorId . '&selDonorProject=' .$iCriteria_DonorProjectId . '&selProjectActivity=' .$iCriteria_ProjectActivityId . '&selChartOfAccountCategory='. $iCriteria_ChartOfAccountsCategoryId . '&selChartOfAccountControl='. $iCriteria_ChartOfAccountsControlId . '&selChartOfAccount='. $iCriteria_ChartOfAccountId. '&txtDateRangeStart='. $dTransactionDate . '&txtDateRangeEnd='. $dTransactionDate .  '\', 800,600);" />' . $sDate . '</td>
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign']. ' ' . $sDebit . '</td>
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . $sCredit . '</td>				 
				</tr>';
			}
						
			$sReturn .= '
			<tr class="GridReport">
			 <td align="right" style="font-weight:bold; font-size:12px;"> Total </td>
			 <td align="right" style="font-weight:bold; font-size:12px;">' . $objEmployee->aSystemSettings['CurrencySign']. ' ' . number_format($dTotal_Debit, 0) . '</td>
			 <td align="right" style="font-weight:bold; font-size:12px;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . number_format($dTotal_Credit, 0) . '</td>			 
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
	// General Journal Receipt Report
	function GenerateGeneralJournalReceiptlReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		//Emplyee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Accounts_GeneralJournals[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
					
		$sReference = $objGeneral->fnGet("txtReference");
		$sReference = mysql_escape_string($sReference);
		
		$iGeneralJournalId = $objGeneral->fnGet("id");
		$iGeneralJournalId = mysql_escape_string($iGeneralJournalId);
		
		$sQuery = "
		SELECT
		 GJ.GeneralJournalId AS 'GeneralJournalId',
		 GJ.Reference AS 'Reference',		 
		 GJ.TransactionDate AS 'TransactionDate',
		 GJ.Series AS 'Series',
		 GJ.TotalDebit AS 'TotalDebit',
		 GJ.EntryType AS 'EntryType',
		 GJ.TotalCredit  AS 'TotalCredit',
		 GJ.BankCheckBookId AS 'BankCheckBookId',
		 GJ.BankAccountId AS 'BankAccountId',
		 GJ.CheckNumber AS 'CheckNumber',
		 BA.BankAccountNumber AS 'BankAccountNumber',
		 CB.CheckPrefix AS 'CheckPrefix',
		 GJ.CheckNumber AS 'CheckNumber',
		 GJ.Description AS 'Description'
		FROM fms_accounts_generaljournal AS GJ
		LEFT JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		LEFT JOIN fms_banking_bankcheckbooks AS CB ON CB.BankCheckBookId = GJ.BankCheckBookId 
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy		
		WHERE 1=1 AND GJ.GeneralJournalId= '$iGeneralJournalId' AND GJ.Reference= '$sReference'";
		
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center" style="font-weight:bold; font-size:16px;">Sorry, No records found, Please check your General Journal Id...</div><br /><br />');
			
		$iGeneralJournalId = $objDatabase->Result($varResult, 0, "GeneralJournalId");
		$iEntryType = $objDatabase->Result($varResult, 0, "EntryType");
		$dTransactionDate = $objDatabase->Result($varResult, 0, "TransactionDate");
		$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
		$sReference = $objDatabase->Result($varResult, 0, "Reference");
		$sDescription = $objDatabase->Result($varResult, 0, "Description");
		$iSeries = $objDatabase->Result($varResult, 0, "Series");
		$iBankAccountId = $objDatabase->Result($varResult, 0, "BankAccountId");
		$iBankCheckBookId = $objDatabase->Result($varResult, 0, "BankCheckBookId");
		$sBankAccountNumber = $objDatabase->Result($varResult, 0, "BankAccountNumber");
		$sCheckPrefix = $objDatabase->Result($varResult, 0, "CheckPrefix");
		$sCheckNumber = $objDatabase->Result($varResult, 0, "CheckNumber");		
		
		$sVoucherType = (($iEntryType == 2) ? substr($sReference, 0, 2) : substr($sReference, 0, 3));
		
		switch ($sVoucherType)
		{
			case "BPV": $sVoucher = "Bank Payment Voucher"; break;
			case "BRV": $sVoucher = "Bank Receipt Voucher"; break;
			case "CPV": $sVoucher = "Cash Payment Voucher"; break;
			case "CRV": $sVoucher = "Cash Receipt Voucher"; break;
			case "JV": $sVoucher = "Journal Voucher"; break;
		}

		$sReturn = '<div align="center"><span class="ReportTitle">' . $sVoucher . '</span></div>';

		if($iBankAccountId > 0)
		{
			$sBankAccount = '
			<tr>
			 <td style="font-size:13px;">Bank Account &nbsp;&nbsp;&nbsp;&nbsp;'.  $sBankAccountNumber . '</td>			 
			</tr>';
		}
		if($iBankCheckBookId > 0)
		{
			$sCheckBook = '
			<tr>
			 <td colspan="2" style="font-size:13px;">Check Book &nbsp;&nbsp;&nbsp;&nbsp;' . $sCheckPrefix . '</td>
			</tr>';
			
			$sCheck = '
			<tr>
			 <td colspan="2" style="font-size:13px;">Check Number &nbsp;&nbsp;&nbsp;&nbsp;' . $sCheckNumber . '</td>
			</tr>';
		}
				
		$sReturn .= '<table cellspacing="0" cellpadding="2" width="98%" align="center">
		<tr>
		  <td width="60%" style="font-size:13px;">
		   '. $sVoucherType . ':&nbsp;No #<span style="text-decoration:underline;">&nbsp;' . $iSeries . '&nbsp;</span>&nbsp;
		  </td>
		  <td align="right" style="font-size:13px;">Date:&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;' . $sTransactionDate . '&nbsp;</span></td>
		 </tr>
		 '. $sBankAccount 
		 . $sCheckBook 
		 . $sCheck . '
		</table><br />';
		//AND CA.ChartOfAccountsCategoryId <> '5'
		$varResult = $objDatabase->Query("
		SELECT
		
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 CA.AccountTitle AS 'AccountTitle',
		 GJE.Detail AS 'Details',
		 GJE.Debit AS 'Debit',
		
		 S.StationName AS 'StationName',
		 D.DonorCode AS 'DonorCode',
		 DP.ProjectCode AS 'DonorProject'
		
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= GJE.ChartOfAccountsId
		LEFT JOIN organization_stations AS S ON S.StationId = GJE.StationId
		INNER JOIN fms_accounts_donors AS D ON D.DonorId = GJE.DonorId
		INNER JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId = GJE.DonorProjectId
		
		WHERE 1=1 AND GJ.GeneralJournalId= '$iGeneralJournalId'  AND GJE.Debit > '0'  AND GJ.IsDeleted='0' ");
		
		$sReturn .= '
		<table style="border: solid 1px #cecece; font-family:Tahoma, Arial;" cellspacing="0" cellpadding="5" width="98%" align="center">
		<thead>
		<tr style="background-color:#e1e1e1;">			
		 <td align="left" style="border-left: solid 2px #000000;border-top: solid 2px #000000; border-bottom: solid 2px #000000; border-right: solid 2px #000000; color:#000000; font-size:12px;">Account Debit</td>		   
		 <td align="right" style="width:80px; border-top: solid 2px #000000; border-bottom: solid 2px #000000; border-right: solid 2px #000000; color:#000000; font-size:12px;">&nbsp;</td>
		</tr>
		</thead>';
		
		$dTotalDebit = 0;	
		$iMaxRows = 6;
		$iRowsNumber = $objDatabase->RowsNumber($varResult);
		if ($iRowsNumber > 0)
	    {
			for ($i=0; $i < $iRowsNumber; $i++)
			{							
				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				$sTransactionDetails = $objDatabase->Result($varResult, $i, "Details");
				$sDetails = $sChartOfAccountsCode . ' - ' . $sAccountTitle . ' - ' . $sTransactionDetails;
				$dDebit = $objDatabase->Result($varResult, $i, "Debit");

				$dTotalDebit += $dDebit;
				
				$sStationName = $objDatabase->Result($varResult, $i, "StationName");
				$sDonorCode = $objDatabase->Result($varResult, $i, "DonorCode");
				$sDonorProject = $objDatabase->Result($varResult, $i, "DonorProject");
				$sDetails .= ' <strong>(&nbsp;Station: ' . $sStationName . ', &nbsp;&nbsp;&nbsp;Donor: ' . $sDonorCode . ', &nbsp;&nbsp;&nbsp;Project:' . $sDonorProject . '&nbsp;)</strong>';
				
				$sReturn .= '
				<tr>				 
				 <td valign="top" style="border-left: solid 2px #000000;border-top: solid 1px #cecece; border-right: solid 2px #000000; font-size:13px;">' . $sDetails . '&nbsp;</td>
				 <td valign="top" style="border-right: solid 2px #000000;border-top: solid 1px #cecece; font-size:13px;" align="right">' . number_format($dDebit, 0) . '</td>
				</tr>';
				
				// Last Row
				if (($i+1) == $iRowsNumber)
				{
					$k = $iMaxRows - ($i+1);
					for ($j=0; $j < $k; $j++)
					{
						$sReturn .= '<tr>
						 <td style="border-left: solid 2px #000000;border-top: solid 1px #cecece; border-right: solid 2px #000000; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 2px #000000; font-size:13px;">&nbsp;</td>
					    </tr>';
					}
				}
			}
			
			$sReturn .= '
			<tr>				 
			 <td align="right" valign="top" style="border-left: solid 2px #000000;border-top: solid 1px #cecece; border-right: solid 2px #000000; font-size:13px;font-weight:bold;">Total ' . $objEmployee->aSystemSettings['CurrencySign'] . '</td>
			 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 2px #000000; font-size:13px;font-weight:bold;" align="right">' . number_format($dTotalDebit, 0) . '</td>
			</tr>
			</table>';
	    }
		
		// Get Credit Entries
		//AND CA.ChartOfAccountsCategoryId <> '5' 
		$varResult = $objDatabase->Query("
		SELECT 
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 CA.AccountTitle AS 'AccountTitle',
		 GJE.Detail AS 'Details',
		 GJE.Credit AS 'Credit'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId= GJE.ChartOfAccountsId
		WHERE 1=1 AND GJ.GeneralJournalId= '$iGeneralJournalId' AND GJE.Credit > '0'  AND GJ.IsDeleted='0' ");
		$sReturn .= '
		<table style="border: solid 1px #cecece; font-family:Tahoma, Arial;" cellspacing="0" cellpadding="5" width="98%" align="center">
		<thead>
		<tr style="background-color:#e1e1e1;">			
		 <td align="left" style="border-left: solid 2px #000000;border-top: solid 2px #000000; border-bottom: solid 2px #000000; border-right: solid 2px #000000;color:#000000; font-size:12px;">Account Credit </td>		   
		 <td align="right" style="width:80px; border-top: solid 2px #000000; border-bottom: solid 2px #000000; border-right: solid 2px #000000; color:#000000; font-size:12px;">&nbsp;</td>
		</tr>
		</thead>';
		$dTotalCredit = 0;	
		$iMaxRows = 6;
		$iRowsNumber = $objDatabase->RowsNumber($varResult);
		if ($iRowsNumber > 0)
	    {
			for ($i=0; $i < $iRowsNumber; $i++)
			{							
				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				$sTransactionDetails = $objDatabase->Result($varResult, $i, "Details");
				$sDetails = $sChartOfAccountsCode . ' - ' . $sAccountTitle . ' - ' . $sTransactionDetails;

				$dCredit = $objDatabase->Result($varResult, $i, "Credit");
				
				$dTotalCredit += $dCredit;
				
				$sReturn .= '
				<tr>				 
				 <td valign="top" style="border-left: solid 2px #000000;border-top: solid 1px #cecece; border-right: solid 2px #000000;font-size:13px;">' . $sDetails . '&nbsp;</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 2px #000000; font-size:13px;" align="right">' . number_format($dCredit, 0) . '</td>
				</tr>';
				
				// Last Row
				if (($i+1) == $iRowsNumber)
				{
					$k = $iMaxRows - ($i+1);
					for ($j=0; $j < $k; $j++)
					{
						$sReturn .= '<tr>
						 <td style="border-left: solid 2px #000000;border-top: solid 1px #cecece; border-right: solid 2px #000000;font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 2px #000000; font-size:13px;">&nbsp;</td>
					    </tr>';
					}
				}
			}
			
			$sReturn .= '
			<tr>				 
			 <td align="right" valign="top" style="border-left: solid 2px #000000;border-top: solid 2px #000000; border-bottom: solid 2px #000000;border-right: solid 2px #000000; font-size:13px;font-weight:bold;">Total ' . $objEmployee->aSystemSettings['CurrencySign'] . '</td>
			 <td valign="top" style="border-top: solid 2px #000000; border-bottom: solid 2px #000000; border-right: solid 2px #000000; font-size:13px;font-weight:bold;" align="right">' . number_format($dTotalCredit, 0) . '</td>
			</tr>
			</table>';
	    }

		// In words
		$sTotalAmountInWords = $objGeneral->NumberToWord($dTotalCredit);
		
		$sReturn .= '<br />
		<table cellspacing="0" cellpadding="5" width="98%" align="center">
		 <tr>
		  <td valign="top" style="border-bottom: solid 1px #cecece; font-size:13px;font-weight:bold;" align="left">Rupees</td>
		  <td colspan="2" valign="top" style="border-bottom: solid 1px #cecece; font-size:13px;font-weight:bold;" align="left">' . $sTotalAmountInWords . '&nbsp;' . $objEmployee->aSystemSettings['CurrencyName'].  ' Only</td>
		 </tr>
		 <tr>
		  <td valign="top" style="border-bottom: solid 1px #cecece; font-size:13px;font-weight:bold;" align="left">Particulars</td>
		  <td colspan="2" valign="top" style="border-bottom: solid 1px #cecece; font-size:13px;font-weight:bold;" align="left">' . $sDescription . '</td>
		 </tr>
		 <tr>
		  <td colspan="3" valign="top" style="border-bottom: solid 1px #cecece;font-size:13px;font-weight:bold;" align="left">&nbsp;</td>
		 </tr>
		  <tr>
		  <td colspan="3" valign="top" style="border-bottom: solid 1px #cecece;font-size:13px;font-weight:bold;" align="left">&nbsp;</td>
		 </tr>
		</table>
		<br /><br /><br />';
		
		if(file_exists('../../system/config/organization_signatories.php'))
		{
			include('../../system/config/organization_signatories.php');
		}
		
		else
			$sReturn .= '
			<table cellspacing="0" cellpadding="5" border="1" width="98%" align="center">
			 <tr>
			  <td align="center" style="border-top: solid 1px #cecece;font-weight:bold; font-size:15px;">Prepared By</td>
			  <td>&nbsp;</td>
			  <td align="center" style="border-top: solid 1px #cecece;font-weight:bold; font-size:15px;">Checked By</td>
			  <td>&nbsp;</td>
			  <td align="center" style="border-top: solid 1px #cecece;font-weight:bold; font-size:15px;">Approved By</td>
			  <td>&nbsp;</td>
			 </tr>
			</table>';
		
		return($sReturn);
	}

	/* Budget List Report */
	function GenerateBudgetListReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (B.BudgetStartDate >= '$dCriteria_StartDate' AND B.BudgetStartDate <= '$dCriteria_EndDate')";
				
		$sReturn = '<div align="center"><span class="ReportTitle">Budget List Report</span></div>';
		
		// Date Range
		$sCriteria_DateRange = date("F j, Y", strtotime($dCriteria_StartDate)) . ' - ' . date("F j, Y", strtotime($dCriteria_EndDate));
		
		$sReturn .= '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>
		 <fieldset><legend style="font-size:13px; font-weight:bold;">Report Criteria:</legend>
		  <table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">		   		   
		   <tr>
			<td>Date Range:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DateRange . '</span></td>
		   </tr>
		  </table>
		 </fieldset>
		 </td></tr>
		</table>';
				
		$sQuery = "
		SELECT
		 *
		FROM fms_accounts_budget AS B		
		INNER JOIN organization_employees AS E ON E.EmployeeId = B.BudgetAddedBy
		WHERE 1=1 $sReportDateTimeCriteria";
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Code</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Title</td>
	      <td align="right" style="font-weight:bold; font-size:12px;">Amount</td>
          <td align="right" style="font-weight:bold; font-size:12px;">Utilized Amount</td>
		  <td align="right" style="font-weight:bold; font-size:12px;">Remaining Amount</td>
          <td align="right" style="font-weight:bold; font-size:12px;">Allocated Amount</td>
	      <td align="right" style="font-weight:bold; font-size:12px;">Remaining Allocated Amount</td>
          <td align="right" style="font-weight:bold; font-size:12px;">Budget Received</td>
	     </tr>
	     </thead>';
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			$dTotalAmount = 0;
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$sBudgetHistory = '';
				$iBudgetId = $objDatabase->Result($varResult, $i, "B.BudgetId");
				$sTitle = $objDatabase->Result($varResult, $i, "B.Title");
				$sBudgetCode = $objDatabase->Result($varResult, $i, "B.BudgetCode");
				/*
				$sProjectTitle = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
				$sProjectCode = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
				$sDonorProject = $sProjectTitle . ' ' . $sProjectCode;
				*/
				$dAmount = $objDatabase->Result($varResult, $i, "B.Amount");
				$sAmount = number_format($dAmount, 2);
				
				// TODO
				$dRemainingAmount = 0;
				$dUsedAmount = 0;
				$varResult2 = $objDatabase->Query("
				SELECT
					SUM(GJE.Debit) AS 'UsedAmount'
				FROM fms_accounts_generaljournal_entries AS GJE
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
                INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
				INNER JOIN fms_accounts_budgetallocation AS BA ON BA.ChartOfAccountId = CA.ChartOfAccountsId
    			WHERE GJE.BudgetId='$iBudgetId' AND CA.ChartOfAccountsCategoryId = '4' AND GJ.IsDeleted='0' ");
				if($objDatabase->RowsNumber($varResult2) > 0)
					$dUsedAmount = $objDatabase->Result($varResult2, 0, "UsedAmount");

				$dRemainingAmount = $dAmount - $dUsedAmount;

				$sRemainingAmount = number_format($dRemainingAmount, 0);

				$varResult2 = $objDatabase->Query("SELECT SUM(BA.Amount) AS 'AllocatedAmount' FROM fms_accounts_budgetallocation AS BA WHERE BA.BudgetId = '$iBudgetId'");
				if($objDatabase->RowsNumber($varResult2) > 0)
					$dAllocatedAmount = $objDatabase->Result($varResult2, 0, "AllocatedAmount");

				$varResult2 = $objDatabase->Query("SELECT SUM(BA.Amount) AS 'AllocatedAmount' FROM fms_accounts_budgetallocation AS BA WHERE BA.BudgetId = '$iBudgetId'");
				if($objDatabase->RowsNumber($varResult2) > 0)
					$dAllocatedAmount = $objDatabase->Result($varResult2, 0, "AllocatedAmount");

				$dRemainingAllocatedAmount = $dAmount - $dAllocatedAmount;
				$sRemainingAllocatedAmount = number_format($dRemainingAllocatedAmount, 0);

                $dBudgetReceived = 0;
				$varResult2 = $objDatabase->Query("
				SELECT
					SUM(GJE.Credit) AS 'BudgetReceived'
				FROM fms_accounts_generaljournal_entries AS GJE
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
                INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
    			WHERE GJE.BudgetId='$iBudgetId' AND CA.ChartOfAccountsCategoryId = '3' AND GJ.IsDeleted='0' ");
				if($objDatabase->RowsNumber($varResult2) > 0)
					$dBudgetReceived = $objDatabase->Result($varResult2, 0, "BudgetReceived");

				// Grand Total
				$dTotalAmount += $dAmount;
                $dTotalUsedAmount += $dUsedAmount;
				$dTotalRemainingAmount += $dRemainingAmount;
                $dTotalAllocatedAmount += $dAllocatedAmount;
				$dTotalRemainingAllocatedAmount += $dRemainingAllocatedAmount;
                $dTotalBudgetReceived += $dBudgetReceived;
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sBudgetCode . '&nbsp;</td>
				 <td align="left">' . $sTitle . '&nbsp;</td>
				 <td align="right">' . $sAmount . '&nbsp;</td>
                 <td align="right">' . number_format($dUsedAmount, 2) . '&nbsp;</td>
				 <td align="right">' . number_format($dRemainingAmount, 2) . '&nbsp;</td>
                 <td align="right">' . number_format($dAllocatedAmount, 2) . '&nbsp;</td>
				 <td align="right">' . number_format($RemainingAllocatedAmount, 2) . '&nbsp;</td>
                 <td align="right">' . number_format($dBudgetReceived, 2) . '&nbsp;</td>
				</tr>
				<tr>
				</tr>';
			}

			$sReturn .= '<tr class="GridReport">
			 <td align="right" colspan="3" style="font-family:Tahoma;font-size:16px;">Total:</td>
			 <td align="right" style="font-family:Tahoma;font-size:16px;">' . number_format($dTotalAmount,2) . '</td>
             <td align="right" style="font-family:Tahoma;font-size:16px;">' . number_format($dTotalUsedAmount,2) . '</td>
			 <td align="right" style="font-family:Tahoma;font-size:16px;">' . number_format($dTotalRemainingAmount,2) . '</td>
             <td align="right" style="font-family:Tahoma;font-size:16px;">' . number_format($dTotalAllocatedAmount, 2) . '&nbsp;</td>
			 <td align="right" style="font-family:Tahoma;font-size:16px;">' . number_format($dTotalRemainingAllocatedAmount,2) . '</td>
             <td align="right" style="font-family:Tahoma;font-size:16px;">' . number_format($dTotalBudgetReceived,2) . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="9" align="center" style="font-family:Tahoma; Font-size:16px;"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}

	/* Budget Allocation Report */
	function GenerateBudgetAllocationReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BudgetReports_BudgetAllocationReport[0] == 0)
			//return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$iCriteria_BudgetId = $objGeneral->fnGet("selBudget");		
		$sBudgetCondition = (($iCriteria_BudgetId > 0 && $iCriteria_BudgetId != -1) ? "AND B.BudgetId = '$iCriteria_BudgetId'" : '');
		
		// Budget
		if (($iCriteria_BudgetId == -1) || !($iCriteria_BudgetId)) $sCriteria_Budget = "All Budgets";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.BudgetId = '$iCriteria_BudgetId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Budget Id...');
			$sCriteria_Budget = $objDatabase->Result($varResult, 0, "B.BudgetCode");
		}
		
		$sReturn = '<div align="center"><span class="ReportTitle">Budget Allocation Report</span></div>';
		
		$sQuery = "
		SELECT
		 *
		FROM fms_accounts_budget AS B
		INNER JOIN fms_accounts_budgetallocation AS BA ON BA.BudgetId = B.BudgetId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = BA.ChartOfAccountId
		INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BudgetAllocationAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sBudgetCondition
		GROUP BY B.BudgetId, CA.ChartOfAccountsId
		ORDER BY BA.Title";
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">';
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			$dTotalAmount = 0;
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iBudgetId = $objDatabase->Result($varResult, $i, "B.BudgetId");
				$iChartOfAccountId = $objDatabase->Result($varResult, $i, "BA.ChartOfAccountId");
				$sBudgetTitle = $objDatabase->Result($varResult, $i, "B.Title");
				$sBudgetCode = $objDatabase->Result($varResult, $i, "B.BudgetCode");

				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "CA.AccountTitle");
				$sChartOfAccount = $sChartOfAccountsCode . ' - ' . $sAccountTitle;
				$sTitle = $objDatabase->Result($varResult, $i, "BA.Title");
				$dAmount = $objDatabase->Result($varResult, $i, "BA.Amount");
				$sAmount = number_format($dAmount, 0);
				
				// TODO
				$dRemainingAmount = 0;
				$dUsedAmount = 0;
				$dUsedAmount2 = 0;
				$varResult2 = $objDatabase->Query("
				SELECT
					SUM(GJE.Debit) AS 'UsedAmount'
				FROM fms_accounts_generaljournal_entries AS GJE
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
                INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
				WHERE (GJE.ChartOfAccountsId = '$iChartOfAccountId') AND GJE.BudgetId = '$iBudgetId'  AND GJ.IsDeleted='0' /*AND CA.ChartOfAccountsCategoryId = '4'*/");
				if($objDatabase->RowsNumber($varResult2) > 0)
					$dUsedAmount = $objDatabase->Result($varResult2, 0, "UsedAmount");
				
				//Parent Child
				$varResult3 = $objDatabase->Query("SELECT * FROM fms_accounts_chartofaccounts AS CA WHERE CA.ParentChartofAccountsId = '$iChartOfAccountId'");
				if($objDatabase->RowsNumber($varResult3) > 0)
				{
					for($j = 0; $j < $objDatabase->RowsNumber($varResult3); $j++)
					{
						$iChild_ChartOfAccountId = $objDatabase->Result($varResult3, $j, "CA.ChartofAccountsId");
						
						$varResult4 = $objDatabase->Query("
						SELECT
							SUM(GJE.Debit) AS 'UsedAmount'
						FROM fms_accounts_generaljournal_entries AS GJE
						INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
						INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
    					WHERE (GJE.ChartOfAccountsId = '$iChild_ChartOfAccountId')  AND GJ.IsDeleted='0' AND GJE.BudgetId = '$iBudgetId'");
						if($objDatabase->RowsNumber($varResult4) > 0)
						{	
							$dUsedAmount2 = $objDatabase->Result($varResult4, 0, "UsedAmount");
							$dUsedAmount += $dUsedAmount2;
						}
					}
				}
				//End Parent Child
				
				$dRemainingAmount = $dAmount - $dUsedAmount;
				$sRemainingAmount = number_format($dRemainingAmount, 0);
				
				// Check for Account Head change
				if(($iTemp_BudgetId != $iBudgetId && $i == 0))
				{
					$sReturn .='
					<tr style="background-color:#e1e1e1;">
					 <td colspan="5" align="left" style="font-weight:normal; font-family:Tahoma, Arial; font-size:16px;">Budget: ' . $sBudgetTitle . '</td>
					</tr>
					<tr style="background-color:#e1e1e1;">
					 <td align="left" style="font-weight:bold; font-size:12px;">Chart Of Account </td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Title </td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Amount</td>
                     <td align="right" style="font-weight:bold; font-size:12px;">Utilized Amount</td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Remainig Amount</td>
					</tr>';
				}
				if(($iTemp_BudgetId != $iBudgetId && $i > 0))
				{
					$sReturn .='
					<tr>
					 <td align="right" colspan="2" style="font-family:Tahoma; font-size:16px;">Total:</td>
					 <td align="right" style="font-family:Tahoma; font-size:16px;">' . number_format($dTotalAmount, 0) . '</td>
                     <td align="right" style="font-family:Tahoma; font-size:16px;">' . number_format($dTotalUsedAmount,0) . '</td>
					 <td align="right" style="font-family:Tahoma; font-size:16px;">' . number_format($dTotalRemainingAmount,0) . '</td>
					</tr>
					<tr style="background-color:#e1e1e1;">
					 <td align="left" style="font-weight:bold; font-size:12px;">Budget </td>
					 <td align="center"><strong>' . $sBudgetTitle . '</strong></td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Donor Project</td>
					 <td align="center"><strong>' . $sDonorProject . '</strong></td>
                     <td colspan="2">&nbsp;</td>
					</tr>
					<tr style="background-color:#e1e1e1;">
					 <td align="left" style="font-weight:bold; font-size:12px;">Chart Of Account </td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Title </td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Amount</td>
                     <td align="right" style="font-weight:bold; font-size:12px;">Utilized Amount</td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Remainig Amount</td>
					</tr>';
					
                    $dTotalAmount = 0;
                    $dTotalUsedAmount = 0;
                    $dTotalRemainingAmount = 0;
				}
				
				$sReturn .= '<tr>
				 <td align="left">' . $sChartOfAccount . '&nbsp;</td>
				 <td align="left">' . $sTitle . '&nbsp;</td>
				 <td align="right">' . $sAmount . '</td>
                 <td align="right">' . number_format($dUsedAmount+$dUsedAmount3, 0) . '</td>
				 <td align="right">' . number_format($dRemainingAmount, 0) . '</td>
				</tr>';

				$iTemp_BudgetId = $iBudgetId;
				
				// Total
				$dTotalAmount += $dAmount;
				$dTotalRemainingAmount += $dRemainingAmount;
				$dTotalUsedAmount += $dUsedAmount;
				//print $dUsedAmount2 . ' * ';
				$dTotalRemainingAllocatedAmount += $dRemainingAllocatedAmount;
				
				// Grand Total
				$dGrandTotalAmount += $dAmount;
				$dGrandTotalRemainingAmount += $dRemainingAmount;
				$dGrandTotalUsedAmount += $dUsedAmount;
				$dGrandTotalRemainingAllocatedAmount += $dRemainingAllocatedAmount;
				
				
			}
			
			$sReturn .= '<tr>
			 <td align="right" colspan="2" style="font-family:Tahoma; font-size:16px;">Total:</td>
			 <td align="right" style="font-family:Tahoma; font-size:16px;">' . number_format($dTotalAmount,0) . '</td>
             <td align="right" style="font-family:Tahoma; font-size:16px;">' . number_format($dTotalUsedAmount,0) . '</td>
			 <td align="right" style="font-family:Tahoma; font-size:16px;">' . number_format($dTotalRemainingAmount,0) . '</td>
			</tr>';
			
			$sReturn .= '
			<tr>
			<td colspan="5"><br /><br /></td>
			</tr>
			<tr>
			 <td align="right" colspan="2" style="font-family:Tahoma; font-size:16px;">Grand Total:</td>
			 <td align="right" style="font-family:Tahoma; font-size:16px;">' . number_format($dGrandTotalAmount,0) . '</td>
             <td align="right" style="font-family:Tahoma; font-size:16px;">' . number_format($dGrandTotalUsedAmount,0) . '</td>
			 <td align="right" style="font-family:Tahoma; font-size:16px;">' . number_format($dGrandTotalRemainingAmount,0) . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="6" align="center" style="font-family:Tahoma; Font-size:16px;"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	
	function GenerateBudgetUtilizationReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_BudgetUtilizationReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$iCriteria_BudgetId = $objGeneral->fnGet("selBudget");		
		$sBudgetCondition = (($iCriteria_BudgetId > 0 && $iCriteria_BudgetId != -1) ? "AND B.BudgetId = '$iCriteria_BudgetId'" : '');
		
		// Budget
		if ( ($iCriteria_BudgetId == -1) || !($iCriteria_BudgetId)) $sCriteria_Budget = "All Budgets";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_budget AS B WHERE B.BudgetId = '$iCriteria_BudgetId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Budget Id...');
			$sCriteria_Budget = $objDatabase->Result($varResult, 0, "B.BudgetCode");
		}
		
		$sReturn = '<div align="center"><span class="ReportTitle">Deleted Transactions Report</span></div>';
		
		$sReturn .= '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>
		 <fieldset><legend style="font-size:13px; font-weight:bold;">Report Criteria:</legend>
		  <table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">		   
		   <tr>
			<td width="33%" colspan="2">Budget:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Budget . '</span></td> 
		   </tr>
		  </table>
		 </fieldset>
		 </td></tr>
		</table>';
		
		$varResult = $objDatabase->Query("
		SELECT * 
		FROM fms_accounts_generaljournal_entries AS GJE
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		INNER JOIN fms_accounts_budget AS B ON B.BudgetId = GJE.BudgetId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_budgetallocation AS BA ON BA.ChartOfAccountId = CA.ChartOfAccountsId		
		WHERE ChartOfAccountsCategoryId='4'  AND GJ.IsDeleted='0'  $sBudgetCondition 
		GROUP BY CA.ChartOfAccountsId");	
		
		$sReturn .= '<br /><table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">';
		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			$dTotalAmount = 0;
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iBudgetId = $objDatabase->Result($varResult, $i, "B.BudgetId");
				$iChartOfAccountId = $objDatabase->Result($varResult, $i, "GJE.ChartOfAccountsId");
				$sBudgetTitle = $objDatabase->Result($varResult, $i, "B.Title");
				/*
				$sProjectTitle = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
				$sProjectCode = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
				$sDonorProject = $sProjectTitle . ' ' . $sProjectCode;
				*/
				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "CA.AccountTitle");
				$sChartOfAccountTitle = $sAccountTitle;
				$dDebitAmount = $objDatabase->Result($varResult, $i, "GJE.Debit");
				$dCreditAmount = $objDatabase->Result($varResult, $i, "GJE.Credit");
				$sCreditAmount  = number_format($dCreditAmount, 2);
				$sCreditAmount  = number_format($dCreditAmount, 2);
												
				// Grand Total
				$dTotalRemainingAmount += $dRemainingAmount;
                $dTotalUsedAmount += $dUsedAmount;
				$dTotalRemainingAllocatedAmount += $dRemainingAllocatedAmount;

				$dTotalDebitAmount = 0;
				$dTotalCreditAmount = 0;
				$sDetialRows ='';
				$varResult1 = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_entries AS GJE
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
				WHERE GJE.ChartOfAccountsId = '$iChartOfAccountId' AND GJE.BudgetId = '$iBudgetId'  AND GJ.IsDeleted='0'");
				if($objDatabase->RowsNumber($varResult1) > 0)
				for ($j=0; $j < $objDatabase->RowsNumber($varResult1); $j++)
				{
					$sReference = $objDatabase->Result($varResult1, $j, "GJ.Reference");
					$sDetials = $objDatabase->Result($varResult1, $j, "GJE.Detail");
					$sDebit = $objDatabase->Result($varResult1, $j, "GJE.Debit");
					$sCredit = $objDatabase->Result($varResult1, $j, "GJE.Credit");
					
					$sDetialRows .='
					<tr>
					 <td align="left">' . $sReference . '</td>
					 <td align="left">' . $sDetials . '</td>
					 <td align="right">' . $sDebit . '</td>
					 <td align="right">' . $sCredit . '</td>
					</tr>';
									
					$dTotalDebitAmount += $sDebit;
					$dTotalCreditAmount += $sCredit;
				
				}	
				
				// Check for Account Head change
				if(($iTemp_BudgetId != $iBudgetId && $i == 0))
				{
					$sReturn .='
					<tr style="background-color:#e1e1e1;">
					 <td align="left" colspan="4" style="font-size:12px;"><strong>Budget </strong> : ' . $sBudgetTitle . '					 
					</tr>
					';
				}
				if(($iTemp_BudgetId != $iBudgetId && $i > 0))
				{
					$sReturn .='
					<tr style="background-color:#e1e1e1;">
					 <td align="left" colspan="4" style="font-size:12px;"><strong>Budget </strong> : ' . $sBudgetTitle . '					 
					</tr>';
                    
				}

				$sReturn .= 
				'<tr style="background-color:#e1e1e1;">
				 <td align="left" colspan="4"><strong>Chart of Account</strong> : ' . $sChartOfAccountTitle . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Code </strong> : ' . $sChartOfAccountsCode . '&nbsp;</td>
				 </tr>
				 <tr style="background-color:#e1e1e1;">
				  <td align="left"><strong>Reference</strong></td>
				  <td><strong>Details</strong></td>
				  <td align="right"><strong>Debit</strong></td>
				  <td align="right"><strong>Credit</strong></td>
				 </tr>'.$sDetialRows;
			
				$sReturn .= '<tr class="GridReport">
				 <td align="right" colspan="2" style="font-family:Tahoma; font-size:12px;"><strong>Total:</strong></td>
				 <td align="right" style="font-family:Tahoma; font-size:12px;"><strong>' . number_format($dTotalDebitAmount,2) . '</strong></td>
				 <td align="right" style="font-family:Tahoma; font-size:12px;"><strong>' . number_format($dTotalCreditAmount,2) . '</strong></td>
				</tr>';
				
				$iTemp_BudgetId = $iBudgetId;

			}

	    }
	    else
	    	$sReturn .= '<tr><td colspan="6" align="center" style="font-family:Tahoma; Font-size:16px;"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}

	function GenerateDeletedTransactionsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankAccountsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		//$iCriteria_SourceOfIncomeId = $objGeneral->fnGet("selSourceOfIncome");
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		$iCriteria_DonorId = $objGeneral->fnGet("selDonor");
		$iCriteria_DonorProjectId = $objGeneral->fnGet("selDonorProject");
		$iCriteria_ProjectActivityId = $objGeneral->fnGet("selProjectActivity");
		$iCriteria_BudgetId = $objGeneral->fnGet("selBudget");		
		$iCriteria_ChartOfAccountsCategoryId = $objGeneral->fnGet("selChartOfAccountCategory");
		$iCriteria_ChartOfAccountsControlId = $objGeneral->fnGet("selChartOfAccountControl");		
		$iCriteria_ChartOfAccountId = $objGeneral->fnGet("selChartOfAccount");
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">Deleted Transactions Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sBankTable = "";
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		if($iCriteria_BankId != -1 && $iCriteria_BankId != "")	$sBankTable = "INNER JOIN fms_banking_bankaccounts AS BA ON BA.ChartOfAccountsId = CA.ChartOfAccountsId";
		
		//AND CA.ChartOfAccountsCategoryId != '5' 
		$sQuery = "
		SELECT
		 GJ.GeneralJournalId AS 'Id',
		 GJ.Reference AS 'Reference'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
		$sBankTable
		WHERE 1=1 AND GJ.IsDeleted='1' $sReportDateTimeCriteria $sReportCriteriaSQL
        GROUP BY GJ.GeneralJournalId";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">Id</td>
		  <td align="center" style="font-weight:bold; font-size:12px;">Reference</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Account Code</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Account Title</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Detail</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Check No#</td>
		  <td align="right" style="font-weight:bold; font-size:12px;">Debit</td>
          <td align="right" style="font-weight:bold; font-size:12px;">Credit</td>
	     </tr>
	     </thead>';
		 $dTotalDebit = 0;
		 $dTotalCredit = 0;
		 $iTempId = 0;

	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iId = $objDatabase->Result($varResult, $i, "Id");
				$sReference = $objDatabase->Result($varResult, $i, "Reference");
				//AND CA.ChartOfAccountsCategoryId != '5'
                $sQuery = "
        		SELECT
        		 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
        		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
        		 CA.AccountTitle AS 'AccountTitle',
                 GJE.Detail AS 'Detail',
                 GJ.CheckNumber AS 'CheckNumber',
				 GJ.GeneralJournalId AS 'GeneralJournalId',
                 GJE.Debit AS 'Debit',
        		 GJE.Credit AS 'Credit'
        		FROM fms_accounts_generaljournal AS GJ
        		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
        		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
        		WHERE 1=1  AND GJ.IsDeleted='1' AND GJE.GeneralJournalId = '$iId'";

                $varResult2 = $objDatabase->Query($sQuery);
                for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
			    {
                    $iGeneralJournalId = $objDatabase->Result($varResult2, $k, "GeneralJournalId");
					$iChartOfAccountsId = $objDatabase->Result($varResult2, $k, "ChartOfAccountsId");
					$sAccountTitle = $objDatabase->Result($varResult2, $k, "AccountTitle");
    				$sChartOfAccountsCode = $objDatabase->Result($varResult2, $k, "ChartOfAccountsCode");
                    $sDetail = $objDatabase->Result($varResult2, $k, "Detail");
                    $sCheckNumber = $objDatabase->Result($varResult2, $k, "CheckNumber");
                    if($sCheckNumber == 0) $sCheckNumber = "";
    				$dDebit = $objDatabase->Result($varResult2, $k, "Debit");
    				$sDebit = number_format($dDebit, 0);
    				$dCredit = $objDatabase->Result($varResult2, $k, "Credit");
    				$sCredit = number_format($dCredit, 0);
    				$dTotalCredit += $dCredit;
    				$dTotalDebit += $dDebit;

                    $iTransactionId = $iId;
    				if($iTempId == $iId)
    				{
    					$iTransactionId = '';
    					$sReference = '';
    				}
    				$iTempId = $iId;

                    $sReturn .= '
    				<tr>
    				 <td align="center">' . $iTransactionId . '&nbsp;</td>    				 
					 <!--<td align="center"><a style="font-family:Tahoma, Arial; text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&id=' . $iGeneralJournalId. '&txtReference=' . $sReference . '\', 800,600);">' . $sReference . '&nbsp;</a>&nbsp;</td>-->
					<td align="center"><a style="font-family:Tahoma, Arial; font-size:12px; text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&id=' . $iGeneralJournalId. '&txtReference=' . $sReference . '\', 800,600);">' . $sReference . '&nbsp;</a></td>
    				 <td align="left"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selBank=' . $iCriteria_BankId . '&selBankAccount=' .$iCriteria_BankAccountId . '&selDonor=' .$iCriteria_DonorId . '&selDonorProject=' .$iCriteria_DonorProjectId . '&selProjectActivity=' .$iCriteria_ProjectActivityId . '&selChartOfAccountCategory='. $iCategoryId . '&selChartOfAccountControl='. $iCriteria_ChartOfAccountsControlId . '&selChartOfAccount='. $iChartOfAccountsId . '&txtDateRangeStart='. $dCriteria_StartDate . '&txtDateRangeEnd='. $dCriteria_EndDate . '\', 800,600);" />' . $sChartOfAccountsCode . '&nbsp;</td>
    				 <td align="left">' . $sAccountTitle . '&nbsp;</td>
                     <td align="left">' . $sDetail . '&nbsp;</td>
                     <td align="left">' . $sCheckNumber . '&nbsp;</td>
    				 <td align="right">' . $sDebit . '&nbsp;</td>
    				 <td align="right">' . $sCredit . '&nbsp;</td>
    				</tr>';
                    }

			}

			$sReturn .= '
			<tr>
			 <td style="font-weight:bold;font-size:14px;" align="right" colspan="6">Total</td>
			 <td style="font-weight:bold;font-size:14px;" align="right">' . number_format($dTotalDebit, 0) . '&nbsp;</td>
			 <td style="font-weight:bold;font-size:14px;" align="right">' . number_format($dTotalCredit, 0). '&nbsp;</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma;font-size:16px;"  align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	
	
	/* AJAX Functions */
	function AJAX_GetControlCode($iChartOfAccountsCategoryId, $sTargetSelectBox)
	{
	   	$objResponse = new xajaxResponse();

	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
	   	$aData = $this->GetControls($iChartOfAccountsCategoryId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		return($objResponse->getXML());
	}
	
	function GetControls($iChartOfAccountsCategoryId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_chartofaccounts_controls AS CAC		
		WHERE CAC.ChartOfAccountsCategoryId='$iChartOfAccountsCategoryId'");		
		$aResult[0][0] = -1;
		$aResult[0][1] = "Select Control Code";
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{						
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "CAC.ChartOfAccountsControlId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "CAC.ControlCode") . '-' .$objDatabase->Result($varResult, $i, "CAC.ControlName");
		}
		return($aResult);
	}	
	
	
	function AJAX_GetChartOfAccounts($iChartOfAccountsControlId, $sTargetSelectBox)
	{
	   	$objResponse = new xajaxResponse();

	   	$objResponse->addScript("OptionsList_RemoveAll('" . $sTargetSelectBox . "')");
	   	$aData = $this->GetChartOfAccounts($iChartOfAccountsControlId);
	   	for ($i=0; $i < count($aData); $i++)
	    	$objResponse->addScript("FillSelectBox('$sTargetSelectBox', '" . $aData[$i][1] . "', '" . $aData[$i][0] . "');");
		return($objResponse->getXML());
	}
	
	function GetChartOfAccounts($iChartOfAccountsControlId)
	{
		global $objDatabase;
		
		$varResult = $objDatabase->Query("SELECT * 
		FROM fms_accounts_chartofaccounts AS CA
		WHERE CA.ChartOfAccountsControlId='$iChartOfAccountsControlId'");		
		$aResult[0][0] = -1;
		$aResult[0][1] = "Select Chart Of Account";
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{						
			$aResult[$i+1][0] = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsId");
			$aResult[$i+1][1] = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode") . '-' .$objDatabase->Result($varResult, $i, "CA.AccountTitle");
		}
		return($aResult);
	}
		
	/* End */


}
?>