<?php

class clsFMS_Reports_BankingReports extends clsFMS_Reports
{
	public $aWithdrawalType;
	
	function __construct()
	{		
		$this->aWithdrawalType = array("By Check", "By Online Transfer", "By ATM");
	}
	
	function ReportFilter(&$sReportCriteria, &$sReportCriteriaSQL)
	{
		global $objGeneral;
		global $objDatabase;
				
		// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_BankId = $objGeneral->fnGet("selBank");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		//if ($iCriteria_EmployeeId == '') $iCriteria_EmployeeId = -1;
		if ($iCriteria_BankId == '' || $iCriteria_BankId == 0) $iCriteria_BankId = -1;
		if ($iCriteria_BankAccountId == '' || $iCriteria_BankAccountId == 0) $iCriteria_BankAccountId = -1;
		
		//$sReportCriteriaSQL = ($iCriteria_EmployeeId != -1) ? " AND E.EmployeeId='$iCriteria_EmployeeId'" : '';
		$sReportCriteriaSQL = ($iCriteria_BankId != -1) ? " AND BA.BankId='$iCriteria_BankId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_BankAccountId != -1) ? " AND BA.BankAccountId='$iCriteria_BankAccountId'" : '';
		
		// Date Range
		$sCriteria_DateRange = date("F j, Y", strtotime($dCriteria_StartDate)) . ' - ' . date("F j, Y", strtotime($dCriteria_EndDate));

		// Employees
		if ($iCriteria_EmployeeId == -1 || !$iCriteria_EmployeeId) $sCriteria_Employee = "All Employees";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId = '$iCriteria_EmployeeId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Id...');
			$sCriteria_Employee = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
		}
		// Banks
		if ( ($iCriteria_BankId == -1) || !($iCriteria_BankId)) $sCriteria_Banks= "All Banks";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_banks AS B WHERE B.BankId = '$iCriteria_BankId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Id...');
			$sCriteria_Banks = $objDatabase->Result($varResult, 0, "B.BankName");
		}

		// Bank Accounts
		if ( ($iCriteria_BankAccountId == -1) || !($iCriteria_BankAccountId)) $sCriteria_BankAccounts = "All Bank Accounts";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.BankAccountId = '$iCriteria_BankAccountId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Account Id...');
			$sCriteria_BankAccounts = $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");
		}		
		
		$sReportCriteria .= '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>
		 <fieldset><legend style="font-size:13px; font-weight:bold;">Report Criteria:</legend>
		  <table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		   <tr>
		    <td width="33%">Bank :&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Banks . '</span></td>  
		    <td width="33%">Bank Account :&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_BankAccounts . '</span></td>			
		   </tr>
		   <tr>		    
		    <td>Date Range:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DateRange . '</span></td>
		   </tr>		   
		  </table>
		 </fieldset>
		 </td></tr>
		</table>';

		return(true);
	}
	
    function ShowBankingReports($sReportName)
    {
    	global $objDatabase;
    	global $objEmployee;
    	$iEmployeeId = $objEmployee->iEmployeeId;
		
		/*
    	// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports[0] == 0)
    		return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Customer Reports</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';
		
		$sBankLedger = '<a ' . (($sReportName == "BankLedger") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/bankingreports_show.php?reportname=BankLedger"><img src="../images/reports/iconBankingReports_BankStatement.gif" border="0" alt="Bank Ledger" title="Bank Ledger" /><br />Bank Ledger</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankLedgerReport[0] == 0)
			$sBankLedger = '<img src="../images/reports/iconBankingReports_BankStatement_disabled.gif" border="0" alt="Bank Ledger" title="Bank Ledger" /><br />Bank Ledger';
		
		$sBankDeposits = '<a ' . (($sReportName == "BankDeposits") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/bankingreports_show.php?reportname=BankDeposits"><img src="../images/banking/iconBankDeposits.gif" border="0" alt="Bank Deposits Report" title="Bank Deposits Report" /><br />Bank Deposits Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] == 0)
			$sBankDeposits = '<img src="../images/banking/iconBankDeposits_disabled.gif" border="0" alt="Bank Deposits Report" title="Bank Deposits Report" /><br />Bank Deposits Report';
				
		$sBankWithdrawals = '<a ' . (($sReportName == "BankWithdrawals") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/bankingreports_show.php?reportname=BankWithdrawals"><img src="../images/banking/iconBankWithdrawals.gif" border="0" alt="Bank Withdrawals Report" title="Bank Withdrawals Report" /><br />Bank Withdrawals Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankWithdrawalsReport[0] == 0)
			$sBankWithdrawals = '<img src="../images/banking/iconBankWithdrawals_disabled.gif" border="0" alt="Bank Withdrawals Report" title="Bank Withdrawals Report" /><br />Bank Withdrawals Report';
		
		$sBankReconciliation = '<a ' . (($sReportName == "BankReconciliation") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/bankingreports_show.php?reportname=BankReconciliation"><img src="../images/banking/iconBankReconciliation.gif" alt="Bank Reconciliation Report" title="Bank Reconciliation Report" border="0" /><br />Bank Reconciliation Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankReconciliationReport[0] == 0)
			$sBankReconciliation = '<img src="../images/banking/iconBankReconciliation_disabled.gif" alt="Bank Reconciliation Report" title="Bank Reconciliation Report" border="0" /><br />Bank Reconciliation Report';
			
		$sBankCheckBooks = '<a ' . (($sReportName == "BankCheckBooks") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/bankingreports_show.php?reportname=BankCheckBooks"><img src="../images/banking/iconBankCheckbooks.gif" border="0" alt="Bank Check Books Report" title="Bank Check Books Report" /><br />Bank Check Books Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankCheckBooksReport[0] == 0)
			$sBankCheckBooks = '<img src="../images/banking/iconBankCheckbooks_disabled.gif" border="0" alt="Bank Check Books Report" title="Bank Check Books Report" /><br />Bank Check Books Report';
		
		$sBankAccounts = '<a ' . (($sReportName == "BankAccounts") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/bankingreports_show.php?reportname=BankAccounts"><img src="../images/banking/iconBankAccounts.gif" border="0" alt="Bank Accounts Report" title="Bank Accounts Report" /><br />Bank Accounts Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankAccountsReport[0] == 0)
			$sBankAccounts = '<img src="../images/banking/iconBankAccounts_disabled.gif" border="0" alt="Bank Accounts Report" title="Bank Accounts Report" /><br />Bank Accounts Report';
		
		$sBanks = '<a ' . (($sReportName == "Banks") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/bankingreports_show.php?reportname=Banks"><img src="../images/banking/iconBanks.gif" border="0" alt="Banks Report" title="Banks Report" /><br />Banks Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport[0] == 0)
			$sBanks = '<img src="../images/banking/iconBanks_disabled.gif" border="0" alt="Banks Report" title="Banks Report" /><br />Banks Report';		
		
		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
		  <td width="10%" valign="top" align="center">' . $sBankLedger . '</td>
		  <td width="10%" valign="top" align="center">' . $sBankDeposits . '</td>
		  <td width="10%" valign="top" align="center">' . $sBankWithdrawals . '</td>
		  <td width="10%" valign="top" align="center">' . $sBankReconciliation . '</td>
		  <td width="10%" valign="top" align="center">' . $sBankCheckBooks . '</td>
		  <td width="10%" valign="top" align="center">' . $sBankAccounts . '</td>
		  <td width="10%" valign="top" align="center">' . $sBanks . '</td>
		 </tr>		 
		</table>
		<br />';

        $sReturn .= $this->ShowReportCriteria($sReportName);

		$sReturn .= '</td></tr></table></td></tr></table>';
		return($sReturn);
    }

    function ShowReportCriteria($sReportName)
    {			
    	switch($sReportName)
    	{   
			case "BankLedger": $sReturn = $this->BankLedgerCriteria(); break;
			case "BankDeposits": $sReturn = $this->BankDepositsCriteria(); break;
			case "BankWithdrawals": $sReturn = $this->BankWithdrawalsCriteria(); break;
			case "BankCheckBooks": $sReturn = $this->BankCheckBooksCriteria(); break;
			case "BankReconciliation": $sReturn = $this->BankReconciliationCriteria(); break;
			case "BankAccounts": $sReturn = $this->BankAccountsCriteria(); break;
			case "Banks": $sReturn = $this->BanksCriteria(); break;
    	}

    	$sReturn .= '
    	<script type="text/javascript" language="JavaScript">
		function GenerateReport(sReportName, sReportType)
		{
			var aReportCriteria;
			var sReportFilter;

			aReportCriteria = new Array();

			//iEmployee = (GetVal("selEmployee") == "") ? -1 : GetVal("selEmployee");
			iBank = (GetVal("selBank") == "") ? -1 : GetVal("selBank");
			iBankAccount = (GetVal("selBankAccount") == "") ? -1 : GetVal("selBankAccount");		
			
			dDateRange_Start = (GetVal("txtDateRange_Start") == "") ? -1 : GetVal("txtDateRange_Start");
			dDateRange_End = (GetVal("txtDateRange_End") == "") ? -1 : GetVal("txtDateRange_End");

			//sReportFilter = "&selEmployee=" + iEmployee;
			sReportFilter = "&selBank=" + iBank;
			sReportFilter += "&selBankAccount=" + iBankAccount;
			
			if(sReportType == "BankReconciliation")
				sReportFilter += "&selReconciliationStatus=" + GetVal("selReconciliationStatus");
								
			sReportFilter += "&txtDateRangeStart=" + dDateRange_Start;
			sReportFilter += "&txtDateRangeEnd=" + dDateRange_End;
						
			jsOpenWindow("../reports/showreport.php?report=" + sReportName + "&reporttype=" + sReportType + sReportFilter, 800,600);
		}
		</script>';

    	return($sReturn);
    }    
	
	function BankLedgerCriteria()
    {
		global $objEmployee;
				
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankLedgerReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
							
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Bank Ledger Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'BankingReports\', \'BankLedger\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
	

	function BankDepositsCriteria()
    {
		global $objEmployee;
				
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
							
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Bank Deposits Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'BankingReports\', \'BankDeposits\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
    
	function BankWithdrawalsCriteria()
    {
		global $objEmployee;
				
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankWithdrawalsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
							
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Bank Withdrawals Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'BankingReports\', \'BankWithdrawals\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
	
	function BankReconciliationCriteria()
    {
		global $objEmployee;
				
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankReconciliationReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
			
		$sReconciliationStatus = '<select class="Tahoma16" name="selReconciliationStatus" id="selReconciliationStatus">';
		$sReconciliationStatus .= '<option value="-1">All Transactions</option>
		 <option value="0">Non Reconciliated Transactions</option>
		 <option value="1">Reconciliated Transactions</option>
		</select>';
									
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Bank Reconciliation Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>
			   <tr><td>Reconciliation Status:</td><td>' . $sReconciliationStatus . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'BankingReports\', \'BankReconciliation\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
	
	function BankCheckBooksCriteria()
    {
		global $objEmployee;
				
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankCheckBooksReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
				
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Bank Check Books Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'BankingReports\', \'BankCheckBooks\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
	
	function BankAccountsCriteria()
    {
		global $objEmployee;
			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankAccountsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
				
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Bank Accounts Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Bank:</td><td>' . $this->GenerateReportCriteria("Bank", "selBank") . '</td></tr>
			   <tr><td>Bank Account:</td><div id="divBankAccount" name="divBankAccount" style="display:none;"><td>' . $this->GenerateReportCriteria("BankAccount", "selBankAccount") . '</div></td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'BankingReports\', \'BankAccounts\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
	
	function BanksCriteria()
    {
		global $objEmployee;
			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
				
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Banks Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'BankingReports\', \'Banks\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
	
    function GenerateBankingReports($sReportType, $bExportToExcel, &$aExcelData, $sAction = "")
    {			
    	$sReport = $this->GenerateReportHeader($sAction);
    	switch ($sReportType)
    	{   
			case "BankLedger": $sReport .= $this->GenerateBankLedgerReport($bExportToExcel, $aExcelData); break;
			case "BankDeposits": $sReport .= $this->GenerateBankDepositsReport($bExportToExcel, $aExcelData); break;
			case "BankWithdrawals": $sReport .= $this->GenerateBankWithdrawalsReport($bExportToExcel, $aExcelData); break;
			case "ChequeDrawnStatement": $sReport .= $this->GenerateChequeDrawnStatementReport($bExportToExcel, $aExcelData); break;
			case "BankReconciliation": $sReport .= $this->GenerateBankReconciliationReport($bExportToExcel, $aExcelData); break;
			case "BankCheckBooks": $sReport .= $this->GenerateBankCheckBooksReport($bExportToExcel, $aExcelData); break;
			case "BankAccounts": $sReport .= $this->GenerateBankAccountsReport($bExportToExcel, $aExcelData); break;
			case "Banks": $sReport .= $this->GenerateBanksReport($bExportToExcel, $aExcelData); break;
    	}
    	//$sReport .= $this->GenerateReportFooter();
    	return($sReport);
    }   
	
	
	/* Bank Statement Report */
	function GenerateBankLedgerReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankLedgerReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria1 .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";
		$sReportDateTimeCriteria2 .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";
		
		//$sBankCondition = "AND GJ.BankAccountId='$iBankAccountId'";

		$sReturn = '<div align="center"><span class="ReportTitle">Bank Ledger</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		// $sReportDateTimeCriteria $sReportCriteriaSQL
		$sQuery = "
		(SELECT 
			'Deposit' AS 'TransactionType',
			GJ.Reference AS 'Reference',
			GJE.GeneralJournalentryId AS 'GeneralJournalEntryId',
			GJE.Debit AS 'Amount',
			GJE.Detail AS 'Description',
			GJ.TransactionDate AS 'TransactionDate',
			BA.AccountTitle AS 'AccountTitle',
			BA.BankAccountNumber AS 'AccountNumber',
			B.BankName AS 'BankName'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		WHERE GJ.EntryType='1' AND GJ.ReceiptType='0' AND GJ.IsDeleted='0' $sBankCondition $sReportDateTimeCriteria1 $sReportCriteriaSQL)

		UNION 
		(SELECT 
			'Withdrawal' AS 'TransactionType',
			GJ.Reference AS 'Reference',
			GJE.GeneralJournalentryId AS 'GeneralJournalEntryId',
			GJE.Credit AS 'Amount',
			GJE.Detail AS 'Description',
			GJ.TransactionDate AS 'TransactionDate',
			BA.AccountTitle AS 'AccountTitle',
			BA.BankAccountNumber AS 'AccountNumber',
			B.BankName AS 'BankName'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		WHERE GJ.EntryType='0' AND GJ.PaymentType='0' AND GJ.IsDeleted='0' $sBankCondition $sReportDateTimeCriteria2 $sReportCriteriaSQL)

		ORDER BY TransactionDate";
		
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		
		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="width:40px; font-weight:bold; font-size:12px;">S#</td>
	      <td align="left" style="width:120px; font-weight:bold; font-size:12px;">Date</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Reference No.</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Details</td>
	      <td align="right" style="width:100px; font-weight:bold; font-size:12px;">Debit</td>
		  <td align="right" style="width:100px; font-weight:bold; font-size:12px;">Credit</td>
	     </tr>
	     </thead>';		

		$dTotalCredit = 0; 
		$dTotalDebit = 0;
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTransactionType = $objDatabase->Result($varResult, $i, "TransactionType");
			$dTranactionDate = $objDatabase->Result($varResult, $i, "TransactionDate");
			$sDetails = $objDatabase->Result($varResult, $i, "Description");
			$dAmount = $objDatabase->Result($varResult, $i, "Amount");
			$sReference = $objDatabase->Result($varResult, $i, "Reference");
			
			$sBankName = $objDatabase->Result($varResult, $i, "BankName");
			$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
			$sBankAccountNumber = $objDatabase->Result($varResult, $i, "AccountNumber");
			
			if ($sTransactionType == "Deposit")
			{
				$sDebit = number_format($dAmount,0);
				$sCredit = "";
				
				$dTotalDebit += $dAmount;
			}
			else
			{
				$sDebit = "";
				$sCredit = number_format($dAmount,0);
				
				$dTotalCredit += $dAmount;
			}

			$sBankAccount = $sBankName . ' - ' . $sAccountTitle . ' - ' . $sBankAccountNumber;
			
			$dSubTotalDebit = $dTotalDebit;
			$dSubTotalCredit = $dTotalCredit;
				
			if (($i == 0) || ($iTemp != $iBankAccountId))
			{
				if ($i > 0)
				{
					$sReturn .= '<tr style="background-color:#f6f6f6;"><td style="font-family:Tahoma, Arial; font-size:13px;" colspan="6" align="right">Sub Total</td><td style="font-family:Tahoma, Arial; font-size:13px;" align="right">' . number_format($dSubTotalDebit, 0) . '</td><td style="font-family:Tahoma, Arial; font-size:13px;" align="right">' . number_format($dSubTotalCredit, 0) . '</td></tr>';
					$dSubTotalDebit = 0;
					$dSubTotalCredit = 0;
				}
				
				$sReturn .= '<tr style="background-color:#fff8cf;"><td style="font-family:Tahoma, Arial; font-size:14px;" colspan="7">' . $sBankAccount . '</td></tr>';
				$iTemp = $iBankAccountId;
			}
			
			$sReturn .= '<tr>
			 <td align="center" style="font-family:Tahoma, Arial; font-size:11px;">' . ($i+1) . '</td>
			 <td align="left" style="font-family:Tahoma, Arial; font-size:11px;">' . date("F j, Y", strtotime($dTranactionDate)) . '</td>
			 <td align="left" style="font-family:Tahoma, Arial; font-size:11px;">' . $sReference . '&nbsp;</td>
			 <td align="left" style="font-family:Tahoma, Arial; font-size:11px;">' . $sDetails . '&nbsp;</td>
			 <td align="right" style="font-family:Tahoma, Arial; font-size:11px;">' .  $sDebit . '</td>
			 <td align="right" style="font-family:Tahoma, Arial; font-size:11px;">' .  $sCredit . '</td>
			</tr>';
		}
			
		$sReturn .= '
		<tr style="background-color:#f6f6f6;"><td style="font-family:Tahoma, Arial; font-size:13px;" colspan="4" align="right">Sub Total</td><td style="font-family:Tahoma, Arial; font-size:13px;" align="right">' . number_format($dSubTotalDebit, 0) . '</td><td style="font-family:Tahoma, Arial; font-size:13px;" align="right">' . number_format($dSubTotalCredit, 0) . '</td></tr>
		<tr class="GridReport">
		 <td align="right" colspan="4" style="font-family:Tahoma, Arial; font-size:16px;">Grand Total:</td>
		 <td align="right" style="font-family:Tahoma, Arial; font-size:16px;">' . number_format($dTotalDebit, 0) . '</td>
		 <td align="right" style="font-family:Tahoma, Arial; font-size:16px;">' . number_format($dTotalCredit, 0) . '</td>
		</tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
		
	/* Bank Deposits Report */
	function GenerateBankDepositsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">Bank Deposits Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT * FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
		WHERE GJ.EntryType='1' AND GJ.ReceiptType='0' AND GJ.IsDeleted='0' $sReportDateTimeCriteria $sReportCriteriaSQL
		ORDER BY GJ.BankAccountId";		
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Date</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Reference No.</td>		  
		  <td align="left" style="font-weight:bold; font-size:12px;">Details</td>
	      <td align="right" style="font-weight:bold; font-size:12px;">Amount</td>
	     </tr>
	     </thead>';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			$dTotalAmount = 0;
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
				$sDescription = $objDatabase->Result($varResult, $i, "GJE.Detail");
				$sReferenceNumber = $objDatabase->Result($varResult, $i, "GJ.Reference");
				$iCurrentBankAccountId = $objDatabase->Result($varResult, $i, "BA.BankAccountId");
				
				$sBankName = $objDatabase->Result($varResult, $i, "B.BankName");
				$sBankName = mysql_escape_string($sBankName);
				$sBankName = stripslashes($sBankName);
				
				$sBranchName = $objDatabase->Result($varResult, $i, "BA.BranchName");
				$sBranchName = mysql_escape_string($sBranchName);
				$sBranchName = stripslashes($sBranchName);
				
				$sBankAccountNumber = $objDatabase->Result($varResult, $i, "BA.BankAccountNumber");
				$sBankAccountNumber = mysql_escape_string($sBankAccountNumber);
				$sBankAccountNumber = stripslashes($sBankAccountNumber);
				
				$dAmount = $objDatabase->Result($varResult, $i, "GJE.Debit");
				$sAmount = number_format($dAmount, 0);
				
				$dBankDepositDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
				$sBankDepositDate = date("F j, Y", strtotime($dBankDepositDate));
				
				$sBankAccount = $sBankName . ' - ' . $sAccountTitle . ' - ' . $sBankAccountNumber;
				
				// Grand Total
				$dTotalAmount += $dAmount;
				$dSubTotalAmount += $dAmount;
				
				if (($i == 0) || ($iTemp != $iBankAccountId))
				{
					if ($i > 0)
					{
						$sReturn .= '<tr style="background-color:#f6f6f6;"><td style="font-family:Tahoma, Arial; font-size:13px;" colspan="4" align="right">Sub Total</td><td style="font-family:Tahoma, Arial; font-size:13px;" align="right">' . number_format($dSubTotalAmount, 0) . '</td></tr>';
						$dSubTotalAmount = 0;
					}
					
					$sReturn .= '<tr style="background-color:#fff8cf;"><td style="font-family:Tahoma, Arial; font-size:14px;" colspan="5">' . $sBankAccount . '</td></tr>';
					$iTemp = $iBankAccountId;
				}
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sBankDepositDate . '</td>
				 <td align="left">' . $sReferenceNumber . '&nbsp;</td>				 
				 <td align="left">' . $sDescription . '&nbsp;</td>
				 <td align="right">' .  $sAmount . '</td>
				</tr>';
			}
			
			$sReturn .= '
			<tr style="background-color:#f6f6f6;"><td style="font-family:Tahoma, Arial; font-size:13px;" colspan="4" align="right">Sub Total</td><td style="font-family:Tahoma, Arial; font-size:13px;" align="right">' . number_format($dSubTotalAmount, 0) . '</td></tr>
			<tr class="GridReport">
			 <td align="right" colspan="4" style="font-family:Tahoma, Arial; font-size:16px;">Grand Total:</td>
			 <td align="right" style="font-family:Tahoma, Arial; font-size:16px;">' . number_format($dTotalAmount, 0) . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma, Arial; font-size:16px;" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
	/* Bank Withdrawals Report */
	function GenerateBankWithdrawalsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankWithdrawalsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">Bank Withdrawals Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT * FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
		WHERE GJ.EntryType='0' AND GJ.PaymentType='0'  AND GJ.IsDeleted='0' $sReportDateTimeCriteria $sReportCriteriaSQL
		ORDER BY GJ.BankAccountId";
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Date</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Reference No.</td>
		  <!--<td align="left" style="font-weight:bold; font-size:12px;">Method</td>-->
		  <td align="left" style="font-weight:bold; font-size:12px;">Details</td>
	      <td align="right" style="font-weight:bold; font-size:12px;">Amount</td>
	     </tr>
	     </thead>';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			$dTotalAmount = 0;
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
				$sDescription = $objDatabase->Result($varResult, $i, "GJE.Detail");
				$sReferenceNumber = $objDatabase->Result($varResult, $i, "GJ.Reference");
				$iCurrentBankAccountId = $objDatabase->Result($varResult, $i, "BA.BankAccountId");
				
				$sBankName = $objDatabase->Result($varResult, $i, "B.BankName");
				$sBankName = mysql_escape_string($sBankName);
				$sBankName = stripslashes($sBankName);
				
				$sBranchName = $objDatabase->Result($varResult, $i, "BA.BranchName");
				$sBranchName = mysql_escape_string($sBranchName);
				$sBranchName = stripslashes($sBranchName);
				
				$sBankAccountNumber = $objDatabase->Result($varResult, $i, "BA.BankAccountNumber");
				$sBankAccountNumber = mysql_escape_string($sBankAccountNumber);
				$sBankAccountNumber = stripslashes($sBankAccountNumber);
				
				$dAmount = $objDatabase->Result($varResult, $i, "GJE.Credit");
				$sAmount = number_format($dAmount, 0);
				$dBankWithdrawalDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
				$sBankWithdrawalDate = date("F j, Y", strtotime($dBankWithdrawalDate));
								
				$sBankAccount = $sBankName . ' - ' . $sAccountTitle . ' - ' . $sBankAccountNumber;
				
				// Grand Total
				$dTotalAmount += $dAmount;
				$dSubTotalAmount += $dAmount;
				
				if (($i == 0) || ($iTemp != $iBankAccountId))
				{
					if ($i > 0)
					{
						$sReturn .= '<tr style="background-color:#f6f6f6;"><td style="font-family:Tahoma, Arial; font-size:13px;" colspan="4" align="right">Sub Total</td><td style="font-family:Tahoma, Arial; font-size:13px;" align="right">' . number_format($dSubTotalAmount, 0) . '</td></tr>';
						$dSubTotalAmount = 0;
					}
					
					$sReturn .= '<tr style="background-color:#fff8cf;"><td style="font-family:Tahoma, Arial; font-size:14px;" colspan="5">' . $sBankAccount . '</td></tr>';
					$iTemp = $iBankAccountId;
				}
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sBankWithdrawalDate . '</td>
				 <td align="left">' . $sReferenceNumber . '&nbsp;</td>
				 <!--<td align="left">' . $this->aWithdrawalType[$iWithdrawalType] . '&nbsp;</td>-->
				 <td align="left">' . $sDescription . '&nbsp;</td>
				 <td align="right">' .  $sAmount . '</td>
				</tr>';
			}
			
			$sReturn .= '
			<tr style="background-color:#f6f6f6;"><td style="font-family:Tahoma, Arial; font-size:13px;" colspan="4" align="right">Sub Total</td><td style="font-family:Tahoma, Arial; font-size:13px;" align="right">' . number_format($dSubTotalAmount, 0) . '</td></tr>
			<tr class="GridReport">
			 <td align="right" colspan="4" style="font-family:Tahoma, Arial; font-size:16px;">Grand Total:</td>
			 <td align="right" style="font-family:Tahoma, Arial; font-size:16px;">' . number_format($dTotalAmount, 0) . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma, Arial; font-size:16px;" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
	/* Bank Reconciliation Report */
	function GenerateBankReconciliationReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankReconciliationReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dStartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dEndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		$iReconciliationStatus = $objGeneral->fnGet("selReconciliationStatus");
		if($iReconciliationStatus != -1) 
		{
			$sReconciliationStatusCondition = "AND GJE.ReconciliationStatus = '$iReconciliationStatus'"; 			
		}

		$sReturn = '<div align="center"><span class="ReportTitle">Bank Reconciliation Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$varResult = $objDatabase->Query("
		(SELECT 
			'Deposit' AS 'TransactionType',
			GJE.GeneralJournalentryId AS 'GeneralJournalEntryId',
			GJE.Debit AS 'Amount',
			GJE.Detail AS 'Description',
			GJ.TransactionDate AS 'TransactionDate',
			GJE.ReconciliationStatus AS 'ReconciliationStatus'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		WHERE GJ.EntryType='1' AND GJ.ReceiptType='0' AND GJ.IsDeleted='0' $sReconciliationStatusCondition $sReportCriteriaSQL AND (GJ.TransactionDate BETWEEN '$dStartDate' AND '$dEndDate'))

		UNION 
		(SELECT 
			'Withdrawal' AS 'TransactionType',
			GJE.GeneralJournalentryId AS 'GeneralJournalEntryId',
			GJE.Credit AS 'Amount',
			GJE.Detail AS 'Description',
			GJ.TransactionDate AS 'TransactionDate',
			GJE.ReconciliationStatus AS 'ReconciliationStatus'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		WHERE GJ.EntryType='0' AND GJ.PaymentType='0' AND GJ.IsDeleted='0' $sReconciliationStatusCondition $sReportCriteriaSQL AND (GJ.TransactionDate BETWEEN '$dStartDate' AND '$dEndDate'))
		
		ORDER BY TransactionDate");
		
		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr class="GridTR">
   	      <td align="center" width="3%"><span class="WhiteHeading">S#</span></td>		  
		  <td align="left"><span class="WhiteHeading">Date</span></td>
   	      <td align="left"><span class="WhiteHeading">Details</span></td>
   	      <td align="right" width="10%"><span class="WhiteHeading">Debit</span></td>
		  <td align="right" width="10%"><span class="WhiteHeading">Credit</span></td>
		  <td align="right" width="10%"><span class="WhiteHeading">Reconciliation Status</span></td>
   	     </tr>
	     </thead>';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			$dTotalAmount = 0;
			
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{				
				$sTransactionType = $objDatabase->Result($varResult, $i, "TransactionType");
				$dTranactionDate = $objDatabase->Result($varResult, $i, "TransactionDate");
				$sDetails = $objDatabase->Result($varResult, $i, "Description");
				$dAmount = $objDatabase->Result($varResult, $i, "Amount");
				$iReconciliationStatus = $objDatabase->Result($varResult, $i, "ReconciliationStatus");
				$sReconciliationStatus = (($iReconciliationStatus == 1) ? '<img src="../images/icons/tick.gif" title="" />' : '<img src="../images/icons/cross.gif" title="" />');
				
				if ($sTransactionType == "Deposit")
				{
					$dDebit = $dAmount;
					$dTotalDebit += $dAmount;
					$sDebit = number_format($dAmount,0);
					$sCredit = "";
				}
				else
				{
					$dCredit = $dAmount;
					$dtotalCredit += $dCredit;
					$sDebit = "";
					$sCredit = number_format($dAmount,0);
				}
				
				$sReturn .= '<tr>
				 <td valign="top" align="center">' . ($i+1) . '</td>				 
				 <td valign="top" align="left">' . date("F j, Y", strtotime($dTranactionDate)) . '</td>
				 <td valign="top">' . $sDetails . '</td>
				 <td valign="top" align="right">
				 ' . $sDebit . '
				 </td>
				 <td valign="top" align="right">
 				 ' . $sCredit . '
 				 </td>
				 <td valign="top">' . $sReconciliationStatus . '</td>
				</tr>';
			}
			
			$sReturn .= '			
			<tr class="GridReport">
			 <td align="right" colspan="3" style="font-family:Tahoma, Arial; font-size:16px;">Total:</td>
			 <td align="right" style="font-family:Tahoma, Arial; font-size:16px;">' . number_format($dTotalDebit, 0) . '</td>
			 <td align="right" style="font-family:Tahoma, Arial; font-size:16px;">' . number_format($dtotalCredit, 0) . '</td>
			 <td>&nbsp;</td>
			</tr>';
			
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma; font-size:16px;" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
	/* Bank Check Books Report */
	function GenerateBankCheckBooksReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankCheckBooksReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (BCB.BankCheckBookAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">Bank Check Books Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_banking_bankcheckbooks AS BCB
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = BCB.BankAccountId
		INNER JOIN organization_employees AS E ON E.EmployeeId = BCB.BankCheckBookAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Account Title </td>
          <td align="left" style="font-weight:bold; font-size:12px;">Account Number</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Check Book Prefix.</td>
	      <td align="center" style="font-weight:bold; font-size:12px;">Check Number Start</td>
		  <td align="center" style="font-weight:bold; font-size:12px;">Check Number End</td>
	     </tr>
	     </thead>';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iBankCheckBookId = $objDatabase->Result($varResult, $i, "BCB.BankCheckBookId");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "BA.AccountTitle");
				$sBankAccountNumber = $objDatabase->Result($varResult, $i, "BA.BankAccountNumber");				
				$sCheckPrefix = $objDatabase->Result($varResult, $i, "BCB.CheckPrefix");				
				$iCheckNumberStart = $objDatabase->Result($varResult, $i, "BCB.CheckNumberStart");				
				$iCheckNumberEnd = $objDatabase->Result($varResult, $i, "BCB.CheckNumberEnd");
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sAccountTitle . '</td>
				 <td align="left">' . $sBankAccountNumber . '</td>
				 <td align="left">' . $sCheckPrefix . '</td>
				 <td align="center">' . $iCheckNumberStart . '</td>
				 <td align="center">' . $iCheckNumberEnd . '&nbsp;</td>				 
				</tr>';
			}
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma; font-size:16px;" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	
	/* Bank Accounts Report */
	function GenerateBankAccountsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankAccountsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (BA.BankAccountAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">Bank Accounts Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_banking_bankaccounts AS BA
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		INNER JOIN organization_stations AS S ON S.Stationid = BA.StationId
		INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BankAccountAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";
				
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Bank </td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Branch </td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Branch Code</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Branch City</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Branch Contact No.</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Station </td>
          <td align="left" style="font-weight:bold; font-size:12px;">Account Title </td>
          <td align="left" style="font-weight:bold; font-size:12px;">Account Number</td>
		  <td align="right" style="font-weight:bold; font-size:12px;">Balance</td>
	     </tr>
	     </thead>';
		$dBalance = 0;
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iBankAccountId = $objDatabase->Result($varResult, $i, "BA.BankAccountId");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "BA.AccountTitle");
				$sBankAccountNumber = $objDatabase->Result($varResult, $i, "BA.BankAccountNumber");				
				$sBankName = $objDatabase->Result($varResult, $i, "B.BankName");
				$sBranchName = $objDatabase->Result($varResult, $i, "BA.BranchName");
				$sBranchCode = $objDatabase->Result($varResult, $i, "BA.BranchCode");
				$sBranchCity = $objDatabase->Result($varResult, $i, "BA.BranchCity");
				$sBranchPhoneNumber = $objDatabase->Result($varResult, $i, "BA.BranchPhoneNumber");
				$sStationName = $objDatabase->Result($varResult, $i, "S.StationName");
				// Get Bank Account Balances
				$varResult2 = $objDatabase->Query("
				SELECT					
					(SUM(GJE.Debit) - SUM(GJE.Credit)) AS 'Balance'

				FROM fms_banking_bankaccounts AS BA
				INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
				INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.ChartOfAccountsId = BA.ChartOfAccountsId
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
				WHERE BA.BankAccountId = '$iBankAccountId'");
				if ($objDatabase->RowsNumber($varResult) > 0)
				{
					$dBalance = $objDatabase->Result($varResult2, 0, "Balance");
				}
				
				$dTotalBalance += $dBalance;
				
				$sReturn .= '
				<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sBankName . '</td>
				 <td align="left">' . $sBranchName . '</td>
				 <td align="left">' . $sBranchCode . '</td>
				 <td align="left">' . $sBranchCity . '</td>
				 <td align="left">' . $sBranchPhoneNumber . '</td>
				 <td align="left">' . $sStationName . '</td>
				 <td align="left">' . $sAccountTitle . '</td>
				 <td align="left">' . $sBankAccountNumber . '</td>
				 <td align="right">' . number_format($dBalance, 2) . '</td>
				</tr>';
			}
			
			$sReturn .= '<tr>
			 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" colspan="9" align="right">Total</td>
			 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotalBalance, 0) . '</td>			 
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma; font-size:16px;" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
	/* Banks Report */
	function GenerateBanksReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (B.BankAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">Banks Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_banking_banks AS B
		INNER JOIN organization_employees AS E ON E.EmployeeId = B.BankAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";
		//die($sQuery);
		
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr bgcolor="#bfbfbf">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Bank </td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Bank Abbreviation</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Bank Status</td>
	     </tr>
	     </thead>';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {			
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iBankId = $objDatabase->Result($varResult, $i, "B.BankId");
				$sBankName = $objDatabase->Result($varResult, $i, "B.BankName");
				$sBankAbbreviation = $objDatabase->Result($varResult, $i, "B.BankAbbreviation");
				$iBankStatus = $objDatabase->Result($varResult, $i, "B.Status");
				if($iBankStatus==0) $sBankStatus = 'Inactive';
				if($iBankStatus==1) $sBankStatus = 'Active';
								
				$sReturn .= '
				<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sBankName . '</td>
				 <td align="left">' . $sBankAbbreviation . '&nbsp;</td>
				 <td align="left">' . $sBankStatus . '&nbsp;</td>
				</tr>';
			}
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma; font-size:16px;" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
		
	/* Cheque Drawn Statement */
	
	function GenerateChequeDrawnStatementReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		$iGeneralJournalId = $objGeneral->fnGet("txtId");

		$sQuery = "
		SELECT
			GJ.GeneralJournalId AS 'Id',
			GJ.TransactionDate AS 'TransactionDate',
			GJ.Reference AS 'Reference',
			B.BankName AS 'BankName',
			BA.BranchName AS 'BranchName',
			BA.BankAccountNumber AS 'BankAccountNumber',
			BA.AccountTitle AS 'AccountTitle',
			GJ.CheckNumber AS 'CheckNumber',
			GJE.Credit AS 'Credit',
			GJE.Detail AS 'Detail'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_banking_bankaccounts AS BA ON BA.BankAccountId = GJ.BankAccountId 
		INNER JOIN fms_banking_banks AS B ON B.BankId = BA.BankId
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId AND GJE.ChartOfAccountsId = BA.ChartOfAccountsId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy		
		WHERE 1=1 AND GJ.GeneralJournalId= '$iGeneralJournalId'  AND GJ.IsDeleted='0' ";
						
		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center" style="font-weight:bold; font-size:16px;">Sorry, No records found, Please Check Drawn Statement Id...</div><br /><br />');
			
		$iId = $objDatabase->Result($varResult, 0, "Id");
		$dTransactionDate = $objDatabase->Result($varResult, 0, "TransactionDate");
		$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
		$sReference = $objDatabase->Result($varResult, 0, "Reference");
		$sBankName = $objDatabase->Result($varResult, 0, "BankName");
		$sBranchName = $objDatabase->Result($varResult, 0, "BranchName");
		$sBankAccountNumber = $objDatabase->Result($varResult, 0, "BankAccountNumber");
		$sAccountTitle = $objDatabase->Result($varResult, 0, "AccountTitle");
		$sCheckNumber = $objDatabase->Result($varResult, 0, "CheckNumber");
		$dCredit = $objDatabase->Result($varResult, 0, "Credit");
		$sDetail = $objDatabase->Result($varResult, 0, "Detail");
		
		$sCredit = number_format($dCredit, 2);		
		$sCreditInWords = $objGeneral->NumberToWord($dCredit);				
		
		$sReturn = '<div align="center"><span class="ReportTitle">Cheque Drawn Statement</span></div>';
		
		$sReturn .= '<table cellspacing="0" cellpadding="5"  width="98%" align="center">		
		<tr>
		 <td width="20%" align="left" style="font-size:13px;">Bank</td>
		 <td width="80%" style="font-size:13px;">'. $sBankName . '&nbsp;</span></td>
		</tr>
		<tr>
		 <td width="20%" align="left" style="font-size:13px;">Account Number</td>
		 <td width="80%" style="font-size:13px;">'. $sBankAccountNumber. '&nbsp;</span></td>
		</tr>
		<tr>
		 <td width="20%" align="left" style="font-size:13px;">Date</td>
		 <td width="80%" style="font-size:13px;">'. $sTransactionDate. '&nbsp;</span></td>
		</tr>
		<tr>
		 <td width="20%" align="left" style="font-size:13px;">Amount' . $objEmployee->aSystemSettings['CurrencySign'] . '</td>
		 <td width="80%" style="font-size:13px;">'. $sCredit. '&nbsp;</span></td>
		</tr>
		<tr>
		 <td width="20%" align="left" style="font-size:13px;">' . $objEmployee->aSystemSettings['CurrencyName'] . '</td>
		 <td width="80%" style="font-size:13px;">'. $sCreditInWords. ' only&nbsp;</span></td>
		</tr>
		<tr>
		 <td width="20%" align="left" style="font-size:13px;">Purpose</td>
		 <td width="80%" style="font-size:13px;">'. $sDetail. '&nbsp;</span></td>
		</tr>
		</table><br />';
		
		$sReturn .= '
		<table style="border: solid 1px #cecece; font-family:Tahoma, Arial;" cellspacing="0" cellpadding="5" width="98%" align="center">
		<thead>
		<tr style="background-color:#e1e1e1;">			
		 <td align="center" style="border-right: solid 1px #cecece; color:#000000; font-size:12px;">S.No.</td>
		 <td align="center" width="80%" style="border-right: solid 1px #cecece; color:#000000; font-size:12px;">Details</td>
		 <td align="center" style="border-right: solid 1px #cecece; color:#000000; font-size:12px;">'. $objEmployee->aSystemSettings['CurrencyName']. '&nbsp;</td>
		</tr>
		</thead>';
		
		$dTotalDebit = 0;	
		$iMaxRows = 8;
		$iRowsNumber = $objDatabase->RowsNumber($varResult);
		if ($iRowsNumber > 0)
	    {
			for ($i=0; $i < $iRowsNumber; $i++)
			{	
				$sReturn .= '
				<tr>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="center">' . ($i + 1) . '&nbsp;</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">' . $sDetail . '&nbsp;</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="center">' . $sCredit . '</td>
				</tr>';
				
				// Last Row
				if (($i+1) == $iRowsNumber)
				{
					$k = $iMaxRows - ($i+1);
					for ($j=0; $j < $k; $j++)
					{
						$sReturn .= '<tr>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
					    </tr>';
					}
				}
			}
			
			$sReturn .= '
			<tr>				 
			 <td align="right" valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;font-weight:bold;">Total</td>
			 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;font-weight:bold;" align="left">' . $sCreditInWords . ' only &nbsp;</td>
			 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="center">' . $sCredit . '</td>
			</tr>
			</table>
			<br /><br /><br />
			<table cellspacing="0" cellpadding="5" width="98%" align="center">
			 <tr>
			  <td align="center" style="border-top: solid 1px #cecece;font-weight:bold; font-size:15px;">Prepared By</td>
			  <td>&nbsp;</td>
			  <td align="center" style="border-top: solid 1px #cecece;font-weight:bold; font-size:15px;">Checked By</td>
			  <td>&nbsp;</td>
			  <td align="center" style="border-top: solid 1px #cecece;font-weight:bold; font-size:15px;">Approved By</td>
			  <td>&nbsp;</td>
			  <td align="center" style="border-top: solid 1px #cecece; font-weight:bold; font-size:15px;">Received By</td>
			  <td>&nbsp;</td>
			 </tr>
			</table>';
	    }
		
		return($sReturn);
	}

}

?>