<?php

class clsFMS_Reports_BudgetReports extends clsFMS_Reports
{
	
	function __construct()
	{		

	}
	
	function ReportFilter(&$sReportCriteria, &$sReportCriteriaSQL)
	{
		global $objGeneral;
		global $objDatabase;
				
		// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		if ($iCriteria_EmployeeId == '') $iCriteria_EmployeeId = -1;
		if ($iCriteria_BankAccountId == '') $iCriteria_BankAccountId = -1;
		
		$sReportCriteriaSQL = ($iCriteria_EmployeeId != -1) ? " AND E.EmployeeId='$iCriteria_EmployeeId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_BankAccountId != -1) ? " AND BA.BankAccountId='$iCriteria_BankAccountId'" : '';
		
		// Date Range
		$sCriteria_DateRange = date("F j, Y", strtotime($dCriteria_StartDate)) . ' - ' . date("F j, Y", strtotime($dCriteria_EndDate));

		// Employees
		if ($iCriteria_EmployeeId == -1 || !$iCriteria_EmployeeId) $sCriteria_Employee = "All Employees";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId = '$iCriteria_EmployeeId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Id...');
			$sCriteria_Employee = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
		}

		// Bank Accounts
		if ( ($iCriteria_BankAccountId == -1) || !($iCriteria_BankAccountId)) $sCriteria_BankAccounts = "All Bank Accounts";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.BankAccountId = '$iCriteria_BankAccountId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Account Id...');
			$sCriteria_BankAccounts = $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");
		}		
		
		$sReportCriteria .= '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>
		 <fieldset><legend style="font-size:13px; font-weight:bold;">Report Criteria:</legend>
		  <table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		   <tr>
		    <td width="33%">Employee:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Employee . '</span></td>
			<td width="33%">Bank Account:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_BankAccounts . '</span></td>
		   </tr>		   		  
		   <tr>		    
		    <td>Date Range:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DateRange . '</span></td>
		   </tr>		   
		  </table>
		 </fieldset>
		 </td></tr>
		</table>';

		return(true);
	}
	
    function ShowBudgetReports($sReportName)
    {
    	global $objDatabase;
    	global $objEmployee;
    	$iEmployeeId = $objEmployee->iEmployeeId;
		
		/*
    	// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports[0] == 0)
    		return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Customer Reports</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';
		
		$sBudgetList = '<a ' . (($sReportName == "BudgetList") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/budgetreports_show.php?reportname=BudgetList"><img src="../images/budget/iconBudget.gif" border="0" alt="Budget List Report" title="Budget List Report" /><br />Budget List Report</a>';
		$sBudgetAllocation = '<a ' . (($sReportName == "BudgetAllocation") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/budgetreports_show.php?reportname=BudgetAllocation"><img src="../images/budget/iconBudget.gif" border="0" alt="Budget Allocation Report" title="Budget Allocation Report" /><br />Budget Allocation Report</a>';
				
		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
		 <tr>
		  <td width="10%" valign="top" align="center">' . $sBudgetList . '</td>
		  <td width="10%" valign="top" align="center">' . $sBudgetAllocation . '</td>
		 </tr>		 
		</table>
		<br />';

        $sReturn .= $this->ShowReportCriteria($sReportName);

		$sReturn .= '</td></tr></table></td></tr></table>';
		return($sReturn);
    }

    function ShowReportCriteria($sReportName)
    {			
    	switch($sReportName)
    	{   
			case "BudgetList": $sReturn = $this->BudgetListCriteria(); break;
			case "BudgetAllocation": $sReturn = $this->BudgetAllocationCriteria(); break;
    	}

    	$sReturn .= '
    	<script type="text/javascript" language="JavaScript">
		function GenerateReport(sReportName, sReportType)
		{
			var aReportCriteria;
			var sReportFilter;

			aReportCriteria = new Array();

			iEmployee = (GetVal("selEmployee") == "") ? -1 : GetVal("selEmployee");
			dDateRange_Start = (GetVal("txtDateRange_Start") == "") ? -1 : GetVal("txtDateRange_Start");
			dDateRange_End = (GetVal("txtDateRange_End") == "") ? -1 : GetVal("txtDateRange_End");

			sReportFilter = "&selEmployee=" + iEmployee;			
			sReportFilter += "&txtDateRangeStart=" + dDateRange_Start;
			sReportFilter += "&txtDateRangeEnd=" + dDateRange_End;
						
			jsOpenWindow("../reports/showreport.php?report=" + sReportName + "&reporttype=" + sReportType + sReportFilter, 800,600);
		}
		</script>';

    	return($sReturn);
    }    

	function BudgetListCriteria()
    {
		global $objEmployee;
				
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] == 0)
			//return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
							
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Budget List Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>			   
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'BudgetReports\', \'BudgetList\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
    
	function BudgetAllocationCriteria()
    {
		global $objEmployee;
				
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] == 0)
			//return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
							
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Budget Allocation Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>			   
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'BudgetReports\', \'BudgetAllocation\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }	
		
    function GenerateBudgetReports($sReportType, $bExportToExcel, &$aExcelData, $sAction = "")
    {			
    	$sReport = $this->GenerateReportHeader($sAction);
    	switch ($sReportType)
    	{   
			case "BudgetList": $sReport .= $this->GenerateBudgetListReport($bExportToExcel, $aExcelData); break;
			case "BudgetAllocation": $sReport .= $this->GenerateBudgetAllocationReport($bExportToExcel, $aExcelData); break;
    	}
    	//$sReport .= $this->GenerateReportFooter();
    	return($sReport);
    }   
	
	/* Budget List Report */
	function GenerateBudgetListReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BankDepositsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (B.BudgetStartDate >= '$dCriteria_StartDate' AND B.BudgetStartDate <= '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">Budget List Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_budget_budget AS B
		LEFT JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId = B.DonorProjectId
		INNER JOIN organization_employees AS E ON E.EmployeeId = B.BudgetAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Donor Project</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Title</td>
	      <td align="right" style="font-weight:bold; font-size:12px;">Amount</td>
		  <td align="right" style="font-weight:bold; font-size:12px;">Remaining Amount</td>
	      <td align="right" style="font-weight:bold; font-size:12px;">Remaining Allocated Amount</td>
	     </tr>
	     </thead>';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			$dTotalAmount = 0;
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iBudgetId = $objDatabase->Result($varResult, $i, "B.BudgetId");
				$sTitle = $objDatabase->Result($varResult, $i, "B.Title");
				$sProjectTitle = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
				$sProjectCode = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
				$sDonorProject = $sProjectTitle . ' ' . $sProjectCode;				
				$dAmount = $objDatabase->Result($varResult, $i, "B.Amount");
				$sAmount = number_format($dAmount, 2);
				$dRemainingAmount = $objDatabase->Result($varResult, $i, "B.RemainingAmount");
				$sRemainingAmount = number_format($dRemainingAmount, 2);
				$dRemainingAllocatedAmount = $objDatabase->Result($varResult, $i, "B.RemainingAllocatedAmount");
				$sRemainingAllocatedAmount = number_format($dRemainingAllocatedAmount, 2);
												
				// Grand Total
				$dTotalAmount += $dAmount;
				$dTotalRemainingAmount += $dRemainingAmount;
				$dTotalRemainingAllocatedAmount += $dRemainingAllocatedAmount;
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sDonorProject . '&nbsp;</td>
				 <td align="left">' . $sTitle . '&nbsp;</td>
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . $sAmount . '&nbsp;</td>
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign']. $sRemainingAmount . '&nbsp;</td>
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . $sRemainingAllocatedAmount . '&nbsp;</td>				 
				</tr>';
			}
			
			$sReturn .= '<tr class="GridReport">
			 <td align="right" colspan="3" style="font-weight:bold;">Total:</td>
			 <td align="right" style="font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . number_format($dTotalAmount,2) . '</td>
			 <td align="right" style="font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . number_format($dTotalRemainingAmount,2) . '</td>
			 <td align="right" style="font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . number_format($dTotalRemainingAllocatedAmount,2) . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="6" align="center" style="font-family:Tahoma; Font-size:16px;"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	
	/* Budget Allocation Report */
	function GenerateBudgetAllocationReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BudgetReports_BudgetAllocationReport[0] == 0)
			//return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (B.BudgetStartDate >= '$dCriteria_StartDate' AND B.BudgetStartDate <= '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">Budget Allocation Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_budget_budget AS B
		INNER JOIN fms_budget_budgetallocation AS BA ON BA.BudgetId = B.BudgetId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = BA.ChartOfAccountId
		LEFT JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId = B.DonorProjectId
		INNER JOIN organization_employees AS E ON E.EmployeeId = BA.BudgetAllocationAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL
		GROUP BY B.BudgetId, CA.ChartOfAccountsId";
		//die($sQuery);		
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			$dTotalAmount = 0;
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				
				$iBudgetId = $objDatabase->Result($varResult, $i, "B.BudgetId");
				$iChartOfAccountId = $objDatabase->Result($varResult, $i, "BA.ChartOfAccountId");				
				$sBudgetTitle = $objDatabase->Result($varResult, $i, "B.Title");
				$sProjectTitle = $objDatabase->Result($varResult, $i, "DP.ProjectTitle");
				$sProjectCode = $objDatabase->Result($varResult, $i, "DP.ProjectCode");
				$sDonorProject = $sProjectTitle . ' ' . $sProjectCode;
				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "CA.ChartOfAccountsCode");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "CA.AccountTitle");
				$sChartOfAccount = $sChartOfAccountsCode . ' - ' . $sAccountTitle;				
				$sTitle = $objDatabase->Result($varResult, $i, "BA.Title");
				$dAmount = $objDatabase->Result($varResult, $i, "BA.Amount");
				$sAmount = number_format($dAmount, 2);
				$dRemainingAmount = $objDatabase->Result($varResult, $i, "BA.RemainingAmount");
				$sRemainingAmount = number_format($dRemainingAmount, 2);
												
				// Grand Total
				$dTotalAmount += $dAmount;
				$dTotalRemainingAmount += $dRemainingAmount;
				$dTotalRemainingAllocatedAmount += $dRemainingAllocatedAmount;
				
				// Check for Account Head change				
				if(($iTemp_BudgetId != $iBudgetId && $i == 0))
				{
					$sReturn .='					
					<tr style="background-color:#e1e1e1;">
					 <td align="left" style="font-weight:bold; font-size:12px;">Budget </td>
					 <td align="center"><strong>' . $sBudgetTitle . '</strong></td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Donor Project</td>
					 <td align="center"><strong>' . $sDonorProject . '</strong></td>					 
					</tr>					
					<tr style="background-color:#e1e1e1;">
					 <td align="left" style="font-weight:bold; font-size:12px;">Chart Of Account </td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Title </td>					 
					 <td align="right" style="font-weight:bold; font-size:12px;">Amount</td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Remainig Amount</td>
					</tr>';
				}
				if(($iTemp_BudgetId != $iBudgetId && $i > 0))
				{
					$sReturn .='
					<tr class="GridReport">
					 <td align="right" colspan="2" style="font-weight:bold;">Total:</td>
					 <td align="right" style="font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . number_format($dTotalAmount,2) . '</td>
					 <td align="right" style="font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . number_format($dTotalRemainingAmount,2) . '</td>			 
					</tr>
					<tr style="background-color:#e1e1e1;">
					 <td align="left" style="font-weight:bold; font-size:12px;">Budget </td>
					 <td><strong>' . $sBudgetTitle . '</strong></td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Donor Project</td>
					 <td><strong>' . $sDonorProject . '</strong></td>					 
					</tr>					
					<tr style="background-color:#e1e1e1;">
					 <td align="left" style="font-weight:bold; font-size:12px;">Chart Of Account </td>
					 <td align="left" style="font-weight:bold; font-size:12px;">Title </td>					 
					 <td align="right" style="font-weight:bold; font-size:12px;">Amount</td>
					 <td align="right" style="font-weight:bold; font-size:12px;">Remainig Amount</td>
					</tr>';
				}
				
				$sReturn .= '<tr>				 
				 <td align="left">' . $sChartOfAccount . '&nbsp;</td>
				 <td align="left">' . $sTitle . '&nbsp;</td>
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . $sAmount . '&nbsp;</td>
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign']. $sRemainingAmount . '&nbsp;</td>				 
				</tr>';
				
				$iTemp_BudgetId = $iBudgetId;
				
			}
			
			$sReturn .= '<tr class="GridReport">
			 <td align="right" colspan="2" style="font-weight:bold;">Total:</td>
			 <td align="right" style="font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . number_format($dTotalAmount,2) . '</td>
			 <td align="right" style="font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . number_format($dTotalRemainingAmount,2) . '</td>			 
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="6" align="center" style="font-family:Tahoma; Font-size:16px;"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}	
	

}

?>