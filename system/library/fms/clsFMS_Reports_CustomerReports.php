<?php

class clsFMS_Reports_CustomerReports extends clsFMS_Reports
{
	public $aPaymentType;
	public $aInvoiceStatus;
	public $aQuotationStatus;
	public $aSalesOrderStatus;
	
	function __construct()
	{
		$this->aPaymentType = array("Paid By Check", "Paid By Cash", "Paid by Online Transfer");
		$this->aInvoiceStatus = array("Pending", "Outstanding", "Cleared", "Cancelled");
		$this->aQuotationStatus = array("Generated", "Processed", "Approved", "Rejected");
		$this->aSalesOrderStatus = array("Postponed", "Cancelled", "Approved");
	}
	
	
	function ReportFilter(&$sReportCriteria, &$sReportCriteriaSQL)
	{
		global $objGeneral;
		global $objDatabase;
				
		// Filters
		//$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		$iCriteria_CustomerId = $objGeneral->fnGet("selCustomer");
		$iCriteria_CategoryId = $objGeneral->fnGet("selCategory");
		$iCriteria_ProductId = $objGeneral->fnGet("selProduct");		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		//if ($iCriteria_EmployeeId == '') $iCriteria_EmployeeId = -1;
		if ($iCriteria_StationId == '') $iCriteria_StationId = -1;
		if ($iCriteria_CustomerId == '') $iCriteria_CustomerId = -1;
		if ($iCriteria_CategoryId == '') $iCriteria_CategoryId = -1;
		if ($iCriteria_ProductId == '') $iCriteria_ProductId = -1;
		
		
		//$sReportCriteriaSQL = ($iCriteria_EmployeeId != -1) ? " AND E.EmployeeId='$iCriteria_EmployeeId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_StationId != -1) ? " AND C.StationId='$iCriteria_StationId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_CustomerId != -1) ? " AND C.CustomerId='$iCriteria_CustomerId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_CategoryId != -1) ? " AND PC.CategoryId='$iCriteria_CategoryId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_ProductId != -1) ? " AND P.ProductId='$iCriteria_ProductId'" : '';	
		
		// Date Range
		$sCriteria_DateRange = date("F j, Y", strtotime($dCriteria_StartDate)) . ' - ' . date("F j, Y", strtotime($dCriteria_EndDate));

		// Employees
		if ($iCriteria_EmployeeId == -1 || !$iCriteria_EmployeeId) $sCriteria_Employee = "All Employees";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId = '$iCriteria_EmployeeId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Id...');
			$sCriteria_Employee = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
		}

		// Stations
		if ($iCriteria_StationId == -1 || !$iCriteria_StationId) $sCriteria_Station = "All Stations";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.StationId = '$iCriteria_StationId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Station Id...');
			$sCriteria_Station = $objDatabase->Result($varResult, 0, "S.StationName");
		}		
		// Customers
		if ($iCriteria_CustomerId == -1 || !$iCriteria_CustomerId) $sCriteria_Customer = "All Customer";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C WHERE C.CustomerId = '$iCriteria_CustomerId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Customer Id...');
			$sCriteria_Customer= $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName");
		}
		
		$sReportCriteria .= '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>
		 <fieldset><legend style="font-size:13px; font-weight:bold;">Report Criteria:</legend>
		  <table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		   <tr>		    
			<td width="33%">Customer:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Customer . '</span></td>
			<td>Date Range:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DateRange . '</span></td> 
		   </tr>		   
		  </table>
		 </fieldset>
		 </td></tr>
		</table>';

		return(true);
	}
	
    function ShowCustomerReports($sReportName)
    {
    	global $objDatabase;
    	global $objEmployee;
    	$iEmployeeId = $objEmployee->iEmployeeId;
		
		/*
    	// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_PosReports[0] == 0)
    		return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Customer Reports</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';
		
		$sCustomersList = '<a ' . (($sReportName == "Customers") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=Customers"><img src="../images/reports/iconCustomerReports_ListOfCustomers.gif" border="0" alt="Customers List" title="Customers List" /><br />Customers List</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersList[0] == 0)
			$sCustomersList = '<img src="../images/inventory/iconCustomers_disabled.gif" border="0" alt="Customers List" title="Customers List" /><br />Customers List';
		
		$sCustomersLedgerReport = '<a ' . (($sReportName == "CustomersLedger") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=CustomersLedger"><img src="../images/customers/iconQuotations.gif" border="0" alt="Customers Ledger Report" title="Customers Ledger Report" /><br />Customers Ledger Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersLedgerReport[0] == 0)
    		$sCustomersLedgerReport = '<img src="../images/vendors/iconCustomer_disabled.gif" border="0" alt="Customers Ledger Report" title="Customers Ledger Report" /><br />Customers Ledger Report';
				
		$sQuotation = '<a ' . (($sReportName == "Quotation") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=Quotation"><img src="../images/customers/iconQuotations.gif" border="0" alt="Quotation" title="Quotation" /><br />Quotation</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_QuotationsReport[0] == 0)
			$sQuotation = '<img src="../images/customers/iconQuotations_disabled.gif" border="0" alt="Quotation" title="Quotation" /><br />Quotation';
		
		$sSalesOrder = '<a ' . (($sReportName == "SalesOrder") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=SalesOrder"><img src="../images/customers/iconSalesOrders.gif" border="0" alt="Sales Order" title="Sales Order" /><br />Sales Order</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_SalesOrdersReport[0] == 0)
			$sSalesOrder = '<img src="../images/customers/iconSalesOrders_disabled.gif" border="0" alt="Sales Order" title="Sales Order" /><br />Sales Order';
		
		
		$sInvoice = '<a ' . (($sReportName == "Invoice") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=Invoice"><img src="../images/customers/iconInvoices.gif" border="0" alt="Invoice" title="Invoice" /><br />Invoice</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_InvoicesReport[0] == 0)
			$sInvoice = '<img src="../images/customers/iconInvoice_disabled.gif" border="0" alt="Invoice" title="Invoice" /><br />Invoice';
		
		$sReceipt = '<a ' . (($sReportName == "Receipt") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=Receipt"><img src="../images/customers/iconReceipts.gif" border="0" alt="Receipt" title="Receipt" /><br />Receipt</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_ReceiptsReport[0] == 0)
			$sReceipt = '<img src="../images/customers/iconReceipt_disabled.gif" border="0" alt="Receipt" title="Receipt" /><br />Receipt';
				
		$sListOfJobsReport = '<a ' . (($sReportName == "ListOfJobs") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=ListOfJobs"><img src="../images/customers/iconJobs.gif" border="0" alt="Invoice" title="List of Jobs" /><br />List of Jobs</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_ListOfJobsReport[0] == 0)
		//	$sListOfJobsReport = '<img src="../images/customers/iconJobs_disabled.gif" border="0" alt="List of Jobs" title="List of Jobs" /><br />List of Jobs';
		
		$sListOfInvoices = '<a ' . (($sReportName == "ListOfInvoices") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=ListOfInvoices"><img src="../images/reports/iconCustomerReports_ListOfInvoices.gif" border="0" alt="List of Invoices" title="List of Invoices" /><br />List of Invoices</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			//$sInvoice = '<img src="../images/customers/iconInvoice_disabled.gif" border="0" alt="List of Invoices" title="Invoice" /><br />List of Invoices';			
					
		$sListOfQuotationsReport = '<a ' . (($sReportName == "ListOfQuotations") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=ListOfQuotations"><img src="../images/customers/iconQuotations.gif" border="0" alt="List Of Quotations" title="List Of Quotations" /><br />List Of Quotations</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports[0] == 0)
			//$sQuotation = '<img src="../images/customers/iconQuotations.gif" border="0" alt="Quotation" title="Quotation" /><br />Quotation';
			
		
		$sListOfSalesOrdersReport = '<a ' . (($sReportName == "ListOfSalesOrders") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=ListOfSalesOrders"><img src="../images/customers/iconSalesOrders.gif" border="0" alt="List of Sales Orders" title="List of Sales Orders" /><br />List of Sales Orders</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			//$sSalesOrder = '<img src="../images/orders/iconSalesOrder_disabled.gif" border="0" alt="SalesOrder" title="SalesOrder" /><br />Job Posts Report';
				
		
		$sListOfReceiptsReport = '<a ' . (($sReportName == "ListOfReceipts") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=ListOfReceipts"><img src="../images/reports/iconCustomerReports_ListOfReceipts.gif" border="0" alt="List of Receipts" title="List of Receipts" /><br />List of Receipts</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports[0] == 0)
			//$sQuotation = '<img src="../images/customers/iconReceipt_disabled.gif" border="0" alt="Receipt" title="Receipt" /><br />Receipt';

		$sYearlyRecurringInvoicesReport = '<a ' . (($sReportName == "YearlyRecurringInvoices") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/customerreports_show.php?reportname=YearlyRecurringInvoices"><img src="../images/reports/iconCustomerReports_YearlyRecurringInvoices.gif" border="0" alt="Yearly Recurring Invoices Report" title="Yearly Recurring Invoices Report" /><br />Yearly Recurring Invoices Report</a>';
			
		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
		 <tr>
		  <td width="10%" valign="top" align="center">' . $sCustomersList . '</td>
		  <td width="10%" valign="top" align="center">' . $sCustomersLedgerReport . '</td>		  
		  <td width="10%" valign="top" align="center">' . $sQuotation . '</td>
		  <td width="10%" valign="top" align="center">' . $sSalesOrder . '</td>
		  <td width="10%" valign="top" align="center">' . $sInvoice . '</td>
		  <td width="10%" valign="top" align="center">' . $sReceipt . '</td>
		  <td width="10%" valign="top" align="center">' . $sListOfJobsReport . '</td>		  
		 </tr>
		 <tr>
		  <td width="10%" valign="top" align="center">' . $sListOfQuotationsReport . '</td>
		  <td width="10%" valign="top" align="center">' . $sListOfSalesOrdersReport . '</td>
		  <td width="10%" valign="top" align="center">' . $sListOfInvoices . '</td>
		  <td width="10%" valign="top" align="center">' . $sListOfReceiptsReport . '</td>		   
		  <td width="10%" valign="top" align="center">' . $sYearlyRecurringInvoicesReport . '</td>
		 </tr>
		</table>
		<br />';

        $sReturn .= $this->ShowReportCriteria($sReportName);

		$sReturn .= '</td></tr></table></td></tr></table>';
		return($sReturn);
    }

    function ShowReportCriteria($sReportName)
    {			
    	switch($sReportName)
    	{   
			case "Customers": $sReturn = $this->CustomerCriteria(); break;
			case "CustomersLedger": $sReturn = $this->CustomersLedgerCriteria(); break;
			case "Quotation": $sReturn = $this->QuotationCriteria(); break;
    		case "SalesOrder": $sReturn = $this->SalesOrderCriteria(); break;
			case "Invoice": $sReturn = $this->InvoiceCriteria(); break;
			case "Receipt": $sReturn = $this->ReceiptCriteria(); break;
			case "ListOfJobs": $sReturn = $this->ListOfJobsCriteria(); break;
			case "ListOfQuotations": $sReturn = $this->ListOfQuotationsCriteria(); break;
    		case "ListOfSalesOrders": $sReturn = $this->ListOfSalesOrdersCriteria(); break;
			case "ListOfReceipts": $sReturn = $this->ListOfReceiptsCriteria(); break;
			case "YearlyRecurringInvoices": $sReturn = $this->YearlyRecurringInvoicesCriteria(); break;
			case "ListOfInvoices": $sReturn = $this->ListOfInvoicesCriteria(); break;
    	}

    	$sReturn .= '
    	<script type="text/javascript" language="JavaScript">
		function GenerateReport(sReportName, sReportType)
		{
			var aReportCriteria;
			var sReportFilter;

			aReportCriteria = new Array();

			//iEmployee = (GetVal("selEmployee") == "") ? -1 : GetVal("selEmployee");
			iStation = (GetVal("selStation") == "") ? -1 : GetVal("selStation");
			iCustomer = (GetVal("selCustomer") == "") ? -1 : GetVal("selCustomer");
			
			dDateRange_Start = (GetVal("txtDateRange_Start") == "") ? -1 : GetVal("txtDateRange_Start");
			dDateRange_End = (GetVal("txtDateRange_End") == "") ? -1 : GetVal("txtDateRange_End");

			//sReportFilter = "&selEmployee=" + iEmployee;
			sReportFilter = "&selStation=" + iStation;
			sReportFilter += "&selCustomer=" + iCustomer;
			sReportFilter += "&txtDateRangeStart=" + dDateRange_Start;
			sReportFilter += "&txtDateRangeEnd=" + dDateRange_End;

			jsOpenWindow("../reports/showreport.php?report=" + sReportName + "&reporttype=" + sReportType + sReportFilter, 800,600);
		}
		
		function GenerateCustomerReport(sReportName, sReportType)
		{
			var aReportCriteria;
			var sReportFilter;

			aReportCriteria = new Array();

			//iEmployee = (GetVal("selEmployee") == "") ? -1 : GetVal("selEmployee");
			iCustomer = (GetVal("selCustomer") == "") ? -1 : GetVal("selCustomer");
			iStation = (GetVal("selStation") == "") ? -1 : GetVal("selStation");
			
			iId = (GetVal("txtId") == "") ? -1 : GetVal("txtId");
			
			dDateRange_Start = (GetVal("txtDateRange_Start") == "") ? -1 : GetVal("txtDateRange_Start");
			dDateRange_End = (GetVal("txtDateRange_End") == "") ? -1 : GetVal("txtDateRange_End");
			
			//sReportFilter = "&selEmployee=" + iEmployee;
			sReportFilter = "&selCustomer=" + iCustomer;
			sReportFilter += "&selStation=" + iStation;			
			sReportFilter += "&txtId=" + iId;
			sReportFilter += "&txtDateRangeStart=" + dDateRange_Start;
			sReportFilter += "&txtDateRangeEnd=" + dDateRange_End;
			
			if (sReportType == "ListOfInvoices") sReportFilter += "&status=" + GetVal("selInvoiceStatus");
			if (sReportType == "ListOfQuotations") sReportFilter += "&status=" + GetVal("selQuotationStatus");
			if (sReportType == "ListOfSalesOrders") sReportFilter += "&status=" + GetVal("selSalesOrderStatus");
				
			jsOpenWindow("../reports/showreport.php?report=" + sReportName + "&reporttype=" + sReportType + sReportFilter, 800,600);
		}
		</script>';

    	return($sReturn);
    }    

	function CustomerCriteria()
    {
		global $objEmployee;		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersList[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
					
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Customer Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'CustomerReports\', \'Customers\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
    function QuotationCriteria()
    {
		global $objEmployee;
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_QuotationsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Quotation Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
    	       <tr><td>Quotation Id:</td><td><input type="text" name="txtId" id="txtId" class="form1" value="" size="10" /></td></tr>    	       
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'Quotation\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function SalesOrderCriteria()
    {
		global $objEmployee;
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_SalesOrdersReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Sales Order Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
    	       <tr><td>Sales Order Id:</td><td><input type="text" name="txtId" id="txtId" class="form1" value="" size="10" /></td></tr>    	      
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'SalesOrder\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function InvoiceCriteria()
    {
		global $objEmployee;
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_InvoicesReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Invoice Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
    	       <tr><td>Incoice Id:</td><td><input type="text" name="txtId" id="txtId" class="form1" value="" size="10" /></td></tr>    	       
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'Invoice\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function ReceiptCriteria()
    {
		global $objEmployee;
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_ReceiptsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Receipt Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
    	       <tr><td>Receipt Id:</td><td><input type="text" name="txtId" id="txtId" class="form1" value="" size="10" /></td></tr>    	       
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'Receipt\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function ListOfJobsCriteria()
    {
		global $objEmployee;
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_ListOfJobsReport[0] == 0)
		//	return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">List of Jobs Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	         
			   <!--<tr><td>Employees:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>-->
			   <tr><td>Customers:</td><td>' . $this->GenerateReportCriteria("Customer", "selCustomer") . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'ListOfJobs\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function ListOfQuotationsCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$sQuotationStatusSelect = '<select class="Tahoma16" name="selQuotationStatus" id="selQuotationStatus">
		<option value="-1">Quotations with All Status</option>';
		for ($i=0; $i < count($this->aQuotationStatus); $i++)
			$sQuotationStatusSelect .= '<option value="' . $i . '">' . $this->aQuotationStatus[$i] . '</option>';
		$sQuotationStatusSelect .= '</select>';
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">List Of Quotations Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	         
			   <tr><td>Customers:</td><td>' . $this->GenerateReportCriteria("Customer", "selCustomer") . '</td></tr>
			   <tr><td>Status:</td><td>' . $sQuotationStatusSelect . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'ListOfQuotations\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function ListOfInvoicesCriteria()
    {
		global $objEmployee;
		
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$sInvoiceStatusSelect = '<select class="Tahoma16" name="selInvoiceStatus" id="selInvoiceStatus">
		<option value="-1">Invoices with All Status</option>';
		for ($i=0; $i < count($this->aInvoiceStatus); $i++)
			$sInvoiceStatusSelect .= '<option value="' . $i . '">' . $this->aInvoiceStatus[$i] . '</option>';
		$sInvoiceStatusSelect .= '</select>';
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">List of Invoices Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Customer:</td><td>' . $this->GenerateReportCriteria("Customer", "selCustomer") . '</td></tr>
			   <tr><td>Status:</td><td>' . $sInvoiceStatusSelect . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'ListOfInvoices\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function ListOfSalesOrdersCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$sSalesOrderStatusSelect = '<select class="Tahoma16" name="selSalesOrderStatus" id="selSalesOrderStatus">
		<option value="-1">Sales Orders with All Status</option>';
		for ($i=0; $i < count($this->aSalesOrderStatus); $i++)
			$sSalesOrderStatusSelect .= '<option value="' . $i . '">' . $this->aSalesOrderStatus[$i] . '</option>';
		$sSalesOrderStatusSelect .= '</select>';
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">List of Sales Orders Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	       
			   <tr><td>Customers:</td><td>' . $this->GenerateReportCriteria("Customer", "selCustomer") . '</td></tr>
			   <tr><td>Status:</td><td>' . $sSalesOrderStatusSelect . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'ListOfSalesOrders\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }

	function ListOfReceiptsCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">List of Receipts Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Customer:</td><td>' . $this->GenerateReportCriteria("Customer", "selCustomer") . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'ListOfReceipts\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function YearlyRecurringInvoicesCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Yearly Recurring Invoices Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Customer:</td><td>' . $this->GenerateReportCriteria("Customer", "selCustomer") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'YearlyRecurringInvoices\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function CustomersLedgerCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Customers Ledger Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">			   
			   <tr><td>Customer:</td><td>' . $this->GenerateReportCriteria("Customer", "selCustomer") . '</td></tr>
			   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange", 0) . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateCustomerReport(\'CustomerReports\', \'CustomersLedger\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
    function GenerateCustomerReports($sReportType, $bExportToExcel, &$aExcelData, $sAction = "")
    {
    	$sReport = $this->GenerateReportHeader($sAction);
					
    	switch ($sReportType)
    	{   
			case "Customers": $sReport .= $this->CustomersList($bExportToExcel, $aExcelData); break;
			case "CustomersLedger": $sReport .= $this->CustomersLedgerReport($bExportToExcel, $aExcelData); break;
			case "Quotation": $sReport .= $this->Quotation($bExportToExcel, $aExcelData); break;
			case "SalesOrder": $sReport .= $this->SalesOrder($bExportToExcel, $aExcelData); break;
			case "Invoice": $sReport .= $this->Invoice($bExportToExcel, $aExcelData); break;
			case "Receipt": $sReport .= $this->Receipt($bExportToExcel, $aExcelData); break;
			case "ListOfJobs": $sReport .= $this->ListOfJobsReport($bExportToExcel, $aExcelData); break;
			case "ListOfQuotations": $sReport .= $this->ListOfQuotationsReport($bExportToExcel, $aExcelData); break;
			case "ListOfSalesOrders": $sReport .= $this->ListOfSalesOrdersReport($bExportToExcel, $aExcelData); break;			
			case "ListOfInvoices": $sReport .= $this->ListOfInvoices($bExportToExcel, $aExcelData); break;
			case "ListOfReceipts": $sReport .= $this->ListOfReceiptsReport($bExportToExcel, $aExcelData); break;
			case "YearlyRecurringInvoices": $sReport .= $this->YearlyRecurringInvoicesReport($bExportToExcel, $aExcelData); break;
    	}

    	//$sReport .= $this->GenerateReportFooter();
    	return($sReport);
    }   
	
	function CustomersList($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersList[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (C.CustomerAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Customers List</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_customers AS C
		INNER JOIN organization_employees AS E ON E.EmployeeId = C.CustomerAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL
		ORDER BY C.FirstName";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Customer Name </td>
          <td align="left" style="font-weight:bold; font-size:12px;">Company Name</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Website</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Phone Number</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Mobile Number</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Email Address</td>	     
	     </tr>
	     </thead>';		
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iCustomerId = $objDatabase->Result($varResult, $i, "C.CustomerId");
				$sFirstName = $objDatabase->Result($varResult, $i, "C.FirstName");
				$sLastName = $objDatabase->Result($varResult, $i, "C.LastName");
				$sCustomerName = $sFirstName . ' ' . $sLastName;
				
				$sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");
				$sWebsite = $objDatabase->Result($varResult, $i, "C.CompanyWebsite");
				$sPhoneNumber = $objDatabase->Result($varResult, $i, "C.PhoneNumber");
				$sEmailAddress = $objDatabase->Result($varResult, $i, "C.EmailAddress");							
				$sMobileNumber = $objDatabase->Result($varResult, $i, "C.MobileNumber");
				if($sNICNumber == '') $sNICNumber = '&nbsp;';
				if($sCountry == '')	$sCountry = '&nbsp;';
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sCustomerName . '</td>
				 <td align="left">' . $sCompanyName . '</td>
				 <td align="left">' . $sWebsite . '</td>
				 <td align="left">' . $sPhoneNumber . '&nbsp;</td>
				 <td align="left">' . $sMobileNumber . '&nbsp;</td>
				 <td align="left">' . $sEmailAddress . '&nbsp;</td>
				</tr>';
			}
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	
	/* Customers Ledger Report */
	function CustomersLedgerReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">Customers Ledger Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
				
		// ChartOfAccountsCategoryId 5 = Equity
		$sQuery = "
		SELECT
		 C.FirstName AS 'FirstName',
		 C.LastName AS 'LastName',
		 C.NICNumber AS 'NICNumber',
		 C.CustomerId AS 'CustomerId',
		 C.Designation AS 'Designation',
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 CA.AccountTitle AS 'AccountTitle',
		 GJ.Reference AS 'Reference',
		 GJ.CheckNumber AS 'CheckNumber',
		 GJ.TransactionDate AS 'TransactionDate',
		 GJE.Detail AS 'Detail',
		 GJE.Debit AS 'Debit',
		 GJE.Credit AS 'Credit'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_customers AS C ON C.CustomerId = GJE.CustomerId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy		
		WHERE 1=1 AND CA.ChartOfAccountsCategoryId !='5' $sReportCriteriaSQL $sReportDateTimeCriteria
		ORDER BY C.CustomerId";
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		$iTempCustomerId = 0;
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$sCustomerName = $objDatabase->Result($varResult, $i, "FirstName") . ' ' . $objDatabase->Result($varResult, $i, "LastName");
				$sNICNumber = $objDatabase->Result($varResult, $i, "NICNumber");
				$sDesignation = $objDatabase->Result($varResult, $i, "Designation");
				$iCustomerId = $objDatabase->Result($varResult, $i, "CustomerId");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
				$sReference = $objDatabase->Result($varResult, $i, "Reference");
				$sCheckNumber = $objDatabase->Result($varResult, $i, "CheckNumber");
				$sCheckNumber = (($sCheckNumber != 0) ? $sCheckNumber : ' ');
				$dTransactionDate = $objDatabase->Result($varResult, $i, "TransactionDate");
				$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
				$sDetail = $objDatabase->Result($varResult, $i, "Detail");
				$dDebit = $objDatabase->Result($varResult, $i, "Debit");
				$dCredit = $objDatabase->Result($varResult, $i, "Credit");
				if($i == 0)
				{
					$sReturn .= '<br />
					<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
					<tr>
					 <td style="font-family:Tahoma, Arial; font-size:22px;" align="left" colspan="6">Ledger of - ' . $sCustomerName . '</td>
					</tr>				
					<tr>
					 <td align="center" style="border-bottom:1px solid; width:25px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">S#</td>
					 <td align="left" style="border-bottom:1px solid; width:120px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Date</td>
					 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Ref No.</td>
					 <td align="left" style="border-bottom:1px solid; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Description</td>
					 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Check No#</td>
					 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Debit</td>
					 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Credit</td>
					</tr>';
				}

				if ($i > 0 && $iTempCustomerId != $iCustomerId)
				{					
					$sReturn .= '<tr>
					 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" colspan="5" align="right">Total</td>
					 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Debit, 0) . '</td>
					 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Credit, 0) . '</td>					 
					</tr>';
					
					$sReturn .= '<br /><br /><div style="page-break-after:always"></div>';
					
					$sReturn .= '<br />
					<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
					<tr>
					 <td style="font-family:Tahoma, Arial; font-size:22px;" align="left" colspan="6">Ledger of - ' . $sVendorName . '</td>
					</tr>				
					<tr>
					 <td align="center" style="border-bottom:1px solid; width:25px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">S#</td>
					 <td align="left" style="border-bottom:1px solid; width:120px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Date</td>
					 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Ref No.</td>
					 <td align="left" style="border-bottom:1px solid; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Description</td>
					 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Check No#</td>
					 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Debit</td>
					 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Credit</td>
					</tr>';
					
					$dTotal_Debit = $dTotal_Credit = 0;
				}
					

				$dTotal_Debit += $dDebit;
				$dTotal_Credit += $dCredit;

				$sDebit = number_format($dDebit, 0);
				$sCredit = number_format($dCredit, 0);

				if ($dDebit == 0) $sDebit = '';
				if ($dCredit == 0) $sCredit = '';
				
				$iTempCustomerId = $iCustomerId;
				
				$sReturn .= '
				<tr>
				 <td align="center" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . ($i+1) . '&nbsp;</td>
				 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sTransactionDate. '&nbsp;</td>
				 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sReference . '&nbsp;</td>
				 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sDetail. '&nbsp;</td>
                 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sCheckNumber . '&nbsp;</td>
				 <td align="right" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sDebit. '&nbsp;</td>
				 <td align="right" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sCredit . '&nbsp;</td>				 
				</tr>';
			}

			$sReturn .= '<tr>
			 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" colspan="5" align="right">Total</td>
			 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Debit, 0) . '</td>
			 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Credit, 0) . '</td>			 
			</tr>';
			$sReturn .= '</table>';			
	    }

	    else
	    	$sReturn .= '<div align="center" style="font-family:Tahoma;font-size:16px;">No records found...</div>';
		return($sReturn);
	}
	
	function SalesOrder($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		//Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_SalesOrdersReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$iSalesOrderId = $objGeneral->fnGet("txtId");	
		
		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Sales Order </span></div>';

		$sQuery = "
		SELECT
		 *
		FROM fms_customers_salesorders AS SO
		INNER JOIN organization_employees AS E ON E.EmployeeId = SO.SalesOrderAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = SO.CustomerId
		INNER JOIN fms_customers_salesorders_items AS SOI ON SOI.SalesOrderId = SO.SalesOrderId				
		WHERE 1=1 AND SO.SalesOrderId = '$iSalesOrderId'";

		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found, Please check your Sales Order Id...</div><br /><br />');
			
		$iSalesOrderId = $objDatabase->Result($varResult, 0, "SO.SalesOrderId");
		$dSalesOrderDate = $objDatabase->Result($varResult, 0, "SO.SalesOrderDate");
		$sSalesOrderDate = date("F j, Y", strtotime($dSalesOrderDate));
		$dOrderTotal = $objDatabase->Result($varResult, 0, "SO.TotalAmount");
		//$dDiscountTotal = $objDatabase->Result($varResult, 0, "SO.DiscountTotal");
		$dTotalPayment = $objDatabase->Result($varResult, 0, "SO.TotalAmount");
		//$iPaymentType = $objDatabase->Result($varResult, 0, "SO.PaymentType");
		//$sPaymentType = $this->aPaymentType[$iPaymentType];
		
		$sCustomerName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName") ;
		$sMobileNumber = $objDatabase->Result($varResult, 0, "C.MobileNumber");
		$sAddress = $objDatabase->Result($varResult, 0, "C.Address");
		
		$sReturn .= '<table cellspacing="0" cellpadding="2" width="98%" align="center">
		 <tr>
		  <td width="60%" style="font-size:13px;">
		   Customer Name:&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;' . $sCustomerName . '&nbsp;</span>&nbsp;
		  </td>
		  <td align="right" style="font-size:13px;">Receipt No:&nbsp;&nbsp;<span style="font-size:16px; text-decoration:underline;">&nbsp;' . $iSalesOrderId . '&nbsp;</span></td>
		 </tr>
		 <tr>
		  <td style="font-size:13px;">
		   Contact Number:&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;' . $sMobileNumber . '&nbsp;</span>&nbsp;
		  </td>
 		  <td align="right" style="font-size:13px;">Date:&nbsp;&nbsp;<span style="font-size:16px; text-decoration:underline;">&nbsp;' . $sSalesOrderDate . '&nbsp;</span></td>
		 </tr>
		 <tr>
		  <td style="font-size:13px;" colspan="2" align="left">Address:&nbsp;&nbsp;<span style="text-decoration:underline;">' . $sAddress . '</span>&nbsp;</td>
		 </tr>
		 </table>';
				
		$sReturn .= '
		<table style="border: solid 1px #cecece;" cellspacing="0" cellpadding="8" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" width="1%" style="border-right: solid 1px #cecece; color:#000000; font-size:12px;">S#</td>
          <td align="left" style="border-right: solid 1px #cecece; color:#000000; font-size:12px;">Item Description</td>		   
	      <td align="right" style="border-right: solid 1px #cecece; color:#000000; font-size:12px;">Rate</td>
	      <td align="center" style="border-right: solid 1px #cecece; color:#000000; font-size:12px;">Qty</td>
	      <td align="right" style="color:#000000; font-size:12px;">Amount</td>	      
	     </tr>
	     </thead>';
		
		$iMaxRows = 8;
				
		$iRowsNumber = $objDatabase->RowsNumber($varResult);
		if ($iRowsNumber > 0)
	    {
			for ($i=0; $i < $iRowsNumber; $i++)
			{							
				$sProductName = $objDatabase->Result($varResult, $i, "SOI.ProductName");
				$iQuantity = $objDatabase->Result($varResult, $i, "SOI.Quantity");
				$dUnitPrice = $objDatabase->Result($varResult, $i, "SOI.UnitPrice");
				$dAmount = $objDatabase->Result($varResult, $i, "SOI.Amount");
				
				$sReturn .= '<tr>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="center">' . ($i+1) . $sExtraSpace . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">' . $sProductName . '&nbsp;</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="center">' . $iQuantity . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; font-size:13px;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>				 
				</tr>';
				
				// Last Row
				if (($i+1) == $iRowsNumber)
				{
					$k = $iMaxRows - ($i+1);
					for ($j=0; $j < $k; $j++)
					{
						$sReturn .= '<tr>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 0px #cecece; font-size:13px;">&nbsp;</td>						 
					    </tr>';
					}
				}
			}
			
			$sReturn .= '
			<tr bgcolor="#ffffff">
			 <td colspan="2" style="border-top: solid 1px #6f6f6f;">&nbsp;</td>
			 <td colspan="2" align="right" style="font-size:13px; border-top: solid 1px #6f6f6f;">Order Total:</td>
			 <td align="right" style="font-size:13px; border-left: solid 1px #cecece; border-top: solid 1px #6f6f6f;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dOrderTotal, 0) . '</td>
			</tr>			
			<tr bgcolor="#ffffff">
			 <td colspan="2" style="font-size:13px; border-top: solid 1px #cecece;">&nbsp;</td>
			 <td colspan="2" align="right" style="font-size:13px; font-weight:bold; border-top: solid 1px #cecece;">Total Payment:</td>
			 <td align="right" style="font-size:13px; font-weight:bold; border-left: solid 1px #cecece; border-top: solid 1px #cecece;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalPayment, 0) . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';
		
		$sReturn .= '</table>';

		return($sReturn);
	}
	
	function Quotation($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		//Emplyee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_QuotationsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
					
		$iQuotationId = $objGeneral->fnGet("txtId");
		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">QUOTATION</span></div>';

		$sQuery = "
		SELECT
		 *
		FROM fms_customers_quotations AS Q
		INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = Q.CustomerId
		INNER JOIN fms_customers_quotations_items AS QI ON QI.QuotationId = Q.QuotationId
		WHERE 1=1 AND Q.QuotationId= '$iQuotationId'";

		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found, Please check your Quotation Id...</div><br /><br />');
			
		$iQuotationId = $objDatabase->Result($varResult, 0, "Q.QuotationId");
		$dQuotationDate = $objDatabase->Result($varResult, 0, "Q.QuotationDate");
		$sQuotationDate = date("F j, Y", strtotime($dQuotationDate));
		$dOrderTotal = $objDatabase->Result($varResult, 0, "Q.TotalAmount");
		//$dDiscountTotal = $objDatabase->Result($varResult, 0, "SO.DiscountTotal");
		$dTotalPayment = $objDatabase->Result($varResult, 0, "Q.TotalAmount");
		//$iPaymentType = $objDatabase->Result($varResult, 0, "SO.PaymentType");
		//$sPaymentType = $this->aPaymentType[$iPaymentType];
		
		$sDescription = $objDatabase->Result($varResult, 0, "Q.Description");
		
		$sCustomerName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName") ;
		$sMobileNumber = $objDatabase->Result($varResult, 0, "C.MobileNumber");
		$sAddress = $objDatabase->Result($varResult, 0, "C.Address");
		
		$sReturn .= '<table cellspacing="0" cellpadding="2" width="98%" align="center">
		 <tr>
		  <td width="60%" style="font-size:13px;">
		   Customer Name:&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;' . $sCustomerName . '&nbsp;</span>&nbsp;
		  </td>
		  <td align="right" style="font-size:13px;">Quotation No:&nbsp;&nbsp;<span style="font-size:16px; text-decoration:underline;">&nbsp;' . $iQuotationId . '&nbsp;</span></td>
		 </tr>
		 <tr>
		  <td style="font-size:13px;">
		   Contact Number:&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;' . $sMobileNumber . '&nbsp;</span>&nbsp;
		  </td>
 		  <td align="right" style="font-size:13px;">Date:&nbsp;&nbsp;<span style="font-size:16px; text-decoration:underline;">&nbsp;' . $sQuotationDate . '&nbsp;</span></td>
		 </tr>
		 <tr>
		  <td style="font-size:13px;" colspan="2" align="left">Address:&nbsp;&nbsp;<span style="text-decoration:underline;">' . $sAddress . '</span>&nbsp;</td>
		 </tr>
		 </table>';
				
		$sReturn .= '
		<table style="border: solid 1px #cecece;" cellspacing="0" cellpadding="8" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" width="1%" style="width:20px; border-right: solid 1px #cecece; color:#000000; font-size:12px;">S#</td>
          <td align="left" style="border-right: solid 1px #cecece; color:#000000; font-size:12px;">Item Description</td>		   
	      <td align="right" style="width:80px; border-right: solid 1px #cecece; color:#000000; font-size:12px;">Rate</td>
	      <td align="center" style="width:50px; border-right: solid 1px #cecece; color:#000000; font-size:12px;">Qty</td>
	      <td align="right" style="width:80px; color:#000000; font-size:12px;">Amount</td>
	     </tr>
	     </thead>';
		
		$iMaxRows = 8;
				
		$iRowsNumber = $objDatabase->RowsNumber($varResult);
		if ($iRowsNumber > 0)
	    {
			for ($i=0; $i < $iRowsNumber; $i++)
			{							
				$sProductName = $objDatabase->Result($varResult, $i, "QI.ProductName");
				$iQuantity = $objDatabase->Result($varResult, $i, "QI.Quantity");
				$dUnitPrice = $objDatabase->Result($varResult, $i, "QI.UnitPrice");
				$dAmount = $objDatabase->Result($varResult, $i, "QI.Amount");
				
				$sReturn .= '<tr>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="center">' . ($i+1) . $sExtraSpace . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">' . $sProductName . '&nbsp;</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="center">' . $iQuantity . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; font-size:13px;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>				 
				</tr>';
				
				// Last Row
				if (($i+1) == $iRowsNumber)
				{
					$k = $iMaxRows - ($i+1);
					for ($j=0; $j < $k; $j++)
					{
						$sReturn .= '<tr>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 0px #cecece; font-size:13px;">&nbsp;</td>						 
					    </tr>';
					}
				}
			}
			
			$sReturn .= '
			<tr style="background-color:#e1e1e1;">
			 <td colspan="4" align="right" style="font-weight:bold; font-size:16px; border-top: solid 1px #6f6f6f;">Total Amount:</td>
			 <td align="right" style="font-weight:bold; font-size:16px; border-left: solid 1px #cecece; border-top: solid 1px #6f6f6f;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dOrderTotal, 0) . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';
		
		$sReturn .= '</table>
		<br />
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>' . $sDescription . '</td></tr></table>';

		return($sReturn);
	}
	
	function Invoice($bExportToExcel = false, &$aExcelData = '', $iInvoiceId = 0, $bInvoiceByJob = false)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($bInvoiceByJob == false)
		{
			//Employee Roles
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_InvoicesReport[0] == 0)
				return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		}
		
		if ($iInvoiceId == 0)
			$iInvoiceId = $objGeneral->fnGet("txtId");
		
		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">INVOICE</span></div>';

		$sQuery = "
		SELECT
		 *
		FROM fms_customers_invoices AS I
		INNER JOIN organization_employees AS E ON E.EmployeeId = I.InvoiceAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = I.CustomerId
		INNER JOIN fms_customers_invoices_items AS II ON II.InvoiceId = I.InvoiceId
		WHERE 1=1 AND ((I.InvoiceId = '$iInvoiceId') OR (I.InvoiceNo='$iInvoiceId'))";

		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found, Please check your Invoice Id...</div><br /><br />');
			
		$iInvoiceId = $objDatabase->Result($varResult, 0, "I.InvoiceId");
		$sInvoiceNo = $objDatabase->Result($varResult, 0, "I.InvoiceNo");
		$dInvoiceDate = $objDatabase->Result($varResult, 0, "I.InvoiceDate");
		$sInvoiceDate = date("F j, Y", strtotime($dInvoiceDate));
		$dOrderTotal = $objDatabase->Result($varResult, 0, "I.TotalAmount");
		//$dDiscountTotal = $objDatabase->Result($varResult, 0, "SO.DiscountTotal");
		$dTotalPayment = $objDatabase->Result($varResult, 0, "I.TotalPayment");
		//$iPaymentType = $objDatabase->Result($varResult, 0, "SO.PaymentType");
		//$sPaymentType = $this->aPaymentType[$iPaymentType];
		
		$sDescription = $objDatabase->Result($varResult, 0, "I.Description");
		
		$sCustomerName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName") ;
		$sCompanyName = $objDatabase->Result($varResult, 0, "C.CompanyName");
		$sAddress = $objDatabase->Result($varResult, 0, "C.Address");
		
		$sReturn .= '<table cellspacing="0" cellpadding="2" width="98%" align="center" style="font-family:Tahoma, Arial; font-size:11px;">
		 <tr>
		  <td width="60%" style="font-size:13px;">
		   Customer:&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;' . $sCustomerName . '&nbsp;</span>&nbsp;
		  </td>
		  <td align="right" style="font-size:13px;">Invoice No:&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;' . $sInvoiceNo . '&nbsp;</span></td>
		 </tr>
		 <tr>
		  <td style="font-size:13px;">
		   Company:&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;' . $sCompanyName . '&nbsp;</span>&nbsp;
		  </td>
 		  <td align="right" style="font-size:13px;">Date:&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;' . $sInvoiceDate . '&nbsp;</span></td>
		 </tr>
		 <tr>
		  <td style="font-size:13px;" colspan="2" align="left">Address:&nbsp;&nbsp;<span style="text-decoration:underline;">' . $sAddress . '</span>&nbsp;</td>
		 </tr>
		 </table>';
				
		$sReturn .= '
		<table style="border: solid 1px #cecece; font-family:Tahoma, Arial;" cellspacing="0" cellpadding="8" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" width="1%" style="width:20px; border-right: solid 1px #cecece; color:#000000; font-size:12px;">S#</td>
          <td align="left" style="border-right: solid 1px #cecece; color:#000000; font-size:12px;">Item Description</td>		   
	      <td align="right" style="width:80px; border-right: solid 1px #cecece; color:#000000; font-size:12px;">Rate</td>
	      <td align="center" style="width:50px; border-right: solid 1px #cecece; color:#000000; font-size:12px;">Qty</td>
	      <td align="right" style="width:100px; color:#000000; font-size:12px;">Amount</td>
	     </tr>
	     </thead>';
		
		$iMaxRows = 8;
				
		$iRowsNumber = $objDatabase->RowsNumber($varResult);
		if ($iRowsNumber > 0)
	    {
			for ($i=0; $i < $iRowsNumber; $i++)
			{							
				$sProductName = $objDatabase->Result($varResult, $i, "II.ProductName");
				$iQuantity = $objDatabase->Result($varResult, $i, "II.Quantity");
				$dUnitPrice = $objDatabase->Result($varResult, $i, "II.UnitPrice");
				$dAmount = $objDatabase->Result($varResult, $i, "II.Amount");
				
				$sReturn .= '<tr>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="center">' . ($i+1) . $sExtraSpace . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">' . $sProductName . '&nbsp;</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;" align="center">' . $iQuantity . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; font-size:13px;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>				 
				</tr>';
				
				// Last Row
				if (($i+1) == $iRowsNumber)
				{
					$k = $iMaxRows - ($i+1);
					for ($j=0; $j < $k; $j++)
					{
						$sReturn .= '<tr>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 0px #cecece; font-size:13px;">&nbsp;</td>						 
					    </tr>';
					}
				}
			}
			
			$sReturn .= '
			<tr style="background-color:#e1e1e1;">
			 <td colspan="4" align="right" style="font-weight:bold; font-size:16px; border-top: solid 1px #6f6f6f;">Total Amount:</td>
			 <td align="right" style="font-weight:bold; font-size:16px; border-left: solid 1px #cecece; border-top: solid 1px #6f6f6f;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dOrderTotal, 0) . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center" style="font-family:Tahoma, Arial; font-size:12px;">
		 <tr><td>' . $sDescription . '</td></tr>
		</table>';

		return($sReturn);
	}
	
	function Receipt($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		//Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_ReceiptsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$iReceiptId = $objGeneral->fnGet("txtId");		
		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">RECEIPT</span></div>';

		$sQuery = "
		SELECT
		 *
		FROM fms_customers_receipts AS R
		INNER JOIN organization_employees AS E ON E.EmployeeId = R.ReceiptAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = R.CustomerId
		INNER JOIN fms_customers_receipts_items AS RI ON RI.ReceiptId = R.ReceiptId
		WHERE 1=1 AND R.ReceiptId = '$iReceiptId'";

		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found, Please check your Receipt Id...</div><br /><br />');
			
		$iReceiptId = $objDatabase->Result($varResult, 0, "R.ReceiptId");
		$dPaymentDate = $objDatabase->Result($varResult, 0, "R.PaymentDate");
		$sPaymentDate = date("F j, Y", strtotime($dPaymentDate));
		
		$dReceiptDate = $objDatabase->Result($varResult, 0, "R.ReceiptDate");
		$sReceiptDate = date("F j, Y", strtotime($dReceiptDate));
		
		$dTotalAmount = $objDatabase->Result($varResult, 0, "R.TotalAmount");
		$dTotalPayment = $objDatabase->Result($varResult, 0, "R.TotalAmount");
		$iPaymentType = $objDatabase->Result($varResult, 0, "R.PaymentMethod");
		$sPaymentType = $this->aPaymentType[$iPaymentType];
		$sCompanyName = $objDatabase->Result($varResult, 0, "C.CompanyName");
		$sReceiptNo = $objDatabase->Result($varResult, 0, "R.ReceiptNo");
		
		$sDescription = $objDatabase->Result($varResult, 0, "R.Description");
				
		$sCustomerName = $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName") ;
		$sMobileNumber = $objDatabase->Result($varResult, 0, "C.MobileNumber");
		$sAddress = $objDatabase->Result($varResult, 0, "C.Address");
		
		$sReturn .= '<table cellspacing="0" cellpadding="2" width="98%" align="center">
		 <tr>
		  <td width="60%" style="font-size:13px;font-family:Tahoma, Arial; ">
		   Customer:&nbsp;&nbsp;<span style="font-family:Tahoma, Arial; text-decoration:underline;">&nbsp;' . $sCustomerName . '&nbsp;</span>&nbsp;
		  </td>
		  <td align="right" style="font-size:13px;font-family:Tahoma, Arial; ">Receipt No:&nbsp;&nbsp;<span style="font-family:Tahoma, Arial; text-decoration:underline;">&nbsp;' . $sReceiptNo . '&nbsp;</span></td>
		 </tr>
		 <tr>
		  <td style="font-size:13px;font-family:Tahoma, Arial; ">
		   Company:&nbsp;&nbsp;<span style="font-family:Tahoma, Arial; text-decoration:underline;">&nbsp;' . $sCompanyName . '&nbsp;</span>&nbsp;
		  </td>
 		  <td align="right" style="font-family:Tahoma, Arial; font-size:13px;">Date:&nbsp;&nbsp;<span style="font-family:Tahoma, Arial; text-decoration:underline;">&nbsp;' . $sReceiptDate . '&nbsp;</span></td>
		 </tr>
		 <tr>
		  <td style="font-family:Tahoma, Arial; font-size:13px;" colspan="2" align="left">Address:&nbsp;&nbsp;<span style="font-family:Tahoma, Arial; text-decoration:underline;">' . $sAddress . '</span>&nbsp;</td>
		 </tr>
		 </table>';
				
		$sReturn .= '
		<table style="border: solid 1px #cecece;" cellspacing="0" cellpadding="8" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" width="1%" style="border-right: solid 1px #cecece; color:#000000; font-family:Tahoma, Arial; font-size:12px;">S#</td>
          <td align="left" style="border-right: solid 1px #cecece; color:#000000; font-family:Tahoma, Arial; font-size:12px;">Item Description</td>		   
	      <td align="right" style="width:80px; border-right: solid 1px #cecece; color:#000000; font-family:Tahoma, Arial; font-size:12px;">Rate</td>
	      <td align="center" style="width:50px; border-right: solid 1px #cecece; color:#000000; font-family:Tahoma, Arial; font-size:12px;">Qty</td>
	      <td align="right" style="width:100px; color:#000000; font-family:Tahoma, Arial; font-size:12px;">Amount</td>
	     </tr>
	     </thead>';
		
		$iMaxRows = 8;
				
		$iRowsNumber = $objDatabase->RowsNumber($varResult);
		if ($iRowsNumber > 0)
	    {
			for ($i=0; $i < $iRowsNumber; $i++)
			{							
				$sProductName = $objDatabase->Result($varResult, $i, "RI.ProductName");
				$iQuantity = $objDatabase->Result($varResult, $i, "RI.Quantity");
				$dUnitPrice = $objDatabase->Result($varResult, $i, "RI.UnitPrice");
				$dAmount = $objDatabase->Result($varResult, $i, "RI.Amount");
				
				$sReturn .= '<tr>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-family:Tahoma, Arial; font-size:13px;" align="center">' . ($i+1) . $sExtraSpace . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-family:Tahoma, Arial; font-size:13px;">' . $sProductName . '&nbsp;</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-family:Tahoma, Arial; font-size:13px;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-family:Tahoma, Arial; font-size:13px;" align="center">' . $iQuantity . '</td>
				 <td valign="top" style="border-top: solid 1px #cecece; font-family:Tahoma, Arial; font-size:13px;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>
				</tr>';
				
				// Last Row
				if (($i+1) == $iRowsNumber)
				{
					$k = $iMaxRows - ($i+1);
					for ($j=0; $j < $k; $j++)
					{
						$sReturn .= '<tr>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 1px #cecece; font-size:13px;">&nbsp;</td>
						 <td style="border-top: solid 1px #cecece; border-right: solid 0px #cecece; font-size:13px;">&nbsp;</td>						 
					    </tr>';
					}
				}
			}
			
			$sReturn .= '
			<tr style="background-color:#e1e1e1;">
			 <td colspan="2" style="font-family:Tahoma, Arial; font-size:13px; border-top: solid 1px #6f6f6f;">Mode of Payment:&nbsp;&nbsp;' . $sPaymentType . '</td>
			 <td colspan="2" align="right" style="font-family:Tahoma, Arial; font-family:Tahoma, Arial; font-size:13px; font-weight:bold; border-top: solid 1px #6f6f6f;">Total Payment:</td>
			 <td align="right" style="font-family:Tahoma, Arial; font-size:13px; font-weight:bold; border-left: solid 1px #cecece; border-top: solid 1px #6f6f6f;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalPayment, 0) . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td style="font-size:12px;">' . $sDescription . '</td></tr>
		 <tr><td><br /><br /><br /><br /><br /><br /></td></tr>
		 <tr><td style="font-family:Tahoma, Arial; font-size:14px;" align="right">____________________<br />Stamp &amp; Signature&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
		</table>';

		return($sReturn);
	}	
	
	function ListOfJobsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_ListOfJobsReport[0] == 0)
		//	return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (J.JobAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">List of Jobs</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);		
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_customers_jobs AS J
		INNER JOIN organization_employees AS E ON E.EmployeeId = J.JobAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = J.CustomerId
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Title </td>
          <td align="left" style="font-weight:bold; font-size:12px;">Customer Name</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Supervisor</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Start Date</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">End Date</td>	      
	     </tr>
	     </thead>';

	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iJobId = $objDatabase->Result($varResult, $i, "J.JobId");
				$sFirstName = $objDatabase->Result($varResult, $i, "C.FirstName");
				$sLastName = $objDatabase->Result($varResult, $i, "C.LastName");
				$sCustomerName = $sFirstName . ' ' . $sLastName;
				$sTitle = $objDatabase->Result($varResult, $i, "J.Title");
				$sSupervisor = $objDatabase->Result($varResult, $i, "J.Supervisor");				
				$dStartDate = $objDatabase->Result($varResult, $i, "J.StartDate");
				$sStartDate = date("F j, Y", strtotime($dStartDate));
				$dEndDate = $objDatabase->Result($varResult, $i, "J.EndDate");
				$sEndDate = date("F j, Y", strtotime($dEndDate));
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sTitle . '</td>
				 <td align="left">' . $sCustomerName . '</td>
				 <td align="left">' . $sSupervisor . '</td>
				 <td align="left">' . $sStartDate . '</td>
				 <td align="left">' . $sEndDate . '&nbsp;</td>				 
				</tr>';
			}
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
		
	function ListOfSalesOrdersReport($bExportToExcel = false, &$aExcelData = '')
	{	
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$iSalesOrderStatus = $objGeneral->fnGet("status");
		if ($iSalesOrderStatus >= 0) $sStatusCondition = " AND S.Status='$iSalesOrderStatus'";
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria = " AND (S.SalesOrderDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";
		
		$sReturn .= '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">List of Sales Orders</span></div>';
		
		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
		
		$sQuery = "
		SELECT
		 *
		FROM fms_customers_salesorders AS S
		INNER JOIN organization_employees AS E ON E.EmployeeId = S.SalesOrderAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = S.CustomerId
		WHERE 1=1 $sReportDateTimeCriteria $sStatusCondition $sReportCriteriaSQL
		ORDER BY S.SalesOrderDate DESC";
		
		//die($sQuery);		
		$varResult = $objDatabase->Query($sQuery);
		
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found...</div><br /><br />');
		
		$sReturn .= '
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr style="background-color:#e1e1e1;">
		  <td align="center" style="width:20px; font-size:13px;">S#</td>
		  <td align="left" style="font-size:13px;">Date</td>
		  <td style="font-size:13px;">Customer Name</td>
		  <td style="font-size:13px;">Company Name</td>
		  <td align="left" style="font-size:13px;">Sales Order No</td>
		  <td align="left" style="font-size:13px;">Status</td>
		  <td align="right" style="font-size:13px;">Amount</td>
		 </tr>';
		$dGrandOrderTotal = 0;

		for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iSalesOrderId = $objDatabase->Result($varResult, $i, "S.SalesOrderId");
			$dSalesOrderDate = $objDatabase->Result($varResult, $i, "S.SalesOrderDate");
			$sSalesOrderDate = date("F j, Y", strtotime($dSalesOrderDate));
			$sSalesOrderNo = $objDatabase->Result($varResult, $i, "S.OrderNo");
			$iStatus = $objDatabase->Result($varResult, $i, "S.Status");
			$sStatus = $this->aSalesOrderStatus[$iStatus];
			$dOrderTotal = $objDatabase->Result($varResult, $i, "S.TotalAmount");
			//$dTotalPayment = $objDatabase->Result($varResult, $i, "Q.TotalPayment");
			
			$sCustomerName = $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") ;
			$sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");
			
			
			$dGrandOrderTotal += $dOrderTotal;
				
			$sReturn .= '
			<tr>
			 <td align="center">' . ($i+1) . '</td>
			 <td>' . $sSalesOrderDate . '</td>
			 <td>' . $sCustomerName . '</td>
			 <td>' . $sCompanyName . '</td>
			 <td>' . $sSalesOrderNo . '</td>
			 <td>' . $sStatus . '</td>
			 <td align="right">' . number_format($dOrderTotal, 0)  . '</td>
			</tr>';
			
		}
		$sReturn .= '
		<tr style="background-color:#e1e1e1;">
		 <td colspan="6" align="right" style="font-weight:bold; font-size:13px;">Total:</td>
		 <td align="right" style="font-weight:bold; font-size:13px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dGrandOrderTotal, 0) . '</td>
		</tr>';
					
		$sReturn .= '</table>';

		return($sReturn);
	}
	
	function ListOfQuotationsReport($bExportToExcel = false, &$aExcelData = '')
	{	
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$iQuotationStatus = $objGeneral->fnGet("status");
		if ($iQuotationStatus >= 0) $sStatusCondition = " AND Q.Status='$iQuotationStatus'";
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria = " AND (Q.QuotationDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";
		
		$sReturn .= '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">List of Quotations</span></div>';
		
		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
		
		$sQuery = "
		SELECT
		 *
		FROM fms_customers_quotations AS Q
		INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = Q.CustomerId
		WHERE 1=1 $sReportDateTimeCriteria $sStatusCondition $sReportCriteriaSQL
		ORDER BY Q.QuotationDate DESC";
		
		//die($sQuery);		
		$varResult = $objDatabase->Query($sQuery);
		
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found...</div><br /><br />');
		
		$sReturn .= '
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr style="background-color:#e1e1e1;">
		  <td align="center" style="width:20px; font-size:13px;">S#</td>
		  <td align="left" style="font-size:13px;">Date</td>
		  <td style="font-size:13px;">Customer Name</td>
		  <td style="font-size:13px;">Company Name</td>
		  <td align="left" style="font-size:13px;">Quotation No</td>
		  <td align="left" style="font-size:13px;">Status</td>
		  <td align="right" style="font-size:13px;">Amount</td>
		 </tr>';
		$dGrandOrderTotal = 0;

		for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iQuotationId = $objDatabase->Result($varResult, $i, "Q.QuotationId");
			$dQuotationDate = $objDatabase->Result($varResult, $i, "Q.QuotationDate");
			$sQuotationDate = date("F j, Y", strtotime($dQuotationDate));
			$sQuotationNo = $objDatabase->Result($varResult, $i, "Q.QuotationNo");
			$iStatus = $objDatabase->Result($varResult, $i, "Q.Status");
			$sStatus = $this->aQuotationStatus[$iStatus];
			$dOrderTotal = $objDatabase->Result($varResult, $i, "Q.TotalAmount");
			//$dTotalPayment = $objDatabase->Result($varResult, $i, "Q.TotalPayment");
			
			$sCustomerName = $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") ;
			$sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");
			
			
			$dGrandOrderTotal += $dOrderTotal;
				
			$sReturn .= '
			<tr>
			 <td align="center">' . ($i+1) . '</td>
			 <td>' . $sQuotationDate . '</td>
			 <td>' . $sCustomerName . '</td>
			 <td>' . $sCompanyName . '</td>
			 <td>' . $sQuotationNo . '</td>
			 <td>' . $sStatus . '</td>
			 <td align="right">' . number_format($dOrderTotal, 0)  . '</td>
			</tr>';
			
		}
		$sReturn .= '
		<tr style="background-color:#e1e1e1;">
		 <td colspan="6" align="right" style="font-weight:bold; font-size:13px;">Total:</td>
		 <td align="right" style="font-weight:bold; font-size:13px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dGrandOrderTotal, 0) . '</td>
		</tr>';
					
		$sReturn .= '</table>';

		return($sReturn);
	}
		
	function ListOfInvoices($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$iInvoiceStatus = $objGeneral->fnGet("status");
		if ($iInvoiceStatus >= 0) $sStatusCondition = " AND I.Status='$iInvoiceStatus'";
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria = " AND (I.InvoiceDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";
		
		$sReturn .= '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">List of Invoices</span></div>';
		
		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
		
		$sQuery = "
		SELECT
		 *
		FROM fms_customers_invoices AS I
		INNER JOIN organization_employees AS E ON E.EmployeeId = I.InvoiceAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = I.CustomerId
		WHERE 1=1 $sReportDateTimeCriteria $sStatusCondition $sReportCriteriaSQL
		ORDER BY I.InvoiceDate DESC";
		
		//die($sQuery);		
		$varResult = $objDatabase->Query($sQuery);
		
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found...</div><br /><br />');
		
		$sReturn .= '
		<table border="1" style="border: 1px solid #d7d7d7;" bordercolor="#cecece" cellspacing="0" cellpadding="5" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#d6e6ed;">
          <td align="center" style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">S#</td>
          <td style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Date</td>
          <td style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Customer Name</td>
          <td style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Company Name</td>
          <td style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Invoice No.</td>
          <td style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Status</td>
          <td align="right" style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Amount</td>
		 </tr>
		 </thead>';
		$dGrandOrderTotal = 0;
		
		for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iInvoiceId = $objDatabase->Result($varResult, $i, "I.InvoiceId");
			$dInvoiceDate = $objDatabase->Result($varResult, $i, "I.InvoiceDate");
			$sInvoiceDate = date("F j, Y", strtotime($dInvoiceDate));
			$sInvoiceNo = $objDatabase->Result($varResult, $i, "I.InvoiceNo");
			$iStatus = $objDatabase->Result($varResult, $i, "I.Status");
			$sStatus = $this->aInvoiceStatus[$iStatus];			
			$dOrderTotal = $objDatabase->Result($varResult, $i, "I.TotalAmount");
			$dTotalPayment = $objDatabase->Result($varResult, $i, "I.TotalPayment");
			
			$sCustomerName = $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") ;
			$sCompanyName = $objDatabase->Result($varResult, $i, "C.CompanyName");
			
			
			$dGrandOrderTotal += $dOrderTotal;
				
			$sReturn .= '
			<tr>
			 <td style="border: 1px solid #d7d7d7; font-size:12px;" align="center">' . ($i+1) . '</td>
			 <td style="border: 1px solid #d7d7d7; font-size:12px;">' . $sInvoiceDate . '</td>
			 <td style="border: 1px solid #d7d7d7; font-size:12px;">' . $sCustomerName . '</td>
			 <td style="border: 1px solid #d7d7d7; font-size:12px;">' . $sCompanyName . '</td>
			 <td style="border: 1px solid #d7d7d7; font-size:12px;">' . $sInvoiceNo . '</td>
			 <td style="border: 1px solid #d7d7d7; font-size:12px;">' . $sStatus . '</td>
			 <td style="border: 1px solid #d7d7d7; font-size:12px;" align="right">' . number_format($dOrderTotal, 0)  . '</td>
			</tr>';
			
		}
		$sReturn .= '
		<tr style="background-color:#d6e6ed;">
		 <td colspan="6" align="right" style="border: 1px solid #d7d7d7; font-size:12px;">Total:</td>
		 <td align="right" style="border: 1px solid #d7d7d7; font-size:12px;">' . number_format($dGrandOrderTotal, 0) . '</td>
		</tr>';
					
		$sReturn .= '</table>';

		return($sReturn);
	}

	function ListOfReceiptsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria = " AND (R.ReceiptDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";
		
		$sReturn .= '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">List of Receipts</span></div>';
		
		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
		
		$sQuery = "
		SELECT
		 *
		FROM fms_customers_receipts AS R
		INNER JOIN organization_employees AS E ON E.EmployeeId = R.ReceiptAddedBy
		INNER JOIN fms_customers AS C ON C.CustomerId = R.CustomerId
		LEFT  JOIN fms_customers_invoices AS I ON I.InvoiceId = R.InvoiceId
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL
		ORDER BY R.ReceiptDate DESC";
		
		//die($sQuery);
				
		$varResult = $objDatabase->Query($sQuery);
		
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found...</div><br /><br />');
		
		$sReturn .= '
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr style="background-color:#e1e1e1;">
		  <td align="center" style="width:30px; font-size:13px;">S#</td>
		  <td align="left" style="font-size:13px;">Date</td>
		  <td style="font-size:13px;">Customer Name</td>
		  <td style="font-size:13px;">Company Name</td>
		  <td align="left" style="font-size:13px;">Invoice No.</td>
		  <td align="left" style="font-size:13px;">Receipt No.</td>
		  <td align="left" style="font-size:13px;">Payment Method</td>
		  <td align="right" style="font-size:13px;">Amount</td>
		 </tr>';
		
		$dGrandOrderTotal = 0;
		
		for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iReceiptId = $objDatabase->Result($varResult, $i, "R.ReceiptId");
			$sReceiptNo = $objDatabase->Result($varResult, $i, "R.ReceiptNo");
			$sInvoiceNo = $objDatabase->Result($varResult, $i, "I.InvoiceNo");
			$dReceiptDate = $objDatabase->Result($varResult, $i, "R.ReceiptDate");
			$sReceiptDate = date("F j, Y", strtotime($dReceiptDate));
			$dOrderTotal = $objDatabase->Result($varResult, $i, "R.TotalAmount");
			
			$iPaymentMethod = $objDatabase->Result($varResult, $i, "R.PaymentMethod");
						
			$sCustomerName = $objDatabase->Result($varResult, $i, "C.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "C.LastName") ;
			$sCompanyname = $objDatabase->Result($varResult, $i, "C.CompanyName");
			
			$sPaymentMethod = ($iPaymentMethod == 1) ? 'Cash' : 'Check';
			
			$dGrandOrderTotal += $dOrderTotal;
								
			$sReturn .= '
			<tr>
			 <td align="center">' . ($i+1) . '</td>
			 <td>' . $sReceiptDate . '</td>
			 <td>' . $sCustomerName . '</td>
			 <td>' . $sCompanyname . '</td>
			 <td>' . $sInvoiceNo . '</td>
			 <td>' . $sReceiptNo . '</td>
			 <td>' . $sPaymentMethod . '</td>
			 <td align="right">' . number_format($dOrderTotal, 2)  . '</td>
			</tr>';
			
		}
		
		$sReturn .= '
		<tr style="background-color:#e1e1e1;">		 
		 <td colspan="7" align="right" style="font-weight:bold; font-size:13px;">Total:</td>
		 <td align="right" style="font-weight:bold; font-size:13px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dGrandOrderTotal, 0) . '</td>		 
		</tr>';
					
		$sReturn .= '</table>';

		return($sReturn);
	}

	function YearlyRecurringInvoicesReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
				
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_Reports_RecruitmentReports_JobPostsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$sReturn .= '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Yearly Recurring Invoices</span></div>';
		
		//$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		//$sReturn .= $sReportCriteria;
		
		$sQuery = "
		SELECT
		 C.FirstName AS 'CustomerFirstName',
		 C.LastName AS 'CustomerLastName',
		 C.CompanyName AS 'CustomerCompanyName',
		 RI.Title AS 'Title',
		
		 RI.InvoiceDateOfMonth AS 'DateOfMonth',
		
		 RI.InvoiceMonthOfYear_1 AS 'Month1',
		 RI.InvoiceMonthOfYear_2 AS 'Month2',
		 RI.InvoiceMonthOfYear_3 AS 'Month3',
		 RI.InvoiceMonthOfYear_4 AS 'Month4',
		 RI.InvoiceMonthOfYear_5 AS 'Month5',
		 RI.InvoiceMonthOfYear_6 AS 'Month6',
		 RI.InvoiceMonthOfYear_7 AS 'Month7',
		 RI.InvoiceMonthOfYear_8 AS 'Month8',
		 RI.InvoiceMonthOfYear_9 AS 'Month9',
		 RI.InvoiceMonthOfYear_10 AS 'Month10',
		 RI.InvoiceMonthOfYear_11 AS 'Month11',
		 RI.InvoiceMonthOfYear_12 AS 'Month12',
		
		 SUM(RIT.Amount) AS 'Amount'
		
		FROM fms_customers_invoices_recurringinvoices AS RI
		INNER JOIN fms_customers_invoices_recurringinvoices_items AS RIT ON RIT.RecurringInvoiceId = RI.RecurringInvoiceId
		INNER JOIN fms_customers AS C ON C.CustomerId = RI.CustomerId
		WHERE 1=1 AND RI.Status='1' $sReportCriteriaSQL
		GROUP BY RI.RecurringInvoiceId
		ORDER BY DateOfMonth, Amount DESC";
		
		$varResult = $objDatabase->Query($sQuery);
		
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found...</div><br /><br />');
		
		$sReturn .= '<br />
		<table border="1" style="border: 1px solid #d7d7d7;" bordercolor="#cecece" cellspacing="0" cellpadding="5" width="98%" align="center">
		 <thead>
	     <tr style="background-color:#d6e6ed;">
          <td align="center" style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">S#</td>
          <td style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Date</td>
          <td style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Customer Name</td>
          <td style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Company Name</td>
          <td style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Title</td>
          <td align="right" style="border: 1px solid #d7d7d7; font-weight:bold; font-size:12px;">Amount</td>
		 </tr>
		 </thead>';
		
		$iCounter = 0;
		for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTitle = $objDatabase->Result($varResult, $i, "RI.Title");

			$sCustomerName = $objDatabase->Result($varResult, $i, "CustomerFirstName") . ' ' . $objDatabase->Result($varResult, $i, "CustomerLastName") ;
			$sCompanyname = $objDatabase->Result($varResult, $i, "CustomerCompanyName");
			$dTotal = $objDatabase->Result($varResult, $i, "Amount");
			$iDateOfMonth = $objDatabase->Result($varResult, $i, "DateOfMonth");
			
			for ($j=0; $j < 12; $j++)
			{
				$aMonths[$j] = $objDatabase->Result($varResult, $i, "Month" . ($j+1));
				
				if ($aMonths[$j] == 1)
				{
					$aResult[$iCounter][0] = ($j+1);
					$aResult[$iCounter][1] = $iDateOfMonth;
					$aResult[$iCounter][2] = $sCustomerName;
					$aResult[$iCounter][3] = $sCompanyname;
					$aResult[$iCounter][4] = $sTitle;
					$aResult[$iCounter][5] = $dTotal;
					
					$iCounter++;
				}
			}
		}
		
		$aMonths = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		$iCounter = 1;

		$iLowestMonth = 0;
		$dLowestMonthAmount = 0;

		$iHighestMonth = 0;
		$dHighestMonthAmount = 0;
		
		for ($i=0; $i < 12; $i++)
		{
			$dMonthlyTotal = 0;
			$sReturn .= '<tr style="background-color:#fff8cf;"><td style="border: 1px solid #d7d7d7; font-size:18px; font-family:Tahoma, Arial;" colspan="6">' . $aMonths[$i] . '</td></tr>';
			for ($j=0; $j < count($aResult); $j++)
			{
				if ($aResult[$j][0] == ($i+1))
				{
					$sReturn .= '
					<tr>
					 <td style="border: 1px solid #d7d7d7; font-size:12px;" align="center">' . $iCounter++ . '</td>
					 <td style="border: 1px solid #d7d7d7; font-size:12px;">' . $aMonths[$i] . ' ' . $aResult[$j][1] . '</td>
					 <td style="border: 1px solid #d7d7d7; font-size:12px;">' . $aResult[$j][2] . '</td>
					 <td style="border: 1px solid #d7d7d7; font-size:12px;">' . $aResult[$j][3] . '</td>
					 <td style="border: 1px solid #d7d7d7; font-size:12px;">' . $aResult[$j][4] . '</td>
					 <td style="border: 1px solid #d7d7d7; font-size:12px;" align="right">' . number_format($aResult[$j][5], 0)  . '</td>
					</tr>';
					$dMonthlyTotal += $aResult[$j][5];
				}
			}

			if ($i == 0)
			{
				$iHighestMonth = $i; $iLowestMonth = $i;
				$dHighestMonthAmount = $dMonthlyTotal; $dLowestMonthAmount = $dMonthlyTotal;
			}
			else
			{
				if ($dLowestMonthAmount > $dMonthlyTotal)
				{
					$dLowestMonthAmount = $dMonthlyTotal; $iLowestMonth = $i; 
				}
				
				if ($dHighestMonthAmount < $dMonthlyTotal)
				{
					$dHighestMonthAmount = $dMonthlyTotal; $iHighestMonth = $i;
				}						
			}

			$dGrandTotal += $dMonthlyTotal;
			$sReturn .= '<tr style="background-color:#f6f6f6;"><td colspan="5" style="border: 1px solid #d7d7d7; font-weight:bold; font-family:Tahoma, Arial; font-size:11px;" align="right">Sub Total</td><td style="border: 1px solid #d7d7d7; font-size:11px; font-weight:bold; font-family:Tahoma, Arial;" align="right">' . number_format($dMonthlyTotal, 0) . '</td></tr>';
		}
		
		$dMonthlyAverage = $dGrandTotal / 12;
		
		$sReturn .= '
		<tr style="background-color:#d6e6ed;">
		 <td colspan="5" align="right" style="border: 1px solid #d7d7d7; font-family:Tahoma, Arial; font-weight:bold; font-size:16px;">Grand Total:</td>
		 <td align="right" style="border: 1px solid #d7d7d7; font-family:Tahoma, Arial; font-weight:bold; font-size:16px;">' . number_format($dGrandTotal, 0) . '</td>
		</tr>
		<tr>
		 <td colspan="6" style="border: 1px solid #d7d7d7; ">
		  <table border="0" cellspacing="0" cellpadding="5" style="width:400px;" align="left">
		   <tr><td style="font-family:Tahoma, Arial; font-size:13px;">Monthly Average:</td><td style="font-family:Tahoma, Arial; font-weight:bold; font-size:13px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dMonthlyAverage, 0) . '</td></tr>
		   <tr><td style="font-family:Tahoma, Arial; font-size:13px;">Lowest Month:</td><td style="font-family:Tahoma, Arial; font-weight:bold; font-size:13px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dLowestMonthAmount, 0) . ' (' . $aMonths[$iLowestMonth] . ')</td></tr>
		   <tr><td style="font-family:Tahoma, Arial; font-size:13px;">Highest Month:</td><td style="font-family:Tahoma, Arial; font-weight:bold; font-size:13px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dHighestMonthAmount, 0) . ' (' . $aMonths[$iHighestMonth] . ')</td></tr>
		 </td>
		</tr>
		
		</table>';

		return($sReturn);
	}
}

?>