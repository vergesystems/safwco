<?php

class clsFMS_Reports_FinancialStatements extends clsFMS_Reports
{
	public $aAccountType;	
	function __construct()
	{		
		$this->aAccountType = array('Payment', 'Receipt', 'Journal Entry');		
	}
	
	function ReportFilter(&$sReportCriteria, &$sReportCriteriaSQL)
	{
		global $objGeneral;
		global $objDatabase;
		global $objEmployee;
		$iEmployeeId = $objEmployee->iEmployeeId;
				
		// Filters
		//$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		$iCriteria_BankAccountId = $objGeneral->fnGet("selBankAccount");
		$iCriteria_DonorId = $objGeneral->fnGet("selDonor");
		$iCriteria_DonorProjectId = $objGeneral->fnGet("selDonorProject");
		$iCriteria_ProjectActivityId = $objGeneral->fnGet("selProjectActivity");
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		//if ($iCriteria_EmployeeId == '') $iCriteria_EmployeeId = -1;
		if ($iCriteria_BankAccountId == '') $iCriteria_BankAccountId = -1;
		if ($iCriteria_DonorId == '' || $iCriteria_DonorId == 0) $iCriteria_DonorId = -1;
		if ($iCriteria_DonorProjectId == '' || $iCriteria_DonorProjectId == '0') $iCriteria_DonorProjectId = -1;
		if ($iCriteria_ProjectActivityId == '' || $iCriteria_ProjectActivityId == '0') $iCriteria_ProjectActivityId= -1;
		if ($iCriteria_StationId == '') $iCriteria_StationId = -1;
		
		//$sReportCriteriaSQL = ($iCriteria_EmployeeId != -1) ? " AND E.EmployeeId='$iCriteria_EmployeeId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_DonorId != -1) ? " AND GJE.DonorId='$iCriteria_DonorId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_DonorProjectId != -1) ? " AND GJE.DonorProjectId='$iCriteria_DonorProjectId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_ProjectActivityId != -1) ? " AND GJE.DonorProjectActivityId='$iCriteria_ProjectActivityId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_BankAccountId != -1) ? " AND BA.BankAccountId='$iCriteria_BankAccountId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_StationId != -1) ? " AND GJE.StationId='$iCriteria_StationId'" : '';
		
		if($iCriteria_StationId == -1)
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_employees_stations AS ES WHERE ES.EmployeeId= '$iEmployeeId'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
				{
					$iNewStationId = $objDatabase->Result($varResult, $i, "ES.StationId");
					if($i == 0)	$sReportCriteriaSQL .= " AND ((GJE.StationId = '$iNewStationId')";
					else
						$sReportCriteriaSQL .= " OR (GJE.StationId = '" . $iNewStationId . "')";
				}
				
				$sReportCriteriaSQL .= ")";
			}
		}
		
		// Date Range
		$sCriteria_DateRange = date("F j, Y", strtotime($dCriteria_StartDate)) . ' - ' . date("F j, Y", strtotime($dCriteria_EndDate));

		// Employees
		if ($iCriteria_EmployeeId == -1 || !$iCriteria_EmployeeId) $sCriteria_Employee = "All Employees";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId = '$iCriteria_EmployeeId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Id...');
			$sCriteria_Employee = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
		}
		
		// Station
		if ($iCriteria_StationId == -1 || !$iCriteria_StationId) $sCriteria_Station = "All Stations";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.StationId = '$iCriteria_StationId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Station Id...');
			$sCriteria_Station = $objDatabase->Result($varResult, 0, "S.StationName");
		}

		// Bank Accounts
		if ( ($iCriteria_BankAccountId == -1) || !($iCriteria_BankAccountId)) $sCriteria_BankAccounts = "All Bank Accounts";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA WHERE BA.BankAccountId = '$iCriteria_BankAccountId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Bank Account Id...');
			$sCriteria_BankAccounts = $objDatabase->Result($varResult, 0, "BA.BankAccountNumber");
		}
		// Donors
		if ( ($iCriteria_DonorId == -1) || !($iCriteria_DonorId)) $sCriteria_Donors = "All Donors";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D WHERE D.DonorId = '$iCriteria_DonorId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Donor Id...');
			$sCriteria_Donors = $objDatabase->Result($varResult, 0, "D.DonorCode");
		}

		// Donor Projects
		if ( ($iCriteria_DonorProjectId == -1) || !($iCriteria_DonorProjectId )) $sCriteria_DonorProjects = "All Donor Projects";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.DonorProjectId= '$iCriteria_DonorProjectId '");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Donor Project Id...');
			$sCriteria_DonorProjects = $objDatabase->Result($varResult, 0, "DP.ProjectCode");
		}
		// Project Activities
		if ( ($iCriteria_ProjectActivityId == -1) || !($iCriteria_ProjectActivityId)) $sCriteria_ProjectActivities = "All Projects Activities";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects_activities AS PA WHERE PA.ActivityId= '$iCriteria_ProjectActivityId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Project Activity Id...');
			$sCriteria_ProjectActivities = $objDatabase->Result($varResult, 0, "PA.ActivityCode");
		}
		
		$sReportCriteria .= '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>
		 <fieldset><legend style="font-size:13px; font-weight:bold;">Report Criteria:</legend>
		  <table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		   <tr>
		    <td width="33%">Station:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Station . '</span></td>
			<td width="33%">Donor:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Donors . '</span></td> 
		   </tr>
		   <tr>
			<td width="33%">Donor Projects:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DonorProjects. '</span></td> 
			<td>Project Activities:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_ProjectActivities . '</span></td> 
		   </tr>
		   <tr>		    
		    <td>Date Range:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DateRange . '</span></td>
		   </tr>
		  </table>
		 </fieldset>
		 </td></tr>
		</table>';

		return(true);
	}
	
    function ShowFinancialStatements($sReportName)
    {
    	global $objDatabase;
    	global $objEmployee;
    	$iEmployeeId = $objEmployee->iEmployeeId;
		
		/*
    	// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports[0] == 0)
    		return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Financial Statements</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';
		
		$sIncomeStatement = '<a ' . (($sReportName == "IncomeStatement") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/financialstatements_show.php?reportname=IncomeStatement"><img src="../images/reports/iconFinancialStatements_IncomeStatement.gif" border="0" alt="Statement of Income & Expenditure" title="Statement of Income & Expenditure" /><br />Statement of Income & Expenditure</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountReports_ChartofAccountsReport[0] == 0)
			//$sIncomeStatements = '<img src="../images/accounts/iconChartOfAccounts_disabled.gif" border="0" alt="Income Statement" title="Income Statement" /><br />Income Statement';
			
		$sBalanceSheet = '<a ' . (($sReportName == "BalanceSheet") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/financialstatements_show.php?reportname=BalanceSheet"><img src="../images/reports/iconFinancialStatements_BalanceSheet.gif" border="0" alt="Balance Sheet" title="Balance Sheet" /><br />Balance Sheet</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountReports_ChartofAccountsReport[0] == 0)
			//$sBalanceSheet = '<img src="../images/accounts/iconChartOfAccounts_disabled.gif" border="0" alt="Balance Sheet" title="Balance Sheet" /><br />Balance Sheet';	
			
		$sTrialBalance = '<a ' . (($sReportName == "TrialBalance") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/financialstatements_show.php?reportname=TrialBalance"><img src="../images/reports/iconFinancialStatements_TrialBalance.gif" border="0" alt="Trial Balance" title="Trial Balance" /><br />Trial Balance</a>';
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountReports_ChartofAccountsReport[0] == 0)
			//$sTrialBalance = '<img src="../images/accounts/iconChartOfAccounts_disabled.gif" border="0" alt="Trial Balance" title="Trial Balance" /><br />Trial Balance';		
			
		$sChangeInEquity = '<a ' . (($sReportName == "ChangeInEquity") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/financialstatements_show.php?reportname=ChangeInEquity"><img src="../images/reports/iconFinancialStatements_ChangeInEquity.gif" border="0" alt="Statement of Changes in Funds" title="Statement of Changes in Funds" /><br />Statement of Changes in Funds</a>';
		
		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
		 <tr>
		  <td width="10%" valign="top" align="center">' . $sIncomeStatement . '</td>
		  <td width="10%" valign="top" align="center">' . $sBalanceSheet . '</td>
		  <td width="10%" valign="top" align="center">' . $sTrialBalance . '</td>
		  <td width="10%" valign="top" align="center">' . $sChangeInEquity . '</td>		  
		 </tr>		 
		</table>
		<br />';

        $sReturn .= $this->ShowReportCriteria($sReportName);

		$sReturn .= '</td></tr></table></td></tr></table>';
		return($sReturn);
    }

    function ShowReportCriteria($sReportName)
    {			
    	switch($sReportName)
    	{   
			case "IncomeStatement": $sReturn = $this->IncomeStatementCriteria(); break;
			case "BalanceSheet": $sReturn = $this->BalanceSheetCriteria(); break;
			case "TrialBalance": $sReturn = $this->TrialBalanceCriteria(); break;
			case "ChangeInEquity": $sReturn = $this->ChangeInEquityCriteria(); break;
    	}

    	$sReturn .= '
    	<script type="text/javascript" language="JavaScript">
		function GenerateReport(sReportName, sReportType)
		{
			var aReportCriteria;
			var sReportFilter;

			aReportCriteria = new Array();

			iEmployee = (GetVal("selEmployee") == "") ? -1 : GetVal("selEmployee");
			iDonor = (GetVal("selDonor") == "") ? -1 : GetVal("selDonor");
			iStation = (GetVal("selStation") == "") ? -1 : GetVal("selStation");
			iDonorProject = (GetVal("selDonorProject") == "") ? -1 : GetVal("selDonorProject");
			iProjectActivity = (GetVal("selProjectActivity") == "") ? -1 : GetVal("selProjectActivity");
			dDateRange_Start = (GetVal("txtDateRange_Start") == "") ? -1 : GetVal("txtDateRange_Start");
			dDateRange_End = (GetVal("txtDateRange_End") == "") ? -1 : GetVal("txtDateRange_End");

			sReportFilter = "&selStation=" + iStation;
			sReportFilter += "&selDonor=" + iDonor;
			sReportFilter += "&selDonorProject=" + iDonorProject;
			sReportFilter += "&selProjectActivity=" + iProjectActivity;
			sReportFilter += "&txtDateRangeStart=" + dDateRange_Start;
			sReportFilter += "&txtDateRangeEnd=" + dDateRange_End;

			if (sReportType == "IncomeStatement")
				sReportFilter += "&selComparison=" + GetSelectedListBox("selComparison") + "&txtFiscalYear=" + GetVal("txtFiscalYear");
			else if (sReportType == "BalanceSheet")
				sReportFilter += "&txtDate=" + GetVal("txtDate");
			//else if (sReportType == "ChangeInEquity")
			//	sReportFilter += "&txtYear=" + GetVal("txtYear");
				
			ReportOption_DonorLogo = (GetVal("selReportOption_DonorLogo") == "") ? -1 : GetVal("selReportOption_DonorLogo");
			sReportFilter += "&selReportOption_DonorLogo=" + ReportOption_DonorLogo;

			jsOpenWindow("../reports/showreport.php?report=" + sReportName + "&reporttype=" + sReportType + sReportFilter, 800,600);
		}
		</script>';

    	return($sReturn);
    }

	function IncomeStatementCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ChartOfAccountsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Statement of Income & Expenditure Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			  <tr><td class="Tahoma16" style="width:160px;">Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td class="Tahoma16" style="width:160px;">Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td class="Tahoma16">Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td class="Tahoma16">Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr><td class="Tahoma16">Comparison:</td><td><select onchange="Comparison(GetSelectedListBox(\'selComparison\'));" id="selComparison" name="selComparison" class="Tahoma16"><option value="0">No Comparison</option><option value="2">Two Years Comparison</option></select></td></tr>
			   <tr id="trDateRange"><td class="Tahoma16">Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr id="trFiscalYear" style="display:none;"><td class="Tahoma16">Fiscal Year:</td><td>' . $this->GenerateReportCriteria("Year", "txtFiscalYear") . '</td></tr>
			   <tr><td colspan="2"><br /><hr /><span class="Tahoma16">Report Options:</span></td></tr>
			   <tr><td class="Tahoma16">Donor Logo:</td><td>' . $this->GenerateReportCriteria("ReportOption_DonorLogo", "selReportOption_DonorLogo") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'FinancialStatements\', \'IncomeStatement\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>
		 <script type="text/javascript" language="JavaScript">
		 function Comparison(iComparison)
		 {
			if (iComparison == 0)
			{
				ShowDiv("trDateRange");
				HideDiv("trFiscalYear");
			}
			else if (iComparison == 2)
			{
				ShowDiv("trFiscalYear");
				HideDiv("trDateRange");
			}
		 }
		 </script>';

         return($sReturn);
    }

	function BalanceSheetCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ServicesReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Balance Sheet Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <tr><td class="Tahoma16" style="width:160px;">Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td class="Tahoma16" style="width:160px;">Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td class="Tahoma16">Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td class="Tahoma16">Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr><td class="Tahoma16">As Of:</td><td>' . $this->GenerateReportCriteria("Date", "txtDate") . '</td></tr>
			   <tr><td colspan="2"><br /><hr /><span class="Tahoma16">Report Options:</span></td></tr>
			   <tr><td class="Tahoma16">Donor Logo:</td><td>' . $this->GenerateReportCriteria("ReportOption_DonorLogo", "selReportOption_DonorLogo") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'FinancialStatements\', \'BalanceSheet\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }

	function TrialBalanceCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Trial Balance Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <tr><td class="Tahoma16" style="width:160px;">Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td class="Tahoma16" style="width:160px;">Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td class="Tahoma16">Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td class="Tahoma16">Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr><td class="Tahoma16">Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td colspan="2"><br /><hr /><span class="Tahoma16">Report Options:</span></td></tr>
			   <tr><td class="Tahoma16">Donor Logo:</td><td>' . $this->GenerateReportCriteria("ReportOption_DonorLogo", "selReportOption_DonorLogo") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'FinancialStatements\', \'TrialBalance\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function ChangeInEquityCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Statement of Changes in Funds Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <tr><td class="Tahoma16" style="width:160px;">Donor:</td><td>' . $this->GenerateReportCriteria("Donor", "selDonor") . '</td></tr>
			   <tr><td class="Tahoma16">Donor Project:</td><div id="divDonorProject" name="divDonorProject" style="display:none;"><td>' . $this->GenerateReportCriteria("DonorProject", "selDonorProject") . '</div></td></tr>
			   <tr><td class="Tahoma16">Project Activity:</td><div id="divProjectActivity" name="divProjectActivity" style="display:none;"><td>' . $this->GenerateReportCriteria("ProjectActivity", "selProjectActivity") . '</div></td></tr>
			   <tr id="trDateRange"><td class="Tahoma16">Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'FinancialStatements\', \'ChangeInEquity\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }

    function GenerateFinancialStatements($sReportType, $bExportToExcel, &$aExcelData, $sAction = "")
    {
    	$sReport = $this->GenerateReportHeader($sAction);
    	switch ($sReportType)
    	{
			case "IncomeStatement": $sReport .= $this->GenerateIncomeStatementReport($bExportToExcel, $aExcelData); break;
			case "BalanceSheet": $sReport .= $this->GenerateBalanceSheetReport($bExportToExcel, $aExcelData); break;
			case "TrialBalance": $sReport .= $this->GenerateTrialBalanceReport($bExportToExcel, $aExcelData); break;
			case "ChangeInEquity": $sReport .= $this->GenerateChangeInEquityReport($bExportToExcel, $aExcelData); break;
			case "FixedAssetManagement": $sReport .= $this->GenerateFixedAssetManagementReport($bExportToExcel, $aExcelData); break;
    	}
    	//$sReport .= $this->GenerateReportFooter();
    	return($sReport);
    }

	/* Income Statement*/
	
	function GenerateIncomeStatementReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

		//$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		//$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		$iDonorProjectId = $objGeneral->fnGet("selProject");
		$iDonorProjectId = $objGeneral->fnGet("selProject");

		$iComparison = $objGeneral->fnGet("selComparison");

		if($iDonorProjectId > 0) $sProjectCondition = "AND (GJE.DonorProjectId = '$iDonorProjectId')";

		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Statement of Income & Expenditure</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		/*
		$dFiscalYearStartDate = $objEmployee->aSystemSettings['VF_FiscalYearStartDate'];
		$dFiscalYearEndDate = $objEmployee->aSystemSettings['VF_FiscalYearEndDate'];


		$dFirscalYearStartDate = "01-01"; // 07-01
		$dFiscalYearEndDate = "12-31";   // 06-30

		$dFirscalYearStartDate = "07-01";
		$dFiscalYearEndDate = "06-30";

		if (substr($dFiscalYearStartDate, 0, 2) <= date("m"))
		{
			die("X");
			if (substr($dFiscalYearStartDate, 0, 2) < 1)
				$dYear1_Start = date("Y") . '-' . $dFiscalYearStartDate;
			else
				$dYear1_Start = date("Y")-1 . '-' . $dFiscalYearStartDate;
		}
		else
			$dYear1_Start = date("Y") . '-' . $dFirscalYearStartDate;


		//$dYear1_Start = date("Y") . '-' . $dFiscalYearStartDate;
		//$dYear1_End = date("Y") . '-' . $dFiscalYearEndDate;

		die(">>" . $dYear1_Start . ' - - ' . $dYear1_End);
		*/

		// This Year
		//$dYear1_Start = date("Y-01-01");
		//$dYear1_End = date("Y-12-31");

		// Last Year
		//$dYear2_Start = date("Y-01-01", mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-1));
		//$dYear2_End = date("Y-12-31", mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-1));

		if ($iComparison == 0) // No Comparison
		{
			$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
			$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

			$dYear1_Start = $dCriteria_StartDate;
			$dYear1_End = $dCriteria_EndDate;
			
			//Deferred Capital Grant		
			
			// Only Get Income & Expenses
			$sQuery = "
			SELECT
			 CCAT.CategoryCode AS 'CategoryCode',
			 CCAT.CategoryName AS 'CategoryName',
			 CC.ControlCode AS 'ControlCode',
			 CC.ControlName AS 'ControlName',

			 CA.ChartOfAccountsCode AS 'AccountCode',
			 CA.AccountTitle AS 'AccountTitle',
			 CA.ChartOfAccountsId AS 'ChartOfAccountsId',

			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear1_Start' AND '$dYear1_End', GJE.Debit, 0)) AS 'Year1_Debit',
			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear1_Start' AND '$dYear1_End', GJE.Credit, 0)) AS 'Year1_Credit',

			 CA.ChartOfAccountsCategoryId AS 'CategoryId',
			 CA.ChartOfAccountsControlId AS 'ControlId',
			 CA.ChartOfAccountsId AS 'ChartOfAccountsId'

			FROM fms_accounts_chartofaccounts AS CA
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
			INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
			LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
			LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId
			WHERE (CA.ChartOfAccountsCategoryId='5' OR CA.ChartOfAccountsCategoryId='4') AND GJ.IsDeleted ='0' $sProjectCondition $sReportCriteriaSQL
			GROUP BY CA.ChartOfAccountsCategoryId, CA.ChartOfAccountsControlId, CA.ChartOfAccountsId
			ORDER BY CA.ChartOfAccountsCategoryId DESC";
			
			//GROUP BY CA.ChartOfAccountsCategoryId, CA.ChartOfAccountsControlId, CA.ChartOfAccountsId
			// Category Id = 3 Income & Category Id = 4 Expense
			
			//die($sQuery);
			$varResult = $objDatabase->Query($sQuery);

			$sReturn .= '<br />
			<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
			 <tr>
			  <td style="width:10px;"></td>
			  <td style="width:10px;"></td><td></td>
			  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right"></td>
			 </tr>';

			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
				$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");
				$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");

				$iControlId = $objDatabase->Result($varResult, $i, "ControlId");
				$sControlCode = $objDatabase->Result($varResult, $i, "ControlCode");
				$sControlName = $objDatabase->Result($varResult, $i, "ControlName");

				$sAccountCode = $objDatabase->Result($varResult, $i, "AccountCode");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");

				$dYear1_Debit = $objDatabase->Result($varResult, $i, "Year1_Debit");
				$dYear1_Credit = $objDatabase->Result($varResult, $i, "Year1_Credit");
				
				//$dYear1_Balance = ($dYear1_Debit > 0) ? $dYear1_Debit : $dYear1_Credit;
				$dYear1_Balance = ($dYear1_Debit > 0) ? ($dYear1_Debit - $dYear1_Credit) : ($dYear1_Credit - $dYear1_Debit);
				
				//dont display 0 values
				if(($dYear1_Debit == 0) && ($dYear1_Credit == 0))
					continue;
				
				if (($i == 0) || ($iTemp != $iCategoryId))
				{
					if (($i > 0) && ($iTemp != $iCategoryId))
					{
						if($dTotal_Year1 != 0)
							$sReturn .= '<tr>
							 <td></td>
							 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total  ' . $sCategoryNameTemp . '</td>
							 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
							</tr>';

						$dTotalIncome_Year1 = $dTotal_Year1;
						$dTotal_Year1 = 0;
					}

					$sCategoryNameTemp = $sCategoryName;

					$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:18px;">' . (($i > 0) ? '<br />' : '') . strtoupper($sCategoryName) . '</td></tr>';
					$iTemp = $iCategoryId;
				}
				
				if (($i == 0) || ($iTemp2 != $iControlId))
				{
					$sReturn .= '<tr><td>&nbsp;</td><td align="left" colspan="3" style="font-family:Tahoma, Arial;font-size:16px;"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=-1&selStation=-1&selDonorProject=' . $iDonorProjectId . '&selChartOfAccountCategory=' . $iCategoryId . '&selChartOfAccountControl=' . $iControlId . '\', 800,600);" />' . $sControlName . '</a></td></tr>';
					$iTemp2 = $iControlId;
				}
				
				//if ($dYear1_Balance != 0)
				//{
					$sReturn .= '
					<tr>
					 <td></td>
					 <td></td>
					 <td align="left" style="font-size:12px;font-family:Tahoma, Arial;"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=-1&selStation=-1&selChartOfAccountCategory=' . $iCategoryId . '&selChartOfAccountControl=' . $iControlId . '&txtDateRangeStart='.$dCriteria_StartDate . '&txtDateRangeEnd='. $dCriteria_EndDate . '&selChartOfAccount=' . $iChartOfAccountsId. '\', 800,600);" />' . $sAccountCode . ' - ' . $sAccountTitle . '</td>
					 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear1_Balance, 0) . '</td>
					</tr>';

					$dTotal_Year1 += $dYear1_Balance;
				//}
			}
			
			$dTotalExpense_Year1 = $dTotal_Year1;

			$sReturn .= '<tr>
			 <td></td>
			 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryNameTemp . '</td>
			 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
			</tr>';

			$sReturn .= '<tr><td><br /></td></tr>
			<tr>
			 <td style="font-family:Tahoma, Arial;font-size:18px;" align="left" colspan="3">Operating Result (Surplus/Deficit): </td>
			 <td style="border-bottom:4px double; font-family:Tahoma, Arial;font-size:18px;" align="right">' . number_format(($dTotalIncome_Year1 - $dTotalExpense_Year1), 0) . '</td>
			</tr>';

			$sReturn .= '</table>';
		}
		else if ($iComparison == 2) // 2 Years Comparison
		{
			$dFiscalYear = $objGeneral->fnGet("txtFiscalYear");
			// Set 2 years comparison according to Organization Fiscal Year
			if(cOrganizationFiscalYear == "01-01")		// If Organization Fiscal Year is Jan, 1st
			{
				$dYear1_Start = $dFiscalYear . '-01-01';
				$dYear1_End = $dFiscalYear . '-12-31';

				$dYear2_Start = ($dFiscalYear-1) . '-01-01';
				$dYear2_End = ($dFiscalYear-1) . '-12-31';
			}
			else
			{
				$dYear1_Start = ($dFiscalYear -1) . '-07-01';
				$dYear1_End = $dFiscalYear . '-06-30';

				$dYear2_Start = ($dFiscalYear-2) . '-07-01';
				$dYear2_End = ($dFiscalYear-1) . '-06-30';			
			}
			

			// Only Get Income & Expenses
			$sQuery = "
			SELECT
			 CCAT.CategoryCode AS 'CategoryCode',
			 CCAT.CategoryName AS 'CategoryName',
			 CC.ControlCode AS 'ControlCode',
			 CC.ControlName AS 'ControlName',

			 CA.ChartOfAccountsCode AS 'AccountCode',
			 CA.AccountTitle AS 'AccountTitle',

			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear1_Start' AND '$dYear1_End', GJE.Debit, 0)) AS 'Year1_Debit',
			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear1_Start' AND '$dYear1_End', GJE.Credit, 0)) AS 'Year1_Credit',

			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear2_Start' AND '$dYear2_End', GJE.Debit, 0)) AS 'Year2_Debit',
			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear2_Start' AND '$dYear2_End', GJE.Credit, 0)) AS 'Year2_Credit',

			 CA.ChartOfAccountsCategoryId AS 'CategoryId',
			 CA.ChartOfAccountsControlId AS 'ControlId',
			 CA.ChartOfAccountsId AS 'ChartOfAccountsId'

			FROM fms_accounts_chartofaccounts AS CA
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
			INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
			LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
			LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId

			WHERE (CA.ChartOfAccountsCategoryId='3' OR CA.ChartOfAccountsCategoryId='4')  AND GJ.IsDeleted ='0' $sProjectCondition $sReportCriteriaSQL
			GROUP BY CA.ChartOfAccountsCategoryId, CA.ChartOfAccountsControlId, CA.ChartOfAccountsId";
			// Category Id = 3 Income & Category Id = 4 Expense

			//die($sQuery);
			$varResult = $objDatabase->Query($sQuery);

			$sReturn .= '<br />
			<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
			 <tr>
			  <td style="width:10px;"></td>
			  <td style="width:10px;"></td><td></td>
			  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right">' . date("Y", strtotime($dYear1_End)) . '</td>
			  <td style="width:1px;"></td>
			  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right">' . date("Y", strtotime($dYear2_End)) . '</td>
			 </tr>';

			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
				$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");
				$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");

				$iControlId = $objDatabase->Result($varResult, $i, "ControlId");
				$sControlCode = $objDatabase->Result($varResult, $i, "ControlCode");
				$sControlName = $objDatabase->Result($varResult, $i, "ControlName");
				
				$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");
				$sAccountCode = $objDatabase->Result($varResult, $i, "AccountCode");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				
				$dYear1_Debit = $objDatabase->Result($varResult, $i, "Year1_Debit");
				$dYear1_Credit = $objDatabase->Result($varResult, $i, "Year1_Credit");
				
				$dYear1_Balance = ($dYear1_Debit > 0) ? $dYear1_Debit : $dYear1_Credit;
				
				$dYear2_Debit = $objDatabase->Result($varResult, $i, "Year2_Debit");
				$dYear2_Credit = $objDatabase->Result($varResult, $i, "Year2_Credit");
				
				$dYear2_Balance = ($dYear2_Debit > 0) ? $dYear2_Debit : $dYear2_Credit;
				
				if (($i == 0) || ($iTemp != $iCategoryId))
				{
					if (($i > 0) && ($iTemp != $iCategoryId))
					{
						$sReturn .= '<tr>
						 <td></td>
						 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryNameTemp . '</td>
						 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
						 <td style="width:1px;"></td>
						 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year2, 0) . '</td>
						</tr>';
						
						$dTotalIncome_Year1 = $dTotal_Year1;
						$dTotal_Year1 = 0;
						
						$dTotalIncome_Year2 = $dTotal_Year2;
						$dTotal_Year2 = 0;
					}
					
					$sCategoryNameTemp = $sCategoryName;
					
					$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:18px;">' . (($i > 0) ? '<br />' : '') . strtoupper($sCategoryName) . '</td></tr>';
					$iTemp = $iCategoryId;
				}
				
				if (($i == 0) || ($iTemp2 != $iControlId))
				{
					$sReturn .= '<tr><td>&nbsp;</td><td align="left" colspan="3" style="font-family:Tahoma, Arial;font-size:16px;">' . $sControlName . '</td></tr>';
					$iTemp2 = $iControlId;
				}
				
				
				$sYear1_Balance = number_format($dYear1_Balance, 0);
				if ($sYear1_Balance == 0) $sYear1_Balance = '-';
				
				$sYear2_Balance = number_format($dYear2_Balance, 0);
				if ($sYear2_Balance == 0) $sYear2_Balance = '-';
				
				$sReturn .= '
				<tr>
				 <td></td>
				 <td></td>
				 <td align="left" style="font-size:12px;font-family:Tahoma, Arial;"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=-1&selStation=-1&selChartOfAccountCategory=' . $iCategoryId . '&selChartOfAccountControl=' . $iControlId . '&txtDateRangeStart='.$dYear2_Start . '&txtDateRangeEnd='. $dYear1_End . '&selChartOfAccount=' . $iChartOfAccountsId. '\', 800,600);" />' . $sAccountCode . ' - ' . $sAccountTitle . '</a></td>
				 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . $sYear1_Balance . '</td>
				 <td style="width:1px;"></td>
				 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . $sYear2_Balance . '</td>
				</tr>';
				
				$dTotal_Year1 += $dYear1_Balance;
				$dTotal_Year2 += $dYear2_Balance;
			}
			
			$dTotalExpense_Year1 = $dTotal_Year1;
			$dTotalExpense_Year2 = $dTotal_Year2;
			
			$sReturn .= '<tr>
			 <td></td>
			 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryNameTemp . '</td>
			 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
			 <td style="width:1px;"></td>
			 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year2, 0) . '</td>
			</tr>';

			$sReturn .= '<tr><td><br /></td></tr>
			<tr>
			 <td style="font-family:Tahoma, Arial;font-size:18px;" align="left" colspan="3">Operating Result (Surplus/Deficit): </td>
			 <td style="border-bottom:4px double; font-family:Tahoma, Arial;font-size:18px;" align="right">' . number_format(($dTotalIncome_Year1 - $dTotalExpense_Year1), 0) . '</td>
			 <td style="width:1px;"></td>
			 <td style="border-bottom:4px double; font-family:Tahoma, Arial;font-size:18px;" align="right">' . number_format(($dTotalIncome_Year2 - $dTotalExpense_Year2), 0) . '</td>
			</tr>';

			$sReturn .= '</table>';
		}

		return($sReturn);
	}	
	/*Backup*/
	function OLD_GenerateIncomeStatementReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

		//$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		//$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		$iDonorProjectId = $objGeneral->fnGet("selProject");
		$iDonorProjectId = $objGeneral->fnGet("selProject");

		$iComparison = $objGeneral->fnGet("selComparison");

		if($iDonorProjectId > 0) $sProjectCondition = "AND (GJE.DonorProjectId = '$iDonorProjectId')";

		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Income Statement</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		/*
		$dFiscalYearStartDate = $objEmployee->aSystemSettings['VF_FiscalYearStartDate'];
		$dFiscalYearEndDate = $objEmployee->aSystemSettings['VF_FiscalYearEndDate'];


		$dFirscalYearStartDate = "01-01"; // 07-01
		$dFiscalYearEndDate = "12-31";   // 06-30

		$dFirscalYearStartDate = "07-01";
		$dFiscalYearEndDate = "06-30";

		if (substr($dFiscalYearStartDate, 0, 2) <= date("m"))
		{
			die("X");
			if (substr($dFiscalYearStartDate, 0, 2) < 1)
				$dYear1_Start = date("Y") . '-' . $dFiscalYearStartDate;
			else
				$dYear1_Start = date("Y")-1 . '-' . $dFiscalYearStartDate;
		}
		else
			$dYear1_Start = date("Y") . '-' . $dFirscalYearStartDate;


		//$dYear1_Start = date("Y") . '-' . $dFiscalYearStartDate;
		//$dYear1_End = date("Y") . '-' . $dFiscalYearEndDate;

		die(">>" . $dYear1_Start . ' - - ' . $dYear1_End);
		*/

		// This Year
		//$dYear1_Start = date("Y-01-01");
		//$dYear1_End = date("Y-12-31");

		// Last Year
		//$dYear2_Start = date("Y-01-01", mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-1));
		//$dYear2_End = date("Y-12-31", mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-1));

		if ($iComparison == 0) // No Comparison
		{
			$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
			$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

			$dYear1_Start = $dCriteria_StartDate;
			$dYear1_End = $dCriteria_EndDate;

			// Only Get Income & Expenses
			$sQuery = "
			SELECT
			 CCAT.CategoryCode AS 'CategoryCode',
			 CCAT.CategoryName AS 'CategoryName',
			 CC.ControlCode AS 'ControlCode',
			 CC.ControlName AS 'ControlName',

			 CA.ChartOfAccountsCode AS 'AccountCode',
			 CA.AccountTitle AS 'AccountTitle',
			 CA.ChartOfAccountsId AS 'ChartOfAccountsId',

			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear1_Start' AND '$dYear1_End', GJE.Debit, 0)) AS 'Year1_Debit',
			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear1_Start' AND '$dYear1_End', GJE.Credit, 0)) AS 'Year1_Credit',

			 CA.ChartOfAccountsCategoryId AS 'CategoryId',
			 CA.ChartOfAccountsControlId AS 'ControlId',
			 CA.ChartOfAccountsId AS 'ChartOfAccountsId'

			FROM fms_accounts_chartofaccounts AS CA
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
			INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
			LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
			LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId
			WHERE (CA.ChartOfAccountsCategoryId='3' OR CA.ChartOfAccountsCategoryId='4')  AND GJ.IsDeleted ='0' $sProjectCondition $sReportCriteriaSQL
			GROUP BY CA.ChartOfAccountsCategoryId, CA.ChartOfAccountsControlId, CA.ChartOfAccountsId";
			// Category Id = 3 Income & Category Id = 4 Expense
			
			//die($sQuery);
			$varResult = $objDatabase->Query($sQuery);

			$sReturn .= '<br />
			<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
			 <tr>
			  <td style="width:10px;"></td>
			  <td style="width:10px;"></td><td></td>
			  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right"></td>
			 </tr>';

			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
				$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");
				$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");

				$iControlId = $objDatabase->Result($varResult, $i, "ControlId");
				$sControlCode = $objDatabase->Result($varResult, $i, "ControlCode");
				$sControlName = $objDatabase->Result($varResult, $i, "ControlName");

				$sAccountCode = $objDatabase->Result($varResult, $i, "AccountCode");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");

				$dYear1_Debit = $objDatabase->Result($varResult, $i, "Year1_Debit");
				$dYear1_Credit = $objDatabase->Result($varResult, $i, "Year1_Credit");

				//$dYear1_Balance = ($dYear1_Debit > 0) ? $dYear1_Debit : $dYear1_Credit;
				$dYear1_Balance = ($dYear1_Debit > 0) ? ($dYear1_Debit - $dYear1_Credit) : ($dYear1_Credit - $dYear1_Debit);

				if (($i == 0) || ($iTemp != $iCategoryId))
				{
					if (($i > 0) && ($iTemp != $iCategoryId))
					{
						$sReturn .= '<tr>
						 <td></td>
						 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryNameTemp . '</td>
						 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
						</tr>';

						$dTotalIncome_Year1 = $dTotal_Year1;
						$dTotal_Year1 = 0;
					}

					$sCategoryNameTemp = $sCategoryName;

					$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:18px;">' . (($i > 0) ? '<br />' : '') . strtoupper($sCategoryName) . '</td></tr>';
					$iTemp = $iCategoryId;
				}

				if (($i == 0) || ($iTemp2 != $iControlId))
				{
					$sReturn .= '<tr><td>&nbsp;</td><td align="left" colspan="3" style="font-family:Tahoma, Arial;font-size:16px;"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=-1&selStation=-1&selChartOfAccountCategory=' . $iCategoryId . '&selChartOfAccountControl=' . $iControlId . '\', 800,600);" />' . $sControlName . '</a></td></tr>';
					$iTemp2 = $iControlId;
				}

				if ($dYear1_Balance > 0)
				{
					$sReturn .= '
					<tr>
					 <td></td>
					 <td></td>
					 <td align="left" style="font-size:12px;font-family:Tahoma, Arial;"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=-1&selStation=-1&selChartOfAccountCategory=' . $iCategoryId . '&selChartOfAccountControl=' . $iControlId . '&txtDateRangeStart='.$dCriteria_StartDate . '&txtDateRangeEnd='. $dCriteria_EndDate . '&selChartOfAccount=' . $iChartOfAccountsId. '\', 800,600);" />' . $sAccountCode . ' - ' . $sAccountTitle . '</td>
					 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear1_Balance, 0) . '</td>
					</tr>';
				}

				$dTotal_Year1 += $dYear1_Balance;
			}

			$dTotalExpense_Year1 = $dTotal_Year1;

			$sReturn .= '<tr>
			 <td></td>
			 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryNameTemp . '</td>
			 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
			</tr>';

			$sReturn .= '<tr><td><br /></td></tr>
			<tr>
			 <td style="font-family:Tahoma, Arial;font-size:18px;" align="left" colspan="3">Operating Result (Surplus/Deficit): </td>
			 <td style="border-bottom:4px double; font-family:Tahoma, Arial;font-size:18px;" align="right">' . number_format(($dTotalIncome_Year1 - $dTotalExpense_Year1), 0) . '</td>
			</tr>';

			$sReturn .= '</table>';
		}
		else if ($iComparison == 2) // 2 Years Comparison
		{
			$dFiscalYear = $objGeneral->fnGet("txtFiscalYear");
			// Set 2 years comparison according to Organization Fiscal Year
			if(cOrganizationFiscalYear == "01-01")		// If Organization Fiscal Year is Jan, 1st
			{
				$dYear1_Start = $dFiscalYear . '-01-01';
				$dYear1_End = $dFiscalYear . '-12-31';

				$dYear2_Start = ($dFiscalYear-1) . '-01-01';
				$dYear2_End = ($dFiscalYear-1) . '-12-31';
			}
			else
			{
				$dYear1_Start = ($dFiscalYear -1) . '-07-01';
				$dYear1_End = $dFiscalYear . '-06-30';

				$dYear2_Start = ($dFiscalYear-2) . '-07-01';
				$dYear2_End = ($dFiscalYear-1) . '-06-30';			
			}
			

			// Only Get Income & Expenses
			$sQuery = "
			SELECT
			 CCAT.CategoryCode AS 'CategoryCode',
			 CCAT.CategoryName AS 'CategoryName',
			 CC.ControlCode AS 'ControlCode',
			 CC.ControlName AS 'ControlName',

			 CA.ChartOfAccountsCode AS 'AccountCode',
			 CA.AccountTitle AS 'AccountTitle',

			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear1_Start' AND '$dYear1_End', GJE.Debit, 0)) AS 'Year1_Debit',
			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear1_Start' AND '$dYear1_End', GJE.Credit, 0)) AS 'Year1_Credit',

			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear2_Start' AND '$dYear2_End', GJE.Debit, 0)) AS 'Year2_Debit',
			 SUM(IF(GJ.TransactionDate BETWEEN '$dYear2_Start' AND '$dYear2_End', GJE.Credit, 0)) AS 'Year2_Credit',

			 CA.ChartOfAccountsCategoryId AS 'CategoryId',
			 CA.ChartOfAccountsControlId AS 'ControlId',
			 CA.ChartOfAccountsId AS 'ChartOfAccountsId'

			FROM fms_accounts_chartofaccounts AS CA
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
			INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
			LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
			LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId

			WHERE (CA.ChartOfAccountsCategoryId='3' OR CA.ChartOfAccountsCategoryId='4')  AND GJ.IsDeleted ='0' $sProjectCondition $sReportCriteriaSQL
			GROUP BY CA.ChartOfAccountsCategoryId, CA.ChartOfAccountsControlId, CA.ChartOfAccountsId";
			// Category Id = 3 Income & Category Id = 4 Expense

			//die($sQuery);
			$varResult = $objDatabase->Query($sQuery);

			$sReturn .= '<br />
			<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
			 <tr>
			  <td style="width:10px;"></td>
			  <td style="width:10px;"></td><td></td>
			  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right">' . date("Y", strtotime($dYear1_End)) . '</td>
			  <td style="width:1px;"></td>
			  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right">' . date("Y", strtotime($dYear2_End)) . '</td>
			 </tr>';

			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
				$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");
				$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");

				$iControlId = $objDatabase->Result($varResult, $i, "ControlId");
				$sControlCode = $objDatabase->Result($varResult, $i, "ControlCode");
				$sControlName = $objDatabase->Result($varResult, $i, "ControlName");
				
				$iChartOfAccountsId = $objDatabase->Result($varResult, $i, "ChartOfAccountsId");
				$sAccountCode = $objDatabase->Result($varResult, $i, "AccountCode");
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				
				$dYear1_Debit = $objDatabase->Result($varResult, $i, "Year1_Debit");
				$dYear1_Credit = $objDatabase->Result($varResult, $i, "Year1_Credit");
				
				$dYear1_Balance = ($dYear1_Debit > 0) ? $dYear1_Debit : $dYear1_Credit;
				
				$dYear2_Debit = $objDatabase->Result($varResult, $i, "Year2_Debit");
				$dYear2_Credit = $objDatabase->Result($varResult, $i, "Year2_Credit");
				
				$dYear2_Balance = ($dYear2_Debit > 0) ? $dYear2_Debit : $dYear2_Credit;
				
				if (($i == 0) || ($iTemp != $iCategoryId))
				{
					if (($i > 0) && ($iTemp != $iCategoryId))
					{
						$sReturn .= '<tr>
						 <td></td>
						 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryNameTemp . '</td>
						 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
						 <td style="width:1px;"></td>
						 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year2, 0) . '</td>
						</tr>';
						
						$dTotalIncome_Year1 = $dTotal_Year1;
						$dTotal_Year1 = 0;
						
						$dTotalIncome_Year2 = $dTotal_Year2;
						$dTotal_Year2 = 0;
					}
					
					$sCategoryNameTemp = $sCategoryName;
					
					$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:18px;">' . (($i > 0) ? '<br />' : '') . strtoupper($sCategoryName) . '</td></tr>';
					$iTemp = $iCategoryId;
				}
				
				if (($i == 0) || ($iTemp2 != $iControlId))
				{
					$sReturn .= '<tr><td>&nbsp;</td><td align="left" colspan="3" style="font-family:Tahoma, Arial;font-size:16px;">' . $sControlName . '</td></tr>';
					$iTemp2 = $iControlId;
				}
				
				
				$sYear1_Balance = number_format($dYear1_Balance, 0);
				if ($sYear1_Balance == 0) $sYear1_Balance = '-';
				
				$sYear2_Balance = number_format($dYear2_Balance, 0);
				if ($sYear2_Balance == 0) $sYear2_Balance = '-';
				
				$sReturn .= '
				<tr>
				 <td></td>
				 <td></td>
				 <td align="left" style="font-size:12px;font-family:Tahoma, Arial;"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=-1&selStation=-1&selChartOfAccountCategory=' . $iCategoryId . '&selChartOfAccountControl=' . $iControlId . '&txtDateRangeStart='.$dYear2_Start . '&txtDateRangeEnd='. $dYear1_End . '&selChartOfAccount=' . $iChartOfAccountsId. '\', 800,600);" />' . $sAccountCode . ' - ' . $sAccountTitle . '</a></td>
				 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . $sYear1_Balance . '</td>
				 <td style="width:1px;"></td>
				 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . $sYear2_Balance . '</td>
				</tr>';

				$dTotal_Year1 += $dYear1_Balance;
				$dTotal_Year2 += $dYear2_Balance;
			}
			
			$dTotalExpense_Year1 = $dTotal_Year1;
			$dTotalExpense_Year2 = $dTotal_Year2;
			
			$sReturn .= '<tr>
			 <td></td>
			 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryNameTemp . '</td>
			 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
			 <td style="width:1px;"></td>
			 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year2, 0) . '</td>
			</tr>';

			$sReturn .= '<tr><td><br /></td></tr>
			<tr>
			 <td style="font-family:Tahoma, Arial;font-size:18px;" align="left" colspan="3">Operating Result (Surplus/Deficit): </td>
			 <td style="border-bottom:4px double; font-family:Tahoma, Arial;font-size:18px;" align="right">' . number_format(($dTotalIncome_Year1 - $dTotalExpense_Year1), 0) . '</td>
			 <td style="width:1px;"></td>
			 <td style="border-bottom:4px double; font-family:Tahoma, Arial;font-size:18px;" align="right">' . number_format(($dTotalIncome_Year2 - $dTotalExpense_Year2), 0) . '</td>
			</tr>';

			$sReturn .= '</table>';
		}

		return($sReturn);
	}	
	
	/* Balance Sheet*/
	/*
	function GenerateBalanceSheetReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		$iDonorProjectId = $objGeneral->fnGet("selProject");

		if($iDonorProjectId > 0) $sProjectCondition = "AND (GJE.DonorProjectId = '$iDonorProjectId')";
		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Balance Sheet</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
		
		$dDate = $objGeneral->fnGet("txtDate");

		$dDate1 = $dDate;			// As Of
		$dDate2 = date("Y-12-31", mktime(0, 0, 0, date("m"), date("d"),   date("Y", strtotime($dDate))-1));
		
		//die($dDate1 . ' - - ' . $dDate2);
		
		// Only Get Assets & Liabilities
		$sQuery = "
		SELECT		
		 CCAT.CategoryCode AS 'CategoryCode',
		 CCAT.CategoryName AS 'CategoryName',
		 CC.ControlCode AS 'ControlCode',
		 CC.ControlName AS 'ControlName',
		 
		 CA.ChartOfAccountsCode AS 'AccountCode',
		 CA.AccountTitle AS 'AccountTitle',
		 
		 SUM(IF(GJ.TransactionDate <= '$dDate1', GJE.Debit, 0)) AS 'Year1_Debit',
		 SUM(IF(GJ.TransactionDate <= '$dDate1', GJE.Credit, 0)) AS 'Year1_Credit',
		
		 SUM(IF(GJ.TransactionDate <= '$dDate2', GJE.Debit, 0)) AS 'Year2_Debit',
		 SUM(IF(GJ.TransactionDate <= '$dDate2', GJE.Credit, 0)) AS 'Year2_Credit',

		 CA.ChartOfAccountsCategoryId AS 'CategoryId',
		 CA.ChartOfAccountsControlId AS 'ControlId',
		 CA.ChartOfAccountsId AS 'ChartOfAccountsId'
		  
		FROM fms_accounts_chartofaccounts AS CA
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId

		WHERE (CA.ChartOfAccountsCategoryId='1' OR CA.ChartOfAccountsCategoryId='2') $sProjectCondition $sReportCriteriaSQL
		GROUP BY CCAT.ChartOfAccountsCategoryId, CA.ChartOfAccountsId";
		
		//  OR CA.ChartOfAccountsCategoryId='5'
		//die($sQuery);
		
		$varResult = $objDatabase->Query($sQuery);		

		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr>
		  <td style="width:10px;"></td>
		  <td style="width:10px;"></td><td></td>
		  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right">' . date("Y", strtotime($dDate1)) . '</td>
		  <td style="width:1px;"></td>
		  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right">' . date("Y", strtotime($dDate2)) . '</td>
		 </tr>';
		
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
			$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");
			$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");
			
			$iControlId = $objDatabase->Result($varResult, $i, "ControlId");
			$sControlCode = $objDatabase->Result($varResult, $i, "ControlCode");
			$sControlName = $objDatabase->Result($varResult, $i, "ControlName");
			
			$sAccountCode = $objDatabase->Result($varResult, $i, "AccountCode");
			$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
			
			$dYear1_Debit = $objDatabase->Result($varResult, $i, "Year1_Debit");
			$dYear1_Credit = $objDatabase->Result($varResult, $i, "Year1_Credit");
			
			$dYear2_Debit = $objDatabase->Result($varResult, $i, "Year2_Debit");
			$dYear2_Credit = $objDatabase->Result($varResult, $i, "Year2_Credit");
			
			if ($iCategoryId == 1)		// Assets
			{
				$dYear1_Balance = $dYear1_Debit - $dYear1_Credit;
				$dYear2_Balance = $dYear2_Debit - $dYear2_Credit;
			}
			else if (($iCategoryId == 2) || ($iCategoryId == 5))	// Liabilities
			{
				$dYear1_Balance = $dYear1_Credit - $dYear1_Debit;
				$dYear2_Balance = $dYear2_Credit - $dYear2_Debit;
			}

			
			if (($i == 0) || ($iTemp != $iCategoryId))
			{
				if (($i > 0) && ($iTemp != $iCategoryId))
				{
					$sReturn .= '<tr>
					 <td></td>
					 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryNameTemp . '</td>
					 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
					 <td style="width:1px;"></td>
					 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year2, 0) . '</td>
					</tr>';
					
					$dTotalIncome_Year1 = $dTotal_Year1;
					$dTotal_Year1 = 0;
					
					$dTotalIncome_Year2 = $dTotal_Year2;
					$dTotal_Year2 = 0;
				}
				
				$sCategoryNameTemp = $sCategoryName;
				
				$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:18px;">' . (($i > 0) ? '<br />' : '') . strtoupper($sCategoryName) . '</td></tr>';
				$iTemp = $iCategoryId;
			}
			
			if (($i == 0) || ($iTemp2 != $iControlId))
			{
				$sReturn .= '<tr><td>&nbsp;</td><td align="left" colspan="3" style="font-family:Tahoma, Arial;font-size:16px;">' . $sControlName . '</td></tr>';
				$iTemp2 = $iControlId;
			}
			
			$sReturn .= '
			<tr>
			 <td></td>
			 <td></td>
			 <td align="left" style="font-size:12px;font-family:Tahoma, Arial;">' . $sAccountCode . ' - ' . $sAccountTitle . '</td>
			 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear1_Balance, 0) . '</td>
			 <td style="width:1px;"></td>
			 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear2_Balance, 0) . '</td>
			</tr>';

			$dTotal_Year1 += $dYear1_Balance;
			$dTotal_Year2 += $dYear2_Balance;
	    }
		
		$dTotalExpense_Year1 = $dTotal_Year1;
		$dTotalExpense_Year2 = $dTotal_Year2;
		
		$sReturn .= '<tr>
		 <td></td>
		 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryNameTemp . '</td>
		 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
		 <td style="width:1px;"></td>
		 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year2, 0) . '</td>
		</tr>';
		
		// Equity
		$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:18px;"><br />EQUITY</td></tr>';

		$sQuery = "
		SELECT
			CA.ChartOfAccountsCategoryId AS 'CategoryId',   
			CCAT.CategoryName AS 'CategoryName',

			SUM(IF(GJ.TransactionDate <= '$dDate1', GJE.Debit, 0)) AS 'Year1_Debit',
			SUM(IF(GJ.TransactionDate <= '$dDate1', GJE.Credit, 0)) AS 'Year1_Credit',
			
			SUM(IF(GJ.TransactionDate <= '$dDate2', GJE.Debit, 0)) AS 'Year2_Debit',
			SUM(IF(GJ.TransactionDate <= '$dDate2', GJE.Credit, 0)) AS 'Year2_Credit'
			
			FROM fms_accounts_chartofaccounts AS CA
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
			INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
			LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
			LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId

			WHERE (CA.ChartOfAccountsCategoryId='3' OR CA.ChartOfAccountsCategoryId='4') $sProjectCondition $sReportCriteriaSQL
			GROUP BY CA.ChartOfAccountsCategoryId";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
			
			if ($iCategoryId == 3) // Income
			{
				$dIncome_Year1_Debit = $objDatabase->Result($varResult, $i, "Year1_Debit");
				$dIncome_Year1_Credit = $objDatabase->Result($varResult, $i, "Year1_Credit");
				
				//$dIncome_Year1_Balance = ($dIncome_Year1_Debit > 0) ? $dIncome_Year1_Debit : $dIncome_Year1_Credit;
				$dIncome_Year1_Balance = ($dIncome_Year1_Debit > 0) ? ($dIncome_Year1_Debit - $dIncome_Year1_Credit) :($dIncome_Year1_Credit - $dIncome_Year1_Debit);
				
				$dIncome_Year2_Debit = $objDatabase->Result($varResult, $i, "Year2_Debit");
				$dIncome_Year2_Credit = $objDatabase->Result($varResult, $i, "Year2_Credit");

				//$dIncome_Year2_Balance = ($dIncome_Year2_Debit > 0) ? $dIncome_Year2_Debit : $dIncome_Year2_Credit;
				$dIncome_Year2_Balance = ($dIncome_Year2_Debit > 0) ? ($dIncome_Year2_Debit - $dIncome_Year2_Credit) : ($dIncome_Year2_Credit - $dIncome_Year2_Debit);
			}
			else if ($iCategoryId == 4) // Expenses
			{
				$dExpense_Year1_Debit = $objDatabase->Result($varResult, $i, "Year1_Debit");
				$dExpense_Year1_Credit = $objDatabase->Result($varResult, $i, "Year1_Credit");

				//$dExpense_Year1_Balance = ($dExpense_Year1_Debit > 0) ? $dExpense_Year1_Debit : $dExpense_Year1_Credit;
				$dExpense_Year1_Balance = ($dExpense_Year1_Debit > 0) ? ($dExpense_Year1_Debit -  $dExpense_Year1_Credit): ($dExpense_Year1_Credit - $dExpense_Year1_Debit) ;
				
				$dExpense_Year2_Debit = $objDatabase->Result($varResult, $i, "Year2_Debit");
				$dExpense_Year2_Credit = $objDatabase->Result($varResult, $i, "Year2_Credit");
				
				//$dExpense_Year2_Balance = ($dExpense_Year2_Debit > 0) ? $dExpense_Year2_Debit : $dExpense_Year2_Credit;
				$dExpense_Year2_Balance = ($dExpense_Year2_Debit > 0) ? ($dExpense_Year2_Debit - $dExpense_Year2_Credit) : ($dExpense_Year2_Credit - $dExpense_Year2_Debit);
			}
		}
		
		$sReturn .= '<tr>
		 <td></td>
		 <td colspan="2" style="font-size:16px;font-family:Tahoma, Arial;">Operating Result (Surplus/Deficit):</td>
		 <td style="border-bottom:4px double; font-size:16px;font-family:Tahoma, Arial;" align="right">' . number_format(($dIncome_Year1_Balance-$dExpense_Year1_Balance), 0) . '</td>
		 <td></td> 
		 <td style="border-bottom:4px double; font-size:16px;font-family:Tahoma, Arial;" align="right">' . number_format(($dIncome_Year2_Balance-$dExpense_Year2_Balance), 0) . '</td>
		</tr>
		<tr>
		<td><br /><br /><br /><br /></td>
		 <td colspan="2" style="font-size:16px;font-family:Tahoma, Arial;">Total:</td>
		 <td style="border-bottom:4px double; font-size:16px;font-family:Tahoma, Arial;" align="right">' . number_format(($dIncome_Year1_Balance-$dExpense_Year1_Balance + $dTotal_Year1), 0) . '</td>
		 <td></td> 
		 <td style="border-bottom:4px double; font-size:16px;font-family:Tahoma, Arial;" align="right">' . number_format(($dIncome_Year2_Balance-$dExpense_Year2_Balance + $dTotal_Year2), 0) . '</td>
		</tr>
		<tr><td><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	*/
	
	
	/* New Balance Sheet*/
	function GenerateBalanceSheetReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/
		
		$iDonorProjectId = $objGeneral->fnGet("selProject");

		if($iDonorProjectId > 0) $sProjectCondition = "AND (GJE.DonorProjectId = '$iDonorProjectId')";
		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Balance Sheet</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
		
		$dDate = $objGeneral->fnGet("txtDate");

		$dDate1 = $dDate;			// As Of
		// Set 2 years comparison according to Organization Fiscal Year
		if(cOrganizationFiscalYear == "01-01")		// If Organization Fiscal Year is Jan, 1st
		{
			$dDate2 = date("Y-12-31", mktime(0, 0, 0, date("m"), date("d"),   date("Y", strtotime($dDate))-1));			
		}
		else
		{
			$dDate2 = date("Y-06-30", mktime(0, 0, 0, date("m"), date("d"),   date("Y", strtotime($dDate))-1)); // Set to June			
		}
				
		$sQuery = "
		SELECT
		 CA.ChartOfAccountsCategoryId AS 'CategoryId',
		 CCAT.CategoryCode AS 'CategoryCode',
		 CCAT.CategoryName AS 'CategoryName'		  
		FROM fms_accounts_chartofaccounts AS CA
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId
		WHERE (CA.ChartOfAccountsCategoryId='1' OR CA.ChartOfAccountsCategoryId='2')  AND GJ.IsDeleted ='0' $sProjectCondition $sReportCriteriaSQL
		GROUP BY CCAT.ChartOfAccountsCategoryId";		
		//  OR CA.ChartOfAccountsCategoryId='5'
		//die($sQuery);
		
		$varResult = $objDatabase->Query($sQuery);		

		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr>
		  <td style="width:10px;"></td>
		  <td style="width:10px;"></td><td></td>
		  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right">' . date("Y", strtotime($dDate1)) . '</td>
		  <td style="width:1px;"></td>
		  <td style="width:100px;font-size:16px; font-family:Tahoma, Arial;" align="right">' . date("Y", strtotime($dDate2)) . '</td>
		 </tr>';
		$dTotalAssets_Year1 = $dTotalAssets_Year2 = $dTotalLiabilities_Year1 = $dTotalLiabilities_Year2 = 0;
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
			$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");
			$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");
			
			$dTotal_Year1 = 0;
			$dTotal_Year2 = 0;
			
			$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:18px;">' . (($i > 0) ? '<br />' : '') . strtoupper($sCategoryName) . '</td></tr>';
			
			// Get Parent Controls
			$varResult2 = $objDatabase->Query("
			SELECT
			 CC.ControlCode AS 'ControlCode',
			 CC.ControlName AS 'ControlName',
			 CC.ChartOfAccountsControlId AS 'ControlId'			
			FROM fms_accounts_chartofaccounts_controls AS CC			
			WHERE CC.ChartOfAccountsCategoryId='$iCategoryId' AND (CC.ChartOfAccountsControlParentId = '0' OR CC.ChartOfAccountsControlParentId = '-1')");
			
			for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
			{
				$iControlId = $objDatabase->Result($varResult2, $j, "ControlId");
				$sControlCode = $objDatabase->Result($varResult2, $j, "ControlCode");
				$sControlName = $objDatabase->Result($varResult2, $j, "ControlName");
				
				$sReturn .= '<tr><td>&nbsp;</td><td align="left" colspan="3" style="font-family:Tahoma, Arial;font-size:16px;">' . $sControlName . '</td></tr>';
				
				// Get Child Controls				
				$varResult3 = $objDatabase->Query("
				SELECT
				 CC.ControlCode AS 'Child_ControlCode',
				 CC.ControlName AS 'Child_ControlName',
				 CC.ChartOfAccountsControlId AS 'Child_ControlId'			
				FROM fms_accounts_chartofaccounts_controls AS CC			
				WHERE CC.ChartOfAccountsCategoryId='$iCategoryId' AND CC.ChartOfAccountsControlParentId = '$iControlId'");
				for ($k=0; $k < $objDatabase->RowsNumber($varResult3); $k++)
				{
					$iChild_ControlId = $objDatabase->Result($varResult3, $k, "Child_ControlId");
					$sChild_ControlCode = $objDatabase->Result($varResult3, $k, "Child_ControlCode");
					$sChild_ControlName = $objDatabase->Result($varResult3, $k, "Child_ControlName");
					
					$sReturn .= '<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align="left" colspan="2" style="font-family:Tahoma, Arial;font-size:16px;">' . $sChild_ControlName . '</td></tr>';
					
					// Get Chart Of Acconts of Child Controls
					$varResult4 = $objDatabase->Query("
					SELECT
					 CA.ChartOfAccountsCode AS 'AccountCode',
					 CA.AccountTitle AS 'AccountTitle',
					 
					 SUM(IF(GJ.TransactionDate <= '$dDate1', GJE.Debit, 0)) AS 'Year1_Debit',
					 SUM(IF(GJ.TransactionDate <= '$dDate1', GJE.Credit, 0)) AS 'Year1_Credit',
					
					 SUM(IF(GJ.TransactionDate <= '$dDate2', GJE.Debit, 0)) AS 'Year2_Debit',
					 SUM(IF(GJ.TransactionDate <= '$dDate2', GJE.Credit, 0)) AS 'Year2_Credit',

					 CA.ChartOfAccountsCategoryId AS 'CategoryId',
					 CA.ChartOfAccountsControlId AS 'ControlId',
					 CA.ChartOfAccountsId AS 'ChartOfAccountsId'
					  
					FROM fms_accounts_chartofaccounts AS CA
					INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
					INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
					LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
					LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId

					WHERE (CA.ChartOfAccountsCategoryId='$iCategoryId' AND CA.ChartOfAccountsControlId = '$iChild_ControlId')  AND GJ.IsDeleted ='0' $sProjectCondition $sReportCriteriaSQL
					GROUP BY CA.ChartOfAccountsId");
					for ($l=0; $l < $objDatabase->RowsNumber($varResult4); $l++)
					{
						$iChartOfAccountsId = $objDatabase->Result($varResult4, $l, "ChartOfAccountsId");
						$sAccountCode = $objDatabase->Result($varResult4, $l, "AccountCode");
						$sAccountTitle = $objDatabase->Result($varResult4, $l, "AccountTitle");
						
						$dYear1_Debit = $objDatabase->Result($varResult4, $l, "Year1_Debit");
						$dYear1_Credit = $objDatabase->Result($varResult4, $l, "Year1_Credit");
						
						$dYear2_Debit = $objDatabase->Result($varResult4, $l, "Year2_Debit");
						$dYear2_Credit = $objDatabase->Result($varResult4, $l, "Year2_Credit");
						
						//dont show if balances are 0 by aqureshi
						if(($dYear1_Debit == 0) && ($dYear1_Credit == 0) && ($dYear2_Debit == 0) && ($dYear2_Credit == 0))
							continue;
						
						if ($iCategoryId == 1)		// Assets
						{
							$dYear1_Balance = $dYear1_Debit - $dYear1_Credit;
							$dYear2_Balance = $dYear2_Debit - $dYear2_Credit;
							
							$dTotalAssets_Year1 += $dYear1_Balance;
							$dTotalAssets_Year2 += $dYear2_Balance;
						}
						else if (($iCategoryId == 2))	// Liabilities
						{
							$dYear1_Balance = $dYear1_Credit - $dYear1_Debit;
							$dYear2_Balance = $dYear2_Credit - $dYear2_Debit;
							
							$dTotalLiabilities_Year1 += $dYear1_Balance;
							$dTotalLiabilities_Year2 += $dYear2_Balance;
						}
						//dont show if balances are 0 by aqureshi
						if(($dYear1_Balance == 0) && ($dYear2_Balance == 0))
							continue;
							
						$sReturn .= '
						<tr>
						 <td></td>
						 <td></td>
						 <td align="left" style="font-size:12px;font-family:Tahoma, Arial;"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=-1&selStation=-1&selChartOfAccountCategory=' . $iCategoryId . '&selChartOfAccountControl=' . $iChild_ControlId . '&txtAsOfDate='.$dDate1 . '&selChartOfAccount=' . $iChartOfAccountsId. '\', 800,600);" />' . $sAccountCode . ' - ' . $sAccountTitle . '</a></td>
						 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear1_Balance, 0) . '</td>
						 <td style="width:1px;"></td>
						 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear2_Balance, 0) . '</td>
						</tr>';
						
						$dTotal_Year1 += $dYear1_Balance;
						$dTotal_Year2 += $dYear2_Balance;
					}
				}
				// Get Chart of Accounts of Parent Control
				
				$varResult5 = $objDatabase->Query("
				SELECT		
				 CA.ChartOfAccountsCode AS 'AccountCode',
				 CA.AccountTitle AS 'AccountTitle',
				 
				 SUM(IF(GJ.TransactionDate <= '$dDate1', GJE.Debit, 0)) AS 'Year1_Debit',
				 SUM(IF(GJ.TransactionDate <= '$dDate1', GJE.Credit, 0)) AS 'Year1_Credit',
				
				 SUM(IF(GJ.TransactionDate <= '$dDate2', GJE.Debit, 0)) AS 'Year2_Debit',
				 SUM(IF(GJ.TransactionDate <= '$dDate2', GJE.Credit, 0)) AS 'Year2_Credit',

				 CA.ChartOfAccountsCategoryId AS 'CategoryId',
				 CA.ChartOfAccountsControlId AS 'ControlId',
				 CA.ChartOfAccountsId AS 'ChartOfAccountsId'
				  
				FROM fms_accounts_chartofaccounts AS CA
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
				LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
				LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId

				WHERE (CA.ChartOfAccountsCategoryId='$iCategoryId' AND CA.ChartOfAccountsControlId = '$iControlId')  AND GJ.IsDeleted ='0' $sProjectCondition $sReportCriteriaSQL
				GROUP BY CA.ChartOfAccountsId");
				for ($m=0; $m < $objDatabase->RowsNumber($varResult5); $m++)
				{
					$iChartOfAccountsId = $objDatabase->Result($varResult5, $m, "ChartOfAccountsId");
					$sAccountCode = $objDatabase->Result($varResult5, $m, "AccountCode");
					$sAccountTitle = $objDatabase->Result($varResult5, $m, "AccountTitle");
					
					$dYear1_Debit = $objDatabase->Result($varResult5, $m, "Year1_Debit");
					$dYear1_Credit = $objDatabase->Result($varResult5, $m, "Year1_Credit");
					
					$dYear2_Debit = $objDatabase->Result($varResult5, $m, "Year2_Debit");
					$dYear2_Credit = $objDatabase->Result($varResult5, $m, "Year2_Credit");
					
					if(($dYear1_Debit == 0) && ($dYear1_Credit == 0) && ($dYear2_Debit == 0) && ($dYear2_Credit == 0))
						continue;
					
					if ($iCategoryId == 1)		// Assets
					{
						$dYear1_Balance = $dYear1_Debit - $dYear1_Credit;
						$dYear2_Balance = $dYear2_Debit - $dYear2_Credit;
						
						$dTotalAssets_Year1 += $dYear1_Balance;
						$dTotalAssets_Year2 += $dYear2_Balance;
					}
					else if (($iCategoryId == 2))	// Liabilities
					{
						$dYear1_Balance = $dYear1_Credit - $dYear1_Debit;
						$dYear2_Balance = $dYear2_Credit - $dYear2_Debit;
						
						$dTotalLiabilities_Year1 += $dYear1_Balance;
						$dTotalLiabilities_Year2 += $dYear2_Balance;
					}
					
					$sReturn .= '
					<tr>
					 <td></td>
					 <td></td>
					 <td align="left" style="font-size:12px;font-family:Tahoma, Arial;"><a style="text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=-1&selStation=-1&selChartOfAccountCategory=' . $iCategoryId . '&selChartOfAccountControl=' . $iControlId . '&txtAsOfDate='.$dDate1 . '&selChartOfAccount=' . $iChartOfAccountsId. '\', 800,600);" />' . $sAccountCode . ' - ' . $sAccountTitle . '</a></td>
					 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear1_Balance, 0) . '</td>
					 <td style="width:1px;"></td>
					 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear2_Balance, 0) . '</td>
					</tr>';
					
					$dTotal_Year1 += $dYear1_Balance;
					$dTotal_Year2 += $dYear2_Balance;
				}			
			}
			
			$sReturn .= '<tr>
			 <td></td>
			 <td colspan="2" align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total ' . $sCategoryName . '</td>
			 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year1, 0) . '</td>
			 <td style="width:1px;"></td>
			 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year2, 0) . '</td>
			</tr>';
	    }
		
		// Equity
		$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:18px;"><br />EQUITY</td></tr>';
		
		$dEquity_Year1 = $dTotalAssets_Year1 - $dTotalLiabilities_Year1;
		$dEquity_Year2 = $dTotalAssets_Year2 - $dTotalLiabilities_Year2;
		
		$sReturn .= '<tr>
		 <td></td>
		 <td colspan="2" style="font-size:16px;font-family:Tahoma, Arial;">Operating Result (Surplus/Deficit):</td>
		 <td style="border-bottom:4px double; font-size:16px;font-family:Tahoma, Arial;" align="right">' . number_format($dEquity_Year1, 0) . '</td>
		 <td></td> 
		 <td style="border-bottom:4px double; font-size:16px;font-family:Tahoma, Arial;" align="right">' . number_format($dEquity_Year2, 0) . '</td>
		</tr>';
		$sReturn .= '</table>';

		return($sReturn);
	}	
		
	/* Trial Balance*/
	function GenerateTrialBalanceReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

		$iDonorProjectId = $objGeneral->fnGet("selProject");
		if($iDonorProjectId > 0) $sProjectCondition = "AND (GJE.DonorProjectId = '$iDonorProjectId')";
		
		// Filters		
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		$iCriteria_DonorId = $objGeneral->fnGet("selDonor");
		$iCriteria_DonorProjectId = $objGeneral->fnGet("selDonorProject");
		$iCriteria_ProjectActivityId = $objGeneral->fnGet("selProjectActivity");
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		//$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";
		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '2001-01-01' AND '$dCriteria_EndDate')";


		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Trial Balance</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
		
		$sQuery = "
		SELECT
		 CA.ChartOfAccountsCategoryId AS 'CategoryId',
		 CCAT.CategoryCode AS 'CategoryCode',
		 CCAT.CategoryName AS 'CategoryName'		  
		FROM fms_accounts_chartofaccounts AS CA
		INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId
		WHERE 1=1  AND GJ.IsDeleted ='0' $sProjectCondition $sReportCriteriaSQL
		GROUP BY CCAT.ChartOfAccountsCategoryId";		
		//  OR CA.ChartOfAccountsCategoryId='5'
		// die($sQuery);
		
		$varResult = $objDatabase->Query($sQuery);		

		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="5" width="98%" align="center">
		 <thead>
		 <tr>
		  <td align="left" style="font-family:Tahoma, Arial; font-weight:bold; font-size:14px;">Account</td>
		  <td align="right" style="width:120px; font-family:Tahoma, Arial; font-weight:bold; font-size:14px;">Debit</td>
		  <td style="width:10px;"></td>
		  <td align="right" style="width:120px; font-family:Tahoma, Arial; font-weight:bold; font-size:14px;">Credit</td>		  
		 </tr>
		 </thead>
		 <tr><td><br /></td></tr>';	
		$dTotal_Debit = $dTotal_Credit = 0;
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
			$sCategoryCode = $objDatabase->Result($varResult, $i, "CategoryCode");
			$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");
			
			$sReturn .= '<tr><td align="left" colspan="3" style="font-family:Tahoma, Arial;font-size:18px;">' . (($i > 0) ? '<br />' : '') . strtoupper($sCategoryName) . '</td></tr>';
			
			// Get Parent Controls
			$varResult2 = $objDatabase->Query("
			SELECT
			 CC.ControlCode AS 'ControlCode',
			 CC.ControlName AS 'ControlName',
			 CC.ChartOfAccountsControlId AS 'ControlId'			
			FROM fms_accounts_chartofaccounts_controls AS CC			
			WHERE CC.ChartOfAccountsCategoryId='$iCategoryId' AND (CC.ChartOfAccountsControlParentId = '0' OR CC.ChartOfAccountsControlParentId = '-1')");
			
			for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
			{
				$iControlId = $objDatabase->Result($varResult2, $j, "ControlId");
				$sControlCode = $objDatabase->Result($varResult2, $j, "ControlCode");
				$sControlName = $objDatabase->Result($varResult2, $j, "ControlName");
				
				$sReturn .= '<tr><td align="left" colspan="3" style="font-family:Tahoma, Arial;font-size:16px;">&nbsp;&nbsp;' . $sControlName . '</td></tr>';
				
				// Get Child Controls				
				$varResult3 = $objDatabase->Query("
				SELECT
				 CC.ControlCode AS 'Child_ControlCode',
				 CC.ControlName AS 'Child_ControlName',
				 CC.ChartOfAccountsControlId AS 'Child_ControlId'			
				FROM fms_accounts_chartofaccounts_controls AS CC			
				WHERE CC.ChartOfAccountsCategoryId='$iCategoryId' AND CC.ChartOfAccountsControlParentId = '$iControlId'");
				for ($k=0; $k < $objDatabase->RowsNumber($varResult3); $k++)
				{
					$iChild_ControlId = $objDatabase->Result($varResult3, $k, "Child_ControlId");
					$sChild_ControlCode = $objDatabase->Result($varResult3, $k, "Child_ControlCode");
					$sChild_ControlName = $objDatabase->Result($varResult3, $k, "Child_ControlName");
					
					$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:16px;">&nbsp;&nbsp;&nbsp;&nbsp;' . $sChild_ControlName . '</td></tr>';
		
					$varResult4 = $objDatabase->Query("
					SELECT
					 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
					 CA.ChartOfAccountsCategoryId AS 'ChartOfAccountsCategoryId',
					 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
					 CA.AccountTitle AS 'AccountTitle',
					 SUM(GJE.Debit) AS 'Debit',
					 SUM(GJE.Credit) AS 'Credit',
					 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance',
					 CCAT.DebitIncrease AS 'DebitIncrease',
					 CCAT.CreditIncrease AS 'CreditIncrease'
					FROM fms_accounts_chartofaccounts AS CA
					INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
					INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
					LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
					LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId
					WHERE 1=1 $sProjectCondition $sReportCriteriaSQL $sReportDateTimeCriteria  AND GJ.IsDeleted ='0' AND CA.ChartOfAccountsCategoryId = '$iCategoryId' AND CA.ChartOfAccountsControlId = '$iChild_ControlId'
					GROUP BY CA.ChartOfAccountsId
					ORDER BY CA.ChartOfAccountsCategoryId");						
					for ($l=0; $l < $objDatabase->RowsNumber($varResult4); $l++)
					{
						$iChartOfAccountsId = $objDatabase->Result($varResult4, $l, "ChartOfAccountsId");
						$iChartOfAccountsCategoryId = $objDatabase->Result($varResult4, $l, "ChartOfAccountsCategoryId");
						$sAccountTitle = $objDatabase->Result($varResult4, $l, "AccountTitle");
						$sChartOfAccountsCode = $objDatabase->Result($varResult4, $l, "ChartOfAccountsCode");
						$dBalance = $objDatabase->Result($varResult4, $l, "Balance");						
						$iDebitIncrease = $objDatabase->Result($varResult4, $l, "DebitIncrease");
						$iCreditIncrease = $objDatabase->Result($varResult4, $l, "CreditIncrease");
						
						//$dDebit = $objDatabase->Result($varResult, $i, "Debit");
						//$dCredit = $objDatabase->Result($varResult, $i, "Credit");
						
						$dDebit = 0;
						$dCredit = 0;
						$sBalance = number_format($dBalance, 0);

						if ($iDebitIncrease == 1)
							$dDebit = $dBalance;
						else
							$dCredit = $dBalance;
						
						$sDebit = number_format($dDebit, 0);
						$sCredit = number_format($dCredit, 0);
						
						if ($sDebit == 0) $sDebit = "";
						if ($sCredit == 0) $sCredit = "";
						
						$dTotal_Debit += $dDebit;
						$dTotal_Credit += $dCredit;
						
						if ($dBalance != 0)
						{
							$sReturn .= '
							<tr>				 
							 <td align="left" style="font-size:12px;font-family:Tahoma, Arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-family:Tahoma, Arial; font-size:12px; text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selBank=' . $iCriteria_BankId . '&selBankAccount=' .$iCriteria_BankAccountId . '&selDonor=' .$iCriteria_DonorId . '&selDonorProject=' .$iCriteria_DonorProjectId . '&selProjectActivity=' .$iCriteria_ProjectActivityId . '&selChartOfAccountCategory='. $iCategoryId . '&txtDateRangeStart='. $dCriteria_StartDate . '&txtDateRangeEnd='. $dCriteria_EndDate . '&selChartOfAccount='. $iChartOfAccountsId . '\', 800,600);" />' . $sChartOfAccountsCode . ' - ' . $sAccountTitle . '</a></td>
							 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . $sDebit . '</td>
							 <td></td>
							 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . $sCredit . '</td>
							</tr>';
						}
					}
				}
				
				// Get Chart of Accounts based on Parent Control
				$varResult4 = $objDatabase->Query("
				SELECT
				 CA.ChartOfAccountsId AS 'ChartOfAccountsId',
				 CA.ChartOfAccountsCategoryId AS 'ChartOfAccountsCategoryId',
				 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
				 CA.AccountTitle AS 'AccountTitle',
				 SUM(GJE.Debit) AS 'Debit',
				 SUM(GJE.Credit) AS 'Credit',
				 IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance',
				 CCAT.DebitIncrease AS 'DebitIncrease',
				 CCAT.CreditIncrease AS 'CreditIncrease'
				FROM fms_accounts_chartofaccounts AS CA
				INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
				INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
				LEFT JOIN fms_accounts_generaljournal_entries AS GJE ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
				LEFT JOIN fms_accounts_generaljournal AS GJ ON GJE.GeneralJournalId = GJ.GeneralJournalId
				WHERE 1=1 $sProjectCondition $sReportCriteriaSQL $sReportDateTimeCriteria  AND GJ.IsDeleted ='0'  AND CA.ChartOfAccountsCategoryId = '$iCategoryId' AND CA.ChartOfAccountsControlId = '$iControlId'
				GROUP BY CA.ChartOfAccountsId
				ORDER BY CA.ChartOfAccountsCategoryId");						
				for ($l=0; $l < $objDatabase->RowsNumber($varResult4); $l++)
				{
					$iChartOfAccountsId = $objDatabase->Result($varResult4, $l, "ChartOfAccountsId");
					$iChartOfAccountsCategoryId = $objDatabase->Result($varResult4, $l, "ChartOfAccountsCategoryId");
					$sAccountTitle = $objDatabase->Result($varResult4, $l, "AccountTitle");
					$sChartOfAccountsCode = $objDatabase->Result($varResult4, $l, "ChartOfAccountsCode");
					$dBalance = $objDatabase->Result($varResult4, $l, "Balance");					
					$iDebitIncrease = $objDatabase->Result($varResult4, $l, "DebitIncrease");
					$iCreditIncrease = $objDatabase->Result($varResult4, $l, "CreditIncrease");

					$varResultIncome = $objDatabase->Query("
					SELECT 
					IF( CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance_Income'
					FROM fms_accounts_generaljournal AS GJ
					INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
					LEFT JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
					LEFT JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
					WHERE (CA.ChartOfAccountsCategoryId='3') $sReportCriteriaSQL
					AND GJ.IsDeleted ='0' AND GJ.TransactionDate <'$dCriteria_StartDate'");
					
					if ($objDatabase->RowsNumber($varResultIncome) > 0)
						$dBalance_Income = $objDatabase->Result($varResultIncome, 0, "Balance_Income");


					$varResultExpense = $objDatabase->Query("
					SELECT 
					IF( CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance_Expense'
					FROM fms_accounts_generaljournal AS GJ
					INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
					LEFT JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
					LEFT JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
					WHERE (CA.ChartOfAccountsCategoryId='4') $sReportCriteriaSQL
					AND GJ.IsDeleted ='0' AND GJ.TransactionDate <'$dCriteria_StartDate'");
					
					if ($objDatabase->RowsNumber($varResultExpense) > 0)
						$dBalance_Expense = $objDatabase->Result($varResultExpense, 0, "Balance_Expense");

					
					$varResultClosing2 = $objDatabase->Query("
					SELECT
						CA.ChartOfAccountsCategoryId AS 'ChartOfAccountsCategoryId',
						IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance2'
					FROM fms_accounts_generaljournal AS GJ
					INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
					LEFT JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
					LEFT JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
					$sBankTable
					WHERE (CA.ChartOfAccountsCategoryId='3' OR CA.ChartOfAccountsCategoryId='4') 
					AND GJE.ChartOfAccountsId= '$iChartOfAccountsId'  AND GJ.IsDeleted ='0' $sReportCriteriaSQL AND GJ.TransactionDate <'$dCriteria_StartDate'");
					if ($objDatabase->RowsNumber($varResultClosing2) > 0)
						$dBalance2 = $objDatabase->Result($varResultClosing2, 0, "Balance2");
										
					$dDebit = 0;
					$dCredit = 0;
					$sBalance = number_format($dBalance, 0);
					
					/*
					if ($iDebitIncrease == 1)
						$dDebit = $dBalance;
					else
						$dCredit = $dBalance;
					*/
					
					if ($iDebitIncrease == 1 && $iChartOfAccountsCategoryId == 1)
						$dDebit = $dBalance  - $dCredit;
					else if ($iDebitIncrease == 0 && $iChartOfAccountsCategoryId == 2)
						$dCredit = $dBalance - $dDebit;
					else if ($iDebitIncrease == 0 && $iChartOfAccountsCategoryId == 3)
						$dCredit =  $dBalance - $dDebit - $dBalance2;
					else if ($iDebitIncrease == 1 && $iChartOfAccountsCategoryId == 4)
						$dDebit = $dBalance - $dCredit - $dBalance2;
					else
						$dCredit = $dBalance + $dBalance_Income - $dBalance_Expense;

					$sDebit = number_format($dDebit, 0);
					$sCredit = number_format($dCredit, 0);
					
					if ($sDebit == 0) $sDebit = "";
					if ($sCredit == 0) $sCredit = "";
					
					$dTotal_Debit += $dDebit;
					$dTotal_Credit += $dCredit;
					
					//if ($dBalance != 0)
					//{
						$sReturn .= '
						<tr>				 
						 <td align="left" style="font-size:12px;font-family:Tahoma, Arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-family:Tahoma, Arial; font-size:12px; text-decoration:none;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=' . $iCriteria_EmployeeId . '&selStation=' . $iCriteria_StationId . '&selBank=' . $iCriteria_BankId . '&selBankAccount=' .$iCriteria_BankAccountId . '&selDonor=' .$iCriteria_DonorId . '&selDonorProject=' .$iCriteria_DonorProjectId . '&selProjectActivity=' .$iCriteria_ProjectActivityId . '&selChartOfAccountCategory='. $iCategoryId . '&txtDateRangeStart='. $dCriteria_StartDate . '&txtDateRangeEnd='. $dCriteria_EndDate . '&selChartOfAccount='. $iChartOfAccountsId . '\', 800,600);" />' . $sChartOfAccountsCode . ' - ' . $sAccountTitle . '</a></td>
						 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . $sDebit . '</td>
						 <td></td>
						 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . $sCredit . '</td>
						</tr>';
					//}
				}				
			}
		}
		
	   $sReturn .= '
		 <tr><td><br /></td></tr>
		 <tr>
		  <td></td>
		  <td align="right" style="border-bottom:4px double; border-top: 1px solid; font-size:15px;font-family:Tahoma, Arial;">' . number_format($dTotal_Debit, 0) . '</td>
		  <td></td>
		  <td align="right" style="border-bottom:4px double; border-top: 1px solid; font-size:15px;font-family:Tahoma, Arial;">' . number_format($dTotal_Credit, 0) . '</td>
		 </tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	
	/* Change in Equity */
	function GenerateChangeInEquityReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ProductsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		$dFiscalYear = date("Y", strtotime($dCriteria_StartDate));
		$dFiscalYear = $dFiscalYear . '-' . cOrganizationFiscalYear;
		
		$sReturn = '<br /><div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Statement of Changes in Funds</span><br />
		<span style="font-family:Tahoma, Arial; font-size:16px;">For the year ended ' . date("F j, Y", strtotime($dCurrentFiscalYear)) . '</div><br /><br />';
		
		$sReturn .= '
		<table border="0" cellspacing="0" cellpadding="5" width="98%" align="center">';
			
		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		
		/*
		$sQuery = "
		SELECT
			
			CA.ChartOfAccountsCategoryId AS 'CategoryId',   
			CA.ChartOfAccountsCode AS 'AccountCode',
			CA.AccountTitle AS 'AccountTitle',
			CA.ChartOfAccountsControlId AS 'ControlId',
			
			CCAT.CategoryName AS 'CategoryName',
			CC.ControlCode AS 'ControlCode',
			CC.ControlName AS 'ControlName',
			
			SUM(IF(GJ.TransactionDate > '$dLastFiscalYear' AND GJ.TransactionDate <= '$dCurrentFiscalYear', GJE.Debit, 0)) AS 'Debit',
			SUM(IF(GJ.TransactionDate > '$dLastFiscalYear' AND GJ.TransactionDate <= '$dCurrentFiscalYear', GJE.Credit, 0)) AS 'Credit',
			
			SUM(IF(GJ.TransactionDate <= '$dBeforeLastFiscalYear', GJE.Debit, 0)) AS 'Year1_Debit',
			SUM(IF(GJ.TransactionDate <= '$dBeforeLastFiscalYear', GJE.Credit, 0)) AS 'Year1_Credit',
			
			SUM(IF(GJ.TransactionDate > '$dBeforeLastFiscalYear' AND GJ.TransactionDate <= '$dLastFiscalYear', GJE.Debit, 0)) AS 'Year2_Debit',
			SUM(IF(GJ.TransactionDate > '$dBeforeLastFiscalYear' AND GJ.TransactionDate <= '$dLastFiscalYear', GJE.Credit, 0)) AS 'Year2_Credit',
			
			SUM(IF(GJ.TransactionDate > '$dLastFiscalYear' AND GJ.TransactionDate <= '$dCurrentFiscalYear', GJE.Debit, 0)) AS 'Year3_Debit',
			SUM(IF(GJ.TransactionDate > '$dLastFiscalYear' AND GJ.TransactionDate <= '$dCurrentFiscalYear', GJE.Credit, 0)) AS 'Year3_Credit'

			FROM fms_accounts_generaljournal AS GJ
			
			INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
			INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
			INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId

		WHERE (CA.ChartOfAccountsCategoryId='5' OR CA.ChartOfAccountsCategoryId='4') $sReportCriteriaSQL
		GROUP BY CA.ChartOfAccountsCategoryId, CA.ChartOfAccountsControlId, CA.ChartOfAccountsId
		ORDER BY CA.ChartOfAccountsCategoryId DESC
		";
		*/
		
		// Get Closing Balance
		$varResultX = $objDatabase->Query("
		SELECT
			IF(CCAT.DebitIncrease = '1' , SUM(GJE.Debit)-SUM(GJE.Credit), SUM(GJE.Credit)-SUM(GJE.Debit)) AS 'Balance'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy
		WHERE GJ.TransactionDate < '$dFiscalYear'  AND GJ.IsDeleted ='0' $sReportCriteriaSQL");
		if ($objDatabase->RowsNumber($varResultX) > 0)
		{
			$dBalance = $objDatabase->Result($varResultX, 0, "Balance");
		}
		
		$sQuery = "
		SELECT
			
			CA.ChartOfAccountsCategoryId AS 'CategoryId',   
			CA.ChartOfAccountsCode AS 'AccountCode',
			CA.AccountTitle AS 'AccountTitle',
			CA.ChartOfAccountsControlId AS 'ControlId',
			
			CCAT.CategoryName AS 'CategoryName',
			CC.ControlCode AS 'ControlCode',
			CC.ControlName AS 'ControlName',
			
			SUM(IF(GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate', GJE.Debit, 0)) AS 'Debit',
			SUM(IF(GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate', GJE.Credit, 0)) AS 'Credit'
			
			FROM fms_accounts_generaljournal AS GJ
			
			INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
			INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
			INNER JOIN fms_accounts_chartofaccounts_controls AS CC ON CC.ChartOfAccountsControlId = CA.ChartOfAccountsControlId
			INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId

		WHERE (CA.ChartOfAccountsCategoryId='5' OR CA.ChartOfAccountsCategoryId='4')  AND GJ.IsDeleted ='0' $sReportCriteriaSQL
		GROUP BY CA.ChartOfAccountsCategoryId, CA.ChartOfAccountsControlId
		ORDER BY CA.ChartOfAccountsCategoryId DESC
		";
		//Category Id = 3 Income & Category Id = 4 Expense & Category Id = 5 Owners Equity
			
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
			$iControlId = $objDatabase->Result($varResult, $i, "ControlId");
			$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");
			
			$sControlCode = $objDatabase->Result($varResult, $i, "ControlCode");
			$sControlName = $objDatabase->Result($varResult, $i, "ControlName");
			$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
			
			$dDebit = $objDatabase->Result($varResult, $i, "Debit");
			$dCredit = $objDatabase->Result($varResult, $i, "Credit");
			
			$dYear_Balance = ($dDebit > 0) ? ($dDebit - $dCredit) : ($dCredit - $dDebit);
			
			if (($i == 0) || ($iTemp != $iCategoryId))
			{
				if (($i > 0) && ($iTemp != $iCategoryId))
				{
					
					if($iShowBalance != 1)
					{
						$sReturn .= '
						<tr><td></td></tr>
						<tr>
						 <td class="Tahoma16">Balance as at beginning of the year</td>
						 <td align="right" class="Tahoma16">' . number_format($dBalance, 0) . '</td>
						</tr>';
						
						$iShowBalance = 1;
					}
					
					$sReturn .= '
					<tr>
					 <td align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total: </td>
					 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format(($dTotal_Year_Balance + $dBalance), 0) . '</td>
					</tr>';
					
					$dTotalIncome_Year = $dTotal_Year_Balance;
					$dTotal_Year_Balance = 0;
				}
					
				$sReturn .= '<tr><td align="left" colspan="4" style="font-family:Tahoma, Arial;font-size:18px;">' . (($i > 0) ? '<br />' : '') . strtoupper($sCategoryName) . '</td></tr>';
				$iTemp = $iCategoryId;
			}
			
			if (($i == 0) || ($iTemp2 != $iControlId))
			{
				$sReturn .= '
				<tr>
				 <td align="left" style="font-family:Tahoma, Arial;font-size:20px;">&nbsp;&nbsp;&nbsp;<a style="text-decoration:none; font-family:Tahoma, Arial;font-size:16px;" href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralLedger&selEmployee=-1&selStation=-1&selDonorProject=' . $iDonorProjectId . '&selChartOfAccountCategory=' . $iCategoryId . '&selChartOfAccountControl=' . $iControlId . '\', 800,600);" />' . $sControlName . '</a></td>
				 <td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear_Balance, 0) . '</td>
				</tr>';
				$iTemp2 = $iControlId;
			}
			
			$dTotal_Year_Balance += $dYear_Balance;
			$iTemp2 = $iControlId;
			
			//$dYear_Balance
			/*
			$sReturn .= '
			 <tr>
			  <td align="left" style="font-size:12px;font-family:Tahoma, Arial;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $sAccountTitle . '</td><td align="right" style="font-size:12px;font-family:Tahoma, Arial;">' . number_format($dYear_Balance, 0) . '</td>
			 </tr>
			';
			*/
		}
		
		$dTotalExpense_Year = $dTotal_Year_Balance;
		
		$sReturn .= '
		<tr>
		 <td align="left" style="font-family:Tahoma, Arial; font-size:15px;">Total: </td>
		 <td align="right" style="border-top:1px solid;font-family:Tahoma, Arial; font-size:15px;">' . number_format($dTotal_Year_Balance, 0) . '</td>
		</tr>';
		
		$sReturn .= '<tr><td><br /></td></tr>
		<tr>
		 <td style="font-family:Tahoma, Arial;font-size:18px;" align="left" colspan="1">Operating Result (Surplus/Deficit): </td>
		 <td style="border-bottom:4px double; font-family:Tahoma, Arial;font-size:18px;" align="right">' . number_format(($dTotalIncome_Year + $dBalance - $dTotalExpense_Year), 0) . '</td>
		</tr>';
		
		$sReturn .= '</table>';
		
		return($sReturn);
	}	
	
}

?>