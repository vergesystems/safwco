<?php

class clsFMS_Reports_Graphs extends clsFMS_Reports
{
	public $aColors;
	function __construct()
	{
		$this->aColors = array("AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "C07878", "78C08b", "C0b878", "7892c0");
	}
	
	function ReportFilter(&$sReportCriteria, &$sReportCriteriaSQL)
	{
		global $objGeneral;
		global $objDatabase;
				
		// Filters
		$iCriteria_EmployeeId = $objGeneral->fnGet("selEmployee");
		$iCriteria_StationId = $objGeneral->fnGet("selStation");
		$iCriteria_CustomerId = $objGeneral->fnGet("selCustomer");
		$iCriteria_CategoryId = $objGeneral->fnGet("selCategory");
		$iCriteria_ProductId = $objGeneral->fnGet("selProduct");		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");
		
		if ($iCriteria_EmployeeId == '') $iCriteria_EmployeeId = -1;
		if ($iCriteria_StationId == '') $iCriteria_StationId = -1;
		if ($iCriteria_CustomerId == '') $iCriteria_CustomerId = -1;
		if ($iCriteria_CategoryId == '') $iCriteria_CategoryId = -1;
		if ($iCriteria_ProductId == '') $iCriteria_ProductId = -1;
		
		
		$sReportCriteriaSQL = ($iCriteria_EmployeeId != -1) ? " AND E.EmployeeId='$iCriteria_EmployeeId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_StationId != -1) ? " AND C.StationId='$iCriteria_StationId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_CustomerId != -1) ? " AND C.CustomerId='$iCriteria_CustomerId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_CategoryId != -1) ? " AND PC.CategoryId='$iCriteria_CategoryId'" : '';
		$sReportCriteriaSQL .= ($iCriteria_ProductId != -1) ? " AND P.ProductId='$iCriteria_ProductId'" : '';	
		
		// Date Range
		$sCriteria_DateRange = date("F j, Y", strtotime($dCriteria_StartDate)) . ' - ' . date("F j, Y", strtotime($dCriteria_EndDate));

		// Employees
		if ($iCriteria_EmployeeId == -1 || !$iCriteria_EmployeeId) $sCriteria_Employee = "All Employees";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_employees AS E WHERE E.EmployeeId = '$iCriteria_EmployeeId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Employee Id...');
			$sCriteria_Employee = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
		}

		// Stations
		if ($iCriteria_StationId == -1 || !$iCriteria_StationId) $sCriteria_Station = "All Stations";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.StationId = '$iCriteria_StationId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Station Id...');
			$sCriteria_Station = $objDatabase->Result($varResult, 0, "S.StationName");
		}		
		// Customers
		if ($iCriteria_CustomerId == -1 || !$iCriteria_CustomerId) $sCriteria_Customer = "All Customer";
		else
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_customers AS C WHERE C.CustomerId = '$iCriteria_CustomerId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Customer Id...');
			$sCriteria_Customer= $objDatabase->Result($varResult, 0, "C.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "C.LastName");
		}
		
		$sReportCriteria .= '<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr><td>
		 <fieldset><legend style="font-size:13px; font-weight:bold;">Report Criteria:</legend>
		  <table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
		   <tr>
		    <td width="33%">Employee:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Employee . '</span></td>
			<td width="33%">Customer:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_Customer . '</span></td>
		   </tr>		   		  
		   <tr>		    
		    <td>Date Range:&nbsp;&nbsp;<span style="font-weight:bold;">' . $sCriteria_DateRange . '</span></td>
		   </tr>		   
		  </table>
		 </fieldset>
		 </td></tr>
		</table>';

		return(true);
	}
	
    function ShowGraphs($sReportName)
    {
    	global $objDatabase;
    	global $objEmployee;
    	$iEmployeeId = $objEmployee->iEmployeeId;
		
		
    	// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Graphs[0] == 0)
    		return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		include(cVSFFolder . '/classes/clsDHTMLSuite.php');

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Graphs</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';
		
		$sIncomeVsExpenditureGraph = '<a ' . (($sReportName == "IncomeVsExpenditureGraph") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/graphs_show.php?reportname=IncomeVsExpenditureGraph"><img src="../images/reports/iconGraphs_IncomeVsExpenditure.gif" border="0" alt="Income Vs. Expenditure Graph" title="Income Vs. Expenditure Graph" /><br />Income Vs. Expenditure<br />[ Bar Graph ]</a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Graphs_IncomeVSExpenditure[0] == 0)
			$sIncomeVsExpenditureGraph = '<img src="../images/inventory/iconGraphs_IncomeVsExpenditure_disabled.gif" border="0" alt="Income Vs. Expenditure Graph" title="Income Vs. Expenditure Graph" /><br />Income Vs. Expenditure<br />[ Bar Graph ]';
		
		$sSalesGraph = '<a ' . (($sReportName == "SalesGraph") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/graphs_show.php?reportname=SalesGraph"><img src="../images/reports/iconGraphs_SalesGraph.gif" border="0" alt="Sales Graph" title="Sales Graph" /><br />Sales Graph<br />[ Area Graph ]</a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Graphs_SalesGraph[0] == 0)
			$sSalesGraph = '<img src="../images/inventory/iconGraphs_SalesGraph_disabled.gif" border="0" alt="Income Vs. Expenditure Graph" title="Income Vs. Expenditure Graph" /><br />Income Vs. Expenditure<br />[ Bar Graph ]';
			
		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
		 <tr>
		  <td width="10%" valign="top" align="center">' . $sIncomeVsExpenditureGraph . '</td>
		  <td width="10%" valign="top" align="center">' . $sSalesGraph . '</td>
		 </tr>
		</table>
		<br />';

        $sReturn .= $this->ShowReportCriteria($sReportName);

		$sReturn .= '</td></tr></table></td></tr></table>';
		return($sReturn);
    }

    function ShowReportCriteria($sReportName)
    {			
    	switch($sReportName)
    	{   
			case "IncomeVsExpenditureGraph": $sReturn = $this->IncomeVsExpenditureGraphCriteria(); break;
			case "SalesGraph": $sReturn = $this->SalesGraphCriteria(); break;
    	}

    	$sReturn .= '
    	<script type="text/javascript" language="JavaScript">
		function GenerateReport(sReportName, sReportType)
		{
			var aReportCriteria;
			var sReportFilter;
			
			aReportCriteria = new Array();

			iEmployee = (GetVal("selEmployee") == "") ? -1 : GetVal("selEmployee");
			iStation = (GetVal("selStation") == "") ? -1 : GetVal("selStation");
			iCustomer = (GetVal("selCustomer") == "") ? -1 : GetVal("selCustomer");
			
			dDateRange_Start = (GetVal("txtDateRange_Start") == "") ? -1 : GetVal("txtDateRange_Start");
			dDateRange_End = (GetVal("txtDateRange_End") == "") ? -1 : GetVal("txtDateRange_End");

			sReportFilter = "&selEmployee=" + iEmployee;
			sReportFilter += "&selStation=" + iStation;
			sReportFilter += "&selCustomer=" + iCustomer;
			sReportFilter += "&txtDateRangeStart=" + dDateRange_Start;
			sReportFilter += "&txtDateRangeEnd=" + dDateRange_End;
			
			if ((sReportType == "IncomeVsExpenditureGraph") || (sReportType == "SalesGraph"))
				sReportFilter += "&txtYear=" + GetVal("txtYear");
				
			jsOpenWindow("../reports/showreport.php?report=" + sReportName + "&reporttype=" + sReportType + sReportFilter, 800,600);
		}
		</script>';

    	return($sReturn);
    }    

	function IncomeVsExpenditureGraphCriteria()
    {
		global $objEmployee;		
		
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersList[0] == 0)
		//	return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
					
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Income Vs. Expenditure Graph Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Year:</td><td>' . $this->GenerateReportCriteria("Year", "txtYear") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'Graphs\', \'IncomeVsExpenditureGraph\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
	function SalesGraphCriteria()
    {
		global $objEmployee;		
		
		//if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_CustomerReports_CustomersList[0] == 0)
		//	return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
					
        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Sales Graph Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">    	          
			   <tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			   <tr><td>Year:</td><td>' . $this->GenerateReportCriteria("Year", "txtYear") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'Graphs\', \'SalesGraph\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }

    function GenerateGraphs($sReportType, $bExportToExcel, &$aExcelData, $sAction = "")
    {
    	$sReport = $this->GenerateReportHeader($sAction);

    	switch ($sReportType)
    	{
			case "IncomeVsExpenditureGraph": $sReport .= $this->IncomeVsExpenditureGraph("", true, $bExportToExcel, $aExcelData); break;
			case "SalesGraph": $sReport .= $this->SalesGraph("", true, $bExportToExcel, $aExcelData); break;
    	}

    	//$sReport .= $this->GenerateReportFooter();
    	return($sReport);
    }

	function Graph_CurrentBudgetGraph()
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		// Some data
		// Data for plotting
		$dBudgetAmount = 0;
		$dBudgetUsed = 0;
		$dRemainingAmount = 0;

		$sQuery = "SELECT * FROM fms_accounts_budget AS B WHERE B.Status='0'";
		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i<$objDatabase->RowsNumber($varResult); $i++)
			{
				$iBudgetId = $objDatabase->Result($varResult, $i, "B.BudgetId");
				$dBudgetAmount += $objDatabase->Result($varResult, $i, "B.Amount");

				// Find Remaining Budget Amount
				// CategoryId = 4   // Expenses
				$varResult2 = $objDatabase->Query("
				SELECT
					SUM(GJE.Debit) AS 'UsedAmount'
				FROM fms_accounts_generaljournal_entries AS GJE
				INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
				INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
				INNER JOIN fms_accounts_budgetallocation AS BA ON BA.ChartOfAccountId = CA.ChartOfAccountsId
				WHERE GJE.BudgetId='$iBudgetId' AND CA.ChartOfAccountsCategoryId = '4'  AND GJ.IsDeleted ='0' ");
				for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
				{
					$dBudgetUsed += $objDatabase->Result($varResult2, 0, "UsedAmount");
				}
			}
		}
		if ($dBudgetAmount == 0) $dBudgetAmount = 0.1;

		$dRemainingAmount = $dBudgetAmount - $dBudgetUsed;

		$dRemainingBudgetPercentage = ($dRemainingAmount / $dBudgetAmount) * 100;
		$dBudgetUtilizedPercentage = 100 - $dRemainingBudgetPercentage;
		
		include_once(cVSFFolder . '/classes/clsCharts.php');
		$objCharts = new clsFusionCharts("Free");
		
		$aChartData[0][0] = number_format(($dBudgetAmount-$dRemainingAmount), 0);
		$aChartData[0][1] = $dBudgetUtilizedPercentage;
		//$aChartData[0][2] = "#ffda00";
		$aChartData[1][0] = number_format($dRemainingAmount, 0);
		$aChartData[1][1] = $dRemainingBudgetPercentage;
		//$aChartData[1][2] = "#0000FF";
				
		//$sReturn .= $objCharts->MSColumn3D($aData, "ChartHome_EmployeesDepartments", "Employees by Departments", "", 320, 320, '');
		$sReturn = $objCharts->PieChart3D($aChartData, "Chart_HomeBudgetUtilization", " Budget Graph", 450, 200, '');
		
		return($sReturn);
	}
	
	function IncomeVsExpenditureGraph($dYear, $iMonth, $bShowTitles = true)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($dYear == "") $dYear = $objGeneral->fnGet("txtYear");
		
		if ($bShowTitles)
			$sReturn .= '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Income Vs. Expenditure Graph</span><br /><span style="color:#6e6e6e; font-weight:bold; font-family:Arial;font-size:15px;">For the Year - ' . $dYear . '</span></div><br />';
		
		if($iMonth >= 7) 
		{
			$dYear2 = $dYear;
			$dYear = $dYear +1;
		}
		else 
		{			
			$dYear2 = $dYear -1;
		}		
		$aMonths = array("Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun");
		
		//Income
		/*
		$sQuery = "
		SELECT
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-07-01' AND '$dYear2-07-31', GJE.Credit, 0)) AS 'Jul_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-08-01' AND '$dYear2-08-31', GJE.Credit, 0)) AS 'Aug_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-09-01' AND '$dYear2-09-30', GJE.Credit, 0)) AS 'Sep_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-10-01' AND '$dYear2-10-31', GJE.Credit, 0)) AS 'Oct_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-11-01' AND '$dYear2-11-30', GJE.Credit, 0)) AS 'Nov_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-12-01' AND '$dYear2-12-31', GJE.Credit, 0)) AS 'Dec_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-01-01' AND '$dYear-01-31', GJE.Credit, 0)) AS 'Jan_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-02-01' AND '$dYear-02-29', GJE.Credit, 0)) AS 'Feb_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-03-01' AND '$dYear-03-31', GJE.Credit, 0)) AS 'Mar_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-04-01' AND '$dYear-04-30', GJE.Credit, 0)) AS 'Apr_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-05-01' AND '$dYear-05-31', GJE.Credit, 0)) AS 'May_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-06-01' AND '$dYear-06-30', GJE.Credit, 0)) AS 'Jun_Income'			
		FROM fms_accounts_generaljournal_entries AS GJE
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CAC ON CAC.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		WHERE CA.ChartOfAccountsCategoryId = '3' AND GJ.IsDeleted ='0' ";
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < count($aMonths); $i++)
				$aIncome[$i] = $objDatabase->Result($varResult, 0, $aMonths[$i] . "_Income");				
		}
		*/
		//Funds..
		$sQuery = "
		SELECT
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-07-01' AND '$dYear2-07-31', GJE.Credit, 0)) AS 'Jul_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-08-01' AND '$dYear2-08-31', GJE.Credit, 0)) AS 'Aug_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-09-01' AND '$dYear2-09-30', GJE.Credit, 0)) AS 'Sep_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-10-01' AND '$dYear2-10-31', GJE.Credit, 0)) AS 'Oct_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-11-01' AND '$dYear2-11-30', GJE.Credit, 0)) AS 'Nov_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-12-01' AND '$dYear2-12-31', GJE.Credit, 0)) AS 'Dec_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-01-01' AND '$dYear-01-31', GJE.Credit, 0)) AS 'Jan_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-02-01' AND '$dYear-02-29', GJE.Credit, 0)) AS 'Feb_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-03-01' AND '$dYear-03-31', GJE.Credit, 0)) AS 'Mar_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-04-01' AND '$dYear-04-30', GJE.Credit, 0)) AS 'Apr_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-05-01' AND '$dYear-05-31', GJE.Credit, 0)) AS 'May_Income',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-06-01' AND '$dYear-06-30', GJE.Credit, 0)) AS 'Jun_Income'			
		FROM fms_accounts_generaljournal_entries AS GJE
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CAC ON CAC.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		WHERE CA.ChartOfAccountsId = '100' AND GJ.IsDeleted ='0' ";
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < count($aMonths); $i++)
				$aIncome[$i] = $objDatabase->Result($varResult, 0, $aMonths[$i] . "_Income");				
		}

		//Expense
		$sQuery = "
		SELECT
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-07-01' AND '$dYear2-07-31', GJE.Debit, 0)) AS 'Jul_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-08-01' AND '$dYear2-08-31', GJE.Debit, 0)) AS 'Aug_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-09-01' AND '$dYear2-09-30', GJE.Debit, 0)) AS 'Sep_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-10-01' AND '$dYear2-10-31', GJE.Debit, 0)) AS 'Oct_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-11-01' AND '$dYear2-11-30', GJE.Debit, 0)) AS 'Nov_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear2-12-01' AND '$dYear2-12-31', GJE.Debit, 0)) AS 'Dec_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-01-01' AND '$dYear-01-31', GJE.Debit, 0)) AS 'Jan_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-02-01' AND '$dYear-02-29', GJE.Debit, 0)) AS 'Feb_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-03-01' AND '$dYear-03-31', GJE.Debit, 0)) AS 'Mar_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-04-01' AND '$dYear-04-30', GJE.Debit, 0)) AS 'Apr_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-05-01' AND '$dYear-05-31', GJE.Debit, 0)) AS 'May_Expense',
			SUM(IF(GJ.TransactionDate BETWEEN '$dYear-06-01' AND '$dYear-06-30', GJE.Debit, 0)) AS 'Jun_Expense'			
		FROM fms_accounts_generaljournal_entries AS GJE
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CAC ON CAC.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		WHERE CA.ChartOfAccountsCategoryId = '4' AND GJ.IsDeleted ='0' ";
		//die($sQuery);		
		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < count($aMonths); $i++)
				$aExpense[$i] = $objDatabase->Result($varResult, 0, $aMonths[$i] . "_Expense");
		}

		include_once(cVSFFolder . '/classes/clsCharts.php');
		$objCharts = new clsFusionCharts("Free");

        $aCategories = $aMonths;

		$aSeries[0] = array("Funds", "8BBA00");
		$aSeries[1] = array("Expense", "FF0000");
		
		for ($i=0; $i < count($aMonths); $i++)
		{
			$aDataSet[0][$i] = $aIncome[$i];
			$aDataSet[1][$i] = $aExpense[$i];
		}
		
		$sReturn .= $objCharts->MSColumn3D($aCategories, $aSeries, $aDataSet, "Chart_IncomeVsExpenditure", "Funds vs. Expenditure", "", 550, 320, $objEmployee->aSystemSettings["CurrencySign"]);
		$sReturn .= '<br />' . $sTable;
		
		return($sReturn);
	}

	function SalesGraph($dYear, $bShowTitles = true)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($dYear == "") $dYear = $objGeneral->fnGet("txtYear");
		
		if ($bShowTitles)
		{
			$sReturn .= '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">Sales Graph</span><br /><span style="color:#6e6e6e; font-weight:bold; font-family:Arial;font-size:15px;">For the Year - ' . $dYear . '</span></div><br />';
		}
		
		$aMonths = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

		$sQuery = "
		SELECT
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-01-01' AND '$dYear-01-31', CR.TotalAmount, 0)) AS 'Jan',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-02-01' AND '$dYear-02-28', CR.TotalAmount, 0)) AS 'Feb',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-03-01' AND '$dYear-03-31', CR.TotalAmount, 0)) AS 'Mar',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-04-01' AND '$dYear-04-30', CR.TotalAmount, 0)) AS 'Apr',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-05-01' AND '$dYear-05-31', CR.TotalAmount, 0)) AS 'May',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-06-01' AND '$dYear-06-30', CR.TotalAmount, 0)) AS 'Jun',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-07-01' AND '$dYear-07-31', CR.TotalAmount, 0)) AS 'Jul',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-08-01' AND '$dYear-08-31', CR.TotalAmount, 0)) AS 'Aug',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-09-01' AND '$dYear-09-30', CR.TotalAmount, 0)) AS 'Sep',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-10-01' AND '$dYear-10-31', CR.TotalAmount, 0)) AS 'Oct',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-11-01' AND '$dYear-11-30', CR.TotalAmount, 0)) AS 'Nov',
			SUM(IF(CR.ReceiptDate BETWEEN '$dYear-12-01' AND '$dYear-12-31', CR.TotalAmount, 0)) AS 'Dec'
		FROM fms_customers_receipts AS CR";

		//die($sQuery);		
		$varResult = $objDatabase->Query($sQuery);
		if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < count($aMonths); $i++)
			{
				$aChartData[$i][0] = $aMonths[$i];
				$aChartData[$i][1] = $objDatabase->Result($varResult, 0, $aMonths[$i]);
			}
		}

		include_once(cVSFFolder . '/classes/clsCharts.php');
		$objCharts = new clsCharts();

		$sReturn .= $objCharts->Area2D($aChartData, "Chart_MonthlyCustomerReceipts", "Monthly Customer Sales", "For the Year " . date("Y"), 550, 320, "Months", $objEmployee->aSystemSettings["CurrencySign"]);
		
		return($sReturn);
	}
}
?>