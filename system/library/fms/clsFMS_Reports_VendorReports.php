<?php

class clsFMS_Reports_VendorReports extends clsFMS_Reports
{
	public $aReportFilterCriteria;
	public $aFixedAssetDepreciationMethod;

	function __construct()
	{
		$this->aFixedAssetDepreciationMethod = array("Straight-line Method", "Decline Method");
	}

	function ShowVendorReports($sReportName)
    {
    	global $objDatabase;
    	global $objEmployee;
    	$iEmployeeId = $objEmployee->iEmployeeId;
    	
		$sVendorsReport = '<a ' . (($sReportName == "Vendors") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/vendorreports_show.php?reportname=Vendors"><img src="../images/vendors/iconVendors.gif" border="0" alt="Vendors Report" title="Vendors Report" /><br />Vendors Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_VendorsReport[0] == 0)
    		$sVendorsReport = '<img src="../images/vendors/iconVendor_disabled.gif" border="0" alt="Vendors Report" title="Vendors Report" /><br />Vendors Report';
    	
		$sFixedAssetsReport = '<a ' . (($sReportName == "FixedAssets") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/vendorreports_show.php?reportname=FixedAssets"><img src="../images/vendors/iconFixedAssets.gif" border="0" alt="Fixed Assets Report" title="Fixed Assets Report" /><br />Fixed Assets Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetsReport[0] == 0)
    		$sFixedAssetsReport = '<img src="../images/vendors/iconFixedAssets_disabled.gif" border="0" alt="Fixed Assets Report" title="Fixed Assets Report" /><br />Fixed Assets Report';
		
		$sVendorsLedgerReport = '<a ' . (($sReportName == "VendorsLedger") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/vendorreports_show.php?reportname=VendorsLedger"><img src="../images/vendors/iconVendors.gif" border="0" alt="VendorsLedger Report" title="Vendors Ledger Report" /><br />Vendors Ledger Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_VendorsLedgerReport[0] == 0)
    		$sVendorsLedgerReport = '<img src="../images/vendors/iconVendor_disabled.gif" border="0" alt="Vendors Ledger Report" title="Vendors Ledger Report" /><br />Vendors Ledger Report';
    	
		$sFixedAssetBarCodesReport = '<a ' . (($sReportName == "FixedAssetBarcodes") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/vendorreports_show.php?reportname=FixedAssetBarcodes"><img src="../images/vendors/iconFixedAssets.gif" border="0" alt="Fixed Asset Barcodes" title="Fixed Asset Barcodes" /><br />Fixed Asset Barcodes</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetBarcodesReport[0] == 0)
    		$sFixedAssetBarCodesReport = '<img src="../images/vendors/iconFixedAssets_disabled.gif" border="0" alt="FixedAssets Barcodes Report" title="FixedAsset Barcodes Report" /><br />FixedAsset Barcodes Report';
			
		$sPurchaseRequestsReport = '<a ' . (($sReportName == "PurchaseRequests") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/vendorreports_show.php?reportname=PurchaseRequests"><img src="../images/vendors/iconPurchaseRequests.gif" border="0" alt="Purchase Requests Report" title="Purchase Requests Report" /><br />Purchase Requests Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_PurchaseRequestsReport[0] == 0)
    		$sPurchaseRequestsReport = '<img src="../images/vendors/iconPurchaseRequests_disabled.gif" border="0" alt="Purchase Requests Report" title="Purchase Requests Report" /><br />Purchase Requests Report';
    	
		$sQuotationsReport = '<a ' . (($sReportName == "Quotations") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/vendorreports_show.php?reportname=Quotations"><img src="../images/vendors/iconQuotations.gif" border="0" alt="Quotations Report" title="Quotations Report" /><br />Quotations Report</a>';		
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_QuotationsReport[0] == 0)
    		$sQuotationsReport = '<img src="../images/vendors/iconQuotations_disabled.gif" border="0" alt="Quotations Report" title="Quotations Report" /><br />Quotations Report';
    	
		$sFixedAssetDepreciationReport = '<a ' . (($sReportName == "FixedAssetDepreciation") ? 'style="font-weight:bold; color:blue; text-decoration:underline;"' : '') . ' href="../reports/vendorreports_show.php?reportname=FixedAssetDepreciation"><img src="../images/vendors/iconFixedAssets.gif" border="0" alt="Fixed Asset Depreciation Report" title="Fixed Asset Depreciation Report" /><br />Fixed Asset Depreciation Report</a>';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetDepreciationReport[0] == 0)
    		$sFixedAssetDepreciationReport = '<img src="../images/vendors/iconFixedAssets_disabled.gif" border="0" alt="Fixed Asset Depreciation Report" title="Fixed Asset Depreciation Report" /><br />Fixed Asset Depreciation Report';
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Vendor Reports</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';
		
		$sReturn .= '<br />
		<table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
		 <tr>
		  <td width="10%" valign="top" align="center">' . $sVendorsReport . '</td>
		  <td width="10%" valign="top" align="center">' . $sFixedAssetsReport . '</td>
		  <td width="10%" valign="top" align="center">' . $sFixedAssetBarCodesReport . '</td>
		  <td width="10%" valign="top" align="center">' . $sVendorsLedgerReport . '</td>
		  <td width="10%" valign="top" align="center">' . $sFixedAssetDepreciationReport . '</td>
		 <!--
		  <td width="10%" valign="top" align="center">' . $sPurchaseRequestsReport . '</td>
		  <td width="10%" valign="top" align="center">' . $sQuotationsReport . '</td>
		 -->
		 </tr>
		</table>
		<br />';
		
		$sReturn .= $this->ShowReportCriteria($sReportName);

		$sReturn .= '</td></tr></table></td></tr></table>';
		return($sReturn);
    }

    function ShowReportCriteria($sReportName)
    {		
    	switch($sReportName)
    	{
    		case "Vendors": $sReturn = $this->VendorsCriteria(); break;
			case "FixedAssets": $sReturn = $this->FixedAssetsCriteria(); break;
			case "FixedAssetBarcodes": $sReturn = $this->FixedAssetBarcodesCriteria(); break;
			case "VendorsLedger": $sReturn = $this->VendorsLedgerCriteria(); break;
			case "PurchaseRequests": $sReturn = $this->PurchaseRequestsCriteria(); break;
			case "Quotations": $sReturn = $this->QuotationsCriteria(); break;
			case "FixedAssetDepreciation": $sReturn = $this->FixedAssetDepreciationCriteria(); break;
    	}

    	$sReturn .= '
    	<script type="text/javascript" language="JavaScript">
		function GenerateReport(sReportName, sReportType)
		{
			var aReportCriteria;
			var sReportFilter;

			aReportCriteria = new Array();

			iCategory = (GetVal("selCategory") == "") ? -1 : GetVal("selCategory");
			iVendor = (GetVal("selVendor") == "") ? -1 : GetVal("selVendor");
			//iStation = (GetVal("selStation") == "") ? -1 : GetVal("selStation");
			iManufacturer = (GetVal("selManufacturer") == "") ? -1 : GetVal("selManufacturer");
			
			dDateRange_Start = (GetVal("txtDateRange_Start") == "") ? -1 : GetVal("txtDateRange_Start");
			dDateRange_End = (GetVal("txtDateRange_End") == "") ? -1 : GetVal("txtDateRange_End");

			sReportFilter = "&selCategory=" + iCategory;
			sReportFilter += "&selVendor=" + iVendor;
			//sReportFilter += "&selStation=" + iStation;
			sReportFilter += "&selManufacturer=" + iManufacturer;
			sReportFilter += "&txtDateRangeStart=" + dDateRange_Start;
			sReportFilter += "&txtDateRangeEnd=" + dDateRange_End;
			
			if (sReportType == "FixedAssetDepreciation")
				sReportFilter += "&txtDate=" + GetVal("txtDate");
							
			jsOpenWindow("../reports/showreport.php?report=" + sReportName + "&reporttype=" + sReportType + sReportFilter, 800,600);
		}
		</script>';

    	return($sReturn);
    }
 
    function VendorsCriteria()
    {
		global $objEmployee;		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_VendorsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
			
		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
			  <tr>
			   <td>
			    <fieldset><legend style="font-weight:bold; font-size:14px;">Vendors Report Criteria:</legend>
			     <br />
			      <form>
			      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">			       
				  <!--
			        <tr><td style="width:160px;">Employee:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>			       
					<tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			        -->
				   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange", 0) . '</td></tr>
			       <tr><td></td><td><br /><input onclick="GenerateReport(\'VendorReports\', \'Vendors\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
			      </table>
			      </form>
			     <br />
			    </fieldset>
			   </td>
			  </td>
			 </table><br />';

		return ($sReturn);

    }
    
	function PurchaseRequestsCriteria()
    {
		global $objEmployee;		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_PurchaseRequestsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
			  <tr>
			   <td>
			    <fieldset><legend style="font-weight:bold; font-size:14px;">Purchase Requests Report Criteria:</legend>
			     <br />
			      <form>
			      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			        <tr><td style="width:160px;">Employee:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>
			        <!--
					<tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			        -->
				   <tr><td>Vendor:</td><td>' . $this->GenerateReportCriteria("Vendor", "selVendor") . '</td></tr>
				   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange", 0) . '</td></tr>
			       <tr><td></td><td><br /><input onclick="GenerateReport(\'VendorReports\', \'PurchaseRequests\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
			      </table>
			      </form>
			     <br />
			    </fieldset>
			   </td>
			  </td>
			 </table><br />';

		return ($sReturn);

    }
	
	function QuotationsCriteria()
    {
		global $objEmployee;		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_QuotationsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
			  <tr>
			   <td>
			    <fieldset><legend style="font-weight:bold; font-size:14px;">Quotations Report Criteria:</legend>
			     <br />
			      <form>
			      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			        <tr><td style="width:160px;">Employee:</td><td>' . $this->GenerateReportCriteria("Employee", "selEmployee") . '</td></tr>
			        <!--
					<tr><td>Station:</td><td>' . $this->GenerateReportCriteria("Station", "selStation") . '</td></tr>
			        -->
				   <tr><td>Vendor:</td><td>' . $this->GenerateReportCriteria("Vendor", "selVendor") . '</td></tr>
				   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange", 0) . '</td></tr>
			       <tr><td></td><td><br /><input onclick="GenerateReport(\'VendorReports\', \'Quotations\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
			      </table>
			      </form>
			     <br />
			    </fieldset>
			   </td>
			  </td>
			 </table><br />';

		return ($sReturn);

    }
	
	function FixedAssetsCriteria()
    {
		global $objEmployee;		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_PurchaseRequestsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
			  <tr>
			   <td>
			    <fieldset><legend style="font-weight:bold; font-size:14px;">Fixed Assets Report Criteria:</legend>
			     <br />
			      <form>
			      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
				   <tr><td>Category:</td><td>' . $this->GenerateReportCriteria("Category", "selCategory") . '</td></tr>
				   <tr><td>Vendor:</td><td>' . $this->GenerateReportCriteria("Vendor", "selVendor") . '</td></tr>
				   <tr><td>Manufacturer:</td><td>' . $this->GenerateReportCriteria("Manufacturer", "selManufacturer") . '</td></tr>
				   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange", 0) . '</td></tr>
			       <tr><td></td><td><br /><input onclick="GenerateReport(\'VendorReports\', \'FixedAssets\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
			      </table>
			      </form>
			     <br />
			    </fieldset>
			   </td>
			  </td>
			 </table><br />';

		return ($sReturn);

    }
	
	function FixedAssetBarcodesCriteria()
    {
		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
			  <tr>
			   <td>
			    <fieldset><legend style="font-weight:bold; font-size:14px;">Fixed Asset Barcodes Report Criteria:</legend>
			     <br />
			      <form>
			      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			       <tr><td>Category:</td><td>' . $this->GenerateReportCriteria("Category", "selCategory") . '</td></tr>
				   <tr><td>Vendor:</td><td>' . $this->GenerateReportCriteria("Vendor", "selVendor") . '</td></tr>
				   <tr><td>Manufacturer:</td><td>' . $this->GenerateReportCriteria("Manufacturer", "selManufacturer") . '</td></tr>
			       <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange", 0) . '</td></tr>
			       <tr><td></td><td><br /><input onclick="GenerateReport(\'VendorReports\', \'FixedAssetBarcodes\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
			      </table>
			      </form>
			     <br />
			    </fieldset>
			   </td>
			  </td>
			 </table>';	
		
		return ($sReturn);
    }
	
	function VendorsLedgerCriteria()
    {
		global $objEmployee;		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_PurchaseRequestsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$sReturn = '<table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
			  <tr>
			   <td>
			    <fieldset><legend style="font-weight:bold; font-size:14px;">Vendors Ledger Report Criteria:</legend>
			     <br />
			      <form>
			      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">				   
				   <tr><td>Vendor:</td><td>' . $this->GenerateReportCriteria("Vendor", "selVendor") . '</td></tr>				   
				   <tr><td>Date Range:</td><td>' . $this->GenerateReportCriteria("DateRange", "txtDateRange", 0) . '</td></tr>
			       <tr><td></td><td><br /><input onclick="GenerateReport(\'VendorReports\', \'VendorsLedger\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
			      </table>
			      </form>
			     <br />
			    </fieldset>
			   </td>
			  </td>
			 </table><br />';

		return ($sReturn);

    }
	
	function FixedAssetDepreciationCriteria()
    {
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_AccountsReports_ServicesReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

        $sReturn = '
        <table border="0" cellspacing="0" cellpadding="3" width="70%" align="center">
		 <tr>
    	   <td>
    	    <fieldset><legend style="font-weight:bold; font-size:14px;">Fixed Asset Depreciation Report Criteria:</legend>
    	     <br />
    	      <form>
    	      <table border="0" cellspacing="0" cellpadding="5" width="90%" align="center">
			   <tr><td>Category:</td><td>' . $this->GenerateReportCriteria("Category", "selCategory") . '</td></tr>
			   <tr><td>Vendor:</td><td>' . $this->GenerateReportCriteria("Vendor", "selVendor") . '</td></tr>
			   <tr><td>Manufacturer:</td><td>' . $this->GenerateReportCriteria("Manufacturer", "selManufacturer") . '</td></tr>
			   <tr><td>As Of:</td><td>' . $this->GenerateReportCriteria("Date", "txtDate") . '</td></tr>
			   <tr><td></td><td><br /><input onclick="GenerateReport(\'VendorReports\', \'FixedAssetDepreciation\');" type="button" class="AdminFormButton1" value="Generate" /></td></tr>
    	      </table>
    	      </form>
    	     <br />
    	    </fieldset><br />
    	   </td>
    	  </td>
    	 </table>';

         return($sReturn);
    }
	
    function GenerateVendorReports($sReportType, $bExportToExcel, &$aExcelData,  $sAction = "")
    {
    	$sReport = $this->GenerateReportHeader($sAction);

    	switch ($sReportType)
    	{
    		case "Vendors": $sReport .= $this->VendorsReport($bExportToExcel, $aExcelData); break;
			case "FixedAssets": $sReport .= $this->FixedAssetsReport($bExportToExcel, $aExcelData); break;
			case "FixedAssetBarcodes": $sReport .= $this->FixedAssetBarcodesReport($bExportToExcel, $aExcelData); break;
			case "VendorsLedger": $sReport .= $this->VendorsLedgerReport($bExportToExcel, $aExcelData); break;
			case "PurchaseRequests": $sReport .= $this->PurchaseRequestsReport($bExportToExcel, $aExcelData); break;
			case "Quotations": $sReport .= $this->QuotationsReport($bExportToExcel, $aExcelData); break;
			case "FixedAssetDepreciation": $sReport .= $this->FixedAssetDepreciationReport($bExportToExcel, $aExcelData); break;			
    	}

    	//$sReport .= $this->GenerateReportFooter();
    	return($sReport);
    }
	
	function VendorsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_VendorsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (V.VendorAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">Vendors Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_vendors AS V
		INNER JOIN organization_employees AS E ON E.EmployeeId = V.VendorAddedBy
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr bgcolor="#bfbfbf">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Vendor Name </td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Vendor Code</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Country</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">State</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">City</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Zip Code</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Address</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Phone Number</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Description</td>
	     </tr>
	     </thead>';

	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iVendorId = $objDatabase->Result($varResult, $i, "V.VendorId");
				$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");
				$sVendorCode = $objDatabase->Result($varResult, $i, "V.VendorCode");
				$sCountry = $objDatabase->Result($varResult, $i, "V.Country");
				$sState = $objDatabase->Result($varResult, $i, "V.State");
				$sZipCode = $objDatabase->Result($varResult, $i, "V.ZipCode");
				$sPhoneNumber = $objDatabase->Result($varResult, $i, "V.PhoneNumber");
				$sAddress = $objDatabase->Result($varResult, $i, "V.Address");
				$sCity = $objDatabase->Result($varResult, $i, "V.City");				
				$sDescription = $objDatabase->Result($varResult, $i, "V.VendorDescription");

				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sVendorName . '</td>
				 <td align="left">' . $sVendorCode . '</td>
				 <td align="left">' . $sCountry . '</td>
				 <td align="left">' . $sState . '&nbsp;</td>
				 <td align="left">' . $sCity . '&nbsp;</td>
				 <td align="left">' . $sZipCode . '&nbsp;</td>
				 <td align="left">' . $sAddress . '&nbsp;</td>
				 <td align="left">' . $sPhoneNumber . '&nbsp;</td>
				 <td align="left">' . $sDescription . '&nbsp;</td>
				</tr>';
			}
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	
	function PurchaseRequestsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_PurchaseRequestsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (PR.PurchaseRequestAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">Purchase Requests Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_vendors_purchaserequests AS PR
		INNER JOIN organization_employees AS E ON E.EmployeeId = PR.PurchaseRequestAddedBy
		INNER JOIN fms_vendors AS V ON V.VendorId = PR.VendorId
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Purchase Request No</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">FixedAsset Name</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Requested On</td>
	      <td align="center" style="font-weight:bold; font-size:12px;">Qty</td>
	      <td align="center" style="font-weight:bold; font-size:12px;">Weight</td>
	      <td align="right" style="font-weight:bold; font-size:12px;">Cost</td>
	     </tr>';

	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iPurchaseRequestId = $objDatabase->Result($varResult, $i, "PR.PurchaseRequestId");
				$sPurchaseRequestNumber = $objDatabase->Result($varResult, $i, "PR.PurchaseRequestNumber");
				//$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");
				//$sVendorCode = $objDatabase->Result($varResult, $i, "V.VendorCode");
				$sFixedAssetName = $objDatabase->Result($varResult, $i, "PR.FixedAssetName");
				$dFixedAssetCost = $objDatabase->Result($varResult, $i, "PR.FixedAssetCost");
				$sFixedAssetCost = number_format($dFixedAssetCost, 2);
				$iFixedAssetQuantity = $objDatabase->Result($varResult, $i, "PR.FixedAssetQuantity");
				$sFixedAssetQuantity = $iFixedAssetQuantity;
				$dFixedAssetWeight = $objDatabase->Result($varResult, $i, "PR.FixedAssetWeight");
				$sFixedAssetWeight = number_format($dFixedAssetWeight, 2);
				$dPurchaseRequestAddedOn = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestAddedOn");
				$sPurchaseRequestAddedOn = date("F j, Y", strtotime($dPurchaseRequestAddedOn)) . ' at ' . date("g:i a", strtotime($dPurchaseRequestAddedOn));
				
				
				$dGrandTotal += $dFixedAssetCost;
				
				$sGrandTotal = number_format($dGrandTotal, 2);
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sPurchaseRequestNumber . '</td>
				 <td align="left">' . $sFixedAssetName . '</td>
				 <td align="left">' . $sPurchaseRequestAddedOn . '&nbsp;</td>
				 <td align="center">' . $sFixedAssetQuantity . '&nbsp;</td>
				 <td align="center">' . $sFixedAssetWeight . '&nbsp;</td>
				 <td align="right">' . $sFixedAssetCost . '&nbsp;</td>
				</tr>';
			}
			$sReturn .= '<tr class="GridReport">
			 <td colspan="5" align="right" style="font-weight:bold;">Total:</td>
			 <td colspan="2" align="right" style="font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . $sGrandTotal . '</td>
			</tr>';
	    }
	    else
	    	$sReturn .= '<tr><td colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	
	function QuotationsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_QuotationsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (Q.QuotationAddedOn BETWEEN '$dCriteria_StartDate 00:00:00' AND '$dCriteria_EndDate 23:59:59')";

		$sReturn = '<div align="center"><span class="ReportTitle">Quotations Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_vendors_quotations AS Q
		INNER JOIN fms_vendors_purchaserequests AS PR ON PR.PurchaseRequestId = Q.PurchaseRequestId
		INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy
		INNER JOIN fms_vendors AS V ON V.VendorId = PR.VendorId
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <tr style="background-color:#e1e1e1;">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Quotation No</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Quotation Title</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Quotation Date</td>
		  <td align="left" style="font-weight:bold; font-size:12px;">Added On</td>
	      <td align="right" style="font-weight:bold; font-size:12px;">Total Amount</td>
	     </tr>';

	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$sQuotationTitle = $objDatabase->Result($varResult, $i, "Q.QuotationTitle");
				
				$dTotalAmount = $objDatabase->Result($varResult, $i, "Q.TotalAmount");
				$sTotalAmount = number_format($dTotalAmount, 2);
				
				$dGrandTotal += $dTotalAmount;
				$sGrandTotal = number_format($dGrandTotal, 2);
				
				$sQuotationNumber = $objDatabase->Result($varResult, $i, "Q.QuotationNumber");
				
				$dQuotationAddedOn = $objDatabase->Result($varResult, 0, "Q.QuotationAddedOn");
				$sQuotationAddedOn = date("F j, Y", strtotime($dQuotationAddedOn)) . ' at ' . date("g:i a", strtotime($dQuotationAddedOn));
				
				$dQuotationDate = $objDatabase->Result($varResult, 0, "Q.QuotationDate");
				$sQuotationDate = date("F j, Y", strtotime($dQuotationDate)) . ' at ' . date("g:i a", strtotime($dQuotationDate));
				
				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sQuotationNumber . '</td>
				 <td align="left">' . $sQuotationTitle . '&nbsp;</td>
				 <td align="left">' . $sQuotationDate . '&nbsp;</td>
				 <td align="left">' . $sQuotationAddedOn . '&nbsp;</td>
				 <td align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . $sTotalAmount . '&nbsp;</td>				 				 
				</tr>';
			}
			
			$sReturn .= '<tr class="GridReport">
			 <td align="right" colspan="4" style="font-weight:bold;">Total:</td>
			 <td align="right" colspan="2" style="font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . $sGrandTotal . '</td>
			</tr>';
	    }		
	    else
	    	$sReturn .= '<tr><td colspan="6" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	
	function FixedAssetsReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include('../../system/library/fms/clsFMS_Vendors_FixedAssets.php');
		$objProuduts = new clsVendors_FixedAssets();
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetsReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (P.FixedAssetPurchaseDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">Fixed Assets Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_vendors_fixedassets AS P
		INNER JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = P.CategoryId
		LEFT JOIN fms_vendors AS V ON V.VendorId = P.VendorId
        LEFT JOIN fms_vendors_fixedassets_manufacturers AS M ON M.ManufacturerId = P.ManufacturerId
		WHERE 1=1 $sReportDateTimeCriteria $sReportCriteriaSQL";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);

		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		 <thead>
	     <tr bgcolor="#bfbfbf">
          <td align="center" style="font-weight:bold; font-size:12px;">S#</td>
          <td align="left" style="font-weight:bold; font-size:12px;">Vendor Name </td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Manufacturer</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">FixedAsset Name</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Code</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Stock Number</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Cost</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Purhcase Date</td>
	      <td align="left" style="font-weight:bold; font-size:12px;">Status</td>
	     </tr>
	     </thead>';

	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{				
				$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");
				$sManufacturerName = $objDatabase->Result($varResult, $i, "M.ManufacturerName");
				$sVendorCode = $objDatabase->Result($varResult, $i, "V.VendorCode");
				$sFixedAssetName = $objDatabase->Result($varResult, $i, "P.FixedAssetName");
				$sFixedAssetCode = $objDatabase->Result($varResult, $i, "P.FixedAssetCode");
				$sFixedAssetStockNumber = $objDatabase->Result($varResult, $i, "P.FixedAssetStockNumber");
				$dFixedAssetCost = $objDatabase->Result($varResult, $i, "P.FixedAssetCost");
				$sFixedAssetCost = number_format($dFixedAssetCost, 2);
				$dFixedAssetPurchaseDate = $objDatabase->Result($varResult, $i, "P.FixedAssetPurchaseDate");
				$sFixedAssetPurchaseDate = date("F j, Y", strtotime($dFixedAssetPurchaseDate));
				$iFixedAssetStatus = $objDatabase->Result($varResult, $i, "P.FixedAssetStatus");
				$sFixedAssetStatus = $objProuduts->aFixedAssetStatus[$iFixedAssetStatus];

				$sReturn .= '<tr>
				 <td align="center">' . ($i+1) . '</td>
				 <td align="left">' . $sVendorName . '</td>
				 <td align="left">' . $sManufacturerName . '</td>
				 <td align="left">' . $sFixedAssetName . '</td>
				 <td align="left">' . $sFixedAssetCode . '&nbsp;</td>
				 <td align="left">' . $sFixedAssetStockNumber . '&nbsp;</td>
				 <td align="left">' . $sFixedAssetCost . '&nbsp;</td>
				 <td align="left">' . $sFixedAssetPurchaseDate . '&nbsp;</td>
				 <td align="left">' . $sFixedAssetStatus . '&nbsp;</td>				 
				</tr>';
			}
	    }
	    else
	    	$sReturn .= '<tr><td style="font-family:Tahoma;font-size:20px;" colspan="10" align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';

		$sReturn .= '</table>';

		return($sReturn);
	}
	
	/* FixedAsset Barcodes Report */
	function FixedAssetBarcodesReport()
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		//$sReturn = '<br /><div align="center"><span class="ReportTitle">FixedAsset Barcodes</span></div>';

		$sReportDateTimeCriteria .= " AND (P.FixedAssetPurchaseDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		//$sReturn .= $sReportCriteria;

		$sQuery = "
		SELECT
		 *
		FROM fms_vendors_fixedassets AS P
		INNER JOIN fms_vendors_fixedassets_categories AS PC ON PC.CategoryId = P.CategoryId
		INNER JOIN fms_vendors AS V ON V.VendorId = P.VendorId
		INNER JOIN fms_vendors_fixedassets_manufacturers AS M ON M.ManufacturerId = P.ManufacturerId		
		WHERE 1=1 $sReportCriteriaSQL $sReportDateTimeCriteria";

		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		
		$sBarcodeImagesPath = $sBarcodeImage = cDataFolder . '/vf/vendors/barcodes/';
		$iCounter = 1;

		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$sReturn .= '<br /><table cellspacing="0" cellpadding="20" width="98%" align="center">
			 <tr>';

			$iTemp = 0;
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iFixedAssetId = $objDatabase->Result($varResult, $i, "P.FixedAssetId");
				$sFixedAssetName = $objDatabase->Result($varResult, $i, "P.FixedAssetName");
				$dFixedAssetCost = $objDatabase->Result($varResult, $i, "P.FixedAssetCost");
				$sFixedAssetCost = number_format($dFixedAssetCost, 2);
				$sBarcodeImage = $sBarcodeImagesPath . $iFixedAssetId . '.jpg';
				
				// border style: style="border-color:#000000; border-style:dashed; border-width:1px 1px 0 0;" 
				
				$sReturn .= '<td align="center" width="33%">
				<table cellspacing="0" cellpadding="0">
				<tr><td align="center">' . $sFixedAssetName . '</td></tr>
				<tr><td align="center"><img src="' . $sBarcodeImage . '" border="0" /></td></tr>
				<tr><td align="center" style="font-size:14px; font-weight:bold;">' . $objEmployee->aSystemSettings['CurrencySign'] . $sFixedAssetCost . '</td></tr>
				</table>
				</td>';
				
				$iTemp++;
				if (($iCounter % 6) == 0)
				{
					$sReturn .= '</tr><tr>';
					$iTemp = 0;
				}

				$iCounter++;				
			}
			
			$sReturn .= '</table>';
		}
		else
			$sReturn .= '<br /><br /><div style="font-family:Tahoma;font-size:20px;" align="center">Sorry, no fixedassets found!</div><br />';

		return($sReturn);
	}
	
	/* Vendors Ledger Report */
	function VendorsLedgerReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		/*
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_BankingReports_BanksReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		*/

		$dCriteria_StartDate = $objGeneral->fnGet("txtDateRangeStart");
		$dCriteria_EndDate = $objGeneral->fnGet("txtDateRangeEnd");

		$sReportDateTimeCriteria .= " AND (GJ.TransactionDate BETWEEN '$dCriteria_StartDate' AND '$dCriteria_EndDate')";

		$sReturn = '<div align="center"><span class="ReportTitle">Vendors Ledger Report</span></div>';


		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
	
		$sReportCriteriaSQL = str_replace("S.","GJE.",$sReportCriteriaSQL);		
		
		// ChartOfAccountsCategoryId 5 = Equity
		$sQuery = "
		SELECT
		 V.VendorName AS 'VendorName',
		 V.VendorId AS 'VendorId',
		 CA.ChartOfAccountsCode AS 'ChartOfAccountsCode',
		 CA.AccountTitle AS 'AccountTitle',
		 GJ.Reference AS 'Reference',
		 GJ.CheckNumber AS 'CheckNumber',
		 GJ.TransactionDate AS 'TransactionDate',
		 GJE.Detail AS 'Detail',
		 GJE.Debit AS 'Debit',
		 GJE.Credit AS 'Credit'
		FROM fms_accounts_generaljournal AS GJ
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.GeneralJournalId = GJ.GeneralJournalId
		INNER JOIN fms_vendors AS V ON V.VendorId = GJE.VendorId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
		INNER JOIN fms_accounts_chartofaccounts_categories AS CCAT ON CCAT.ChartOfAccountsCategoryId = CA.ChartOfAccountsCategoryId
		INNER JOIN organization_employees AS E ON E.EmployeeId = GJ.GeneralJournalAddedBy		
		WHERE 1=1 AND CA.ChartOfAccountsCategoryId !='5'  AND GJ.IsDeleted ='0' $sReportCriteriaSQL $sReportDateTimeCriteria
		ORDER BY V.VendorId";
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		$iTempVendorId = 0;
	    if ($objDatabase->RowsNumber($varResult) > 0)
	    {
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$sVendorName = $objDatabase->Result($varResult, $i, "VendorName");
				$iVendorId = $objDatabase->Result($varResult, $i, "VendorId");				
				$sAccountTitle = $objDatabase->Result($varResult, $i, "AccountTitle");
				$sChartOfAccountsCode = $objDatabase->Result($varResult, $i, "ChartOfAccountsCode");
				$sReference = $objDatabase->Result($varResult, $i, "Reference");
				$sCheckNumber = $objDatabase->Result($varResult, $i, "CheckNumber");
				$sCheckNumber = (($sCheckNumber != 0) ? $sCheckNumber : ' ');
				$dTransactionDate = $objDatabase->Result($varResult, $i, "TransactionDate");
				$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
				$sDetail = $objDatabase->Result($varResult, $i, "Detail");
				$dDebit = $objDatabase->Result($varResult, $i, "Debit");
				$dCredit = $objDatabase->Result($varResult, $i, "Credit");
				if($i == 0)
				{
					$sReturn .= '<br />
					<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
					<tr>
					 <td style="font-family:Tahoma, Arial; font-size:22px;" align="left" colspan="6">Ledger of - ' . $sVendorName . '</td>
					</tr>				
					<tr>
					 <td align="center" style="border-bottom:1px solid; width:25px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">S#</td>
					 <td align="left" style="border-bottom:1px solid; width:120px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Date</td>
					 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Ref No.</td>
					 <td align="left" style="border-bottom:1px solid; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Description</td>
					 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Check No#</td>
					 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Debit</td>
					 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Credit</td>
					</tr>';
				}

				if ($i > 0 && $iTempVendorId != $iVendorId)
				{					
					$sReturn .= '<tr>
					 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" colspan="5" align="right">Total</td>
					 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Debit, 0) . '</td>
					 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Credit, 0) . '</td>					 
					</tr>';
					
					$sReturn .= '<br /><br /><div style="page-break-after:always"></div>';
					
					$sReturn .= '<br />
					<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center">
					<tr>
					 <td style="font-family:Tahoma, Arial; font-size:22px;" align="left" colspan="6">Ledger of - ' . $sVendorName . '</td>
					</tr>				
					<tr>
					 <td align="center" style="border-bottom:1px solid; width:25px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">S#</td>
					 <td align="left" style="border-bottom:1px solid; width:120px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Date</td>
					 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Ref No.</td>
					 <td align="left" style="border-bottom:1px solid; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Description</td>
					 <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Check No#</td>
					 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Debit</td>
					 <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Credit</td>
					</tr>';
					$dTotal_Debit = $dTotal_Credit = 0;
				}
					

				$dTotal_Debit += $dDebit;
				$dTotal_Credit += $dCredit;

				$sDebit = number_format($dDebit, 0);
				$sCredit = number_format($dCredit, 0);

				if ($dDebit == 0) $sDebit = '';
				if ($dCredit == 0) $sCredit = '';
				
				$iTempVendorId = $iVendorId;
				
				$sReturn .= '
				<tr>
				 <td align="center" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . ($i+1) . '&nbsp;</td>
				 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sTransactionDate. '&nbsp;</td>
				 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sReference . '&nbsp;</td>
				 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sDetail. '&nbsp;</td>
                 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sCheckNumber . '&nbsp;</td>
				 <td align="right" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sDebit. '&nbsp;</td>
				 <td align="right" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sCredit . '&nbsp;</td>				 
				</tr>';
			}

			$sReturn .= '<tr>
			 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" colspan="5" align="right">Total</td>
			 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Debit, 0) . '</td>
			 <td style="border-top: 1px solid; font-family:Tahoma, Arial; font-size:14px;" align="right">' . number_format($dTotal_Credit, 0) . '</td>			 
			</tr>';
			$sReturn .= '</table>';			
	    }

	    else
	    	$sReturn .= '<div align="center" style="font-family:Tahoma;font-size:16px;">No records found...</div>';
		return($sReturn);
	}
	
	/* FixedAsset Depreciation Report */
	function FixedAssetDepreciationReport($bExportToExcel = false, &$aExcelData = '')
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Reports_VendorReports_FixedAssetDepreciationReport[0] == 0)
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$iFiscalYearStartDate = $objEmployee->aSystemSettings['VF_FiscalYearStartDate'];
		$iOrganization_StartingYear = $objEmployee->aSystemSettings['Organization_StartingYear'];
		/*
			$iDonorProjectId = $objGeneral->fnGet("selProject");
			if($iDonorProjectId > 0) $sProjectCondition = "AND (P.DonorProjectId = '$iDonorProjectId')";
		*/
		
		$sReturn = '<div align="center"><span style="font-family:Arial, Tahoma; font-size:20px; font-weight:bold; color:green;">FixedAsset Depreciation Report</span></div>';

		$this->ReportFilter($sReportCriteria, $sReportCriteriaSQL);
		$sReturn .= $sReportCriteria;
				
		$dDate = $objGeneral->fnGet("txtDate");

		$iCurrentYear = date("Y");			// As Of
		$iNoOfYears = $iCurrentYear - $iOrganization_StartingYear;
				
		$sReturn .= '<br />
		<table border="1" bordercolor="#cecece" cellspacing="0" cellpadding="3" width="98%" align="center">
		<thead>
		 <tr bgcolor="#bfbfbf">
		  <td align="center" style="border-bottom:1px solid; width:25px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">S#</td>
		  <td align="left" style="border-bottom:1px solid; width:120px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">FixedAsset</td>
		  <td align="center" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Depreciation Value.</td>
		  <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Depreciation Method.</td>
		  <td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Purchase Date</td>
          <td align="right" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">Purchase Cost#</td>';
		
		for($i= $iOrganization_StartingYear; $i<$iCurrentYear; $i++)
			$sReturn .= '<td align="left" style="border-bottom:1px solid; width:100px; font-family:Tahoma, Arial; font-weight:bold; font-size:12px;">' . $i . ' - ' . ($i +1) . '</td>';
			
		$sReturn .= '</tr>
		</thead>';
		
		$sQuery = "
		SELECT
		 C.CategoryId AS 'CategoryId',
		 C.CategoryName AS 'CategoryName',
		 P.FixedAssetId AS 'FixedAssetId',
		 P.FixedAssetName AS 'FixedAssetName',
		 P.FixedAssetCode AS 'FixedAssetCode',		 
		 P.FixedAssetCost AS 'FixedAssetCost',
		 P.DepreciationMethod AS 'DepreciationMethod',
		 P.DepreciationValue AS 'DepreciationValue',
		 P.RecoveryPeriod AS 'RecoveryPeriod',
		 P.SalvageValue AS 'SalvageValue',
		 P.FixedAssetPurchaseDate AS 'FixedAssetPurchaseDate'		 
		FROM fms_vendors_fixedassets AS P
		INNER JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = P.CategoryId
		LEFT JOIN fms_vendors AS V ON V.VendorId = P.VendorId
        LEFT JOIN fms_vendors_fixedassets_manufacturers AS M ON M.ManufacturerId = P.ManufacturerId
		WHERE P.FixedAssetPurchaseDate <='$dDate'
		ORDER BY P.CategoryId, P.FixedAssetPurchaseDate";
		
		//die($sQuery);
		$varResult = $objDatabase->Query($sQuery);
		$iTempCatId = 0;
		if ($objDatabase->RowsNumber($varResult) > 0)
	    {	
			for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			{
				$iCategoryId = $objDatabase->Result($varResult, $i, "CategoryId");
				$sCategoryName = $objDatabase->Result($varResult, $i, "CategoryName");				
				$iFixedAssetId = $objDatabase->Result($varResult, $i, "FixedAssetId");
				$sFixedAssetCode = $objDatabase->Result($varResult, $i, "FixedAssetCode");
				$sFixedAssetName = $objDatabase->Result($varResult, $i, "FixedAssetName");			
				$dFixedAssetCost = $objDatabase->Result($varResult, $i, "FixedAssetCost");
				$iDepreciationMethod = $objDatabase->Result($varResult, $i, "DepreciationMethod");
				$sDepreciationMethod = $this->aFixedAssetDepreciationMethod[$iDepreciationMethod];
				$dDepreciationValue = $objDatabase->Result($varResult, $i, "DepreciationValue");
				
				$dTotalFixedAssetCost += $dFixedAssetCost;
								
				if ($iTempCatId != $iCategoryId)
				{
					$sReturn .= '
					<tr>					 
					 <td colspan="'. ($iNoOfYears + 6) . '" align="left" style="font-family:Tahoma, Arial; font-size:16px; font-weight:bold;">' . $sCategoryName . '&nbsp;</td>					 
					</tr>'; 
				}
						    	
				$dRecoveryPeriod = $objDatabase->Result($varResult, $i, "RecoveryPeriod");
				$dSalvageValue = $objDatabase->Result($varResult, $i, "SalvageValue");			
				$dFixedAssetPurchaseDate = $objDatabase->Result($varResult, $i, "FixedAssetPurchaseDate");
				
				$sReturn .= '
				<tr>
				 <td align="center" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . ($i+1) . '&nbsp;</td>
				 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sFixedAssetName . '&nbsp;</td>
				 <td align="center" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $dDepreciationValue . '&nbsp;(% per year)</td>
				 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . $sDepreciationMethod . '&nbsp;</td>
                 <td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . date("F j, Y", strtotime($dFixedAssetPurchaseDate)) . '&nbsp;</td>
				 <td align="right" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">' . number_format($dFixedAssetCost, 2). '&nbsp;</td>'; 
				
				$dTempFixedAssetCost = $dFixedAssetCost;
				for($k= $iOrganization_StartingYear; $k < $iCurrentYear; $k++)
				{
					$dPurchaseYear = date("Y", strtotime($dFixedAssetPurchaseDate));
					
					if($dPurchaseYear > $k)
					{	
						$sReturn .= '<td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">&nbsp;</td>';
					}
					else
					{						
						if($iDepreciationMethod == 0)
						{						
							$dDepreciatedCost = (($dDepreciationValue * $dFixedAssetCost)/100);
							$dTempFixedAssetCost = $dTempFixedAssetCost - $dDepreciatedCost;
							$aDereciatedCost[$k] += $dDepreciatedCost;
						}
						else
						{
							$dDepreciatedCost = (($dDepreciationValue * $dTempFixedAssetCost)/100);
							$dTempFixedAssetCost = $dTempFixedAssetCost - $dDepreciatedCost;
							
							$aDereciatedCost[$k] += $dDepreciatedCost;
							
						}
												
						$sReturn .= '<td align="left" style="border-bottom:1px solid #dedede; font-family:Tahoma, Arial; font-size:12px;">'. $dTempFixedAssetCost . ' (Dep = ' . $dDepreciatedCost . ')' . '</td>';						
					}					
				}
			
				$sReturn .= '</tr>';
				$iTempCatId = $iCategoryId;
			}
			
			$sReturn .= '
			<tr>
			 <td colspan="'. ($iNoOfYears + 6) . '" align="left">&nbsp;</td>
			</tr>
			<tr>
			 <td style="font-weight:bold;font-size:14px;" align="right" colspan="5">Total</td>
			 <td style="font-weight:bold;font-size:14px;" align="right">' . number_format($dTotalFixedAssetCost, 2) . '&nbsp;</td>';
			
			for($k= $iOrganization_StartingYear; $k < $iCurrentYear; $k++)
			{
				$sReturn .= '<td style="font-weight:bold;font-size:14px;" align="right">' . number_format($aDereciatedCost[$k], 2). '&nbsp;</td>';
			}
			
			$sReturn .= '</tr>';
		}
		
		else
	    	$sReturn .= '<tr><td colspan="10" style="font-family:Tahoma;font-size:16px;"  align="center"><br /><br />No records found...<br /><br /><br /></td></tr>';
		
		$sReturn .= '</table>';

		return($sReturn);
	}	
		
}
?>