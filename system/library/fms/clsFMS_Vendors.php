<?php

include('../../system/library/fms/clsFMS_Accounts_ChartOfAccounts.php');
include('../../system/library/fms/clsFMS_Vendors_FixedAssets.php');
include('../../system/library/fms/clsFMS_Vendors_PurchaseOrders.php');

class clsVendors
{
    // Constructor
    function __construct()
    {
    }
    
    function ShowVendorsMenu($sPage)
    {
    	global $objEmployee;
    	
    	$sVendors = '<a ' . (($sPage == "Vendors") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../vendors/?page=Vendors"><img src="../images/vendors/iconVendors.gif" alt="Vendors" title="Vendors" border="0" /><br />Vendors</a>';

    	$sFixedAssets = '<a ' . (($sPage == "FixedAssets") ? 'style="text-decoration:underline; color:blue; font-weight:bold;"' : '') . ' href="../vendors/?page=FixedAssets"><img src="../images/vendors/iconFixedAssets.gif" alt="Fixed Assets" title="Fixed Assets" border="0" /><br />Fixed Assets</a>';

    	$sPurchaseOrders = '<a ' . (($sPage == "PurchaseOrders") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../vendors/?page=PurchaseOrders"><img src="../images/vendors/iconPurchaseOrders.gif" alt="Purchase Orders" title="Purchase Orders" border="0" /><br />Purchase Orders</a>';
    	//$sBills = '<a ' . (($sCurrentFile == "bills.php") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../vendors/bills.php"><img src="../images/vendors/iconBills.gif" alt="Bills" title="Bills" border="0" /><br />Bills</a>';
    	//$sPayBills = '<a ' . (($sCurrentFile == "paybills.php") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../vendors/paybills.php"><img src="../images/vendors/paybills.gif" alt="Pay Bills" title="Pay Bills" border="0" /><br />Pay Bills</a>';
    	$sQuotations = '<a ' . (($sPage == "Quotations") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../vendors/?page=Quotations"><img src="../images/vendors/iconQuotations.gif" alt="Quotations" title="Quotations" border="0" /><br />Quotations</a>';
		$sPurchaseRequests = '<a ' . (($sPage == "PurchaseRequests") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../vendors/?page=PurchaseRequests"><img src="../images/vendors/iconPurchaseRequests.gif" alt="Purchase Requests" title="Purchase Requests" border="0" /><br />Purchase Requests</a>';
    	
		//$sVendorTypes = '<a ' . (($sCurrentFile == "vendors.php") ? 'style="color:blue; text-decoration:underline; font-weight:bold;"' : '') . ' href="../vendors/vendors_vendortypes.php"><img src="../images/accounts/vendortypes.gif" alt="Vendor Types" title="Vendor Types" border="0" /><br />Vendor Types</a>';    	    	
    	//if ($objEmployee->objEmployeeRoles->aEmployeeRole_Accounts_ChartOfAccounts[0] == 0)
    		//$sChartOfAccounts = '<img src="../images/accounts/iconChartOfAccounts_disabled.gif" alt="Chart Of Accounts" title="Chart Of Accounts" border="0" /><br />Chart Of Accounts';
    	
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[0] == 0)
    		$sVendors = '<img src="../images/vendors/iconVendors_disabled.gif" alt="Vendors" title="Vendors" border="0" /><br />Vendors';
    	
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[0] == 0)
    		$sPurchaseOrders = '<img src="../images/vendors/iconPurchaseOrders_disabled.gif" alt="Purchase Orders" title="Purchase Orders" border="0" /><br />Purchase Orders';
		
		/*if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Bills[0] == 0)
    		$sBills = '<img src="../images/vendors/iconBills_disabled.gif" alt="Bills" title="Bills" border="0" /><br />Bills';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PayBills[0] == 0)
    		$sPayBills = '<img src="../images/vendors/iconPayBills_disabled.gif" alt="Pay Bills" title="Pay Bills" border="0" /><br />Pay Bills';
		*/

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[0] == 0)
    		$sFixedAssets = '<img src="../images/vendors/iconFixedAssets_disabled.gif" alt="Fixed Assets" title="FixedAssets" border="0" /><br />Fixed Assets';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[0] == 0)
    		$sQuotations = '<img src="../images/vendors/iconQuotations_disabled.gif" alt="Quotations" title="Quotations" border="0" /><br />Quotations';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[0] == 0)
    		$sPurchaseRequests = '<img src="../images/vendors/iconPurchaseRequests_disabled.gif" alt="Purchase Requests" title="Purchase Requests" border="0" /><br />Purchase Requests';
		
    	$sReturn .= '
    	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
    	 <tr>
    	  <td valign="bottom">

         <table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
	    	<tr>
	    	 <td align="center" width="10%" valign="top">' . $sVendors . '</td>
	    	 <td align="center" width="10%" valign="top">' . $sFixedAssets . '</td>			
			 <td align="center" width="10%" valign="top">' . $sPurchaseRequests . '</td>
			 <td align="center" width="10%" valign="top">' . $sQuotations . '</td>
	    	 <td align="center" width="10%" valign="top">' . $sPurchaseOrders . '</td>			
	    	</tr>
	       </table>
    	  </td></tr></table>';
    	
    	return($sReturn);    	
    }
    
	function ShowVendorsPages($sPage)
	{
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		if ($sPage == "") $sPage = "Vendors";		
		switch($sPage)
		{
			case "Vendors":
				$aTabs[0][0] = 'Vendors';
				$aTabs[0][1] = '../vendors/vendors.php';				
				break;				
			case "FixedAssets": 
				$aTabs[0][0] = 'Fixed Assets';
				$aTabs[0][1] = '../vendors/fixedassets.php';
				$aTabs[1][0] = 'Categories';
				$aTabs[1][1] = '../vendors/fixedassets.php?pagetype=categories';
				$aTabs[2][0] = 'Manufacturers';
				$aTabs[2][1] = '../vendors/fixedassets.php?pagetype=manufacturers';								
				break;
			case "PurchaseRequests": 
				$aTabs[0][0] = 'Purchase Requests';
				$aTabs[0][1] = '../vendors/purchaserequests.php';
				break;
			case "Quotations": 
				$aTabs[0][0] = 'Quotations';
				$aTabs[0][1] = '../vendors/quotations.php';
				break;	
			case "PurchaseOrders": 
				$aTabs[0][0] = 'Purchase Orders';
				$aTabs[0][1] = '../vendors/purchaseorders.php';
				break;
		}
		
		$sReturn = $objDHTMLSuite->TabBar($aTabs, $this->ShowVendorsMenu($sPage));
		return($sReturn);
	}
}

?>