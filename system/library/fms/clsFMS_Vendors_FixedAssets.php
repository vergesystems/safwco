<?php

/* Prduct Class */
/****************/
class clsVendors_FixedAssets
{
	public $aFixedAssetStatus;
	public $aFixedAssetCondition;
	public $aFixedAssetDepreciationMethod;
	
    // Constructor
    function __construct()
    {
    	$this->aFixedAssetStatus = array("Available", "Lost", "Temporarily Unavailable", "Expired");
    	$this->aFixedAssetCondition = array("New", "Like New", "Used", "Pretty Bad");

    	$this->aFixedAssetDepreciationMethod = array("Straight-line Method", "Decline Method");

    	$this->aFixedAssetDepreciationMethod_Help[0] = "Depreciation charges are spread evenly over the life of an asset. Easy to calculate, the straight-line depreciation method is used by a majority of small businesses.";
    	$this->aFixedAssetDepreciationMethod_Help[1] = "Accelerated depreciation methods include sum-of-years' digits and double-declining balance methods. Accelerated methods assume that an asset is used more heavily during the earlier years and loses a majority of its value during the first several years of use.";
    	$this->aFixedAssetDepreciationMethod_Help[2] = "Activity depreciation methods assume that depreciation is a function of use. The life of the asset is valued according to the output the asset provides or the number of units of activity that it produces. Activity methods may provide an excellent matching of expense to income for a machine where the number of produced units can be estimated, but activity depreciation methods do not work for all asset types.";
    	$this->aFixedAssetDepreciationMethod_Help[3] = "The group depreciation method is appropriate for a large number of similar assets, such as utility telephone poles. The composite depreciation method is used for a group of dissimilar assets, such as a fleet of different types of vehicles. Both methods are based on an average depreciation rate for the group.";
    }
	
	function ShowAllFixedAssets($sSearch, $iCategoryId = 0, $iVendorId = 0, $iManufacturerId = 0, $iFixedAssetStatus = -1)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		include(cVSFFolder . '/classes/clsBarcodeGenerator.php');		
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		if ($iFixedAssetStatus == '') $iFixedAssetStatus = -1;		
		
		$sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = "Fixed Assets";
        if ($sSortBy == "") $sSortBy = "FA.FixedAssetId DESC";

        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((FA.FixedAssetId LIKE '%$sSearch%') OR (FA.FixedAssetName LIKE '%$sSearch%') OR (FA.FixedAssetCode LIKE '%$sSearch%') OR (C.CategoryName LIKE '%$sSearch%') OR (M.ManufacturerName LIKE '%$sSearch%') OR (V.VendorName LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        // Conditions
        if ($iCategoryId > 0) $sCategoryCondition = " AND C.CategoryId='$iCategoryId'";
        if ($iVendorId > 0) $sVendorCondition = " AND V.VendorId='$iVendorId'";
        if ($iManufacturerId > 0) $sManufacturerCondition = " AND M.ManufacturerId='$iManufacturerId'";
        if ($iFixedAssetStatus >= 0) $sFixedAssetStatusCondition = " AND FA.FixedAssetStatus='$iFixedAssetStatus'";

		$iTotalRecords = $objDatabase->DBCount("fms_vendors_fixedassets AS FA LEFT JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = FA.CategoryId LEFT JOIN fms_vendors AS V ON V.VendorId = FA.VendorId LEFT JOIN fms_vendors_fixedassets_manufacturers AS M ON M.ManufacturerId = FA.ManufacturerId INNER JOIN organization_employees AS E ON E.EmployeeId = FA.FixedAssetAddedBy", "FA.OrganizationId='" . cOrganizationId . "' $sCategoryCondition $sManufacturerCondition $sVendorCondition $sManufacturerCondition $sFixedAssetStatusCondition $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors_fixedassets AS FA
		LEFT JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = FA.CategoryId
		LEFT JOIN fms_vendors AS V ON V.VendorId = FA.VendorId
        LEFT JOIN fms_vendors_fixedassets_manufacturers AS M ON M.ManufacturerId = FA.ManufacturerId
		INNER JOIN organization_employees AS E ON E.EmployeeId = FA.FixedAssetAddedBy
        WHERE FA.OrganizationId='" . cOrganizationId . "' $sSearchCondition $sCategoryCondition $sManufacturerCondition $sVendorCondition $sFixedAssetStatusCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Fixed Asset Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=FA.FixedAssetName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Fixed Asset Name in Ascending Order" title="Sort by Fixed Asset Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=FA.FixedAssetName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Fixed Asset Name in Descending Order" title="Sort by Fixed Asset Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Fixed Asset Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=FA.FixedAssetCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Fixed Asset Code in Ascending Order" title="Sort by Fixed Asset Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=FA.FixedAssetCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Fixed Asset Code in Descending Order" title="Sort by Fixed Asset Code in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Category&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CategoryName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Category Name in Ascending Order" title="Sort by Category Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CategoryName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Category Name in Descending Order" title="Sort by Category Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Vendor&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Name in Ascending Order" title="Sort by Vendor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Name in Descending Order" title="Sort by Vendor Name in Descending Order" border="0" /></a></span></td>
          <td align="left"><span class="WhiteHeading">Manufacturer&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=M.ManufacturerName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Manufacturer Name in Ascending Order" title="Sort by Manufacturer Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=M.ManufacturerNameName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Manufacturer Name in Descending Order" title="Sort by Manufacturer Name in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=FA.FixedAssetStatus&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=FA.FixedAssetStatus&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iFixedAssetId = $objDatabase->Result($varResult, $i, "FA.FixedAssetId");
			$sFixedAssetStockNumber = $objDatabase->Result($varResult, $i, "FA.FixedAssetStockNumber");
			$iCurrentCategoryId = $objDatabase->Result($varResult, $i, "FA.CategoryId");
			$iCurrentVendorId = $objDatabase->Result($varResult, $i, "FA.VendorId");
			$sFixedAssetCode = $objDatabase->Result($varResult, $i, "FA.FixedAssetCode");
			$iCurrentFixedAssetStatus = $objDatabase->Result($varResult, $i, "FA.FixedAssetStatus");
			$sCurrentFixedAssetStatus = $this->aFixedAssetStatus[$iCurrentFixedAssetStatus];

			if ($iCurrentFixedAssetStatus == 0) $sCurrentFixedAssetStatus = '<span style="color:green">' . $sCurrentFixedAssetStatus . '</span>';
			else if ($iCurrentFixedAssetStatus == 1) $sCurrentFixedAssetStatus = '<span style="color:red">' . $sCurrentFixedAssetStatus . '</span>';
			else if ($iCurrentFixedAssetStatus == 2) $sCurrentFixedAssetStatus = '<span style="color:gray">' . $sCurrentFixedAssetStatus . '</span>';
			else if ($iCurrentFixedAssetStatus == 3) $sCurrentFixedAssetStatus = '<span style="color:blue">' . $sCurrentFixedAssetStatus . '</span>';
			else if ($iCurrentFixedAssetStatus == 4) $sCurrentFixedAssetStatus = '<span style="color:#ff7e00">' . $sCurrentFixedAssetStatus . '</span>';
			else if ($iCurrentFixedAssetStatus == 5) $sCurrentFixedAssetStatus = '<span style="color:#c600ff">' . $sCurrentFixedAssetStatus . '</span>';

			$sCategoryName = $objDatabase->Result($varResult, $i, "C.CategoryName");
			$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");
            $sManufacturerName = $objDatabase->Result($varResult, $i, "M.ManufacturerName");

			if ($sManufacturerName == '') $sManufacturerName = 'No Manufacturer';
			if ($sVendorName == '') $sVendorName = 'No Vendor';

			$sFixedAssetName = $objDatabase->Result($varResult, $i, "FA.FixedAssetName");
            $sFixedAssetDescription = $objDatabase->Result($varResult, $i, "FA.FixedAssetDescription");

            $sEditFixedAsset = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update FixedAsset\', \'../vendors/fixedassets.php?pagetype=details&action2=edit&id=' . $iFixedAssetId. '&categoryid=' . $iCurrentCategoryId. '&manufacturerid=' . $iCurrentManufacturerId . '&vendorid=' . $iCurrentVendorId.  '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Fixed Asset Details" title="Edit this Fixed Asset Details"></a></td>';
            $sDeleteFixedAsset = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this FixedAsset?\')) {window.location = \'?action=DeleteFixedAsset&id=' . $iFixedAssetId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this FixedAsset" title="Delete this FixedAsset"></a></td>';
            $sFixedAssetImages = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Images for ' . $objDatabase->RealEscapeString(str_replace('"', '', $sFixedAssetName)) . '\', \'../vendors/fixedassets_images.php?action2=uploadImage&id=' . $iFixedAssetId. '\', \'520px\', true);"><img src="../images/icons/iconImages.gif" border="0" alt="Fixed Asset Images" title="Fixed Asset Images"></a></td>';
            $sFixedAssetDocuments = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(str_replace('"', '', $sFixedAssetName)) . ' Documents\', \'../vendors/fixedassets_documents_showall.php?id=' . $iFixedAssetId . '\', \'520px\', true);"><img src="../images/icons/iconFolder.gif" border="0" alt="Fixed Asset Documents" title="Fixed Asset Documents"></a></td>';

            if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[2] == 0)	$sEditFixedAsset = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[3] == 0)	$sDeleteFixedAsset = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_FixedAssetImages[0] == 0)	$sFixedAssetImages = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_FixedAssetDocuments[0] == 0)	$sFixedAssetDocuments = '<td class="GridTD">&nbsp;</td>';

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td align="left" valign="top">' . $sFixedAssetName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sFixedAssetCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
             <td align="left" valign="top">' . $sCategoryName  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
             <td align="left" valign="top">' . $sVendorName  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
             <td align="left" valign="top">' . $sManufacturerName  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
             <td align="left" valign="top">' . $sCurrentFixedAssetStatus  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View ' . $objDatabase->RealEscapeString(str_replace('"', '', $sFixedAssetName)) . '\',  \'../vendors/fixedassets.php?pagetype=details&id=' . $iFixedAssetId .'&categoryid=' . $iCurrentCategoryId. '&manufacturerid=' . $iCurrentManufacturerId . '&vendorid=' . $iCurrentVendorId. '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Fixed Asset Details" title="View this Fixed Asset Details"></a></td>
			 ' . $sEditFixedAsset . '			 
			 ' . $sFixedAssetImages . '
             ' . $sDeleteFixedAsset . '
            </tr>';
		}

		$sAddNewFixedAsset = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Fixed Asset\', \'../vendors/fixedassets.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Fixed Asset">';

        if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[1] == 0) // Add Disabled
			$sAddNewFixedAsset = '';

   		$sCategorySelect = '<select class="form1" name="selCategory" onchange="window.location=\'?cat=\'+GetSelectedListBox(\'selCategory\');" id="selCategory">
		<option value="0">Fixed Assets in All Categories</option>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_categories AS C WHERE C.OrganizationId='" . cOrganizationId . "' ORDER BY C.CategoryName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sCategorySelect .= '<option ' . (($iCategoryId == $objDatabase->Result($varResult, $i, "C.CategoryId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "C.CategoryId") . '">' . $objDatabase->Result($varResult, $i, "C.CategoryName") . '</option>';
		$sCategorySelect .= '</select>';

   		$sVendorSelect = '<select class="form1" name="selVendor" onchange="window.location=\'?vendor=\'+GetSelectedListBox(\'selVendor\');" id="selVendor">
		<option value="0">Fixed Assets by All Vendors</option>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "' ORDER BY V.VendorName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sVendorSelect .= '<option ' . (($iVendorId == $objDatabase->Result($varResult, $i, "V.VendorId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult, $i, "V.VendorName") . '</option>';
		$sVendorSelect .= '</select>';

        $sManufacturerSelect = '<select class="form1" name="selManufacturer" onchange="window.location=\'?manufacturer=\'+GetSelectedListBox(\'selManufacturer\');" id="selManufacturer">
		<option value="0">Fixed Assets by All Manufacturers</option>';
		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_manufacturers AS M WHERE M.OrganizationId='" . cOrganizationId . "' ORDER BY M.ManufacturerName");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
			$sManufacturerSelect .= '<option ' . (($iManufacturerId== $objDatabase->Result($varResult, $i, "M.ManufacturerId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "M.ManufacturerId") . '">' . $objDatabase->Result($varResult, $i, "M.ManufacturerName") . '</option>';
		$sManufacturerSelect .= '</select>';

   		$sFixedAssetStatusSelect = '<select class="form1" name="selFixedAssetStatus" onchange="window.location=\'?status=\'+GetSelectedListBox(\'selFixedAssetStatus\');" id="selFixedAssetStatus">
		<option value="-1">Fixed Assets with All Status</option>';
		for ($i=0; $i < count($this->aFixedAssetStatus); $i++)
			$sFixedAssetStatusSelect .= '<option ' . (($iFixedAssetStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aFixedAssetStatus[$i] . '</option>';
		$sFixedAssetStatusSelect .= '</select>';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddNewFixedAsset . '
           <form method="GET" action="">&nbsp;&nbsp;Search for a Fixed Asset:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Fixed Asset" title="Search for a Fixed Asset" border="0"></form>
          </td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		 <tr>
		  <td colspan="2">' . $sCategorySelect . '&nbsp;&nbsp;' . $sVendorSelect . '&nbsp;&nbsp;' . $sManufacturerSelect . '</td>
		 </tr>
		 <tr>
		  <td>' . $sFixedAssetStatusSelect . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Fixed Asset Details" alt="View this Fixed Asset Details"></td><td>View this Fixed Asset Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Fixed Asset Details" alt="Edit this Fixed Asset Details"></td><td>Edit this Fixed Asset Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconFolder.gif" title="Fixed Asset Documents" alt="Fixed Asset Documents"></td><td>Fixed Asset Documents</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconImages.gif" title="Fixed Asset Images" alt="Fixed Asset Images"></td><td>Fixed Asset Images</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Fixed Asset" alt="Delete this Fixed Asset"></td><td>Delete this Fixed Asset</td></tr>
        </table>';

	 	return($sReturn);
    }

    function FixedAssetDetails($iFixedAssetId, $iCategoryId, $iVendorId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();

    	if (($iCategoryId == 0) ||($iVendorId==0))
    		$sWhereCondition = "FA.FixedAssetId='$iFixedAssetId'";
    	else
    		$sWhereCondition = "FA.FixedAssetId = '$iFixedAssetId' AND FA.CategoryId='$iCategoryId' AND FA.VendorId='$iVendorId'";

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_vendors_fixedassets AS FA
		INNER JOIN organization_employees AS E ON E.EmployeeId = FA.FixedAssetAddedBy
		INNER JOIN organization_stations AS S ON S.StationId = FA.StationId
		INNER JOIN organization_stations AS S2 ON S2.StationId = FA.CurrentLocation_StationId
		INNER JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = FA.CategoryId
		LEFT JOIN fms_vendors AS V ON V.VendorId = FA.VendorId
        LEFT JOIN fms_vendors_fixedassets_manufacturers AS M ON M.ManufacturerId = FA.ManufacturerId
		LEFT JOIN fms_accounts_donors_projects AS DP ON DP.DonorProjectId = FA.DonorProjectId
		WHERE FA.OrganizationId='" . cOrganizationId . "' AND $sWhereCondition");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iFixedAssetId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Fixed Asset" title="Edit this Fixed Asset" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this FixedAsset?\')) {window.location = \'?pagetype=details&action=DeleteFixedAsset&id=' . $iFixedAssetId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Fixed Asset" title="Delete this Fixed Asset" /></a>';

    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[3] == 0)	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Fixed Asset Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iFixedAssetId = $objDatabase->Result($varResult, 0, "FA.FixedAssetId");
		    	$iStationId = $objDatabase->Result($varResult, 0, "FA.StationId");
		    	$iCurrentLocation_StationId = $objDatabase->Result($varResult, 0, "FA.CurrentLocation_StationId");

		    	$iCategoryId = $objDatabase->Result($varResult, 0, "FA.CategoryId");
		    	$iVendorId = $objDatabase->Result($varResult, 0, "FA.VendorId");
		    	$iDonorProjectId = $objDatabase->Result($varResult, 0, "FA.DonorProjectId");
                $iManufacturerId = $objDatabase->Result($varResult, 0, "FA.ManufacturerId");

		    	$sFixedAssetName = $objDatabase->Result($varResult, 0, "FA.FixedAssetName");
		    	$dFixedAssetCost = $objDatabase->Result($varResult, 0, "FA.FixedAssetCost");
		    	$sFixedAssetStockNumber = $objDatabase->Result($varResult, 0, "FA.FixedAssetStockNumber");

		    	$sFixedAssetCost = number_format($dFixedAssetCost, 2);

		    	$sCategoryCode = $objDatabase->Result($varResult, 0, "C.CategoryCode");

		    	$iStationId = $objDatabase->Result($varResult, 0, "FA.StationId");
		    	$sStationName  = $objDatabase->Result($varResult, 0, "S.StationName") . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Station Information\', \'../organization/stations.php?pagetype=details&id=' . $iStationId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
		    	
		    	$sCurrentLocation_StationName  = $objDatabase->Result($varResult, 0, "S2.StationName") . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Station Information\', \'../organization/stations.php?pagetype=details&id=' . $iCurrentLocation_StationId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Station Information" title="Station Information" border="0" /></a>';
			
				$sFixedAssetAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sFixedAssetAddedBy = $sFixedAssetAddedBy . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sFixedAssetAddedBy . '\', \'../organization/employees.php?pagetype=details&id=' . $objDatabase->Result($varResult, 0, "E.EmployeeId") . '\', \'520px\', true);"><img align="absmiddle" src="../images/icons/iconUserDetails.gif" alt="Employee Information" title="Employee Information" border="0" /></a>';

		    	$dFixedAssetWeight = $objDatabase->Result($varResult, 0, "FA.FixedAssetWeight");

		    	$dFixedAssetPurchase_Date=$objDatabase->Result($varResult, 0, "FA.FixedAssetPurchaseDate");
		        $sFixedAssetPurchase_Date = date("F j, Y", strtotime($dFixedAssetPurchase_Date));

		    	$iFixedAssetStatus = $objDatabase->Result($varResult, 0, "FA.FixedAssetStatus");
		    	$sFixedAssetStatus = $this->aFixedAssetStatus[$iFixedAssetStatus];

		    	$dFixedAssetSalvageValue = $objDatabase->Result($varResult, 0, "FA.SalvageValue");
		    	$sFixedAssetSalvageValue = number_format($dFixedAssetSalvageValue, 2);

		    	$iFixedAssetRecoveryPeriod = $objDatabase->Result($varResult, 0, "FA.RecoveryPeriod");
		    	$sFixedAssetRecoveryPeriod = $iFixedAssetRecoveryPeriod;
		    	$iFixedAssetDepreciationMethod = $objDatabase->Result($varResult, 0, "FA.DepreciationMethod");

		    	$sFixedAssetDepreciationMethod = $this->aFixedAssetDepreciationMethod[$iFixedAssetDepreciationMethod];

		    	$dDepreciationValue = $objDatabase->Result($varResult, 0, "FA.DepreciationValue");
				$sDepreciationValue = number_format($dDepreciationValue, 2);
				/*
		    	$aDepreciationValue = unserialize($sDepreciationValue);

		    	if ($iFixedAssetDepreciationMethod == 0)
		    	{
		    		$sDepreciationValue = $aDepreciationValue[0] . '% per year';
		    	}
		    	else if ($iFixedAssetDepreciationMethod == 1)
		    	{
		    		$sDepreciationValue = '';
		    		for ($i=0; $i < $objEmployee->aSystemSettings['FMS_Vendors_Depreciation_AcceleratedMaxYears']; $i++)
		    		{
		    			$sDepreciationValue .= 'Year ' . ($i+1) . ': ' . $aDepreciationValue[$i] . '%<br />';
		    		}
		    	}
				*/
				$iFixedAssetConsumable = $objDatabase->Result($varResult, 0, "FA.FixedAssetConsumable");
				$sFixedAssetConsumable = (($iFixedAssetConsumable == 1) ? '<img src="../images/icons/tick.gif" />' : '<img src="../images/icons/cross.gif" />');
				
		    	$sCategoryName = $objDatabase->Result($varResult, 0, "C.CategoryName");
		        $sCategoryName = $sCategoryName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sCategoryName . '\', \'../vendors/fixedassets.php?pagetype=categories_details&id=' . $iCategoryId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Category Information" title="Category Information" border="0" /></a>';

		        $sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");
		        if ($sVendorName == '') $sVendorName = 'No Vendor';
		        else $sVendorName = $sVendorName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sVendorName . '\', \'../vendors/vendors.php?pagetype=details&id=' . $iVendorId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Vendor Information" title="Vendor Information" border="0" /></a>';

                $sManufacturerName = $objDatabase->Result($varResult, 0, "M.ManufacturerName");
		        if ($sManufacturerName == '') $sManufacturerName = 'No Manufacturer';
		        else $sManufacturerName = $sManufacturerName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sManufacturerName . '\', \'../vendors/fixedassets.php?pagetype=manufacturers_details&id=' . $iManufacturerId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Manufacturer Information" title="Manufacturer Information" border="0" /></a>';

				$iDonorProjectId = $objDatabase->Result($varResult, 0, "FA.DonorProjectId");
				$sDonorProject = $objDatabase->Result($varResult, 0, "DP.ProjectTitle");

				if ($sDonorProject == '') $sDonorProject = 'No Project';
		        else $sDonorProject = $sDonorProject . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sDonorProject . '\', \'../vendors/donors.php?pagetype=donorprojects_details&id=' . $iDonorProjectId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Donor Project Information" title="Donor Project Information" border="0" /></a>';

		        $sDescription = str_replace("\n", '<br />', $objDatabase->Result($varResult, 0, "FA.FixedAssetDescription"));
		        $sNotes = str_replace("\n", '<br />', $objDatabase->Result($varResult, 0, "FA.Notes"));

				$dFixedAssetAddedOn = $objDatabase->Result($varResult, 0, "FA.FixedAssetAddedOn");
				$sFixedAssetAddedOn = date("F j, Y", strtotime($dFixedAssetAddedOn)) . ' at ' . date("g:i a", strtotime($dFixedAssetAddedOn));

				$sFixedAssetCode = $objDatabase->Result($varResult, 0, "FA.FixedAssetCode"); // . '<br /><a style="text-decoration:none;" href="?id=' . $iFixedAssetId . '&action=ReIssueFixedAssetCode">Re-issue</a>';

				if ($sManufacturerName == '') $sManufacturerName = 'No Manufacturer';
				if ($sVendorName == '') $sVendorName = 'No Vendor';
				if ($sDonorProject == '') $sDonorProject = 'No Project';

				$sBarcodeImage = cDataFolder .'/vf/vendors/barcodes/' . $iFixedAssetId . '.jpg';
		    }

			if ($sAction2 == "edit")
			{

				$sReturn .= '<form method="post" action="../vendors/fixedassets.php?pagetype=details" onsubmit="return ValidateForm();">';

				$sFixedAssetPurchase_Date = '<input type="text" id="txtDateOfPurchase" name="txtDateOfPurchase" class="form1" value="' . $dFixedAssetPurchase_Date . '" />' . $objjQuery->Calendar('txtDateOfPurchase') . '&nbsp;<span style="color:red;">*</span>';

        		$sCategoryName = '<select class="form1" name="selCategory" id="selCategory">';
		    	$varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_categories AS C WHERE C.OrganizationId='" . cOrganizationId . "' ORDER BY CategoryName");
		       	if ($objDatabase->RowsNumber($varResult2) <= 0) die('Sorry, Invalid Category Id');
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iCategoryId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		               $sCategoryName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' .  $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		            else if ($iCategoryId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		            	$sCategoryName .= '';
		            else
		               $sCategoryName .= '<option value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' . $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		        }
		        $sCategoryName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selCategory\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selCategory\'), \'../vendors/fixedassets.php?pagetype=categories_details&id=\'+GetSelectedListBox(\'selCategory\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0"  align="absmiddle" alt="Category Details" title="Category Details" /></a>';

		        $sVendorName = '<select class="form1" name="selVendor" id="selVendor">
		        <option value="0">No Vendor</option>';

		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "' ORDER BY VendorName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iVendorId == $objDatabase->Result($varResult2, $i, "V.VendorId"))
		               $sVendorName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "V.VendorId") . '">' .  $objDatabase->Result($varResult2, $i, "V.VendorName") . '</option>';
		            else if ($iVendorId == $objDatabase->Result($varResult2, $i, "V.VendorId"))
		            	$sVendorName .= '';
		            else
		               $sVendorName .= '<option value="' . $objDatabase->Result($varResult2, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult2, $i, "V.VendorName") . '</option>';
		        }
		        $sVendorName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selVendor\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selVendor\'), \'../vendors/vendors.php?pagetype=details&id=\'+GetSelectedListBox(\'selVendor\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Vendor Details" title="Vendor Details" /></a>';

                $sManufacturerName = '<select class="form1" name="selManufacturer" id="selManufacturer">
		        <option value="0">No Manufacturer</option>';

		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_manufacturers AS M WHERE M.OrganizationId='" . cOrganizationId . "' ORDER BY M.ManufacturerName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iManufacturerId == $objDatabase->Result($varResult2, $i, "M.ManufacturerId"))
		               $sManufacturerName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "M.ManufacturerId") . '">' .  $objDatabase->Result($varResult2, $i, "M.ManufacturerName") . '</option>';
		            else if ($iManufacturerId == $objDatabase->Result($varResult2, $i, "M.ManufacturerId"))
		            	$sManufacturerName .= '';
		            else
		               $sManufacturerName .= '<option value="' . $objDatabase->Result($varResult2, $i, "M.ManufacturerId") . '">' .  $objDatabase->Result($varResult2, $i, "M.ManufacturerName") . '</option>';
		        }
		        $sManufacturerName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selManufacturer\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selManufacturer\'), \'../vendors/fixedassets.php?pagetype=manufacturers_details&id=\'+GetSelectedListBox(\'selManufacturer\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Manufacturer Details" title="Manufacturer Details" /></a>';

				$sDonorProject = '<select class="form1" name="selDonorProject" id="selDonorProject">
		        <option value="0">No Project</option>';
		        $varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D WHERE D.OrganizationId='" . cOrganizationId . "' ORDER BY D.DonorName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		        {
		        	$iDonorId = $objDatabase->Result($varResult, $i, "D.DonorId");
		        	$sDonorProject .= '<option style="font-weight:BOLD; color:BLUE;" value="0">' . $objDatabase->Result($varResult, $i, "D.DonorName") . '</option>';

		        	$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.OrganizationId='" . cOrganizationId . "' AND DP.DonorId='$iDonorId' ORDER BY DP.ProjectTitle");
		        	for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
		        	{
						$sDonorProject .= '<option ' . (($iDonorProjectId == $objDatabase->Result($varResult2, $j, 'DP.DonorProjectId')) ? 'selected="true"' : '') . 'style="font-weight:NORMAL; color:GREEN;" value="' . $iDonorProjectId . '">&nbsp;&nbsp;+ ' . $objDatabase->Result($varResult2, $j, "DP.ProjectTitle") . '</option>';
		        	}

		        }
		        $sDonorProject .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selDonorProject\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selDonorProject\'), \'../vendors/donors.php?pagetype=donorprojects_details&id=\'+GetSelectedListBox(\'selDonorProject\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Donor Project Details" title="Donor Project Details" /></a>';

		        $sFixedAssetStatus = '<select class="form1" name="selFixedAssetStatus" id="selFixedAssetStatus">';
		        for ($i=0; $i < count($this->aFixedAssetStatus); $i++)
		        $sFixedAssetStatus .= '<option ' . (($sFixedAssetStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aFixedAssetStatus[$i] . '</option>';
		        $sFixedAssetStatus .= '</select>&nbsp;<span style="color:red;">*</span>';

		        $sStationName = '<select class="form1" name="selStation" id="selStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "'  ORDER BY S.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					if ($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId"))
		               $sStationName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' .  $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
		            else if ($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId"))
		            	$sStationName .= '';
		            else
		               $sStationName .= '<option value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
				}
				$sStationName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selStation\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Station Details" title="Station Details" /></a>';

				$sCurrentLocation_StationName = '<select class="form1" name="selCurrentStation" id="selCurrentStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS CS WHERE CS.OrganizationId='" . cOrganizationId . "' ORDER BY CS.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					if ($iCurrentLocation_StationId == $objDatabase->Result($varResult2, $i, "CS.StationId"))
		               $sCurrentLocation_StationName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "CS.StationId") . '">' .  $objDatabase->Result($varResult2, $i, "CS.StationName") . '</option>';
		            else if ($iCurrentLocation_StationId == $objDatabase->Result($varResult2, $i, "CS.StationId"))
		            	$sCurrentLocation_StationName .= '';
		            else
		               $sCurrentLocation_StationName .= '<option value="' . $objDatabase->Result($varResult2, $i, "CS.StationId") . '">' . $objDatabase->Result($varResult2, $i, "CS.StationName") . '</option>';
				}
				$sCurrentLocation_StationName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selCurrentStation\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selCurrentStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selCurrentStation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Station Details" title="Station Details" /></a>';

				$sFixedAssetName = '<input type="text" name="txtFixedAssetName" id="txtFixedAssetName" class="form1" value="' . htmlentities($sFixedAssetName) . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sFixedAssetCost = '<input type="text" name="txtFixedAssetCost" id="txtFixedAssetCost" class="form1" value="' . number_format($dFixedAssetCost, 2, '.', '') . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sFixedAssetStockNumber = '<input type="text" name="txtFixedAssetStockNumber" id="txtFixedAssetStockNumber" class="form1" value="' . $sFixedAssetStockNumber . '" size="30" />';
				$dFixedAssetWeight ='<input type="text" name="txtFixedAssetWeight" id="txtFixedAssetWeight" class="form1" value="' . $dFixedAssetWeight . '" size="5" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sFixedAssetRecoveryPeriod = '<input type="text" name="txtFixedAssetRecoveryPeriod" id="txtFixedAssetRecoveryPeriod" class="form1" value="' . $iFixedAssetRecoveryPeriod . '" size="8" />';
				$sFixedAssetSalvageValue = '<input type="text" name="txtFixedAssetSalvageValue" id="txtFixedAssetSalvageValue" class="form1" value="' . $dFixedAssetSalvageValue . '" size="15" />';

				$sFixedAssetDepreciationMethod = '';
			   	for ($i=0; $i < count($this->aFixedAssetDepreciationMethod); $i++)
			   		$sFixedAssetDepreciationMethod .= '<input ' . (($iFixedAssetDepreciationMethod == $i) ? 'checked="true"' : '') . ' type="radio" name="radFixedAssetDepreciationMethod" id="radFixedAssetDepreciationMethod" value="' . $i . '">' . $this->aFixedAssetDepreciationMethod[$i] . '</option>&nbsp;' . $objGeneral->HelpToolTip(str_replace(' ', '', $this->aFixedAssetDepreciationMethod[$i] ), $this->aFixedAssetDepreciationMethod[$i], $this->aFixedAssetDepreciationMethod_Help[$i]) . '<br />';
				
				$sDepreciationValue = '<input type="text" name="txtDepreciationValue" id="txtDepreciationValue" class="form1" value="' . number_format($dDepreciationValue, 2) . '" size="5" /> </div>';				
				$sFixedAssetConsumable = '<input type="checkbox" ' . (($iFixedAssetConsumable == 1) ? 'checked="true"' : '') . ' name="chkFixedAssetConsumable" id="chkFixedAssetConsumable" value="on" />';
				
				/*
			   	$sDepreciationValue = '<div id="divDepreciationValue_StraightLineMethod" style="display:none;"><input type="text" name="txtDepreciationValue" id="txtDepreciationValue" class="form1" value="' . $aDepreciationValue[0] . '" size="5" /> % per year</div>';

			   	$sDepreciationValue .= '<div id="divDepreciationValue_AcceleratedMethod" style="display:none;">';
	    		for ($i=0; $i < $objEmployee->aSystemSettings['FMS_Vendors_Depreciation_AcceleratedMaxYears']; $i++)
			   		$sDepreciationValue .= 'Year ' . ($i+1) . ': <input type="text" name="txtDepreciationValue' . ($i+1) . '" id="txtDepreciationValue' . ($i+1) . '" class="form1" value="' . $aDepreciationValue[$i] . '" size="5" /> %<br />';
			   	$sDepreciationValue .= '</div>
			   	<script language="JavaScript" type="text/javascript">
			   	function ChangeDepreciationMethod()
			   	{
			   		var iSelectedDepreciationMethod = GetCheckedValue(document.forms[0].radFixedAssetDepreciationMethod);

			   		if (iSelectedDepreciationMethod == 0)
			   		{
			   			HideDiv("divDepreciationValue_AcceleratedMethod");
			   			ShowDiv("divDepreciationValue_StraightLineMethod");
			   		}
			   		else if (iSelectedDepreciationMethod == 1)
			   		{
			   			ShowDiv("divDepreciationValue_AcceleratedMethod");
			   			HideDiv("divDepreciationValue_StraightLineMethod");
			   		}
			   		else
			   		{
		   				HideDiv("divDepreciationValue_AcceleratedMethod");
		   				ShowDiv("divDepreciationValue_StraightLineMethod");
			   		}
			   	}

			   	ChangeDepreciationMethod();
			   	</script>';
				*/
			}
			else if ($sAction2 == "addnew")
			{
				$sFixedAssetAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '\', \'../organization/employees.php?pagetype-details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				$sBarcodeImage = '../images/spacer.gif';

				$iFixedAssetId = "";
		        $sFixedAssetName = $objGeneral->fnGet("txtFixedAssetName");
		        $dFixedAssetCost = $objGeneral->fnGet("txtFixedAssetCost");
		        $sFixedAssetStockNumber = $objGeneral->fnGet("txtFixedAssetStockNumber");
		        $dFixedAssetWeight = $objGeneral->fnGet("txtFixedAssetWeight");
		        $dFixedAssetPurchase_Date = $objGeneral->fnGet("txtDateOfPurchase");
		        $iCategoryId = $objGeneral->fnGet("selCategoryId");
   			   	$iFixedAssetDepreciationMethod = $objGeneral->fnGet("radFixedAssetDepreciationMethod");
   			   	$dDepreciationValue = $objGeneral->fnGet("txtDepreciationValue");
				$dFixedAssetSalvageValue = $objGeneral->fnGet("txtFixedAssetSalvageValue");
				$iFixedAssetRecoveryPeriod = $objGeneral->fnGet("txtProuctRecoveryPeriod");

				if($dDepreciationValue == "") $dDepreciationValue = 0;
				if($dFixedAssetWeight == "") $dFixedAssetWeight = 0;
				if($dFixedAssetSalvageValue == "") $dFixedAssetSalvageValue = 0;
				if($iFixedAssetRecoveryPeriod == "") $iFixedAssetRecoveryPeriod = 0;
				if($dFixedAssetCost == "") $dFixedAssetCost = 0;
		        if ($iFixedAssetDepreciationMethod == "") $iFixedAssetDepreciationMethod = 0;
		        if ($dFixedAssetPurchase_Date == "") $dFixedAssetPurchase_Date = date("Y-m-d");

		        $iFixedAssetStatus = $objGeneral->fnGet("selFixedAssetStatus");
		        $iStationId= $objGeneral->fnGet("selStation");

		        $sDescription = $objGeneral->fnGet("txtDescription");

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

		        $sFixedAssetPurchase_Date = '<input type="text" id="txtDateOfPurchase" name="txtDateOfPurchase" class="form1" value="' . $dFixedAssetPurchase_Date . '" />' . $objjQuery->Calendar('txtDateOfPurchase') . '&nbsp;<span style="color:red;">*</span>';

		        $sCategoryName = '<select class="form1" name="selCategory" id="selCategory">';
		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_categories AS C WHERE C.OrganizationId='" . cOrganizationId . "' ORDER BY CategoryName");
		        if ($objDatabase->RowsNumber($varResult2) <= 0) die('Sorry, Invalid Category Id');
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iCategoryId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		               $sCategoryName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' .  $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		            else if ($iCategoryId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		            	$sCategoryName .= '';
		            else
		               $sCategoryName .= '<option value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' . $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		        }
		        $sCategoryName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selCategory\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selCategory\'), \'../vendors/fixedassets.php?pagetype=categories_details&id=\'+GetSelectedListBox(\'selCategory\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0"  align="absmiddle" alt="Category Details" title="Category Details" /></a>';

		       $sVendorName = '<select class="form1" name="selVendor" id="selVendor">
		        <option value="0">No Vendor</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "' ORDER BY VendorName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iVendorId == $objDatabase->Result($varResult2, $i, "V.VendorId"))
		               $sVendorName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "V.VendorId") . '">' .  $objDatabase->Result($varResult2, $i, "V.VendorName") . '</option>';
		            else if ($iVendorId == $objDatabase->Result($varResult2, $i, "V.VendorId"))
		            	$sVendorName .= '';
		            else
		               $sVendorName .= '<option value="' . $objDatabase->Result($varResult2, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult2, $i, "V.VendorName") . '</option>';
		        }
		        $sVendorName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selVendor\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selVendor\'), \'../vendors/vendors.php?pagetype=details&id=\'+GetSelectedListBox(\'selVendor\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Vendor Details" title="Vendor Details" /></a>';

                $sManufacturerName = '<select class="form1" name="selManufacturer" id="selManufacturer">
		        <option value="0">No Manufacturer</option>';

		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_manufacturers AS M WHERE M.OrganizationId='" . cOrganizationId . "'  ORDER BY M.ManufacturerName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iManufacturerId == $objDatabase->Result($varResult2, $i, "M.ManufacturerId"))
		               $sManufacturerName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "M.ManufacturerId") . '">' .  $objDatabase->Result($varResult2, $i, "M.ManufacturerName") . '</option>';
		            else if ($iManufacturerId == $objDatabase->Result($varResult2, $i, "M.ManufacturerId"))
		            	$sManufacturerName .= '';
		            else
		               $sManufacturerName .= '<option value="' . $objDatabase->Result($varResult2, $i, "M.ManufacturerId") . '">' .  $objDatabase->Result($varResult2, $i, "M.ManufacturerName") . '</option>';
		        }
		        $sManufacturerName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selManufacturer\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selManufacturer\'), \'../vendors/fixedassets.php?pagetype=manufacturers_details&id=\'+GetSelectedListBox(\'selManufacturer\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Manufacturer Details" title="Manufacturer Details" /></a>';

				$sDonorProject = '<select class="form1" name="selDonorProject" id="selDonorProject">
		        <option value="0">No Project</option>';
		        $varResult = $objDatabase->Query("SELECT * FROM fms_accounts_donors AS D WHERE D.OrganizationId='" . cOrganizationId . "' ORDER BY D.DonorName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		        {
		        	$iDonorId = $objDatabase->Result($varResult, $i, "D.DonorId");
		        	$sDonorProject .= '<option style="font-weight:BOLD; color:BLUE;" value="0">' . $objDatabase->Result($varResult, $i, "D.DonorName") . '</option>';

		        	$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_donors_projects AS DP WHERE DP.OrganizationId='" . cOrganizationId . "' AND DP.DonorId='$iDonorId' ORDER BY DP.ProjectTitle");
		        	for ($j=0; $j < $objDatabase->RowsNumber($varResult2); $j++)
		        	{
		        		$iDonorProjectId = $objDatabase->Result($varResult2, $j, "DP.DonorProjectId");
						$sDonorProject .= '<option style="font-weight:NORMAL; color:GREEN;" value="' . $iDonorProjectId . '">&nbsp;&nbsp;+ ' . $objDatabase->Result($varResult2, $j, "DP.ProjectTitle") . '</option>';
		        	}

		        }
		        $sDonorProject .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selDonorProject\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selDonorProject\'), \'../vendors/donors.php?pagetype=donorprojects_details&id=\'+GetSelectedListBox(\'selDonorProject\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Donor Project Details" title="Donor Project Details" /></a>';

		        $sFixedAssetStatus = '<select class="form1" name="selFixedAssetStatus" id="selFixedAssetStatus">';
		        for ($i=0; $i < count($this->aFixedAssetStatus); $i++)
		        	$sFixedAssetStatus .= '<option ' . (($sFixedAssetStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aFixedAssetStatus[$i] . '</option>';
		        $sFixedAssetStatus .= '</select>&nbsp;<span style="color:red;">*</span>';

		        $sStationName = '<select class="form1" name="selStation" id="selStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS S WHERE S.OrganizationId='" . cOrganizationId . "' ORDER BY S.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					if ($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId"))
		               $sStationName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' .  $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
		            else if ($iStationId == $objDatabase->Result($varResult2, $i, "S.StationId"))
		            	$sStationName .= '';
		            else
		               $sStationName .= '<option value="' . $objDatabase->Result($varResult2, $i, "S.StationId") . '">' . $objDatabase->Result($varResult2, $i, "S.StationName") . '</option>';
				}
				$sStationName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selStation\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selStation\'), \'../organization/stations.php?pagetype=details&id=\'+GetSelectedListBox(\'selStation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Station Details" title="Station Details" /></a>';

				$sCurrentLocation_StationName = '<select class="form1" name="selCurrentStation" id="selCurrentStation">';
				$varResult2 = $objDatabase->Query("SELECT * FROM organization_stations AS CS WHERE CS.OrganizationId='" . cOrganizationId . "' ORDER BY CS.StationName");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					if ($iCurrentLocation_StationId == $objDatabase->Result($varResult2, $i, "CS.StationId"))
		               $sCurrentLocation_StationName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "CS.StationId") . '">' .  $objDatabase->Result($varResult2, $i, "CS.StationName") . '</option>';
		            else if ($iCurrentLocation_StationId == $objDatabase->Result($varResult2, $i, "CS.StationId"))
		            	$sCurrentLocation_StationName .= '';
		            else
		               $sCurrentLocation_StationName .= '<option value="' . $objDatabase->Result($varResult2, $i, "CS.StationId") . '">' . $objDatabase->Result($varResult2, $i, "CS.StationName") . '</option>';
				}
				$sCurrentLocation_StationName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selCurrentStation\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selCurrentStation\'), \'../organization/stations?pagetype=details&id=\'+GetSelectedListBox(\'selCurrentStation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Station Details" title="Station Details" /></a>';

                $sFixedAssetName = '<input type="text" name="txtFixedAssetName" id="txtFixedAssetName" class="form1" value="' . $sFixedAssetName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sFixedAssetCost = '<input type="text" name="txtFixedAssetCost" id="txtFixedAssetCost" class="form1" value="' . $dFixedAssetCost . '" size="10" />&nbsp;<span style="color:red;">*</span>';

				$sFixedAssetStockNumber = '<input type="text" name="txtFixedAssetStockNumber" id="txtFixedAssetStockNumber" class="form1" value="' . $sFixedAssetStockNumber . '" size="30" />';
				$sFixedAssetRecoveryPeriod = '<input type="text" name="txtFixedAssetRecoveryPeriod" id="txtFixedAssetRecoveryPeriod" class="form1" value="' . $iFixedAssetRecoveryPeriod . '" size="8" />';
				$sFixedAssetSalvageValue = '<input type="text" name="txtFixedAssetSalvageValue" id="txtFixedAssetSalvageValue" class="form1" value="' . $dFixedAssetSalvageValue . '" size="15" />';

				$dFixedAssetWeight = '<input type="text" name="txtFixedAssetWeight" id="txtFixedAssetWeight" class="form1" value="' . $dFixedAssetWeight . '" size="5" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
			   	$sFixedAssetAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));

			   	for ($i=0; $i < count($this->aFixedAssetDepreciationMethod); $i++)
			   		$sFixedAssetDepreciationMethod .= '<input ' . (($iFixedAssetDepreciationMethod == $i) ? 'checked="true"' : '') . ' type="radio" name="radFixedAssetDepreciationMethod" id="radFixedAssetDepreciationMethod" value="' . $i . '">' . $this->aFixedAssetDepreciationMethod[$i] . '</option>&nbsp;' .  '<br />';

			   	//$sDepreciationValue = '<div id="divDepreciationValue_StraightLineMethod" style="display:none;"><input type="text" name="txtDepreciationValue" id="txtDepreciationValue" class="form1" value="' . $dDepreciationValue . '" size="5" /> % per year</div>';
				
				$sDepreciationValue = '<input type="text" name="txtDepreciationValue" id="txtDepreciationValue" class="form1" value="' . number_format($dDepreciationValue, 2) . '" size="5" /> </div>';
				
				/*
			   	$sDepreciationValue .= '<div id="divDepreciationValue_AcceleratedMethod" style="display:none;">';
	    		for ($i=0; $i < $objEmployee->aSystemSettings['FMS_Vendors_Depreciation_AcceleratedMaxYears']; $i++)
			   		$sDepreciationValue .= 'Year ' . ($i+1) . ': <input type="text" name="txtDepreciationValue' . ($i+1) . '" id="txtDepreciationValue' . ($i+1) . '" class="form1" value="' . $aDepreciationValue[$i] . '" size="5" /> %<br />';
			   	$sDepreciationValue .= '</div>

			   	<script language="JavaScript" type="text/javascript">
			   	function ChangeDepreciationMethod()
			   	{
			   		var iSelectedDepreciationMethod = GetCheckedValue(document.forms[0].radFixedAssetDepreciationMethod);

			   		if (iSelectedDepreciationMethod == 0)
			   		{
			   			HideDiv("divDepreciationValue_AcceleratedMethod");
			   			ShowDiv("divDepreciationValue_StraightLineMethod");
			   		}
			   		else if (iSelectedDepreciationMethod == 1)
			   		{
			   			ShowDiv("divDepreciationValue_AcceleratedMethod");
			   			HideDiv("divDepreciationValue_StraightLineMethod");
			   		}
			   		else
			   		{
		   				HideDiv("divDepreciationValue_AcceleratedMethod");
		   				ShowDiv("divDepreciationValue_StraightLineMethod");
			   		}
			   	}

			   	ChangeDepreciationMethod();
			   	</script>';
				*/
				
				$sFixedAssetConsumable = '<input type="checkbox" ' . (($iFixedAssetConsumable == 1) ? 'checked="true"' : '') . ' name="chkFixedAssetConsumable" id="chkFixedAssetConsumable" value="on" />';				
			   	$sFixedAssetCode = '?';
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
		    function ValidateForm()
		    {
				if (GetVal(\'txtFixedAssetName\') == "") return(AlertFocus(\'Please enter a valid FixedAsset Name\', \'txtFixedAssetName\'));
				if (!isNumeric(GetVal(\'txtFixedAssetRecoveryPeriod\'))) return(AlertFocus(\'Please enter a valid FixedAsset Recovery Period\', \'txtFixedAssetRecoveryPeriod\'));
				if (!isDate(GetVal(\'txtDateOfPurchase\'))) return(AlertFocus(\'Please enter a valid FixedAsset Purchase Date\', \'txtDateOfPurchase\'));
				if (!isNumeric(GetVal("txtFixedAssetCost"))) return(AlertFocus("Please enter a valid value for FixedAsset Cost", "txtFixedAssetCost"));
				if (!isNumeric(GetVal("txtFixedAssetWeight"))) return(AlertFocus("Please enter a valid value for FixedAsset Weight", "txtFixedAssetWeight"));

				return true;
		    }
		    </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="GridTR"><td colspan="4"><span class="Details_Title">Fixed Asset Information:</span></td></tr>
			 <tr bgcolor="#ffffff">
			  <td width="20%">Fixed Asset Added At:</td><td><strong>' . $sStationName . '</strong></td>
			  <td colspan="2" bgcolor="#ffffff" valign="top" align="center" rowspan="8">
			   <fieldset><legend style="font-size:12px; font-weight:bold;">Fixed Asset Code:</legend><table border="0" cellspacing="0" cellpadding="10" align="center" width="100%"><tr><td align="center" style="font-size:20px; font-weight:bold; color:green;">' . $sFixedAssetCode . '</td></tr></table></fieldset>
			   <br />
			   <fieldset><legend style="font-size:12px; font-weight:bold;">Fixed Asset Barcode:</legend><table border="0" cellspacing="0" cellpadding="10" align="center" width="100%"><tr><td align="center"><img src="' . $sBarcodeImage . '" border="0" alt="Fixed Asset Barcode" title="Fixed Asset Barcode" /></td></tr></table></fieldset>
			  </td>
			 </tr>
			 <!--<tr bgcolor="#edeff1"><td>Fixed Asset Added At:</td><td><strong>' . $sStationName . '</strong></td></tr>-->
			 <tr bgcolor="#edeff1"><td>Category Name:</td><td><strong>' . $sCategoryName . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Vendor Name:</td><td><strong>' . $sVendorName . '</strong></td></tr>
             <tr bgcolor="#edeff1"><td>Manufacturer Name:</td><td><strong>' . $sManufacturerName . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Donor Project:</td><td><strong>' . $sDonorProject . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Fixed Asset Name:</td><td><strong>' . $sFixedAssetName . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td>Fixed Asset Stock Number:</td><td><strong>' . $sFixedAssetStockNumber . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Fixed Asset Current Location:</td><td colspan="3"><strong>' . $sCurrentLocation_StationName . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">Description:</td></tr>
			 <tr><td colspan="4" valign="top"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="GridTR"><td colspan="4"><span class="Details_Title">Fixed Asset Attributes:</span></td></tr>
			 <tr><td>Fixed Asset Cost:</td><td><strong>' . $objEmployee->aSystemSettings['CurrencySign'] . $sFixedAssetCost . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Fixed Asset Weight:</td><td colspan="3"><strong>' . $dFixedAssetWeight . ' ' . $objEmployee->aSystemSettings['Vendors_FixedAssetWeightUnit'] . '</strong></td></tr>
			 <tr><td>Fixed Asset Status:</td><td colspan="3"><strong>' . $sFixedAssetStatus . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Fixed Asset Purchase Date:</td><td colspan="3"><strong>' . $sFixedAssetPurchase_Date . '</strong></td></tr>
			 <tr><td>Fixed Asset Consumable:</td><td colspan="3"><strong>' . $sFixedAssetConsumable . '</strong></td></tr>
			 <tr class="GridTR"><td colspan="4"><span class="Details_Title">Fixed Asset Depreciation:</span></td></tr>
			 <tr><td>Fixed Asset Recovery Period:&nbsp;</td><td colspan="3"><strong>' . $sFixedAssetRecoveryPeriod . '&nbsp;&nbsp;years</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Fixed Asset Salvage Value:&nbsp;</td><td colspan="3"><strong>' . $objEmployee->aSystemSettings['CurrencySign'] . $sFixedAssetSalvageValue . '</strong></td></tr>
			 <tr><td valign="top">Depreciation Method:&nbsp;</td><td colspan="3"><strong>' . $sFixedAssetDepreciationMethod . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Depreciation Value:&nbsp;</td><td colspan="3"><strong>' . $sDepreciationValue . '&nbsp;(% per year)</strong></td></tr>
			 <tr class="GridTR"><td colspan="4"><span class="Details_Title">Extra Information:</span></td></tr>
		     <tr><td valign="top">Notes:</td><td colspan="3"><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Fixed Asset Added On:</td><td colspan="3"><strong>' . $sFixedAssetAddedOn . '</strong></td></tr>
			 <tr><td>Fixed Asset Added By:</td><td><strong>' . $sFixedAssetAddedBy . '</strong></td></tr>
			 </table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iFixedAssetId . '"><input type="hidden" name="action" id="action" value="UpdateFixedAsset"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Fixed Asset" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewFixedAsset"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';

		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
	}

    // Add New FixedAsset
	function AddNewFixedAsset($iCategoryId, $iVendorId, $iManufacturerId, $sFixedAssetName, $sFixedAssetStockNumber, $iStationId, $iDonorProjectId, $iCurrentLocation_StationId,  $dFixedAssetPurchaseDate, $iFixedAssetStatus, $dFixedAssetCost, $dFixedAssetWeight, $iRecoveryPeriod, $dSalvageValue, $iDepreciationMethod, $dDepreciationValue, $sDescription, $iFixedAssetConsumable, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;

        $varNow = $objGeneral->fnNow();
		// Employee Roles
   		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[1] == 0) // Add Disabled
			return(1010);

   		$iFixedAssetAddedBy = $objEmployee->iEmployeeId;

		// FixedAsset StockNumber already added in the system
        // if ($objDatabase->DBCount("fms_vendors_fixedassets AS FA", "FA.FixedAssetStockNumber='$sFixedAssetStockNumber'") > 0) return(6014);

        $sFixedAssetName = $objDatabase->RealEscapeString($sFixedAssetName);
        $sFixedAssetStockNumber = $objDatabase->RealEscapeString($sFixedAssetStockNumber);
        $sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);

        if ($iDepreciationMethod == '') $iDepreciationMethod = 0;
        if ($dFixedAssetWeight == '') $dFixedAssetWeight = 0;
        if ($iRecoveryPeriod == '') $iRecoveryPeriod = 1;
        if ($dSalvageValue == '') $dSalvageValue = 0;
        if ($dFixedAssetCost == '') $dFixedAssetCost = 0;
		if($dDepreciationValue == '') $dDepreciationValue = 0;
		if ($iFixedAssetConsumable == "on") $iFixedAssetConsumable = 1; else $iFixedAssetConsumable = 0;
		/*
        if ($iDepreciationMethod == 0)				// Straight-Line Depreciation Method
        	$aDepreciationValue[0] = $dDepreciationValue;
        else if ($iDepreciationMethod == 1)			// Accelerated Depreciation Method
        {
        	for ($i=0; $i < $objEmployee->aSystemSettings['FMS_Vendors_Depreciation_AcceleratedMaxYears']; $i++)
        		$aDepreciationValue[$i] = $objGeneral->fnGet("txtDepreciationValue" . ($i+1));
        }

        $sDepreciationValue = serialize($aDepreciationValue);
		*/

        $varResult = $objDatabase->Query("INSERT INTO fms_vendors_fixedassets
        (OrganizationId, CategoryId, VendorId, ManufacturerId, FixedAssetName, FixedAssetStockNumber, StationId, DonorProjectId,
        CurrentLocation_StationId, FixedAssetPurchaseDate , FixedAssetStatus,
        RecoveryPeriod, SalvageValue, DepreciationMethod, DepreciationValue,
        FixedAssetCost, FixedAssetWeight, Notes, FixedAssetDescription, FixedAssetConsumable, FixedAssetAddedOn, FixedAssetAddedBy)
        VALUES
        ('" . cOrganizationId . "', '$iCategoryId', '$iVendorId', '$iManufacturerId', '$sFixedAssetName', '$sFixedAssetStockNumber', '$iStationId', '$iDonorProjectId',
        '$iCurrentLocation_StationId', '$dFixedAssetPurchaseDate', '$iFixedAssetStatus',
        '$iRecoveryPeriod', '$dSalvageValue', '$iDepreciationMethod', '$dDepreciationValue',
        '$dFixedAssetCost', '$dFixedAssetWeight', '$sNotes', '$sDescription', '$iFixedAssetConsumable', '$varNow', '$iFixedAssetAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets AS FA INNER JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = FA.CategoryId WHERE FA.OrganizationId='" . cOrganizationId . "' AND FA.FixedAssetName='$sFixedAssetName' AND FA.FixedAssetAddedOn='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {
        	$iFixedAssetId = $objDatabase->Result($varResult, 0, "FA.FixedAssetId");
        	$sCategoryCode = $objDatabase->Result($varResult, 0, "C.CategoryCode");
  	        
			$sFixedAssetCode = $this->GetFixedAssetCode($iFixedAssetId, $iCategoryId, $sCategoryCode, $dFixedAssetPurchaseDate);

  	        $varResult = $objDatabase->Query("UPDATE fms_vendors_fixedassets SET FixedAssetCode='$sFixedAssetCode' WHERE FixedAssetId='$iFixedAssetId'");
            $varResult = $objDatabase->Query("UPDATE fms_vendors_fixedassets_categories SET FixedAssetCodeSeries=FixedAssetCodeSeries+1 WHERE CategoryId='$iCategoryId'");
			
  			$objSystemLog->AddLog("Add New FixedAsset - " . $sFixedAssetName);
			
  			$objGeneral->fnRedirect('?pagetype=details&error=6001&id=' . $iFixedAssetId);
        }
        else
             return(6002);
    }

	// Update FixedAsset
	function UpdateFixedAsset($iFixedAssetId, $iCategoryId, $iVendorId, $iManufacturerId, $sFixedAssetName, $sFixedAssetStockNumber, $iStationId, $iDonorProjectId, $iCurrentLocation_StationId,  $dFixedAssetPurchaseDate, $iFixedAssetStatus, $dFixedAssetCost, $dFixedAssetWeight, $iRecoveryPeriod, $dSalvageValue, $iDepreciationMethod, $dDepreciationValue, $sDescription, $iFixedAssetConsumable, $sNotes)
    {
        global $objDatabase;
        global $objEmployee;
        global $objGeneral;
		global $objSystemLog;

		// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[2] == 0) // Update Disabled
			return(1010);

	    // FixedAsset StockNumber already added in the system
        //if ($objDatabase->DBCount("fms_vendors_fixedassets AS FA", "FA.FixedAssetId <> '$iFixedAssetId' AND FA.FixedAssetStockNumber='$sFixedAssetStockNumber'") > 0) return(6014);

	    $sFixedAssetName = $objDatabase->RealEscapeString($sFixedAssetName);
        $sDescription = $objDatabase->RealEscapeString($sDescription);
		$sFixedAssetStockNumber = $objDatabase->RealEscapeString($sFixedAssetStockNumber);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		if($dDepreciationValue == '') $dDepreciationValue = 0;
		if ($iFixedAssetConsumable == "on") $iFixedAssetConsumable = 1; else $iFixedAssetConsumable = 0;
		/*
        if ($iDepreciationMethod == 0)				// Straight-Line Depreciation Method
        	$aDepreciationValue[0] = $dDepreciationValue;
        else if ($iDepreciationMethod == 1)			// Accelerated Depreciation Method
        {
        	for ($i=0; $i < $objEmployee->aSystemSettings['FMS_Vendors_Depreciation_AcceleratedMaxYears']; $i++)
        		$aDepreciationValue[$i] = $objGeneral->fnGet("txtDepreciationValue" . ($i+1));
        }

        $sDepreciationValue = serialize($aDepreciationValue);
		*/

        $varResult = $objDatabase->Query("
	    UPDATE fms_vendors_fixedassets
		SET
        	CategoryId='$iCategoryId',
        	VendorId='$iVendorId',
            ManufacturerId='$iManufacturerId',
            FixedAssetName='$sFixedAssetName',
            FixedAssetStockNumber='$sFixedAssetStockNumber',
            StationId='$iStationId',
            DonorProjectId='$iDonorProjectId',
            CurrentLocation_StationId='$iCurrentLocation_StationId',
            FixedAssetPurchaseDate='$dFixedAssetPurchaseDate',
            FixedAssetStatus='$iFixedAssetStatus',
            FixedAssetCost='$dFixedAssetCost',
            FixedAssetWeight='$dFixedAssetWeight',
            RecoveryPeriod='$iRecoveryPeriod',
            SalvageValue='$dSalvageValue',
            DepreciationMethod='$iDepreciationMethod',
            DepreciationValue='$dDepreciationValue',
			FixedAssetConsumable='$iFixedAssetConsumable',
            Notes='$sNotes',
            FixedAssetDescription='$sDescription'
    	WHERE OrganizationId='" . cOrganizationId . "' AND FixedAssetId='$iFixedAssetId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Update FixedAsset - " . $sFixedAssetName);
            $objGeneral->fnRedirect('../vendors/fixedassets.php?pagetype=details&error=6003&id=' . $iFixedAssetId);
        }
        else
            return(6004);
    }
	
	// Delete FixedAsset
    function DeleteFixedAsset($iFixedAssetId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;

        // Employee Roles
	   	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets[3] == 0) // Delete Disabled
	   	{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iFixedAssetId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}
				
        $varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets AS FA WHERE FA.OrganizationId='" . cOrganizationId . "' AND FA.FixedAssetId='$iFixedAssetId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid FixedAsset Id');

        $sFixedAssetName = $objDatabase->Result($varResult, 0, "FA.FixedAssetName");
		$sFixedAssetName = $objDatabase->RealEscapeString($sFixedAssetName);		
		/*
		if ($objDatabase->DBCount("fms_vendors_fixedassets_images AS FAI", "PI.FixedAssetId='$iFixedAssetId'") > 0) return(6015);
		if ($objDatabase->DBCount("fms_vendors_fixedassets_repairs AS FAR", "PR.FixedAssetId='$iFixedAssetId'") > 0) return(6016);
		if ($objDatabase->DBCount("fms_vendors_fixedassets_rent AS FAR", "PR.FixedAssetId='$iFixedAssetId'") > 0) return(6017);
		if ($objDatabase->DBCount("fms_vendors_fixedassets_sell AS FAS", "PS.FixedAssetId='$iFixedAssetId'") > 0) return(6018);
		if ($objDatabase->DBCount("fms_vendors_fixedassets_assign AS FAA", "PA.FixedAssetId='$iFixedAssetId'") > 0) return(6019);
		if ($objDatabase->DBCount("fms_vendors_fixedassets_checkout AS FAC", "PC.FixedAssetId='$iFixedAssetId'") > 0) return(6021);
		*/
        $varResult = $objDatabase->Query("DELETE FROM fms_vendors_fixedassets WHERE FixedAssetId='$iFixedAssetId'");
       	if ($objDatabase->AffectedRows($varResult) > 0)
        {           	
  			$objSystemLog->AddLog("Delete FixedAsset - " . $sFixedAssetName);
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iFixedAssetId . '&error=6005');
			else
				$objGeneral->fnRedirect('?id=0&error=6005');
        }
        else
              return(6006);
    }

    function ReIssueFixedAssetCode($iFixedAssetId)
    {
    	global $objDatabase;

	    $varResult = $objDatabase->Query("SELECT * FROM vendors_fixedassets AS FA INNER JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = FA.CategoryId WHERE FA.OrganizationId='" . cOrganizationId . "' AND FA.FixedAssetId='$iFixedAssetId'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {
        	$iFixedAssetId = $objDatabase->Result($varResult, 0, "FA.FixedAssetId");
        	$sCategoryCode = $objDatabase->Result($varResult, 0, "C.CategoryCode");
        	$iCategoryId = $objDatabase->Result($varResult, 0, "C.CategoryId");
        	$dFixedAssetPurchaseDate = $objDatabase->Result($varResult, 0, "FA.FixedAssetPurchaseDate");
  	        $sFixedAssetCode = $this->GetFixedAssetCode($iFixedAssetId, $iCategoryId, $sCategoryCode, $dFixedAssetPurchaseDate);

  	        $varResult = $objDatabase->Query("UPDATE vendors_fixedassets SET FixedAssetCode='$sFixedAssetCode' WHERE FixedAssetId='$iFixedAssetId'");
        }

        return(6020);
    }
    
    function ShowAllFixedAssetImages($iFixedAssetId)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

    	$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;

    	$i = 1;
        // Try to open the directory
        $sFixedAssetImagesDir = cDataFolder . "/vendors/fixedassets/thumbnails/" . $iFixedAssetId;

        if (!file_exists($sFixedAssetImagesDir))
        	mkdir($sFixedAssetImagesDir);

        $varResult = $objDatabase->Query("SELECT * FROM vendors_fixedassets AS FA WHERE FA.FixedAssetId='$iFixedAssetId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid FixedAsset Id');

        $sFixedAssetName = $objDatabase->Result($varResult, 0, "FA.FixedAssetName");

        $varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_images AS FAI WHERE PI.FixedAssetId='$iFixedAssetId'");

        $sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">FixedAsset Images for ' . $sFixedAssetName . '</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0"  align="center">
		 <tr>
		  <td width="100%">
 	 	   <br />
		   <table width="100%" border="0" cellpadding="0" cellspacing="0" >
		    <tr>';

        if ($objDatabase->RowsNumber($varResult) <= 0)
        {
        	$sReturn .= '<br /><br /><div align="center">No Images Found...<br /></div>';
        }
        else
        {
        	for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
        	{
				$iFixedAssetImageId = $objDatabase->Result($varResult, $i, "PI.FixedAssetImageId");
				$sImageCaption = $objDatabase->Result($varResult, $i, "PI.ImageCaption");

				$sImageThumbnailFile = cDataFolder . "/vendors/fixedassets/thumbnails/" . $iFixedAssetId . '/' . $iFixedAssetImageId;
				$sImageThumbnailFile = (file_exists($sImageThumbnailFile . '.gif')) ? $sImageThumbnailFile . '.gif' : $sImageThumbnailFile . '.jpg';

				$sImageFile = cDataFolder . "/vendors/fixedassets/images/" . $iFixedAssetId . '/' . $iFixedAssetImageId;
				$sImageFile = (file_exists($sImageFile . '.gif')) ? $sImageFile . '.gif' : $sImageFile . '.jpg';

	            list($iImageWidth, $iImageHeight, $sImageType, $sImageAttr) = getimagesize($sImageFile);
            	$iImageWidth = $iImageWidth + 35;
            	$iImageHeight = $iImageHeight + 50;

				$sReturn .= '
			 	<td valign="top" align="center">
			 	 <a href="#noanchor" onclick="jsOpenWindow(\'../fixedassets/fixedassets_images_preview.php?id=' . $iFixedAssetId . '&img=' . $iFixedAssetImageId . '\', ' . $iImageWidth . ', ' . $iImageHeight . ');">
		  	     <img src="' . $sImageThumbnailFile . '" alt="' . $sImageCaption . '" title="' . $sImageCaption . '" border="0" />
		  	     </a>
			  	 <br />
			  	 ' . $sImageCaption . '
			  	 <br /><br />
			  	 [ <a style="color:red;" href="../fixedassets/fixedassets_images.php?action=DeleteFixedAssetImage&id=' . $iFixedAssetId . '&fixedassetimageid=' . $iFixedAssetImageId . '">Delete Image</a> ]
			 	</td>';

				if ((($i+1) % 3) == 0)
					$sReturn .= '</tr><tr>';
			}
        }

        $sReturn .= '
         </tr>
         <tr>
          <td colspan="5">
           <br /><br />
            <a href="#noanchor" onclick="ShowHideDiv(\'divAddNewImage\');" style="color:green; font-size:13px; font-family:Arial; font-weight:bold;" href=""><img align="absmiddle" src="../images/icons/plus.gif" border="0" alt="Add New Image" title="Add New Image" />&nbsp;Add New Image</a>
            <form method="post" action="../fixedassets/fixedassets_images.php" enctype="multipart/form-data">
            <div id="divAddNewImage" align="center" style="display:none;">
             <fieldset style="width:50%;"><legend class="title3">Add New Image</legend>
             <br />
	         Select an Image to Upload for this FixedAsset:<br />
	         <input class="form1" type="file" name="fileField1" accept="text/*" size="30" maxlength="100000000" /><br /><br />
	         Image Caption:<br />
	         <input type="text" class="form1" name="txtImageCaption" id="txtImageCaption" size="38" />
	         <br /><br />
	         <input type="submit" value="Upload Image" class="formbutton">
	         <br /><br />
	        </fieldset>
            </div>
            <input type="hidden" name="action" id="action" value="UploadFixedAssetImage">
            <input type="hidden" name="id" id="id" value="' . $iFixedAssetId. '">
            </form>
            <br />
		   </td>
		  </table>
         </tr>
        </table>';

      	return ($sReturn);
    }

    function FixedAssetImagePreview($iFixedAssetId, $iFixedAssetImageId)
    {
    	global $objDatabase;

    	$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_images AS FAI WHERE PI.FixedAssetId='$iFixedAssetId' AND PI.FixedAssetImageId='$iFixedAssetImageId'");
    	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid FixedAsset Image!!');

		$sImageCaption = $objDatabase->Result($varResult, 0, "PI.ImageCaption");

    	$sFixedAssetImage = cDataFolder . '/vendors/fixedassets/images/' . $iFixedAssetId . '/' . $iFixedAssetImageId;

    	$sFixedAssetImage = (file_exists($sFixedAssetImage . '.gif')) ? $sFixedAssetImage . '.gif' : $sFixedAssetImage . '.jpg';

		$sReturn = '<table border="0" cellspacing="0" cellpadding="5" width="99%" align="center">
		 <tr>
		  <td align="center">
		   <a href="#noanchor" onclick="window.close();"><img src="' . $sFixedAssetImage . '" border="0" alt="Click to Close this Window" title="Click to Close this Window" /></a>
		   <br /><br />
		   <span style="font-size:14px; font-family:Tahoma, Arial;">
		   ' . $sImageCaption . '
		   </span>
		  </td>
		 </tr>
		 </table>';

		$sReturn .= '<script language=javascript>self.focus();</script>';

		return($sReturn);
    }

	function GetFixedAssetCode($iFixedAssetId, $iCategoryId = '', $sCategoryCode = '', $dFixedAssetPurchase_Date = '')
	{
		global $objEmployee;
		global $objDatabase;

		if (($sCategoryCode == '') && ($dFixedAssetPurchase_Date == ''))
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets AS FA
			INNER JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = FA.FixedAssetId
			WHERE FA.OrganizationId='" . cOrganizationId . "' AND FA.FixedAssetId='$iFixedAssetId'");
			if ($objDatabase->RowsNumber($varResult) <= 0) return("");

			$iCategoryId = $objDatabase->Result($varResult, 0, "C.CategoryId");
			$sCategoryCode = $objDatabase->Result($varResult, 0, "C.CategoryCode");
			$dFixedAssetPurchase_Date = $objDatabase->Result($varResult, 0, "FA.FixedAssetPurchaseDate");
		}		
		$sFixedAssetCode = $objEmployee->aSystemSettings['VF_Vendors_FixedAssetCodeRule'];		
		$sFixedAssetCode = str_replace('[YYYY]', date("Y", strtotime($dFixedAssetPurchase_Date)), $sFixedAssetCode);
		$sFixedAssetCode = str_replace('[Category Code]', $sCategoryCode, $sFixedAssetCode);
		$sFixedAssetCode = str_replace('[FixedAsset Id]', $iFixedAssetId, $sFixedAssetCode);
		

		//$iSerialNumber = $objDatabase->DBCount("vendors_fixedassets AS FA INNER JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = FA.CategoryId", "FA.CategoryId='$iCategoryId' AND FA.FixedAssetId < '$iFixedAssetId'");
		$iSerialNumber = $objDatabase->DBGetSingleValue("SELECT C.FixedAssetCodeSeries FROM fms_vendors_fixedassets_categories AS C WHERE C.OrganizationId='" . cOrganizationId . "' AND C.CategoryId='$iCategoryId'", "C.FixedAssetCodeSeries");
		$iSerialNumber++;

		$sFixedAssetCode = str_replace('[Serial Number]', $iSerialNumber, $sFixedAssetCode);
		// Barcode Generator
		include(cVSFFolder . '/classes/clsBarcodeGenerator.php');
		
		$objBarCode = new clsBarcodeGenerator();
		$objBarCode->GenerateCode128($sFixedAssetCode, cDataFolder . '/vf/vendors/barcodes/' . $iFixedAssetId . '.jpg');

		return($sFixedAssetCode);
	}

    function FixedAssetsImageUpload($iFixedAssetId, $sImageCaption)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;

        $varNow = $objGeneral->fnNow();

		include(cVSFFolder . '/classes/clsUploader.php');
		$up = new clsUploader();

	   	$sAllowedTypes = array("image/jpeg", "image/pjpeg", "image/gif");
		$sUploadPath = cDataFolder . '/vendors/fixedassets/images/' . $iFixedAssetId . '/';
		if (!file_exists($sUploadPath)) mkdir($sUploadPath);

		$sUploadPath2 = cDataFolder . '/vendors/fixedassets/thumbnails/' . $iFixedAssetId . '/';
        if (!file_exists($sUploadPath2)) mkdir($sUploadPath2);
        $varPath = $objGeneral->dumpAssociativeArray($up->uploadTo($sUploadPath, true, $sAllowedTypes));
		
		$sImageCaption = $objDatabase->RealEscapeString($sImageCaption);
		
        $varResult = $objDatabase->Query("INSERT INTO fms_vendors_fixedassets_images (FixedAssetId, ImageCaption, ImageAddedDateTime) VALUES ('$iFixedAssetId', '$sImageCaption', '$varNow')");
        $varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_images AS FAI WHERE PI.ImageAddedDateTime='$varNow' AND PI.FixedAssetId='$iFixedAssetId'");

        if ($objDatabase->RowsNumber($varResult) > 0)
	    {
	    	$iImageId = $objDatabase->Result($varResult, 0, "PI.FixedAssetImageId");

	        if ($varPath != '')
	        {
	      		$sFileName = explode('/', $varPath);
	        	$sFileName = $sFileName[count($sFileName)-1];
	    	    $sExt = explode('.', $sFileName);
	    	    $sExt = $sExt[count($sExt)-1];

	            $sExt2 = (strtolower($sExt) == "jpg") ? "gif" : "jpg";
	            $sNewFileName = $sUploadPath . $iImageId . '.' . strtolower($sExt);
	            $sNewFileName2 = $sUploadPath . $iImageId . '.' . strtolower($sExt2);

	            if (file_exists($varPath))
	            {
	            	if (file_exists($sNewFileName)) unlink($sNewFileName);
	                if (file_exists($sNewFileName2)) unlink($sNewFileName2);

	                rename($varPath, $sNewFileName);

	                // Generate Thumbnail
		            include(cVSFFolder . '/classes/clsThumbnails.php');
		            $objThumbnail = new thumbs($sUploadPath);

		            $iThumbnailHeight = $objEmployee->aSystemSettings["Vendors_FixedAssetThumbnailHeight"];
	                $iThumbnailWidth = $objEmployee->aSystemSettings["Vendors_FixedAssetThumbnailWeight"];

	                $objThumbnail->fnCreateThumbnail($sUploadPath, $sUploadPath2, $iImageId . '.' . strtolower($sExt), $iThumbnailWidth, $iThumbnailHeight, $iImageId . '.' . strtolower($sExt));
                }
			}
	        else
	        	return(6008);

	        return(6009);
		}
        else
        	return(6008);
    }

    function FixedAssetsImageDelete($iFixedAssetId, $iFixedAssetImageId)
    {
        global $objDatabase;
        global $objGeneral;

		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_images AS FAI WHERE PI.FixedAssetImageId='$iFixedAssetImageId' AND PI.FixedAssetId='$iFixedAssetId'");
       	if ($objDatabase->RowsNumber($varResult) > 0)
        {
        	$sImageFolder = cDataFolder . '/vendors/fixedassets/images/' . $iFixedAssetId;
        	$sImageThumbnailFolder = cDataFolder . '/vendors/fixedassets/thumbnails/' . $iFixedAssetId;

        	// Delete Image File
        	if (file_exists($sImageFolder . '/' . $iFixedAssetImageId . '.gif')) unlink($sImageFolder . '/' . $iFixedAssetImageId . '.gif');
        	if (file_exists($sImageFolder . '/' . $iFixedAssetImageId . '.jpg')) unlink($sImageFolder . '/' . $iFixedAssetImageId . '.jpg');

        	// Delete Image Thumbnail
        	if (file_exists($sImageThumbnailFolder . '/' . $iFixedAssetImageId . '.gif')) unlink($sImageThumbnailFolder . '/' . $iFixedAssetImageId . '.gif');
        	if (file_exists($sImageThumbnailFolder . '/' . $iFixedAssetImageId . '.jpg')) unlink($sImageThumbnailFolder . '/' . $iFixedAssetImageId . '.jpg');

        	// Remove from the Database
        	$varResult = $objDatabase->Query("DELETE FROM fms_vendors_fixedassets_images WHERE FixedAssetImageId='$iFixedAssetImageId' AND FixedAssetId='$iFixedAssetId'");
        	if ($objDatabase->AffectedRows($varResult) > 0)
        		return(6010);
        }

        return(6011);
    }

    function AJAX_GetFixedAssetInfo($iFixedAssetId)
    {
    	global $objDatabase;

	   	$objResponse = new xajaxResponse();

	   	$varResult = $objDatabase->Query("SELECT *
		FROM vendors_fixedassets AS FA
		LEFT JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = FA.CategoryId
		LEFT JOIN fms_vendors AS V ON V.VendorId = FA.VendorId
		WHERE FA.FixedAssetId = '$iFixedAssetId'");

		if ($objDatabase->RowsNumber($varResult) <= 0)
			$objResponse->addAlert("Sorry, FixedAsset not found...");
		else
		{
	    	$iCategoryId = $objDatabase->Result($varResult, 0, "FA.CategoryId");
	    	$iManufacturerId = $objDatabase->Result($varResult, 0, "FA.ManufacturerId");
	    	$iVendorId = $objDatabase->Result($varResult, 0, "FA.VendorId");

	    	$sCategoryName = $objDatabase->Result($varResult, 0, "C.CategoryName");
	    	$sCategoryName = $sCategoryName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sCategoryName) . '\', \'../vendors/fixedassets.php?pagetype=categories_details&id=' . $iCategoryId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Category Information" title="Category Information" border="0" /></a>';

		    $sManufacturerName = $objDatabase->Result($varResult, 0, "M.ManufacturerName");
	    	$sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");

	        if ($sManufacturerName == '') $sManufacturerName = 'No Manufacturer';
	        else $sManufacturerName = $sManufacturerName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sManufacturerName) . '\', \'../vendors/fixedassets.php?pagetype=manufacturers_details&id=' . $iManufacturerId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Manufacturer Information" title="Manufacturer Information" border="0" /></a>';

	        if ($sVendorName == '') $sVendorName = 'No Vendor';
	        else $sVendorName = $sVendorName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sVendorName) . '\', \'../vendors/vendors.php?pagetype=details&id=' . $iVendorId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" alt="Vendor Information" align="absmiddle" title="Vendor Information" border="0" /></a>';

		   	$objResponse->addAssign("divCategoryName", "innerHTML", $sCategoryName);
		   	$objResponse->addAssign("divManufacturerName", "innerHTML", $sManufacturerName);
		   	$objResponse->addAssign("divVendorName", "innerHTML", $sVendorName);
		}

		return($objResponse->getXML());
    }

    function GetFixedAssetInfo($iFixedAssetId, $bEditableInfo = false)
    {
    	global $objDatabase;
    	$sJavaScript = '';

    	if ($bEditableInfo)
    	{
    		$sFixedAssetName = '<select class="form1" name="selFixedAsset" id="selFixedAsset" onchange="ChangeFixedAssetInformation(GetSelectedListBox(\'selFixedAsset\'));">';
    		$varResult = $objDatabase->Query("SELECT * FROM vendors_fixedassets AS FA ORDER BY FA.FixedAssetName");
    		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
    		{
    			$iCurrentFixedAssetId = $objDatabase->Result($varResult, $i, "FA.FixedAssetId");
    			$sCurrentFixedAssetName = $objDatabase->Result($varResult, $i, "FA.FixedAssetName");
    			$sFixedAssetName .= '<option value="' . $iCurrentFixedAssetId . '">' . $sCurrentFixedAssetName . '</option>';
    		}
    		$sFixedAssetName .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selFixedAsset\'), \'../fixedassets/fixedassets.php?pagetype=details&id=\'+GetSelectedListBox(\'selFixedAsset\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" align="absmiddle" alt="FixedAsset Information" title="FixedAsset Information" border="0" /></a>';

    		$sCategoryName = '<div id="divCategoryName"></div>';
    		$sManufacturerName = '<div id="divManufacturerName"></div>';
    		$sVendorName = '<div id="divVendorName"></div>';

    		$sJavaScript = '<script type="text/javascript" language="JavaScript">
    		function ChangeFixedAssetInformation(iFixedAssetId)
    		{
    			xajax_AJAX_GetFixedAssetInfo(iFixedAssetId);
    		}

    		ChangeFixedAssetInformation(' . $iFixedAssetId . ');
	    	</script>';
    	}
    	else
    	{
			$varResult = $objDatabase->Query("SELECT *
			FROM vendors_fixedassets AS FA
			LEFT JOIN fms_vendors_fixedassets_categories AS C ON C.CategoryId = FA.CategoryId
			LEFT JOIN fms_vendors AS V ON V.VendorId = FA.VendorId
			LEFT JOIN vendors_manufacturers AS M ON M.ManufacturerId = FA.ManufacturerId
			WHERE FA.FixedAssetId = '$iFixedAssetId'");

			if ($objDatabase->RowsNumber($varResult) <= 0) return("");

	    	$iCategoryId = $objDatabase->Result($varResult, 0, "FA.CategoryId");
	    	$iManufacturerId = $objDatabase->Result($varResult, 0, "FA.ManufacturerId");
	    	$iVendorId = $objDatabase->Result($varResult, 0, "FA.VendorId");

	    	$sCategoryName = $objDatabase->Result($varResult, 0, "C.CategoryName");
	    	$sCategoryName = $sCategoryName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sCategoryName) . '\', \'../vendors/fixedassets.php?pagetype=categories_details&id=' . $iCategoryId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Category Information" title="Category Information" border="0" /></a>';

		    $sManufacturerName = $objDatabase->Result($varResult, 0, "M.ManufacturerName");
	    	$sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");

	        if ($sManufacturerName == '') $sManufacturerName = 'No Manufacturer';
	        else $sManufacturerName = $sManufacturerName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sManufacturerName) . '\', \'../vendors/fixedassets.php?pagetype=manufacturers_details&id=' . $iManufacturerId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Manufacturer Information" title="Manufacturer Information" border="0" /></a>';

	        if ($sVendorName == '') $sVendorName = 'No Vendor';
	        else $sVendorName = $sVendorName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sVendorName) . '\', \'../vendors/vendors.php?pagetype=details&id=' . $iVendorId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" alt="Vendor Information" align="absmiddle" title="Vendor Information" border="0" /></a>';

	        $sFixedAssetName = $objDatabase->Result($varResult, 0, "FA.FixedAssetName");
			$sFixedAssetName = $sFixedAssetName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString($sFixedAssetName) . '\', \'../fixedassets/fixedassets?pagetype=details&id=' . $iFixedAssetId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" align="absmiddle" alt="FixedAsset Information" title="FixedAsset Information" border="0" /></a>';
    	}

		$sReturn = '
    	<tr class="GridTR"><td colspan="2"><span class="Details_Title">FixedAsset Information:</span></td></tr>
		<tr bgcolor="#edeff1"><td width="25%">FixedAsset Name:</td><td><strong>' . $sFixedAssetName . '</strong></td></tr>
		<tr><td>Category Name:</td><td><strong>' . $sCategoryName . '</strong></td></tr>
		<tr bgcolor="#edeff1"><td>Manufacturer Name:</td><td><strong>' . $sManufacturerName . '</strong></td></tr>
		<tr><td>Vendor Name:</td><td><strong>' . $sVendorName . '</strong></td></tr>
		' . $sJavaScript;
		
		return($sReturn);
    }
}



/* Category Class */
class clsVendors_FixedAssets_Categories
{
    // Constructor
    function __construct()
    {
    }
	
	function ShowAllCategories($sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
				
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');			
			
        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

        $sTitle = "Categories";
        if ($sSortBy == "") $sSortBy = "C.CategoryId DESC";

        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((C.CategoryId LIKE '%$sSearch%') OR (C.CategoryName LIKE '%$sSearch%') OR (C.CategoryCode LIKE '%$sSearch%') OR (C2.CategoryName LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }
				
		$iTotalRecords = $objDatabase->DBCount("fms_vendors_fixedassets_categories AS C LEFT JOIN fms_vendors_fixedassets_categories AS C2 ON C2.CategoryId = C.ParentCategoryId INNER JOIN organization_employees AS E ON E.EmployeeId = C.CategoryAddedBy", "C.OrganizationId='" . cOrganizationId . "' $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=categories&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT *,C.CategoryId AS MainCategoryId  FROM fms_vendors_fixedassets_categories AS C
		LEFT JOIN fms_vendors_fixedassets_categories AS C2 ON C2.CategoryId = C.ParentCategoryId
		INNER JOIN organization_employees AS E ON E.EmployeeId = C.CategoryAddedBy
        WHERE C.OrganizationId='" . cOrganizationId . "' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">		 
		 <td width="30%" align="left"><span class="WhiteHeading">Category Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CategoryName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Category Name in Ascending Order" title="Sort by Category Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CategoryName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Category Name in Descending Order" title="Sort by Category Name in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Category Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C.CategoryCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Category Code in Ascending Order" title="Sort by Category Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C.CategoryCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Category Code in Descending Order" title="Sort by Category Code in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Parent Category Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=C2.CategoryName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Parent Category Name in Ascending Order" title="Sort by Parent Category Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=C2.CategoryName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Parent Category Name in Descending Order" title="Sort by Parent Category Name in Descending Order" border="0" /></a></span></td>
		 <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iCategoryId = $objDatabase->Result($varResult, $i, "MainCategoryId");
			$sCategoryName = $objDatabase->Result($varResult, $i, "C.CategoryName");
			$sCategoryCode = $objDatabase->Result($varResult, $i, "C.CategoryCode");
			$sCategoryParentName = $objDatabase->Result($varResult, $i, "C2.CategoryName");
            $sCategoryDescription = $objDatabase->Result($varResult, $i, "C.CategoryDescription");

            if ($sCategoryParentName == '') $sCategoryParentName = "No Parent";

			$sEditCategory = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sCategoryCode)) . '\', \'../vendors/fixedassets.php?pagetype=categories_details&action2=edit&id=' . $iCategoryId. '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Category Details" title="Edit this Category Details"></a></td>';
            $sDeleteCategory = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Category?\')) {window.location = \'?action=DeleteCategory&id=' . $iCategoryId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Category" title="Delete this Category"></a></td>';
			
			
       		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[2] == 0)	$sEditCategory = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[3] == 0)	$sDeleteCategory = '<td class="GridTD">&nbsp;</td>';			

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td align="left" valign="top">' . $sCategoryName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sCategoryCode . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sCategoryParentName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'' . $objDatabase->RealEscapeString($sCategoryName) . '\',  \'../vendors/fixedassets.php?pagetype=categories_details&id=' . $iCategoryId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Category Details" title="View this Category Details"></a></td>
			 ' . $sEditCategory . '
             ' . $sDeleteCategory . '
            </tr>';
		}

		$sAddNewCategory = '<td width="10%" align="left"><input onclick="window.top.CreateTab(\'tabContainer\', \'Add Category\', \'../vendors/fixedassets.php?pagetype=categories_details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Category"></td>';
		
        if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[1] == 0) // Add Disabled
			$sAddNewCategory = '';
		

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  ' . $sAddNewCategory . '
          <form method="GET" action=""><td align="left" colspan="2">Search for a Category:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Category" title="Search for Category" border="0"><input type="hidden" name="pagetype" id="pagetype" value="categories" /></td></form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Category Details" alt="View this Category Details"></td><td>View this Category Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Category Details" alt="Edit this Category Details"></td><td>Edit this Category Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Category" alt="Delete this Category"></td><td>Delete this Category</td></tr>
        </table>';

		return($sReturn);
    }

    function CategoryDetails($iCategoryId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_vendors_fixedassets_categories AS C
		LEFT JOIN fms_vendors_fixedassets_categories AS C2 ON C2.CategoryId = C.ParentCategoryId
		INNER JOIN organization_employees AS E ON E.EmployeeId = C.CategoryAddedBy
		WHERE C.OrganizationId='" . cOrganizationId . "' AND C.CategoryId = '$iCategoryId'");
		
		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=categories_details&action2=edit&id=' . $iCategoryId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Category" title="Edit this Category" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Category?\')) {window.location = \'?pagetype=categories_details&action=DeleteCategory&id=' . $iCategoryId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Category" title="Delete this Category" /></a>';
		
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[3] == 0)	$sButtons_Delete = '';
		

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Category Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iCategoryId = $objDatabase->Result($varResult, 0, "C.CategoryId");
		        $sCategoryName = $objDatabase->Result($varResult, 0, "C.CategoryName");
		        $sCategoryCode = $objDatabase->Result($varResult, 0, "C.CategoryCode");
		        $iCategoryParentId = $objDatabase->Result($varResult, 0, "C.ParentCategoryId");
		        $sCategoryParentName = $objDatabase->Result($varResult, 0, "C2.CategoryName");
				$iCategoryAddedBy = $objDatabase->Result($varResult, 0, "C.CategoryAddedBy");
				$sCategoryAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sCategoryAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \''. $objDatabase->RealEscapeString(str_replace('"', '', $sCategoryAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iCategoryAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
		        $sNotes = $objDatabase->Result($varResult, 0, "C.Notes");

		        if ($sCategoryParentName == '') $sParentStation = "No Parent";
				else $sCategoryParentName  = $sCategoryParentName  . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sCategoryParentName  . '\', \'../vendors/fixedassets.php?pagetype=categories_details&id=' . $iCategoryParentId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Category Information" title="Parent Category Information" border="0" /></a>';

		        $sDescription = $objDatabase->Result($varResult, 0, "C.CategoryDescription");
		        if($sCategoryParentName=='') $sCategoryParentName="No Parent";

				$dCategoryAddedOn = $objDatabase->Result($varResult, 0, "C.CategoryAddedOn");
				$sCategoryAddedOn = date("F j, Y", strtotime($dCategoryAddedOn)) . ' at ' . date("g:i a", strtotime($dCategoryAddedOn));
		    }

			if ($sAction2 == "edit")
			{
				 $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
			
				$sCategoryParentName = '<select class="form1" name="selCategoryParentId" id="selCategoryParentId">
		        <option value="0">No Parent</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_categories AS C WHERE C.OrganizationId='" . cOrganizationId . "' ORDER BY C.CategoryName");

		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iCategoryParentId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		               $sCategoryParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' .  $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		            else if ($iCategoryId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		            	$sCategoryParentName .= '';
		            else
		               $sCategoryParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' . $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		        }
		        $sCategoryParentName .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selCategoryParentId\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selCategoryParentId\'), \'../vendors/fixedassets.php?pagetype=categories_details&id=\'+GetSelectedListBox(\'selCategoryParentId\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Category Information" title="Parent Category Information" border="0" /></a>';
				
				$sCategoryName = '<input type="text" name="txtCategoryName" id="txtCategoryName" class="form1" value="' . $sCategoryName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sCategoryCode = '<input type="text" name="txtCategoryCode" id="txtCategoryCode" class="form1" value="' . $sCategoryCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				
			  	$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$iCategoryId = '';
				$sCategoryImageName = '';
				$sCategoryCode = '';
				
				$sReturn .= '<form method="post" action="" enctype="multipart/form-data" onsubmit="return ValidateForm();">';
				
				$sCategoryAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sCategoryAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \''. $objDatabase->RealEscapeString(str_replace('"', '', $sCategoryAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
		        $sCategoryName = $objGeneral->fnGet("txtCategoryName");
		        $sCategoryCode = $objGeneral->fnGet("txtCategoryCode");
		        $sDescription = $objGeneral->fnGet("txtDescription");
		        $sNotes = $objGeneral->fnGet("txtNotes");

		        $sCategoryParentName = '<select class="form1" name="selCategoryParentId" id="selCategoryParentId">
		        <option value="0">No Parent</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_categories AS C WHERE C.OrganizationId='" . cOrganizationId . "' ORDER BY C.CategoryName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {
		            if ($iCategoryParentId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		               $sCategoryParentName .= '<option selected="true" value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' .  $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		            else if ($iCategoryId == $objDatabase->Result($varResult2, $i, "C.CategoryId"))
		            	$sCategoryParentName .= '';
		            else
		               $sCategoryParentName .= '<option value="' . $objDatabase->Result($varResult2, $i, "C.CategoryId") . '">' . $objDatabase->Result($varResult2, $i, "C.CategoryName") . '</option>';
		        }
		        $sCategoryParentName .= '</select>&nbsp;<a href="#noanchor" onclick="if (GetSelectedListBox(\'selCategoryParentId\') > 0) window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selCategoryParentId\'), \'../vendors/fixedassets.php?pagetype=categories_details&?id=\'+GetSelectedListBox(\'selCategoryParentId\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Parent Category Information" title="Parent Category Information" border="0" /></a>';

                $sCategoryName = '<input type="text" name="txtCategoryName" id="txtCategoryName" class="form1" value="' . $sCategoryName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sCategoryCode = '<input type="text" name="txtCategoryCode" id="txtCategoryCode" class="form1" value="' . $sCategoryCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
			  	$sCategoryAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			  	$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
	        function ValidateForm()
	        {
	        	if (GetVal(\'txtCategoryName\') == "") return(AlertFocus(\'Please enter a valid Category Name\', \'txtCategoryName\'));
	        	if (GetVal(\'txtCategoryCode\') == "") return(AlertFocus(\'Please enter a valid Category Code\', \'txtCategoryCode\'));
				
	         	return true;
	        }
	        </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="GridTR"><td colspan="2"><span class="Details_Title">Category Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td style="width:200px;" valign="top">Category Name:</td><td><strong>' . $sCategoryName . '</strong></td></tr>
			 <tr><td>Category Code:</td><td><strong>' . $sCategoryCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Parent Category:</td><td><strong>' . $sCategoryParentName . '</strong></td></tr>
		     <tr bgcolor="#ffffff"><td valign="top" colspan="2">Description:</td></tr>
			 <tr><td valign="top" colspan="4"><strong>' . $sDescription . '</strong></td></tr>
		     <tr class="GridTR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
		     <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
		     <tr bgcolor="#ffffff"><td>Category Added On:</td><td><strong>' . $sCategoryAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Category Added By:</td><td><strong>' . $sCategoryAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Category" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iCategoryId . '"><input type="hidden" name="action" id="action" value="UpdateCategory"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
	  		else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Category" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewCategory"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

	function AddNewCategory($sCategoryName, $sCategoryCode, $sDescription, $sNotes, $iParentCategoryId)
    {
        global $objDatabase;
        global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
				
		// Employee Roles
   		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[1] == 0) // Add Disabled
			return(1010);		
		
        $varNow = $objGeneral->fnNow();
		$iCategoryAddedBy = $objEmployee->iEmployeeId;
		
		$sCategoryCode = $objDatabase->RealEscapeString($sCategoryCode);
		
		if($objDatabase->DBCount("fms_vendors_fixedassets_categories AS C", "C.OrganizationId='" . cOrganizationId . "' AND C.CategoryCode='$sCategoryCode'") > 0)
			return(5008);
				
        $sCategoryName = $objDatabase->RealEscapeString($sCategoryName);
        $sDescription = $objDatabase->RealEscapeString($sDescription);
        $sNotes = $objDatabase->RealEscapeString($sNotes);

        $varResult = $objDatabase->Query("INSERT INTO fms_vendors_fixedassets_categories
        (OrganizationId, CategoryName, CategoryCode, CategoryDescription, Notes, ParentCategoryId, CategoryAddedOn, CategoryAddedBy)
        VALUES ('" . cOrganizationId . "', '$sCategoryName', '$sCategoryCode', '$sDescription', '$sNotes', '$iParentCategoryId', '$varNow', '$iCategoryAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_categories AS C WHERE C.OrganizationId='" . cOrganizationId . "' AND C.CategoryName='$sCategoryName' AND C.CategoryCode='$sCategoryCode' AND C.CategoryAddedOn='$varNow'");

        if ($objDatabase->RowsNumber($varResult) > 0)
        {
        	$iCategoryId = $objDatabase->Result($varResult, $i, "C.CategoryId");
        	
  			$objSystemLog->AddLog("Add New Category - " . $sCategoryName);

            $objGeneral->fnRedirect('?pagetype=categories_details&error=5001&id=' . $objDatabase->Result($varResult, 0, "C.CategoryId"));
        }
       	else
       		return(5002);
    }
		
    function UpdateCategory($iCategoryId, $sCategoryName, $sCategoryCode, $sDescription, $sNotes, $iParentCategoryId)
    {
        global $objDatabase;
        global $objEmployee;
        global $objGeneral;
		global $objSystemLog;
		
        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[2] == 0) // Update Disabled
			return(1010);			
		
		$sCategoryCode = $objDatabase->RealEscapeString($sCategoryCode);
		$sCategoryName = $objDatabase->RealEscapeString($sCategoryName);        
        $sDescription = $objDatabase->RealEscapeString($sDescription);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		
		
		if($objDatabase->DBCount("fms_vendors_fixedassets_categories AS C", "C.OrganizationId='" . cOrganizationId . "' AND C.CategoryId <> '$iCategoryId' AND C.CategoryCode='$sCategoryCode'") > 0)
			return(5008);
			
        $varResult = $objDatabase->Query("
	    UPDATE fms_vendors_fixedassets_categories
	    SET
        	CategoryName='$sCategoryName',
            CategoryCode='$sCategoryCode',
            CategoryDescription='$sDescription',
            Notes='$sNotes',
            ParentCategoryId='$iParentCategoryId'
    	WHERE OrganizationId='" . cOrganizationId . "' AND CategoryId='$iCategoryId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Update Category - " . $sCategoryName);
            $objGeneral->fnRedirect('../vendors/fixedassets.php?pagetype=categories_details&?error=5003&id=' . $iCategoryId);
        }
        else
            return(5004);
    }

    function DeleteCategory($iCategoryId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Categories[3] == 0) // Delete Disabled
	    {
			if ($objGeneral->fnGet("pagetype") == "categories_details")
				$objGeneral->fnRedirect('?pagetype=categories_details&id=' . $iCategoryId . '&error=1010');
			else
				$objGeneral->fnRedirect('?pagetype=categories&id=0&error=1010');
		}
					
		if ($objDatabase->DBCount("fms_vendors_fixedassets AS FA", "FA.OrganizationId='" . cOrganizationId . "' AND FA.CategoryId='$iCategoryId'") > 0) return(5007);

       	$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_categories AS C WHERE C.OrganizationId='" . cOrganizationId . "' AND C.CategoryId='$iCategoryId'");

       	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Category Id');
		$sCategoryName = $objDatabase->Result($varResult, 0, "C.CategoryName");

        $varResult = $objDatabase->Query("DELETE FROM fms_vendors_fixedassets_categories WHERE CategoryId='$iCategoryId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Delete Category - " . $sCategoryName);
			
			if ($objGeneral->fnGet("pagetype") == "categories_details")
				$objGeneral->fnRedirect('?pagetype=categories_details&id=' . $iCategoryId . '&error=5005');
			else
				$objGeneral->fnRedirect('?pagetype=categories&id=0&error=5005');
        }
       else
           return(5006);
    }
}

?>