<?php

/* Manufacturer Class */
class clsManufacturers
{
    // Constructor
    function __construct()
    {
    }


    function UpdateManufacturer($iManufacturerId, $sManufacturerName, $sManufacturerCode, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sNotes, $sDescription)
    {
        global $objDatabase;
        global $objEmployee;
        global $objGeneral;
		global $objSystemLog;

        // Employee Roles
	  	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[2] == 0) // Update Disabled
			return(1010);


        $sManufacturerName = $objDatabase->RealEscapeString($sManufacturerName);
        $sManufacturerCode = $objDatabase->RealEscapeString($sManufacturerCode);

	  	// Manufacturer Code already added in the System
	  	if ($objDatabase->DBCount("fms_vendors_fixedassets_manufacturers AS M", "M.OrganizationId='" . cOrganizationId . "' AND M.ManufacturerId <> '$iManufacturerId' AND M.ManufacturerCode='$sManufacturerCode'") > 0) return(5107);

        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
        $sCountry = $objDatabase->RealEscapeString($sCountry);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		$sPhoneNumber = $objDatabase->RealEscapeString($sPhoneNumber);

        $sDescription = $objDatabase->RealEscapeString($sDescription);

        $varResult = $objDatabase->Query("
        UPDATE fms_vendors_fixedassets_manufacturers SET
            ManufacturerName='$sManufacturerName',
            ManufacturerCode='$sManufacturerCode',
            Address='$sAddress',
            City='$sCity',
            State='$sState',
            ZipCode='$sZipCode',
            Country='$sCountry',
            PhoneNumber='$sPhoneNumber',
            Notes='$sNotes',
            ManufacturerDescription='$sDescription'
        WHERE OrganizationId='" . cOrganizationId . "' AND ManufacturerId='$iManufacturerId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Update Manufacturer - " . $sManufacturerName);

  			$objGeneral->fnRedirect('../vendors/fixedassets.php?pagetype=manufacturers_details&error=5103&id=' . $iManufacturerId);
        }
        else
            return(5104);
    }

    function DeleteManufacturer($iManufacturerId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;

        // Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[3] == 0) // Delete Disabled
	    {
			if ($objGeneral->fnGet("pagetype") == "manufacturers_details")
				$objGeneral->fnRedirect('?pagetype=manufacturers_details&id=' . $iManufacturerId . '&error=1010');
			else
				$objGeneral->fnRedirect('?pagetype=manufacturers&id=0&error=1010');
		}			

		if ($objDatabase->DBCount("fms_vendors_fixedassets AS P", "P.OrganizationId='" . cOrganizationId . "' AND P.ManufacturerId='$iManufacturerId'") > 0) return(5108);

       	$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_manufacturers AS M WHERE M.OrganizationId='" . cOrganizationId . "' AND M.ManufacturerId='$iManufacturerId'");

       	if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Manufacturer Id');
		$sManufacturerName = $objDatabase->Result($varResult, 0, "M.ManufacturerName");
		$sManufacturerName = $objDatabase->RealEscapeString($sManufacturerName);
		$sManufacturerName = stripslashes($sManufacturerName);

        $varResult = $objDatabase->Query("DELETE FROM fms_vendors_fixedassets_manufacturers WHERE ManufacturerId='$iManufacturerId'");

        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Delete Manufacturer - " . $sManufacturerName);
			if ($objGeneral->fnGet("pagetype") == "manufacturers_details")
				$objGeneral->fnRedirect('?pagetype=manufacturers_details&id=' . $iManufacturerId . '&error=5105');
			else
				$objGeneral->fnRedirect('?pagetype=manufacturers&id=0&error=5105');			    
        }
        else
            return(5106);
    }

    function AddNewManufacturer($sManufacturerName, $sManufacturerCode, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sNotes, $sDescription)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;

        $varNow = $objGeneral->fnNow();
		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[1] == 0) // Add Disabled
			return(1010);

		$iManufacturerAddedBy = $objEmployee->iEmployeeId;
        $sManufacturerName = $objDatabase->RealEscapeString($sManufacturerName);
        $sManufacturerCode = $objDatabase->RealEscapeString($sManufacturerCode);

	    // Manufacturer Code already added in the System
	  	if ($objDatabase->DBCount("fms_vendors_fixedassets_manufacturers AS M", "M.OrganizationId='" . cOrganizationId . "' AND M.ManufacturerCode = '$sManufacturerCode'") > 0) return(5107);

        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
        $sCountry = $objDatabase->RealEscapeString($sCountry);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		$sPhoneNumber = $objDatabase->RealEscapeString($sPhoneNumber);

        $sDescription = $objDatabase->RealEscapeString($sDescription);

        $varResult = $objDatabase->Query("INSERT INTO fms_vendors_fixedassets_manufacturers
        (OrganizationId, ManufacturerName, ManufacturerCode, Address, City, State, ZipCode, Country, PhoneNumber, Notes, ManufacturerDescription, ManufacturerAddedOn, ManufacturerAddedBy)
        VALUES
        ('" . cOrganizationId . "', '$sManufacturerName', '$sManufacturerCode', '$sAddress', '$sCity', '$sState', '$sZipCode', '$sCountry', '$sPhoneNumber', '$sNotes', '$sDescription', '$varNow', '$iManufacturerAddedBy')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_vendors_fixedassets_manufacturers AS M WHERE M.OrganizationId='" . cOrganizationId . "' AND M.ManufacturerName='$sManufacturerName' AND M.ManufacturerAddedOn='$varNow'");

        if ($objDatabase->RowsNumber($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Add New Manufacturer - " . $sManufacturerName);						
            $objGeneral->fnRedirect('../vendors/fixedassets.php?pagetype=manufacturers_details&error=5101&id=' . $objDatabase->Result($varResult, 0, "M.ManufacturerId"));
        }
        else
            return(5102);
    }

    function ShowAllManufacturers($sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = "Manufacturers";
        if ($sSortBy == "") $sSortBy = "M.ManufacturerId DESC";

        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((M.ManufacturerId LIKE '%$sSearch%') OR (M.ManufacturerName LIKE '%$sSearch%') OR (M.ManufacturerCode LIKE '%$sSearch%') OR (M.Country LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

		$iTotalRecords = $objDatabase->DBCount("fms_vendors_fixedassets_manufacturers AS M INNER JOIN organization_employees AS E ON E.EmployeeId = M.ManufacturerAddedBy", "M.OrganizationId='" . cOrganizationId . "' $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=manufacturers&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors_fixedassets_manufacturers AS M
		INNER JOIN organization_employees AS E ON E.EmployeeId = M.ManufacturerAddedBy
        WHERE M.OrganizationId='" . cOrganizationId . "' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">		 
		 <td width="30%" align="left"><span class="WhiteHeading">Manufacturer Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=M.ManufacturerName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Manufacturer Name in Ascending Order" title="Sort by Manufacturer Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=M.ManufacturerName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Manufacturer Name in Descending Order" title="Sort by Manufacturer Name in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Manufacturer Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=M.ManufacturerCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Manufacturer Code in Ascending Order" title="Sort by Manufacturer Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=M.ManufacturerCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Manufacturer Code in Descending Order" title="Sort by Manufacturer Code in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Phone Number&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=M.PhoneNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Manufacturer Phone Number in Ascending Order" title="Sort by Manufacturer Phone Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=M.PhoneNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Manufacturer Phone Number in Descending Order" title="Sort by Manufacturer Phone Number in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Added By&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=E.FirstName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Manufacturer Added By in Ascending Order" title="Sort by Manufacturer Added By in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=E.FirstName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Manufacturer Added By in Descending Order" title="Sort by Manufacturer Added By in Descending Order" border="0" /></a></span></td>
		 <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$sManufacturerName = $objDatabase->Result($varResult, $i, "M.ManufacturerName");
            $sManufacturerName = htmlspecialchars($sManufacturerName);
            $sManufacturerName = $objDatabase->RealEscapeString($sManufacturerName);
            $sManufacturerName = stripslashes($sManufacturerName);

			$iManufacturerId = $objDatabase->Result($varResult, $i, "M.ManufacturerId");

			$iManufacturerCode = $objDatabase->Result($varResult, $i, "M.ManufacturerCode");

            $sPhoneNumber = $objDatabase->Result($varResult, $i, "M.PhoneNumber");
			$iManufacturerAddedBy = $objDatabase->Result($varResult, $i, "M.ManufacturerAddedBy");
			$sManufacturerAddedBy = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");

            $sEditManufacturer = '<td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update '. $objDatabase->RealEscapeString(stripcslashes($sManufacturerName)) . '\', \'../vendors/fixedassets.php?pagetype=manufacturers_details&action2=edit&id=' . $iManufacturerId. '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit Manufacturer Details" title="Edit this Manufacturer Details"></a></td>';
            $sDeleteManufacturer = '<td align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Manufacturer?\')) {window.location = \'?action=DeleteManufacturer&id=' . $iManufacturerId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Manufacturer" title="Delete this Manufacturer"></a></td>';

       		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[2] == 0)	$sEditManufacturer = '<td class="GridTd">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[3] == 0)	$sDeleteManufacturer = '<td class="GridTd">&nbsp;</td>';
			
            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td align="left" valign="top">' . $sManufacturerName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
             <td align="left" valign="top">' . $iManufacturerCode  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sPhoneNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sManufacturerAddedBy . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'' . $objDatabase->RealEscapeString(stripslashes($sManufacturerName)) . '\',  \'../vendors/fixedassets.php?pagetype=manufacturers_details&id=' . $iManufacturerId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Manufacturer Details" title="View this Manufacturer Details"></a></td>
			 ' . $sEditManufacturer . '
             ' . $sDeleteManufacturer . '
            </tr>';
		}

		$sAddNewManufacturer = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Manufacturer\', \'../vendors/fixedassets.php?pagetype=manufacturers_details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Manufacturer">';

       	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[1] == 0) // Add Disabled
			$sAddNewManufacturer = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddNewManufacturer . '
           <form method="GET" action="">&nbsp;&nbsp;&nbsp;Search for a Manufacturer:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" align="absmiddle" alt="Search for a Manufacturer" title="Search for Manufacturer" border="0"><input type="hidden" name="pagetype" id="pagetype" value="manufacturers" /></form>
          </td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Manufacturer Details" alt="View this Manufacturer Details"></td><td>View this Manufacturer Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Manufacturer Details" alt="Edit this Manufacturer Details"></td><td>Edit this Manufacturer Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Manufacturer" alt="Delete this Manufacturer"></td><td>Delete this Manufacturer</td></tr>
        </table>';

		return($sReturn);
    }

    function ManufacturerDetails($iManufacturerId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;

		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_vendors_fixedassets_manufacturers AS M
		INNER JOIN organization_employees AS E ON E.EmployeeId = M.ManufacturerAddedBy
		WHERE M.OrganizationId='" . cOrganizationId . "' AND M.ManufacturerId = '$iManufacturerId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=manufacturers_details&action2=edit&id=' . $iManufacturerId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Manufacturer" title="Edit this Manufacturer" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Manufacturer?\')) {window.location = \'?pagetype=manufacturers_details&action=DeleteManufacturer&id=' . $iManufacturerId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Manufacturer" title="Delete this Manufacturer" /></a>';

    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[2] == 0)
   			$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_FixedAssets_Manufacturers[3] == 0)
			$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Manufacturer Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iManufacturerId = $objDatabase->Result($varResult, 0, "M.ManufacturerId");
		        $sManufacturerName = $objDatabase->Result($varResult, 0, "M.ManufacturerName");
                $sManufacturerName = htmlspecialchars($sManufacturerName);
                $sManufacturerName = $objDatabase->RealEscapeString($sManufacturerName);
                $sManufacturerName = stripslashes($sManufacturerName);

		        // Manufacturer Address
		        $sAddress = $objDatabase->Result($varResult, 0, "M.Address");
                $sAddress = htmlspecialchars($sAddress);
                $sAddress = $objDatabase->RealEscapeString($sAddress);
                $sAddress = stripslashes($sAddress);

		        $sCity = $objDatabase->Result($varResult, 0, "M.City");
                $sCity = htmlspecialchars($sCity);
                $sCity = $objDatabase->RealEscapeString($sCity);
                $sCity = stripslashes($sCity);

		        $sState = $objDatabase->Result($varResult, 0, "M.State");
                $sState = htmlspecialchars($sState);
                $sState = $objDatabase->RealEscapeString($sState);
                $sState = stripslashes($sState);

		        $sZipCode = $objDatabase->Result($varResult, 0, "M.ZipCode");

		        $sCountry = $objDatabase->Result($varResult, 0, "M.Country");
                $sCountry = htmlspecialchars($sCountry);
                $sCountry = $objDatabase->RealEscapeString($sCountry);
                $sCountry = stripslashes($sCountry);

				$sPhoneNumber = $objDatabase->Result($varResult, 0, "M.PhoneNumber");
				//$sEmailAddress = $objDatabase->Result($varResult, 0, "M.EmailAddress");

				$sManufacturerCode = $objDatabase->Result($varResult, 0, "M.ManufacturerCode");
				$sNotes = $objDatabase->Result($varResult, 0, "M.Notes");
		        $sDescription = $objDatabase->Result($varResult, 0, "M.ManufacturerDescription");

				$iManufacturerAddedBy = $objDatabase->Result($varResult, 0, "M.ManufacturerId");
				$sManufacturerAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
                $sManufacturerAddedBy = htmlspecialchars($sManufacturerAddedBy);
                $sManufacturerAddedBy = $objDatabase->RealEscapeString($sManufacturerAddedBy);
                $sManufacturerAddedBy = stripslashes($sManufacturerAddedBy);

				$sManufacturerAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \''. $objDatabase->RealEscapeString(str_replace('"', '', $sManufacturerAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iManufacturerAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

				$dManufacturerAddedOn = $objDatabase->Result($varResult, 0, "M.ManufacturerAddedOn");
				$sManufacturerAddedOn = date("F j, Y", strtotime($dManufacturerAddedOn)) . ' at ' . date("g:i a", strtotime($dManufacturerAddedOn));
		    }

			if ($sAction2 == "edit")
			{
				$sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';

				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';

				$sManufacturerName = '<input type="text" name="txtManufacturerName" id="txtManufacturerName" class="form1" value="' . $sManufacturerName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sManufacturerCode = '<input type="text" name="txtManufacturerCode" id="txtManufacturerCode" class="form1" value="' . $sManufacturerCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />';
				$sCountry = $sCountryString;
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				//$sDescription = '<textarea name="txtDescription" id="txtDescription" class="form1" rows="5" cols="60">' . $sDescription . '</textarea>';

			}
			else if ($sAction2 == "addnew")
			{
				$iManufacturerId = "";
		        $sManufacturerName = $objGeneral->fnGet("txtManufacturerName");
		        $sAddress = $objGeneral->fnGet("txtAddress");
		        $sCity = $objGeneral->fnGet("txtCity");
		        $sZipCode = $objGeneral->fnGet("txtZipCode");
		        $sState = $objGeneral->fnGet("txtState");
		        $sCountry = $objGeneral->fnGet("txtCountry");
		        $sManufacturerCode = $objGeneral->fnGet("txtManufacturerCode");
		        $sNotes = $objGeneral->fnGet("txtNotes");

				$sManufacturerAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sManufacturerAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \''. $objDatabase->RealEscapeString(str_replace('"', '', $sManufacturerAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';	

		        $sPhoneNumber = $objGeneral->fnGet("txtPhoneNumber");
		        //  $sEmailAddress = $objGeneral->fnGet("txtEmailAddress");

		        $sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';

		        $sDescription = $objGeneral->fnGet("txtDescription");

                $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
		        
                $sManufacturerName = '<input type="text" name="txtManufacturerName" id="txtManufacturerName" class="form1" value="' . $sManufacturerName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sManufacturerCode = '<input type="text" name="txtManufacturerCode" id="txtManufacturerCode" class="form1" value="' . $sManufacturerCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';

				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");				
				$sCountry = $sCountryString;
			   	$sManufacturerAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
		    function ValidateForm()
		    {
				if (GetVal(\'txtManufacturerName\') == "") return(AlertFocus(\'Please enter a valid Manufacturer Name\', \'txtManufacturerName\'));
				if (GetVal(\'txtManufacturerCode\') == "") return(AlertFocus(\'Please enter a valid Manufacturer Code\', \'txtManufacturerCode\'));

				return true;
		    }
		    </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="GridTR"><td colspan="2"><span class="Details_Title">Manufacturer Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td style="width:200px;" valign="top">Manufacturer Name:</td><td><strong>' . $sManufacturerName . '</strong></td></tr>
			 <tr><td>Manufacturer Code:</td><td><strong>' . $sManufacturerCode . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" colspan="2">Description:</td><tr>
			 <tr><td valign="top" colspan="4"><strong>' . $sDescription . '</strong></td></tr>
		     <tr class="GridTR"><td colspan="2"><span class="Details_Title">Manufacturer Address:</span></td></tr>
			 <tr><td>Address:</td><td><strong>' . $sAddress . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>City:</td><td><strong>' . $sCity . '</strong></td></tr>
			 <tr><td>State/Province:</td><td><strong>' . $sState . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Zip Code:</td><td><strong>' . $sZipCode . '</strong></td></tr>
			 <tr><td>Country:</td><td><strong>' . $sCountry . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Phone Number:</td><td><strong>' . $sPhoneNumber . '</strong></td></tr>
			 <tr class="GridTR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
		     <tr><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
		     <tr bgcolor="#edeff1"><td>Manufacturer Added On:</td><td><strong>' . $sManufacturerAddedOn . '</strong></td></tr>
			 <tr><td valign="top">Manufacturer Added By:</td><td><strong>' . $sManufacturerAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iManufacturerId . '"><input type="hidden" name="action" id="action" value="UpdateManufacturer"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Manufacturer" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewManufacturer"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';

		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }

}

?>