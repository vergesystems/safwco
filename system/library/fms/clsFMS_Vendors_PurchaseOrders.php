<?php

// Class: Purchase Order
class clsVendors_PurchaseOrders extends clsVendors
{
	// Class Constructor
	function __construct()
	{
	}
	
	function ShowAllPurchaseOrders($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Purchase Orders";
		if ($sSortBy == "") $sSortBy = "PO.PurchaseOrderId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((PO.PurchaseOrderId LIKE '%$sSearch%') OR (PO.PurchaseOrderNumber LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (Q.QuotationNumber LIKE '%$sSearch%') OR (Q.QuotationTitle LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_vendors_purchaseorders AS PO INNER JOIN fms_vendors_quotations AS Q ON Q.QuotationId = PO.QuotationId INNER JOIN organization_employees AS E ON E.EmployeeId = PO.PurchaseOrderAddedBy", "PO.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors_purchaseorders AS PO
		INNER JOIN fms_vendors_quotations AS Q ON Q.QuotationId = PO.QuotationId
		INNER JOIN fms_vendors AS V ON V.VendorId=Q.Vendorid
		INNER JOIN organization_employees AS E ON E.EmployeeId = PO.PurchaseOrderAddedBy
		WHERE PO.OrganizationId='" . cOrganizationId . "' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Quotation No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Quotation No in Ascending Order" title="Sort by Quotation No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Quotation No in Descending Order" title="Sort by Quotation No in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Purchase Order No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=PO.PurchaseOrderNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Purchase Order No in Ascending Order" title="Sort by Purchase Order No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=PO.PurchaseOrderNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Description in Purchase Order No Order" title="Sort by Purchase Order No in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Vendor Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Name in Ascending Order" title="Sort by Vendor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Name in Descending Order" title="Sort by Vendor Name in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Total Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.TotalAmount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Total Amount in Ascending Order" title="Sort by Total Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.TotalAmount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Total Amount in Descending Order" title="Sort by Total Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Delivery Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=PO.DeliveryDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Delivery Date in Ascending Order" title="Sort by Delivery Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.DeliveryDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Delivery Date in Descending Order" title="Sort by Delivery Date in Descending Order" border="0" /></a></span></td>
		  <td width="4%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						
			$iPurchaseOrderId = $objDatabase->Result($varResult, $i, "PO.PurchaseOrderId");
			$sQuotationNo = $objDatabase->Result($varResult, $i, "Q.QuotationNumber");
						
			$dTotalAmount = $objDatabase->Result($varResult, $i, "Q.TotalAmount");
			$sTotalAmount = number_format($dTotalAmount, 2);
									
			$iPurchaseOrderAddedBy = $objDatabase->Result($varResult, $i, "PO.PurchaseOrderAddedBy");
			$sPurchaseOrderAddedBy = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
						
			$sPurchaseOrderNumber = $objDatabase->Result($varResult, $i, "PO.PurchaseOrderNumber");
			$sPurchaseOrderNumber = $objDatabase->RealEscapeString($sPurchaseOrderNumber);
			$sPurchaseOrderNumber = stripslashes($sPurchaseOrderNumber);
			
			$dPurchaseOrderAddedOn = $objDatabase->Result($varResult, $i, "PO.PurchaseOrderAddedOn");
			$sPurchaseOrderAddedOn = date("F j, Y", strtotime($dPurchaseOrderAddedOn));
			
			$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");
			
			$dDeliveryDate = $objDatabase->Result($varResult, $i, "PO.DeliveryDate");
			$sDeliveryDate = date("F j, Y", strtotime($dDeliveryDate));

			$sEditPurchaseOrder = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sPurchaseOrderNumber)) . '\', \'../vendors/purchaseorders.php?pagetype=details&action2=edit&id=' . $iPurchaseOrderId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Purchase Order Details" title="Edit this Purchase Order Details"></a></td>';
			$sDeletePurchaseOrder = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Purchase Order?\')) {window.location = \'?action=DeletePurchaseOrder&id=' . $iPurchaseOrderId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Purchase Order" title="Delete this Purchase Order"></a></td>';
			$sDocuments = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/documents_show.php?componentname=Vendors_PurchaseOrders&id=' . $iPurchaseOrderId . '\', \'Documents of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sPurchaseOrderNumber)) . '\', \'700 420\');"><img src="../images/icons/save.gif" border="0" alt="Purchase Orders Documents" title="Purchase Orders Documents"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[2] == 0)$sEditPurchaseOrder = '<td class="GridTD">&nbsp;</td>';			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[3] == 0)$sDeletePurchaseOrder = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders_Documents[0] == 0)$sDocuments = '<td class="GridTD">&nbsp;</td>';		
			
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">						  
			 <td class="GridTD" align="left" valign="top">' . $sQuotationNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sPurchaseOrderNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sVendorName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . $sTotalAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sDeliveryDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sPurchaseOrderNumber)) . '\', \'../vendors/purchaseorders.php?pagetype=details&id=' . $iPurchaseOrderId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Purchase Order Details" title="View this Purchase Order Details"></a></td>
			' . $sEditPurchaseOrder . $sDocuments . $sDeletePurchaseOrder . '</tr>';
		}

		$sAddPurchaseOrder = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Purchase Order\', \'../vendors/purchaseorders.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add New">';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[1] == 0) // Add Disabled
			$sAddPurchaseOrder = '';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddPurchaseOrder . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Purchase Order:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Purchase Order" title="Search for a Purchase Order" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Purchase Order Details" alt="View this Purchase Order Details"></td><td>View this Purchase Order Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Purchase Order Details" alt="Edit this Purchase Order Details"></td><td>Edit this Purchase Order Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Purchase Order" alt="Delete this Purchase Order"></td><td>Delete this Purchase Order</td></tr>
		</table>';

		return($sReturn);
	}
	
	function PurchaseOrderDetails($iPurchaseOrderId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();
				
		$varResult = $objDatabase->Query("
		SELECT *,PO.Description AS Description
		FROM fms_vendors_purchaseorders AS PO
		INNER JOIN fms_vendors_quotations AS Q ON Q.QuotationId = PO.QuotationId
		INNER JOIN fms_vendors AS V ON V.VendorId = Q.VendorId
		INNER JOIN organization_employees AS E ON E.EmployeeId = PO.PurchaseOrderAddedBy
		WHERE PO.OrganizationId='" . cOrganizationId . "' AND PO.PurchaseOrderId = '$iPurchaseOrderId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iPurchaseOrderId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this PurchaseOrder" title="Edit this Purchase Order" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Purchase Order?\')) {window.location = \'?pagetype=details&action=DeletePurchaseOrder&id=' . $iPurchaseOrderId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Purchase Order" title="Delete this Purchase Order" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[2] == 0)$sButtons_Edit = '';		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[3] == 0)$sButtons_Delete = '';
			
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Purchase Order Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{$sDescription = $objDatabase->Result($varResult, 0, "Description");
				$iPurchaseOrderId = $objDatabase->Result($varResult, 0, "PO.PurchaseOrderId");
				$iQuotationId = $objDatabase->Result($varResult, 0, "PO.QuotationId");
				
				$iPurchaseOrderAddedBy = $objDatabase->Result($varResult, 0, "PO.PurchaseOrderAddedBy");
				$sPurchaseOrderAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");;
				$sPurchaseOrderAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sPurchaseOrderAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iPurchaseOrderAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sPurchaseOrderNumber = $objDatabase->Result($varResult, 0, "PO.PurchaseOrderNumber");
				$sPurchaseOrderNumber = $objDatabase->RealEscapeString($sPurchaseOrderNumber);
				$sPurchaseOrderNumber = stripslashes($sPurchaseOrderNumber);
				$dDeliveryDate = $objDatabase->Result($varResult, 0, "PO.DeliveryDate");
				$sDeliveryDate = date("F j, Y", strtotime($dDeliveryDate));
				
				$sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");
				$sQuotationNo = $objDatabase->Result($varResult, 0, "Q.QuotationNumber");
				$sQuotationNo = $objDatabase->RealEscapeString($sQuotationNo);
				$sQuotationNo = stripslashes($sQuotationNo);
				$sQuotationNo = $sQuotationNo . ' ' . $sVendorName;
				$sQuotationNo = $sQuotationNo . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sQuotationNo)) . '\', \'../vendors/quotations.php?pagetype=details&quotationid=' . $iQuotationId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Quotation Information" title="Quotation Information" border="0" /></a><br />';				
								
				$sDescription = $objDatabase->Result($varResult, 0, "PO.Description");
				$sNotes = $objDatabase->Result($varResult, 0, "PO.Notes");
				$dPurchaseOrderAddedOn = $objDatabase->Result($varResult, 0, "PO.PurchaseOrderAddedOn");
				$sPurchaseOrderAddedOn = date("F j, Y", strtotime($dPurchaseOrderAddedOn)) . ' at ' . date("g:i a", strtotime($dPurchaseOrderAddedOn));
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sQuotationNo = '<select name="selQuotation" class="form1" id="selQuotation">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_quotations AS Q 
				INNER JOIN fms_vendors AS V ON V.VendorId = Q.VendorId
				WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.Status='2'
				ORDER BY Q.QuotationNumber");
				if($objDatabase->RowsNumber($varResult) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)					
						$sQuotationNo .= '<option ' . (($iQuotationId == $objDatabase->Result($varResult, $i, "Q.QuotationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "Q.QuotationId") . '">' . $objDatabase->Result($varResult, $i, "V.VendorName"). ' - ' . $objDatabase->Result($varResult, $i, "Q.QuotationNumber") . '</option>';
				}				
				$sQuotationNo .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selQuotation\'), \'../vendors/quotations.php?pagetype=details&quotationid=\'+GetSelectedListBox(\'selQuotation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Quotation Details" title="Quotation Details" /></a>';
				
				$sPurchaseOrderNumber = '<input type="text" name="txtPurchaseOrderNumber" id="txtPurchaseOrderNumber" class="form1" size="10" value="' . $sPurchaseOrderNumber . '">';
				$sDeliveryDate = '<input size="12" type="text" id="txtDeliveryDate" name="txtDeliveryDate" class="form1" value="' . $dDeliveryDate . '" />' . $objjQuery->Calendar('txtDeliveryDate') . '&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$sPurchaseOrderAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sPurchaseOrderAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sPurchaseOrderAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$iPurchaseOrderId = "";
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$sPurchaseOrderNumber = $objGeneral->fnGet("txtPurchaseOrderNumber");
				$dDeliveryDate = $objGeneral->fnGet("txtDeliveryDate");
				if($dDeliveryDate == "") $dDeliveryDate = date("Y-m-d", mktime(0, 0, 0, date("m")+1,   date("d"),   date("Y")));
				
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
							
				$sQuotationNo = '<select name="selQuotation" class="form1" id="selQuotation">';
				$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_quotations AS Q 
				INNER JOIN fms_vendors AS V ON V.VendorId = Q.VendorId
				WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.Status='2'
				ORDER BY Q.QuotationNumber");
				if($objDatabase->RowsNumber($varResult) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)					
						$sQuotationNo .= '<option ' . (($iQuotationId == $objDatabase->Result($varResult, $i, "Q.QuotationId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "Q.QuotationId") . '">' . $objDatabase->Result($varResult, $i, "V.VendorName"). ' - ' . $objDatabase->Result($varResult, $i, "Q.QuotationNumber") . '</option>';
				}				
				$sQuotationNo .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selQuotation\'), \'../vendors/quotations.php?pagetype=details&quotationid=\'+GetSelectedListBox(\'selQuotation\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Quotation Details" title="Quotation Details" /></a>';
				$sPurchaseOrderNumber = '<input type="text" name="txtPurchaseOrderNumber" id="txtPurchaseOrderNumber" class="form1" size="10" value="' . $sPurchaseOrderNumber . '">';
				$sDeliveryDate = '<input size="12" type="text" id="txtDeliveryDate" name="txtDeliveryDate" class="form1" value="' . $dDeliveryDate . '" />' . $objjQuery->Calendar('txtDeliveryDate') . '&nbsp;<span style="color:red;">*</span>';
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sPurchaseOrderAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if (GetVal(\'txtPurchaseOrderNumber\') == "") return(AlertFocus(\'Please enter a valid Purchase Order No\', \'txtPurchaseOrderNumber\'));
				if (!isDate(GetVal(\'txtDeliveryDate\'))) return(AlertFocus(\'Please enter a valid value for Delivery Date\', \'txtDeliveryDate\'));
				
				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Purchase Order Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top" style="width:200px;">Purchase Order No:</td><td><strong>' . $sPurchaseOrderNumber . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Quotation No:</td><td><strong>' . $sQuotationNo . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Delivery Date:</td><td><strong>' . $sDeliveryDate . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr><td valign="top" colspan="4"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Purchase Order Added On:</td><td><strong>' . $sPurchaseOrderAddedOn . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Purchase Order Added By:</td><td><strong>' . $sPurchaseOrderAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iPurchaseOrderid . '"><input type="hidden" name="action" id="action" value="UpdatePurchaseOrder"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add New" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewPurchaseOrder"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewPurchaseOrder($iQuotationId, $sPurchaseOrderNumber, $dDeliveryDate, $sDescription, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[1] == 0) // Add Disabled
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		$iPurchaseOrderAddedBy = $objEmployee->iEmployeeId;
		
		$sPurchaseOrderNumber = $objDatabase->RealEscapeString($sPurchaseOrderNumber);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_vendors_purchaseorders AS PO" , "PO.OrganizationId='" . cOrganizationId . "' AND PO.PurchaseOrderNumber = '$sPurchaseOrderNumber'") > 0) return(5306);
		
		$bCheck = $objDatabase->Query("INSERT INTO fms_vendors_purchaseorders
		(OrganizationId, QuotationId, PurchaseOrderNumber, DeliveryDate, Description, Notes, PurchaseOrderAddedOn, PurchaseOrderAddedBy)
		VALUES ('" . cOrganizationId . "', '$iQuotationId', '$sPurchaseOrderNumber', '$dDeliveryDate', '$sDescription', '$sNotes', '$varNow', '$iPurchaseOrderAddedBy')");

		if ($bCheck)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_purchaseorders AS PO WHERE PO.OrganizationId='" . cOrganizationId . "' AND PO.PurchaseOrderAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$sPurchaseOrderNumber = $objDatabase->Result($varResult, 0, "PO.PurchaseOrderNumber");				
				
				$objSystemLog->AddLog("Add New Purchase Order - " . $sPurchaseOrderNumber);
				
				$objGeneral->fnRedirect('../vendors/purchaseorders.php?pagetype=details&error=5300&id=' . $objDatabase->Result($varResult, 0, "PO.PurchaseOrderId"));
			}
		}

		return(5301);
	}
	
	function UpdatePurchaseOrder ($iPurchaseOrderId, $iQuotationId, $sPurchaseOrderNumber, $dDeliveryDate, $sDescription, $sNotes)
	{
		global $objDatabase;		
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[2] == 0) // Edit Disabled
			return(1010);

		$sPurchaseOrderNumber = $objDatabase->RealEscapeString($sPurchaseOrderNumber);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_vendors_purchaseorders AS PO" , "PO.OrganizationId='" . cOrganizationId . "' AND PO.PurchaseOrderNumber = '$sPurchaseOrderNumber' AND PO.PurchaseOrderId <> '$iPurchaseOrderId'") > 0) return(5306);
		
		$varResult = $objDatabase->Query("
		UPDATE fms_vendors_purchaseorders 
		SET	
			QuotationId='$iQuotationId',
			PurchaseOrderNumber='$sPurchaseOrderNumber',
			DeliveryDate='$dDeliveryDate',
			Description='$sDescription', 
			Notes='$sNotes'
		WHERE PurchaseOrderId='$iPurchaseOrderId'");
		
		$objSystemLog->AddLog("Update Purchase Order - " . $sPurchaseOrderNumber);
		$objGeneral->fnRedirect('../vendors/purchaseorders.php?pagetype=details&error=5302&id=' . $iPurchaseOrderId);			
	
	}
	
	function DeletePurchaseOrder($iPurchaseOrderId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseOrders[3] == 0) // Delete Disabled
		{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iPurchaseOrderId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}		

		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_purchaseorders AS PO WHERE PO.OrganizationId='" . cOrganizationId . "' AND PO.PurchaseOrderId='$iPurchaseOrderId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(5305);	
		$sPurchaseOrderNumber = $objDatabase->Result($varResult, 0, "PO.PurchaseOrderNumber");
		
		$varResult = $objDatabase->Query("DELETE FROM fms_vendors_purchaseorders WHERE PurchaseOrderId='$iPurchaseOrderId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Delete Purchase Order - " . $sPurchaseOrderNumber);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iPurchaseOrderId . '&error=5304');
			else
				$objGeneral->fnRedirect('?id=0&error=5304');
		}
		else
			return(5305);
	}

}

?>