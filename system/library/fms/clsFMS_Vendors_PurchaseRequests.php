<?php

// Products Purchase Requests Class
class clsVendors_PurchaseRequests
{
	public $aPurchaseRequestStatus;

	// Constructor
    function __construct()
    {
    	$this->aPurchaseRequestStatus = array("Requested", "Processed", "Approved", "Rejected");
    }

	function ShowAllPurchaseRequests($sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry Access Denied to this Area...</div><br /><br />');

		//$sVendorCondition = "AND 1=1";

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = "Purchase Requests";
        if ($sSortBy == "") $sSortBy = "PR.PurchaseRequestId DESC";

        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((PR.ProductName LIKE '%$sSearch%') OR (PR.PurchaseRequestId LIKE '%$sSearch%') OR (PR.PurchaseRequestNumber LIKE '%$sSearch%') OR (PR.ProductCost LIKE '%$sSearch%') OR (PR.ProductQuantity LIKE '%$sSearch%') OR (V.VendorName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

		$iTotalRecords = $objDatabase->DBCount("fms_vendors_purchaserequests AS PR LEFT JOIN fms_vendors AS V ON V.VendorId = PR.VendorId INNER JOIN organization_employees AS E ON E.EmployeeId = PR.PurchaseRequestAddedBy", "PR.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors_purchaserequests AS PR
		LEFT JOIN fms_vendors AS V ON V.VendorId = PR.VendorId
		INNER JOIN organization_employees AS E ON E.EmployeeId = PR.PurchaseRequestAddedBy
        WHERE PR.OrganizationId='" . cOrganizationId . "' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td align="left"><span class="WhiteHeading">Request No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=PR.PurchaseRequestNumber&sortorder="><img src="../images/sort_up.gif" alt="Sort by Purchase Request Number in Ascending Order" title="Sort by Purchase Request Number in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=PR.PurchaseRequestNumber&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Purchase Request Number in Descending Order" title="Sort by Purchase Request Number in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Total Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=PR.TotalAmount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Total Amount in Ascending Order" title="Sort by Total Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=PR.TotalAmount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Total Amount in Descending Order" title="Sort by Total Amount in Descending Order" border="0" /></a></span></td>		  
		  <td align="left"><span class="WhiteHeading">Status&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=PR.Status&sortorder="><img src="../images/sort_up.gif" alt="Sort by Status in Ascending Order" title="Sort by Status in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=PR.Status&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Status in Descending Order" title="Sort by Status in Descending Order" border="0" /></a></span></td>
		  <td width="4%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iPurchaseRequestId = $objDatabase->Result($varResult, $i, "PR.PurchaseRequestId");
			$sPurchaseRequestNumber = $objDatabase->Result($varResult, $i, "PR.PurchaseRequestNumber");
			$sPurchaseRequestNumber = $objDatabase->RealEscapeString($sPurchaseRequestNumber);
			$sPurchaseRequestNumber = stripslashes($sPurchaseRequestNumber);
			$iPurchaseRequestStatus = $objDatabase->Result($varResult, $i, "PR.Status");
			$sPurchaseRequestStatus = $this->aPurchaseRequestStatus[$iPurchaseRequestStatus];
			
			$dTotalAmount = $objDatabase->Result($varResult, $i, "PR.TotalAmount");
									
			$sEditPurchaseRequest = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Edit ' . $objDatabase->RealEscapeString(str_replace('"', '', $sPurchaseRequestNumber)) . '\', \'../vendors/purchaserequests.php?pagetype=details&action2=edit&id=' . $iPurchaseRequestId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Purchase Request" title="Edit this Purchase Request"></a></td>';
            $sDeletePurchaseRequest = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Purchase Request?\')) {window.location = \'?action=DeletePurchaseRequest&id=' . $iPurchaseRequestId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Purchase Request" title="Delete this Purchase Request"></a></td>';
			$sQuotationsList = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Purchase Request ' . $objDatabase->RealEscapeString(str_replace('"', '', $sPurchaseRequestNumber)) . '\', \'../vendors/quotationsforpurchaserequest_showall.php?id=' . $iPurchaseRequestId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" alt="View Quotations list against this Purchase Request" title="View Quotations list against this Purchase Request"></a></td>';
			$sDocuments = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/documents_show.php?componentname=Vendors_PurchaseRequests&id=' . $iInvoiceId . '\', \'Documents of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sPurchaseRequestNumber)) . '\', \'700 420\');"><img src="../images/icons/save.gif" border="0" alt="Purchase Request Documents" title="Purchase Request Documents"></a></td>';

       		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[2] == 0)$sEditPurchaseRequest = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[3] == 0)$sDeletePurchaseRequest = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests_Documents[0] == 0)	$sDocuments = '<td class="GridTD">&nbsp;</td>';	

            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
             <td class="GridTD" align="left" valign="top">' . $sPurchaseRequestNumber  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . $objEmployee->aSystemSettings['CurrencySign'] . '&nbsp;' . number_format($dTotalAmount, 2)  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>			 
			 <td class="GridTD" align="left" valign="top">' . $sPurchaseRequestStatus  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\',\'View ' . $objDatabase->RealEscapeString(str_replace('"', '', $sPurchaseRequestNumber)) . '\',  \'../vendors/purchaserequests.php?pagetype=details&id=' . $iPurchaseRequestId .'\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Purchase Request Details" title="View this Purchase Request Details"></a></td>
			 ' . $sEditPurchaseRequest . '			 
			 ' . $sDocuments . '
             ' . $sDeletePurchaseRequest . '
            </tr>';
		}

		$ssAddNewPurchaseRequest = '<input onclick="window.top.CreateTab(\'tabContainer\', \'New Purchase Request\', \'../vendors/purchaserequests.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="New Request">';

        if ($objEmployee->objEmployeeRoles->aEmployeeRole_Inventory_Products_PurchaseRequests[1] == 0) // Add Disabled
			$sAddNewProductPurchase = '';

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $ssAddNewPurchaseRequest . '
           <form method="GET" action="">&nbsp;&nbsp;&nbsp;Search for a Purchase Request:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Purchase Request" title="Search for Purchase Request" border="0"></form>
          </td> 
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Purchase Request Details" alt="View this Purchase Request Details"></td><td>View this Purchase Request Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Purchase Request Details" alt="Edit this Purchase Request Details"></td><td>Edit this Purchase Request Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconContents.gif" border="0" alt="View Quotations list against this Purchase Request" title="View Quotations list against this Purchase Request"></td><td>View Quotations list against this Purchase Request</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Purchase Request " alt="Delete this Purchase Request"></td><td>Delete this Purchase Request</td></tr>
        </table>';

	 	return($sReturn);
    }

    function PurchaseRequestDetails($iPurchaseRequestId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		//die($iPurchaseRequestId . " test");
		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_vendors_purchaserequests AS PR				
		INNER JOIN organization_employees AS E ON E.EmployeeId = PR.PurchaseRequestAddedBy
		INNER JOIN organization_employees AS E2 ON E2.EmployeeId = PR.EmployeeId
		WHERE PR.OrganizationId='" . cOrganizationId . "' AND PR.PurchaseRequestId = '$iPurchaseRequestId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iPurchaseRequestId. '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Purchase Request" title="Edit this Purchase Request" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Purchase Requests?\')) {window.location = \'?pagetype=details&action=DeletePurchaseRequest&id=' . $iPurchaseRequestId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Purchase Request" title="Delete this Purchase Request" /></a>';

    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[3] == 0)	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Purchase Request Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iPurchaseRequestId = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestId");
		    	$iVendorId = $objDatabase->Result($varResult, 0, "PR.VendorId");
				$iEmployeeId = $objDatabase->Result($varResult, 0, "PR.EmployeeId");
								
		    	$dTotalAmount = $objDatabase->Result($varResult, 0, "PR.TotalAmount");
				$sTotalAmount = number_format($dTotalAmount,2);
						    	
				$sPurchaseRequestNumber = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestNumber");
				$sPurchaseRequestNumber = $objDatabase->RealEscapeString($sPurchaseRequestNumber);
				$sPurchaseRequestNumber = stripslashes($sPurchaseRequestNumber);
				
				$iPurchaseRequestAddedBy = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestAddedBy");
		    	$sPurchaseRequestAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				
				$sPurchaseRequestAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Employee Information\', \'../organization/employees.php?pagetype=details&id=' . $iPurchaseRequestAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';

		    	$iPurchaseRequestStatus = $objDatabase->Result($varResult, 0, "PR.Status");
		    	$sPurchaseRequestStatus = $this->aPurchaseRequestStatus[$iPurchaseRequestStatus];
		        
		        $sEmployeeName = $objDatabase->Result($varResult, 0, "E2.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E2.LastName");		        
		        $sEmployeeName  = $sEmployeeName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sEmployeeName . '\', \'../organization/employees.php?pagetype=details&id=' . $iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
		        
		        $sDescription = $objDatabase->Result($varResult, 0, "PR.Description");
		        $sNotes = $objDatabase->Result($varResult, 0, "PR.Notes");

				$dPurchaseRequestAddedOn = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestAddedOn");
				$sPurchaseRequestAddedOn = date("F j, Y", strtotime($dPurchaseRequestAddedOn)) . ' at ' . date("g:i a", strtotime($dPurchaseRequestAddedOn));
				
				$varResult2 = $objDatabase->Query("SELECT * 
				FROM fms_vendors_purchaserequests_items AS PRI 
				WHERE PRI.PurchaseRequestId = '$iPurchaseRequestId' 
				ORDER BY PurchaseRequestItemId ASC");
						       	
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';
										
					$sProductName = $objDatabase->Result($varResult2, $i, "PRI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $i, "PRI.Quantity");
					$dUnitPrice = $objDatabase->Result($varResult2, $i, "PRI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $i, "PRI.Amount");				
					
					$sOrderParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . ($i+1) . '</td>					 
 					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;">' . $sProductName . '</td>					 					 
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . $iQuantity . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>
					</tr>';
				}
				
				$sPurchaseRequestItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>				  				  
				 </tr>
				 ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:18px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</td>
				 </tr>				 
				</table>';
		    }

			if ($sAction2 == "edit")
			{	
		        $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sPurchaseRequestStatus = '<select class="form1" name="selPurchaseRequestStatus" id="selPurchaseRequestStatus">';
		        for ($i=0; $i < count($this->aPurchaseRequestStatus); $i++)
		        	$sPurchaseRequestStatus .= '<option ' . (($iPurchaseRequestStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aPurchaseRequestStatus[$i] . '</option>';
		        $sPurchaseRequestStatus .= '</select>';
		        
				$sEmployeeName = $objEmployee->GetEmployeesSelectListBox($iEmployeeId, "selEmployee");
				
				$sPurchaseRequestNumber = '<input type="text" name="txtPurchaseRequestNumber" id="txtPurchaseRequestNumber" class="form1" value="' . $sPurchaseRequestNumber . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sProductName = '<input type="text" name="txtProductName" id="txtProductName" class="form1" value="' . $sProductName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$iProductQuantity= '<input type="text" name="txtProductQty" id="txtProductQty" class="form1" value="' . $iProductQuantity . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sProductCost= '<input type="text" name="txtProductCost" id="txtProductCost" class="form1" value="' . $dProductCost  . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$dProductWeight='<input type="text" name="txtProductWeight" id="txtProductWeight" class="form1" value="' . $dProductWeight . '" size="10" />';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				
				$sOrderParticulars_Rows = "";
				$varResult2 = $objDatabase->Query("SELECT * 
				FROM fms_vendors_purchaserequests_items AS PRI				
				WHERE PRI.PurchaseRequestId= '$iPurchaseRequestId'
				ORDER BY PurchaseRequestItemId ASC");
						       	
		        for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';			
					
					$sProductName = $objDatabase->Result($varResult2, $k, "PRI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $k, "PRI.Quantity");					
					$dUnitPrice = $objDatabase->Result($varResult2, $k, "PRI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $k, "PRI.Amount");					
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');"  type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				for ($j = $k; $j < 50; $j++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'none';
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				$sPurchaseRequestItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				  ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</div></td>
				 </tr>
				</table>				
				<input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="' . $dTotalAmount . '" />
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $j . ';
				 var iRowVisible = ' . ($k +1) . ';
				</script>';
			}
			else if ($sAction2 == "addnew")
			{
				$iPurchaseRequestId = "";
		        $sProductName = $objGeneral->fnGet("txtProductName");
		        $iProductQuantity = $objGeneral->fnGet("txtProductQty");
		        $dProductCost = $objGeneral->fnGet("txtProductCost");
		        $dProductWeight = $objGeneral->fnGet("txtProductWeight");
		        $iProductStatus = $objGeneral->fnGet("selProductPurchaseRequestStatus");
		        $iStationId= $objGeneral->fnGet("selStation");
		        $sDescription = $objGeneral->fnGet("txtDescription");
		        $sNotes = $objGeneral->fnGet("txtNotes");
				
				$sEmployeeName = $objEmployee->GetEmployeesListBox("selEmployee", $objEmployee->iEmployeeId);
				
   				$sPurchaseRequestAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objEmployee->sEmployeeUserName . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
   				
		       
		        $sPurchaseRequestStatus = '<select class="form1" name="selPurchaseRequestStatus" id="selPurchaseRequestStatus">';
		        for ($i=0; $i < count($this->aPurchaseRequestStatus); $i++)
		        	$sPurchaseRequestStatus .= '<option ' . (($iPurchaseRequestStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aPurchaseRequestStatus[$i] . '</option>';
		        $sPurchaseRequestStatus .= '</select>';
		        
                $sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
		        
				$sPurchaseRequestNumber = '<input type="text" name="txtPurchaseRequestNumber" id="txtPurchaseRequestNumber" class="form1" value="' . $sPurchaseRequestNumber . '" size="30" />&nbsp;<span style="color:red;">*</span>';
                $sProductName = '<input type="text" name="txtProductName" id="txtProductName" class="form1" value="' . $sProductName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
                $iProductQuantity= '<input type="text" name="txtProductQty" id="txtProductQty" class="form1" value="' . $iProductQuantity . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sProductCost= '<input type="text" name="txtProductCost" id="txtProductCost" class="form1" value="' . $dProductCost . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$dProductWeight='<input type="text" name="txtProductWeight" id="txtProductWeight" class="form1" value="' . $dProductWeight . '" size="10" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sPurchaseRequestAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				for ($k=0; $k < 50; $k++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = (($k <= 4) ? 'table-row' : 'none');
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" size="3" /></td> 
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
					</tr>';
				}				
				
				$sPurchaseRequestItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				 ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount"></div></td>
				 </tr>
				</table>
				<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="" />
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $k . ';
				 var iRowVisible = 6;	
				</script>';
				
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">			
		    function ValidateForm()
		    {
				if (GetVal(\'txtPurchaseRequestNumber\') == "") return(AlertFocus(\'Please enter a valid Purchase Request Number\', \'txtPurchaseRequestNumber\'));
		        return true;
		    }
		    </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Purchase Request Information:</span></td></tr>			 
			 <tr><td valign="top" style="width:200px;">Employee Name:</td><td><strong>' . $sEmployeeName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Purchase Request No.:</td><td><strong>' . $sPurchaseRequestNumber . '</strong></td></tr>
			 <!--<tr><td>Product Name:</td><td><strong>' . $sProductName . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Product Attributes:</span></td></tr>
			 <tr bgcolor="#edeff1"><td>Product Quantity:</td><td><strong>' . $iProductQuantity . '</strong></td></tr>
			 <tr><td>Product Cost:</td><td><strong>' . $sProductCost . '&nbsp;' . $objEmployee->aSystemSettings['CurrencySign'] . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Product Weight:</td><td><strong>' . $dProductWeight . ' KG ' . '</strong></td></tr>
			-->
			 <tr><td>Purchase Request Status:</td><td><strong>' . $sPurchaseRequestStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Purchase Request Items:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">' . $sPurchaseRequestItems . '</td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr><td>Requested On:</td><td><strong>' . $sPurchaseRequestAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Requested By:</td><td><strong>' . $sPurchaseRequestAddedBy . '</strong></td></tr>			 
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iPurchaseRequestId . '"><input type="hidden" name="action" id="action" value="UpdatePurchaseRequest"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Purchase Request" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewPurchaseRequest"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
	}
	
    function UpdatePurchaseRequest($iPurchaseRequestId, $iEmployeeId, $sPurchaseRequestNumber, $iPurchaseRequestStatus, $sDescription, $dTotalAmount, $sNotes)
    {
        global $objDatabase;
        global $objEmployee;
        global $objGeneral;
		global $objSystemLog;

		// Employee Roles
	    if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[2] == 0) // Update Disabled
			return(1010);
	    
		$sPurchaseRequestNumber = $objDatabase->RealEscapeString($sPurchaseRequestNumber);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_vendors_purchaserequests AS PR", "PR.OrganizationId='" . cOrganizationId . "' AND PR.PurchaseRequestId <> '$iPurchaseRequestId' AND PR.PurchaseRequestNumber = '$sPurchaseRequestNumber'") > 0)
			return(6108);

        $varResult = $objDatabase->Query("
	    UPDATE fms_vendors_purchaserequests SET        	
            Status='$iPurchaseRequestStatus',
			PurchaseRequestNumber='$sPurchaseRequestNumber',
            Description='$sDescription',
			TotalAmount='$dTotalAmount',
            Notes='$sNotes'
    	WHERE OrganizationId='" . cOrganizationId . "' AND PurchaseRequestId='$iPurchaseRequestId'");
		$varResult = $objDatabase->Query("DELETE FROM fms_vendors_purchaserequests_items WHERE PurchaseRequestId='$iPurchaseRequestId'");
		
		for ($k=0; $k < 50; $k++)
		{
			$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));
			$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
			$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
			$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);
			
			if(($sProductName != "") && ($iProductQty > 0))
				$this->AddPurchaseRequestItems($iPurchaseRequestId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
			
		}		
		
		$objSystemLog->AddLog("Update Purchase Requests - " . $sPurchaseRequestNumber);

		$objGeneral->fnRedirect('?pagetype=details&error=6103&id=' . $iPurchaseRequestId);
       
    }

    function DeletePurchaseRequest($iPurchaseRequestId)
    {
        global $objDatabase;
        global $objEmployee;
		global $objGeneral;
		global $objSystemLog;

        // Employee Roles
	   	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[3] == 0) // Delete Disabled
	   	{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iPurchaseRequestId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}
		
		if ($objDatabase->DBCount("fms_vendors_quotations AS Q", "Q.OrganizationId='" . cOrganizationId . "' AND Q.PurchaseRequestId= '$iPurchaseRequestId'") > 0) return(6107);
		
        $varResult = $objDatabase->Query("SELECT * FROM fms_vendors_purchaserequests AS PR WHERE PR.OrganizationId='" . cOrganizationId . "' AND PR.PurchaseRequestId='$iPurchaseRequestId'");
        if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Purchase Request Id');

        $sPurchaseRequestNumber = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestNumber");
       	$varResult = $objDatabase->Query("DELETE FROM fms_vendors_purchaserequests WHERE PurchaseRequestId='$iPurchaseRequestId'");

       	if ($objDatabase->AffectedRows($varResult) > 0)
        {
			$varResult = $objDatabase->Query("DELETE FROM fms_vendors_purchaserequests_items WHERE PurchaseRequestId='$iPurchaseRequestId'");
			
			$objSystemLog->AddLog("Delete Purchase Requests - " . $sPurchaseRequestNumber);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iPurchaseRequestId . '&error=6105');
			else
				$objGeneral->fnRedirect('?id=0&error=6105');
        }
        else
            return(6106);
    }

    function AddNewPurchaseRequest($iEmployeeId, $sPurchaseRequestNumber, $iPurchaseRequestStatus,$sDescription, $dTotalAmount, $sNotes)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
		
        $iPurchaseRequestAddedBy = $objEmployee->iEmployeeId;
        $varNow = $objGeneral->fnNow();

		// Employee Roles
   		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_PurchaseRequests[1] == 0) // Add Disabled
			return(1010);
		
		$sPurchaseRequestNumber = $objDatabase->RealEscapeString($sPurchaseRequestNumber);		
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
	
		if($objDatabase->DBCount("fms_vendors_purchaserequests AS PR", "PR.OrganizationId='" . cOrganizationId . "' AND PR.PurchaseRequestNumber = '$sPurchaseRequestNumber'") > 0)
			return(6100);
			
        $varResult = $objDatabase->Query("INSERT INTO fms_vendors_purchaserequests
        (OrganizationId, EmployeeId, Status, PurchaseRequestNumber, Description, TotalAmount, PurchaseRequestAddedOn, PurchaseRequestAddedBy, Notes)
        VALUES ('" . cOrganizationId . "', '$iEmployeeId', '$iPurchaseRequestStatus', '$sPurchaseRequestNumber', '$sDescription', '$dTotalAmount', '$varNow', '$iPurchaseRequestAddedBy', '$sNotes')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_vendors_purchaserequests AS PR WHERE PR.PurchaseRequestNumber='$sPurchaseRequestNumber' AND PR.PurchaseRequestAddedON='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {
			$iPurchaseRequestId = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestId");
			for ($k=0; $k < 50; $k++)
			{					
				$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));
				$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
				$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
				$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);
				
				if(($sProductName != "") && ($iProductQty > 0))
					$this->AddPurchaseRequestItems($iPurchaseRequestId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);
				
			}
						        	
  			$objSystemLog->AddLog("Add New Purchase Request - " . $sPurchaseRequestNumber);
  			$objGeneral->fnRedirect('?pagetype=details&error=6101&id=' . $iPurchaseRequestId);
        }
        else
             return(6102);
    }
	
	function AddPurchaseRequestItems($iPurchaseRequestId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount)
	{
		global $objDatabase;
		global $objGeneral;
		
		$sProductName = $objDatabase->RealEscapeString($sProductName);
		if($iProductQty == "" ) $iProductQty = 0;
		if($dProductUnitPrice == "" ) $dProductUnitPrice = 0;
		if($dProductTotalAmount == "" ) $dProductTotalAmount = 0;
		
		$varResult2 = $objDatabase->Query("INSERT INTO fms_vendors_purchaserequests_items
		(PurchaseRequestId, ProductName, Quantity, UnitPrice, Amount)
		VALUES ('$iPurchaseRequestId', '$sProductName', '$iProductQty', '$dProductUnitPrice', '$dProductTotalAmount')");
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
		
		
	}
    
}
?>