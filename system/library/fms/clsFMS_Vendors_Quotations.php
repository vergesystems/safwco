<?php

// Class: Quotation
class clsQuotations
{
	public $aQuotationStatus;
	// Class Constructor
	function __construct()
	{
		$this->aQuotationStatus = array("Received", "Processed", "Approved", "Rejected");
	}
	
	function ShowAllQuotations($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Quotations";
		if ($sSortBy == "") $sSortBy = "Q.QuotationId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((Q.QuotationId LIKE '%$sSearch%') OR (Q.QuotationNumber LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (PR.PurchaseRequestNumber LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}
		
		$iTotalRecords = $objDatabase->DBCount("fms_vendors_quotations AS Q INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy INNER JOIN fms_vendors_purchaserequests AS PR ON PR.PurchaseRequestId = Q.PurchaseRequestId", "Q.OrganizationId='" . cOrganizationId . "' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors_quotations AS Q 
		INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy
		LEFT JOIN fms_vendors_purchaserequests AS PR ON PR.PurchaseRequestId = Q.PurchaseRequestId
		INNER JOIN fms_vendors AS V ON V.VendorId = Q.VendorId
		WHERE Q.OrganizationId='" . cOrganizationId . "' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationTitle&sortorder="><img src="../images/sort_up.gif" alt="Sort by Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationTitle&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Quotation No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Quotation No in Ascending Order" title="Sort by Quotation No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Quotation No in Descending Order" title="Sort by Quotation No in Descending Order" border="0" /></a></span></td>
		  <td width="18%" align="left"><span class="WhiteHeading">Request No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=P.PurchaseOrderNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Purchase Order No in Ascending Order" title="Sort by Purchase Order No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=P.PurchaseOrderNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Purchase Order No in Descending Order" title="Sort by Purchase Order No in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Vendor Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Name in Ascending Order" title="Sort by Vendor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Name in Descending Order" title="Sort by Vendor Name in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Total Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.TotalAmount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Total Amount in Ascending Order" title="Sort by Total Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.TotalAmount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Total Amount in Descending Order" title="Sort by Total Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Quotation Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Quotation Date in Ascending Order" title="Sort by Quotation Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Quotation Date in Descending Order" title="Sort by Quotation Date in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iQuotationId = $objDatabase->Result($varResult, $i, "Q.QuotationId");
			
			$sQuotationTitle = $objDatabase->Result($varResult, $i, "Q.QuotationTitle");
			$sQuotationTitle = $objDatabase->RealEscapeString($sQuotationTitle);
			$sQuotationTitle = stripslashes($sQuotationTitle);
			
			$dTotalAmount = $objDatabase->Result($varResult, $i, "Q.TotalAmount");
			$sTotalAmount = number_format($dTotalAmount, 2);
			
			$sQuotationNo = $objDatabase->Result($varResult, $i, "Q.QuotationNumber");
			$sQuotationNo = $objDatabase->RealEscapeString($sQuotationNo);
			$sQuotationNo = stripslashes($sQuotationNo);
			
			$sPurchaseRequestNumber = $objDatabase->Result($varResult, $i, "PR.PurchaseRequestNumber");
			$sPurchaseRequestNumber = $objDatabase->RealEscapeString($sPurchaseRequestNumber);
			$sPurchaseRequestNumber = stripslashes($sPurchaseRequestNumber);
			
			$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");		
			
			$dQuotationDate = $objDatabase->Result($varResult, $i, "Q.QuotationDate");
			$sQuotationDate = date("F j, Y", strtotime($dQuotationDate));
			
			$sEditQuotation = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sQuotationTitle)) . '\', \'../vendors/quotations.php?pagetype=details&action2=edit&quotationid=' . $iQuotationId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quotation Details" title="Edit this Quotation Details"></a></td>';
			$sDeleteQuotation = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Quotation?\')) {window.location = \'?action=DeleteQuotation&quotationid=' . $iQuotationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quotation" title="Delete this Quotation"></a></td>';	
			$sDocuments = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/documents_show.php?componentname=Vendors_Quotations&id=' . $iInvoiceId . '\', \'Documents of ' . $objDatabase->RealEscapeString(str_replace('"', '', $sPurchaseRequestNumber)) . '\', \'700 420\');"><img src="../images/icons/save.gif" border="0" alt="Quotation Documents" title="Quotation Documents"></a></td>';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[2] == 0)	$sEditQuotation = '<td class="GridTD">&nbsp;</td>';			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[3] == 0)	$sDeleteQuotation = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations_Documents[0] == 0)	$sDocuments = '<td class="GridTD">&nbsp;</td>';	
				
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td class="GridTD" align="left" valign="top">' . $sQuotationTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sQuotationNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sPurchaseRequestNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sVendorName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="right" valign="top">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . $sTotalAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sQuotationDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'View ' . $objDatabase->RealEscapeString(stripslashes($sQuotationTitle)) . '\', \'../vendors/quotations.php?pagetype=details&quotationid=' . $iQuotationId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quotation Details" title="View this Quotation Details"></a></td>
			 ' . $sEditQuotation . $sDocuments . $sDeleteQuotation . '</tr>';
		}

		$sAddQuotation = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Quotation\', \'../vendors/quotations.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Quotation">';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[1] == 0) // Add Disabled
			$sAddQuotation = '';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddQuotation . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Quotation:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Quotation" title="Search for a Quotation" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Quotation Details" alt="View this Quotation Details"></td><td>View this Quotation Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Quotation Details" alt="Edit this Quotation Details"></td><td>Edit this Quotation Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Quotation" alt="Delete this Quotation"></td><td>Delete this Quotation</td></tr>
		</table>';

		return($sReturn);
	}
	
	function QuotationDetails($iQuotationId, $iPurchaseRequestId, $sAction2)
	{		
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		$k = 0;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_vendors_quotations AS Q
		INNER JOIN fms_vendors AS V ON V.VendorId = Q.VendorId
		INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy
		LEFT JOIN fms_vendors_purchaserequests AS PR ON PR.PurchaseRequestId = Q.PurchaseRequestId
		WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationId = '$iQuotationId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&quotationid=' . $iQuotationId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quotation Details" title="Edit this Quotation Details" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Quotation?\')) {window.location = \'?pagetype=details&action=DeleteQuotation&quotationid=' . $iQuotationId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quotation" title="Delete this Quotation" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[3] == 0)	$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Quotation Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iQuotationId = $objDatabase->Result($varResult, 0, "Q.QuotationId");
				$iPurchaseRequestId = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestId");
				$iVendorId = $objDatabase->Result($varResult, 0, "Q.VendorId");
								
				$sPurchaseRequestNo = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestNumber");
				$sPurchaseRequestNo = $objDatabase->RealEscapeString($sPurchaseRequestNo);
				$sPurchaseRequestNo = stripslashes($sPurchaseRequestNo);
				if($sPurchaseRequestNo !="")
					$sPurchaseRequestNo .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sPurchaseRequestNo)) . '\', \'../vendors/purchaserequests.php?pagetype=details&id=' . $iPurchaseRequestId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Purchase Request Information" title="Purchase Request Information" border="0" /></a><br />';
								
				$sQuotationTitle = $objDatabase->Result($varResult, 0, "Q.QuotationTitle");
				$sQuotationTitle = $objDatabase->RealEscapeString($sQuotationTitle);
				$sQuotationTitle = stripslashes($sQuotationTitle);
				$sQuotationNumber = $objDatabase->Result($varResult, 0, "Q.QuotationNumber");
				$sQuotationNumber = $objDatabase->RealEscapeString($sQuotationNumber);
				$sQuotationNumber = stripslashes($sQuotationNumber);
				
				$dQuotationDate = $objDatabase->Result($varResult, 0, "Q.QuotationDate");
				$sQuotationDate = date("F j, Y", strtotime($dQuotationDate));
				
				$dValidityDate = $objDatabase->Result($varResult, 0, "Q.ValidityDate");
				$sValidityDate= date("F j, Y", strtotime($dValidityDate));
								
				$dTotalAmount = $objDatabase->Result($varResult, 0, "Q.TotalAmount");
				$sTotalAmount = number_format($dTotalAmount,4);
				$iQuotationStatus = $objDatabase->Result($varResult, 0, "Q.Status");
				$sQuotationStatus = $this->aQuotationStatus[$iQuotationStatus];
				
				$sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");
		        if ($sVendorName == '') $sVendorName = 'No Vendor';
		        else $sVendorName = $sVendorName . '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $sVendorName . '\', \'../vendors/vendors.php?pagetype=details&id=' . $iVendorId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" align="absmiddle" alt="Vendor Information" title="Vendor Information" border="0" /></a>';
		        
				
				$iQuotationAddedBy = $objDatabase->Result($varResult, 0, "Q.QuotationAddedBy");
				$sQuotationAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sQuotationAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sQuotationAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iQuotationAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a><br />';
				
				$sDescription = $objDatabase->Result($varResult, 0, "Q.Description");
				$sDescription = $objDatabase->RealEscapeString($sDescription);
				$sDescription = stripslashes($sDescription);
				
				$sNotes = $objDatabase->Result($varResult, 0, "Q.Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
			    
				$dQuotationAddedOn = $objDatabase->Result($varResult, 0, "Q.QuotationAddedOn");
				$sQuotationAddedOn = date("F j, Y", strtotime($dQuotationAddedOn)) . ' at ' . date("g:i a", strtotime($dQuotationAddedOn));
				
				$varResult2 = $objDatabase->Query("SELECT * 
				FROM fms_vendors_quotations_items AS QI 
				WHERE QI.QuotationId = '$iQuotationId'
				ORDER BY QI.QuotationItemId DESC");						       	
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';
					
					$sProductName = $objDatabase->Result($varResult2, $i, "QI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $i, "QI.Quantity");
					$dUnitPrice = $objDatabase->Result($varResult2, $i, "QI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $i, "QI.Amount");				
					
					$sOrderParticulars_Rows .= '<tr style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . ($i+1) . '</td>					 
 					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;">' . $sProductName . '</td>					 					 
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dUnitPrice, 0) . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="center">' . $iQuantity . '</td>
					 <td valign="top" style="font-size:13px; font-family: Tahoma, Arial;" align="right">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</td>
					</tr>';
				}
				
				$sQuotationItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>				  				  
				 </tr>
				 ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000; font-family:Tahoma, Arial; font-size:18px;">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</td>
				 </tr>				 
				</table>';
			}
			
			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';		
				
				$sVendorName = '<select class="form1" name="selVendor" id="selVendor">
				<option value="0">Select Vendor</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "' Order by VendorName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {   
					$iCurrentVendorId =  $objDatabase->Result($varResult2, $i, "V.VendorId");   
					$sVendorName .= '<option ' . (($iVendorId == $iCurrentVendorId) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult2, $i, "V.VendorName") . '</option>';
		        }
		        $sVendorName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selVendor\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selVendor\'), \'../vendors/vendors.php?pagetype=details&id=\'+GetSelectedListBox(\'selVendor\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Vendor Details" title="Vendor Details" /></a>';
				
				$sPurchaseRequestNo = '<select name="selPurchaseRequest" class="form1" id="selPurchaseRequest">
				<option value="0">No Request Number</option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_purchaserequests AS PR WHERE PR.OrganizationId='" . cOrganizationId . "' AND PR.Status = '2' ORDER BY PR.PurchaseRequestNumber");
				if($objDatabase->RowsNumber($varResult) > 0)
				{
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)					
						$sPurchaseRequestNo .= '<option ' . (($iPurchaseRequestId == $objDatabase->Result($varResult, $i, "PR.PurchaseRequestId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "PR.PurchaseRequestId") . '">' . $objDatabase->Result($varResult, $i, "PR.PurchaseRequestNumber") . '</option>';
				}				
				$sPurchaseRequestNo .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selPurchaseRequest\'), \'../vendors/purchaserequests.php?pagetype=details&id=\'+GetSelectedListBox(\'selPurchaseRequest\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Purchase Request Details" title="Purchase Request Details" /></a>';
				
				$sQuotationStatus = '<select class="form1" name="selQuotationtStatus" id="selQuotationtStatus">';
		        for ($i=0; $i < count($this->aQuotationStatus); $i++)
		        	$sQuotationStatus .= '<option ' . (($iQuotationtStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aQuotationStatus[$i] . '</option>';
		        $sQuotationStatus .= '</select>';
				
				$sQuotationTitle = '<input type="text" name="txtQuotationTitle" id="txtQuotationTitle" class="form1" value="' . $sQuotationTitle . '" size="15" />&nbsp;<span style="color:red;">*</span>';
				$sQuotationDate = '<input size="12" type="text" id="txtQuotationDate" name="txtQuotationDate" class="form1" value="' . $dQuotationDate . '" />' . $objjQuery->Calendar('txtQuotationDate') . '&nbsp;<span style="color:red;">*</span>';
				$sValidityDate = '<input size="12" type="text" id="txtValidityDate" name="txtValidityDate" class="form1" value="' . $dValidityDate . '" />' . $objjQuery->Calendar('txtValidityDate') . '&nbsp;<span style="color:red;">*</span>';
				$sQuotationNumber = '<input type="text" name="txtQuotationNumber" id="txtQuotationNumber" class="form1" value="' . $sQuotationNumber . '" size="15" />&nbsp;<span style="color:red;">*</span>';
				$sTotalAmount = '<input type="text" name="txtTotalAmount" id="txtTotalAmount" class="form1" value="' . $dTotalAmount . '" size="15" />&nbsp;<span style="color:red;">*</span>';
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sOrderParticulars_Rows = "";
				$varResult2 = $objDatabase->Query("SELECT * 
				FROM fms_vendors_quotations_items AS QI				
				WHERE QI.QuotationId= '$iQuotationId'
				ORDER BY QI.QuotationItemId DESC");
						       	
		        for ($k=0; $k < $objDatabase->RowsNumber($varResult2); $k++)
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'visible';			
					
					$sProductName = $objDatabase->Result($varResult2, $k, "QI.ProductName");
					$iQuantity = $objDatabase->Result($varResult2, $k, "QI.Quantity");					
					$dUnitPrice = $objDatabase->Result($varResult2, $k, "QI.UnitPrice");
					$dAmount = $objDatabase->Result($varResult2, $k, "QI.Amount");					
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');"  type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" type="text" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dAmount, 0) . '</div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				for ($j = $k; $j < 50; $j++) 
				{
					$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
					$sRowVisible = 'none';
					
					$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
					 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 					 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
					 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
					 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="3" /></td>
					 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
					 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
					</tr>';
				}
				
				$sQuotationItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				  ' . $sOrderParticulars_Rows . '
				 <tr bgcolor="#ffffff">
				  <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
				 </tr>
				 <tr bgcolor="#ffffff">
				  <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;">Total:</td>
				  <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 1px #000000;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">' . $objEmployee->aSystemSettings['CurrencySign'] . number_format($dTotalAmount, 0) . '</div></td>
				 </tr>
				</table>
				<input type="hidden" name="TotalPayment" id="TotalPayment" value="" />
				<input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="' . $dTotalAmount . '" />
				<script type="text/javascript" language="JavaScript">
				 var iRowCounter = ' . $j . ';
				 var iRowVisible = ' . ($k +1) . ';
				</script>';
			}
			
			else if ($sAction2 == "addnew")
			{
				$iQuotationId = "";				
				$sQuotationNumber = $objGeneral->fnGet("txtQuotationNumber");
				$sQuotationTitle = $objGeneral->fnGet("txtQuotationTitle");
				$sDescription = $objGeneral->fnGet("txtDescription");
				$sTotalAmount = $objGeneral->fnGet("txtTotalAmount");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$dQuotationDate = $objGeneral->fnGet("txtQuotationDate");
				$dValidityDate = $objGeneral->fnGet("txtValidityDate");
				
				if($dQuotationDate == "") $dQuotationDate = date("Y-m-d");
				if($dValidityate == "") $dValidityDate = date("Y-m-d", mktime(0, 0, 0, date("m")+1,   date("d"),   date("Y")));
								
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				
				$sVendorName = '<select class="form1" name="selVendor" id="selVendor">
				<option value="0">Select Vendor</option>';
		        $varResult2 = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "' ORDER BY VendorName");
		        for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
		        {   
					$iCurrentVendorId =  $objDatabase->Result($varResult2, $i, "V.VendorId");   
					$sVendorName .= '<option ' . (($iVendorId == $iCurrentVendorId) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult2, $i, "V.VendorName") . '</option>';
		        }
		        $sVendorName .= '</select>&nbsp;<a href="#noanchor" onclick="if(GetSelectedListBox(\'selVendor\') > 0) window.top.CreateTab(\'tabContainer\',GetSelectedListBoxValue(\'selVendor\'), \'../vendors/vendors.php?pagetype=details&id=\'+GetSelectedListBox(\'selVendor\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Vendor Details" title="Vendor Details" /></a>';
								
				$sPurchaseRequestNo = '<select name="selPurchaseRequest" class="form1" onchange="window.location=\'?pagetype=details&action2=addnew&purchaserequestid=\'+GetSelectedListBox(\'selPurchaseRequest\');" id="selPurchaseRequest">
				<option value="0"> No Request Number </option>';
				$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_purchaserequests AS PR WHERE PR.OrganizationId='" . cOrganizationId . "' AND PR.Status = '2' ORDER BY PR.PurchaseRequestNumber");
				if($objDatabase->RowsNumber($varResult) > 0)
				{	
					for($i = 0; $i < $objDatabase->RowsNumber($varResult); $i++)					
					$sPurchaseRequestNo .= '<option ' . (($iPurchaseRequestId == $objDatabase->Result($varResult, $i, "PR.PurchaseRequestId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult, $i, "PR.PurchaseRequestId") . '">' . $objDatabase->Result($varResult, $i, "PR.PurchaseRequestNumber") . '</option>';
				}
				$sPurchaseRequestNo .= '</select>&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', GetSelectedListBoxValue(\'selPurchaseRequest\'), \'../vendors/purchaserequests.php?pagetype=details&id=\'+GetSelectedListBox(\'selPurchaseRequest\'), \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" align="absmiddle" alt="Purchase Request Details" title="Purchase Request Details" /></a>';
				
				$sQuotationStatus = '<select class="form1" name="selQuotationtStatus" id="selQuotationtStatus">';
		        for ($i=0; $i < count($this->aQuotationStatus); $i++)
		        	$sQuotationStatus .= '<option ' . (($iQuotationtStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aQuotationStatus[$i] . '</option>';
		        $sQuotationStatus .= '</select>';
				
				$sQuotationTitle = '<input type="text" name="txtQuotationTitle" id="txtQuotationTitle" class="form1" value="' . $sQuotationTitle . '" size="15" />&nbsp;<span style="color:red;">*</span>';
				$sQuotationNumber = '<input type="text" name="txtQuotationNumber" id="txtQuotationNumber" class="form1" value="' . $sQuotationNumber . '" size="15" />&nbsp;<span style="color:red;">*</span>';
				$sQuotationDate = '<input size="12" type="text" id="txtQuotationDate" name="txtQuotationDate" class="form1" value="' . $dQuotationDate . '" />' . $objjQuery->Calendar('txtQuotationDate') . '&nbsp;<span style="color:red;">*</span>';
				$sValidityDate = '<input size="12" type="text" id="txtValidityDate" name="txtValidityDate" class="form1" value="' . $dValidityDate . '" />&nbsp;' . $objjQuery->Calendar('txtValidityDate') . '&nbsp;&nbsp;<span style="color:red;">*</span>';
				$sTotalAmount = '<input type="text" name="txtTotalAmount" id="txtTotalAmount" class="form1" value="' . $sTotalAmount . '" size="15" />&nbsp;<span style="color:red;">*</span>';		
				
				$sDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtDescription", $sDescription, 10, 80, "100%", "200px", "Basic");
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sQuotationAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
				
				$sQuotationAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;				
				$sQuotationAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sQuotationAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a><br />';				
				
				if($iPurchaseRequestId > 0)
				{
					$varResult = $objDatabase->Query("
					SELECT *
					FROM fms_vendors_purchaserequests AS PR
					INNER JOIN fms_vendors_purchaserequests_items AS PRI ON PRI.PurchaseRequestId = PR.PurchaseRequestId
					WHERE PR.OrganizationId='" . cOrganizationId . "' AND PR.PurchaseRequestId= '$iPurchaseRequestId'");
					for($k = 0; $k < $objDatabase->RowsNumber($varResult); $k++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';					
						$sRowVisible = 'visible';
												
						$sProductName = $objDatabase->Result($varResult, $k, "PRI.ProductName");					
						$iQuantity= $objDatabase->Result($varResult, $k, "PRI.Quantity");
						$dUnitPrice = $objDatabase->Result($varResult, $k, "PRI.UnitPrice");
						$dAmount = $objDatabase->Result($varResult, $k, "PRI.Amount");
						$dTotalAmount += $dAmount;
						
						$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" value="' . $sProductName . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" value="' . $dUnitPrice . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" value="' . $iQuantity . '" size="3" /></td> 
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '">'. $dAmount . '</div></td>
						 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
						</tr>';
					}
					for ($j = $k; $j < 50; $j++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						$sRowVisible = 'none';
						
						$sOrderParticulars_Rows .= '<tr name="row_'. ($j+1) . '" id="row_'. ($j+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($j+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($j+1) . '" id="txtOrderParticulars_ProductName_' . ($j+1) . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($j+1) . ');"  style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($j+1) . '" id="txtOrderParticulars_UnitPrice_' . ($j+1) . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($j+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($j+1) . '" id="txtOrderParticulars_Qty_' . ($j+1) . '" size="3" /></td>
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($j+1) . '"></div></td>
						 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($j+1) . ') "><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row"></a></td> 					 
						</tr>';
					}
					
					$sOrderParticulars_Rows .= '
					<tr bgcolor="#ffffff">
					 <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
					</tr>
					<tr bgcolor="#ffffff">
					 <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
					 <td align="right" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount">'. number_format($dTotalAmount, 2) . '</div></td>
					</tr>
					</table>
					<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="'. $dTotalAmount . '" />
					<script type="text/javascript" language="JavaScript">
					 var iRowCounter = ' . $j . ';
					 var iRowVisible = ' . ($k +1) . ';
					</script>';
					
				}				
				else
				{
					for ($k=0; $k < 50; $k++) 
					{
						$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
						$sRowVisible = (($k <= 4) ? 'visible' : 'none');
						
						$sOrderParticulars_Rows .= '<tr name="row_'. ($k+1) . '" id="row_'. ($k+1) . '" style="display:' . $sRowVisible . ';" onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
						 <td align="center" style="font-size:14px; font-family:Tahoma, Arial;">' . ($k+1) . '</td>
 						 <td><input type="text" style="font-size:14px; font-family:Tahoma, Arial;" name="txtOrderParticulars_ProductName_' . ($k+1) . '" id="txtOrderParticulars_ProductName_' . ($k+1) . '" size="100%" /></td>
						 <td align="right"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:right; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_UnitPrice_' . ($k+1) . '" id="txtOrderParticulars_UnitPrice_' . ($k+1) . '" size="10" /></td>
						 <td align="center"><input onkeyup="UpdateProductQty(' . ($k+1) . ');" style="text-align:center; font-size:14px; font-family:Tahoma, Arial;" type="text" name="txtOrderParticulars_Qty_' . ($k+1) . '" id="txtOrderParticulars_Qty_' . ($k+1) . '" size="3" /></td> 
						 <td align="right"><div style="font-family:Tahoma, Arial; font-size:14px;" id="divOrderParticulars_Amount_' . ($k+1) . '"></div></td>
						 <td align="center"><a href="#noanchor" onclick="DeleteRow(' . ($k+1) . ');"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Row" title="Delete this Row" /></a></td> 
						</tr>';
					}
					
					$sOrderParticulars_Rows .= '
					<tr bgcolor="#ffffff">
					 <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#noanchor" onclick="AddNewRow();"><img src="../images/icons/plus.gif" border="0" align="absmiddle" alt="Add New Row" title="Add New Row" />&nbsp;Add New Row</a></td>
					</tr>
					<tr bgcolor="#ffffff">
					 <td align="right" colspan="4" style="font-size:18px; font-family:Tahoma, Arial; border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;">Total:</td>
					 <td align="center" colspan="2" style="border-left: solid 1px #a1a1a1; border-top: solid 2px #a6a6a6;"><div style="font-family:Tahoma, Arial; font-size:18px;" id="divOrderTotalAmount" name="divOrderTotalAmount"></div></td>
					</tr>
					</table>
					<input type="hidden" name="TotalPayment" id="TotalPayment" value="" /><input type="hidden" name="OrderTotalAmount" id="OrderTotalAmount" value="" />
					<script type="text/javascript" language="JavaScript">
					 var iRowCounter = ' . $k . ';
					 var iRowVisible = 6;	
					</script>';
				}
				
				$sQuotationItems = '<table class="GridTable" border="1" cellspacing="0" cellpadding="5" width="100%" align="center">
		         <tr class="GridTR">
				  <td width="1%" align="center"><span class="WhiteHeading">S#</span></td>				  
				  <td><span class="WhiteHeading">Item</span></td>				  				  
				  <td width="14%" align="right"><span class="WhiteHeading">Unit Price</span></td>
				  <td width="8%" align="center"><span class="WhiteHeading">Qty</span></td>
				  <td width="12%" align="right"><span class="WhiteHeading">Amount</span></td>
				  <td width="1%" align="center"><span class="WhiteHeading">&nbsp;</span></td>
				 </tr>
				 ' . $sOrderParticulars_Rows;
			}
			
			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{	
				if(GetSelectedListBox(\'selVendor\') <= 0)	return(AlertFocus(\'Please select Vendor\', \'selVendor\'));			
				if (GetVal(\'txtQuotationTitle\') == "") return(AlertFocus(\'Please enter a valid Quotation Title\', \'txtQuotationTitle\'));
				if (GetVal(\'txtQuotationNumber\') == "") return(AlertFocus(\'Please enter a valid Quotation No\', \'txtQuotationNumber\'));
				if (!isDate(GetVal(\'txtQuotationDate\'))) return(AlertFocus(\'Please enter a valid value for Quotation Date\', \'txtQuotationDate\'));
				if (!isDate(GetVal(\'txtValidityDate\'))) return(AlertFocus(\'Please enter a valid value for Validity Date\', \'txtValidityDate\'));
				
				return true;
			}			
			</script>			
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Quotation Information:</span></td></tr>			 
			 <tr bgcolor="#edeff1"><td valign="top" style="width:200px;">Purchase Request No:</td><td><strong>' . $sPurchaseRequestNo . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Vendor Name:</td><td><strong>' . $sVendorName . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Quotation Title:</td><td><strong>' . $sQuotationTitle . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Quotation No:</td><td><strong>' . $sQuotationNumber . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Quotation Date:</td><td><strong>' . $sQuotationDate . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Validity Date:</td><td><strong>' . $sValidityDate . '</strong></td></tr>			 
			 <tr bgcolor="#ffffff"><td>Status:</td><td><strong>' . $sQuotationStatus . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Quotation Items:</span></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top" colspan="2">' . $sQuotationItems . '</td></tr> 
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Description:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2"><strong>' . $sDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Quotation Added On:</td><td><strong>' . $sQuotationAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Quotation Added By:</td><td><strong>' . $sQuotationAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align="center"><input type="submit" value="Update Quotation" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iQuotationId . '"><input type="hidden" name="action" id="action" value="UpdateQuotation"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align="center"><input type="submit" value="Add Quotation" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewQuotation"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewQuotation($iPurchaseRequestId, $iVendorId, $dQuotationDate, $dValidityDate, $sQuotationNumber, $sQuotationTitle, $sDescription, $dTotalAmount, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[1] == 0) // Add Disabled
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		$iQuotationAddedBy = $objEmployee->iEmployeeId;
				
		if($dTotalAmount == "") $dTotalAmount = 0;
				
		$sQuotationNumber = $objDatabase->RealEscapeString($sQuotationNumber);
		$sQuotationTitle = $objDatabase->RealEscapeString($sQuotationTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
				
		if($objDatabase->DBCount("fms_vendors_quotations AS Q", "Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationNumber = '$sQuotationNumber'") > 0)
			return(10000);
		
		$varResult = $objDatabase->Query("INSERT INTO fms_vendors_quotations
		(OrganizationId, PurchaseRequestId, VendorId, QuotationTitle, QuotationDate, ValidityDate, QuotationNumber, TotalAmount, Status, Description, Notes, QuotationAddedOn, QuotationAddedBy)
		VALUES ('" . cOrganizationId . "', '$iPurchaseRequestId', '$iVendorId', '$sQuotationTitle', '$dQuotationDate', '$dValidityDate', '$sQuotationNumber', '$dTotalAmount', '$iStatus', '$sDescription', '$sNotes', '$varNow', '$iQuotationAddedBy')");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_quotations AS Q WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationNumber='$sQuotationNumber' AND Q.QuotationAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
				$iQuotationId = $objDatabase->Result($varResult, 0, "Q.QuotationId");				
				for ($k=0; $k < 50; $k++)
				{					
					$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));					
					
					$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
					$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
					$dProductTotalAmount = $iProductQty * $dProductUnitPrice;					
					
					if(($sProductName != "") && ($iProductQty > 0))
						$this->AddQuotationItems($iQuotationId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);
					
				}
				
				$objSystemLog->AddLog("Add New Quotation - " . $sQuotationTitle);

				$objGeneral->fnRedirect('?pagetype=details&error=10001&quotationid=' . $objDatabase->Result($varResult, 0, "Q.QuotationId"));
			}
		}

		return(10002);
	}
	
	function UpdateQuotation($iQuotationId, $iPurchaseRequestId, $iVendorId, $dQuotationDate, $dValidityDate, $sQuotationNumber, $sQuotationTitle, $sDescription, $dTotalAmount, $iStatus, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[2] == 0) // Edit Disabled
			return(1010);
		
		if($dTotalAmount == "") $dTotalAmount = 0;		
		$sQuotationNumber = $objDatabase->RealEscapeString($sQuotationNumber);
		$sQuotationTitle = $objDatabase->RealEscapeString($sQuotationTitle);
		$sDescription = $objDatabase->RealEscapeString($sDescription);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_vendors_quotations AS Q", "Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationNumber = '$sQuotationNumber' AND Q.QuotationId <> '$iQuotationId'") > 0)
			return(10000);
		
		$varResult = $objDatabase->Query("
		UPDATE fms_vendors_quotations 
		SET
			PurchaseRequestId='$iPurchaseRequestId',
			VendorId='$iVendorId',
			QuotationDate='$dQuotationDate',
			ValidityDate='$dValidityDate',
			QuotationNumber='$sQuotationNumber',
			QuotationTitle='$sQuotationTitle',
			TotalAmount='$dTotalAmount',
			Status='$iStatus',
			Description='$sDescription', 
			Notes='$sNotes'
		WHERE OrganizationId='" . cOrganizationId . "' AND QuotationId='$iQuotationId'");
		
		$varResult = $objDatabase->Query("DELETE FROM fms_vendors_quotations_items WHERE QuotationId='$iQuotationId'");
		
		for ($k=0; $k < 50; $k++)
		{					
			$sProductName = $objGeneral->fnGet("txtOrderParticulars_ProductName_" . ($k +1));					
			
			$iProductQty = $objGeneral->fnGet("txtOrderParticulars_Qty_" . ($k +1));
			$dProductUnitPrice = $objGeneral->fnGet("txtOrderParticulars_UnitPrice_" . ($k +1));
			$dProductTotalAmount = floatval($iProductQty) * floatval($dProductUnitPrice);					
			
			if(($sProductName != "") && ($iProductQty > 0))
				$this->AddQuotationItems($iQuotationId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount);		
			
		}
		
		$objSystemLog->AddLog("Update Quotation - " . $sQuotationTitle);
		$objGeneral->fnRedirect('../vendors/quotations.php?pagetype=details&error=10003&quotationid=' . $iQuotationId . '&id=' . $iVendorId);		
		
	}
	
	function DeleteQuotation($iQuotationId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[3] == 0) // Delete Disabled
		{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iQuotationId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}
			
		if ($objDatabase->DBCount("fms_vendors_purchaseorders AS PO", "PO.OrganizationId='" . cOrganizationId . "' AND PO.QuotationId= '$iQuotationId'") > 0) return(10009);
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_quotations AS Q WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.QuotationId='$iQuotationId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(10007);

		$sQuotationTitle = $objDatabase->Result($varResult, 0, "Q.QuotationTitle");
		$sQuotationTitle = $objDatabase->RealEscapeString($sQuotationTitle);
		
		$varResult = $objDatabase->Query("DELETE FROM fms_vendors_quotations WHERE QuotationId='$iQuotationId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("DELETE FROM fms_vendors_quotations_items WHERE QuotationId='$iQuotationId'");
						
			$objSystemLog->AddLog("Delete Quotation - " . $sQuotationTitle);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iQuotationId . '&error=10005');
			else
				$objGeneral->fnRedirect('?id=0&error=10005');
		}
		else
			return(10006);
	}
	
	function AddQuotationItems($iQuotationId, $sProductName, $iProductQty, $dProductUnitPrice, $dProductTotalAmount)
	{
		global $objDatabase;
		global $objGeneral;
		
		$sProductName = $objDatabase->RealEscapeString($sProductName);
		if($iProductQty == "" ) $iProductQty = 0;
		if($dProductUnitPrice == "" ) $dProductUnitPrice = 0;
		if($dProductTotalAmount == "" ) $dProductTotalAmount = 0;
		
		$varResult2 = $objDatabase->Query("INSERT INTO fms_vendors_quotations_items
		(QuotationId, ProductName, Quantity, UnitPrice, Amount)
		VALUES ('$iQuotationId', '$sProductName', '$iProductQty', '$dProductUnitPrice', '$dProductTotalAmount')");
		if ($objDatabase->AffectedRows($varResult2) > 0) return(true);
		else return(false);
		
		
	}
	
	// Quotations list against Purchase Request..
	
	function ShowAllQuotationsForPurchaseRequest($iPurchaseRequestId, $sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_purchaserequests AS PR WHERE PR.OrganizationId='" . cOrganizationId . "' AND PR.PurchaseRequestId='$iPurchaseRequestId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Purchase Request Id');
		$sPurchaseRequestNumber = $objDatabase->Result($varResult, 0, "PR.PurchaseRequestNumber");
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = 'Quotations List for Request No ' . $sPurchaseRequestNumber ;
		if ($sSortBy == "") $sSortBy = "Q.QuotationId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((Q.QuotationId LIKE '%$sSearch%') OR (Q.QuotationNumber LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%') OR (PR.PurchaseRequestNumber LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_vendors_quotations AS Q INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy INNER JOIN fms_vendors_purchaserequests AS PR ON PR.PurchaseRequestId = Q.PurchaseRequestId", "Q.PurchaseRequestId='$iPurchaseRequestId' $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?id=' .$iPurchaseRequestId. '&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors_quotations AS Q 
		INNER JOIN organization_employees AS E ON E.EmployeeId = Q.QuotationAddedBy
		INNER JOIN fms_vendors_purchaserequests AS PR ON PR.PurchaseRequestId = Q.PurchaseRequestId
		INNER JOIN fms_vendors AS V ON V.VendorId = Q.VendorId
		WHERE Q.OrganizationId='" . cOrganizationId . "' AND Q.PurchaseRequestId='$iPurchaseRequestId' $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">
		  <td align="left"><span class="WhiteHeading">Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Quotation Id in Ascending Order" title="Sort by Quotation Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Quotation Id in Descending Order" title="Sort by Quotation Id in Descending Order" border="0" /></a></span></td>
		  <td width="18%" align="left"><span class="WhiteHeading">Request No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=P.PurchaseOrderNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Purchase Order No in Ascending Order" title="Sort by Purchase Order No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=P.PurchaseOrderNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Purchase Order No in Descending Order" title="Sort by Purchase Order No in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Title&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationTitle&sortorder="><img src="../images/sort_up.gif" alt="Sort by Title in Ascending Order" title="Sort by Title in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationTitle&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Title in Descending Order" title="Sort by Title in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Quotation No&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationNo&sortorder="><img src="../images/sort_up.gif" alt="Sort by Quotation No in Ascending Order" title="Sort by Quotation No in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationNo&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Quotation No in Descending Order" title="Sort by Quotation No in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Vendor Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Name in Ascending Order" title="Sort by Vendor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Name in Descending Order" title="Sort by Vendor Name in Descending Order" border="0" /></a></span></td>
		  <td align="right"><span class="WhiteHeading">Total Amount&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.TotalAmount&sortorder="><img src="../images/sort_up.gif" alt="Sort by Total Amount in Ascending Order" title="Sort by Total Amount in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.TotalAmount&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Total Amount in Descending Order" title="Sort by Total Amount in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Quotation Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=Q.QuotationDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Quotation Date in Ascending Order" title="Sort by Quotation Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=Q.QuotationDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Quotation Date in Descending Order" title="Sort by Quotation Date in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="10"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iQuotationId = $objDatabase->Result($varResult, $i, "Q.QuotationId");
			
			$sQuotationTitle = $objDatabase->Result($varResult, $i, "Q.QuotationTitle");
			$sQuotationTitle = $objDatabase->RealEscapeString($sQuotationTitle);
			$sQuotationTitle = stripslashes($sQuotationTitle);
			
			$dTotalAmount = $objDatabase->Result($varResult, $i, "Q.TotalAmount");
			$sTotalAmount = number_format($dTotalAmount, 2);
			
			$sQuotationNo = $objDatabase->Result($varResult, $i, "Q.QuotationNumber");
			$sQuotationNo = $objDatabase->RealEscapeString($sQuotationNo);
			$sQuotationNo = stripslashes($sQuotationNo);
			
			$sPurchaseOrderNo = $objDatabase->Result($varResult, $i, "PR.PurchaseRequestNumber");
			$sPurchaseOrderNo = $objDatabase->RealEscapeString($sPurchaseOrderNo);
			$sPurchaseOrderNo = stripslashes($sPurchaseOrderNo);
			
			$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");		
			
			$dQuotationDate = $objDatabase->Result($varResult, $i, "Q.QuotationDate");
			$sQuotationDate = date("F j, Y", strtotime($dQuotationDate));
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[2] == 0) // Edit Disabled
				$sEditQuotation = '';
			
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Quotations[3] == 0) // Delete Disabled
				$sDeleteQuotation = '';
				
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td align="left" valign="top">' . $iQuotationId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sPurchaseOrderNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sQuotationTitle . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sQuotationNo . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sVendorName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="right" valign="top">' . $objEmployee->aSystemSettings['CurrencySign'] . ' ' . $sTotalAmount . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sQuotationDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sQuotationNo)) . '\', \'../vendors/quotations.php?pagetype=details&quotationid=' . $iQuotationId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quotation Details" title="View this Quotation Details"></a></td>
			</tr>';
		}
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>		 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Quotation:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" /><input type="hidden" name="id" id="id" value="'. $iPurchaseRequestId . '" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Quotation" title="Search for a Quotation" border="0"></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Quotation Details" alt="View this Quotation Details"></td><td>View this Quotation Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Quotation Details" alt="Edit this Quotation Details"></td><td>Edit this Quotation Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Quotation" alt="Delete this Quotation"></td><td>Delete this Quotation</td></tr>
		</table>';

		return($sReturn);
	}
}

?>