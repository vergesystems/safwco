<?php

class clsVendors_Vendors
{
	public $aVendorStatus;
    // Constructor
    function __construct()
    {
    	$this->aVendorStatus = array("Active", "Inactive");
    }

    function ShowAllVendors($sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = "Vendors";
        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((V.VendorId LIKE '%$sSearch%') OR (V.VendorName LIKE '%$sSearch%') OR (V.VendorCode LIKE '%$sSearch%') OR (V.PhoneNumber LIKE '%$sSearch%') OR (VT.TypeName LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%'))";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        if ($sSortBy == "") $sSortBy = "V.VendorId DESC";

		$iTotalRecords = $objDatabase->DBCount("fms_vendors AS V INNER JOIN fms_common_types AS VT ON VT.TypeId = V.VendorType INNER JOIN organization_employees AS E ON E.EmployeeId = V.VendorAddedBy", "V.OrganizationId='" . cOrganizationId . "' AND VT.ComponentName='Vendors_VendorTypes' $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors AS V
		INNER JOIN fms_common_types AS VT ON VT.TypeId = V.VendorType
		INNER JOIN organization_employees AS E ON E.EmployeeId = V.VendorAddedBy		
        WHERE V.OrganizationId='" . cOrganizationId . "' AND VT.ComponentName='Vendors_VendorTypes' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">		 
		 <td width="20%" align="left"><span class="WhiteHeading">Vendor Type&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=V.VendorType&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Type Name in Ascending Order" title="Sort by Vendor Type Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=V.VendorType&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Type Name in Descending Order" title="Sort by Vendor Type Name in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Vendor Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Name in Ascending Order" title="Sort by Vendor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=V.VendorName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Name in Descending Order" title="Sort by Vendor Name in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Vendor Code&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=V.VendorCode&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Code in Ascending Order" title="Sort by Vendor Code in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=V.VendorCode&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Code in Descending Order" title="Sort by Vendor Code in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Phone Number&nbsp;&nbsp;</span></td>
		 <td width="3%" colspan="4"><span class="WhiteHeading">Operations</span></td>
		</tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iVendorId = $objDatabase->Result($varResult, $i, "V.VendorId");
			$iVendorTypeId = $objDatabase->Result($varResult, $i, "V.VendorType");
			$sVendorTypeName = $objDatabase->Result($varResult, $i, "VT.TypeName");
			
			$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");
			$sVendorName = $objDatabase->RealEscapeString($sVendorName);
			$sVendorName = stripslashes($sVendorName);			
			
			$sVendorTypeName = $objDatabase->RealEscapeString(stripslashes($sVendorTypeName));
			
			$sVendorCode = $objDatabase->Result($varResult, $i, "V.VendorCode");
			$sVendorCode = $objDatabase->RealEscapeString(stripslashes($sVendorCode));
			
			$sPhoneNumber = $objDatabase->Result($varResult, $i, "V.PhoneNumber");
            
            $iStatus = $objDatabase->Result($varResult, $i, "V.Status");
            $sStatus = $this->aVendorStatus[$iStatus];
              
			$sVendorAddedOn = $objDatabase->Result($varResult, $i, "V.VendorAddedOn");

			$sEditVendor = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sVendorCode)) . '\', \'../vendors/vendors.php?pagetype=details&action2=edit&id=' . $iVendorId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Vendor Details" title="Edit this Vendor Details"></a></td>';
            $sDeleteVendor = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Vendor?\')) {window.location = \'?action=DeleteVendor&id=' . $iVendorId . '&show=' . $iShow . '&page=' . $iPage . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Vendor" title="Delete this Vendor"></a></td>';
			$sVendorsLedger = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Vendors Ledger ' . $objDatabase->RealEscapeString(stripslashes($sVendorCode)) . '\', \'../vendors/vendorsledger_showall.php?id=' . $iVendorId . '\', \'520px\', true);"><img src="../images/icons/iconContents.gif" border="0" alt="Vendors Ledger" title="Vendors Ledger"></a></td>';

       		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[2] == 0)	$sEditVendor = '<td class="GridTD">&nbsp;</td>';
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[3] == 0)	$sDeleteVendor = '<td class="GridTD">&nbsp;</td>';
							
            $sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			 
			 <td class="GridTD" align="left" valign="top">' . $sVendorTypeName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sVendorName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sVendorCode  . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <!--<td align="left" valign="top">' . $sStatus . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>-->
			 <td class="GridTD" align="left" valign="top">' . $sPhoneNumber . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sVendorName)) . '\', \'../vendors/vendors.php?pagetype=details&id=' . $iVendorId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Vendor Details" title="View this Vendor Details"></a></td>
			 ' . $sEditVendor . '
			 ' . $sVendorsLedger . '
			 ' . $sDeleteVendor . '
			</tr>';
		}

		$sAddNewVendor = '<td width="10%" align="left"><input onclick="window.top.CreateTab(\'tabContainer\', \'Add Vendor\', \'../vendors/vendors.php?pagetype=details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add New Vendor"></td>';

		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[1] == 0) // Add Disabled
			$sAddNewVendor = '';

		$sReturn .= '</table>

		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  ' . $sAddNewVendor . '
          <td align="left" colspan="2"><form method="GET" action="">Search for a Vendor:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Vendor" title="Search for a Vendor" border="0"></form></td>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
        <br /><br />
        <table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
         <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Vendor Details" alt="View this Vendor Details"></td><td>View this Vendor Details</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Vendor Details" alt="Edit this Vendor Details"></td><td>Edit this Vendor Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconContents.gif" title="Vendors Ledger" alt="Vendors Ledger"></td><td>Vendors Ledger</td></tr>
         <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Vendor" alt="Delete this Vendor"></td><td>Delete this Vendor</td></tr>
        </table>';

		return($sReturn);
    }

    function VendorDetails($iVendorId, $sAction2)
    {
    	global $objDatabase;
    	global $objGeneral;
    	global $objEmployee;
		
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();

    	$varResult = $objDatabase->Query("
    	SELECT * FROM fms_vendors AS V
    	INNER JOIN fms_common_types AS VT ON VT.TypeId = V.VendorType
		INNER JOIN organization_employees AS E ON E.EmployeeId = V.VendorAddedBy		
        WHERE V.OrganizationId='" . cOrganizationId . "' AND VT.ComponentName='Vendors_VendorTypes' AND V.VendorId = '$iVendorId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=details&action2=edit&id=' . $iVendorId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Vendor" title="Edit this Vendor" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Vendor?\')) {window.location = \'?pagetype=details&action=DeleteVendor&id=' . $iVendorId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Vendor" title="Delete this Vendor" /></a>';

    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[2] == 0)	$sButtons_Edit = '';
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[3] == 0)	$sButtons_Delete = '';

		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Vendor Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{
		    	$iVendorId = $objDatabase->Result($varResult, 0, "V.VendorId");
		        $iVendorTypeId = $objDatabase->Result($varResult, 0, "V.VendorType");
		        $iVendorAddedBy = $objDatabase->Result($varResult, 0, "V.VendorAddedBy");
						    	
		    	$sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");
		        $sVendorName = $objDatabase->RealEscapeString($sVendorName);
		        $sVendorName = stripslashes($sVendorName);
		        
		        $sVendorCode = $objDatabase->Result($varResult, 0, "V.VendorCode");
				$sVendorCode = $objDatabase->RealEscapeString($sVendorCode);
				$sVendorCode = stripslashes($sVendorCode);
		        // Vendor Address
		        $sAddress = $objDatabase->Result($varResult, 0, "V.Address");
		        $sAddress = $objDatabase->RealEscapeString($sAddress);
		        $sAddress = stripslashes($sAddress);
		        
		        $sVendorTypeName = $objDatabase->Result($varResult, $i, "VT.TypeName");
				$sVendorTypeName = $objDatabase->RealEscapeString($sVendorTypeName);
				$sVendorTypeName = stripslashes($sVendorTypeName);
						        
				$sVendorAddedBy = $objDatabase->Result($varResult, $i, "E.FirstName") . ' ' . $objDatabase->Result($varResult, $i, "E.LastName");
				$sVendorAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sVendorAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iVendorAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
		        $sCity = $objDatabase->Result($varResult, 0, "V.City");
		        $sCity = $objDatabase->RealEscapeString($sCity);
		        $sCity = stripslashes($sCity);		        
		        
		        $sState = $objDatabase->Result($varResult, 0, "V.State");
		        $sState = $objDatabase->RealEscapeString($sState);
		        $sState = stripslashes($sState);
		        
		        $sZipCode = $objDatabase->Result($varResult, 0, "V.ZipCode");
		        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
		        $sZipCode = stripslashes($sZipCode);
		        
		        $sCountry = $objDatabase->Result($varResult, 0, "V.Country");
				$sCountry = $objDatabase->RealEscapeString($sCountry);
				$sCountry = stripslashes($sCountry);
				
				$sOfficeManager = $objDatabase->Result($varResult, 0, "V.OfficeManager");
				$sOfficeManager = $objDatabase->RealEscapeString($sOfficeManager);
				$sOfficeManager = stripslashes($sOfficeManager);
				
				$sAccountRepresentative = $objDatabase->Result($varResult, 0, "V.AccountRepresentative");
				$sAccountRepresentative = $objDatabase->RealEscapeString($sAccountRepresentative);
				$sAccountRepresentative = stripslashes($sAccountRepresentative);
				
				$sSpecialNote = $objDatabase->Result($varResult, 0, "V.SpecialNote");
				$sSpecialNote = $objDatabase->RealEscapeString($sSpecialNote);
				$sSpecialNote = stripslashes($sSpecialNote);
				
		        $sPhoneNumber = $objDatabase->Result($varResult, 0, "V.PhoneNumber");
				$sEmailAddress = $objDatabase->Result($varResult, 0, "V.EmailAddress");
				$sNTNNumber = $objDatabase->Result($varResult, 0, "V.NTNNumber");
				$sCNICNumber = $objDatabase->Result($varResult, 0, "V.CNICNumber");
				$sNotes = $objDatabase->Result($varResult, 0, "V.Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
				
		        $sVendorDescription= $objDatabase->Result($varResult, 0, "V.VendorDescription");
				$sVendorDescription = $objDatabase->RealEscapeString($sVendorDescription);
				$sVendorDescription = stripslashes($sVendorDescription);
				$iStatus = $objDatabase->Result($varResult, 0, "V.Status");
				$sStatus = $this->aVendorStatus[$iStatus];
				
				$dVendorAddedOn = $objDatabase->Result($varResult, 0, "V.VendorAddedOn");
				$sVendorAddedOn = date("F j, Y", strtotime($dVendorAddedOn)) . ' at ' . date("g:i a", strtotime($dVendorAddedOn));
				
				$sVendorPhoto = '<img src="' . $this->FindVendorPhoto($iVendorId) . '" border="0" alt="' . $sVendorName . '" title="' . $sVendorName . '" />';
		    }

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();" enctype="multipart/form-data">';
				
				$sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';

		        $sStatus = '<select name="selStatus" id="selStatus" class="form1">';		        			
		        for($i = 0; $i < count($this->aVendorStatus); $i++)
				{
					$sStatus .= '<option ' . (($iStatus == $i) ? 'selected="true"' : '') . ' value="' . $i . '">' . $this->aVendorStatus[$i];
				}
				$sStatus .='</select>';
		        
				$sVendorPhotoUpload = '<tr bgcolor="#edeff1"><td>Vendor Photo:</td><td><input class="form1" type="file" name="fileField1" accept="text/*" size="30" maxlength="' . cUploadFileMaxLimit . '"></td></tr>';
				
				$sVendorTypeName = '<select class="form1" name="selVendorType" id="selVendorType">';
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_common_types AS VT WHERE VT.OrganizationId='" . cOrganizationId . "' AND VT.ComponentName='Vendors_VendorTypes'");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sVendorTypeName .= '<option ' . (($iStationType == $objDatabase->Result($varResult2, $i, "VT.TypeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "VT.TypeId") . '">' . $objDatabase->Result($varResult2, $i, "VT.TypeName") . '</option>';
				$sVendorTypeName .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/types.php?componentname=Vendors_VendorTypes\', \'Vendor Types\', \'700 420\');"><img src="../images/icons/plus.gif" align="center" border="0" alt="Manage Vendor Types" title="Manage Vendor Types" /></a>';

				$sVendorName = '<input type="text" name="txtVendorName" id="txtVendorName" class="form1" value="' . $sVendorName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sVendorCode = '<input type="text" name="txtVendorCode" id="txtVendorCode" class="form1" value="' . $sVendorCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sOfficeManager = '<input type="text" name="txtOfficeManager" id="txtOfficeManager" class="form1" value="' . $sOfficeManager . '" size="10" />';
				$sAccountRepresentative = '<input type="text" name="txtAccountRepresentative" id="txtAccountRepresentative" class="form1" value="' . $sAccountRepresentative . '" size="10" />';
				$sSpecialNote = '<input type="text" name="txtSpecialNote" id="txtSpecialNote" class="form1" value="' . $sSpecialNote . '" size="30" />';
				
				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />';
				$sEmailAddress = '<input type="text" name="txtEmailAddress" id="txtEmailAddress" class="form1" value="' . $sEmailAddress . '" size="30" />';
				$sNTNNumber = '<input type="text" name="txtNTNNumber" id="txtNTNNumber" class="form1" value="' . $sNTNNumber . '" size="30" />';
				$sCNICNumber = '<input type="text" name="txtCNICNumber" id="txtCNICNumber" class="form1" value="' . $sCNICNumber . '" size="30" />';
				$sCountry = $sCountryString;
				$sVendorDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtVendorDescription", $sVendorDescription, 10, 80, "100%", "200px", "Basic");
			}
			else if ($sAction2 == "addnew")
			{
				$iVendorId = "";
		        $sVendorName = $objGeneral->fnGet("txtVendorName");
		        $sAddress = $objGeneral->fnGet("txtAddress");
		        $sCity = $objGeneral->fnGet("txtCity");
		        $sZipCode = $objGeneral->fnGet("txtZipCode");
		        $sState = $objGeneral->fnGet("txtState");
		        $sCountry = $objGeneral->fnGet("txtCountry");
		        $sVendorCode = $objGeneral->fnGet("txtVendorCode");
		        $sNotes = $objGeneral->fnGet("txtNotes");

		        $sPhoneNumber = $objGeneral->fnGet("txtPhoneNumber");
		        $sVendorDescription = $objGeneral->fnGet("txtVendorDescription");
		        $sEmailAddress = $objGeneral->fnGet("txtEmailAddress");
				$sNTNNumber = $objGeneral->fnGet("txtNTNNumber");
				$sCNICNumber = $objGeneral->fnGet("txtCNICNumber");
				
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();" enctype="multipart/form-data">';

		        $sCountryString = '<select name="selCountry" id="selCountry" size="1" class="form1">';
		        if ($sCountry == '')
		        	$sCountryString .= $objGeneral->fnCountryOptionsList();
		        else
			        $sCountryString .= $objGeneral->fnCountryOptionsList($sCountry);
		        $sCountryString .= '</select>';
				
		        $sStatus = '<select name="selStatus" id="selStatus" class="form1">';		        			
		        for($i = 0; $i < count($this->aVendorStatus); $i++)
				{
					$sStatus .= '<option value="' . $i . '">' . $this->aVendorStatus[$i];
				}
				$sStatus .='</select>';		        
		        
				$sVendorPhoto = '<img src="../images/vendors/NoImage.jpg" alt="No Vendor Photo" title="No Vendor Photo" border="0" />';				
				$sVendorPhotoUpload = '<tr bgcolor="#edeff1"><td>Vendor Photo:</td><td><input class="form1" type="file" name="fileField1" accept="text/*" size="30" maxlength="' . cUploadFileMaxLimit . '"></td></tr>';
				
                $sVendorTypeName = '<select class="form1" name="selVendorType" id="selVendorType">';
				$varResult2 = $objDatabase->Query("SELECT * FROM fms_common_types AS VT WHERE VT.OrganizationId='" . cOrganizationId . "' AND VT.ComponentName='Vendors_VendorTypes'");
				for ($i=0; $i < $objDatabase->RowsNumber($varResult2); $i++)
					$sVendorTypeName .= '<option ' . (($iStationType == $objDatabase->Result($varResult2, $i, "VT.TypeId")) ? 'selected="true"' : '') . ' value="' . $objDatabase->Result($varResult2, $i, "VT.TypeId") . '">' . $objDatabase->Result($varResult2, $i, "VT.TypeName") . '</option>';
				$sVendorTypeName .= '</select>&nbsp;&nbsp;<a href="#noanchor" onclick="window.top.MOOdalBox.open(\'../common/types.php?componentname=Vendors_VendorTypes\', \'Vendor Types\', \'700 420\');"><img src="../images/icons/plus.gif" align="center" border="0" alt="Manage Vendor Types" title="Manage Vendor Types" /></a>';
				
                $sVendorAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
                $sVendorAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sVendorAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
                
                $sVendorName = '<input type="text" name="txtVendorName" id="txtVendorName" class="form1" value="' . $sVendorName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
                $sAddress = '<input type="text" name="txtAddress" id="txtAddress" class="form1" value="' . $sAddress . '" size="30" />';
				$sCity = '<input type="text" name="txtCity" id="txtCity" class="form1" value="' . $sCity . '" size="30" />';
				$sState = '<input type="text" name="txtState" id="txtState" class="form1" value="' . $sState . '" size="30" />';
				$sZipCode = '<input type="text" name="txtZipCode" id="txtZipCode" class="form1" value="' . $sZipCode . '" size="30" />';
				$sVendorCode = '<input type="text" name="txtVendorCode" id="txtVendorCode" class="form1" value="' . $sVendorCode . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				
				$sOfficeManager = '<input type="text" name="txtOfficeManager" id="txtOfficeManager" class="form1" value="' . $sOfficeManager . '" size="10" />';
				$sAccountRepresentative = '<input type="text" name="txtAccountRepresentative" id="txtAccountRepresentative" class="form1" value="' . $sAccountRepresentative . '" size="10" />';
				$sSpecialNote = '<input type="text" name="txtSpecialNote" id="txtSpecialNote" class="form1" value="' . $sSpecialNote . '" size="30" />';
				
				$sPhoneNumber = '<input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form1" value="' . $sPhoneNumber . '" size="30" />';
				$sEmailAddress = '<input type="text" name="txtEmailAddress" id="txtEmailAddress" class="form1" value="' . $sEmailAddress . '" size="30" />';
				$sNTNNumber = '<input type="text" name="txtNTNNumber" id="txtNTNNumber" class="form1" value="' . $sNTNNumber . '" size="30" />';
				$sCNICNumber = '<input type="text" name="txtCNICNumber" id="txtCNICNumber" class="form1" value="' . $sCNICNumber . '" size="30" />';
				$sCountry = $sCountryString;
				$sVendorDescription = $objDHTMLSuite->CKEditor($objEmployee->iEmployeeId, "txtVendorDescription", $sVendorDescription, 10, 80, "100%", "200px", "Basic");				
				//$sVendorDescription = '<textarea name="txtVendorDescription" id="txtVendorDescription" class="form1" rows="5" cols="60">' . $sVendorDescription . '</textarea>';
			   	$sVendorAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}		

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
		    function ValidateForm()
		    {
				if (GetVal(\'txtVendorName\') == "") return(AlertFocus(\'Please enter a valid Vendor Name\', \'txtVendorName\'));
				if (GetVal(\'txtVendorCode\') == "") return(AlertFocus(\'Please enter a valid Vendor Code\', \'txtVendorCode\'));

				return true;
			}
		    </script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
		     <!--<tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Test Grid:</span></td></tr>
			 <tr bgcolor="#edeff1"><td colspan="2">' . $sGrid . '</td></tr>-->
		     <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Vendor Information:</span></td></tr>
			 <tr bgcolor="#edeff1">
			  <td width="20%">Vednor Type:</td><td><strong>' . $sVendorTypeName . '</strong></td>
			  <td rowspan="6" colspan="2" valign="top">
			   <table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			    <tr>
			     <td align="right" valign="top">' . $sVendorPhoto . '</td>
			    </tr>
			   </table>
			  </td>
			 </tr>			 
			 <tr  bgcolor="#edeff1"><td>Vendor Name:</td><td><strong>' . $sVendorName . '</strong></td></tr>
			 <tr><td>Vendor Code:</td><td><strong>' . $sVendorCode . '</strong></td></tr>
			 ' . $sVendorPhotoUpload . '
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Vendor Address:</span></td></tr>
			 <tr><td>Address:</td><td><strong>' . $sAddress . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>City:</td><td colspan="3"><strong>' . $sCity . '</strong></td></tr>
			 <tr><td>State/Province:</td><td colspan="3"><strong>' . $sState . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Zip Code:</td><td colspan="3"><strong>' . $sZipCode . '</strong></td></tr>
			 <tr><td>Country:</td><td colspan="3"><strong>' . $sCountry . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Phone Number:</td><td colspan="3"><strong>' . $sPhoneNumber . '</strong></td></tr>
			 <tr><td>Email Address:</td><td colspan="3"><strong>' . $sEmailAddress . '</strong></td></tr> 
			 <tr bgcolor="#edeff1"><td>NTN Number:</td><td colspan="3"><strong>' . $sNTNNumber . '</strong></td></tr>
			 <tr><td>CNIC Number:</td><td colspan="3"><strong>' . $sCNICNumber . '</strong></td></tr> 
			 <tr bgcolor="#edeff1"><td>Status:</td><td colspan="3"><strong>' . $sStatus . '</strong></td></tr>			 
			 <tr><td valign="top" colspan="2">Description:</td></tr>
			 <tr><td valign="top" colspan="4"><strong>' . $sVendorDescription . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Customizable Fields:</span></td></tr>
			 <tr><td valign="top">Office Manager:</td><td colspan="3"><strong>' . $sOfficeManager . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Account Representative:</td><td colspan="3"><strong>' . $sAccountRepresentative . '</strong></td></tr>
			 <tr><td valign="top">Special Note:</td><td colspan="3"><strong>' . $sSpecialNote . '</strong></td></tr>
			 <tr class="Details_Title_TR"><td colspan="4"><span class="Details_Title">Extra Information:</span></td></tr>
		     <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td colspan="3"><strong>' . $sNotes . '</strong></td></tr>
			 <tr><td>Vendor Added On:</td><td colspan="3"><strong>' . $sVendorAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td>Vendor Added By:</td><td colspan="3"><strong>' . $sVendorAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update Vendor" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iVendorId . '"><input type="hidden" name="action" id="action" value="UpdateVendor"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Vendor" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewVendor"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';

		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

    	return($sReturn);
    }
	
	function FindVendorPhoto($iVendorId)
	{
		$sPhotoFolder = cDataFolder . '/fms/vendorphotos/';

    	if (file_exists($sPhotoFolder . $iVendorId . '.jpg'))
    		return($sPhotoFolder . $iVendorId . '.jpg');
    	else if (file_exists($sPhotoFolder . $iVendorId . '.gif'))
    		return($sPhotoFolder . $iVendorId . '.gif');
    	else
    		return('../images/vendors/NoImage.jpg');
	}
	
    function UpdateVendor($iVendorId, $iVendorType, $sVendorName, $sVendorCode, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sEmailAddress, $sNTNNumber, $sCNICNumber, $sOfficeManager, $sAccountRepresentative, $sSpecialNote, $sNotes, $sVendorDescription, $iStatus)
    {
        global $objDatabase;        
        global $objEmployee;
        global $objGeneral;
		global $objSystemLog;
		
        // Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[2] == 0) // Update Disabled
			return(1010);
		$sVendorCode = $objDatabase->RealEscapeString($sVendorCode);
		
	    // Vendor Code already added in the System
	  	if ($objDatabase->DBCount("fms_vendors AS V", "V.OrganizationId='" . cOrganizationId . "' AND V.VendorId <> '$iVendorId' AND V.VendorCode='$sVendorCode'") > 0) return(5207);

        $sVendorName = $objDatabase->RealEscapeString($sVendorName);        
        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
        $sCountry = $objDatabase->RealEscapeString($sCountry);
		$sPhoneNumber = $objDatabase->RealEscapeString($sPhoneNumber);
		$sEmailAddress = $objDatabase->RealEscapeString($sEmailAddress);
		$sNTNNumber = $objDatabase->RealEscapeString($sNTNNumber);
		$sCNICNumber = $objDatabase->RealEscapeString($sCNICNumber);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
		$sOfficeManager = $objDatabase->RealEscapeString($sOfficeManager);
        $sAccountRepresentative = $objDatabase->RealEscapeString($sAccountRepresentative);
        $sSpecialNote = $objDatabase->RealEscapeString($sSpecialNote);
      
        $sVendorDescription = $objDatabase->RealEscapeString($sVendorDescription);

        $varResult = $objDatabase->Query("
        UPDATE fms_vendors
        SET
        	VendorType = '$iVendorType',
	        VendorName='$sVendorName',
	        VendorCode='$sVendorCode',
	        Address='$sAddress',
	        City='$sCity',
	        State='$sState',
	        ZipCode='$sZipCode',
	        Country='$sCountry',
	        PhoneNumber='$sPhoneNumber',
			EmailAddress='$sEmailAddress',
			NTNNumber='$sNTNNumber',
			CNICNumber='$sCNICNumber',
	        Notes='$sNotes',
	        VendorDescription='$sVendorDescription',
	        Status='$iStatus',
	        OfficeManager='$sOfficeManager', 
	        AccountRepresentative='$sAccountRepresentative', 
	        SpecialNote='$sSpecialNote'
        WHERE OrganizationId='" . cOrganizationId . "' AND VendorId='$iVendorId'");
		
		// Update Vendor Photo
   		include(cVSFFolder . '/classes/clsUploader.php');
		$up = new clsUploader();
		$sUploadPath = cDataFolder . '/fms/vendorphotos/';
		$sAllowedTypes = array("image/jpeg", "image/pjpeg", "image/gif");
		$varPath = $objGeneral->dumpAssociativeArray($up->uploadTo($sUploadPath, true, $sAllowedTypes));
		if ($varPath != '')
		{
			$sFileName = explode('/', $varPath);
			$sFileName = $sFileName[count($sFileName)-1];
			$sExt = explode('.', $sFileName);
			$sExt = strtolower($sExt[count($sExt)-1]);

			$sNewFileName = $sUploadPath . $iVendorId . '.' . $sExt;
			if (file_exists($sNewFileName)) unlink($sNewFileName);

			if (!rename($varPath, $sNewFileName))
    			return(4001);

    		// Make Thumbnail
    		$iThumbnailHeight = $objEmployee->aSystemSettings["FMS_Vendors_VendorPhoto_Height"];
			$iThumbnailWidth = $objEmployee->aSystemSettings["FMS_Vendors_VendorPhoto_Width"];

			include(cVSFFolder . '/classes/clsThumbnails.php');
			$objThumbnail = new thumbs($sUploadPath);
			$objThumbnail->fnCreateThumbnail($sUploadPath, $sUploadPath, $iVendorId . '.' . strtolower($sExt), $iThumbnailWidth, $iThumbnailHeight, $iVendorId . '.' . strtolower($sExt));
		}
		
  		$objSystemLog->AddLog("Update Vendor - " . $sVendorName);			
  		$objGeneral->fnRedirect('../vendors/vendors.php?pagetype=details&error=5203&id=' . $iVendorId);
		
        //else
            //return(5204);
    }

    function DeleteVendor($iVendorId)
    {
        global $objDatabase;
		global $objEmployee;
		global $objSystemLog;
		
        // Employee Roles
	   	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[3] == 0) // Delete Disabled
	   	{
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iVendorId . '&error=1010');
			else
				$objGeneral->fnRedirect('?id=0&error=1010');
		}		
			
		if ($objDatabase->DBCount("fms_accounts_generaljournal AS GJ", "GJ.OrganizationId='" . cOrganizationId . "' AND GJ.VendorId= '$iVendorId'  AND GJ.IsDeleted ='0' ") > 0) return(5208);
		
       	$varResult = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "' AND V.VendorId='$iVendorId'");
		if ($objDatabase->RowsNumber($varResult) <= 0) die('Sorry, Invalid Vendor Id');
		$sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");


        $varResult = $objDatabase->Query("DELETE FROM fms_vendors WHERE VendorId='$iVendorId'");
        if ($objDatabase->AffectedRows($varResult) > 0)
        {        	
  			$objSystemLog->AddLog("Delete Vendor - " . $sVendorName);
			
			if ($objGeneral->fnGet("pagetype") == "details")
				$objGeneral->fnRedirect('?pagetype=details&id=' . $iVendorId . '&error=5205');
			else
				$objGeneral->fnRedirect('?id=0&error=5205');
        }
        else
            return(5206);
    }

    function AddNewVendor($iVendorType, $sVendorName, $sVendorCode, $sAddress, $sCity, $sState, $sZipCode, $sCountry, $sPhoneNumber, $sEmailAddress, $sNTNNumber, $sCNICNumber, $sOfficeManager, $sAccountRepresentative, $sSpecialNote, $sNotes, $sDescription, $iStatus)
    {
        global $objDatabase;
        global $objGeneral;
        global $objEmployee;
		global $objSystemLog;
        
        $varNow = $objGeneral->fnNow();
        $iVendorAddedBy = $objEmployee->iEmployeeId;

        // Employee Roles
   		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[1] == 0) // Add Disabled
			return(1010);

		$sVendorCode = $objDatabase->RealEscapeString($sVendorCode);
		
   		// Vendor Code already added in the System
	  	if ($objDatabase->DBCount("fms_vendors AS V", "V.OrganizationId='" . cOrganizationId . "' AND V.VendorCode = '$sVendorCode' ") > 0) return(5207);

        $sVendorName = $objDatabase->RealEscapeString($sVendorName);
        
        $sAddress = $objDatabase->RealEscapeString($sAddress);
        $sCity = $objDatabase->RealEscapeString($sCity);
        $sState = $objDatabase->RealEscapeString($sState);
        $sZipCode = $objDatabase->RealEscapeString($sZipCode);
        $sCountry = $objDatabase->RealEscapeString($sCountry);
		$sPhoneNumber = $objDatabase->RealEscapeString($sPhoneNumber);
		$sEmailAddress = $objDatabase->RealEscapeString($sEmailAddress);
		$sNTNNumber = $objDatabase->RealEscapeString($sNTNNumber);
		$sCNICNumber = $objDatabase->RealEscapeString($sCNICNumber);
        $sOfficeManager = $objDatabase->RealEscapeString($sOfficeManager);
        $sAccountRepresentative = $objDatabase->RealEscapeString($sAccountRepresentative);
        $sSpecialNote = $objDatabase->RealEscapeString($sSpecialNote);
        $sNotes = $objDatabase->RealEscapeString($sNotes);
        $sDescription = $objDatabase->RealEscapeString($sDescription);

        $varResult = $objDatabase->Query("INSERT INTO fms_vendors
        (OrganizationId, VendorAddedBy, VendorType, VendorName, VendorCode, Address, City, State, ZipCode, Country, PhoneNumber, EmailAddress, NTNNumber, CNICNumber, Notes, VendorDescription, Status, OfficeManager, AccountRepresentative, SpecialNote, VendorAddedOn)
        VALUES
        ('" . cOrganizationId . "', '$iVendorAddedBy', '$iVendorType', '$sVendorName', '$sVendorCode', '$sAddress', '$sCity', '$sState', '$sZipCode', '$sCountry', '$sPhoneNumber', '$sEmailAddress', '$sNTNNumber', '$sCNICNumber', '$sNotes', '$sDescription', '$iStatus', '$sOfficeManager', '$sAccountRepresentative', '$sSpecialNote', '$varNow')");

        $varResult = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "' AND V.VendorName='$sVendorName' AND V.VendorAddedOn='$varNow'");
        if ($objDatabase->RowsNumber($varResult) > 0)
        {
			$iVendorId = $objDatabase->Result($varResult, 0, "V.VendorId");
			
			// Add Vendor Photo
	       	include(cVSFFolder . '/classes/clsUploader.php');
			$up = new clsUploader();
			$sUploadPath = cDataFolder . '/fms/vendorphotos/';
			$sAllowedTypes = array("image/jpeg", "image/pjpeg", "image/gif");
			$varPath = $objGeneral->dumpAssociativeArray($up->uploadTo($sUploadPath, true, $sAllowedTypes));
			if ($varPath != '')
			{
				$sFileName = explode('/', $varPath);
	   			$sFileName = $sFileName[count($sFileName)-1];
				$sExt = explode('.', $sFileName);
				$sExt = strtolower($sExt[count($sExt)-1]);

				$sNewFileName = $sUploadPath . $iVendorId . '.' . $sExt;
				if (file_exists($sNewFileName)) unlink($sNewFileName);

	    		if (!rename($varPath, $sNewFileName))
	        		return(4001);

	        	// Make Thumbnail
		    	$iThumbnailHeight = $objEmployee->aSystemSettings["FMS_Vendors_VendorPhoto_Height"];
	    		$iThumbnailWidth = $objEmployee->aSystemSettings["FMS_Vendors_VendorPhoto_Width"];

				include(cVSFFolder . '/classes/clsThumbnails.php');
		        $objThumbnail = new thumbs($sUploadPath);
		        $objThumbnail->fnCreateThumbnail($sUploadPath, $sUploadPath, $iVendorId . '.' . strtolower($sExt), $iThumbnailWidth, $iThumbnailHeight, $iVendorId . '.' . strtolower($sExt));
		    }
        	
  			$objSystemLog->AddLog("Add New Vendor - " . $sVendorName);
			//return (5201);
            $objGeneral->fnRedirect('../vendors/vendors.php?pagetype=details&error=5201&id=' . $objDatabase->Result($varResult, 0, "V.VendorId"));
        }
        else
            return(5202);
    }

	// Vendors Ledger
	function ShowAllVendorsLedger($iVendorId, $sSearch)
    {
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_Vendors[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors AS V
		WHERE V.OrganizationId='" . cOrganizationId . "' AND V.VendorId = '$iVendorId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return('<br /><br /><div align="center">Sorry, No records found...</div><br /><br />');
		$sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");

        $sSortBy = $objGeneral->fnGet("sortby");
        $sSortOrder = $objGeneral->fnGet("sortorder");
        $iShow = $objGeneral->fnGet("show");
        $iPage = $objGeneral->fnGet("page");
   		if ($iPage == '') $iPage = 1;

   		$sTitle = 'Ledger Details Of ' . $sVendorName;

        if ($sSearch != "")
        {
        	$sSearch = $objDatabase->RealEscapeString($sSearch);
            $sSearchCondition = " AND ((GJ.GeneralJournalId LIKE '%$sSearch%') OR (GJ.TransactionDate LIKE '%$sSearch%') OR (GJE.Detail LIKE '%$sSearch%') OR (GJE.Debit LIKE '%$sSearch%') OR (GJE.Credit LIKE '%$sSearch%'))";
            $sSearch = stripslashes($sSearch);
            $sTitle = 'Search Results for "' . $sSearch . '"';
        }

        if ($sSortBy == "") $sSortBy = "V.VendorName";

		$iTotalRecords = $objDatabase->DBCount("fms_vendors AS V INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.VendorId = V.VendorId INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId", "V.OrganizationId='" . cOrganizationId . "' AND V.VendorId = '$iVendorId'  AND GJ.IsDeleted ='0' $sSearchCondition");

		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?id='. $iVendorId . '&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors AS V
		INNER JOIN fms_accounts_generaljournal_entries AS GJE ON GJE.VendorId = V.VendorId
		INNER JOIN fms_accounts_generaljournal AS GJ ON GJ.GeneralJournalId = GJE.GeneralJournalId
		INNER JOIN fms_accounts_chartofaccounts AS CA ON CA.ChartOfAccountsId = GJE.ChartOfAccountsId
        WHERE V.OrganizationId='" . cOrganizationId . "' AND V.VendorId = '$iVendorId' AND CA.ChartOfAccountsCategoryId !='5'  AND GJ.IsDeleted ='0' $sSearchCondition
        ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		<tr class="GridTR">
		 <td width = "15%" align="left"><span class="WhiteHeading">Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.GeneralJournalId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Id in Ascending Order" title="Sort by Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.GeneralJournalId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Id in Descending Order" title="Sort by Id in Descending Order" border="0" /></a></span></td>
         <td width = "15%" align="left"><span class="WhiteHeading">Reference &nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.Reference&sortorder="><img src="../images/sort_up.gif" alt="Sort by Reference in Ascending Order" title="Sort by Reference in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.GeneralJournalId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Reference Number in Descending Order" title="Sort by Reference Number in Descending Order" border="0" /></a></span></td>
		 <td width="20%" align="left"><span class="WhiteHeading">Transaction Date&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder="><img src="../images/sort_up.gif" alt="Sort by Transaction Date in Ascending Order" title="Sort by Transaction Date in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJ.TransactionDate&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Transaction Date in Descending Order" title="Sort by Transaction Date in Descending Order" border="0" /></a></span></td>
		 <td width="30%" align="left"><span class="WhiteHeading">Detail&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Detail&sortorder="><img src="../images/sort_up.gif" alt="Sort by Detail in Ascending Order" title="Sort by Vendor Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Detail&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Detail in Descending Order" title="Sort by Detail in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Debit&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Debit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Debit in Ascending Order" title="Sort by Debit in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Debit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Debit in Descending Order" title="Sort by Debit in Descending Order" border="0" /></a></span></td>
		 <td width="15%" align="left"><span class="WhiteHeading">Credit&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=GJE.Credit&sortorder="><img src="../images/sort_up.gif" alt="Sort by Credit in Ascending Order" title="Sort by Debit in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=GJE.Credit&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Credit in Descending Order" title="Sort by Credit in Descending Order" border="0" /></a></span></td>
         <td width="2%" colspan="2"><span class="WhiteHeading">Operations</span></td>
		</tr>
		';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';

			$iVendorId = $objDatabase->Result($varResult, $i, "V.VendorId");
			$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
			$dTransactionDate = $objDatabase->Result($varResult, $i, "GJ.TransactionDate");
			$sDetail = $objDatabase->Result($varResult, $i, "GJE.Detail");
            $sReference = $objDatabase->Result($varResult, $i, "GJ.Reference");
			$sTransactionDate = date("F j, Y", strtotime($dTransactionDate));
			$dDebit = $objDatabase->Result($varResult, $i, "GJE.Debit");
			$dCredit = $objDatabase->Result($varResult, $i, "GJE.Credit");

			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">
			 <td align="left" valign="top">' . $iGeneralJournalId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
             <td align="left" valign="top">' . $sReference . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sTransactionDate . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . $sDetail . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . number_format($dDebit, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td align="left" valign="top">' . number_format($dCredit, 0) . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
             <td class="GridTD" align="center">
              <a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Transaction Details\', \'../accounts/generaljournal.php?pagetype=details&id=' . $iGeneralJournalId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Transaction Details" title="View this Transaction Details"></a>
              <a href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&txtId=' . $iGeneralJournalId . '\', 800,600);" style="font-family: Verdana, Arial; font-size:12px;"><img border="0" src = "../images/icons/iconPrint2.gif" tilte="Print Receipt" alt="Print Receipt" /></a>
             </td>
			</tr>';
		}

		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
          <form method="GET" action=""><td align="left" colspan="2">Search for a Transaction:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" /><input type="hidden" name="id" id="id" value="' . $iVendorId . '" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Vendor" title="Search for a Vendor" border="0"></td> </form>
          <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>';

		return($sReturn);
    }

	//Vendors List
	function VendorsList()
	{
		global $objDatabase;

		$aVendors = array();
		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "'");
		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sVendorName = $objDatabase->Result($varResult, $i, "V.VendorName");
			$sVendorCode = $objDatabase->Result($varResult, $i, "V.VendorCode");
			$iVendorId = $objDatabase->Result($varResult, $i, "V.VendorId");			
			
			$aVendors[$i][0] = $iVendorId;
			$aVendors[$i][1] = $sVendorCode . ' - ' . $sVendorName;
			$aVendors[$i][2] = $sVendorCode;
		}
		
		return($aVendors);
	} 
	
	function ShowAdvancedSearch($sWidth = "70%", $iFormId)
    {
    	global $objDatabase;
		    			
		$dVendorAddeDateTime = date("Y-m-d", mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-15));
		$dVendorAddeDateTime2 = date("Y-m-d");
		
	    // Calendar Control
		include(cVSFFolder . '/classes/clsDHTMLSuite.php');
		$objDHTMLSuite = new clsDHTMLSuite();
		
		include(cVSFFolder . '/classes/clsjQuery.php');
		$objjQuery = new clsjQuery();
		
		$sVendorAddeDateTime = '<input size="10" type="text" id="txtVendorAddedDate" name="txtVendorAddedDate" class="form1" value="' . $dVendorAddeDateTime . '" />' . $objjQuery->Calendar('txtVendorAddedDate');
		$sVendorAddeDateTime2 = '<input size="10" type="text" id="txtVendorAddedDate2" name="txtVendorAddedDate2" class="form1" value="' . $dVendorAddeDateTime2 . '" />' . $objjQuery->Calendar('txtVendorAddedDate2');
		    	
    	$sReturn = '<div id="divAdvancedSearch" style="display:none;">
		 <form method="GET" action="">
		 <table border="0" cellspacing="0" cellpadding="3" width="' . $sWidth . '" align="center">
		  <tr>
		   <td>
		    <fieldset><legend style="font-size:12px; font-weight:bold;">Advanced Search</legend>
		     <br />
		      <table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		       <tr><td align="right">Vendor Name:</td><td><input type="text" class="form1" name="txtVendorName" id="txtVendorName" /></td></tr>
		       <tr><td align="right">Vendor Code:</td><td><input type="text" class="form1" name="txtVendorCode" id="txtVendorCode" /></td></tr>
		       <tr><td align="right">Vendor Added Between:</td><td>' . $sVendorAddeDateTime . ' and ' . $sVendorAddeDateTime2 . '</td></tr>
		      </table>
		     <br />
		     <div align="center"><input type="submit" value="Search" class="AdminFormButton1" /></div>
		    </fieldset>
		   </td>
		  </tr>
		 </table>
		 <input type="hidden" name="action" id="action" value="SearchVendor" />
		 <input type="hidden" name="view" id="view" value="1" />
		 </form>
		</div>';

    	return($sReturn);
    }
	
	function ShowVendorsDirectory($iVendorId = 0, $sAction, $iView = 0)
	{
    	global $objDatabase;
    	global $objGeneral;
    	
    	$sVendorSearch = $objGeneral->fnGet("txtVendorSearch");
    	$iPagingLimit = 6;
    	
    	$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
    	$sButtons = $sButtons_Print;
    	
    	$sReturn = '<table border="0" width="825" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">Vendors Directory</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="825" cellpadding="1" cellspacing="0" bgcolor="#838274" align="center"><tr><td><table width="825" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">
		<br />

		<table border="0" cellspacing="0" cellpadding="3" width="60%" align="center">
		 <tr><td colspan="2"><span style="font-size:16px; font-weight:bold;">Vendor Search:</span></td></tr>
		 <tr>
		  <td>
		  <form method="POST" action="../vendors/vendorsdirectory.php">
		  <input type="text" name="txtVendorSearch" id="txtVendorSearch" size="27" class="form2" value="' . $sVendorSearch . '" />
		  </td>
		  <td>
		   <input type="submit" class="AdminFormButton1" value="Search" /><input type="hidden" name="action" id="action" value="SearchVendor" /></form>
		  </td>
		 </tr>
		 <tr>
		  <td></td>
		  <td>[ <a href="#noanchor" onclick="ShowHideDiv(\'divAdvancedSearch\');">Advanced Search</a> ]</td>
		 </tr>
		</table>
		' . $this->ShowAdvancedSearch(' ', 1) . '
		<br /><br />';
    	    	
    	if (($sAction == "SearchVendor") && ($iVendorId == 0))
    	{
    		
    		if ($iView == 0)
    		{
	    		// If there is a space in vendor search
	    		if (strpos($sVendorSearch, ' ') !== false)
	    		{
	    			$aVendorSearch = explode(' ', $sVendorSearch);
	    			if (count($aVendorSearch) > 0)
	    				$sVendorSearch2 = $aVendorSearch[0];
	    		}
	    		else 
	    			$sVendorSearch2 = $sVendorSearch;
    		}
    		else 
    		{
            	$sAdvancedSearch_VendorName = $objGeneral->fnGet("txtVendorName");
            	$sAdvancedSearch_VendorName = $objDatabase->RealEscapeString($sAdvancedSearch_VendorName);
            	$sAdvancedSearch_VendorCode = $objGeneral->fnGet("txtVendorCode");
            	$sAdvancedSearch_VendorCode = $objDatabase->RealEscapeString($sAdvancedSearch_VendorCode);
            	
	        	$dAdvancedSearch_StartDate = $objGeneral->fnGet("txtVendorAddedDate");
	        	$dAdvancedSearch_StartDate = $objDatabase->RealEscapeString($dAdvancedSearch_StartDate);
	        	$dAdvancedSearch_EndDate = $objGeneral->fnGet("txtVendorAddedDate2");
	        	$dAdvancedSearch_EndDate = $objDatabase->RealEscapeString($dAdvancedSearch_EndDate);
	        	
	        	$sAdvancedSearchString = "";
	        	if ($sAdvancedSearch_VendorName != '') $sAdvancedSearchString .= " AND (V.VendorName LIKE '%$sAdvancedSearch_VendorName%')";
	        	if ($sAdvancedSearch_VendorCode!= '') $sAdvancedSearchString .= " AND (V.VendorCode LIKE '%$sAdvancedSearch_VendorCode%')";
				
				$sAdvancedSearchString .= " AND (V.VendorAddedOn BETWEEN '$dAdvancedSearch_StartDate 00:00:00' AND '$dAdvancedSearch_EndDate 23:59:59')";
    		}
    		    		
    		if ($sVendorSearch2 != "")
	        {
	        	$sVendorSearch2 = $objDatabase->RealEscapeString($sVendorSearch2);
	            $sSearchCondition = " AND ((V.VendorId LIKE '%$sVendorSearch2%') OR (V.VendorName LIKE '%$sVendorSearch2%') OR (V.VendorCode LIKE '%$sVendorSearch2%') OR (V.OfficeManager LIKE '%$sVendorSearch2%') OR (V.AccountRepresentative LIKE '%$sVendorSearch2%') OR (V.PhoneNumber LIKE '%$sVendorSearch2%') OR (V.EmailAddress LIKE '%$sVendorSearch2%')) ";
	        }
	        else 
	        	$sSearchCondition = $sAdvancedSearchString;

			$sQuery = "SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "' $sSearchCondition ";
			$varResult = $objDatabase->Query($sQuery);
			$iNumberOfRecords = $objDatabase->RowsNumber($varResult);
    		if ($iNumberOfRecords <= 0)
    		{
	    		$sReturn .= '
	    		<table boder="0" cellspacing="0" cellpadding="3" width="90%" align="center">
	    		 <tr><td align="center"><span style="font-size:12px; color:red;">Sorry, your search returned no results...<br /><br /></span></td></tr>
	    		</table>';
    		}
			else
    		{
    			if ($iNumberOfRecords == 1)
    			{
    				$iVendorId = $objDatabase->Result($varResult, 0, "V.VendorId");
					$objGeneral->fnRedirect('../vendors/vendorsdirectory.php?txtVendorSearch=' . urlencode($sVendorSearch) . '&id=' . $iVendorId);
    			}
    			else
    			{
					$sReturn .= '<br />
					<div align="center">
					 <span style="font-size:12px; color:green;">Your search returned more than one results,<br />please select a vendor from the following list:<br /><br /></span>
					</div>
		    		<table boder="0" cellspacing="0" cellpadding="3" width="90%" align="center">
		    		 <tr>
		    		  <td>
				       <table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">

						<table class="GridTable"  border="1" style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;" cellspacing="0" cellpadding="3" width="100%" align="center">
				         <tr class="GridTR"><td colspan="3"><span class="title2">Select a Vendor:</span></td></tr>
						 <tr class="GridTR"><td class="WhiteHeading">Vendor</td><td class="WhiteHeading">Vendor Code</td><td class="WhiteHeading">Phone Number</td></tr>';

					for ($i=0; $i < $iNumberOfRecords; $i++)
					{
						$sReturn .= '<tr bgcolor="#ffffff">
						 <td><a style="font-size:12px; text-decoration:underline; color:blue;" href="../vendors/vendorsdirectory.php?txtVendorSearch=' . $sVendorSearch . '&id=' . $objDatabase->Result($varResult, $i, "V.VendorId") . '">' . $objDatabase->Result($varResult, $i, "V.VendorName"). '</a></td>
						  <td style="font-size:12px;">' . $objDatabase->Result($varResult, $i, "V.VendorCode") . '</td>
						  <td style="font-size:12px;">' . $objDatabase->Result($varResult, $i, "V.PhoneNumber") . '</td>
						 </tr>';
					}

					$sReturn .= '
						</table>
				       </td></tr></table></td></tr></table>
		    		  </td>
		    		 </tr>
		    		</table>
		    		<br />';
    			}
    		}
    	}

    	if ($iVendorId > 0)
    	{
    		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors AS V WHERE V.OrganizationId='" . cOrganizationId . "' AND V.VendorId='$iVendorId'");

    		if ($objDatabase->RowsNumber($varResult) <= 0) die('Invalid Vendor Id');

    		$sVendorName = $objDatabase->Result($varResult, 0, "V.VendorName");
    		$sVendorCode = $objDatabase->Result($varResult, 0, "V.VendorCode");
    		
			$sOfficeManager = $objDatabase->Result($varResult, 0, "V.OfficeManager");
			$sAccountRepresentative = $objDatabase->Result($varResult, 0, "V.AccountRepresentative");
			$sPhoneNumber = $objDatabase->Result($varResult, 0, "V.PhoneNumber");
			$sNTNNumber = $objDatabase->Result($varResult, 0, "V.NTNNumber");
			$sEmailAddress = $objDatabase->Result($varResult, 0, "V.EmailAddress");
			
			$iStatus = $objDatabase->Result($varResult, 0, "V.Status");
    		$sStatus = ($iStatus == 1) ? 'Active' : 'Inactive';
			
			$sAddress = $objDatabase->Result($varResult, 0, "V.Address");
			$sCity = $objDatabase->Result($varResult, 0, "V.City");
			$sState = $objDatabase->Result($varResult, 0, "V.State");
			$sZipCode = $objDatabase->Result($varResult, 0, "V.ZipCode");
			$sCountry = $objDatabase->Result($varResult, 0, "V.Country");
			
			$sReturn .= '
    		<table boder="0" cellspacing="0" cellpadding="3" width="90%" align="center">
    		 <tr>
    		  <td>
				<table border="0" style="border: 2px; border-style: solid; border-color:#E6E6E6; border-collapse: collapse;" cellspacing="0" cellpadding="8" width="100%" align="center">
		         <tr class="GridTR"><td colspan="4"><span class="title2">' . $sVendorName . '</span></td></tr>
				 <tr>
				  <td style="width:150px;" class="Tahoma16">Vendor Name:</td><td style="width:330px;text-decoration:underline;" class="Tahoma16">' . $sVendorName . '</td>
				  <td rowspan="4" colspan="2" align="right" valign="top"><img src="' . $this->FindVendorPhoto($iVendorId) . '" /></td>
		         </tr>
				 <tr><td class="Tahoma16">Vendor Code:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sVendorCode . '</td></tr>
				 <tr><td class="Tahoma16">Office Manager:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sOfficeManager . '</td></tr>
				 <tr><td class="Tahoma16">Account Representative:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sAccountRepresentative . '</td></tr>
				 <tr><td class="Tahoma16">E-Mail Address:</td><td colspan="3" style="text-decoration:underline;" class="Tahoma16">' . $sEmailAddress . '</td></tr>
				 <tr>
				  <td valign="top" class="Tahoma16">Phone Number:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sPhoneNumber . '</td>
				  <td valign="top" class="Tahoma16">NTN:</td><td valign="top" width="30%" style="text-decoration:underline;" class="Tahoma16">' . $sNTNNumber . '</td>
				 </tr>
				 <tr>
				  <td class="Tahoma16" valign="top">Address:</td><td colspan="3" style="text-decoration:underline;" class="Tahoma16">' . $sAddress . '</td>
				 </tr>
				 <tr>
				  <td class="Tahoma16" valign="top">City:</td><td valign="top" style="text-decoration:underline;" class="Tahoma16">' . $sCity . '</td>
				  <td class="Tahoma16">State:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sState . '</td>
				 </tr>
				 <tr>
				  <td class="Tahoma16">Zip Code:</td><td style="text-decoration:underline;" class="Tahoma16">' . $sZipCode . '</td>
				  <td class="Tahoma16">Country:</td><td style="text-decoration:underline;" class="Tahoma16">' . $objGeneral->aCountriesList[$sCountry] . '</td>
				 </tr>
				</table>
    		  </td>
    		 </tr>
    		</table>
    		<br />';
    	}

    	$sReturn .= '</td></tr></table></td></tr></table><br />
    	<div align="center"><input type="button" class="AdminFormButton1" onclick="window.close();" value="Close" /></div>';			

		return($sReturn);
	}
	
}

// Class: Vendors Type
class clsVendors_Types
{
	// Class Constructor
	function __construct()
	{
	}
	
	function ShowAllVendorTypes($sSearch)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		
		// Employee Roles
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[0] == 0) // View Disabled
			return('<br /><br /><div align="center">Sorry, Access Denied to this Area...</div><br /><br />');
		
		$sSortBy = $objGeneral->fnGet("sortby");
		$sSortOrder = $objGeneral->fnGet("sortorder");
		$iShow = $objGeneral->fnGet("show");
		$iPage = $objGeneral->fnGet("page");
		if ($iPage == '') $iPage = 1;

		$sTitle = "Vendor Types";
		if ($sSortBy == "") $sSortBy = "VT.VendorTypeId DESC";

		if ($sSearch != "")
		{
			$sSearch = $objDatabase->RealEscapeString($sSearch);
			$sSearchCondition = " AND ((VT.VendorTypeId LIKE '%$sSearch%') OR (VT.VendorTypeName LIKE '%$sSearch%') OR (E.FirstName LIKE '%$sSearch%') OR (E.LastName LIKE '%$sSearch%'))";
			$sSearch = stripslashes($sSearch);
			$sTitle = 'Search Results for "' . $sSearch . '"';
		}

		$iTotalRecords = $objDatabase->DBCount("fms_vendors_types AS VT INNER JOIN organization_employees AS E ON E.EmployeeId = VT.VendorTypeAddedBy", "1=1 $sSearchCondition");
		$sRefresh = '<a href="#noanchor" onclick="window.location=\'?pagetype=vendortypes&show=' . $iShow . '&page=' . $iPage . '\';"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sPagingString = $objGeneral->Grid_GeneratePagingString($iPage, $iShow, $iTotalRecords, $sRefresh, $iPagingLimit, $iOffSet, $sCurrentURL);

		$varResult = $objDatabase->Query("
		SELECT * FROM fms_vendors_types AS VT
		INNER JOIN organization_employees AS E ON E.EmployeeId = VT.VendorTypeAddedBy
		WHERE 1=1 $sSearchCondition
		ORDER BY $sSortBy $sSortOrder
		LIMIT $iOffSet, $iPagingLimit");

		$sReturn = '
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td colspan="2" align="left"><span class="heading">' . $sTitle . ' (' . $iTotalRecords . ' Records)</span></td>
		  <td colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<table class="GridTable" border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr class="GridTR">		  
		  <td width = "15%" align="left"><span class="WhiteHeading">Vendor Type Id&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=VT.VendorTypeId&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Type Id in Ascending Order" title="Sort by Vendor Type Id in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=VT.VendorTypeId&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Type Id in Descending Order" title="Sort by Vendor Type Id in Descending Order" border="0" /></a></span></td>
		  <td align="left"><span class="WhiteHeading">Vendor Type Name&nbsp;&nbsp;<a href="' . $sCurrentURL . '&sortby=VT.VendorTypeName&sortorder="><img src="../images/sort_up.gif" alt="Sort by Vendor Type Name in Ascending Order" title="Sort by Vendor Type Name in Ascending Order" border="0" /></a><a href="' . $sCurrentURL . '&sortby=VT.VendorTypeName&sortorder=DESC"><img src="../images/sort_dn.gif" alt="Sort by Vendor Type Name in Descending Order" title="Sort by Vendor Type Name in Descending Order" border="0" /></a></span></td>
		  <td width="3%" colspan="3"><span class="WhiteHeading">Operations</span></td>
		 </tr>';

		for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
		{
			$sTRBGColor = ($sTRBGColor == '#edeff1') ? '#ffffff' : '#edeff1';
			
			$iVendorTypeId = $objDatabase->Result($varResult, $i, "VT.VendorTypeId");
			
			$sVendorTypeName = $objDatabase->Result($varResult, $i, "VT.VendorTypeName");
			$sVendorTypeName = $objDatabase->RealEscapeString($sVendorTypeName);
			$sVendorTypeName = stripslashes($sVendorTypeName);
						
			$dVendorTypeAddedOn = $objDatabase->Result($varResult, $i, "VT.VendorTypeAddedOn");
			
			$sEditVendorType = '<td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'Update ' . $objDatabase->RealEscapeString(stripslashes($sVendorTypeName)) . '\', \'../vendors/vendors.php?pagetype=vendortypes_details&action2=edit&id=' . $iVendorTypeId . '\', \'520px\', true);"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Vendor Type Details" title="Edit this Vendor Type Details"></a></td>';
			$sDeleteVendorType = '<td class="GridTD" align="center"><a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Vendor Type?\')) {window.location = \'?action=DeleteVendorType&id=' . $iVendorTypeId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Vendor Type" title="Delete this Vendor Type"></a></td>';
						
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[2] == 0) // Edit Disabled
				$sEditVendorType = '';
				
			if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[3] == 0) // Edit Disabled
				$sDeleteVendorType = '';
			
			$sReturn .= '
			<tr onMouseOver="bgColor=\'#ffe69c\';" onMouseOut="bgColor=\'' . $sTRBGColor . '\';" bgcolor="' . $sTRBGColor . '">			
			 <td class="GridTD" align="left" valign="top">' . $iVendorTypeId . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="left" valign="top">' . $sVendorTypeName . '<img src="../images/spacer.gif" border="0" alt="blank" title="blank" /></td>
			 <td class="GridTD" align="center"><a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sVendorTypeName)) . '\', \'../vendors/vendors.php?pagetype=vendortypes_details&id=' . $iVendorTypeId . '\', \'520px\', true);"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Vendor Type Details" title="View this Vendor Type Details"></a></td>
			' . $sEditVendorType . $sDeleteVendorType . 
		   '</tr>';
		}

		$sAddVendorsType = '<input onclick="window.top.CreateTab(\'tabContainer\', \'Add Vendor Type\', \'../vendors/vendors.php?pagetype=vendortypes_details&action2=addnew\', \'520px\', true);" type="button" class="formbutton" value="Add Vendor Type">';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[1] == 0) // Add Disabled
			$sAddVendorsType = '';
		
		$sReturn .= '</table>
		<table border="0" cellspacing="0" cellpadding="3" width="100%" align="center">
		 <tr>
		  <td>
		   ' . $sAddVendorsType . ' 
		   &nbsp;&nbsp;&nbsp;
 		   <form method="GET" action="">Search for a Vendor Type:&nbsp;<input type="text" class="form1" size=15 name="p" id="p" />&nbsp;<input type="image" src="../images/icons/search.gif" alt="Search for a Vendor Type" title="Search for a Vendor Type" border="0"><input type="hidden" name="pagetype" id="pagetype" value="vendortypes" /></form>
		  </td>
		  <td height="40" colspan="2" align="right">' . $sPagingString . '</td>
		 </tr>
		</table>
		<br /><br />
		<table border="0" cellspacing="0" cellpadding="3" width="95%" align="center">
		 <tr><td align="center" width="10"><img src="../images/icons/iconDetails.gif" title="View this Vendor Type Details" alt="View this Vendor Type Details"></td><td>View this Vendor Type Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconEdit.gif" title="Edit this Vendor Type Details" alt="Edit this Vendor Type Details"></td><td>Edit this Vendor Type Details</td></tr>
		 <tr><td align="center" width="10"><img src="../images/icons/iconDelete.gif" title="Delete this Vendor Type" alt="Delete this Vendor Type"></td><td>Delete this Vendor Type</td></tr>
		</table>';

		return($sReturn);
	}
	
	function VendorTypeDetails($iVendorTypeId, $sAction2)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;

		$varResult = $objDatabase->Query("
		SELECT *
		FROM fms_vendors_types AS VT
		INNER JOIN organization_employees AS E ON E.EmployeeId = VT.VendorTypeAddedBy
		WHERE VT.VendorTypeId = '$iVendorTypeId'");

		$sButtons_Refresh = '<a href="#noanchor" onclick="window.location.reload();"><img src="../images/icons/iconRefresh.gif" border="0" alt="Refresh" title="Refresh" /></a>';
		$sButtons_Print = '<a href="#noanchor" onclick="window.print();"><img src="../images/icons/iconPrint.gif" border="0" alt="Print this Page" title="Print this Page" /></a>';
		$sButtons_Edit = '<a href="?pagetype=vendortypes_details&action2=edit&id=' . $iVendorTypeId . '"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Vendor Type Details" title="Edit this Vendor Type Details" /></a>';
		$sButtons_Delete = '<a href="#noanchor" onclick="if(confirm(\'Do you really want to delete this Vendor Type?\')) {window.location = \'?action=DeleteVendorType&id=' . $iVendorTypeId . '\';}"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Vendor Type" title="Delete this Vendor Type" /></a>';
		
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[2] == 0)
   			$sButtons_Edit = '';
			
		if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[3] == 0)
			$sButtons_Delete = '';
		
		$sButtons = $sButtons_Refresh . '&nbsp;' . $sButtons_Print;
		if ($sAction2 != "addnew") $sButtons .= '&nbsp;' . $sButtons_Edit . '&nbsp;' . $sButtons_Delete;

		$sReturn = '
		<table border="0" width="100%" cellpadding="1" cellspacing="0" align="center"><tr><td><span class="heading">VendorsType Details</span></td><td align="right">' . $sButtons . '</td></tr></table>
		<table border="0" width="100%" cellpadding="1" cellspacing="0" bgcolor="#336699" align="center"><tr><td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#aac7e4"><tr><td bgcolor="#ffffff" valign="top">';

		if (($objDatabase->RowsNumber($varResult) <= 0) && ($sAction2 != "addnew"))
			return('<br /><br /><br /><br/ >');

		if (($objDatabase->RowsNumber($varResult) > 0) || ($sAction2 == "addnew"))
		{
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$iVendorTypeId = $objDatabase->Result($varResult, 0, "VT.VendorTypeId");
				
				$sVendorTypeName = $objDatabase->Result($varResult, 0, "VT.VendorTypeName");
				$sVendorTypeName = $objDatabase->RealEscapeString($sVendorTypeName);
				$sVendorTypeName = stripslashes($sVendorTypeName);
				
				$iVendorTypeAddedBy = $objDatabase->Result($varResult, 0, "VT.VendorTypeAddedBy");
				$sVendorTypeAddedBy = $objDatabase->Result($varResult, 0, "E.FirstName") . ' ' . $objDatabase->Result($varResult, 0, "E.LastName");
				$sVendorTypeAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sVendorTypeAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $iVendorTypeAddedBy . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$sNotes = $objDatabase->Result($varResult, $i, "VT.Notes");
				$sNotes = $objDatabase->RealEscapeString($sNotes);
				$sNotes = stripslashes($sNotes);
					
				$dVendorTypeAddedOn = $objDatabase->Result($varResult, 0, "VT.VendorTypeAddedOn");
				$sVendorTypeAddedOn = date("F j, Y", strtotime($dVendorTypeAddedOn)) . ' at ' . date("g:i a", strtotime($dVendorTypeAddedOn));
			}

			if ($sAction2 == "edit")
			{
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sVendorTypeName = '<input type="text" name="txtVendorTypeName" id="txtVendorTypeName" class="form1" value="' . $sVendorTypeName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
			}
			else if ($sAction2 == "addnew")
			{
				$sVendorTypeAddedBy = $objEmployee->sEmployeeFirstName . ' ' . $objEmployee->sEmployeeLastName;
				$sVendorTypeAddedBy .= '&nbsp;<a href="#noanchor" onclick="window.top.CreateTab(\'tabContainer\', \'' . $objDatabase->RealEscapeString(stripslashes($sVendorTypeAddedBy)) . '\', \'../organization/employees.php?pagetype=details&id=' . $objEmployee->iEmployeeId . '\', \'520px\', true);"><img src="../images/icons/iconUserDetails.gif" align="absmiddle" alt="Employee Information" title="Employee Information" border="0" /></a>';
				
				$iVendorTypeId = "";
				$sVendorTypeName = $objGeneral->fnGet("txtVendorTypeName");
				$sNotes = $objGeneral->fnGet("txtNotes");
				$sOfficeManager = $objGeneral->fnGet("txtOfficeManager");
				$sAccountRepresentative = $objGeneral->fnGet("txtAccountRepresentative");
				$sSpecialNote = $objGeneral->fnGet("txtSpecialNote");
				
				$sReturn .= '<form method="post" action="" onsubmit="return ValidateForm();">';
				$sVendorTypeName = '<input type="text" name="txtVendorTypeName" id="txtVendorTypeName" class="form1" value="' . $sVendorTypeName . '" size="30" />&nbsp;<span style="color:red;">*</span>';
				$sNotes = '<textarea name="txtNotes" id="txtNotes" class="form1" rows="5" cols="60">' . $sNotes . '</textarea>';
				$sVendorTypeAddedOn = date("F j, Y", strtotime($objGeneral->fnNow())) . ' at ' . date("g:i a", strtotime($objGeneral->fnNow()));
			}

			$sReturn .= '
			<script language="JavaScript" type="text/javascript">
			function ValidateForm()
			{
				if (GetVal(\'txtVendorTypeName\') == "") return(AlertFocus(\'Please enter a valid Vendor Type Name\', \'txtVendorTypeName\'));
					
				return true;
			}
			</script>
			<table border="0" cellspacing="0" cellpadding="5" width="100%" align="center">
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Vendor Type Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top" width="25%">Vendor Type Id:</td><td><strong>' . $iVendorTypeId . ' (Automatically Assigned)</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Vendor Type Name:</td><td><strong>' . $sVendorTypeName . '</strong></td></tr>			  
			 <tr class="Details_Title_TR"><td colspan="2"><span class="Details_Title">Extra Information:</span></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Notes:</td><td><strong>' . $sNotes . '</strong></td></tr>
			 <tr bgcolor="#ffffff"><td valign="top">Vendor Type Added On:</td><td><strong>' . $sVendorTypeAddedOn . '</strong></td></tr>
			 <tr bgcolor="#edeff1"><td valign="top">Vendor Type Added By:</td><td><strong>' . $sVendorTypeAddedBy . '</strong></td></tr>
			</table>';

			if ($sAction2 == "edit")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Update" class="AdminFormButton1"><input type="hidden" name="id" id="id" value="' . $iVendorTypeId . '"><input type="hidden" name="action" id="action" value="UpdateVendorType"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
			else if ($sAction2 == "addnew")
				$sReturn .= '<br /><br /><div align=center><input type="submit" value="Add Vendor Type" class="AdminFormButton1"><input type="hidden" name="action" id="action" value="AddNewVendorType"></form></div><br /><div align="left">&nbsp;<span style="color:red;">* Required Fields</span></div>';
		}

		$sReturn .= '
		<br />
		</td></tr></table></td></tr></table>';

		return($sReturn);
	}
	
	function AddNewVendorType($sVendorTypeName, $sNotes)
	{
		global $objDatabase;
		global $objGeneral;
		global $objEmployee;
		global $objSystemLog;
		
		// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[1] == 0) // Add Disabled
			return(1010);
		
		$varNow = $objGeneral->fnNow();
		
		$sVendorTypeName = $objDatabase->RealEscapeString($sVendorTypeName);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		$iVendorTypeAddedBy = $objEmployee->iEmployeeId;
		
		if($objDatabase->DBCount("fms_vendors_types AS VT", "VT.VendorTypeName = '$sVendorTypeName'") > 0) return(5220);
		
		$varResult = $objDatabase->Query("INSERT INTO fms_vendors_types
		(VendorTypeName, Notes, VendorTypeAddedOn, VendorTypeAddedBy)
		VALUES ('$sVendorTypeName', '$sNotes', '$varNow', '$iVendorTypeAddedBy')");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{
			$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_types AS VT WHERE VT.VendorTypeName='$sVendorTypeName' AND VT.VendorTypeAddedOn='$varNow'");
			if ($objDatabase->RowsNumber($varResult) > 0)
			{				
				$objSystemLog->AddLog("Add New Vendor Type - " . $sVendorTypeName);
				$objGeneral->fnRedirect('?pagetype=vendortypes_details&error=5221&id=' . $objDatabase->Result($varResult, 0, "VT.VendorTypeId"));
			}
		}

		return(5222);
	}
	
	function UpdateVendorType($iVendorTypeId, $sVendorTypeName, $sNotes)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[2] == 0) // Edit Disabled
			return(1010);
		
		$sVendorTypeName = $objDatabase->RealEscapeString($sVendorTypeName);
		$sNotes = $objDatabase->RealEscapeString($sNotes);
		
		if($objDatabase->DBCount("fms_vendors_types AS VT", "VT.VendorTypeName = '$sVendorTypeName' AND VT.VendorTypeId <> '$iVendorTypeId'") > 0) return(5220);
		
		$varResult = $objDatabase->Query("
		UPDATE fms_vendors_types
		SET	
			VendorTypeName='$sVendorTypeName', 
			Notes='$sNotes'			
		WHERE VendorTypeId='$iVendorTypeId'");

		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Update Vendor Type - " . $sVendorTypeName);
			$objGeneral->fnRedirect('../vendors/vendors.php?pagetype=vendortypes_details&error=5223' . '&id=' . $iVendorTypeId);
			//return(3002);
		}
		else
			return(5224);
	}
	
	function DeleteVendorType($iVendorTypeId)
	{
		global $objDatabase;
		global $objEmployee;
		global $objGeneral;
		global $objSystemLog;
		
		// Employee Roles
    	if ($objEmployee->objEmployeeRoles->aEmployeeRole_FMS_Vendors_VendorTypes[3] == 0) // Delete Disabled
			return(1010);
		if ($objDatabase->DBCount("fms_vendors AS V", "V.VendorTypeId= '$iVendorTypeId'") > 0) return(5228);
		
		$varResult = $objDatabase->Query("SELECT * FROM fms_vendors_types AS VT WHERE VT.VendorTypeId='$iVendorTypeId'");
		if ($objDatabase->RowsNumber($varResult) <= 0)
			return(5227);

		$sVendorTypeName = $objDatabase->Result($varResult, 0, "VT.VendorTypeName");
		$sVendorTypeName = $objDatabase->RealEscapeString($sVendorTypeName);

		$varResult = $objDatabase->Query("DELETE FROM fms_vendors_types WHERE VendorTypeId='$iVendorTypeId'");
		if ($objDatabase->AffectedRows($varResult) > 0)
		{			
			$objSystemLog->AddLog("Delete Vendor Type - " . $sVendorTypeName);
			
			$objGeneral->fnRedirect('../vendors/vendors.php?pagetype=vendortypes&error=5225');
			//return(5225);
		}
		else
			return(5226);
	}

}

?>