<?php
include(cVSFFolder . '/classes/clsXajax.php');
$objAJAX = new xajax();

$objAJAX->registerFunction("AJAX_FMS_Banking_BankAccounts_FillBankAccounts");
$objAJAX->registerFunction("AJAX_FMS_Banking_BankWithdrawals_FillBankCheckBooks");
$objAJAX->registerFunction("AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountCode"); // Get Next Chart Of Account Code based on Control
$objAJAX->registerFunction("AJAX_FMS_Accounts_ChartOfAccounts_GetControlCode"); // Get Control Code based on Category
$objAJAX->registerFunction("AJAX_FMS_Accounts_GeneralJournal_LoadQuickEntry");			// Quick Entry
$objAJAX->registerFunction("AJAX_FMS_Accounts_GeneralJournal_LoadGeneralJournal");			// Quick Entry
$objAJAX->registerFunction("AJAX_FMS_Accounts_GeneralJournal_GetSeries");				// General Journal
$objAJAX->registerFunction("AJAX_FMS_Accounts_GeneralJournal_GetNextAvailableCheck");				// General Journal
$objAJAX->registerFunction("AJAX_FMS_Accounts_GeneralJournal_GetDonorProject"); // Get Donor Project
$objAJAX->registerFunction("AJAX_FMS_Accounts_GeneralJournal_GetProjectActivity"); // Get Project Activity
/* Quick Entry */
$objAJAX->registerFunction("AJAX_FMS_Accounts_QuickEntries_LoadQuickEntry");			// Quick Entry
$objAJAX->registerFunction("AJAX_FMS_Accounts_QuickEntries_GetDonorProject"); // Get Donor Project
$objAJAX->registerFunction("AJAX_FMS_Accounts_QuickEntries_GetProjectActivity"); // Get Project Activity



// Banking Module
$objAJAX->registerFunction("AJAX_FMS_Banking_BankAccounts_GetChartOfAccountCode");
$objAJAX->registerFunction("AJAX_FMS_Banking_BankAccounts_GetControlCode");

/* Banking */

// Homepage
$objAJAX->registerFunction("AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountsControls");
$objAJAX->registerFunction("AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccounts");
$objAJAX->registerFunction("AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountsChildren");
$objAJAX->registerFunction("AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountsControls_Children");
/* Homepage */

/* Reports */
$objAJAX->registerFunction("AJAX_FMS_Reports_GetControlCode");
$objAJAX->registerFunction("AJAX_FMS_Reports_GetChartOfAccounts");

$objAJAX->processRequests();

/* Homepage */

function AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountsControls($sChartOfAccountsCategoryCode)
{
	include('../../system/library/fms/clsFMS_Accounts.php');
	$objChartOfAccounts = new clsChartOfAccounts();
	
    $objResponse = $objChartOfAccounts->AJAX_GetChartOfAccountsControls($sChartOfAccountsCategoryCode);
    return($objResponse);
}

function AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccounts($iChartOfAccountsControlId)
{
	include('../../system/library/fms/clsFMS_Accounts.php');
	$objChartOfAccounts = new clsChartOfAccounts();
	
    $objResponse = $objChartOfAccounts->AJAX_GetChartOfAccounts($iChartOfAccountsControlId);
    return($objResponse);
}

function AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountsChildren($iChartOfAccountsId, $bChildControl)
{
	include('../../system/library/fms/clsFMS_Accounts.php');
	$objChartOfAccounts = new clsChartOfAccounts();
	
    $objResponse = $objChartOfAccounts->AJAX_GetChartOfAccountsChildren($iChartOfAccountsId, $bChildControl);
    return($objResponse);
}

function AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountsControls_Children($iChartOfAccountsControlParentId)
{
	include('../../system/library/fms/clsFMS_Accounts.php');
	$objChartOfAccounts = new clsChartOfAccounts();
	
    $objResponse = $objChartOfAccounts->AJAX_GetChartOfAccountsControls_Children($iChartOfAccountsControlParentId);
    return($objResponse);
}


/*Banking Module*/
function AJAX_FMS_Banking_BankAccounts_FillBankAccounts($iBankId, $sTargetSelectBox)
{
	global $objBankCheck;
    $objResponse = $objBankCheck->AJAX_FillBankAccounts($iBankId, $sTargetSelectBox);
    return($objResponse);
}

function AJAX_FMS_Banking_BankWithdrawals_FillBankCheckBooks($iBankAccountId, $sTargetSelectBox)
{
	global $objBankWithdrawal;
    $objResponse = $objBankWithdrawal->AJAX_FillBankCheckBooks($iBankAccountId, $sTargetSelectBox);
    return($objResponse);
}

/*
function AJAX_FMS_Accounts_GeneralJournal_FillBankAccounts($iBankId, $sTargetSelectBox, $iDefaultBankAccountId)
{
	global $objGeneralJournal;
    $objResponse = $objGeneralJournal->AJAX_FillBankAccounts($iBankId, $sTargetSelectBox, $iDefaultBankAccountId);
    return($objResponse);
}

function AJAX_FMS_Accounts_GeneralJournal_FillCheckBooks($iBankAccountId, $sTargetSelectBox, $iDefaultBankCheckBookId)
{
	global $objGeneralJournal;
    $objResponse = $objGeneralJournal->AJAX_FillCheckBooks($iBankAccountId, $sTargetSelectBox, $iDefaultBankCheckBookId);
    return($objResponse);
}
Accounting Module*/

/*General Journal Module */

function AJAX_FMS_Accounts_GeneralJournal_LoadQuickEntry($iQuickEntryId)
{
	global $objGeneralJournal;
    $objResponse = $objGeneralJournal->AJAX_LoadQuickEntry($iQuickEntryId);
    return($objResponse);
}


function AJAX_FMS_Accounts_GeneralJournal_LoadGeneralJournal($iGeneralJournalId)
{
	global $objGeneralJournal;
    $objResponse = $objGeneralJournal->AJAX_LoadGeneralJournal($iGeneralJournalId);
    return($objResponse);
}

function AJAX_FMS_Accounts_GeneralJournal_GetSeries($iEntryType, $iReceiptType, $iPaymentType, $iBankAccountId)
{
	global $objGeneralJournal;
    $objResponse = $objGeneralJournal->AJAX_GetSeries($iEntryType, $iReceiptType, $iPaymentType, $iBankAccountId);
    return($objResponse);
}

function AJAX_FMS_Accounts_GeneralJournal_GetDonorProject($iDonorId, $sTargetSelectBox)
{
	global $objGeneralJournal;
    $objResponse = $objGeneralJournal->AJAX_GetDonorProjects($iDonorId, $sTargetSelectBox);	
    return($objResponse);
}

function AJAX_FMS_Accounts_GeneralJournal_GetProjectActivity($iDonorProjectId, $sTargetSelectBox)
{
	global $objGeneralJournal;
    $objResponse = $objGeneralJournal->AJAX_GetProjectActivities($iDonorProjectId, $sTargetSelectBox);	
    return($objResponse);
}

function AJAX_FMS_Accounts_GeneralJournal_GetNextAvailableCheck($iCheckBookId)
{
	global $objGeneralJournal;
    $objResponse = $objGeneralJournal->AJAX_GetNextAvailableCheck($iCheckBookId);
    return($objResponse);
}

/* Quick Entries */
function AJAX_FMS_Accounts_QuickEntries_LoadQuickEntry($iQuickEntryId)
{
	global $objQuickEntry;
    $objResponse = $objQuickEntry->AJAX_LoadQuickEntry($iQuickEntryId);
    return($objResponse);
}

function AJAX_FMS_Accounts_QuickEntries_GetDonorProject($iDonorId, $sTargetSelectBox)
{
	global $objQuickEntry;
    $objResponse = $objQuickEntry->AJAX_GetDonorProjects($iDonorId, $sTargetSelectBox);	
    return($objResponse);
}

function AJAX_FMS_Accounts_QuickEntries_GetProjectActivity($iDonorProjectId, $sTargetSelectBox)
{
	global $objQuickEntry;
    $objResponse = $objQuickEntry->AJAX_GetProjectActivities($iDonorProjectId, $sTargetSelectBox);	
    return($objResponse);
}
/* End */

function AJAX_FMS_Accounts_ChartOfAccounts_GetChartOfAccountCode($iChartOfAccountsControlId, $sTargetSelectBox)
{
	global $objChartOfAccounts;
    $objResponse = $objChartOfAccounts->AJAX_GetChartOfAccountsCode($iChartOfAccountsControlId, $sTargetSelectBox);	
    return($objResponse);
}

function AJAX_FMS_Accounts_ChartOfAccounts_GetControlCode($iChartOfAccountsCategoryId, $sTargetSelectBox)
{
	global $objChartOfAccounts;
    $objResponse = $objChartOfAccounts->AJAX_GetControlCode($iChartOfAccountsCategoryId, $sTargetSelectBox);	
    return($objResponse);
}

/* Banking Module */
function AJAX_FMS_Banking_BankAccounts_GetChartOfAccountCode($iChartOfAccountsControlId)
{
	global $objBankAccounts;
    $objResponse = $objBankAccounts->AJAX_GetChartOfAccountsCode($iChartOfAccountsControlId);	
    return($objResponse);
}

function AJAX_FMS_Banking_BankAccounts_GetControlCode($iChartOfAccountsCategoryId, $sTargetSelectBox)
{
	global $objBankAccounts;
    $objResponse = $objBankAccounts->AJAX_GetControlCode($iChartOfAccountsCategoryId, $sTargetSelectBox);	
    return($objResponse);
}

// End of Banking Module

/*Reports */

function AJAX_FMS_Reports_GetControlCode($iChartOfAccountsCategoryId, $sTargetSelectBox)
{
	global $objAccountReports;
	$objResponse = $objAccountReports->AJAX_GetControlCode($iChartOfAccountsCategoryId, $sTargetSelectBox);	
	return($objResponse);
}


function AJAX_FMS_Reports_GetChartOfAccounts($iControlId, $sTargetSelectBox)
{
	global $objAccountReports;
	$objResponse = $objAccountReports->AJAX_GetChartOfAccounts($iControlId, $sTargetSelectBox);	
	return($objResponse);
}



?>