<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_AccountsRegister.php');
$objAccountsRegister = new clsAccounts_AccountsRegister();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	/*if ($varAction == "UpdateAccountsRegister")
		$varError = $objAccountsRegister->UpdateAccountsRegister(
																	$objGeneral->fnGet("id"),
																	$objGeneral->fnGet("selAccountType"),
																	$objGeneral->fnGet("selCustomer"),
																	$objGeneral->fnGet("selVendor"),
																	$objGeneral->fnGet("txtReference"),
																	$objGeneral->fnGet("txtDescription"),
																	$objGeneral->fnGet("TotalDebit"),
																	$objGeneral->fnGet("TotalCredit"),
																	$objGeneral->fnGet("txtNotes"));*/
    if ($varAction == "AddNewAccountsRegister")
        $varError = $objAccountsRegister->AddNewAccountsRegister(
																	$objGeneral->fnGet("selAccountType"),
																	$objGeneral->fnGet("selCustomer"),
																	$objGeneral->fnGet("selVendor"),
																	$objGeneral->fnGet("txtReference"),
																	$objGeneral->fnGet("txtDescription"),
																	$objGeneral->fnGet("TotalDebit"),
																	$objGeneral->fnGet("TotalCredit"),
																	$objGeneral->fnGet("txtNotes"));
	//else if ($varAction == "DeleteAccountsRegister")
	//	$varError = $objAccountsRegister->DeleteAccountsRegister($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objAccountsRegister->AccountsRegisterDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>