<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_AccountsRegister.php');
$objAccountsRegister = new clsAccounts_AccountsRegister();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteAccountsRegister")
		$varError = $objAccountsRegister->DeleteAccountsRegister($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objAccountsRegister->ShowAllAccountsRegisters($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>