<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');
include('../../system/library/fms/clsFMS_Accounts_BudgetAllocation.php');
$objBudgetAllocation = new clsFMS_Accounts_BudgetAllocation();

$varAction = $objGeneral->fnGet("action");

if ($varAction != "")
{	
	if ($varAction == "decrease")
		$varError = $objBudgetAllocation->IncreaseOrDecreaseBudgetAllocation(
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("txtAmount"),
												$objGeneral->fnGet("txtBudgetAllocationTitle")
												);
	else if ($varAction == "increase")
		$varError = $objBudgetAllocation->IncreaseOrDecreaseBudgetAllocation(
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("txtAmount"),
												$objGeneral->fnGet("txtBudgetAllocationTitle")
												);
												
    else if ($varAction == "AddNewBudgetAllocation")
        $varError = $objBudgetAllocation->AddNewBudgetAllocation(
												$objGeneral->fnGet("selBudget"), 
												$objGeneral->fnGet("selChartOfAccount"),
												$objGeneral->fnGet("selStation"),
												$objGeneral->fnGet("txtBudgetAllocationTitle"), 
												$objGeneral->fnGet("selAllocationType"), 
												$objGeneral->fnGet("txtAmount"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
	  else if ($varAction == "UpdateBudgetAllocation")
        $varError = $objBudgetAllocation->UpdateBudgetAllocation(
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("selBudget"),
												$objGeneral->fnGet("selChartOfAccount"),
												$objGeneral->fnGet("selStation"),
												$objGeneral->fnGet("txtBudgetAllocationTitle"),
												$objGeneral->fnGet("selAllocationType"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBudgetAllocation")
		$varError = $objBudgetAllocation->DeleteBudgetAllocation($objGeneral->fnGet("id"));	
	else if($varAction == "changeamount")
		$varError = $objBudgetAllocation->ChangeAmount(
												$objGeneral->fnGet("id"),
												$objGeneral->fnGet("txtTitle"),
												$objGeneral->fnGet("txtAmount"),
												$objGeneral->fnGet("selChangeAllocationAmountType"),
												$objGeneral->fnGet("txtDescription"));		
		
}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objBudgetAllocation->ShowAllBudgetAllocation($objGeneral->fnGet("p")));
else if($sPageType == "details")
	print($objBudgetAllocation->BudgetAllocationDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
else if($sPageType == "changeallocationamount")	
	print($objBudgetAllocation->ShowAllocationAmount($objGeneral->fnGet("id")));
	
include('../include/bottom2.php');

?>