<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();
	
include('../../system/library/fms/clsFMS_Accounts_BudgetAllocation.php');
$objBudgetAllocation = new clsFMS_Accounts_BudgetAllocation();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "changeamount")
		$varError = $objBudgetAllocation->ChangeAmount(
											$objGeneral->fnGet("id"),
											$objGeneral->fnGet("txtTitle"),
											$objGeneral->fnGet("txtAmount"),
											$objGeneral->fnGet("selChangeAllocationAmountType"),
											$objGeneral->fnGet("txtDescription"));	
}

include('../include/top2.php');
print($objBudgetAllocation->ShowAllocationAmount($objGeneral->fnGet("id")));
include('../include/bottom2.php');
?>