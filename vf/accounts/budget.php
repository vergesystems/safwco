<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');


include('../../system/library/fms/clsFMS_Accounts_Budget.php');
$objBudget = new clsFMS_Accounts_Budget();

$varAction = $objGeneral->fnGet("action");

if ($varAction != "")
{	
	if ($varAction == "decrease")
		$varError = $objBudget->IncreaseOrDecreaseBudget(
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("txtTitle"), 
												$objGeneral->fnGet("txtBudgetCode"),
												$objGeneral->fnGet("txtAmount"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "increase")
		$varError = $objBudget->IncreaseOrDecreaseBudget(
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("txtTitle"), 
												$objGeneral->fnGet("txtBudgetCode"),
												$objGeneral->fnGet("txtAmount"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
												
    else if ($varAction == "AddNewBudget")
        $varError = $objBudget->AddNewBudget(												
												$objGeneral->fnGet("txtTitle"),
												$objGeneral->fnGet("txtBudgetCode"),
												$objGeneral->fnGet("txtAmount"), 
												$objGeneral->fnGet("txtBudgetStartDate"), 
												$objGeneral->fnGet("txtBudgetEndDate"), 
												$objGeneral->fnGet("selStatus"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateBudget")
        $varError = $objBudget->UpdateBudget(
												$objGeneral->fnGet("id"),
												$objGeneral->fnGet("txtTitle"),
												$objGeneral->fnGet("txtBudgetCode"),
												$objGeneral->fnGet("txtBudgetStartDate"), 
												$objGeneral->fnGet("txtBudgetEndDate"),
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBudget")
		$varError = $objBudget->DeleteBudget($objGeneral->fnGet("id"));
		
	else if ($varAction == "changeamount")
		$varError = $objBudget->ChangeAmount(	
												$objGeneral->fnGet("id"),
												$objGeneral->fnGet("txtTitle"),
												$objGeneral->fnGet("txtAmount"),
												$objGeneral->fnGet("selChangeBudgetAmountType"),
												$objGeneral->fnGet("txtDescription"));
}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objBudget->ShowAllBudgets($objGeneral->fnGet("p")));
else if ($sPageType == "details")
	print($objBudget->BudgetDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
else if ($sPageType == "changebudgetamount")
	print($objBudget->ShowBudgetAmount($objGeneral->fnGet("id")));
include('../include/bottom2.php');
?>