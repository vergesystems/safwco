<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_Budget.php');
$objBudget = new clsFMS_Accounts_Budget();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "changeamount")
		$varError = $objBudget->ChangeAmount(	
												$objGeneral->fnGet("id"),
												$objGeneral->fnGet("txtTitle"),
												$objGeneral->fnGet("txtAmount"),
												$objGeneral->fnGet("selChangeBudgetAmountType"),
												$objGeneral->fnGet("txtDescription"));	
}

include('../include/top2.php');
print($objBudget->ShowBudgetAmount($objGeneral->fnGet("id")));
include('../include/bottom2.php');
?>