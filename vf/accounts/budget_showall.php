<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_Budget.php');
$objBudget = new clsFMS_Accounts_Budget();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBudget")
		$varError = $objBudget->DeleteBudget($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objBudget->ShowAllBudgets($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>