<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');
	
include('../../system/library/fms/clsFMS_Accounts.php');
$objChartOfAccounts = new clsChartOfAccounts();

$varAction = $objGeneral->fnGet("action");

if ($varAction != "")
{

	if ($varAction == "AddNewChartOfAccount")
        $varError = $objChartOfAccounts->AddNewChartOfAccount(
																$objGeneral->fnGet("selCategory"),
																$objGeneral->fnGet("selControl"), 
																$objGeneral->fnGet("selChartOfAccountParent"),
																$objGeneral->fnGet("txtChartOfAccountsCode"), 
																$objGeneral->fnGet("txtAccountTitle"), 
																$objGeneral->fnGet("selChartOfAccountStatus"), 
																$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateChartOfAccount")
		$varError = $objChartOfAccounts->UpdateChartOfAccount(
																$objGeneral->fnGet("id"), 
																$objGeneral->fnGet("selCategory"),
																$objGeneral->fnGet("selControl"), 
																$objGeneral->fnGet("selChartOfAccountParent"),
																$objGeneral->fnGet("txtChartOfAccountsCode"), 
																$objGeneral->fnGet("txtAccountTitle"), 
																$objGeneral->fnGet("selChartOfAccountStatus"), 
																$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteChartOfAccount")
		$varError = $objChartOfAccounts->DeleteChartOfAccount($objGeneral->fnGet("id"));
}

include('../../system/library/fms/libAjax.php');

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objChartOfAccounts->ShowAllChartOfAccounts($objGeneral->fnGet("p")));
else if($sPageType == "details")
	print($objChartOfAccounts->ChartOfAccountDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("catid"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');
?>