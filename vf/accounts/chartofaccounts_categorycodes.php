<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	print('<script type="text/javascript" language="JavaScript">alert(\'Your session has ended, Please Login Again!\');window.close();</script>');

include('../../system/library/fms/clsFMS_Accounts.php');
$objChartOfAccounts = new clsChartOfAccounts();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "AddNewCategory")
		$varError = $objChartOfAccounts->AddNewCategory($objGeneral->fnGet("txtCategoryCode"), $objGeneral->fnGet("txtCategoryName"), $objGeneral->fnGet("txtStartRange"), $objGeneral->fnGet("txtEndRange"));
	else if ($varAction == "DeleteCategory")
		$varError = $objChartOfAccounts->DeleteCategory($objGeneral->fnGet("id"));		
	else if ($varAction == "UpdateCategory")
		$varError = $objChartOfAccounts->UpdateCategory($objGeneral->fnGet("categoryid"), $objGeneral->fnGet("txtStartRange"), $objGeneral->fnGet("txtEndRange"));	
}

include('../include/top2.php');

print('
<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
 <tr>
  <td>
   <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
    <tr>
     <td>
      ' . $objChartOfAccounts->ShowChartOfAccountsCategoryCodes($objGeneral->fnGet("id"), $objGeneral->fnGet("categoryid"), $objGeneral->fnGet("action2")) . '
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br />
');

include('../include/bottom2.php'); ?>