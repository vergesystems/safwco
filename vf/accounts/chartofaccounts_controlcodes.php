<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	print('<script type="text/javascript" language="JavaScript">alert(\'Your session has ended, Please Login Again!\');window.close();</script>');

include('../../system/library/fms/clsFMS_Accounts.php');
$objChartOfAccounts = new clsChartOfAccounts();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "AddNewControl")
		$varError = $objChartOfAccounts->AddNewControl($objGeneral->fnGet("catid"), $objGeneral->fnGet("txtControlCode"), $objGeneral->fnGet("txtControlName"), $objGeneral->fnGet("selParentControl"), $objGeneral->fnGet("txtStartRange"), $objGeneral->fnGet("txtEndRange"));
	else if ($varAction == "UpdateControl")
		$varError = $objChartOfAccounts->UpdateControl($objGeneral->fnGet("catid"), $objGeneral->fnGet("controlid"), $objGeneral->fnGet("txtControlCode"), $objGeneral->fnGet("txtControlName"), $objGeneral->fnGet("selParentControl"), $objGeneral->fnGet("txtStartRange"), $objGeneral->fnGet("txtEndRange"));	
	else if ($varAction == "DeleteControl")
		$varError = $objChartOfAccounts->DeleteControl($objGeneral->fnGet("catid"), $objGeneral->fnGet("controlid"));
		
}


include('../include/top2.php');

print('
<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
 <tr>
  <td>
   <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
    <tr>
     <td>
      ' . $objChartOfAccounts->ShowChartOfAccountsControlCodes($objGeneral->fnGet("id"), $objGeneral->fnGet("catid"), $objGeneral->fnGet("controlid"), $objGeneral->fnGet("action2")) . '
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br />
');

include('../include/bottom2.php'); ?>