<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts.php');
$objChartOfAccounts = new clsChartOfAccounts();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "GetControlCode")
		print($objChartOfAccounts->AjaxGetControlCodeById($objGeneral->fnGet("iChartOfAccountsCategoryId")));

	if ($varAction == "GetChartOfAccountsCode")
		print($objChartOfAccounts->AjaxGetChartOfAccountsCodeById($objGeneral->fnGet("iChartOfAccountsControlId")));

	if ($varAction == "AddNewChartOfAccount")
        $varError = $objChartOfAccounts->AddNewChartOfAccount(
																$objGeneral->fnGet("selCategory"),
																$objGeneral->fnGet("selControl"), 
																$objGeneral->fnGet("selChartOfAccountParent"),
																$objGeneral->fnGet("txtChartOfAccountsCode"), 
																$objGeneral->fnGet("txtAccountTitle"), 
																$objGeneral->fnGet("selChartOfAccountStatus"), 
																$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateChartOfAccount")
		$varError = $objChartOfAccounts->UpdateChartOfAccount(
																$objGeneral->fnGet("id"), 
																$objGeneral->fnGet("selCategory"),
																$objGeneral->fnGet("selControl"), 
																$objGeneral->fnGet("selChartOfAccountParent"),
																$objGeneral->fnGet("txtChartOfAccountsCode"), 
																$objGeneral->fnGet("txtAccountTitle"), 
																$objGeneral->fnGet("selChartOfAccountStatus"), 
																$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteChartOfAccount")
		$varError = $objChartOfAccounts->DeleteChartOfAccount($objGeneral->fnGet("id"));

}
include('../../system/library/fms/libAjax.php');
include('../include/top2.php');
print($objChartOfAccounts->ChartOfAccountDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("catid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?> 