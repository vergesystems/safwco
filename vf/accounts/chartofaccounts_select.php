<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	print('<script type="text/javascript" language="JavaScript">alert(\'Your session has ended, Please Login Again!\');window.close();</script>');

include('../../system/library/fms/clsFMS_Accounts.php');
$objChartOfAccounts = new clsChartOfAccounts();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "AddNewCategory")
        $varError = $objChartOfAccounts->AddNewCategory($objGeneral->fnGet("txtCategoryCode"), $objGeneral->fnGet("txtCategoryName"));
	else if ($varAction == "DeleteCategory")
		$varError = $objChartOfAccounts->DeleteCategory($objGeneral->fnGet("id"));
}


include('../include/top2.php');

print('
<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
 <tr>
  <td>
   <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
    <tr>
     <td>
      ' . $objChartOfAccounts->ShowChartOfAccountsSelect($objGeneral->fnGet("transactioncode"), $objGeneral->fnGet("p"), $objGeneral->fnGet("control")) . '
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br />
');

include('../include/bottom2.php'); ?>