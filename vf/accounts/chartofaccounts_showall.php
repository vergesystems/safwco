<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts.php');
$objChartOfAccounts = new clsChartOfAccounts();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteChartOfAccount")
		$varError = $objChartOfAccounts->DeleteChartOfAccount($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objChartOfAccounts->ShowAllChartOfAccounts($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>