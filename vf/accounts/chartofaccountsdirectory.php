<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_ChartOfAccounts.php');
$objChartOfAccounts = new clsChartOfAccounts();

include('../include/top2.php');

print($objChartOfAccounts->ChartOfAccountsDirectory($objGeneral->fnGet("p")));

include('../include/bottom2.php'); ?>