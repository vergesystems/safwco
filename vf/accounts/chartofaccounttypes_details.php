<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts.php');
$objChartOfAccountTypes = new clsChartOfAccountTypes();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "UpdateChartOfAccountType")
		$varError = $objChartOfAccountTypes->UpdateChartOfAccountType(
				$objGeneral->fnGet("id"),
				$objGeneral->fnGet("txtTitle"),
				$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "AddNewChartOfAccountType")
		$varError = $objChartOfAccountTypes->AddNewChartOfAccountType(
				$objGeneral->fnGet("txtTitle"), 			
				$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteChartOfAccountType")
		$varError = $objChartOfAccountTypes->DeleteChartOfAccountType($objGeneral->fnGet("id"));
}
include('../include/top2.php');
print($objChartOfAccountTypes->ChartOfAccountTypeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>