<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts.php');
$objChartOfAccountTypes = new clsChartOfAccountTypes();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteChartOfAccountTypes")
		$varError = $objChartOfAccountTypes->DeleteChartOfAccountType($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objChartOfAccountTypes->ShowAllChartOfAccountTypes($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>