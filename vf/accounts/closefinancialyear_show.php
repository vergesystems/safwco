<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts_CloseFinancialYear.php');
$objCloseFinancialYear = new clsCloseFinancialYear();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "CloseFinancialYear")
		$varError = $objCloseFinancialYear->CloseFinancialYear($objGeneral->fnGet("hdnTitle"), $objGeneral->fnGet("hdnOpeningDate"), $objGeneral->fnGet("hdnClosingDate"), $objGeneral->fnGet("hdnResetSeries"));
	else if ($varAction == "DeleteCloseFinancialYear")
		$varError = $objCloseFinancialYear->DeleteCloseFinancialYear($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objCloseFinancialYear->ShowCloseFinancialYear());
include('../include/bottom2.php');
?>