<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts_CloseFinancialYear.php');
$objCloseFinancialYears = new clsCloseFinancialYears();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteCloseFinancialYear")
		$varError = $objCloseFinancialYears->DeleteCloseFinancialYear($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objCloseFinancialYears->ShowAllCloseFinancialYears($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>