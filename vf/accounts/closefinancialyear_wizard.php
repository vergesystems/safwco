<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts_CloseFinancialYear.php');
$objCloseFinancialYear = new clsCloseFinancialYears();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{	
	$varAction = $objGeneral->fnGet("action");
	if ($varAction == "AddNewCloseFinancialYear")
        $varError = $objCloseFinancialYear->AddNewCloseFinancialYear(
																		$objGeneral->fnGet("txtTitle"), 
																		$objGeneral->fnGet("txtClosingDate"), 
																		$objGeneral->fnGet("txtDescription"));	
}


include('../include/top2.php');
print($objCloseFinancialYear->ShowCloseFinancialYearWizard($objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?> 