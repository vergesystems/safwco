<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_Projects.php');
$objProject = new clsProjects();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateProject")
		$varError = $objProject->UpdateProject(											
												$objGeneral->fnGet("Id"), 
												$objGeneral->fnGet("selDonor"), 
												$objGeneral->fnGet("txtProjectTitle"), 
												$objGeneral->fnGet("txtProjectCode"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("selStatus"), 
												$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewProject")
        $varError = $objProject->AddNewProject(	
												$objGeneral->fnGet("selDonor"), 
												$objGeneral->fnGet("txtProjectTitle"), 
												$objGeneral->fnGet("txtProjectCode"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("selStatus"), 
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteProject")
		$varError = $objProject->DeleteProject($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objProject->ProjectDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>