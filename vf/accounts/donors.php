<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts_Donors.php');
$objDonor = new clsDonors();
$objProject = new clsProjects();
$objProjectActivity = new clsProjectActivities();

$varAction = $objGeneral->fnGet("action");

if ($varAction != "")
{
	if ($varAction == "UpdateDonor")
		$varError = $objDonor->UpdateDonor(
											$objGeneral->fnGet("id"), 
											$objGeneral->fnGet("txtDonorName"), 
											$objGeneral->fnGet("txtDonorCode"), 
											$objGeneral->fnGet("txtDescription"), 
											$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewDonor")
        $varError = $objDonor->AddNewDonor(
											$objGeneral->fnGet("txtDonorName"), 
											$objGeneral->fnGet("txtDonorCode"), 
											$objGeneral->fnGet("txtDescription"), 
											$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteDonor")
		$varError = $objDonor->DeleteDonor($objGeneral->fnGet("id"));
		
	else if($varAction == "UpdateProject")
		$varError = $objProject->UpdateProject(
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("selDonor"), 
												$objGeneral->fnGet("txtProjectTitle"), 
												$objGeneral->fnGet("txtProjectCode"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("selStatus"), 
												$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewProject")
        $varError = $objProject->AddNewProject(	
												$objGeneral->fnGet("selDonor"), 
												$objGeneral->fnGet("txtProjectTitle"), 
												$objGeneral->fnGet("txtProjectCode"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("selStatus"), 
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteProject")
		$varError = $objProject->DeleteProject($objGeneral->fnGet("id"));	
	
	else if($varAction == "UpdateActivity")
		$varError = $objProjectActivity->UpdateProjectActivity(
																$objGeneral->fnGet("id"), 
																$objGeneral->fnGet("selDonorProject"), 
																$objGeneral->fnGet("txtActivityTitle"), 
																$objGeneral->fnGet("txtActivityCode"), 
																$objGeneral->fnGet("txtDescription"), 
																$objGeneral->fnGet("selStatus"), 
																$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewActivity")
        $varError = $objProjectActivity->AddNewProjectActivity(
																$objGeneral->fnGet("selDonorProject"), 
																$objGeneral->fnGet("txtActivityTitle"), 
																$objGeneral->fnGet("txtActivityCode"), 
																$objGeneral->fnGet("txtDescription"), 
																$objGeneral->fnGet("selStatus"), 
																$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteActivity")
		$varError = $objProjectActivity->DeleteProjectActivity($objGeneral->fnGet("id"));	

}

include('../include/top2.php');

$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objDonor->ShowAllDonors($objGeneral->fnGet("p")));
else if($sPageType == "details")
	print($objDonor->DonorDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));

else if($sPageType == "donorprojects")
	print($objProject->ShowAllProjects($objGeneral->fnGet("p")));
else if($sPageType == "donorprojects_details")	
	print($objProject->ProjectDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
	
else if($sPageType == "projectactivities")
	print($objProjectActivity->ShowAllProjectActivities($objGeneral->fnGet("p")));
else if($sPageType == "projectactivities_details")
	print($objProjectActivity->ProjectActivityDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');
?>