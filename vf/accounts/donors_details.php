<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_Donors.php');
$objDonor = new clsDonors();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateDonor")
		$varError = $objDonor->UpdateDonor(
											$objGeneral->fnGet("id"), 
											$objGeneral->fnGet("txtDonorName"), 
											$objGeneral->fnGet("txtDonorCode"), 
											$objGeneral->fnGet("txtDescription"), 
											$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewDonor")
        $varError = $objDonor->AddNewDonor(
											$objGeneral->fnGet("txtDonorName"), 
											$objGeneral->fnGet("txtDonorCode"), 
											$objGeneral->fnGet("txtDescription"), 
											$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteDonor")
		$varError = $objDonor->DeleteDonor($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objDonor->DonorDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>