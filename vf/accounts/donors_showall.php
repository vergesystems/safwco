<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_Donors.php');
$objDonor = new clsDonors();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteDonor")
		$varError = $objDonor->DeleteDonor($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objDonor->ShowAllDonors($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>