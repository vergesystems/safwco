<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts_GeneralJournal.php');
$objGeneralJournal = new clsAccounts_GeneralJournal();

$sAction = $objGeneral->fnGet("action");

if ($sAction != "")
{

	if ($sAction == "UpdateGeneralJournal")
        $varError = $objGeneralJournal->UpdateGeneralJournal(
															$objGeneral->fnGet("id"),
															$objGeneral->fnGet("selEntryType"),
															$objGeneral->fnGet("txtTransactionDate"),
															$objGeneral->fnGet("txtDescription"),
															$objGeneral->fnGet("TotalDebit"),
															$objGeneral->fnGet("TotalCredit"),
															$objGeneral->fnGet("txtNotes"),
															$objGeneral->fnGet("selPaymentType"),
															$objGeneral->fnGet("selReceiptType"),
															$objGeneral->fnGet("selBankPaymentType"),
															$objGeneral->fnGet("selCheckBook"),
															$objGeneral->fnGet("txtCheckNumber"),
															$objGeneral->fnGet("selBankAccount"),															
															$objGeneral->fnGet("popup"),
															$objGeneral->fnGet("selDonor"),
															$objGeneral->fnGet("selDonorProject"));

    else if($sAction == "AddNewGeneralJournal")
        $varError = $objGeneralJournal->AddNewGeneralJournal(
															$objGeneral->fnGet("selEntryType"),															
															$objGeneral->fnGet("txtTransactionDate"),
															$objGeneral->fnGet("txtDescription"),
															$objGeneral->fnGet("TotalDebit"),
															$objGeneral->fnGet("TotalCredit"),
															$objGeneral->fnGet("txtNotes"),
															$objGeneral->fnGet("selPaymentType"),
															$objGeneral->fnGet("selReceiptType"),
															$objGeneral->fnGet("selBankPaymentType"),
															$objGeneral->fnGet("selCheckBook"),
															$objGeneral->fnGet("txtCheckNumber"),
															$objGeneral->fnGet("selBankAccount"),															
															$objGeneral->fnGet("popup"),
															$objGeneral->fnGet("selDonor"),
															$objGeneral->fnGet("selDonorProject"));
	else if($sAction == "DeleteGeneralJournal")
		$varError = $objGeneralJournal->DeleteGeneralJournal($objGeneral->fnGet("id"));		
}

include('../../system/library/fms/libAjax.php');
include('../include/top2.php');

$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objGeneralJournal->ShowAllGeneralJournal($objGeneral->fnGet("p")));
else if ($sPageType == "details")
	print($objGeneralJournal->GeneralJournalDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2"),$objGeneral->fnGet("popup")));
include('../include/bottom2.php');

?>