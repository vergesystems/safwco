<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_GeneralJournal.php');
$objGeneralJournal = new clsAccounts_GeneralJournal();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if($varAction=='GetQuickEntry')
		 print($objGeneralJournal->AjaxLoadQuickEntryById($objGeneral->fnGet("iQuickEntryId")));

	if($varAction=='GetNextAvailableCheck')
		 print($objGeneralJournal->AjaxGetNextAvailableCheckById($objGeneral->fnGet("iCheckBookId")));
		 
	if($varAction=='LoadGeneralJournal')
		 print($objGeneralJournal->AjaxLoadGeneralJournalById($objGeneral->fnGet("iGeneralJournalId")));
		 	 

	 if ($varAction == "UpdateGeneralJournal")
        $varError = $objGeneralJournal->UpdateGeneralJournal(
															$objGeneral->fnGet("id"),
															$objGeneral->fnGet("selEntryType"),	
															$objGeneral->fnGet("txtTransactionDate"),
															$objGeneral->fnGet("txtDescription"),
															$objGeneral->fnGet("TotalDebit"),
															$objGeneral->fnGet("TotalCredit"),
															$objGeneral->fnGet("txtNotes"),
															$objGeneral->fnGet("selPaymentType"),
															$objGeneral->fnGet("selReceiptType"),
															$objGeneral->fnGet("selBankPaymentType"),
															$objGeneral->fnGet("selCheckBook"),
															$objGeneral->fnGet("txtCheckNumber"),
															$objGeneral->fnGet("selBankAccount"),
															$objGeneral->fnGet("popup"),
															$objGeneral->fnGet("selDonor"),
															$objGeneral->fnGet("selDonorProject"));

    else if($varAction == "AddNewGeneralJournal")
        $varError = $objGeneralJournal->AddNewGeneralJournal(
															$objGeneral->fnGet("selEntryType"),
															$objGeneral->fnGet("txtTransactionDate"),
															$objGeneral->fnGet("txtDescription"),
															$objGeneral->fnGet("TotalDebit"),
															$objGeneral->fnGet("TotalCredit"),
															$objGeneral->fnGet("txtNotes"),
															$objGeneral->fnGet("selPaymentType"),
															$objGeneral->fnGet("selReceiptType"),
															$objGeneral->fnGet("selBankPaymentType"),
															$objGeneral->fnGet("selCheckBook"),
															$objGeneral->fnGet("txtCheckNumber"),
															$objGeneral->fnGet("selBankAccount"),
															$objGeneral->fnGet("popup"),
															$objGeneral->fnGet("selDonor"),
															$objGeneral->fnGet("selDonorProject"));
	else if($varAction == "DeleteGeneralJournal")
		$varError = $objGeneralJournal->DeleteGeneralJournal($objGeneral->fnGet("id"));		
}
include('../../system/library/fms/libAjax.php');
include('../include/top2.php');
print($objGeneralJournal->GeneralJournalDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2"),$objGeneral->fnGet("popup")));
include('../include/bottom2.php');
?>