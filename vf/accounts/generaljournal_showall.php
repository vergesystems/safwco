<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_GeneralJournal.php');
$objGeneralJournal = new clsAccounts_GeneralJournal();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteGeneralJournal")
		$varError = $objGeneralJournal->DeleteGeneralJournal($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objGeneralJournal->ShowAllGeneralJournal($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>