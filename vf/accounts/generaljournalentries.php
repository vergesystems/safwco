<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts.php');
$objAccounts = new clsAccounts();

include('../include/top.php');

include(cVSFFolder . '/classes/clsDHTMLSuite.php');
$objDHTMLSuite = new clsDHTMLSuite();

$aTabs[0][0] = 'General Journal Entries';
$aTabs[0][1] = '../accounts/generaljournalentries_showall.php';

print($objDHTMLSuite->TabBar($aTabs, $objAccounts->ShowAccountsMenu()));
include('../include/bottom.php'); ?>