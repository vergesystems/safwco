<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_GeneralJournalEntries.php');
$objGeneralJournalEntry = new clsAccounts_GeneralJournalEntries();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateGeneralJournalEntry")
		$varError = $objGeneralJournalEntry->UpdateGeneralJournalEntry(											
																		$objGeneral->fnGet("generaljournalentryid"), 
																		$objGeneral->fnGet("chartofaccountid"), 
																		$objGeneral->fnGet("selEntryType"), 
																		$objGeneral->fnGet("txtDescription"), 
																		$objGeneral->fnGet("txtEntryDate"), 
																		$objGeneral->fnGet("txtAmount"));
    else if ($varAction == "AddNewGeneralJournalEntry")
        $varError = $objGeneralJournalEntry->AddNewGeneralJournalEntry(        									
																		$objGeneral->fnGet("chartofaccountid"), 
																		$objGeneral->fnGet("selEntryType"), 
																		$objGeneral->fnGet("txtDescription"), 
																		$objGeneral->fnGet("txtEntryDate"), 
																		$objGeneral->fnGet("txtAmount"));
	else if ($varAction == "DeleteGeneralJournalEntry")
		$varError = $objGeneralJournalEntry->DeleteGeneralJournalEntry($objGeneral->fnGet("generaljournalid"));

}

include('../include/top2.php');
print($objGeneralJournalEntry->GeneralJournalEntryDetails($objGeneral->fnGet("generaljournalid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>