<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_GeneralJournalEntries.php');
$objGeneralJournalEntry = new clsAccounts_GeneralJournalEntries();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteGeneralJournalEntry")
		$varError = $objGeneralJournalEntry->DeleteGeneralJournalEntry($objGeneral->fnGet("generaljournalid"));
}

include('../include/top2.php');
print($objGeneralJournalEntry->ShowAllGeneralJournalEntries($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>