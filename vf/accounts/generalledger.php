<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts_GeneralLedger.php');
$objGeneralLedger = new clsAccounts_GeneralLedger();

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objGeneralLedger->ShowAllGeneralLedger($objGeneral->fnGet("p")));
else if($sPageType == "details")	
	print($objGeneralLedger->GeneralLedgerDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>