<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts.php');
$objAccounts = new clsAccounts();

include('../include/top.php');
print($objAccounts->ShowAccountsPages($objGeneral->fnGet("page")));
include('../include/bottom.php');

?>