<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsAccounts_Products.php');
$objProducts = new clsAccounts_Products();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateProduct")
		$varError = $objProducts->UpdateProduct(											
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("txtProductName"), 
												$objGeneral->fnGet("txtProductCode"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("selProductStatus"), 
												$objGeneral->fnGet("txtNotes")
												);
    else if ($varAction == "AddNewProduct")
        $varError = $objProducts->AddNewProduct(
									        	$objGeneral->fnGet("txtProductName"), 
												$objGeneral->fnGet("txtProductCode"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("selProductStatus"), 
												$objGeneral->fnGet("txtNotes")
												);
	else if ($varAction == "DeleteProduct")
		$varError = $objProducts->DeleteProduct($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objProducts->ProductsDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>