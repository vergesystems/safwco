<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_Donors.php');
$objProjectActivity = new clsProjectActivities();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateActivity")
		$varError = $objProjectActivity->UpdateProjectActivity(
																$objGeneral->fnGet("id"), 
																$objGeneral->fnGet("selDonorProject"), 
																$objGeneral->fnGet("txtActivityTitle"), 
																$objGeneral->fnGet("txtActivityCode"), 
																$objGeneral->fnGet("txtDescription"), 
																$objGeneral->fnGet("selStatus"), 
																$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewActivity")
        $varError = $objProjectActivity->AddNewProjectActivity(
																$objGeneral->fnGet("selDonorProject"), 
																$objGeneral->fnGet("txtActivityTitle"), 
																$objGeneral->fnGet("txtActivityCode"), 
																$objGeneral->fnGet("txtDescription"), 
																$objGeneral->fnGet("selStatus"), 
																$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteActivity")
		$varError = $objProjectActivity->DeleteProjectActivity($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objProjectActivity->ProjectActivityDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>