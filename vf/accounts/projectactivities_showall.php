<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_Donors.php');
$objProjectActivity = new clsProjectActivities();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteProjectActivity")
		$varError = $objProjectActivity->DeleteProjectActivity($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objProjectActivity->ShowAllProjectActivities($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>