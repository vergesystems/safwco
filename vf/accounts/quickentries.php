<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts_QuickEntries.php');
$objQuickEntry = new clsAccounts_QuickEntries();

$sAction = $objGeneral->fnGet("action");

if ($sAction != "")
{
	if ($sAction == "UpdateQuickEntry")
        $varError = $objQuickEntry->UpdateQuickEntry(
														$objGeneral->fnGet("id"),
														$objGeneral->fnGet("txtTitle"),
														$objGeneral->fnGet("txtEntryOrder"),
														$objGeneral->fnGet("selEntryType"),
														$objGeneral->fnGet("selPaymentType"),
														$objGeneral->fnGet("selReceiptType"),
														$objGeneral->fnGet("selBankPaymentType"),
														$objGeneral->fnGet("selBank"),
														$objGeneral->fnGet("selCheckBook"),
														$objGeneral->fnGet("selBankAccount"),
														$objGeneral->fnGet("selStatus"));

    else if ($sAction == "AddNewQuickEntry")
        $varError = $objQuickEntry->AddNewQuickEntry(
														$objGeneral->fnGet("txtTitle"),
														$objGeneral->fnGet("txtEntryOrder"),
														$objGeneral->fnGet("selEntryType"),
														$objGeneral->fnGet("selPaymentType"),
														$objGeneral->fnGet("selReceiptType"),
														$objGeneral->fnGet("selBankPaymentType"),
														$objGeneral->fnGet("selBank"),
														$objGeneral->fnGet("selCheckBook"),
														$objGeneral->fnGet("selBankAccount"),
														$objGeneral->fnGet("selStatus"));
	else if ($sAction == "DeleteQuickEntry")
        $varError = $objQuickEntry->DeleteQuickEntry($objGeneral->fnGet("id"));
}

include('../../system/library/fms/libAjax.php');
include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objQuickEntry->ShowAllQuickEntries($objGeneral->fnGet("p")));
else if($sPageType == "details")
	print($objQuickEntry->QuickEntryDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');

?>