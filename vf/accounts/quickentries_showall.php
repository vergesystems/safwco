<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_QuickEntries.php');
$objQuickEntry = new clsAccounts_QuickEntries();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteQuickEntry")
		$varError = $objQuickEntry->DeleteQuickEntry($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objQuickEntry->ShowAllQuickEntries($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>