<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Accounts_QuickEntries.php');
$objQuickEntry = new clsAccounts_QuickEntries();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	

	if($varAction=='LoadQuickEntry')
		 print($objQuickEntry->AjaxLoadQuickEntryById($objGeneral->fnGet("iQuickEntryId")));

	if($varAction=='GetDonorProjects')
		 print($objQuickEntry->AjaxGetDonorProjectsById($objGeneral->fnGet("iDonorId")));

	if($varAction=='GetProjectActivities')
		 print($objQuickEntry->AjaxGetProjectActivitiesById($objGeneral->fnGet("iDonorProjectId")));	 	

	if ($varAction == "UpdateQuickEntry")
        $varError = $objQuickEntry->UpdateQuickEntry(
														$objGeneral->fnGet("id"),
														$objGeneral->fnGet("txtTitle"),
														$objGeneral->fnGet("txtEntryOrder"),
														$objGeneral->fnGet("selEntryType"),
														$objGeneral->fnGet("selPaymentType"),
														$objGeneral->fnGet("selReceiptType"),
														$objGeneral->fnGet("selBankPaymentType"),
														$objGeneral->fnGet("selBank"),
														$objGeneral->fnGet("selCheckBook"),
														$objGeneral->fnGet("selBankAccount"),
														$objGeneral->fnGet("selStatus"));

    else if ($varAction == "AddNewQuickEntry")
        $varError = $objQuickEntry->AddNewQuickEntry(
														$objGeneral->fnGet("txtTitle"),
														$objGeneral->fnGet("txtEntryOrder"),
														$objGeneral->fnGet("selEntryType"),
														$objGeneral->fnGet("selPaymentType"),
														$objGeneral->fnGet("selReceiptType"),
														$objGeneral->fnGet("selBankPaymentType"),
														$objGeneral->fnGet("selBank"),
														$objGeneral->fnGet("selCheckBook"),
														$objGeneral->fnGet("selBankAccount"),
														$objGeneral->fnGet("selStatus"));
	else if ($varAction == "DeleteQuickEntry")
        $varError = $objQuickEntry->DeleteQuickEntry($objGeneral->fnGet("id"));
}
include('../../system/library/fms/libAjax.php');
include('../include/top2.php');
print($objQuickEntry->QuickEntryDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>