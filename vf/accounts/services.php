<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');


include('../../system/library/fms/clsAccounts.php');
$objAccounts = new clsAccounts();

include('../include/top.php');

include(cVSFFolder . '/classes/clsDHTMLSuite.php');
$objDHTMLSuite = new clsDHTMLSuite();

$aTabs[0][0] = 'Services';
$aTabs[0][1] = '../accounts/services_showall.php';

print($objDHTMLSuite->TabBar($aTabs, $objAccounts->ShowAccountsMenu()));

include('../include/bottom.php'); 
?>