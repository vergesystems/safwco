<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsAccounts_Services.php');
$objServices = new clsAccounts_Services();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateService")
		$varError = $objServices->UpdateService(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("txtServiceName"),
        									$objGeneral->fnGet("txtServiceCode"),
											$objGeneral->fnGet("txtServiceFrequency"),
											$objGeneral->fnGet("txtServicePrice"),
        									$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("selServiceStatus"),
        									$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewService")
        $varError = $objServices->AddNewService(
        									$objGeneral->fnGet("txtServiceName"),
        									$objGeneral->fnGet("txtServiceCode"),
											$objGeneral->fnGet("txtServiceFrequency"),
											$objGeneral->fnGet("txtServicePrice"),
        									$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("selServiceStatus"),
        									$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteService")
		$varError = $objServices->DeleteService($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objServices->ServiceDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');

?>