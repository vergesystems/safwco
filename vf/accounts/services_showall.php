<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsAccounts_Services.php');
$objServices = new clsAccounts_Services();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteService")
		$varError = $objServices->DeleteService($objGeneral->fnGet("id"));
}

include('../include/top2.php'); 
print($objServices->ShowAllServices($objGeneral->fnGet("p")));
include('../include/bottom2.php');
 
?>