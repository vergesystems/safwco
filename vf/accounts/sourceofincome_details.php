<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts.php');
$objSourceOfIncome = new clsAccounts_SourceOfIncome();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateSourceOfIncome")
		$varError = $objSourceOfIncome->UpdateSourceOfIncome(
															$objGeneral->fnGet("id"),
															$objGeneral->fnGet("selSourceOfIncomeParent"),
    														$objGeneral->fnGet("txtTitle"),
    														$objGeneral->fnGet("txtDescription"),
    								   						$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewSourceOfIncome")
        $varError = $objSourceOfIncome->AddNewSourceOfIncome(
															$objGeneral->fnGet("selSourceOfIncomeParent"),
        													$objGeneral->fnGet("txtTitle"),
        													$objGeneral->fnGet("txtDescription"),
        								   					$objGeneral->fnGet("txtNotes"));
    if ($varAction == "DeleteSourceOfIncome")
		$varError = $objSourceOfIncome->DeleteSourceOfIncome(
										$objGeneral->fnGet("id"));
    	   						
	

}

include('../include/top2.php');
print($objSourceOfIncome->SourceOfIncomeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>
