<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Accounts.php');
$objSourceOfIncome = new clsAccounts_SourceOfIncome();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteSourceOfIncome")
		$varError = $objSourceOfIncome->DeleteSourceOfIncome($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objSourceOfIncome->ShowAllSourceOfIncomes($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>