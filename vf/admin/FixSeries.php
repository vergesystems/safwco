<?php
header("Expires: 0");
@set_time_limit("99999999999");		// Set Unlimited Timeout for the Script

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
	$objGeneral->fnRedirect('../login/?error=2002');

$dStartDate = $objGeneral->fnGet("start");
$dEndDate = $objGeneral->fnGet("end");

if (($dStartDate == "") && ($dEndDate == "")) die("start & end dates missing");


$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_series SET Series='0'");

$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA");
for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
{
	$iBankAccountId = $objDatabase->Result($varResult, $i, "BA.BankAccountId");

	// Payment	
	$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_series AS GJS WHERE GJS.BankAccountId='$iBankAccountId' AND GJS.EntryType='0'");
	if ($objDatabase->RowsNumber($varResult2) <= 0)
		$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_series (EntryType, PaymentType, ReceiptType, BankAccountId, Series) VALUES ('0', '0', '0', '$iBankAccountId', '0')");

	// Receipt
	$varResult2 = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal_series AS GJS WHERE GJS.BankAccountId='$iBankAccountId' AND GJS.EntryType='1'");
	if ($objDatabase->RowsNumber($varResult2) <= 0)
		$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_series (EntryType, PaymentType, ReceiptType, BankAccountId, Series) VALUES ('1', '0', '0', '$iBankAccountId', '0')");
}

$varResult = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_series (EntryType, PaymentType, ReceiptType, BankAccountId, Series) VALUES ('0', '1', '0', '0', '0')");
$varResult = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_series (EntryType, PaymentType, ReceiptType, BankAccountId, Series) VALUES ('1', '0', '1', '0', '0')");
$varResult = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_series (EntryType, PaymentType, ReceiptType, BankAccountId, Series) VALUES ('2', '0', '0', '0', '0')");

$aCounter[0] = 0;		// BPV
$aCounter[1] = 0;		// CPV
$aCounter[2] = 0;		// BRV
$aCounter[3] = 0;		// CRV
$aCounter[4] = 0;		// JV

$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal AS GJ WHERE (GJ.TransactionDate BETWEEN '$dStartDate' AND '$dEndDate') ORDER BY GJ.TransactionDate");
for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
{
	$iGeneralJournalId = $objDatabase->Result($varResult, $i, "GJ.GeneralJournalId");
	
	$iEntryType = $objDatabase->Result($varResult, $i, "GJ.EntryType");
	$iReceiptType = $objDatabase->Result($varResult, $i, "GJ.ReceiptType");
	$iPaymentType = $objDatabase->Result($varResult, $i, "GJ.PaymentType");
	$iBankAccountId = $objDatabase->Result($varResult, $i, "GJ.BankAccountId");

	$sReference = "";
	if (($iEntryType == 0) && ($iPaymentType == 0)) // BPV
	{
		$aCounter[0] = $aCounter[0] + 1;
		$sReference = "BPV" . $aCounter[0];
	}
	else if (($iEntryType == 0) && ($iPaymentType == 1)) // CPV
	{
		$aCounter[1] = $aCounter[1] + 1;
		$sReference = "CPV" . $aCounter[1];
	}
	else if (($iEntryType == 1) && ($iReceiptType == 0)) // BRV
	{
		$aCounter[2] = $aCounter[2] + 1;
		$sReference = "BRV" . $aCounter[2];
	}
	else if (($iEntryType == 1) && ($iReceiptType == 1)) // CRV
	{
		$aCounter[3] = $aCounter[3] + 1;
		$sReference = "CRV" . $aCounter[3];
	}
	else if ($iEntryType == 2) // JV
	{
		$aCounter[4] = $aCounter[4] + 1;
		$sReference = "JV" . $aCounter[4];
	}

	$varResult2 = $objDatabase->Query("UPDATE fms_accounts_generaljournal SET Reference='$sReference' WHERE GeneralJournalId='$iGeneralJournalId'");
}

// Banks Involved
$varResult = $objDatabase->Query("SELECT * FROM fms_banking_bankaccounts AS BA");
for ($i=0; $i < $objDatabase->RowsNumber($varResult); $i++)
{
	$iBankAccountId = $objDatabase->Result($varResult, $i, "BA.BankAccountId");
	
	$varResult2 = $objDatabase->Query("UPDATE fms_accounts_generaljournal_series SET Series='$aCounter[0]' WHERE EntryType='0' AND PaymentType='0' AND BankAccountId='$iBankAccountId'");
	$varResult2 = $objDatabase->Query("UPDATE fms_accounts_generaljournal_series SET Series='$aCounter[2]' WHERE EntryType='1' AND PaymentType='0' AND BankAccountId='$iBankAccountId'");
}


// No bank involved
$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_series SET Series='$aCounter[1]' WHERE EntryType='0' AND PaymentType='1'");
$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_series SET Series='$aCounter[3]' WHERE EntryType='1' AND PaymentType='1'");
$varResult = $objDatabase->Query("UPDATE fms_accounts_generaljournal_series SET Series='$aCounter[4]' WHERE EntryType='2'");
?>