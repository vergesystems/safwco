<?php
header("Expires: 0");
@set_time_limit("99999999999");		// Set Unlimited Timeout for the Script

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

$iAddedBy = 1;

include(cVSFFolder . '/classes/clsExcelReader.php');
$sExcelFileName = "data5.xls";

$data = new Spreadsheet_Excel_Reader();     // ExcelFile($filename, $encoding);
$data->setOutputEncoding('CP1251');         // Set output Encoding.
$data->read($sExcelFileName);

$j = 0;
$iNumOfColumns = $data->sheets[0]['numCols'];

// Loap for adding record into database
for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++)
{
	// Check for the first column, it should have some data
    if ($data->sheets[0]['cells'][$i][1] != '')
    {
      	for ($k=0; $k < $iNumOfColumns; $k++)
       		$aData[$j][$k] = $data->sheets[0]['cells'][$i][$k+1];
        $j++;
    }
}

for ($i=0; $i < count($aData); $i++)
{
	$varNow = $objGeneral->fnNow();

	$iSerialNumber = $aData[$i][0];			// A
	$dTransactionDate = $aData[$i][1];		// B
	$iEntryType = $aData[$i][2];			// C
	$iPaymentType = $aData[$i][3];			// D
	$iReceiptType = $aData[$i][4];			// E
	$iBankPaymentType = $aData[$i][5];		// F
	$sDescription = $aData[$i][6];			// G
	$iBudgetId = $aData[$i][7];				// H
	$sReference = $aData[$i][8];			// I
	$iBankAccountId = $aData[$i][9];		// J
	$iBankCheckbookId = $aData[$i][10];		// K
	$sBankCheckNumber = $aData[$i][11];		// L

	// Entries
	$iChartOfAccountsId1 = $aData[$i][12];	// M
	$iChartOfAccountsId2 = $aData[$i][13];	// N

	$sDetails1 = $aData[$i][14];			// O
	$sDetails2 = $aData[$i][15];			// P

	$iRecipient1 = $aData[$i][16];			// Q
	$iVendor1 = $aData[$i][17];				// R
	$iCustomer1 = $aData[$i][18];			// S
	$iEmployee1 = $aData[$i][19];			// T

	$iRecipient2 = $aData[$i][20];			// U
	$iVendor2 = $aData[$i][21];				// V
	$iCustomer2 = $aData[$i][22];			// W
	$iEmployee2 = $aData[$i][23];			// X

	$dDebit1 = $aData[$i][24];				// Y
	$dCredit1 = $aData[$i][25];				// Z

	$dDebit2 = $aData[$i][26];				// AA
	$dCredit2 = $aData[$i][27];				// AB

	$dTransactionDate = date("Y-m-d", strtotime($dTransactionDate));
	$dTotalDebit = $dDebit1 + $dDebit2;
	$dTotalCredit = $dCredit1 + $dCredit2;

	$sNotes = "";

	$varResult = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal
	(EntryType, BudgetId, TransactionDate, Reference, Description, TotalDebit, TotalCredit, Notes, PaymentType, ReceiptType, BankPaymentType, BankCheckBookId, BankAccountId, CheckNumber, Series, GeneralJournalAddedOn, GeneralJournalAddedBy)
	VALUES ('$iEntryType', '$iBudgetId', '$dTransactionDate', '$sReference', '$sDescription', '$dTotalDebit', '$dTotalCredit', '$sNotes', '$iPaymentType', '$iReceiptType', '$iBankPaymentType', '$iBankCheckBookId', '$iBankAccountId', '$sCheckNumber', '$iSeries', '$varNow', '$iAddedBy')");

	if ($objDatabase->AffectedRows($varResult) > 0)
	{
		$varResult = $objDatabase->Query("SELECT * FROM fms_accounts_generaljournal AS GJ WHERE GJ.TransactionDate='$dTransactionDate' AND GJ.TotalDebit='$dTotalDebit' AND GJ.TotalCredit='$dTotalCredit' AND GJ.GeneralJournalAddedOn='$varNow' ORDER BY GJ.GeneralJournalId DESC LIMIT 1");
		if ($objDatabase->RowsNumber($varResult) > 0)
		{
			$iGeneralJournalId = $objDatabase->Result($varResult, 0, "GJ.GeneralJournalId");

			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_entries
			(GeneralJournalId, ChartOfAccountsId, Recipient, VendorId, CustomerId, EmployeeId, Detail, Debit, Credit)
			VALUES ('$iGeneralJournalId', '$iChartOfAccountsId1', '$iRecipient1', '$iVendor1', '$iCustomer1', '$iEmployee1', '$sDetails1', '$dDebit1', '$dCredit1')");

			$varResult2 = $objDatabase->Query("INSERT INTO fms_accounts_generaljournal_entries
			(GeneralJournalId, ChartOfAccountsId, Recipient, VendorId, CustomerId, EmployeeId, Detail, Debit, Credit)
			VALUES ('$iGeneralJournalId', '$iChartOfAccountsId2', '$iRecipient2', '$iVendor2', '$iCustomer2', '$iEmployee2', '$sDetails2', '$dDebit2', '$dCredit2')");
		}
	}

	print($iSerialNumber . '<br />');
}
?>