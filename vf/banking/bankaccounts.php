<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_BankAccounts.php');
$objBankAccounts = new clsBanking_BankAccounts();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "AddNewAccount")
        $varError = $objBankAccounts->AddNewBankAccount(														
														$objGeneral->fnGet("selBank"), 
														$objGeneral->fnGet("selBankAccountType"),
														$objGeneral->fnGet("txtBankAccountNumber"),
														$objGeneral->fnGet("txtAccountTitle"),
														$objGeneral->fnGet("txtBranchName"),
														$objGeneral->fnGet("txtBranchCode"),
														$objGeneral->fnGet("txtBranchContactPerson"),											
														$objGeneral->fnGet("txtBranchAddress"),
        												$objGeneral->fnGet("txtBranchCity"),
        												$objGeneral->fnGet("txtBranchState"),
        												$objGeneral->fnGet("txtBranchZipCode"),
        												$objGeneral->fnGet("selCountry"),
        												$objGeneral->fnGet("txtBranchPhoneNumber"),        									
        												$objGeneral->fnGet("txtBranchEmailAddress"),
														$objGeneral->fnGet("selStation"),
														$objGeneral->fnGet("selChartOfAccount"),
														$objGeneral->fnGet("txtNotes"),
														$objGeneral->fnGet("selAccountStatus"));

	else if ($varAction == "UpdateAccount")
		$varError = $objBankAccounts->UpdateBankAccount(
														$objGeneral->fnGet("id"),
														$objGeneral->fnGet("selBank"), 
														$objGeneral->fnGet("selBankAccountType"), 
														$objGeneral->fnGet("txtBankAccountNumber"),
														$objGeneral->fnGet("txtAccountTitle"),
														$objGeneral->fnGet("txtBranchName"),
														$objGeneral->fnGet("txtBranchCode"),
														$objGeneral->fnGet("txtBranchContactPerson"),											
														$objGeneral->fnGet("txtBranchAddress"),
        												$objGeneral->fnGet("txtBranchCity"),
        												$objGeneral->fnGet("txtBranchState"),
        												$objGeneral->fnGet("txtBranchZipCode"),
        												$objGeneral->fnGet("selCountry"),
        												$objGeneral->fnGet("txtBranchPhoneNumber"),        									
        												$objGeneral->fnGet("txtBranchEmailAddress"),
                                                        $objGeneral->fnGet("selStation"),
														$objGeneral->fnGet("selChartOfAccount"),
														$objGeneral->fnGet("txtNotes"),
														$objGeneral->fnGet("selAccountStatus"));
	else if ($varAction == "DeleteBankAccount")
		$varError = $objBankAccounts->DeleteBankAccount($objGeneral->fnGet("id"));
}

include('../../system/library/fms/libAjax.php');
include('../include/top2.php');

$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objBankAccounts->ShowAllBankAccounts($objGeneral->fnGet("bankid"), $objGeneral->fnGet("stationid"), $objGeneral->fnGet("p")));
else if ($sPageType == "details")	
	print($objBankAccounts->BankAccountDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');

?>