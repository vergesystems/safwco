<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_BankAccounts.php');
$objBankAccount = new clsBanking_BankAccounts();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBankAccount")
		$varError = $objBankAccount->DeleteBankAccount($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objBankAccount->ShowAllBankAccounts($objGeneral->fnGet("bankid"), $objGeneral->fnGet("stationid"), $objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>