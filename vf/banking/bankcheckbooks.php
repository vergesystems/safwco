<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_BankCheckBooks.php');
$objBankCheckBooks = new clsBankCheckBooks();
$objCancelledChecks = new clsBankChecksCancelled();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "AddNewBankCheckBook")
        $varError = $objBankCheckBooks->AddNewBankCheckBook(
														$objGeneral->fnGet("selBankAccount"), 
														$objGeneral->fnGet("txtCheckPrefix"), 
														$objGeneral->fnGet("txtCheckNumberStart"), 
														$objGeneral->fnGet("txtCheckNumberEnd"),
														$objGeneral->fnGet("selCheckBookStatus"),
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateBankCheckBook")
		$varError = $objBankCheckBooks->UpdateBankCheckBook(
														$objGeneral->fnGet("id"), 
														$objGeneral->fnGet("selBankAccount"), 
														$objGeneral->fnGet("txtCheckPrefix"), 
														$objGeneral->fnGet("txtCheckNumberStart"), 
														$objGeneral->fnGet("txtCheckNumberEnd"),
														$objGeneral->fnGet("selCheckBookStatus"),
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBankCheckBook")
		$varError = $objBankCheckBooks->DeleteBankCheckBook($objGeneral->fnGet("id"));
		
	else if($varAction == "AddNewCancelledCheck")
        $varError = $objCancelledChecks->AddNewCancelledCheck(
															$objGeneral->fnGet("id"),
															$objGeneral->fnGet("txtCancelledCheckNumber"),
															$objGeneral->fnGet("txtNotes"));
			
	else if ($varAction == "UpdateCancelledCheck")
		$varError = $objCancelledChecks->UpdateCancelledCheck(
															$objGeneral->fnGet("id"),
															$objGeneral->fnGet("cancelledcheckid"),
															$objGeneral->fnGet("txtCancelledCheckNumber"),
															$objGeneral->fnGet("txtNotes")); 
														
	else if ($varAction == "DeleteCancelledCheck")
		$varError = $objCancelledChecks->DeleteCancelledCheck($objGeneral->fnGet("id"), $objGeneral->fnGet("cancelledcheckid"));
	
}

include('../../system/library/fms/libAjax.php');
include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
//print($sPageType);
//die();
if ($sPageType == "")
	print($objBankCheckBooks->ShowAllBankCheckBooks($objGeneral->fnGet("bankaccountid"), $objGeneral->fnGet("p")));
else if ($sPageType == "details")	
	print($objBankCheckBooks->BankCheckBookDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));

else if ($sPageType == "issuedchecks")
	print($objBankCheckBooks->ShowAllIssuedChecks($objGeneral->fnGet("checkbookid"), $objGeneral->fnGet("p")));	

else if ($sPageType == "cancelledchecks")
	print($objCancelledChecks->ShowAllCancelledChecks($objGeneral->fnGet("id"), $objGeneral->fnGet("p")));
else if ($sPageType == "cancelledchecks_details")	
	print($objCancelledChecks->CancelledCheckDetails($objGeneral->fnGet("id"),  $objGeneral->fnGet("cancelledcheckid"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');
?>