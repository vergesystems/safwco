<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_BankCheckBooks.php');
$objBankCheckBook = new clsBankCheckBooks();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "AddNewBankCheckBook")
        $varError = $objBankCheckBook->AddNewBankCheckBook(
														$objGeneral->fnGet("selBankAccount"), 
														$objGeneral->fnGet("txtCheckPrefix"), 
														$objGeneral->fnGet("txtCheckNumberStart"), 
														$objGeneral->fnGet("txtCheckNumberEnd"),
														$objGeneral->fnGet("selCheckBookStatus"),
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateBankCheckBook")
		$varError = $objBankCheckBook->UpdateBankCheckBook(
														$objGeneral->fnGet("id"), 
														$objGeneral->fnGet("selBankAccount"), 
														$objGeneral->fnGet("txtCheckPrefix"), 
														$objGeneral->fnGet("txtCheckNumberStart"), 
														$objGeneral->fnGet("txtCheckNumberEnd"),
														$objGeneral->fnGet("selCheckBookStatus"),
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBankCheckBook")
		$varError = $objBankCheckBook->DeleteBankCheckBook($objGeneral->fnGet("id"));

}
include('../../system/library/fms/libAjax.php');
include('../include/top2.php');
print($objBankCheckBook->BankCheckBookDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>