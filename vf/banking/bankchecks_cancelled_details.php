<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_Banks.php');
$objBankCheckCancelled = new clsBanking_BankChecksCancelled();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "AddNewBankCheckCancelled")
        $varError = $objBankCheckCancelled->AddNewBankCheckCancelled($objGeneral->fnGet("id"), $objGeneral->fnGet("txtCancelledCheckNumber"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateBankCheckCancelled")
		$varError = $objBankCheckCancelled->UpdateBankCheckCancelled($objGeneral->fnGet("id"), $objGeneral->fnGet("bankcheckcancelledid"), $objGeneral->fnGet("txtCancelledCheckNumber"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBankCheckCancelled")
		$varError = $objBankCheckCancelled->DeleteBankCheckCancelled($objGeneral->fnGet("id"), $objGeneral->fnGet("bankcheckcancelledid"));

}	

include('../include/top2.php');
print($objBankCheckCancelled->BankCheckCancelledDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("bankcheckcancelledid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>