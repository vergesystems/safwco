<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_Banks.php');
$objBankCheckCancelled = new clsBanking_BankChecksCancelled();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBankCheckCancelled")
		$varError = $objBankCheckCancelled->DeleteBankCheckCancelled($objGeneral->fnGet("id"), $objGeneral->fnGet("bankcheckcancelledid"));
}

include('../include/top2.php');
print($objBankCheckCancelled->ShowAllBankChecksCancelled($objGeneral->fnGet("id"), $objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>