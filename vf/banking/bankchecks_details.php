<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsBanking_Banks.php');
$objBankCheck = new clsBankChecks();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "AddNewBankCheck")
        $varError = $objBankCheck->AddNewBankCheck($objGeneral->fnGet("selBankAccount"), $objGeneral->fnGet("txtCheckPrefix"), $objGeneral->fnGet("txtCheckNumberStart"), $objGeneral->fnGet("txtCheckNumberEnd"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateBankCheck")
		$varError = $objBankCheck->UpdateBankCheck($objGeneral->fnGet("selStation"), $objGeneral->fnGet("bankcheckid"), $objGeneral->fnGet("selBankAccount"), $objGeneral->fnGet("txtCheckPrefix"), $objGeneral->fnGet("txtCheckNumberStart"), $objGeneral->fnGet("txtCheckNumberEnd"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBankCheck")
		$varError = $objBankCheck->DeleteBankCheck($objGeneral->fnGet("id"), $objGeneral->fnGet("bankcheckid"));

}
include('../../system/library/fms/libAjax.php');
include('../include/top2.php');
print($objBankCheck->BankCheckDetails($objGeneral->fnGet("accountid"), $objGeneral->fnGet("bankcheckid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>