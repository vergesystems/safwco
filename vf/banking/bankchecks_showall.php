<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsBanking_Banks.php');
$objBankCheck = new clsBankChecks();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBankCheck")
		$varError = $objBankCheck->DeleteBankCheck($objGeneral->fnGet("id"), $objGeneral->fnGet("bankcheckid"));
}

include('../include/top2.php');
print($objBankCheck->ShowAllBankChecks($objGeneral->fnGet("id"), $objGeneral->fnGet("stationid"), $objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>