<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Banking_BankDeposits.php');
$objBankDeposit = new clsBanking_BankDeposits();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateBankDeposit")
		$varError = $objBankDeposit->UpdateBankDeposit(											
														$objGeneral->fnGet("id"), 														
														$objGeneral->fnGet("selBankDepositType"),
														$objGeneral->fnGet("chkReconciliation"), 
														$objGeneral->fnGet("txtDescription"), 
														$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewBankDeposit")
        $varError = $objBankDeposit->AddNewBankDeposit(        									
														$objGeneral->fnGet("selBankAccountNumber"), 
														$objGeneral->fnGet("txtAmount"),
														$objGeneral->fnGet("txtBankDepositDate"),
														$objGeneral->fnGet("selBankDepositType"),
														$objGeneral->fnGet("selStatus"),														
														$objGeneral->fnGet("txtDescription"), 
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBankDeposit")
		$varError = $objBankDeposit->DeleteBankDeposit($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objBankDeposit->BankDepositDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>