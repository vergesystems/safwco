<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Banking_BankDeposits.php');
$objBankDeposit = new clsBanking_BankDeposits();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBankDeposit")
		$varError = $objBankDeposit->DeleteBankDeposit($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objBankDeposit->ShowAllBankDeposits($objGeneral->fnGet("bankaccountid"), $objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>