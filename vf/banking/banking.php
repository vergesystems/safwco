<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking.php');
$objBanks = new clsBanking();
include('../include/top.php');

include(cVSFFolder . '/classes/clsDHTMLSuite.php');
$objDHTMLSuite = new clsDHTMLSuite();

$aTabs[0][0] = 'Banks';
$aTabs[0][1] = '../banking/banks_showall.php';

print($objDHTMLSuite->TabBar($aTabs, $objBanks->ShowBanksMenu()));
include('../include/bottom.php');?>