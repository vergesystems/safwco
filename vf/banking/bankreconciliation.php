<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');
	
include('../../system/library/fms/clsFMS_Banking_BankReconciliation.php');
$objBankReconciliation = new clsBanking_BankReconciliation();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "SaveBankReconciliation")
		$varError = $objBankReconciliation->SaveBankReconciliation();
}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objBankReconciliation->ShowBankReconciliation($objGeneral->fnGet("action2")));
include('../include/bottom2.php'); 
?>