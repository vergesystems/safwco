<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
	$objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Banking_BankReconciliation.php');
$objBankReconciliation = new clsBanking_BankReconciliation();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "SaveBankReconciliation")
		$varError = $objBankReconciliation->SaveBankReconciliation();
	if ($varAction == "SaveBankReconciliation_Undo")
		$varError = $objBankReconciliation->SaveBankReconciliation_Undo();
}

include('../include/top2.php');
print($objBankReconciliation->ShowBankReconciliation($objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>