<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_Banks.php');
$objBanks = new clsBanking_Banks();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
		if ($varAction == "AddNewBank")
        $varError = $objBanks->AddNewBank(
											$objGeneral->fnGet("txtBankName"), 
											$objGeneral->fnGet("txtBankAbbreviation"),
											/*
											$objGeneral->fnGet("txtBranchName"),
											$objGeneral->fnGet("txtBranchCode"),
											$objGeneral->fnGet("txtBranchContactPerson"),											
											$objGeneral->fnGet("txtBranchAddress"),
        									$objGeneral->fnGet("txtBranchCity"),
        									$objGeneral->fnGet("txtBranchState"),
        									$objGeneral->fnGet("txtBranchZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtBranchPhoneNumber"),        									
        									$objGeneral->fnGet("txtBranchEmailAddress"),
											*/
        									$objGeneral->fnGet("selStatus"), 
											$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateBank")
		$varError = $objBanks->UpdateBank(
											$objGeneral->fnGet("id"), 
											$objGeneral->fnGet("txtBankName"), 
											$objGeneral->fnGet("txtBankAbbreviation"),
											/*
											$objGeneral->fnGet("txtBranchName"),
											$objGeneral->fnGet("txtBranchCode"),
											$objGeneral->fnGet("txtBranchContactPerson"),											
											$objGeneral->fnGet("txtBranchAddress"),
        									$objGeneral->fnGet("txtBranchCity"),
        									$objGeneral->fnGet("txtBranchState"),
        									$objGeneral->fnGet("txtBranchZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtBranchPhoneNumber"),        									
        									$objGeneral->fnGet("txtBranchEmailAddress"),
											*/
											$objGeneral->fnGet("selStatus"),
											$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBank")
		$varError = $objBanks->DeleteBank($objGeneral->fnGet("id"));
}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");

if ($sPageType == "")
	print($objBanks->ShowAllBanks($objGeneral->fnGet("p")));
else if ($sPageType == "details")	
	print($objBanks->BankDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');
?>