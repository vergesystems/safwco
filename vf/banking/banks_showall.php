<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_Banks.php');
$objBank = new clsBanking_Banks();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBank")
		$varError = $objBank->DeleteBank($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objBank->ShowAllBanks($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>