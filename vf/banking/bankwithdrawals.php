<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_BankWithdrawals.php');
$objBankWithdrawals = new clsBanking_BankWithdrawals();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBankWithdrawal")
		$varError = $objBankWithdrawals->DeleteBankWithdrawal($objGeneral->fnGet("id"));
}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objBankWithdrawals->ShowAllBankWithdrawals($objGeneral->fnGet("bankaccountid"), $objGeneral->fnGet("p")));
	
include('../include/bottom2.php');
?>