<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Banking_BankWithdrawals.php');
$objBankWithdrawal = new clsBanking_BankWithdrawals();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateBankWithdrawal")
		$varError = $objBankWithdrawal->UpdateBankWithdrawal(
																$objGeneral->fnGet("id"),																
																$objGeneral->fnGet("txtWithdrawBy"),																
																$objGeneral->fnGet("selWithdrawalType"),
																$objGeneral->fnGet("chkReconciliation"),
																$objGeneral->fnGet("txtDescription"), 																
																$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewBankWithdrawal")
        $varError = $objBankWithdrawal->AddNewBankWithdrawal(
																$objGeneral->fnGet("selBankAccount"), 
																$objGeneral->fnGet("selBankCheckBook"),
																$objGeneral->fnGet("txtBankCheckNumber"), 
																$objGeneral->fnGet("txtWithdrawBy"),
																$objGeneral->fnGet("txtAmount"), 
																$objGeneral->fnGet("txtWithdrawalDate"), 
																$objGeneral->fnGet("selWithdrawalType"), 
																$objGeneral->fnGet("txtDescription"), 
																$objGeneral->fnGet("selStatus"), 
																$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBankWithdrawal")
		$varError = $objBankWithdrawal->DeleteBankWithdrawal($objGeneral->fnGet("id"));

}
include('../../system/library/fms/libAjax.php');
include('../include/top2.php');
print($objBankWithdrawal->BankWithdrawalDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>