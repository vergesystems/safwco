<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_BankCheckBooks.php');
$objCancelledChecks = new clsBankChecksCancelled();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "AddNewCancelledCheck")
        $varError = $objCancelledChecks->AddNewCancelledCheck(
															$objGeneral->fnGet("id"),
															$objGeneral->fnGet("txtCancelledCheckNumber"),
															$objGeneral->fnGet("txtNotes"));
			
	else if ($varAction == "UpdateCancelledCheck")
		$varError = $objCancelledChecks->UpdateCancelledCheck(
															$objGeneral->fnGet("id"),
															$objGeneral->fnGet("cancelledcheckid"),
															$objGeneral->fnGet("txtCancelledCheckNumber"),
															$objGeneral->fnGet("txtNotes")); 
														
	else if ($varAction == "DeleteCancelledCheck")
		$varError = $objCancelledChecks->DeleteCancelledCheck($objGeneral->fnGet("id"), $objGeneral->fnGet("cancelledcheckid"));

}
include('../include/top2.php');
print($objCancelledChecks->CancelledCheckDetails($objGeneral->fnGet("id"),  $objGeneral->fnGet("cancelledcheckid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>