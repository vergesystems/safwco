<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_BankCheckBooks.php');
$objCancelledChecks = new clsBankChecksCancelled();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteCancelledCheck")
		$varError = $objCancelledChecks->DeleteCancelledCheck($objGeneral->fnGet("id"), $objGeneral->fnGet("cancelledcheckid"));
}

include('../include/top2.php');
print($objCancelledChecks->ShowAllCancelledChecks($objGeneral->fnGet("id"), $objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>