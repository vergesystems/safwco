<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking.php');
$objBanking = new clsBanking();

include('../include/top.php');

print($objBanking->ShowBankingPages($objGeneral->fnGet("page")));

include('../include/bottom.php'); 

?>