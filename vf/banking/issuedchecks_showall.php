<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Banking_BankCheckBooks.php');
$objBankCheckBooks = new clsBankCheckBooks();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBankCheckBook")
		$varError = $objBankCheckBooks->DeleteBankCheckBook($objGeneral->fnGet("id"), $objGeneral->fnGet("bankcheckid"));
}

include('../include/top2.php');
print($objBankCheckBooks->ShowAllIssuedChecks($objGeneral->fnGet("checkbookid"), $objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>