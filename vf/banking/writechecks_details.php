<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Banking_Banks.php');
$objWriteCheck = new clsBanking_WriteChecks();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateWriteCheck")
		$varError = $objWriteCheck->UpdateWriteCheck(											
														$objGeneral->fnGet("writecheckid"),
														$objGeneral->fnGet("bankcheckid"),
														$objGeneral->fnGet("selVendor"), 
														$objGeneral->fnGet("txtBankCheckNo"), 
														$objGeneral->fnGet("txtAmount"), 
														$objGeneral->fnGet("txtDescription"), 
														$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewWriteCheck")
        $varError = $objWriteCheck->AddNewWriteCheck(        									
														$objGeneral->fnGet("selVendor"),
														$objGeneral->fnGet("bankcheckid"), 
														$objGeneral->fnGet("txtBankCheckNo"), 
														$objGeneral->fnGet("txtAmount"), 
														$objGeneral->fnGet("txtDescription"), 
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteWriteCheck")
		$varError = $objWriteCheck->DeleteWriteCheck($objGeneral->fnGet("writecheckid"));

}

include('../include/top2.php');
print($objWriteCheck->WriteCheckDetails($objGeneral->fnGet("writecheckid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>