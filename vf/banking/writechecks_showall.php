<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Banking_Banks.php');
$objWriteCheck = new clsBanking_WriteChecks();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteWriteCheck")
		$varError = $objWriteCheck->DeleteWriteCheck($objGeneral->fnGet("writecheckid"));
}

include('../include/top2.php');
print($objWriteCheck->ShowAllWriteChecks($objGeneral->fnGet("bankcheckid"), $objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>