<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Budget_BudgetAllocation.php');
$objBudgetAllocation = new clsFMS_Budget_BudgetAllocation();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "decrease")
		$varError = $objBudgetAllocation->IncreaseOrDecreaseBudgetAllocation(
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("txtAmount"),
												$objGeneral->fnGet("txtBudgetAllocationTitle")
												);
	else if ($varAction == "increase")
		$varError = $objBudgetAllocation->IncreaseOrDecreaseBudgetAllocation(
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("txtAmount"),
												$objGeneral->fnGet("txtBudgetAllocationTitle")
												);
												
    else if ($varAction == "AddNewBudgetAllocation")
        $varError = $objBudgetAllocation->AddNewBudgetAllocation(
												$objGeneral->fnGet("selBudget"), 
												$objGeneral->fnGet("selChartOfAccount"), 
												$objGeneral->fnGet("txtBudgetAllocationTitle"), 
												$objGeneral->fnGet("txtAmount"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes")
												);
	  else if ($varAction == "UpdateBudgetAllocation")
        $varError = $objBudgetAllocation->AddNewBudgetAllocation(
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("selBudget"),
												$objGeneral->fnGet("selChartOfAccount"),
												$objGeneral->fnGet("txtBudgetAllocationTitle"),
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes")
												);											
	else if ($varAction == "DeleteBudgetAllocation")
		$varError = $objBudgetAllocation->DeleteBudgetAllocation($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objBudgetAllocation->BudgetAllocationDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>