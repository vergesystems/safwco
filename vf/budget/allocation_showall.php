<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Budget_BudgetAllocation.php');
$objBudgetAllocation = new clsFMS_Budget_BudgetAllocation();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBudgetAllocation")
		$varError = $objBudgetAllocation->DeleteBudgetAllocation($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objBudgetAllocation->ShowAllBudgetAllocation($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>