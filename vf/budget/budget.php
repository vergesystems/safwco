<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Budget.php');
$objBudget = new clsFMS_Budget();

include('../include/top.php');

include(cVSFFolder . '/classes/clsDHTMLSuite.php');
$objDHTMLSuite = new clsDHTMLSuite();

$aTabs[0][0] = 'Budget';
$aTabs[0][1] = '../budget/budget_showall.php';

print($objDHTMLSuite->TabBar($aTabs, $objBudget->ShowBudgetMenu()));
include('../include/bottom.php'); ?>