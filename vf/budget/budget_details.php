<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Budget_Budget.php');
$objBudget = new clsFMS_Budget_Budget();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "decrease")
		$varError = $objBudget->IncreaseOrDecreaseBudget(											
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("txtTitle"), 
												$objGeneral->fnGet("txtAmount"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes")
												);
	else if ($varAction == "increase")
		$varError = $objBudget->IncreaseOrDecreaseBudget(											
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("txtTitle"), 
												$objGeneral->fnGet("txtAmount"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes")
												);
												
    else if ($varAction == "AddNewBudget")
        $varError = $objBudget->AddNewBudget(
												$objGeneral->fnGet("selDonorProject"), 
												$objGeneral->fnGet("txtTitle"), 
												$objGeneral->fnGet("txtAmount"), 
												$objGeneral->fnGet("txtBudgetStartDate"), 
												$objGeneral->fnGet("txtBudgetEndDate"), 
												$objGeneral->fnGet("selStatus"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes")
												);
	else if ($varAction == "DeleteBudget")
		$varError = $objBudget->DeleteBudget($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objBudget->BudgetDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>