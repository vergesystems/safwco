<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Budgets.php');
$objInventory = new clsInventory();

include('../include/top.php');

include(cVSFFolder . '/classes/clsDHTMLSuite.php');
$objDHTMLSuite = new clsDHTMLSuite();

$aTabs[0][0] = 'Products';
$aTabs[0][1] = '../budgets/budgets_showall.php';

print($objDHTMLSuite->TabBar($aTabs, $objInventory->ShowBudget()));
include('../include/bottom.php'); ?>