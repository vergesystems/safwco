<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Common_Documents.php');
$objDocuments = new clsFMS_Common_Documents();

print($objDocuments->DocumentDownload($objGeneral->fnGet("componentname"), $objGeneral->fnGet("id"), $objGeneral->fnGet("documentid")));
?>