<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Common_Documents.php');
$objDocuments = new clsFMS_Common_Documents();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "AddDocument")
		$varError = $objDocuments->AddDocument($objGeneral->fnGet("componentname"), $objGeneral->fnGet("id"), $objGeneral->fnGet("txtTitle"), $objGeneral->fnGet("iAccessLevel"), $objGeneral->fnGet("txtFileName"));
	else if ($varAction == "DeleteDocument")
		$varError = $objDocuments->DeleteDocument($objGeneral->fnGet("componentname"), $objGeneral->fnGet("id"), $objGeneral->fnGet("documentid"));
}

include('../include/top2.php');
print($objDocuments->ShowDocuments($objGeneral->fnGet("componentname"), $objGeneral->fnGet("id")));
include('../include/bottom2.php');
?>