<?php 
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Common_Documents.php');
$objDocuments = new clsFMS_Common_Documents();

if (!empty($_FILES))
{
	$objDocuments->UploadFile();
}
else
{	
	include('../include/top2.php');
	print($objDocuments->ShowUpload());
	include('../include/bottom2.php');
}
?>