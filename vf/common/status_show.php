<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Common_Status.php');
$objStatus = new clsFMS_Common_Status();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "AddStatus")
		$varError = $objStatus->AddStatus($objGeneral->fnGet("componentname"), $objGeneral->fnGet("id"), $objGeneral->fnGet("selStatus"), $objGeneral->fnGet("txtComments"));
}

include('../include/top2.php');
print($objStatus->ShowStatus($objGeneral->fnGet("componentname"), $objGeneral->fnGet("id")));
include('../include/bottom2.php');
?>