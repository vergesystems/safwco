<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Customers.php');
$objCustomers = new clsCustomers_Customers();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateCustomer")
		$varError = $objCustomers->UpdateCustomer(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("txtFirstName"),
        									$objGeneral->fnGet("txtLastName"),
        									$objGeneral->fnGet("txtNICNumber"),
        									$objGeneral->fnGet("selGender"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtMobileNumber"),
        									$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtFaxNumber"),
        									$objGeneral->fnGet("selStatus"),
        									$objGeneral->fnGet("selStation"),
											$objGeneral->fnGet("txtCompanyName"),
        									$objGeneral->fnGet("txtDesignation"),
        									$objGeneral->fnGet("txtCompanyWebsite"),
        									$objGeneral->fnGet("txtNotes"));						        									       									
        								   	
    else if ($varAction == "AddNewCustomer")
        $varError = $objCustomers->AddNewCustomer(
        									$objGeneral->fnGet("txtFirstName"),
        									$objGeneral->fnGet("txtLastName"),
        									$objGeneral->fnGet("txtNICNumber"),
        									$objGeneral->fnGet("selGender"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtMobileNumber"),
        									$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtFaxNumber"),
        									$objGeneral->fnGet("selStatus"),
        									$objGeneral->fnGet("selStation"),
											$objGeneral->fnGet("txtCompanyName"),
        									$objGeneral->fnGet("txtDesignation"),
        									$objGeneral->fnGet("txtCompanyWebsite"),
        									$objGeneral->fnGet("txtNotes"));
        									
	else if ($varAction == "DeleteCustomer")
		$varError = $objCustomers->DeleteCustomer($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objCustomers->CustomerDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');

?>