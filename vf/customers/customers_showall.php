<?php include('../include/includes.php');

	if (!$objEmployee->EmployeeVerify())		// Verify the Store
    	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Customers.php');
$objCustomers = new clsCustomers_Customers();

	if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
	{
		$varAction = $objGeneral->fnGet("action");
	
		if ($varAction == "DeleteCustomer")
			$varError = $objCustomers->DeleteCustomer($objGeneral->fnGet("id"));
	}

include('../include/top2.php');
print($objCustomers->ShowAllCustomers($objGeneral->fnGet("stationid"), $objGeneral->fnGet("p")));
include('../include/bottom2.php');

?>