<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Customers.php');
$objCustomer = new clsCustomers();

include('../include/top.php');

print($objCustomer->ShowCustomersPages($objGeneral->fnGet("page")));

include('../include/bottom.php'); 

?>