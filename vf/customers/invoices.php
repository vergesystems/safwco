<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Customers.php');

$objInvoice = new clsCustomers_Invoices();
$objRecurringInvoice = new clsCustomers_RecurringInvoices();

$varAction = $objGeneral->fnGet("action");
if ($varAction != "")
{
	if ($varAction == "UpdateInvoice")
		$varError = $objInvoice->UpdateInvoice(											
												$objGeneral->fnGet("id"),
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("selSalesOrder"),
												$objGeneral->fnGet("txtInvoiceNo"),
												$objGeneral->fnGet("txtInvoiceDate"),
												$objGeneral->fnGet("OrderTotalAmount"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("selStatus"),
												$objGeneral->fnGet("txtNotes"));
			
    else if ($varAction == "AddNewInvoice")
        $varError = $objInvoice->AddNewInvoice(			 
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("selSalesOrder"),
												$objGeneral->fnGet("txtInvoiceNo"),
												$objGeneral->fnGet("txtInvoiceDate"),
												$objGeneral->fnGet("OrderTotalAmount"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("selStatus"),
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteInvoice")
		$varError = $objInvoice->DeleteInvoice($objGeneral->fnGet("id"));
	
	else if($varAction == "UpdateRecurringInvoice")
		$varError = $objRecurringInvoice->UpdateRecurringInvoice(
												$objGeneral->fnGet("id"),
												$objGeneral->fnGet("txtTitle"),
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("txtInvoiceDateOfMonth"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_1"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_2"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_3"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_4"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_5"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_6"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_7"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_8"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_9"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_10"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_11"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_12"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("selStatus"));
			
    else if ($varAction == "AddNewRecurringInvoice")
        $varError = $objRecurringInvoice->AddNewRecurringInvoice(
												$objGeneral->fnGet("txtTitle"),
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("txtInvoiceDateOfMonth"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_1"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_2"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_3"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_4"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_5"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_6"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_7"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_8"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_9"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_10"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_11"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_12"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("selStatus"));
	else if ($varAction == "DeleteRecurringInvoice")
		$varError = $objRecurringInvoice->DeleteRecurringInvoice($objGeneral->fnGet("id"));	

}

include('../include/top2.php');

$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objInvoice->ShowAllInvoices($objGeneral->fnGet("p"), $objGeneral->fnGet("status")));
else if($sPageType == "details")	
	print($objInvoice->InvoiceDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("salesorderid") , $objGeneral->fnGet("action2")));
	
else if($sPageType == "recurringinvoices")	
	print($objRecurringInvoice->ShowAllRecurringInvoices($objGeneral->fnGet("p"), $objGeneral->fnGet("status")));
else if($sPageType == "recurringinvoices_details")
	print($objRecurringInvoice->RecurringInvoiceDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("salesorderid") , $objGeneral->fnGet("action2")));

include('../include/bottom2.php');

?>