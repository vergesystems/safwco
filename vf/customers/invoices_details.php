<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers.php');
$objInvoice = new clsCustomers_Invoices();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateInvoice")
		$varError = $objInvoice->UpdateInvoice(
											
												$objGeneral->fnGet("id"),
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("selSalesOrder"),
												$objGeneral->fnGet("txtInvoiceNo"),
												$objGeneral->fnGet("txtInvoiceDate"),
												$objGeneral->fnGet("OrderTotalAmount"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("selStatus"),
												$objGeneral->fnGet("txtNotes"));
			
    else if ($varAction == "AddNewInvoice")
        $varError = $objInvoice->AddNewInvoice(			 
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("selSalesOrder"),
												$objGeneral->fnGet("txtInvoiceNo"),
												$objGeneral->fnGet("txtInvoiceDate"),
												$objGeneral->fnGet("OrderTotalAmount"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("selStatus"),
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteInvoice")
		$varError = $objInvoice->DeleteInvoice($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objInvoice->InvoiceDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("salesorderid") , $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>