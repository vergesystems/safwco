<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers.php');
$objInvoice = new clsCustomers_Invoices(); 

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteInvoice")
		$varError = $objInvoice->DeleteInvoice($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objInvoice->ShowAllInvoices($objGeneral->fnGet("p"), $objGeneral->fnGet("status")));
include('../include/bottom2.php');
?>