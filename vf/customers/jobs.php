<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');
	
include('../../system/library/fms/clsFMS_Customers.php');
$objJobs = new clsCustomers_Jobs();

$varAction = $objGeneral->fnGet("action");

if ($varAction != "")
{
	if ($varAction == "UpdateJob")
		$varError = $objJobs->UpdateJob(											
										$objGeneral->fnGet("id"),
										$objGeneral->fnGet("txtSupervisor"),
										$objGeneral->fnGet("hdnCustomerId"),
										$objGeneral->fnGet("txtTitle"), 
										$objGeneral->fnGet("txtStartDate"), 
										$objGeneral->fnGet("txtEndDate"), 
										$objGeneral->fnGet("txtDescription"), 
										$objGeneral->fnGet("selJobStatus"), 
										$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewJob")
        $varError = $objJobs->AddNewJob(
										$objGeneral->fnGet("txtSupervisor"),
										$objGeneral->fnGet("hdnCustomerId"),
										$objGeneral->fnGet("txtTitle"), 
										$objGeneral->fnGet("txtStartDate"), 
										$objGeneral->fnGet("txtEndDate"), 
										$objGeneral->fnGet("txtDescription"), 
										$objGeneral->fnGet("selJobStatus"), 
										$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteJob")
		$varError = $objJobs->DeleteJob($objGeneral->fnGet("id"));

}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objJobs->ShowAllJobs($objGeneral->fnGet("p")));
else if($sPageType == "details")
	print($objJobs->JobDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');

?>