<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers.php');
$objJobs = new clsCustomers_Jobs();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateJob")
		$varError = $objJobs->UpdateJob(											
										$objGeneral->fnGet("id"),
										$objGeneral->fnGet("txtSupervisor"),			
										$objGeneral->fnGet("selCustomer"), 
										$objGeneral->fnGet("txtTitle"), 
										$objGeneral->fnGet("txtStartDate"), 
										$objGeneral->fnGet("txtEndDate"), 
										$objGeneral->fnGet("txtDescription"), 
										$objGeneral->fnGet("selJobStatus"), 
										$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewJob")
        $varError = $objJobs->AddNewJob(        									
										$objGeneral->fnGet("txtSupervisor"),			
										$objGeneral->fnGet("selCustomer"), 
										$objGeneral->fnGet("txtTitle"), 
										$objGeneral->fnGet("txtStartDate"), 
										$objGeneral->fnGet("txtEndDate"), 
										$objGeneral->fnGet("txtDescription"), 
										$objGeneral->fnGet("selJobStatus"), 
										$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteJob")
		$varError = $objJobs->DeleteJob($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objJobs->JobDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>