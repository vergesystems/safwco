<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers.php');
$objJobs = new clsCustomers_Jobs();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteJob")
		$varError = $objJobs->DeleteJob($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objJobs->ShowAllJobs($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>