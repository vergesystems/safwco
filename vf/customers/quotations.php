<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Customers_Quotations.php');
$objQuotation = new clsQuotations();

$varAction = $objGeneral->fnGet("action");
if ($varAction != "")
{
	
	if ($varAction == "UpdateQuotation")
		$varError = $objQuotation->UpdateQuotation(											
													$objGeneral->fnGet("quotationid"), 
													$objGeneral->fnGet("hdnCustomerId"),
													$objGeneral->fnGet("txtExpiryDate"), 
													$objGeneral->fnGet("txtQuotationDate"),
													$objGeneral->fnGet("txtQuotationNo"),
													$objGeneral->fnGet("hdnQuotationItems"), 
													$objGeneral->fnGet("txtDescription"),
													$objGeneral->fnGet("OrderTotalAmount"),
													$objGeneral->fnGet("txtNotes"),
													$objGeneral->fnGet("selQuotationtStatus"));
    else if ($varAction == "AddNewQuotation")
		$varError = $objQuotation->AddNewQuotation(
													$objGeneral->fnGet("hdnCustomerId"), 
													$objGeneral->fnGet("txtExpiryDate"), 
													$objGeneral->fnGet("txtQuotationDate"), 
													$objGeneral->fnGet("txtQuotationNo"), 
													$objGeneral->fnGet("hdnQuotationItems"), 
													$objGeneral->fnGet("txtDescription"),
													$objGeneral->fnGet("OrderTotalAmount"),
													$objGeneral->fnGet("txtNotes"),
													$objGeneral->fnGet("selQuotationtStatus"));
													
	else if ($varAction == "DeleteQuotation")
		$varError = $objQuotation->DeleteQuotation($objGeneral->fnGet("quotationid"));
}

include('../include/top2.php');

$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objQuotation->ShowAllQuotations($objGeneral->fnGet("p"), $objGeneral->fnGet("status")));
else if ($sPageType == "details")	
	print($objQuotation->QuotationDetails($objGeneral->fnGet("quotationid"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');

?>