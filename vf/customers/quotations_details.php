<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers_Quotations.php');
$objQuotation = new clsQuotations();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateQuotation")
		$varError = $objQuotation->UpdateQuotation(											
													$objGeneral->fnGet("quotationid"), 
													$objGeneral->fnGet("hdnCustomerId"),
													$objGeneral->fnGet("txtExpiryDate"), 
													$objGeneral->fnGet("txtQuotationDate"),
													$objGeneral->fnGet("txtQuotationNo"),
													$objGeneral->fnGet("hdnQuotationItems"), 
													$objGeneral->fnGet("txtDescription"),
													$objGeneral->fnGet("OrderTotalAmount"),
													$objGeneral->fnGet("txtNotes"),
													$objGeneral->fnGet("selQuotationtStatus"));
    else if ($varAction == "AddNewQuotation")
		$varError = $objQuotation->AddNewQuotation(
													$objGeneral->fnGet("hdnCustomerId"), 
													$objGeneral->fnGet("txtExpiryDate"), 
													$objGeneral->fnGet("txtQuotationDate"), 
													$objGeneral->fnGet("txtQuotationNo"), 
													$objGeneral->fnGet("hdnQuotationItems"), 
													$objGeneral->fnGet("txtDescription"),
													$objGeneral->fnGet("OrderTotalAmount"),
													$objGeneral->fnGet("txtNotes"),
													$objGeneral->fnGet("selQuotationtStatus"));
													
	else if ($varAction == "DeleteQuotation")
		$varError = $objQuotation->DeleteQuotation($objGeneral->fnGet("quotationid"));

}

include('../include/top2.php');
print($objQuotation->QuotationDetails($objGeneral->fnGet("quotationid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>