<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');
	
include('../../system/library/fms/clsFMS_Customers.php');
$objReceipt = new clsReceipts();

$varAction = $objGeneral->fnGet("action");
if ($varAction != "")
{
	if ($varAction == "UpdateReceipt")
		$varError = $objReceipt->UpdateReceipt(
												$objGeneral->fnGet("receiptid"),
												$objGeneral->fnGet("txtReceiptNo"),
												$objGeneral->fnGet("OrderTotalAmount"),
												$objGeneral->fnGet("selPaymentMethod"),
												$objGeneral->fnGet("txtReceiptDate"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewReceipt")
        $varError = $objReceipt->AddNewReceipt(
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("selInvoice"),
												$objGeneral->fnGet("txtReceiptNo"),
												$objGeneral->fnGet("OrderTotalAmount"),
												$objGeneral->fnGet("selPaymentMethod"),
												$objGeneral->fnGet("txtReceiptDate"),
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteReceipt")
		$varError = $objReceipt->DeleteReceipt($objGeneral->fnGet("receiptid"));

}

include('../include/top2.php');

$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objReceipt->ShowAllReceipts($objGeneral->fnGet("p"), $objGeneral->fnGet("status")));
else if ($sPageType == "details")
	print($objReceipt->ReceiptDetails($objGeneral->fnGet("receiptid"), $objGeneral->fnGet("invoiceid"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');
?>