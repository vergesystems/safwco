<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers_Receipts.php');
$objReceipt = new clsReceipts();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateReceipt")
		$varError = $objReceipt->UpdateReceipt(
												$objGeneral->fnGet("receiptid"),
												$objGeneral->fnGet("txtReceiptNo"),
												$objGeneral->fnGet("OrderTotalAmount"),
												$objGeneral->fnGet("selPaymentMethod"),
												$objGeneral->fnGet("txtReceiptDate"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewReceipt")
        $varError = $objReceipt->AddNewReceipt(
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("selInvoice"),
												$objGeneral->fnGet("txtReceiptNo"),
												$objGeneral->fnGet("OrderTotalAmount"),
												$objGeneral->fnGet("selPaymentMethod"),
												$objGeneral->fnGet("txtReceiptDate"),
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteReceipt")
		$varError = $objReceipt->DeleteReceipt($objGeneral->fnGet("receiptid"));

}

include('../include/top2.php');
print($objReceipt->ReceiptDetails($objGeneral->fnGet("receiptid"), $objGeneral->fnGet("invoiceid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>