<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers_Receipts.php');
$objReceipt = new clsReceipts();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteReceipt")
		$varError = $objReceipt->DeleteReceipt($objGeneral->fnGet("receiptid"));
}

include('../include/top2.php');
print($objReceipt->ShowAllReceipts($objGeneral->fnGet("p"), $objGeneral->fnGet("status")));
include('../include/bottom2.php');
?>