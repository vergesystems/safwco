<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Customers.php');
$objCustomers = new clsCustomers();

include('../include/top.php');

include(cVSFFolder . '/classes/clsDHTMLSuite.php');
$objDHTMLSuite = new clsDHTMLSuite();

$aTabs[0][0] = 'Received Payments';
$aTabs[0][1] = '../customers/receivedpayments_showall.php';

print($objDHTMLSuite->TabBar($aTabs, $objCustomers->ShowCustomersMenu()));
include('../include/bottom.php'); ?>