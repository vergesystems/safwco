<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers_ReceivedPayments.php');
$objReceivedPayment = new clsReceivedPayments();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateReceivedPayment")
		$varError = $objReceivedPayment->UpdateReceivedPayment(											
													$objGeneral->fnGet("receivedpaymentid"),
													$objGeneral->fnGet("selInvoice"),
													$objGeneral->fnGet("txtReference"),
													$objGeneral->fnGet("txtReceiptNo"),
													$objGeneral->fnGet("txtAmount"),
													$objGeneral->fnGet("selPaymentMethod"),
													$objGeneral->fnGet("txtDueDate"),
													$objGeneral->fnGet("txtDiscountDate"),
													$objGeneral->fnGet("txtAppliedCredit"),
													$objGeneral->fnGet("txtPaymentDate"),
													$objGeneral->fnGet("txtDescription"), 
													$objGeneral->fnGet("txtNotes")
													
													);
    else if ($varAction == "AddNewReceivedPayment")
        $varError = $objReceivedPayment->AddNewReceivedPayment(	       									
													$objGeneral->fnGet("selInvoice"),
													$objGeneral->fnGet("txtReference"),
													$objGeneral->fnGet("txtReceiptNo"),
													$objGeneral->fnGet("txtAmount"),
													$objGeneral->fnGet("selPaymentMethod"),
													$objGeneral->fnGet("txtDueDate"),
													$objGeneral->fnGet("txtDiscountDate"),
													$objGeneral->fnGet("txtAppliedCredit"),
													$objGeneral->fnGet("txtPaymentDate"),
													$objGeneral->fnGet("txtDescription"), 
													$objGeneral->fnGet("txtNotes")
													);
	else if ($varAction == "DeleteReceivedPayment")
		$varError = $objReceivedPayment->DeleteReceivedPayment($objGeneral->fnGet("receivedpaymentid"));

}

include('../include/top2.php');
print($objReceivedPayment->ReceivedPaymentDetails($objGeneral->fnGet("receivedpaymentid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>