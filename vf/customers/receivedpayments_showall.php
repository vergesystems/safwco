<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers_ReceivedPayments.php');
$objReceivedPayment = new clsReceivedPayments();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteReceivedPayment")
		$varError = $objReceivedPayment->DeleteReceivedPayment($objGeneral->fnGet("receivedpaymentid"));
}

include('../include/top2.php');
print($objReceivedPayment->ShowAllReceivedPayments($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>