<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers.php');
$objRecurringInvoice = new clsCustomers_RecurringInvoices();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateRecurringInvoice")
		$varError = $objRecurringInvoice->UpdateRecurringInvoice(
												$objGeneral->fnGet("id"),
												$objGeneral->fnGet("txtTitle"),
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("txtInvoiceDateOfMonth"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_1"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_2"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_3"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_4"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_5"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_6"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_7"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_8"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_9"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_10"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_11"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_12"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("selStatus"));
			
    else if ($varAction == "AddNewRecurringInvoice")
        $varError = $objRecurringInvoice->AddNewRecurringInvoice(
												$objGeneral->fnGet("txtTitle"),
												$objGeneral->fnGet("hdnCustomerId"),
												$objGeneral->fnGet("txtInvoiceDateOfMonth"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_1"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_2"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_3"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_4"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_5"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_6"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_7"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_8"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_9"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_10"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_11"),
												$objGeneral->fnGet("chkInvoiceMonthOfYear_12"),
												$objGeneral->fnGet("txtDescription"),
												$objGeneral->fnGet("selStatus"));
	else if ($varAction == "DeleteRecurringInvoice")
		$varError = $objRecurringInvoice->DeleteRecurringInvoice($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objRecurringInvoice->RecurringInvoiceDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("salesorderid") , $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>