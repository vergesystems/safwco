<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers.php');
$objRecurringInoice = new clsCustomers_RecurringInvoices(); 

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteRecurringInoice")
		$varError = $objRecurringInoice->DeleteRecurringInoice($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objRecurringInoice->ShowAllRecurringInvoices($objGeneral->fnGet("p"), $objGeneral->fnGet("status")));
include('../include/bottom2.php');
?>