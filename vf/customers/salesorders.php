<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Customers.php');
$objSalesOrder = new clsCustomers_SalesOrders();

$varAction = $objGeneral->fnGet("action");

if ($varAction != "")
{
	if ($varAction == "UpdateSalesOrder")
		$varError = $objSalesOrder->UpdateSalesOrder(											
														$objGeneral->fnGet("salesorderid"), 
														$objGeneral->fnGet("hdnCustomerId"),
														$objGeneral->fnGet("selQuotation"), 
														$objGeneral->fnGet("txtOrderNo"), 
														$objGeneral->fnGet("txtDeliveryDate"), 
														$objGeneral->fnGet("txtSalesOrderDate"),
														$objGeneral->fnGet("OrderTotalAmount"),
														$objGeneral->fnGet("txtDescription"),
														$objGeneral->fnGet("selStatus"),
														$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewSalesOrder")
        $varError = $objSalesOrder->AddNewSalesOrder(
														$objGeneral->fnGet("hdnCustomerId"),
														$objGeneral->fnGet("selQuotation"),
														$objGeneral->fnGet("txtOrderNo"), 
														$objGeneral->fnGet("txtDeliveryDate"), 
														$objGeneral->fnGet("txtSalesOrderDate"),
														$objGeneral->fnGet("OrderTotalAmount"),
														$objGeneral->fnGet("txtDescription"),
														$objGeneral->fnGet("selStatus"),
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteSalesOrder")
		$varError = $objSalesOrder->DeleteSalesOrder($objGeneral->fnGet("salesorderid"));

}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objSalesOrder->ShowAllSalesOrders($objGeneral->fnGet("p"), $objGeneral->fnGet("status")));
else	if ($sPageType == "details")
	print($objSalesOrder->SalesOrderDetails($objGeneral->fnGet("salesorderid"), $objGeneral->fnGet("quotationid"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');
?>