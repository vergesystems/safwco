<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers.php');
$objSalesOrder = new clsCustomers_SalesOrders();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateSalesOrder")
		$varError = $objSalesOrder->UpdateSalesOrder(											
														$objGeneral->fnGet("salesorderid"), 
														$objGeneral->fnGet("hdnCustomerId"),
														$objGeneral->fnGet("selQuotation"), 
														$objGeneral->fnGet("txtOrderNo"), 
														$objGeneral->fnGet("txtDeliveryDate"), 
														$objGeneral->fnGet("txtSalesOrderDate"),														 
														$objGeneral->fnGet("OrderTotalAmount"),														
														$objGeneral->fnGet("txtDescription"),
														$objGeneral->fnGet("selStatus"),
														$objGeneral->fnGet("txtNotes") 
														);
    else if ($varAction == "AddNewSalesOrder")
        $varError = $objSalesOrder->AddNewSalesOrder(
														$objGeneral->fnGet("hdnCustomerId"),
														$objGeneral->fnGet("selQuotation"),
														$objGeneral->fnGet("txtOrderNo"), 
														$objGeneral->fnGet("txtDeliveryDate"), 
														$objGeneral->fnGet("txtSalesOrderDate"),														 
														$objGeneral->fnGet("OrderTotalAmount"),														
														$objGeneral->fnGet("txtDescription"),
														$objGeneral->fnGet("selStatus"),
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteSalesOrder")
		$varError = $objSalesOrder->DeleteSalesOrder($objGeneral->fnGet("salesorderid"));

}

include('../include/top2.php');
print($objSalesOrder->SalesOrderDetails($objGeneral->fnGet("salesorderid"), $objGeneral->fnGet("quotationid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>