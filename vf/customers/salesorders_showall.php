<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers.php');
$objSalesOrder = new clsCustomers_SalesOrders();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteSalesOrder")
		$varError = $objSalesOrder->DeleteSalesOrder($objGeneral->fnGet("salesorderid"));
}

include('../include/top2.php');
print($objSalesOrder->ShowAllSalesOrders($objGeneral->fnGet("p"), $objGeneral->fnGet("status")));
include('../include/bottom2.php');
?>