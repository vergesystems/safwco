<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers_Statements.php');
$objStatement = new clsStatements();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateStatement")
		$varError = $objStatement->UpdateStatement(											
													$objGeneral->fnGet("id"), 
													$objGeneral->fnGet("selCustomer"), 
													$objGeneral->fnGet("txtTitle"), 
													$objGeneral->fnGet("txtPeriodFrom"), 
													$objGeneral->fnGet("txtPeriodTo"), 
													$objGeneral->fnGet("txtDescription"), 
													$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewStatement")
        $varError = $objStatement->AddNewStatement(        									
													$objGeneral->fnGet("selCustomer"), 
													$objGeneral->fnGet("txtTitle"), 
													$objGeneral->fnGet("txtPeriodFrom"), 
													$objGeneral->fnGet("txtPeriodTo"), 
													$objGeneral->fnGet("txtDescription"), 
													$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteStatement")
		$varError = $objStatement->DeleteStatement($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objStatement->StatementDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>