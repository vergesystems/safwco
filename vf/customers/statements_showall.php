<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Customers_Statements.php');
$objStatement = new clsStatements();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteStatement")
		$varError = $objStatement->DeleteStatement($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objStatement->ShowAllStatements($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>