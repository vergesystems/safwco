<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Help.php');
$objHelp = new clsFMS_Help();

include('../include/top2.php');
print($objHelp->Help_Banking());
include('../include/bottom2.php');
?>