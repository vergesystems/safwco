<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
 <a name="#Top"></a>
 <span class="Help_Title">Accounts Module</span><br /><br />
<div class="Help_Contents" align="justify">
This module deals with managing the accounts settings, that will help in operating the entire FM System. Most of the actions in this module are to be performed once, usually when setting up the system. A certain minor changes may be required here and there, especially when there is a change in organizational structure. Components in this module are mostly for administrative purposes only, users can have the role to view information on a certain components.
<br /><br />
The Organization module comprises of the following components (Click for details):
<br /><br /><br />
<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
 <tr>
  <td width="10%" align="center"><a href="#GeneralJournal"><img src="../images/accounts/iconGeneralJournal.gif" title="General Journal" alt="General Journal" border="0" /><br />General Journal</a></td>
  <td width="10%" align="center"><a href="#GeneralLedger"><img src="../images/accounts/iconGeneralLedger.gif" title="General Ledger" alt="General Ledger" border="0" /><br />General Ledger</a></td>
  <td width="10%" align="center"><a href="#ChartofAccounts"><img src="../images/accounts/iconChartofAccounts.gif" title="Chart of Accounts" alt="Chart of Accounts" border="0" /><br />Chart of Accounts</a></td>
  <td width="10%" align="center"><a href="#QuickEntries"><img src="../images/accounts/iconQuickEntries.gif" title="Quick Entries" alt="Quick Entries" border="0" /><br />Quick Entries</a></td>
  <td width="10%" align="center"><a href="#Donors"><img src="../images/accounts/iconDonors.gif" title="Donors" alt="Donors" border="0" /><br />Donors</a></td>
  <td width="10%" align="center"><a href="#CloseFinancialYear"><img src="../images/accounts/iconCloseFinancialYear.gif" title="Close Financial Year" alt="Close Financial Year" border="0" /><br />Close Financial Year</a></td>
  <td width="10%" align="center"><a href="#Budget"><img src="../images/accounts/iconBudget.gif" title="Budget" alt="Budget" border="0" /><br />Budget</a></td>
  <td width="10%" align="center"><a href="#BudgetAllocation"><img src="../images/accounts/iconBudgetAllocation.gif" title="Budget Allocation" alt="Budget Allocation" border="0" /><br />Budget Allocation</a></td>
 </tr>
</table>
<br /><br />
<hr size="1" />

<!-- General Journal-->
<a name="#GeneralJournal"></a><span class="Help_Title">General Journal</span><br /><br />
This component manages the regional hierarchy of the regions / areas where the organization works. A typical example of regional hiearchy would be an organization working in different regions, e.g. Lahore, Karachi, Sukkur and Queeta, would add the following entries:<br />
Regional hierarchy helps mostly in reporting, where the report is to be generated, lets say on Sindh Province, would display results from all branches in Sindh. The reason for the report displaying all branches in Sindh when province Sindh is selected is because Regional Hiearchy works like a tree, derived from top to bottom. In our example, the top branch of the tree would be Country Pakistan, then, the next level of hierarchy would be Provinces and finally the Cities at the bottom. Regional hieararchy is completely controllable by the user to allow any number of levels in the hierarchy. This will give flexibility to add further levels of regions, for example, if the user wants to add Districts, Tehsils, Talukas and even Villages.
<br /><br />
Following is the screenshot of a user view of Gnral Journal component:
<br /><br />
<div align="center"><img src="../images/help/Accounts/GenralJournal1.gif" /></div>
<br /><br />
Regional hierarchy component is divided in two main sections, Regions and Region Types. Region Types must be defined first, in our example following were the region types that were added in the system: Country, Province, City. After adding Region Types, the user will add different regions by selecting its region type, in our example Karachi is listed as a City, and so on.
<br /><br />
<span class="Help_Title2">How to add a new General Journal:</span><br />
To add a new General Journal, click on General Journal tab, then click on Add New button <img src="../images/help/Accounts/GeneralJournal2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add New <img src="../images/help/Accounts/GeneralJournal2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new General Journal cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on General Journal:</span><br />
Following operations can be performed on a selected General Journal:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this General Journal Details" title="View this General Journal Details" /></td>
  <td><strong>View this General Journal Details:</strong> This will open a window that will display all of the details of the selected General Journal.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this General Journal Details" title="Edit this General Journal Details" /></td>
  <td><strong>Edit this General Journal Details:</strong> This will open a window that will allow modifying/updating the selected General Journal.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this General Journal" title="Delete this General Journal" /></td>
  <td><strong>Delete this General Journal:</strong> This action will delete the selected General Journal. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconPrint2.gif" border="0" alt="Receipt" title="Receipt" /></td>
  <td><strong>Receipt:</strong> This action will show the Receipt of selected General Journal.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconSave.gif" border="0" alt="General Journal Documents" title="General Journal Documents" /></td>
  <td><strong>General Journal Documents:</strong> This action is used to manage the selected General Journal Documents.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/tick.gif" border="0" alt="Update Status" title="Update Status" /></td>
  <td><strong>Update Status:</strong> This action is used to Update Status of the selected General Journal.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Genral Ledger -->
<a name="#GeneralLedger"></a><span class="Help_Title">General Ledger</span><br /><br />
This component shows all the General Journal Entries. 
<br /><br />
Following is the screenshot of a user view of General Ledger component:
<br /><br />
<div align="center"><img src="../images/help/Accounts/GenralLedger1.gif" /></div>
<br /><br />
Just like regional hierarchy, stations component is divided in two main sections, Stations and Station Types. Station Types must be added before adding any station.
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Station Type Details" title="View this Station Type Details" /></td>
  <td><strong>View this General Ledger Details:</strong> This will open a window that will display all of the details of the selected General Ledger Entry.</td>
 </tr> 
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Chart of Accounts -->
<a name="#ChartofAccounts"></a><span class="Help_Title">Chart of Accounts</span><br /><br />
Every organization is divided in several different departments, each department usually have a department head, who manages every employee working under his supervision. This component helps in defining the departments and their department heads in the organization.
<br /><br />
Following is the screenshot of a user view of Chart of Accounts component:
<br /><br />
<div align="center"><img src="../images/help/Organization/ChartofAccounts1.gif" /></div>
<br /><br />
Just like regional hierarchy, stations component is divided in two main sections, Stations and Station Types. Station Types must be added before adding any station.
<br /><br />
<span class="Help_Title2">How to add a new Chart of Account:</span><br />
To add a new Chart of Account, click on Chart of Account, tab, then click on Add Account button <img src="../images/help/Accounts/ChartofAccount2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add <img src="../images/help/Accounts/ChartofAccount2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Chart of Account cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Chart of Account:</span><br />
Following operations can be performed on a selected Chart of Account:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Chart of Accounts Details" title="View this Chart of Accounts Details" /></td>
  <td><strong>View this Chart Of Account Details:</strong> This will open a window that will display all of the details of the selected Chart of Accounts.</td>
 </tr>
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Chart of Accounts Details" title="Edit this Chart of Accounts Details" /></td>
  <td><strong>Edit this Chart of Account Details:</strong> This will open that will allow modifying/updating the selected Chart of Accounts.</td>
 </tr>
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconExport.gif" border="0" alt="View Transaction Details" title="View Transaction Details" /></td>
  <td><strong>View Transaction Details:</strong> This will open a window that will display all of the details of the selected Chart of Account's Transaction Details.</td>
 </tr> 
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Chart Of Account" title="Delete this Chart Of Account" /></td>
  <td><strong>Delete this Chart Of Account:</strong> This action will delete the selected Chart Of Account. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />		

<!-- Quick Entries -->
<a name="#QuickEntries"></a><span class="Help_Title">Quick Entries</span><br /><br />
This comoponent is used to take complete backup of the complete database. This is a purely technical function and must be used only by the authorized personnel. In information technology, backup refers to making copies of data so that these additional copies may be used to restore the original after a data loss event. These additional copies are typically called "backups." Backups are useful primarily for two purposes. The first is to restore a state following a disaster (called disaster recovery). The second is to restore small numbers of files after they have been accidentally deleted or corrupted.
<br /><br />
Please note that only one data backup is allowed per day. Backup files save as the current date. In a particular day, if three data backups are taken, the first two will be overwritten by the third attempt.
<br /><br />
It is recommended to take data backup atleast once a week, most organizations prefer taking their data backup on Sundays, so it won't interfere with the regular operations of the system. Data backup files generated through the system can be copied on any external media.
<br /><br />
Following is the screenshot of a user view of Quick Entries component:
<br /><br />
<div align="center"><img src="../images/help/Organization/QuickEntries.gif" /></div>
<br /><br />
<span class="Help_Title2">How to add a new Quick Entry:</span><br />
To add a new Quick Entry, click on Quick Entry, tab, then click on Add New button <img src="../images/help/Accounts/QuickEntry2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add <img src="../images/help/Accounts/QuickEntry2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Quick Entry cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Quick Entry:</span><br />
Following operations can be performed on a selected Quick Entry:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quick Entry Details" title="View this Quick Entry Details" /></td>
  <td><strong>View this Quick Entry Details:</strong> This will open a window that will display all of the details of the selected Quick Entry.</td>
 </tr>
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quick Entry Details" title="Edit this Quick Entry Details" /></td>
  <td><strong>Edit this Quick Entry Details:</strong> This will open that will allow modifying/updating the selected Quick Entry.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quick Entry" title="Delete this Quick Entry" /></td>
  <td><strong>Delete this Quick Entry:</strong> This action will delete the selected Quick Entry. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Donors -->
<a name="#Donors"></a><span class="Help_Title">Donors</span><br /><br />
This component manages the Donors, Projects and Project Activities:<br />

<br /><br />
Following is the screenshot of a user view of Donors component:
<br /><br />
<div align="center"><img src="../images/help/Organization/Donors1.gif" /></div>
<br /><br />
Donors component is divided in three main sections, Donors, donor Projects and Project Activities. Donors must be defined first.
<br /><br />
<span class="Help_Title2">How to add a new Donor:</span><br />
To add a new Donor, click on Donor tab, then click on Add Donor button <img src="../images/help/Organization/Donors2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Donor <img src="../images/help/Accounts/Donors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Donor cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Donors:</span><br />
Following operations can be performed on a selected Donor:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Donor Details" title="View this Donor Details" /></td>
  <td><strong>View this Donor Details:</strong> This will open a window that will display all of the details of the selected Donor.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Donor Details" title="Edit this Donor Details" /></td>
  <td><strong>Edit this Donor Details:</strong> This will open a window that will allow modifying/updating the selected Donor.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Donor" title="Delete this Donor" /></td>
  <td><strong>Delete this Donor:</strong> This action will delete the selected Donor. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<span class="Help_Title2">How to add a new Donor Project:</span><br />
To add a new Donor Project, click on Donor Projects tab, then click on Add Donor button <img src="../images/help/Accounts/DonorProjects2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Project <img src="../images/help/Accounts/DonorProjects2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Donor Project cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Donor Projects:</span><br />
Following operations can be performed on a selected Donor Project:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Donor Project Details" title="View this Donor Project Details" /></td>
  <td><strong>View this Donor Project Details:</strong> This will open a window that will display all of the details of the selected Donor Project.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Donor Project Details" title="Edit this Donor Project Details" /></td>
  <td><strong>Edit this Donor Project Details:</strong> This will open a window that will allow modifying/updating the selected Donor Project.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Donor Project" title="Delete this Donor Project" /></td>
  <td><strong>Delete this Donor Project:</strong> This action will delete the selected Donor Project. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<span class="Help_Title2">How to add a new Project Activity:</span><br />
To add a new Project Activity, click on Project Activities tab, then click on Add Activity button <img src="../images/help/Accounts/ProjectActivity2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Activity <img src="../images/help/Accounts/ProjectActivity2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Project Activity cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Project Activity:</span><br />
Following operations can be performed on a selected Project Activity:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Project Activity Details" title="View this Project Activity Details" /></td>
  <td><strong>View this Project Activity Details:</strong> This will open a window that will display all of the details of the selected Project Activity.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Project Activity Details" title="Edit this Project Activity Details" /></td>
  <td><strong>Edit this Project Activity Details:</strong> This will open a window that will allow modifying/updating the selected Project Activity.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Project Activity" title="Delete this Project Activity" /></td>
  <td><strong>Delete this Project Activity:</strong> This action will delete the selected Project Activity. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Close Financial Year -->
<a name="#CloseFinancialYear"></a><span class="Help_Title">Close Financial Year</span><br /><br />
This component manages the regional hierarchy of the regions / areas where the organization works. A typical example of regional hiearchy would be an organization working in different regions, e.g. Lahore, Karachi, Sukkur and Queeta, would add the following entries:<br />
<br /><br />
Following is the screenshot of a user view of Close Financial Year module:
<br /><br />
<div align="center"><img src="../images/help/Accounts/CloseFinancialYear1.gif" /></div>
<br /><br />
Regional hierarchy component is divided in two main sections, Regions and Region Types. Region Types must be defined first, in our example following were the region types that were added in the system: Country, Province, City. After adding Region Types, the user will add different regions by selecting its region type, in our example Karachi is listed as a City, and so on.
<br /><br />
<span class="Help_Title2">How to Close a Financial Year:</span><br />
To add a new region type, click on Region Types tab, then click on Add Region Type button <img src="../images/help/Organization/RegionalHierarchy2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Region Type <img src="../images/help/Organization/RegionalHierarchy2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new region type cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Close Financial Year:</span><br />
Following operations can be performed on a selected Close Financial Year:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Region Type Details" title="View this Region Type Details" /></td>
  <td><strong>View this Region Type Details:</strong> This will open a window that will display all of the details of the selected region type.</td>
 </tr>
 </table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Budget -->
<a name="#Budget"></a><span class="Help_Title">Budget</span><br /><br />
This component manages the Budget of the organization.<br />
<br /><br />
Following is the screenshot of a user view of Budget component:
<br /><br />
<div align="center"><img src="../images/help/Accounts/Budget1.gif" /></div>
<br /><br />
<span class="Help_Title2">How to add a new Budget:</span><br />
To add a new Budget, click on Budget tab, then click on Add Budget button <img src="../images/help/Accounts/Budget2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Budget <img src="../images/help/Accounts/Budget2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new region type cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Budget:</span><br />
Following operations can be performed on a selected Budget:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Budget Details" title="View this Budget Details" /></td>
  <td><strong>View this Budget Details:</strong> This will open a window that will display all of the details of the selected Budget.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Details" title="Edit this Budget Details" /></td>
  <td><strong>Edit this Budget Details:</strong> This will open a window that will allow modifying/updating the selected Budget.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/tick.gif" border="0" alt="Change Budget Amount" title="Change Budget Amount" /></td>
  <td><strong>Change Budget Amount:</strong> This will open a window that will allow modifying/updating the selected Budget Amount.</td>
 </tr> 
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Budget" title="Delete this Budget" /></td>
  <td><strong>Delete this Budget:</strong> This action will delete the selected Budget. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Budget Allocation-->
<a name="#BudgetAllocation"></a><span class="Help_Title">Budget Allocation</span><br /><br />
This component is used to Allocate the Budget.
<br /><br />
Following is the screenshot of a user view of Budget Allocation component:
<br /><br />
<div align="center"><img src="../images/help/Accounts/BudgetAllocation1.gif" /></div>
<br /><br />
<span class="Help_Title2">How to Allocate Budget:</span><br />
To Allocate Budget, click on Budget Allocation tab, then click on Allocate Budget button <img src="../images/help/Accounts/BudgetAllocation2.gif" />. This will open a new tab, fill in the required information in the new window, then click Allocate Type <img src="../images/help/Accounts/BudgetAllocation2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Budget cannot be allocated until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Budget Allocation:</span><br />
Following operations can be performed on a selected Allocated Budget:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Budget Allocation Details" title="View this Budget Allocation Details" /></td>
  <td><strong>View this Budget Allocation Details:</strong> This will open a window that will display all of the details of the selected Budget Allocation.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Budget Allocation Details" title="Edit this Budget Allocation Details" /></td>
  <td><strong>Edit this Budget Allocation Details:</strong> This will open a window that will allow modifying/updating the selected Budget Allocation.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/tick.gif" border="0" alt="Change Budget Allocation Amount" title="Change Budget Allocation Amount" /></td>
  <td><strong>Change Budget Allocation Amount:</strong> This action will change the allocated budget amount.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />


</div>		
</td></tr></table>