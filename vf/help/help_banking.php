<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
 <a name="#Top"></a>
 <span class="Help_Title">Banking Module</span><br /><br />
<div class="Help_Contents" align="justify">
This module deals with managing the accounts settings, that will help in operating the entire FM System. Most of the actions in this module are to be performed once, usually when setting up the system. A certain minor changes may be required here and there, especially when there is a change in organizational structure. Components in this module are mostly for administrative purposes only, users can have the role to view information on a certain components.
<br /><br />
The Organization module comprises of the following components (Click for details):
<br /><br /><br />
<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
 <tr>
  <td width="10%" align="center"><a href="#BankDeposits"><img src="../images/banking/iconBankDeposits.gif" title="Bank Deposits" alt="Bank Deposits" border="0" /><br />Bank Deposits</a></td>
  <td width="10%" align="center"><a href="#BankWithdrawals"><img src="../images/banking/iconBankWithdrawals.gif" title="Bank Withdrawals" alt="Bank Withdrawals" border="0" /><br />Bank Withdrawals</a></td>
  <td width="10%" align="center"><a href="#BankReconciliation"><img src="../images/banking/iconBankReconciliation.gif" title="Bank Reconciliation" alt="Bank Reconciliation" border="0" /><br />Bank Reconciliation</a></td>
  <td width="10%" align="center"><a href="#BankCheckBooks"><img src="../images/banking/iconBankCheckBooks.gif" title="Bank Check Books" alt="Bank Check Books" border="0" /><br />Bank Check Books</a></td>
  <td width="10%" align="center"><a href="#BankAccounts"><img src="../images/banking/iconBankAccounts.gif" title="Bank Accounts" alt="Bank Accounts" border="0" /><br />Bank Accounts</a></td>
  <td width="10%" align="center"><a href="#Banks"><img src="../images/banking/iconBanks.gif" title="Banks" alt="Banks" border="0" /><br />Banks</a></td>  
 </tr>
</table>
<br /><br />
<hr size="1" />

<!-- BankDeposits -->
<a name="#BankDeposits"></a><span class="Help_Title">Bank Deposits</span><br /><br />
This component manages the Bank Deposits.
<br /><br />
Following is the screenshot of a user view of Bank Deposits component:
<br /><br />
<div align="center"><img src="../images/help/Banking/BankDeposits1.gif" /></div>
<br /><br />
<span class="Help_Title2">Operations on Bank Deposits:</span><br />
Following operations can be performed on a selected Bank Deposits:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Deposit Details" title="View this Bank Deposit Details" /></td>
  <td><strong>View this Bank Deposit Details:</strong> This will open a window that will display all of the details of the selected Bank Deposit.</td>
 </tr> 
</table>
<br /><br />
<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />	

<!-- Bank Withdrawals -->
<a name="#BankWithdrawals"></a><span class="Help_Title">Bank Withdrawals</span><br /><br />
This component manages the Bank Withdrawals.
<br /><br />
Following is the screenshot of a user view of Bank Withdrawals component:
<br /><br />
<div align="center"><img src="../images/help/Banking/BankWithdrawals1.gif" /></div>
<br /><br />
<span class="Help_Title2">Operations on Bank Withdrawals:</span><br />
Following operations can be performed on a selected Bank Withdrawals:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Withdrawal Details" title="View this Bank Withdrawal Details" /></td>
  <td><strong>View this Bank Withdrawal Details:</strong> This will open a window that will display all of the details of the selected Bank Withdrawal.</td>
 </tr> 
</table>
<br /><br />
<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />	

<!-- Bank Reconciliation -->
<a name="#BankReconciliation"></a><span class="Help_Title">Bank Reconciliation</span><br /><br />
This component manages the Bank Reconciliation.
<br /><br />
Following is the screenshot of a user view of Bank Reconciliation component:
<br /><br />
<div align="center"><img src="../images/help/Banking/BankReconciliation1.gif" /></div>
<br /><br />
<span class="Help_Title2">Operations on Bank Reconciliation:</span><br />
Following operations can be performed on a selected Bank Reconciliation:
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />	

<!-- Bank Check Books -->
<a name="#BankCheckBooks"></a><span class="Help_Title">Bank Check Books</span><br /><br />
This component manages the Bank Check Books.
<br /><br />
Following is the screenshot of a user view of Bank Check Books component:
<br /><br />
<div align="center"><img src="../images/help/Vendors/Vendors1.gif" /></div>
<br /><br />
Bank Check Books component is divided in two main sections, Bank Check Books and Issued Checks.
<br /><br />
Issued Checks shows just the Checks used/issued for a trnasaction.
<br /><br />
<span class="Help_Title2">How to add a new Bank Check Book:</span><br />
To add a new Bank Check Book, click on Bank Check Books tab, then click on Add Bank Check Book button <img src="../images/help/Vendors/Vendors2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Bank Check Book <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Bank Check Book cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Bank Check Books:</span><br />
Following operations can be performed on a selected Bank Check Book:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Check Book Details" title="View this Bank Check Book Details" /></td>
  <td><strong>View this Bank Check Book Details:</strong> This will open a window that will display all of the details of the selected Bank Check Book.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Bank Check Book Details" title="Edit this Bank Check Book Details" /></td>
  <td><strong>Edit this Bank Check Book Details:</strong> This will open a window that will allow modifying/updating the selected Bank Check Book.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/banking/iconCancelledChecks.gif" border="0" alt="Cancelled Checks" title="Cancelled Checks" /></td>
  <td><strong>Cancelled Checks:</strong> This will open a window that will allow to Cancel the Checks from selected Bank Check Book.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank Check Book" title="Delete this Bank Check Book" /></td>
  <td><strong>Delete this Bank Check Book:</strong> This action will delete the selected Bank Check Book. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />		

<!-- Bank Accounts -->
<a name="#BankAccounts"></a><span class="Help_Title">Bank Accounts</span><br /><br />
This component manages the Bank Accounts.
<br /><br />
Following is the screenshot of a user view of Bank Accounts component:
<br /><br />
<div align="center"><img src="../images/help/Banking/BankAccounts1.gif" /></div>
<br /><br />
<span class="Help_Title2">How to add a new Bank Account:</span><br />
To add a new Bank Account, click on Bank Accounts tab, then click on Add Bank Account button <img src="../images/help/Banking/BankAccounts2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Bank Account <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Bank Account cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Bank Accounts:</span><br />
Following operations can be performed on a selected Bank Account:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Account Details" title="View this Bank Account Details" /></td>
  <td><strong>View this Bank Account Details:</strong> This will open a window that will display all of the details of the selected Bank Account.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Bank Account Details" title="Edit this Bank Account Details" /></td>
  <td><strong>Edit this Bank Account Details:</strong> This will open a window that will allow modifying/updating the selected Bank Account.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank Account" title="Delete this Bank Account" /></td>
  <td><strong>Delete this Bank Account:</strong> This action will delete the selected Bank Account. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Banks -->
<a name="#Banks"></a><span class="Help_Title">Banks</span><br /><br />
This component manages the Banks.
<br /><br />
Following is the screenshot of a user view of Banks component:
<br /><br />
<div align="center"><img src="../images/help/Banking/BankAccounts1.gif" /></div>
<br /><br />
<span class="Help_Title2">How to add a new Bank:</span><br />
To add a new Bank, click on Banks tab, then click on Add New Bank button <img src="../images/help/Banking/Banks2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Bank <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Bank cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Banks:</span><br />
Following operations can be performed on a selected Bank:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Bank Details" title="View this Bank Details" /></td>
  <td><strong>View this Bank Details:</strong> This will open a window that will display all of the details of the selected Bank.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Bank Details" title="Edit this Bank Details" /></td>
  <td><strong>Edit this Bank Details:</strong> This will open a window that will allow modifying/updating the selected Bank.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Bank" title="Delete this Bank" /></td>
  <td><strong>Delete this Bank:</strong> This action will delete the selected Bank. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />
</div>		
</td></tr></table>