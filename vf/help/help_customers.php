<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
 <a name="#Top"></a>
 <span class="Help_Title">Customers Module</span><br /><br />
<div class="Help_Contents" align="justify">
This module deals with managing the cstomers.
<br /><br />
The Customers module comprises of the following components (Click for details):
<br /><br /><br />
<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
 <tr>
  <td width="10%" align="center"><a href="#Customers"><img src="../images/customers/iconCustomers.gif" title="Customers" alt="Customers" border="0" /><br />Customers</a></td>
  <td width="10%" align="center"><a href="#Quotations"><img src="../images/customers/iconQuotations.gif" title="Quotations" alt="Quotations" border="0" /><br />Quotations</a></td>
  <td width="10%" align="center"><a href="#SalesOrders"><img src="../images/customers/iconSalesOrders.gif" title="Sales Orders" alt="Sales Orders" border="0" /><br />Sales Orders</a></td>
  <td width="10%" align="center"><a href="#Invoices"><img src="../images/customers/iconInvoices.gif" title="Invoices" alt="Invoices" border="0" /><br />Invoices</a></td>
  <td width="10%" align="center"><a href="#Receipts"><img src="../images/customers/iconReceipts.gif" title="Receipts" alt="Receipts" border="0" /><br />Receipts</a></td>
  <td width="10%" align="center"><a href="#Jobs"><img src="../images/customers/iconJobs.gif" title="Jobs" alt="Jobs" border="0" /><br />Jobs</a></td>  
 </tr>
</table>
<br /><br />
<hr size="1" />

<!-- Customers -->
<a name="#Customers"></a><span class="Help_Title">Customers</span><br /><br />
This component manages the Customers.
<br /><br />
Following is the screenshot of a user view of Customers component:
<br /><br />
<div align="center"><img src="../images/help/Customers/Customers1.gif" /></div>
<br /><br />
<span class="Help_Title2">How to add a new Customers:</span><br />
To add a new Customer, click on Customers tab, then click on Add Customer button <img src="../images/help/Customers/Customers2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Customer<img src="../images/help/Customers/Customers2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Customer cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Customers:</span><br />
Following operations can be performed on a selected Customer:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Customer Details" title="View this Customer Details" /></td>
  <td><strong>View this Customer Details:</strong> This will open a window that will display all of the details of the selected Customer.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Customer Details" title="Edit this Customer Details" /></td>
  <td><strong>Edit this Customer Details:</strong> This will open a window that will allow modifying/updating the selected Customer.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconContents.gif" border="0" alt="Customers Ledger" title="Customers Ledger" /></td>
  <td><strong>Customers Ledger:</strong> This will open a window that will allow to view the selected Customer's Ledger.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Customer" title="Delete this Customer" /></td>
  <td><strong>Delete this Customer:</strong> This action will delete the selected Customer. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />		

<!-- Quotations -->
<a name="#Quotations"></a><span class="Help_Title">Quotations</span><br /><br />
This component is used for Quotations.
<br /><br />
Following is the screenshot of a user view of Quotation component:
<br /><br />
<div align="center"><img src="../images/help/Vendors/Quotations.gif" /></div>
<br /><br />

<span class="Help_Title2">How to add a new Quotation:</span><br />
To add a new Quotation, click on Quotations tab, then click on Add Quotation button <img src="../images/help/Quotations/Quotations2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Quotation <img src="../images/help/Quotations/Quotations2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Quotation cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Quotations:</span><br />
Following operations can be performed on a selected Quotation:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quotation Details" title="View this Quotation Details" /></td>
  <td><strong>View this Quotation Details:</strong> This will open a window that will display all of the details of the selected Quotation.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quotation Details" title="Edit this Quotation Details" /></td>
  <td><strong>Edit this Quotation Details:</strong> This will open a window that will allow modifying/updating the selected station type.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quotation" title="Delete this Quotation" /></td>
  <td><strong>Delete this Quotation:</strong> This action will delete the selected Quotation. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Sales Orders -->
<a name="#SalesOrders"></a><span class="Help_Title">Sales Orders</span><br /><br />
This component is used for Sales Orders.
<br /><br />
Following is the screenshot of a user view of Sales Orders component:
<br /><br />
<div align="center"><img src="../images/help/Vendors/SalesOrders.gif" /></div>
<br /><br />

<span class="Help_Title2">How to add a new Sales Order:</span><br />
To add a new Sales Order, click on Sales Orders tab, then click on Add New button <img src="../images/help/Vendors/SalesOrders2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add New <img src="../images/help/Sales Orders/Sales Orders2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Sales Order cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Sales Orders:</span><br />
Following operations can be performed on a selected Sales Order:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Sales Order Details" title="View this Sales Order Details" /></td>
  <td><strong>View this Sales Order Details:</strong> This will open a window that will display all of the details of the selected Sales Order.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Sales Order Details" title="Edit this Sales Order Details" /></td>
  <td><strong>Edit this Sales Order Details:</strong> This will open a window that will allow modifying/updating the selected Sales Order.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Sales Order" title="Delete this Sales Order" /></td>
  <td><strong>Delete this Sales Order:</strong> This action will delete the selected Sales Order. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Invoices -->
<a name="#Invoices"></a><span class="Help_Title">Invoices</span><br /><br />
This component is used for Invoices.
<br /><br />
Following is the screenshot of a user view of Invoices component:
<br /><br />

Invoices component is divided in two main sections, Invoices and Recurring Invoices.
<br /><br />
<div align="center"><img src="../images/help/Customers/Invoices.gif" /></div>
<br /><br />
<span class="Help_Title2">How to add a new Recurring Invoices:</span><br />
To add a new Recurring Invoice, click on Recurring Invoices tab, then click on Add button <img src="../images/help/Custoemrs/RecurringInvoices2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add <img src="../images/help/Customers/RecurringInvoices2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Recurring Invoice cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Recurring Invoices:</span><br />
Following operations can be performed on a selected Recurring Invoices:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Recurring Invoices Details" title="View this Recurring Invoices Details" /></td>
  <td><strong>View this Recurring Invoices Details:</strong> This will open a window that will display all of the details of the selected Recurring Invoices.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Recurring Invoices Details" title="Edit this Recurring Invoices Details" /></td>
  <td><strong>Edit this Recurring Invoices Details:</strong> This will open a window that will allow modifying/updating the selected Recurring Invoices.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Recurring Invoices" title="Delete this Recurring Invoices" /></td>
  <td><strong>Delete this Recurring Invoices:</strong> This action will delete the selected Recurring Invoices. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<span class="Help_Title2">How to add a new Invoice:</span><br />
To add a new Invoice, click on Invoices tab, then click on Add Invoice button <img src="../images/help/Custoemrs/Invoices2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Invoice <img src="../images/help/Vendors/Invoices2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Invoice cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Invoices:</span><br />
Following operations can be performed on a selected Invoice:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Invoice Details" title="View this Invoice Details" /></td>
  <td><strong>View this Invoice Details:</strong> This will open a window that will display all of the details of the selected Invoice.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Invoice Details" title="Edit this Invoice Details" /></td>
  <td><strong>Edit this Invoice Details:</strong> This will open a window that will allow modifying/updating the selected Invoice.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconRight.gif" border="0" alt="Make Receipt" title="Make Receipt" /></td>
  <td><strong>Make Receipt:</strong> This will open a window that will allow to Make Receipt against the selected Invoice.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconPrint2.gif" border="0" alt="Print Invoice" title="Print Invoice" /></td>
  <td><strong>Print Invoice:</strong> This will open a window that will allow to print Invoice.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconFolder.gif" border="0" alt="Invoice Documents" title="Invoice Documents" /></td>
  <td><strong>Invoice Documents:</strong> This will open a window that will allow modifying/uploading/deleting the selected Invoice Documents.</td>
 </tr> 
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Invoice" title="Delete this Invoice" /></td>
  <td><strong>Delete this Invoice:</strong> This action will delete the selected Invoice. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Receipts -->
<a name="#Receipts"></a><span class="Help_Title">Receipts</span><br /><br />
This component is used for Receipts.
<br /><br />
Following is the screenshot of a user view of Receipts component:
<br /><br />

<span class="Help_Title2">How to add a new Receipt:</span><br />
To add a new Receipt, click on Receipts tab, then click on Add Receipt button <img src="../images/help/Custoemrs/Receipts2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Receipt <img src="../images/help/Vendors/Receipts2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Receipt cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Receipts:</span><br />
Following operations can be performed on a selected Receipt:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Receipt Details" title="View this Receipt Details" /></td>
  <td><strong>View this Receipt Details:</strong> This will open a window that will display all of the details of the selected Receipt.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Receipt Details" title="Edit this Receipt Details" /></td>
  <td><strong>Edit this Receipt Details:</strong> This will open a window that will allow modifying/updating the selected Receipt.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconPrint2.gif" border="0" alt="Print Receipt" title="Print Receipt" /></td>
  <td><strong>Print Receipt:</strong> This will open a window that will allow to print Receipt.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconFolder.gif" border="0" alt="Receipt Documents" title="Receipt Documents" /></td>
  <td><strong>Receipt Documents:</strong> This will open a window that will allow modifying/uploading/deleting the selected Receipt Documents.</td>
 </tr> 
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Receipt" title="Delete this Receipt" /></td>
  <td><strong>Delete this Receipt:</strong> This action will delete the selected Receipt. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Jobs -->
<a name="#Jobs"></a><span class="Help_Title">Jobs</span><br /><br />
This component is used for Jobs.
<br /><br />
Following is the screenshot of a user view of Jobs component:
<br /><br />

<span class="Help_Title2">How to add a new Job:</span><br />
To add a new Job, click on Jobs tab, then click on Add Job button <img src="../images/help/Custoemrs/Jobs2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Job <img src="../images/help/Vendors/Jobs2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Job cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Jobs:</span><br />
Following operations can be performed on a selected Job:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Job Details" title="View this Job Details" /></td>
  <td><strong>View this Job Details:</strong> This will open a window that will display all of the details of the selected Job.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Job Details" title="Edit this Job Details" /></td>
  <td><strong>Edit this Job Details:</strong> This will open a window that will allow modifying/updating the selected Job.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Job" title="Delete this Job" /></td>
  <td><strong>Delete this Job:</strong> This action will delete the selected Job. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />
</div>		
</td></tr></table>