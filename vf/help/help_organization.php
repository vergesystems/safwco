<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
 <a name="#Top"></a>
 <span class="Help_Title">The Organization Module</span><br /><br />
<div class="Help_Contents" align="justify">
This module deals with managing the organization / company settings, that will help in operating the entire FM System. Most of the actions in this module are to be performed once, usually when setting up the system. A certain minor changes may be required here and there, especially when there is a change in organizational structure. Components in this module are mostly for administrative purposes only, users can have the role to view information on a certain components. 
<br /><br />
The Organization module comprises of the following components (Click for details):
<br /><br /><br />
<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
 <tr>
  <td width="10%" align="center"><a href="#RegionalHierarchy"><img src="../images/organization/iconRegionalHierarchy.gif" title="Regional Hierarchy" alt="Regional Hierarchy" border="0" /><br />Regional Hierarchy</a></td>
  <td width="10%" align="center"><a href="#Stations"><img src="../images/organization/iconStations.gif" title="Stations" alt="Stations" border="0" /><br />Stations</a></td>
  <td width="10%" align="center"><a href="#Departments"><img src="../images/organization/iconDepartments.gif" title="Departments" alt="Departments" border="0" /><br />Departments</a></td>
  <td width="10%" align="center"><a href="#DataBackup"><img src="../images/organization/iconDataBackup.gif" title="Data Backup" alt="Data Backup" border="0" /><br />Data Backup</a></td>
  <td width="10%" align="center"><a href="#SystemSettings"><img src="../images/organization/iconSettings.gif" title="System Settings" alt="System Settings" border="0" /><br />System Settings</a></td>
  <td width="10%" align="center"><a href="#SystemLog"><img src="../images/organization/iconSystemLog.gif" title="System Log" alt="System Log" border="0" /><br />System Log</a></td>
 </tr>
</table>
<br /><br />
<hr size="1" />

<!-- Regional Hierarchy -->
<a name="#RegionalHierarchy"></a><span class="Help_Title">Regional Hierarchy</span><br /><br />
This component manages the regional hierarchy of the regions / areas where the organization works. A typical example of regional hiearchy would be an organization working in different regions, e.g. Lahore, Karachi, Sukkur and Queeta, would add the following entries:<br />
<ul>
 <li>Pakistan as a Country</li>
 <li>Punjab as a Province</li>
 <li>Balochistan as a Province</li>
 <li>Sindh as a Province</li>
 <li>Lahore as a City</li>
 <li>Quetta as a City</li>
 <li>Karachi as a City</li> 
 <li>Sukkur as a City</li> 
</ul>
Regional hierarchy helps mostly in reporting, where the report is to be generated, lets say on Sindh Province, would display results from all branches in Sindh. The reason for the report displaying all branches in Sindh when province Sindh is selected is because Regional Hiearchy works like a tree, derived from top to bottom. In our example, the top branch of the tree would be Country Pakistan, then, the next level of hierarchy would be Provinces and finally the Cities at the bottom. Regional hieararchy is completely controllable by the user to allow any number of levels in the hierarchy. This will give flexibility to add further levels of regions, for example, if the user wants to add Districts, Tehsils, Talukas and even Villages.
<br /><br />
Following is the screenshot of a user view of Regional Hierarchy component:
<br /><br />
<div align="center"><img src="../images/help/Organization/RegionalHierarchy1.gif" /></div>
<br /><br />
Regional hierarchy component is divided in two main sections, Regions and Region Types. Region Types must be defined first, in our example following were the region types that were added in the system: Country, Province, City. After adding Region Types, the user will add different regions by selecting its region type, in our example Karachi is listed as a City, and so on.
<br /><br />
<span class="Help_Title2">How to add a new Region Type:</span><br />
To add a new region type, click on Region Types tab, then click on Add Region Type button <img src="../images/help/Organization/RegionalHierarchy2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Region Type <img src="../images/help/Organization/RegionalHierarchy2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new region type cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Region Types:</span><br />
Following operations can be performed on a selected region type:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Region Type Details" title="View this Region Type Details" /></td>
  <td><strong>View this Region Type Details:</strong> This will open a window that will display all of the details of the selected region type.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Region Type Details" title="Edit this Region Type Details" /></td>
  <td><strong>Edit this Region Type Details:</strong> This will open a window that will allow modifying/updating the selected region type.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Region Type" title="Delete this Region Type" /></td>
  <td><strong>Delete this Region Type:</strong> This action will delete the selected region type. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<span class="Help_Title2">How to add a new Region:</span><br />
To add a new region, click on Add Region button <img src="../images/help/Organization/RegionalHierarchy3.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Region <img src="../images/help/Organization/RegionalHierarchy3.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new region cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Region:</span><br />
Following operations can be performed on a selected region:
<br /><br />
<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Region Details" title="View this Region Details" /></td>
  <td><strong>View this Region Details:</strong> This will open a window that will display all of the details of the selected region.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Region Details" title="Edit this Region Details" /></td>
  <td><strong>Edit this Region Details:</strong> This will open a window that will allow modifying/updating the selected region.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Region" title="Delete this Region" /></td>
  <td><strong>Delete this Region:</strong> This action will delete the selected region. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />		

<!-- Stations -->
<a name="#Stations"></a><span class="Help_Title">Stations</span><br /><br />
This component manages different stations / branches / offices of the organization. 
<br /><br />
Following is the screenshot of a user view of Stations component:
<br /><br />
<div align="center"><img src="../images/help/Organization/Stations1.gif" /></div>
<br /><br />
Just like regional hierarchy, stations component is divided in two main sections, Stations and Station Types. Station Types must be added before adding any station.
<br /><br />
<span class="Help_Title2">How to add a new Station Type:</span><br />
To add a new station type, click on Station Types tab, then click on Add Station Type button <img src="../images/help/Organization/Stations2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Station Type <img src="../images/help/Organization/Stations2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new station type cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Station Types:</span><br />
Following operations can be performed on a selected station type:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Station Type Details" title="View this Station Type Details" /></td>
  <td><strong>View this Station Type Details:</strong> This will open a window that will display all of the details of the selected station type.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Station Type Details" title="Edit this Station Type Details" /></td>
  <td><strong>Edit this Station Type Details:</strong> This will open a window that will allow modifying/updating the selected station type.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Station Type" title="Delete this Station Type" /></td>
  <td><strong>Delete this Station Type:</strong> This action will delete the selected station type. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<span class="Help_Title2">How to add a new Station:</span><br />
To add a new station, click on Add Station button <img src="../images/help/Organization/Stations3.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Station <img src="../images/help/Organization/Stations3.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new station cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Stations:</span><br />
Following operations can be performed on a selected station:
<br /><br />
<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Stations Details" title="View this Stations Details" /></td>
  <td><strong>View this Stations Details:</strong> This will open a window that will display all of the details of the selected station.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Stations Details" title="Edit this Stations Details" /></td>
  <td><strong>Edit this Stations Details:</strong> This will open a window that will allow modifying/updating the selected station.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Stations" title="Delete this Stations" /></td>
  <td><strong>Delete this Stations:</strong> This action will delete the selected station. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />		

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Departments -->
<a name="#Departments"></a><span class="Help_Title">Departments</span><br /><br />
Every organization is divided in several different departments, each department usually have a department head, who manages every employee working under his supervision. This component helps in defining the departments and their department heads in the organization.
<br /><br />
Following is the screenshot of a user view of departments component:
<br /><br />
<div align="center"><img src="../images/help/Organization/Departments1.gif" /></div>
<br /><br />
<span class="Help_Title2">How to add a new department:</span><br />
To add a new department, click on Add Department button <img src="../images/help/Organization/Departments2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Department <img src="../images/help/Organization/Departments2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new department cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Departments:</span><br />
Following operations can be performed on a selected department:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Department Details" title="View this Department Details" /></td>
  <td><strong>View this Department Details:</strong> This will open a window that will display all of the details of the selected department.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Department Details" title="Edit this Department Details" /></td>
  <td><strong>Edit this Department Details:</strong> This will open a window that will allow modifying/updating the selected department.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Department" title="Delete this Department" /></td>
  <td><strong>Delete this Department:</strong> This action will delete the selected department. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />		

<!-- Data Backup -->
<a name="#DataBackup"></a><span class="Help_Title">Data Backup</span><br /><br />
This comoponent is used to take complete backup of the complete database. This is a purely technical function and must be used only by the authorized personnel. In information technology, backup refers to making copies of data so that these additional copies may be used to restore the original after a data loss event. These additional copies are typically called "backups." Backups are useful primarily for two purposes. The first is to restore a state following a disaster (called disaster recovery). The second is to restore small numbers of files after they have been accidentally deleted or corrupted.
<br /><br />
Please note that only one data backup is allowed per day. Backup files save as the current date. In a particular day, if three data backups are taken, the first two will be overwritten by the third attempt.
<br /><br />
It is recommended to take data backup atleast once a week, most organizations prefer taking their data backup on Sundays, so it won't interfere with the regular operations of the system. Data backup files generated through the system can be copied on any external media.
<br /><br />
Following is the screenshot of a user view of Data Backup component:
<br /><br />
<div align="center"><img src="../images/help/Organization/DataBackup1.gif" /></div>
<br /><br />
<span class="Help_Title2">How to take a new data backup:</span><br />
To take a new data backup, click on Backup Data button <img src="../images/help/Organization/DataBackup2.gif" />. 
<br /><br />
<span class="Help_Title2">Operations on Data Backup:</span><br />
Following operations can be performed on a selected data backup file:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconBackup.gif" border="0" alt="Download this Backup File" title="Download this Backup File" /></td>
  <td><strong>Download this Backup File:</strong> This will download the selected data backup file.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Backup File" title="Delete this Backup File" /></td>
  <td><strong>Delete this Backup File:</strong> This action will delete the selected data backup file. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- System Settings -->
<a name="#SystemSettings"></a><span class="Help_Title">System Settings</span><br /><br />
This comoponent is used to define a certain settings that affect the whole FM System. Access to this area must be restricted to the administrator only, as any unnecessary change may create problems in the system. 
<br /><br />
Following is the screenshot of a user view of System Settings component:
<br /><br />
<div align="center"><img src="../images/help/Organization/SystemSettings1.gif" /></div>
<br /><br />
<span class="Help_Title2">How to save system settings:</span><br />
To save settings, click on Save button <img src="../images/help/Organization/SystemSettings2.gif" />. 
<br /><br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />		

<!-- System Log -->
<a name="#SystemLog"></a><span class="Help_Title">System Log</span><br /><br />
This component displays log of each action users perform. The exact server date/time, along with the user name and IP address is recorded. 
<br /><br />
Following is the screenshot of a user view of System Log component:
<br /><br />
<div align="center"><img src="../images/help/Organization/SystemLog1.gif" /></div>
<br /><br /><br />
<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />


</div>		
</td></tr></table>