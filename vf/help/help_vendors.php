<table border="0" cellspacing="0" cellpadding="3" width="98%" align="center"><tr><td>
 <a name="#Top"></a>
 <span class="Help_Title">Vendors Module</span><br /><br />
<div class="Help_Contents" align="justify">
This module deals with managing the vendors settings, that will help in operating the entire FM System. Most of the actions in this module are to be performed once, usually when setting up the system. A certain minor changes may be required here and there, especially when there is a change in organizational structure. Components in this module are mostly for administrative purposes only, users can have the role to view information on a certain components.
<br /><br />
The Vendor module comprises of the following components (Click for details):
<br /><br /><br />
<table border="0" cellspacing="0" cellpadding="3" width="90%" align="center">
 <tr>
  <td width="10%" align="center"><a href="#Vendors"><img src="../images/vendors/iconVendors.gif" title="Vendors" alt="Vendors" border="0" /><br />Vendors</a></td>
  <td width="10%" align="center"><a href="#Products"><img src="../images/vendors/iconProducts.gif" title="Products" alt="Products" border="0" /><br />Products</a></td>
  <td width="10%" align="center"><a href="#PurchaseRequests"><img src="../images/vendors/iconPurchaseRequests.gif" title="Purchase Requests" alt="Purchase Requests" border="0" /><br />Purchase Requests</a></td>
  <td width="10%" align="center"><a href="#Quotations"><img src="../images/vendors/iconQuotations.gif" title="Quotations" alt="Quotations" border="0" /><br />Quotations</a></td>
  <td width="10%" align="center"><a href="#PurchaseOrders"><img src="../images/vendors/iconPurchaseOrders.gif" title="Purchase Orders" alt="Purchase Orders" border="0" /><br />Purchase Orders</a></td>  
 </tr>
</table>
<br /><br />
<hr size="1" />

<!-- Vendors -->
<a name="#Vendors"></a><span class="Help_Title">Vendors</span><br /><br />
This component manages the Vendors.
<br /><br />
Following is the screenshot of a user view of Vendors component:
<br /><br />
<div align="center"><img src="../images/help/Vendors/Vendors1.gif" /></div>
<br /><br />
Vendor component is divided in two main sections, Vendors and Vendor Types. Vendor Types must be defined first.
<br /><br />
<span class="Help_Title2">How to add a new Vendor Type:</span><br />
To add a new Vendor Type, click on Vendor Types tab, then click on Add Vendor Type button <img src="../images/help/Vendors/Vendors2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Vendor Type <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Vendor Type cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Vendor Types:</span><br />
Following operations can be performed on a selected Vendor Type:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Vendor Type Details" title="View this Vendor Type Details" /></td>
  <td><strong>View this Vendor Type Details:</strong> This will open a window that will display all of the details of the selected Vendor Type.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Vendor Type Details" title="Edit this Vendor Type Details" /></td>
  <td><strong>Edit this Vendor Type Details:</strong> This will open a window that will allow modifying/updating the selected Vendor Type.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Vendor Type" title="Delete this Vendor Type" /></td>
  <td><strong>Delete this Vendor Type:</strong> This action will delete the selected Vendor Type. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<span class="Help_Title2">How to add a new Vendor:</span><br />
To add a new Vendor, click on Vendors tab, then click on Add Vendor button <img src="../images/help/Vendors/Vendors2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Vendor <img src="../images/help/Vendors/Vendors2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Vendor cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Vendors:</span><br />
Following operations can be performed on a selected Vendor:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Vendor Details" title="View this Vendor Details" /></td>
  <td><strong>View this Vendor Details:</strong> This will open a window that will display all of the details of the selected Vendor.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Vendor Details" title="Edit this Vendor Details" /></td>
  <td><strong>Edit this Vendor Details:</strong> This will open a window that will allow modifying/updating the selected Vendor.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Vendor" title="Delete this Vendor" /></td>
  <td><strong>Delete this Vendor:</strong> This action will delete the selected Vendor. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />		

<!-- Products -->
<a name="#Products"></a><span class="Help_Title">Products</span><br /><br />
This component manages different Products. 
<br /><br />
Following is the screenshot of a user view of Products component:
<br /><br />
<div align="center"><img src="../images/help/Vendors/Products1.gif" /></div>
<br /><br />
Products component is divided in two main sections, Products and Categories. Categories must be added before adding any Product.
<br /><br />

<span class="Help_Title2">How to add a new Category:</span><br />
To add a new Category, click on Categorys tab, then click on Add Category button <img src="../images/help/Categorys/Categorys2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Category <img src="../images/help/Categorys/Categorys2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Category cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Categorys:</span><br />
Following operations can be performed on a selected Category:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Category Details" title="View this Category Details" /></td>
  <td><strong>View this Category Details:</strong> This will open a window that will display all of the details of the selected Category.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Category Details" title="Edit this Category Details" /></td>
  <td><strong>Edit this Category Details:</strong> This will open a window that will allow modifying/updating the selected Category.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Category" title="Delete this Category" /></td>
  <td><strong>Delete this Category:</strong> This action will delete the selected Category. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<span class="Help_Title2">How to add a new Product:</span><br />
To add a new Product, click on Products tab, then click on Add Product button <img src="../images/help/Products/Products2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Product <img src="../images/help/Products/Products2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Product cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Products:</span><br />
Following operations can be performed on a selected Product:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Product Details" title="View this Product Details" /></td>
  <td><strong>View this Product Details:</strong> This will open a window that will display all of the details of the selected Product.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Product Details" title="Edit this Product Details" /></td>
  <td><strong>Edit this Product Details:</strong> This will open a window that will allow modifying/updating the selected Product.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconFolder.gif" border="0" alt="Product Documents" title="Product Documents" /></td>
  <td><strong>Product Documents:</strong> This will open a window that will allow uploading/deleting/downloading the selected Product Documents.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconImages.gif" border="0" alt="Product Images" title="Product Images" /></td>
  <td><strong>Product Images:</strong> This will open a window that will allow uploading/deleting/downloading the selected Product Images.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Product" title="Delete this Product" /></td>
  <td><strong>Delete this Product:</strong> This action will delete the selected Product. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Purchase Requests -->
<a name="#PurchaseRequests"></a><span class="Help_Title">Purchase Requests</span><br /><br />
Every organization is divided in several different departments, each department usually have a department head, who manages every employee working under his supervision. This component helps in defining the departments and their department heads in the organization.
<br /><br />
Following is the screenshot of a user view of Purchase Requests component:
<br /><br />
<div align="center"><img src="../images/help/Vendors/PurchaseRequests1.gif" /></div>
<br /><br />
<span class="Help_Title2">How to add a new Purchase Request:</span><br />
To add a new Purchase Request, click on Purchase Requests tab, then click on Add Purchase Request button <img src="../images/help/Purchase Requests/Purchase Requests2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Purchase Request <img src="../images/help/Purchase Requests/Purchase Requests2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Purchase Request cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Purchase Requests:</span><br />
Following operations can be performed on a selected Purchase Request:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Purchase Request Details" title="View this Purchase Request Details" /></td>
  <td><strong>View this Purchase Request Details:</strong> This will open a window that will display all of the details of the selected Purchase Request.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Purchase Request Details" title="Edit this Purchase Request Details" /></td>
  <td><strong>Edit this Purchase Request Details:</strong> This will open a window that will allow modifying/updating the selected Purchase Request.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconContents.gif" border="0" alt="View Quotations list against this Purchase Request" title="View Quotations list against this Purchase Request" /></td>
  <td><strong>View Quotations list against this Purchase Request:</strong> This will open a window that will allow to view Quotations against the select Purchase Request.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Purchase Request" title="Delete this Purchase Request" /></td>
  <td><strong>Delete this Purchase Request:</strong> This action will delete the selected Purchase Request. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />		

<!-- Quotations -->
<a name="#Quotations"></a><span class="Help_Title">Quotations</span><br /><br />
This component is used for Quotations.
<br /><br />
Following is the screenshot of a user view of Quotation component:
<br /><br />
<div align="center"><img src="../images/help/Vendors/Quotations.gif" /></div>
<br /><br />

<span class="Help_Title2">How to add a new Quotation:</span><br />
To add a new Quotation, click on Quotations tab, then click on Add Quotation button <img src="../images/help/Quotations/Quotations2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add Quotation <img src="../images/help/Quotations/Quotations2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Quotation cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Quotations:</span><br />
Following operations can be performed on a selected Quotation:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Quotation Details" title="View this Quotation Details" /></td>
  <td><strong>View this Quotation Details:</strong> This will open a window that will display all of the details of the selected Quotation.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Quotation Details" title="Edit this Quotation Details" /></td>
  <td><strong>Edit this Quotation Details:</strong> This will open a window that will allow modifying/updating the selected Quotation.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Quotation" title="Delete this Quotation" /></td>
  <td><strong>Delete this Quotation:</strong> This action will delete the selected Quotation. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

<!-- Purchase Orders -->
<a name="#PurchaseOrders"></a><span class="Help_Title">Purchase Orders</span><br /><br />
This component is used for Purchase Orders.
<br /><br />
Following is the screenshot of a user view of Purchase Orders component:
<br /><br />
<div align="center"><img src="../images/help/Vendors/PurchaseOrders.gif" /></div>
<br /><br />

<span class="Help_Title2">How to add a new Purchase Order:</span><br />
To add a new Purchase Order, click on Purchase Orders tab, then click on Add New button <img src="../images/help/Vendors/PurchaseOrders2.gif" />. This will open a new tab, fill in the required information in the new window, then click Add New <img src="../images/help/Purchase Orders/Purchase Orders2.gif" /> button in that window. All mandatory fields have a red asterisk in front of the field. A new Purchase Order cannot be created until all required fields are filled in. 
<br /><br />
<span class="Help_Title2">Operations on Purchase Orders:</span><br />
Following operations can be performed on a selected Purchase Order:
<br /><br />

<table style="border: 1px solid #999999; background-color:#fffbdc;" border="0" cellspacing="0" cellpadding="6" width="100%" align="center">
 <tr>
  <td width="20" valign="top" align="center"><img src="../images/icons/iconDetails.gif" border="0" alt="View this Purchase Order Details" title="View this Purchase Order Details" /></td>
  <td><strong>View this Purchase Order Details:</strong> This will open a window that will display all of the details of the selected Purchase Order.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconEdit.gif" border="0" alt="Edit this Purchase Order Details" title="Edit this Purchase Order Details" /></td>
  <td><strong>Edit this Purchase Order Details:</strong> This will open a window that will allow modifying/updating the selected Purchase Order.</td>
 </tr>
 <tr>
  <td valign="top" align="center"><img src="../images/icons/iconDelete.gif" border="0" alt="Delete this Purchase Order" title="Delete this Purchase Order" /></td>
  <td><strong>Delete this Purchase Order:</strong> This action will delete the selected Purchase Order. Please use this function carefully, information once deleted cannot be retrieved back. There will be a Confirmation window, asking for the final decision before removing the data from the database.</td>
 </tr>
</table>
<br /><br />

<a href="#Top"><img src="../images/icons/toparrow.gif" border="0" /> Back to Top</a>
<hr size="1" />

</div>		
</td></tr></table>