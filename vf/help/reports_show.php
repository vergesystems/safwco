<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/hr/clsHR_Help.php');
$objHelp = new clsHR_Help();

include('../include/top2.php');
print($objHelp->Help_Reports());
include('../include/bottom2.php');
?>