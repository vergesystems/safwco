<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Help.php');
$objHelp = new clsFMS_Help();

include('../include/top2.php');
print($objHelp->Help_TheOrganization());
include('../include/bottom2.php');
?>