<?php

// General Errors
$varErrorArray[1000] = '1|Sorry, demo account restricted to make any changes';
$varErrorArray[1001] = '1|Sorry, Invaild Request...';
$varErrorArray[1002] = '2|Your Preferences have been Saved...';
$varErrorArray[1003] = '1|Sorry, Unable to upload your file...';
$varErrorArray[1004] = '2|Your image has been uploaded Successfully...';
$varErrorArray[1005] = '1|Sorry, you can\'t upload very large images. Maximum limit is 1 MB';
$varErrorArray[1006] = '2|Image has been deleted Successfully...';
$varErrorArray[1007] = '2|Thumbnail Created Successfully...';
$varErrorArray[1008] = '1|Sorry, Unable to Upload your File!!';
$varErrorArray[1009] = '1|Sorry, You don\'t have access to this area of the Control Panel!';

$varErrorArray[1010] = '1|Sorry, You are not authorized to perform this action!';
$varErrorArray[1011] = '1|Sorry, You cannot delete the Administrator account!!';

$varErrorArray[1100] = '2|Data backup has been saved successfully...';
$varErrorArray[1101] = '1|Sorry, Unable to take this Data Backup, please contact the Administrator!';
$varErrorArray[1102] = '2|Data Backup file has been deleted successfully!';
$varErrorArray[1103] = '1|Sorry, Unable to delete this data backup file, please contact the Administrator!';
$varErrorArray[1104] = '2|Your settings have been saved!';

// Employee Management - 2000
$varErrorArray[2000] = '1|Sorry, Wrong Username and/or Password...';
$varErrorArray[2001] = '1|Sorry, your account is not Active yet!';
$varErrorArray[2002] = '1|Please login first!';
$varErrorArray[2003] = '1|Sorry, that Username already exists!';
$varErrorArray[2004] = '2|Employee Account Created Successfully!!!';
$varErrorArray[2005] = '1|Sorry! Unable to create the employee account!';
$varErrorArray[2006] = '1|Sorry, that E-Mail Address alreay exists!';
$varErrorArray[2007] = '2|Employee Account Updated Successfully...';
$varErrorArray[2008] = '2|Employee Account has been Deleted Successfully...';
$varErrorArray[2009] = '1|Sorry Unable to Delete Employee\'s Account!';
$varErrorArray[2010] = '1|Sorry Employee Account Not Found!';
$varErrorArray[2011] = '1|Sorry the UserName you entered contains invalid characters!';
$varErrorArray[2012] = '1|No changes were made!';

$varErrorArray[2013] = '1|Sorry unable to delete this employee! One or more customers are created by this employee!';
$varErrorArray[2014] = '1|Sorry unable to delete this employee! One or more customer groups were created by this employee!';
$varErrorArray[2015] = '1|Sorry unable to delete this employee! This employee has one or more documents available in the system!';
$varErrorArray[2016] = '1|Sorry unable to delete this employee! One or more regions are assigned to this employee!';
$varErrorArray[2017] = '1|Sorry unable to delete this employee! One or more targets are assigned to this employee!';
$varErrorArray[2018] = '1|Sorry unable to delete this employee! One or more loans were applied by this employee!';
$varErrorArray[2019] = '1|Sorry unable to delete this employee! One or more loan repayments were collected by this employee!';
$varErrorArray[2020] = '1|Sorry unable to delete this employee! One or more loan write off was applied by this employee!';
$varErrorArray[2021] = '1|Sorry unable to delete this employee! One or more loan was processed by this employee!';
$varErrorArray[2022] = '1|Sorry unable to delete this employee! One or more transactions were found in the system for this employee!';
$varErrorArray[2023] = '2|Employee Roles Updated Successfully...';

// 2100 Employee Types
$varErrorArray[2100] = '1|Sorry, that Employee Type Name already exists in the database!';
$varErrorArray[2101] = '2|Employee Type added successfully...!';
$varErrorArray[2102] = '1|Sorry, Unable to Add this Employee Type!!';
$varErrorArray[2103] = '2|Emplooyee Type Updated Successfully...';
$varErrorArray[2104] = '1|No Changes Were Made...';
$varErrorArray[2105] = '2|Employee Type Deleted Successfully...';
$varErrorArray[2106] = '1|Sorry, unable to delete this Employee Type.';
$varErrorArray[2107] = '1|Sorry, Invalid Employee Type!';
$varErrorArray[2108] = '2|Employee Type Roles Updated Successfully...';
$varErrorArray[2109] = '1|Sorry, Employee Type Administrator cannot be Deleted!';
$varErrorArray[2110] = '1|Sorry, Administrator Roles cannot be changed!';
$varErrorArray[2111] = '1|Sorry, Unable to delete this employee type, one or more employees are added as this employee type!';
$varErrorArray[2112] = '1|Sorry, Unable to delete this employee type, one or more employee types are dependent on this employee type';

// 2200 Employee Documents
$varErrorArray[2300] = '2|Document has been added successfully...!';
$varErrorArray[2301] = '1|Sorry, Unable to Add this Document!!';
$varErrorArray[2302] = '2|Document has been Updated Successfully...';
$varErrorArray[2303] = '1|No Changes Were Made...';
$varErrorArray[2304] = '2|Document Deleted Successfully...';
$varErrorArray[2305] = '1|Sorry, unable to delete this Document, Please contact the Administrator';
$varErrorArray[2307] = '1|Sorry, Invalid Employee Document!';
$varErrorArray[2308] = '1|Sorry, this filename already exists in our database!';

// The Organization - 3000
// 3000s Regional Hierarchy
$varErrorArray[3000] = '1|Sorry, that Region Type Name already exists in the database!';
$varErrorArray[3001] = '1|Sorry, Region Parent does not exist!';
$varErrorArray[3002] = '2|Region Type added successfully...!';
$varErrorArray[3003] = '1|Sorry, Unable to Add this Region Type!!';
$varErrorArray[3004] = '2|Region Type Updated Successfully...';
$varErrorArray[3005] = '1|No Changes Were Made...';
$varErrorArray[3006] = '2|Region Type Deleted Successfully...';
$varErrorArray[3007] = '1|Sorry, unable to delete this Region Type.';
$varErrorArray[3008] = '1|Sorry, Invalid Region Type!';
$varErrorArray[3009] = '1|Sorry, Unable to delete this region type, it contains one or more regions!';
$varErrorArray[3010] = '1|Sorry, Unable to delete this region type, because it is parent of one or more region types!';

// 3020s Regions
$varErrorArray[3020] = '2|Region has been added successfully...';
$varErrorArray[3021] = '1|Unable to add this region!';
$varErrorArray[3022] = '2|Region Updated Successfully...';
$varErrorArray[3023] = '1|No Changes Were Made...';
$varErrorArray[3024] = '2|Region Deleted Successfully...';
$varErrorArray[3025] = '1|Sorry, unable to delete this Region!';
$varErrorArray[3026] = '1|Sorry, cannot delete this Region, because it is parent of one or more regions!';
$varErrorArray[3027] = '2|Region Statistics Updated Successfully';
$varErrorArray[3028] = '1|No changes were made!';
$varErrorArray[3029] = '1|Sorry, cannot delete this Region, because one or more customers are in this region!';
$varErrorArray[3030] = '1|Sorry, cannot delete this Region, because one or more employees are assigned to this region!';
$varErrorArray[3031] = '1|Sorry, cannot delete this Region, because one or more stations are assigned to this region!';


// 3100s Stations
$varErrorArray[3100] = '1|Sorry, that Station already exists in the database!';
$varErrorArray[3101] = '2|Station Added successfully...!';
$varErrorArray[3102] = '1|Sorry, Unable to Add this Station!!';
$varErrorArray[3103] = '2|Station Updated Successfully...';
$varErrorArray[3104] = '1|No Changes Were Made...';
$varErrorArray[3105] = '2|Station Deleted Successfully...';
$varErrorArray[3106] = '1|Sorry, unable to delete this Station.';

$varErrorArray[3107] = '1|Sorry, unable to delete this Station! One or more Bank Cheques are assigned to this Station';
$varErrorArray[3108] = '1|Sorry, unable to delete this Station! One or more Customers are assigned to this Station';
$varErrorArray[3109] = '1|Sorry, unable to delete this Station! One or more Employees are assigned to this Station';
$varErrorArray[3110] = '1|Sorry, unable to delete this Station! It contains one or more station links';
$varErrorArray[3111] = '1|Sorry, unable to delete this Station! It contains one or more station targets';
$varErrorArray[3112] = '1|Sorry, unable to delete this Station! It contains one or more Transactions';
$varErrorArray[3113] = '1|Sorry, unable to delete this Station! It contains one or more Accounts';

$varErrorArray[3114] = '1|Please select a valid region!';



// 3150s Station Types
$varErrorArray[3150] = '1|Sorry, that Station Type Name already exists in the database!';
$varErrorArray[3151] = '2|Station Type added successfully...!';
$varErrorArray[3152] = '1|Sorry, Unable to Add this Station Type!!';
$varErrorArray[3153] = '2|Station Type Updated Successfully...';
$varErrorArray[3154] = '1|No Changes Were Made...';
$varErrorArray[3155] = '2|Station Type Deleted Successfully...';
$varErrorArray[3156] = '1|Sorry, unable to delete this Station Type.';
$varErrorArray[3157] = '1|Sorry, Invalid Station Type!';
$varErrorArray[3158] = '1|Sorry, Unable to delete this station type, it contains one or more Stations!!';

// 3200s Donors
$varErrorArray[3200] = '1|Sorry, that Donor already exists in the database!';
$varErrorArray[3201] = '2|Donor Added successfully...!';
$varErrorArray[3202] = '1|Sorry, Unable to Add this Donor!!';
$varErrorArray[3203] = '2|Donor Updated Successfully...';
$varErrorArray[3204] = '1|No Changes Were Made...';
$varErrorArray[3205] = '2|Donor Deleted Successfully...';
$varErrorArray[3206] = '1|Sorry, unable to delete this donor.';
$varErrorArray[3207] = '1|Sorry, unable to delete this Donor, because it contains one or more Projects!';
$varErrorArray[3208] = '1|Sorry, unable to delete this Donor, because it contains one or more Transactions!';

// 3220s Donor Projects
$varErrorArray[3210] = '2|Donor Project Added successfully...!';
$varErrorArray[3211] = '1|Sorry, Unable to Add this Donor Project!!';
$varErrorArray[3212] = '2|Donor Project Updated Successfully...';
$varErrorArray[3213] = '1|No Changes Were Made...';
$varErrorArray[3214] = '2|Donor Project Deleted Successfully...';
$varErrorArray[3215] = '1|Sorry, unable to delete this Donor Project!';
$varErrorArray[3216] = '1|Sorry, unable to delete this Donor Project, because it contains one or more Activities!';
$varErrorArray[3217] = '1|Sorry, unable to Add this Donor Project, Project Code already exists in the database!';
$varErrorArray[3218] = '1|Sorry, unable to Update this Donor Project, Project Code already exists in the database!';

// 3220 - ProjectA ctivities
$varErrorArray[3220] = '2|Project Activity has been added successfully...';
$varErrorArray[3221] = '1|Sorry, unable to add a new Project Activity!';
$varErrorArray[3222] = '2|Project Activity updated successfully...';
$varErrorArray[3223] = '1|No Changes Were Made...';
$varErrorArray[3224] = '2|Project Activity has been deleted successfully...';
$varErrorArray[3225] = '1|Sorry, unable to delete this Project Activity!';
$varErrorArray[3226] = '1|Sorry, unable to delete this Activity, because it contains one or more Ledger Transaction!';
$varErrorArray[3227] = '1|Sorry, unable to Add this Activity, Activity Code already exists in the database!';
$varErrorArray[3228] = '1|Sorry, unable to Update this Activity, Activity Code already exists in the database!';

/* Accounts  */

// Chart of Accounts Categories
$varErrorArray[3400] = '1|Sorry, this Category contains one or more Controls!';
$varErrorArray[3401] = '2|Category has been deleted successfully!';
$varErrorArray[3402] = '1|Sorry, unable to delete this category, please contact the administrator!';
$varErrorArray[3403] = '2|Category has been added successfully!';
$varErrorArray[3404] = '1|Sorry, unable to add this category, please contact the administrator!';
$varErrorArray[3405] = '1|Sorry, unable to add this category, Please check Start and End Range!';
$varErrorArray[3406] = '2|Category has been updateed successfully!';
$varErrorArray[3407] = '1|Sorry, unable to update this category, please contact the administrator!';
$varErrorArray[3408] = '1|Sorry, unable to update this category, Please check Start and End Range!';

$varErrorArray[3410] = '1|Sorry, this Control Code contains one or more Chart of Accounts!';
$varErrorArray[3411] = '2|Control has been deleted successfully!';
$varErrorArray[3412] = '1|Sorry, unable to delete this control, please contact the administrator!';
$varErrorArray[3413] = '2|Control has been added successfully!';
$varErrorArray[3414] = '1|Sorry, unable to add this control, please contact the administrator!';
$varErrorArray[3415] = '1|Sorry, unable to add this control, Please check Start and End Range!';
$varErrorArray[3416] = '2|Control has been updated successfully!';
$varErrorArray[3417] = '1|Sorry, unable to update this control, please contact the administrator!';
$varErrorArray[3418] = '1|Sorry, unable to update this control, Please check Start and End Range!';
// 7000 Chart of Accounts

$varErrorArray[7000] = '2|Chart Of Account has been added successfully...';
$varErrorArray[7001] = '1|Sorry, unable to add a new Chart Of Account!';
$varErrorArray[7002] = '2|Chart Of Account updated successfully...';
$varErrorArray[7003] = '1|No Changes Were Made...';
$varErrorArray[7004] = '2|Chart Of Account has been deleted successfully...';
$varErrorArray[7005] = '1|Sorry, unable to delete this Chart Of Account, Please contact the administrator!';
$varErrorArray[7006] = '1|Sorry, unable to delete this Chart Of Account, one or more transactions are done against this Chart of Account!';
$varErrorArray[7007] = '1|Sorry, unable to delete this Chart Of Account, It contains one or more Chart of Accounts!';
$varErrorArray[7008] = '1|Sorry, unable to delete this Chart Of Account, It contains one or more Bank Accounts!';
$varErrorArray[7009] = '1|Sorry, unable to add a new Chart Of Account, Account Code does not lie under Controls range!';
$varErrorArray[7010] = '1|Sorry, Parent Chart of Account is already a Child of another Account... Cannot create child of a child account!';
$varErrorArray[7011] = '1|Sorry, unable to delete this Chart Of Account, Budget has been allocated to this account!';


// 7100 - Chart Of Account Type
$varErrorArray[7100] = '2|Chart Of Account Type has been added successfully...';
$varErrorArray[7101] = '1|Sorry, unable to add a new Chart Of Account Type!';
$varErrorArray[7102] = '2|Chart Of Account Type updated successfully...';
$varErrorArray[7103] = '1|No Changes Were Made...';
$varErrorArray[7104] = '2|Chart Of Account Type has been deleted successfully...';
$varErrorArray[7105] = '1|Sorry, unable to delete this Chart Of Account Type!';
$varErrorArray[7106] = '1|Sorry, that Chart of Account Code already exist in the database!';

// 7200 - General Journal
$varErrorArray[7200] = '2|General Journal Entry has been added successfully...<br /><br /><a href="#noanchor" onclick="jsOpenWindow(\'../reports/showreport.php?report=AccountsReports&reporttype=GeneralJournalReceipt&selEmployee=-1&selStation=-1&id=' . $objGeneral->fnGet("id") . '&txtReference=' . $objGeneral->fnGet("reference") . '\', 800,600);" style="font-family: Verdana, Arial; font-size:12px;">Click here to Print Receipt</a><br /><br />';
$varErrorArray[7201] = '1|Sorry, unable to add a new General Journal Entry, Please contact the Administrator!';
$varErrorArray[7202] = '2|General Journal updated successfully...';
$varErrorArray[7203] = '1|No Changes Were Made...';
$varErrorArray[7204] = '2|General Journal has been deleted successfully...';
$varErrorArray[7205] = '1|Sorry, unable to delete this General Journal, Please contact the Administrator!';
$varErrorArray[7206] = '1|Sorry, unable to add a new Entry, Reference Number already exist in the database!';
$varErrorArray[7207] = '1|Sorry, unable to add a new Entry, Cheque Number has been already issed!';
$varErrorArray[7208] = '1|Sorry, unable to add a new Entry, Wrong cheque number is specified!';
$varErrorArray[7209] = '1|Sorry, unable to add a new General Journal Entry, Transaction Amount is greater then Remaing Allocated Amount!';
$varErrorArray[7210] = '1|Sorry, unable to add a new General Journal Entry, Please make sure that Debit and Credit are equal!';
$varErrorArray[7211] = '1|Sorry, unable to update a General Journal Entry, Please make sure that Debit and Credit are equal!';
// 7220 - Quick Entry
$varErrorArray[7220] = '2|Quick Entry has been added successfully...';
$varErrorArray[7221] = '1|Sorry, unable to add a new Quick Entry!';
$varErrorArray[7222] = '2|Quick Entry updated successfully...';
$varErrorArray[7223] = '1|No Changes Were Made...';
$varErrorArray[7224] = '2|Quick Entry has been deleted successfully...';
$varErrorArray[7225] = '1|Sorry, unable to delete this Quick Entry!';
$varErrorArray[7226] = '1|Sorry, unable to add Quick Entry, Debit and Credit must be equal!';
$varErrorArray[7227] = '1|Sorry, unable to add transaction, Please select Donor / Project.';

// Source Of Income
$varErrorArray[7300] = '1|Sorry, that Source Of Income already exists in the database!';
$varErrorArray[7301] = '2|Source Of Income Added successfully...!';
$varErrorArray[7302] = '1|Sorry, Unable to Add this Source Of Income!!';
$varErrorArray[7303] = '2|Source Of Income Updated Successfully...';
$varErrorArray[7304] = '1|No Changes Were Made...';
$varErrorArray[7305] = '2|Source Of Income Deleted Successfully...';
$varErrorArray[7306] = '1|Sorry, unable to delete this Source Of Income, Please contact the Administrator!.';

// 7400 - Close Financial Year
$varErrorArray[7400] = '2|Financial Year has been closed successfully...';
$varErrorArray[7401] = '1|Sorry, unable to Close Financial Year, Please contact the Administrator!';
$varErrorArray[7404] = '2|Close Financial Year has been deleted successfully...';
$varErrorArray[7405] = '1|Sorry, unable to delete this Close Financial Year!';
$varErrorArray[7406] = '1|Sorry, Unable to Close Financial Year on the specified date!';

/* End of Accounts  */

// Manufacturer Management
$varErrorArray[5100] = '1|Sorry, that Manufacturer already exists in the database!';
$varErrorArray[5101] = '2|Manufacturer Added successfully...!';
$varErrorArray[5102] = '1|Sorry, Unable to Add this Manufacturer!!';
$varErrorArray[5103] = '2|Manufacturer Updated Successfully...';
$varErrorArray[5104] = '1|No Changes Were Made...';
$varErrorArray[5105] = '2|Manufacturer Deleted Successfully...';
$varErrorArray[5106] = '1|Sorry, unable to delete this Manufacturer.';
$varErrorArray[5107] = '1|Sorry, this Manufacturer Code already exists in the database!';
$varErrorArray[5108] = '1|Sorry, unable to delete this Manufacturer, because it contains one or more products';
/* Vendor's Management*/
$varErrorArray[5200] = '1|Sorry, that Vendor already exists in the database!';
$varErrorArray[5201] = '2|Vendor Added successfully...!';
$varErrorArray[5202] = '1|Sorry, Unable to Add this Vendor!!';
$varErrorArray[5203] = '2|Vendor Updated Successfully...';
$varErrorArray[5204] = '1|No Changes Were Made...';
$varErrorArray[5205] = '2|Vendor Deleted Successfully...';
$varErrorArray[5206] = '1|Sorry, unable to delete this Vendor.';
$varErrorArray[5207] = '1|Sorry, this Vendor Code already exists in the database!';
$varErrorArray[5208] = '1|Sorry, unable to delete this Vendor, one or more transactions are based on this Vendor';
/* Vendors Types*/
$varErrorArray[5220] = '1|Sorry, that Vendor Type already exists in the database!';
$varErrorArray[5221] = '2|Vendor Type Added successfully...!';
$varErrorArray[5222] = '1|Sorry, Unable to Add this Vendor Type!!';
$varErrorArray[5223] = '2|Vendor Type Updated Successfully...';
$varErrorArray[5224] = '1|No Changes Were Made...';
$varErrorArray[5225] = '2|Vendor Type Deleted Successfully...';
$varErrorArray[5226] = '1|Sorry, unable to delete this Vendor Type';
$varErrorArray[5227] = '1|Sorry, No Records were found!!';
$varErrorArray[5228] = '1|Sorry, unable to delete this Vendor Type, it contains one or more Vendors';

// 5300 - Purchase Orders
$varErrorArray[5300] = '2|Purchase Order has been added successfully...';
$varErrorArray[5301] = '1|Sorry, unable to add a new Purchase Order!';
$varErrorArray[5302] = '2|Purchase Order updated successfully...';
$varErrorArray[5303] = '1|No Changes Were Made...';
$varErrorArray[5304] = '2|Purchase Order has been deleted successfully...';
$varErrorArray[5305] = '1|Sorry, unable to delete this Purchase Order!';
$varErrorArray[5306] = '1|Sorry, This Purchase Order already exists!';

// 5400 - Bill
$varErrorArray[5400] = '2|Bill has been added successfully...';
$varErrorArray[5401] = '1|Sorry, unable to add a new Bill!';
$varErrorArray[5402] = '2|Bill updated successfully...';
$varErrorArray[5403] = '1|No Changes Were Made...';
$varErrorArray[5404] = '2|Bill has been deleted successfully...';
$varErrorArray[5405] = '1|Sorry, unable to delete this Bill!';
// 5900 - Pay Bill
$varErrorArray[5900] = '2|Pay Bill has been added successfully...';
$varErrorArray[5901] = '1|Sorry, unable to add a new Pay Bill!';
$varErrorArray[5902] = '2|Pay Bill updated successfully...';
$varErrorArray[5903] = '1|No Changes Were Made...';
$varErrorArray[5904] = '2|Pay Bill has been deleted successfully...';
$varErrorArray[5905] = '1|Sorry, unable to delete this Pay Bill!';


/* Bank Accounts*/
$varErrorArray[3230] = '2|Bank Account Added successfully...!';
$varErrorArray[3231] = '1|Sorry, Unable to Add this Bank Account!!';
$varErrorArray[3232] = '2|Bank Account Updated Successfully...';
$varErrorArray[3233] = '1|No Changes Were Made...';
$varErrorArray[3234] = '2|Bank Account Deleted Successfully...';
$varErrorArray[3235] = '1|Sorry, unable to delete this Bank Account, please contact the Administrator!';
$varErrorArray[3236] = '1|Sorry, unable to delete this Bank Account, one or more transactions are done against this Bank Account!';
$varErrorArray[3237] = '1|Sorry, unable to delete this Bank Account, It contains one or more Chart of Accounts!';
$varErrorArray[3238] = '1|Sorry, unable to add this Bank Account, This Chart of Account has already been assigned to another Bank Account!';
$varErrorArray[3239] = '1|Sorry, unable to Update this Bank Account, This Chart of Account has already been assigned to another Bank Account!';
/*End of Bank Accounts*/

/*Banks*/
$varErrorArray[3250] = '2|Bank Added successfully...!';
$varErrorArray[3251] = '1|Sorry, Unable to Add this Bank!!';
$varErrorArray[3252] = '2|Bank Updated Successfully...';
$varErrorArray[3253] = '1|No Changes Were Made...';
$varErrorArray[3254] = '2|Bank Deleted Successfully...';
$varErrorArray[3255] = '1|Sorry, unable to delete this Bank!';

$varErrorArray[3270] = '2|Bank Reconciliation has been completed successfully...';


// 3300 Departments
$varErrorArray[3300] = '1|Sorry, that Department Code already exists in the database!';
$varErrorArray[3301] = '2|Department has been Added successfully...!';
$varErrorArray[3302] = '1|Sorry, Unable to Add this Department!!';
$varErrorArray[3303] = '2|Department has been Updated Successfully...';
$varErrorArray[3304] = '1|No Changes Were Made...';
$varErrorArray[3305] = '2|Department has been Deleted Successfully...';
$varErrorArray[3306] = '1|Sorry, unable to delete this Department., One or more child departments exists under this department';
$varErrorArray[3307] = '1|Sorry, unable to delete this Department, One or more employees are added in this department';

/* Bank Check Books*/
$varErrorArray[5500] = '2|Bank Check Book Added successfully...!';
$varErrorArray[5501] = '1|Sorry, Unable to Add this Bank Check Book!';
$varErrorArray[5502] = '2|Bank Check Book Updated Successfully...';
$varErrorArray[5503] = '1|No Changes Were Made...';
$varErrorArray[5504] = '2|Bank Check Book Deleted Successfully...';
$varErrorArray[5505] = '1|Sorry, unable to delete this Bank Check Book!';
$varErrorArray[5506] = '1|Sorry, unable to delete this Bank Check Book! It contains one or more cancelled checks';
$varErrorArray[5507] = '1|Sorry, unable to delete this Bank Check Book! One or more checks from this book have already been issued';
/*End of Bank Checks*/

/*Cancelled Checks*/
$varErrorArray[5600] = '1|This Bank Check has been Cancelled already...!';
$varErrorArray[5601] = '2|Bank Check Cancelled successfully...!';
$varErrorArray[5602] = '1|Sorry, Unable to Cancel this Bank Check!';
$varErrorArray[5603] = '2|Cancelled Bank Check Updated Successfully...';
$varErrorArray[5604] = '1|No Changes Were Made...';
$varErrorArray[5605] = '2|Cancelled Bank Check Deleted Successfully...';
$varErrorArray[5606] = '1|Sorry, unable to delete this Cancelled Bank Check!';
$varErrorArray[5607] = '1|Please enter a valid Check Number!';
$varErrorArray[5608] = '1|Sorry, Unable to Cancel this Bank Check!, It has already been issued';
/*End of Cancelled Checks*/

/*Write Checks*/
$varErrorArray[5700] = '1|This Bank Check Number has been Written already...!';
$varErrorArray[5701] = '2|Bank Check Written successfully...!';
$varErrorArray[5702] = '1|Sorry, Unable to Write this Bank Check!';
$varErrorArray[5703] = '2|Written Bank Check Updated Successfully...';
$varErrorArray[5704] = '1|No Changes Were Made...';
$varErrorArray[5705] = '2|Written Bank Check Deleted Successfully...';
$varErrorArray[5706] = '1|Sorry, unable to delete this Written Bank Check!';
$varErrorArray[5707] = '1|Please enter a valid Bank Check Number!';
$varErrorArray[5708] = '1|This Bank Check Number is Cancelled, try another...!!!!';
$varErrorArray[5709] = '1|No records were found..!!!';
/*End of Write Checks*/

/*Bank Deposits*/
$varErrorArray[5800] = '2|Bank Deposit Added successfully...!';
$varErrorArray[5801] = '1|Sorry, Unable to add this Bank Deposit!';
$varErrorArray[5802] = '2|Bank Deposit Updated Successfully...';
$varErrorArray[5803] = '1|No Changes Were Made...';
$varErrorArray[5804] = '2|Bank Deposit Deleted Successfully...';
$varErrorArray[5805] = '1|Sorry, unable to delete this Bank Deposit!';
/*End Bank Deposits*/


/* Inventory Management */

/* Vendor's Management
$varErrorArray[5200] = '1|Sorry, that Vendor already exists in the database!';
$varErrorArray[5201] = '2|Vendor Added successfully...!';
$varErrorArray[5202] = '1|Sorry, Unable to Add this Vendor!!';
$varErrorArray[5203] = '2|Vendor Updated Successfully...';
$varErrorArray[5204] = '1|No Changes Were Made...';
$varErrorArray[5205] = '2|Vendor Deleted Successfully...';
$varErrorArray[5206] = '1|Sorry, unable to delete this Vendor.';
$varErrorArray[5207] = '1|Sorry, this Vendor Code already exists in the database!';
*/

/* Inventory Management */

// Category Management
$varErrorArray[5000] = '1|Sorry, that Category already exists in the database!';
$varErrorArray[5001] = '2|Category Added successfully...!';
$varErrorArray[5002] = '1|Sorry, Unable to Add this Category!!';
$varErrorArray[5003] = '2|Category Updated Successfully...';
$varErrorArray[5004] = '1|No Changes Were Made...';
$varErrorArray[5005] = '2|Category Deleted Successfully...';
$varErrorArray[5006] = '1|Sorry, unable to delete this category.';
$varErrorArray[5007] = '1|Sorry, unable to delete this Category, because it contains one or more products';
$varErrorArray[5008] = '1|Sorry, Category Code already exists in the database';

//Products Management
$varErrorArray[6000] = '1|Sorry, that Product already exists in the database!';
$varErrorArray[6001] = '2|Product has been Added successfully...!';
$varErrorArray[6002] = '1|Sorry, Unable to Add this Product!!';
$varErrorArray[6003] = '2|Product has been Updated Successfully...';
$varErrorArray[6004] = '1|No Changes Were Made...';
$varErrorArray[6005] = '2|Product has been Deleted Successfully...';
$varErrorArray[6006] = '1|Sorry, unable to delete this Product.';
$varErrorArray[6007] = '1|Sorry, Product does not exist.';


/* Product Images
$varErrorArray[6007] = '1|Sorry, Unable to upload your file';
$varErrorArray[6008] = '1|Unable to upload Product Image, please contact the Administrator';
$varErrorArray[6009] = '2|Product Image has been uploaded successfully...';
$varErrorArray[6010] = '2|Product Image has been deleted successfully...';
$varErrorArray[6011] = '1|Unable to Remove your Image... Please contact the Administrator!';
$varErrorArray[6012] = '2|Product Image has been Makrked as Thumbnail successfully...';
$varErrorArray[6013] = '1|Unable to Mark this Product image as thumbnail!';
$varErrorArray[6014] = '1|Sorry, this Product Stock Number already exists in the database!';

$varErrorArray[6020] = '2|Product code has been re-issued successfully..';
*/

/* Product Purchase Request */
$varErrorArray[6100] = '1|Sorry, Unable to Add this Purchase Request, Request Number already exists in the purchase request database!';
$varErrorArray[6101] = '2|Purchase Request Added successfully...!';
$varErrorArray[6102] = '1|Sorry, Unable to Add this Purchase Request!';
$varErrorArray[6103] = '2|Purchase Request Updated Successfully...';
$varErrorArray[6104] = '1|No Changes Were Made...';
$varErrorArray[6105] = '2|Purchase Request Deleted Successfully!';
$varErrorArray[6106] = '1|Sorry, unable to delete this Purchase Request!';
$varErrorArray[6107] = '1|Sorry, unable to delete this Purchase Request, it contains or more Quotations!';
$varErrorArray[6108] = '1|Sorry, Unable to update this Purchase Request, Request Number already exists in the purchase request database!';

/* Product Checkout
$varErrorArray[6200] = '2|Product has been checked-in successfully';
$varErrorArray[6201] = '1|Sorry, unable to check-in this product!';
$varErrorArray[6202] = '1|Sorry, this product is not available for checkout!';
$varErrorArray[6203] = '2|Product has been checked-out successfully';
$varErrorArray[6204] = '1|Sorry, unable to check-out this product!';
*/

/* Product Sell
$varErrorArray[6300] = '1|Sorry, the product status must be "Available" to sell it!';
$varErrorArray[6301] = '1|Sorry, the prouct could not be sold!';
$varErrorArray[6302] = '2|Product has been sold successfully!';
$varErrorArray[6304] = '1|Sorry, this product is not available to be sold!';
*/

// 6400 - Bank Withdrawal
$varErrorArray[6400] = '2|Bank Withdrawal has been added successfully...';
$varErrorArray[6401] = '1|Sorry, unable to add a new Bank Withdrawal!';
$varErrorArray[6402] = '2|Bank Withdrawal updated successfully...';
$varErrorArray[6403] = '1|No Changes Were Made...';
$varErrorArray[6404] = '2|Bank Withdrawal has been deleted successfully...';
$varErrorArray[6405] = '1|Sorry, unable to delete this Bank Withdrawal!';

// Customer Management
$varErrorArray[8515] = '1|Sorry, that Customer already exists in the database!';
$varErrorArray[8516] = '2|Customer Added successfully...!';
$varErrorArray[8517] = '1|Sorry, Unable to Add this Customer!';
$varErrorArray[8518] = '2|Customer Updated Successfully...';
$varErrorArray[8519] = '1|No Changes Were Made...';
$varErrorArray[8520] = '2|Customer Deleted Successfully...';
$varErrorArray[8521] = '1|Sorry, unable to delete this Customer.';
$varErrorArray[8522] = '1|Sorry, this Customer Code already exists in the database!';
$varErrorArray[8523] = '1|Sorry, unable to delete this Customer, one or more transactions are done against this Customer';

// 8600 - Invoice
$varErrorArray[8600] = '2|Invoice has been added successfully...';
$varErrorArray[8601] = '1|Sorry, unable to add a new Invoice!';
$varErrorArray[8602] = '2|Invoice updated successfully...';
$varErrorArray[8603] = '1|No Changes Were Made...';
$varErrorArray[8604] = '2|Invoice has been deleted successfully...';
$varErrorArray[8605] = '1|Sorry, unable to delete this Invoice!';
$varErrorArray[8606] = '1|Sorry, unable to add a new Invoice, Invoice No already exists in the database!';
$varErrorArray[8607] = '1|Sorry, unable to add a new Invoice, Invoice has been already issued against selected sales order!';
$varErrorArray[8608] = '1|Sorry, unable to update Invoice, Invoice No already exists in the database!';

// 8610 - Invoice
$varErrorArray[8610] = '2|Recurring Invoice has been added successfully...';
$varErrorArray[8611] = '1|Sorry, unable to add a new Recurring Invoice!';
$varErrorArray[8612] = '2|Recurring Invoice updated successfully...';
$varErrorArray[8613] = '1|No Changes Were Made...';
$varErrorArray[8614] = '2|Recurring Invoice has been deleted successfully...';
$varErrorArray[8615] = '1|Sorry, unable to delete this Recurring Invoice!';
$varErrorArray[8616] = '1|Sorry, unable to add a new Recurring Invoice, Recurring Invoice Date must be between 1 and 31!';

// 8700 - Jobs
$varErrorArray[8700] = '2|Job has been added successfully...';
$varErrorArray[8701] = '1|Sorry, unable to add a new Job!';
$varErrorArray[8702] = '2|Job updated successfully...';
$varErrorArray[8703] = '1|No Changes Were Made...';
$varErrorArray[8704] = '2|Job has been deleted successfully...';
$varErrorArray[8705] = '1|Sorry, unable to delete this Job!';

// 8800 - Statements
$varErrorArray[8800] = '2|Statement has been added successfully...';
$varErrorArray[8801] = '1|Sorry, unable to add a new Statement!';
$varErrorArray[8802] = '2|Statement updated successfully...';
$varErrorArray[8803] = '1|No Changes Were Made...';
$varErrorArray[8804] = '2|Statement has been deleted successfully...';
$varErrorArray[8805] = '1|Sorry, unable to delete this Statement!';

// 9000 - Services Management
$varErrorArray[9000] = '1|Sorry, that Service Code already exists in the database!';
$varErrorArray[9001] = '2|Service has been Added successfully...!';
$varErrorArray[9002] = '1|Sorry, Unable to Add this Service!';
$varErrorArray[9003] = '2|Service has been Updated Successfully...';
$varErrorArray[9004] = '1|No Changes Were Made...';
$varErrorArray[9005] = '2|Service has been Deleted Successfully...';
$varErrorArray[9006] = '1|Sorry, unable to delete this Service.';
$varErrorArray[9007] = '1|Sorry, No Records were found..';

/* Service Types*/
$varErrorArray[9100] = '1|Sorry, that Service Type already exists in the database!';
$varErrorArray[9101] = '2|Service Type Added successfully...!';
$varErrorArray[9102] = '1|Sorry, Unable to Add this Service Type!!';
$varErrorArray[9103] = '2|Service Type Updated Successfully...';
$varErrorArray[9104] = '1|No Changes Were Made...';
$varErrorArray[9105] = '2|Service Type Deleted Successfully...';
$varErrorArray[9106] = '1|Sorry, unable to delete this Service Type';
$varErrorArray[9107] = '1|Sorry, No Records were found!!';

// 10000 - Quotations Management
$varErrorArray[10000] = '1|Sorry, that Quotation No already exists in the database!';
$varErrorArray[10001] = '2|Quotation has been Added successfully...!';
$varErrorArray[10002] = '1|Sorry, Unable to Add this Quotation!';
$varErrorArray[10003] = '2|Quotation has been Updated Successfully...';
$varErrorArray[10004] = '1|No Changes Were Made...';
$varErrorArray[10005] = '2|Quotation has been Deleted Successfully...';
$varErrorArray[10006] = '1|Sorry, unable to delete this Quotation.';
$varErrorArray[10007] = '1|Sorry, No Records were found..';
$varErrorArray[10008] = '1|Sorry, unable to add a new Quotation! Please Select a Customer or Add New Customer';
$varErrorArray[10009] = '1|Sorry, unable to delete this Quotation, it contains or more Purchase Orders';

// 11000 - Receipts Management
$varErrorArray[11000] = '1|Sorry, that Receipt No already exits in the database!';
$varErrorArray[11001] = '2|Receipt has been Added successfully...!';
$varErrorArray[11002] = '1|Sorry, Unable to add this Receipt!';
$varErrorArray[11003] = '2|Receipt has been Updated Successfully...';
$varErrorArray[11004] = '1|No Changes Were Made...';
$varErrorArray[11005] = '2|Receipt has been Deleted Successfully...';
$varErrorArray[11006] = '1|Sorry, unable to delete this Receipt.';
$varErrorArray[11007] = '1|Sorry, No Records were found..';

// 12000 - Sales Order
$varErrorArray[12000] = '1|Sorry, This Order No already exists in the database!';
$varErrorArray[12001] = '2|Sales Order has been added successfully...';
$varErrorArray[12002] = '1|Sorry, unable to add a new Sales Order!';
$varErrorArray[12003] = '2|Sales Order updated successfully...';
$varErrorArray[12004] = '1|No Changes Were Made...';
$varErrorArray[12005] = '2|Sales Order has been deleted successfully...';
$varErrorArray[12006] = '1|Sorry, unable to delete this Sales Order!';
$varErrorArray[12007] = '1|Sorry, No records were found...!';
$varErrorArray[12008] = '1|Sorry, unable to add a new Sales Order, Sales Order has already been generated against selected Quotation!';
$varErrorArray[12009] = '1|Sorry, unable to update Sales Order, Sales Order has already been generated against selected Quotation!';

// 13000 - Budget
$varErrorArray[13000] = '2|Budget has been added successfully';
$varErrorArray[13001] = '1|Sorry, Unable to add new budget';
$varErrorArray[13002] = '2|Budget has been updated successfully';
$varErrorArray[13003] = '1|Sorry, No changes were made';
$varErrorArray[13004] = '2|Budget deleted successfully';
$varErrorArray[13005] = '1|Sorry, Unable to delete';
$varErrorArray[13006] = '1|Sorry, Budget Value is too less than the actual value!!!';
$varErrorArray[13007] = '1|Sorry, No Records Found';
$varErrorArray[13008] = '1|Sorry, Unable to delete this Budget, It has been used in General Journal';
$varErrorArray[13009] = '1|Sorry, Unable to delete this Budget, Budget has been allocated';

// 13050 - Budget Allocation
$varErrorArray[13050] = '2|Budget has been Allocated successfully';
$varErrorArray[13051] = '1|Sorry, Unable to Allocate';
$varErrorArray[13052] = '2|Budget Allocation has been updated successfully';
$varErrorArray[13053] = '1|Sorry, No changes were made';
$varErrorArray[13054] = '2|Budget Allocated deleted successfully';
$varErrorArray[13055] = '1|Sorry, Unable to delete';
$varErrorArray[13056] = '1|Sorry, Allocated Amount Can never Exceed the Budget Amount!!!';
$varErrorArray[13057] = '1|Sorry, No Record was found';
$varErrorArray[13058] = '1|Sorry, Amount Can Not Exceed the Remaining Allocated Amount!';
$varErrorArray[13059] = '1|Sorry, Unable to delete this Allocation, one or more transaction(s) exists for this chart of Account';

if ($objGeneral->fnGet("error") != '')
	$varError = $objGeneral->fnGet("error");

if (($varError != '') && ($varError > 100))
{
	$varErrMsg = $varErrorArray[$varError];

    if ($varError == 101)
        $varErrMsg = $objGeneral->fnGet("error2");


	$varErrMsgArray = explode("|", $varErrMsg);
	if ($varErrMsgArray[0] == 1)
	{
		$varErrTableColor = '#808179';
		$varErrTableImg = '../images/iconError.png';
	}
	else if ($varErrMsgArray[0] == 2)
	{
		$varErrTableColor = '#808179';
		$varErrTableImg = '../images/iconErrorTick.png';
	}
	$varErrMsg = $varErrMsgArray[1];

	if ($sExtraError != '') $varErrMsg = $varErrMsg . '<br /><span style="color:red;">' . $sExtraError . '</span>';

	$varErrorMessage = '
		<br><table width="80%" bgcolor="' . $varErrTableColor . '" border="1" bordercolor="#C0C0C0" style="border-collapse:collapse;" cellpadding="0" cellspacing="0" align=center>
		<tr valign="middle">
         <td>
    	  <table width="100%" border="0">
    	   <tr>
    		<td align="left" width="10%"><img src="' . $varErrTableImg . '" border="0"></td>
            <td align="center">
    		 <span style="color:#000000;" class="errormessage">' . $varErrMsg . '</span>
    		</td>
    		<td width="10%"></td>
    	   </tr>
    	  </table>
		 </td>
	    </tr>
	   </table>';

	print($varErrorMessage);
}
?>