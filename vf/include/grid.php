<html>
<head>
	<title>ActiveWidgets Grid :: Examples</title>
	<style>body {font: 12px Tahoma}</style>

	<!-- ActiveWidgets stylesheet and scripts -->
	<link href="../../../VSF-PHP/components/activewidgets/runtime/styles/system/aw.css" rel="stylesheet" type="text/css" ></link>
	<script src="../../../VSF-PHP/components/activewidgets/runtime/lib/aw.js"></script>

	<!-- grid format -->
	<style>
		#myGrid {height: 200px; width: 500px;}
		#myGrid .aw-row-selector {text-align: center}

		#myGrid .aw-column-0 {width:  80px;}
		#myGrid .aw-column-1 {width: 200px;}
		#myGrid .aw-column-2 {text-align: right;}
		#myGrid .aw-column-3 {text-align: right;}
		#myGrid .aw-column-4 {text-align: right;}

		#myGrid .aw-grid-cell {border-right: 1px solid threedlightshadow;}
		#myGrid .aw-grid-row {border-bottom: 1px solid threedlightshadow;}

		/* box model fix for strict doctypes, safari */
		.aw-strict #myGrid .aw-grid-cell {padding-right: 3px;}
		.aw-strict #myGrid .aw-grid-row {padding-bottom: 3px;}

	</style>

	<!-- grid data -->
	<script>
		var myData = [
			["MSFT","Microsoft Corporation", "314,571.156", "32,187.000", "55000"],
			["ORCL", "Oracle Corporation", "62,615.266", "9,519.000", "40650"],
			["SAP", "SAP AG (ADR)", "40,986.328", "8,296.420", "28961"],
		];

		var myColumns = [
			"Ticker", "Company Name", "Market Cap.", "$ Sales", "Employees"
		];
	</script>
</head>
<body>
	<script>

	//	create ActiveWidgets Grid javascript object
	var obj = new AW.UI.Grid;
	obj.setId("myGrid");

	//	define data formats
	var str = new AW.Formats.String;
	var num = new AW.Formats.Number;

	obj.setCellFormat([str, str, num, num, num]);

	//	provide cells and headers text
	obj.setCellText(myData);
	obj.setHeaderText(myColumns);

	//	set number of rows/columns
	obj.setRowCount(6);
	obj.setColumnCount(4);

	//	enable row selectors
	obj.setSelectorVisible(true);
	obj.setSelectorText(function(i){return this.getRowPosition(i)+1});

	//	set headers width/height
	obj.setSelectorWidth(28);
	obj.setHeaderHeight(20);

	//	set row selection
	//obj.setSelectionMode("single-row");

	//	set click action handler
	obj.onCellClicked = function(event, col, row){window.status = this.getCellText(col, row)};

	obj.setCellEditable(true);

	//	write grid html to the page
	document.write(obj);

	</script>

    <a href="#noanchor" onclick="obj.setRowCount(7);obj.refresh();">HELLO</a>
</body>
</html>