<?php
// Settings File
include('../../system/config/settings.php');

// Include Classes
include(cVSFFolder . '/classes/clsErrorHandler.php');		// Error Handler Class
$objErrorHandler = new clsErrorHandler(cErrorHandlerSenderEmail, cErrorHandlerSenderName, cErrorHandlerSubject);	// Error Handler
$objErrorHandler->varDebug = cDebugMode;

include(cVSFFolder . '/classes/clsDatabase.php');			// The Database Class
include(cVSFFolder . '/classes/clsGeneral.php');			// Common PHP Functions
include(cVSFFolder . '/classes/clsEmail.php');				// E-Mail SMTP Class
include(cVSFFolder . '/classes/clsEncryption.php');		    // Encryption Class

// Global Class Objects
//$objDatabaseMaster = new clsDatabase(cDatabaseType, "c", cDatabaseHost, cDatabaseUser, cDatabasePassword, cDatabaseDatabase, cErrorHandlerDatabaseErrorLogFile);	// A Database Object
$objDatabase = new clsDatabase(cDatabaseType, "c", cDatabaseHost, cDatabaseUser, cDatabasePassword, cDatabaseDatabase, cErrorHandlerDatabaseErrorLogFile);	// A Database Object


$objGeneral = new clsGeneral();					// All Common Functions
$objEncryption = new clsBlowfish();				// An Encryption Object

$sServerHost = $_SERVER["HTTP_HOST"];
// $sServerHost = "";

$varResult = $objDatabase->Query("SELECT * FROM organizations AS O WHERE 1=1");
if ($objDatabase->RowsNumber($varResult) <= 0){
	
} //$objGeneral->fnRedirect("http://www.vergefinancials.com");

// Database Select
/*
$sDatabaseType = $objDatabaseMaster->Result($varResult, 0, "D.DatabaseType");
$sDatabaseHost = $objDatabaseMaster->Result($varResult, 0, "D.HostName");
$sDatabaseUser = $objDatabaseMaster->Result($varResult, 0, "D.UserName");
$sDatabasePassword = $objDatabaseMaster->Result($varResult, 0, "D.Password");
$sDatabaseDB = $objDatabaseMaster->Result($varResult, 0, "D.Database");
*/

$sOrganizationName = $objDatabase->Result($varResult, 0, "O.OrganizationName");

define("cOrganizationCode", $objDatabase->Result($varResult, 0, "O.OrganizationCode"));
define("cOrganizationId", $objDatabase->Result($varResult, 0, "O.OrganizationId"));
define("cOrganizationName", $objDatabase->Result($varResult, 0, "O.OrganizationName"));
define("cOrganizationURL", $objDatabase->Result($varResult, 0, "O.OrganizationURL"));
define("cOrganizationDemoAccount", $objDatabase->Result($varResult, 0, "O.DemoAccount"));

// For PHP 5 - Default Time Zone
date_default_timezone_set('Asia/Karachi');  // date_default_timezone_set('UTC'); // Asia/Karachi
// Asia/Dhaka

// Session Start
if(!isset($_SESSION['ses']))
{
	session_start();
	$_SESSION['ses']="start";
}

include('../../system/library/clsOrganization_Employees.php');
include('../../system/library/fms/clsFMS_Common.php');
include('../../system/library/clsOrganization_SystemLog.php');

$objEmployee = new clsEmployees();
$objEmployee->iEmployeeId = $_SESSION[cProjectUniqueName . "_EmployeeId"];

$objSystemLog = new clsSystemLog();
$objFMS_Common = new clsFMS_Common();

if ($objEmployee->iEmployeeId == '')
    $objEmployee->iEmployeeId = $_COOKIE[cProjectUniqueName . "_ckEmployeeId"];


?>