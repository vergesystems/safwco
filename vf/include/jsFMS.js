<!--

function VF_Common_Documents_AddNewDocument(sComponentName, iComponentId, sTitle, iAccessLevel, sFileName)
{
    if (sTitle.length <= 0)
        alert("Please enter the Title...");
    else if (sFileName.length <= 0)
        alert("Please upload a file first...");
    else
        window.top.MOOdalBox.open("../common/documents_show.php?componentname=" + sComponentName + "&id=" + iComponentId + "&action=AddDocument&iAccessLevel=" + iAccessLevel + "&txtTitle=" + sTitle + "&txtFileName=" + sFileName, "Documents of " + sTitle, "700 420");

    return(false);
}


/* General Journal Functions */

function GetBankAccounts(iBankId, iDefaultBankAccountId)
{
	document.getElementById("selCheckBook").value = 0;
	SetVal("txtSeries", "");
	OptionsList_RemoveAll("selBankAccount");
	ShowDiv("divBankAccount");
	
	FillSelectBox("selBankAccount", "Select Bank Account", 0);
	for (i=0; i < aBankAccounts.length; i++)
	{
		if (iBankId == aBankAccounts[i][0])
			FillSelectBox("selBankAccount", aBankAccounts[i][3] + " (" + aBankAccounts[i][2] + ")", aBankAccounts[i][1]);
	}
		
	if (iDefaultBankAccountId > 0)	
		document.getElementById("selBankAccount").value = iDefaultBankAccountId;
}

function GetBankAccountsReports(iBankId, iDefaultBankAccountId)
{	
	OptionsList_RemoveAll("selBankAccount");
	ShowDiv("divBankAccount");
	
	FillSelectBox("selBankAccount", "Select Bank Account", 0);
	for (i=0; i < aBankAccounts.length; i++)
	{
		if (iBankId == aBankAccounts[i][0])
			FillSelectBox("selBankAccount", aBankAccounts[i][3] + " (" + aBankAccounts[i][2] + ")", aBankAccounts[i][1]);
	}
		
	if (iDefaultBankAccountId > 0)	
		document.getElementById("selBankAccount").value = iDefaultBankAccountId;
}
function GetDonorProjects(iDonorId, iDefaultDonorProjectId)
{
	OptionsList_RemoveAll("selDonorProject");
	OptionsList_RemoveAll("selProjectActivity");
	ShowDiv("divDonorProject");
	
	FillSelectBox("selDonorProject", "Select Donor Project", 0);
	FillSelectBox("selProjectActivity", "Select Project Activity", 0);
	for (i=0; i < aDonorProjects.length; i++)
	{
		if (iDonorId == aDonorProjects[i][0])
			FillSelectBox("selDonorProject", aDonorProjects[i][3] + " (" + aDonorProjects[i][2] + ")", aDonorProjects[i][1]);
	}
		
	if (iDefaultDonorProjectId > 0)	
		document.getElementById("selDonorProject").value = iDefaultDonorProjectId;
}

function GetProjectActivities(iDonorProjectId, iDefaultActivityId)
{
	OptionsList_RemoveAll("selProjectActivity");
	ShowDiv("divProjectActivity");
	
	FillSelectBox("selProjectActivity", "Select Project Activity", 0);
	for (i=0; i < aProjectActivities.length; i++)
	{
		if (iDonorProjectId == aProjectActivities[i][0])
			FillSelectBox("selProjectActivity", aProjectActivities[i][3] + " (" + aProjectActivities[i][2] + ")", aProjectActivities[i][1]);
	}
		
	if (iDefaultActivityId > 0)	
		document.getElementById("selProjectActivity").value = iDefaultActivityId;
}


/* End of General Journal Functions */


function CalculateTotalOrderPrice()
{
	var dTotalOrderPrice = 0;				
	for (i=0; i <= iRowCounter; i++)
	{
		iQty = GetVal("txtOrderParticulars_Qty_" + i);
		dUnitPrice = GetVal("txtOrderParticulars_UnitPrice_" + i);
		dTotalAmount = iQty * dUnitPrice;						
		dTotalOrderPrice += dTotalAmount;					
	}				
	SetDivVal("divOrderTotalAmount", addCommas(dTotalOrderPrice.toFixed(2)));
	SetVal("OrderTotalAmount", dTotalOrderPrice);
}

function UpdateTotalPayment()
{
	var dTotalAmount = GetVal("OrderTotalAmount");						
	var dDiscount = GetVal("txtDiscountTotal");
	var dTotalPayment = GetVal("txtTotalPayment");
								
	var dTotalPayment = dTotalAmount - dDiscount;					
	SetVal("txtTotalPayment", dTotalPayment);
	UpdateBalance();
}

function UpdateBalance()
{
	var dBalance = 0;
	var dReturnedAmount = 0;
	var dOrderTotalPayment= 0;
	
	var dTotalAmount = Number(GetVal("OrderTotalAmount"));
	var dDiscount = Number(GetVal("txtDiscountTotal"));				
	var dTotalPayment =  Number(GetVal("txtTotalPayment"));								
	
	dOrderTotalPayment = dTotalAmount - dDiscount;				
	if(dOrderTotalPayment < dTotalPayment)
		var dReturnedAmount = (dTotalPayment + dDiscount - dTotalAmount );
	else
		var dBalance = (dTotalAmount - dTotalPayment - dDiscount);				
	
	SetDivVal("divBalance", addCommas(dBalance.toFixed(2)));
	SetDivVal("divReturnedAmount", addCommas(dReturnedAmount.toFixed(2)));
	SetVal("Balance", dBalance);
}

function UpdateProductQty(iRowNumber)
{    								
	var iQty = GetVal("txtOrderParticulars_Qty_" + iRowNumber);
	var dUnitPrice = GetVal("txtOrderParticulars_UnitPrice_" + iRowNumber);
	var dTotalAmount = dUnitPrice * iQty;
	SetDivVal("divOrderParticulars_Amount_" + iRowNumber, addCommas(dTotalAmount.toFixed(2)));
	
	CalculateTotalOrderPrice();	
}

function AddNewRow()
{    
alert(iRowVisible);
	ShowHideDiv("row_" + iRowVisible);
	iRowVisible++;
}

function DeleteRow(iRowNumber)
{
	SetVal("txtOrderParticulars_ProductName_" + iRowNumber, "");
	SetVal("txtOrderParticulars_Qty_" + iRowNumber, "");
	SetVal("txtOrderParticulars_UnitPrice_" + iRowNumber, "");
	SetDivVal("divOrderParticulars_Amount_" + iRowNumber, "");
	
	CalculateTotalOrderPrice();
	UpdateTotalPayment();						
}

//--> 		