function VF_Common_Documents_AddNewDocument(sComponentName, iComponentId, sTitle, iAccessLevel, sFileName)
{
    if (sTitle.length <= 0)
        alert("Please enter the Title...");
    else if (sFileName.length <= 0)
        alert("Please upload a file first...");
    else
        window.top.MOOdalBox.open("../common/documents_show.php?componentname=" + sComponentName + "&id=" + iComponentId + "&action=AddDocument&iAccessLevel=" + iAccessLevel + "&txtTitle=" + sTitle + "&txtFileName=" + sFileName, "Documents of " + sTitle, "700 420");

    return(false);
}


/* General Journal Functions */

function GetBankAccounts(iBankId, iDefaultBankAccountId)
{
	document.getElementById("selCheckBook").value = 0;
	SetVal("txtSeries", "");
	OptionsList_RemoveAll("selBankAccount");
	ShowDiv("divBankAccount");
	
	FillSelectBox("selBankAccount", "Select Bank Account", 0);
	for (i=0; i < aBankAccounts.length; i++)
	{
		if (iBankId == aBankAccounts[i][0])
			FillSelectBox("selBankAccount", aBankAccounts[i][3] + " (" + aBankAccounts[i][2] + ")", aBankAccounts[i][1]);
	}
		
	if (iDefaultBankAccountId > 0)	
		document.getElementById("selBankAccount").value = iDefaultBankAccountId;
}

function GetDonorProjects(iDonorId, sSelectDonorProjectBox, sSelectActivityBox, sDivDonorProject)
{
    OptionsList_RemoveAll(sSelectDonorProjectBox);	
    OptionsList_RemoveAll(sSelectActivityBox);
	ShowDiv(sDivDonorProject);
	
	//FillSelectBox(sSelectDonorProjectBox, "Select Donor Project", 0);
	//FillSelectBox(sSelectActivityBox, "Select Activity", 0);
	if(iDonorId > 0)
		xajax_AJAX_FMS_Accounts_GeneralJournal_GetDonorProject(iDonorId, sSelectDonorProjectBox);
	
}

function GetProjectActivities(iDonorProjectId, sSelectActivityBox, sDivProjectActivity)
{
	ShowDiv(sDivProjectActivity);
	//FillSelectBox(sSelectActivityBox, "Select Project Activity", 0);
	if(iDonorProjectId > 0)
		xajax_AJAX_FMS_Accounts_GeneralJournal_GetProjectActivity(iDonorProjectId, sSelectActivityBox);
}

function GetDonorProjects_QuickEntries(iDonorId, sSelectDonorProjectBox, sSelectActivityBox, sDivDonorProject)
{
    OptionsList_RemoveAll(sSelectDonorProjectBox);	
    OptionsList_RemoveAll(sSelectActivityBox);
	ShowDiv(sDivDonorProject);
	
	//FillSelectBox(sSelectDonorProjectBox, "Select Donor Project", 0);
	//FillSelectBox(sSelectActivityBox, "Select Activity", 0);
	if(iDonorId > 0)
		xajax_AJAX_FMS_Accounts_QuickEntries_GetDonorProject(iDonorId, sSelectDonorProjectBox);
	
}

function GetProjectActivities_QuickEntries(iDonorProjectId, sSelectActivityBox, sDivProjectActivity)
{
	ShowDiv(sDivProjectActivity);
	//FillSelectBox(sSelectActivityBox, "Select Project Activity", 0);
	if(iDonorProjectId > 0)
		xajax_AJAX_FMS_Accounts_QuickEntries_GetProjectActivity(iDonorProjectId, sSelectActivityBox);
}	
			
function GetBankAccountsReports(iBankId, iDefaultBankAccountId)
{	
	OptionsList_RemoveAll("selBankAccount");
	ShowDiv("divBankAccount");
	
	FillSelectBox("selBankAccount", "Select Bank Account", 0);
	for (i=0; i < aBankAccounts.length; i++)
	{
		if (iBankId == aBankAccounts[i][0])
			FillSelectBox("selBankAccount", aBankAccounts[i][3] + " (" + aBankAccounts[i][2] + ")", aBankAccounts[i][1]);
	}
		
	if (iDefaultBankAccountId > 0)	
		document.getElementById("selBankAccount").value = iDefaultBankAccountId;
}

function GetDonorProjects_Reports(iDonorId, iDefaultDonorProjectId)
{
	OptionsList_RemoveAll("selDonorProject");
	OptionsList_RemoveAll("selProjectActivity");
	ShowDiv("divDonorProject");

	FillSelectBox("selDonorProject", "Select Donor Project", 0);
	FillSelectBox("selProjectActivity", "Select Project Activity", 0);
	for (i=0; i < aDonorProjects.length; i++)
	{
		if (iDonorId == aDonorProjects[i][0])
			FillSelectBox("selDonorProject", aDonorProjects[i][3] + " (" + aDonorProjects[i][2] + ")", aDonorProjects[i][1]);
	}

	if (iDefaultDonorProjectId > 0)
		document.getElementById("selDonorProject").value = iDefaultDonorProjectId;
}

function GetDonors(iDonorId, iDefaultDonorProjectId)
{
	OptionsList_RemoveAll("selDonorProject");		
	FillSelectBox("selDonorProject", "Select Donor Project", 0);	
	for (i=0; i < aDonorProjects.length; i++)
	{
		if (iDonorId == aDonorProjects[i][0])
			FillSelectBox("selDonorProject", aDonorProjects[i][3] + " (" + aDonorProjects[i][2] + ")", aDonorProjects[i][1]);
	}

	if (iDefaultDonorProjectId > 0)
		document.getElementById("selDonorProject").value = iDefaultDonorProjectId;
}

function GetProjectActivities_Reports(iDonorProjectId, iDefaultActivityId)
{
	OptionsList_RemoveAll("selProjectActivity");
	ShowDiv("divProjectActivity");
	
	FillSelectBox("selProjectActivity", "Select Project Activity", 0);
	for (i=0; i < aProjectActivities.length; i++)
	{
		if (iDonorProjectId == aProjectActivities[i][0])
			FillSelectBox("selProjectActivity", aProjectActivities[i][3] + " (" + aProjectActivities[i][2] + ")", aProjectActivities[i][1]);
	}
		
	if (iDefaultActivityId > 0)	
		document.getElementById("selProjectActivity").value = iDefaultActivityId;
}
function GetDonorProjects_Budget(iDonorId)
{
	OptionsList_RemoveAll("selDonorProject");
	ShowDiv("divDonorProject");

	FillSelectBox("selDonorProject", "Select Donor Project", 0);
	for (i=0; i < aDonorProjects.length; i++)
	{
		if (iDonorId == aDonorProjects[i][0])
			FillSelectBox("selDonorProject", aDonorProjects[i][3] + " (" + aDonorProjects[i][2] + ")", aDonorProjects[i][1]);
	}
}
/* End of General Journal Functions */


function CalculateTotalOrderPrice()
{
	var dTotalOrderPrice = 0;				
	for (i=0; i <= iRowCounter; i++)
	{
		iQty = GetVal("txtOrderParticulars_Qty_" + i);
		dUnitPrice = GetVal("txtOrderParticulars_UnitPrice_" + i);
		dTotalAmount = iQty * dUnitPrice;						
		dTotalOrderPrice += dTotalAmount;					
	}				
	SetDivVal("divOrderTotalAmount", addCommas(dTotalOrderPrice.toFixed(2)));
	SetVal("OrderTotalAmount", dTotalOrderPrice);
}

function UpdateTotalPayment()
{
	var dTotalAmount = GetVal("OrderTotalAmount");						
	var dDiscount = GetVal("txtDiscountTotal");
	var dTotalPayment = GetVal("txtTotalPayment");
								
	var dTotalPayment = dTotalAmount - dDiscount;					
	SetVal("txtTotalPayment", dTotalPayment);
	UpdateBalance();
}

function UpdateBalance()
{
	var dBalance = 0;
	var dReturnedAmount = 0;
	var dOrderTotalPayment= 0;
	
	var dTotalAmount = Number(GetVal("OrderTotalAmount"));
	var dDiscount = Number(GetVal("txtDiscountTotal"));				
	var dTotalPayment =  Number(GetVal("txtTotalPayment"));								
	
	dOrderTotalPayment = dTotalAmount - dDiscount;				
	if(dOrderTotalPayment < dTotalPayment)
		var dReturnedAmount = (dTotalPayment + dDiscount - dTotalAmount );
	else
		var dBalance = (dTotalAmount - dTotalPayment - dDiscount);				
	
	SetDivVal("divBalance", addCommas(dBalance.toFixed(2)));
	SetDivVal("divReturnedAmount", addCommas(dReturnedAmount.toFixed(2)));
	SetVal("Balance", dBalance);
}

function UpdateProductQty(iRowNumber)
{    								
	var iQty = GetVal("txtOrderParticulars_Qty_" + iRowNumber);
	var dUnitPrice = GetVal("txtOrderParticulars_UnitPrice_" + iRowNumber);
	var dTotalAmount = dUnitPrice * iQty;
	SetDivVal("divOrderParticulars_Amount_" + iRowNumber, addCommas(dTotalAmount.toFixed(2)));
	
	CalculateTotalOrderPrice();	
}

function AddNewRow()
{    
	ShowHideDiv("row_" + iRowVisible);
	iRowVisible++;
}

function DeleteRow(iRowNumber)
{
	SetVal("txtOrderParticulars_ProductName_" + iRowNumber, "");
	SetVal("txtOrderParticulars_Qty_" + iRowNumber, "");
	SetVal("txtOrderParticulars_UnitPrice_" + iRowNumber, "");
	SetDivVal("divOrderParticulars_Amount_" + iRowNumber, "");
	
	SetVal("hdnChartOfAccountId_"+iRowNumber+"_value", "");
	SetVal("hdnStationId"+iRowNumber+"_value", "");
	SetVal("hdnDepartmentId"+iRowNumber+"_value", "");
	SetVal("hdnBudgetId"+iRowNumber+"_value", "");
		
	SetVal("selDonor"+iRowNumber, "0");
	SetVal("selDonorProject"+iRowNumber, "0");
	SetVal("selDonorProjectActivity"+iRowNumber, "0");
	SetVal("selRecipient"+iRowNumber, "-1");
	
	SetVal("hdnVendorId"+iRowNumber+"_value", "");
	SetVal("hdnCustomerId"+iRowNumber+"_value", "");
	SetVal("hdnEmployeeId"+iRowNumber+"_value", "");
	
	SetVal("txtGeneralJournalParticulars_Detail_"+iRowNumber, "");
	SetVal("txtGeneralJournalParticulars_Debit_"+iRowNumber, "0");
	SetVal("txtGeneralJournalParticulars_Credit_"+iRowNumber, "0");
	HideDiv("row_" + iRowNumber);
	iRowVisible--;
														
	CalculateTotalOrderPrice();
	UpdateTotalPayment();
	UpdateTotal();
}

// General Journal Functions
function GetNextAvailableCheck()
{
	iCheckBookId = GetSelectedListBox("selCheckBook");
	SetVal("txtCheckNumber", "");
	if(iCheckBookId > 0)
	xajax_AJAX_FMS_Accounts_GeneralJournal_GetNextAvailableCheck(GetSelectedListBox("selCheckBook"));
}

function ChangeBankPaymentTypes(iBankPaymentType)
{
	SetVal("txtCheckNumber", "");
	if (iBankPaymentType == 0)
	{
		ShowDiv("trCheckNumber");
		ShowDiv("divCheckBook");
		GetCheckBooks(GetSelectedListBox('selBankAccount'),0);
	}
	else
	{
		HideDiv("trCheckNumber");
		HideDiv("divCheckBook");
	}	
}

function GetCheckBooks(iBankAccountId, iDefaultBankCheckBookId)
{
	if ((GetSelectedListBox("selEntryType") == 0) && (GetSelectedListBox("selPaymentType") == 0))
	{
		OptionsList_RemoveAll("selCheckBook");
		
		FillSelectBox("selCheckBook", "Select Check Book", 0);
		for (i=0; i < aBankCheckBooks.length; i++)
			if (aBankCheckBooks[i][0] == iBankAccountId)
				FillSelectBox("selCheckBook", aBankCheckBooks[i][2] + " (" + aBankCheckBooks[i][3] + " - " + aBankCheckBooks[i][4] + ")", aBankCheckBooks[i][1]);

		if (iDefaultBankCheckBookId > 0)
			document.getElementById("selCheckBook").value = iDefaultBankCheckBookId;
	}
}

function UpdateTotal()
{
	var dTotalDebit = 0;
	var dTotalCredit = 0;	
	for (i=0; i < iRowCounter; i++)
	{					
		dDebit = Number(GetVal("txtGeneralJournalParticulars_Debit_" + i));
		dCredit = Number(GetVal("txtGeneralJournalParticulars_Credit_" + i));
		
		// Only allow Numbers					
		if(dDebit != "")
			if (!isNumeric(GetVal("txtGeneralJournalParticulars_Debit_" + i))) return(AlertFocus('Please enter a Numeric Value', "txtGeneralJournalParticulars_Debit_" + i));
		if(dCredit != "")
			if (!isNumeric(GetVal("txtGeneralJournalParticulars_Credit_" + i))) return(AlertFocus('Please enter a Numeric Value', "txtGeneralJournalParticulars_Credit_" + i));	
		// End
			
		if((dDebit > 0 ) && (dCredit > 0))
		{
			dTotalDebit = 0;
			dTotalCredit = 0;
			alert("Please enter only debit or credit in one row, but not both!");
			break;
		}
		dTotalDebit += dDebit;
		dTotalCredit += dCredit;		
	}
	SetDivVal("divDebitTotalAmount", addCommas(dTotalDebit));
	SetDivVal("divCreditTotalAmount", addCommas(dTotalCredit));
	SetVal("TotalDebit", dTotalDebit);
	SetVal("TotalCredit", dTotalCredit);				
}			
function LoadQuickEntry(iQuickEntryId)
{				
	xajax_AJAX_FMS_Accounts_GeneralJournal_LoadQuickEntry(iQuickEntryId);
}

function LoadGeneralJournal(iGeneralJournalId)
{				
	xajax_AJAX_FMS_Accounts_GeneralJournal_LoadGeneralJournal(iGeneralJournalId);
}

function ChangeEntryType()
{
	document.getElementById("selBank").value = 0;
	document.getElementById("selBankAccount").value = 0;
	
	document.getElementById("selPaymentType").value = -1;
	document.getElementById("selReceiptType").value = -1;
	
	//SetVal("txtReference", "");
	SetVal("txtSeries", "");
	HideDiv("trBanks");
	HideDiv("divBankAccount");
	HideDiv("divBankPaymentType");
	
	if (GetSelectedListBox("selEntryType") == 0)
	{
		document.getElementById("divPaymentType").style.display = "inline";
		HideDiv("divReceiptType");
	}
	else if (GetSelectedListBox("selEntryType") == 1)
	{
		document.getElementById("divPaymentType").style.display = "none";
		document.getElementById("divReceiptType").style.display = "inline";
		document.getElementById("divCheckBook").style.display = "none";
		document.getElementById("trCheckNumber").style.display = "none";
	}
	else if (GetSelectedListBox("selEntryType") == 2)
	{
		// Fill Out Reference No & Series
		//SetVal("txtReference", "JV");
		
		//xajax_AJAX_FMS_Accounts_GeneralJournal_GetSeries(GetSelectedListBox("selEntryType"), -1 , -1, -1);
		document.getElementById("divPaymentType").style.display = "none";
		document.getElementById("divReceiptType").style.display = "none";
		document.getElementById("divCheckBook").style.display = "none";
		document.getElementById("trCheckNumber").style.display = "none";
		document.getElementById("divBankAccount").style.display = "none";
	}
}

function ChangePaymentType()
{	
	//SetVal("txtSeries", "");
	//SetVal("txtReference", "");
	if ((GetSelectedListBox("selEntryType") == 0 ) && (GetSelectedListBox("selPaymentType") == 0 ))
	{
		ShowDiv("trBanks");						
		document.getElementById("selBank").value = 0;
		//SetVal("txtReference", "BPV");
		//SetVal("txtSeries", "");
	}
	else if ((GetSelectedListBox("selEntryType") == 0 ) && (GetSelectedListBox("selPaymentType") == 1 ))
	{
		HideDiv("trBanks");
		HideDiv("divBankAccount");
		HideDiv("divCheckBook");
		HideDiv("trCheckNumber");
		//SetVal("txtReference", "CPV");
		//xajax_AJAX_FMS_Accounts_GeneralJournal_GetSeries(GetSelectedListBox("selEntryType"), -1 , GetSelectedListBox("selPaymentType"), -1);
	}					
}

function ChangeReceiptType()
{
	//SetVal("txtSeries", "");
	//SetVal("txtReference", "");
	if ((GetSelectedListBox("selEntryType") == 1) && (GetSelectedListBox("selReceiptType") == 0))
	{
		ShowDiv("trBanks");						
		document.getElementById("selBank").value = 0;
		//SetVal("txtReference", "BRV");
		//SetVal("txtSeries", "");
	}
	else if ((GetSelectedListBox("selEntryType") == 1) && (GetSelectedListBox("selReceiptType") == 1))
	{
		HideDiv("trBanks");
		HideDiv("divBankAccount");
		HideDiv("divCheckBook");
		HideDiv("trCheckNumber");
		//SetVal("txtReference", "CRV");
		//xajax_AJAX_FMS_Accounts_GeneralJournal_GetSeries(GetSelectedListBox("selEntryType"), GetSelectedListBox("selReceiptType") , -1, -1);
	}					
}

function ChangeRecipient(selRecipient, iRecipientNumber)
{
	HideDiv("divVendor" + iRecipientNumber);
	HideDiv("divCustomer" + iRecipientNumber);
	HideDiv("divEmployee" + iRecipientNumber);
	
	if (selRecipient == 0)	// Vendor
		ShowDiv("divVendor" + iRecipientNumber);
	else if (selRecipient == 1)	// Customer
		ShowDiv("divCustomer" + iRecipientNumber);
	if (selRecipient == 2)	// Vendor
		ShowDiv("divEmployee" + iRecipientNumber);						
}

function GetBankPaymentTypes()
{
	if ((GetSelectedListBox("selEntryType") == 0) && (GetSelectedListBox("selPaymentType") == 0))
		ShowDiv("divBankPaymentType");
	SetVal("txtCheckNumber", "");

	// Get Bank Account Series
	iEntryType = GetSelectedListBox("selEntryType");
	iReceiptType = GetSelectedListBox("selReceiptType");
	iPaymentType = GetSelectedListBox("selPaymentType");
	iBankAccountId = GetSelectedListBox("selBankAccount");
	
	//if(iBankAccountId > 0)
		//xajax_AJAX_FMS_Accounts_GeneralJournal_GetSeries(iEntryType, iReceiptType, iPaymentType, iBankAccountId);	
}