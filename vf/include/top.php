<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head>
 <title><?php print(cSystemName); ?></title>
 <meta name="keywords" content="<?php print(cSystemKeywords); ?>" />
 <meta name="description" content="<?php print(cSystemDescription); ?>" />
 <meta name="author" content="VergeSystems - info@vergesystems.com" />
 <meta name="copyright" content="&copy; 2005-2006" />
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <link href="../include/stylesheet.css" type="text/css" rel="stylesheet" />
 <script type="text/javascript" language="Javascript" src="<?php print(cVSFFolder); ?>/javascript/jsFunctions.js"></script>
 <script type="text/javascript" language="JavaScript" src="<?php print(cVSFFolder); ?>/javascript/jsToolTipMessages.js"></script> 
 <script type="text/javascript" language="Javascript" src="../include/jsFinancials.js"></script>
<link rel="stylesheet" href="<?php print(cVSFFolder); ?>/components/moodalbox/css/moodalbox.css" type="text/css" media="screen" />

<?php if (isset($objAJAX)) print($objAJAX->printJavascript(cVSFFolder . '/classes/')); ?>

</head>
<body>
<table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td width="18" rowspan="3" align="left" valign="top"><img src="../images/template/left_brdr.jpg" width="18" height="556" /></td>
  <td align="left" valign="top">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
     <td width="200" height="67" align="left" valign="middle" background="../images/template/top_logo_bg.jpg"><img src="../images/spacer.gif" style="width:20px;" /><img src="../images/template/logo.png" alt="" /></td>
     <td width="70%" align="left" valign="bottom" background="../images/skin/top_logo_bg.jpg">
      <table border="0" cellspacing="0" cellpadding="0" align="right">
       <tr>
        <td style="height:35px;" colspan="10" valign="top" align="right">
    	<?php
    	if ($_SESSION[cOrganizationCode . "_EmployeeId"] != '')          // User Logged In
		{
	    	print($objEmployee->ShowEmployeeLoggedInTop());
		}
		?>
        </td>
       </tr>
       <tr>
       <?php print($objEmployee->ShowEmployeeMainMenu()); ?>
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
       <tr>
        <td width="350px" height="154" align="center" valign="bottom" background="../images/template/topblueheader_bg.jpg"><img src="../images/template/boxes.jpg"alt="" /></td>
        <td width="70%" align="right" valign="middle" background="../images/template/topblueheader_bg.jpg"><img src="../images/template/title.jpg" alt="" /></td>
        
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
        <td background="../images/template/largeiconbg.jpg">&nbsp;</td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
  <td rowspan="3" align="left" valign="top"><img src="../images/template/right_brdr.jpg" width="16" height="556" /></td>
  
 </tr>
 <tr>
  <td align="left" valign="top">
  <?php include('../include/errors.php'); ?>