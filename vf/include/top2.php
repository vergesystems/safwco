<?php
print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
 <title>' . cSystemName . '</title>
 <meta name="keywords" content="' . cSystemKeywords . '" />
 <meta name="description" content="' . cSystemDescription . '" />
 <link href="../include/stylesheet.css" type="text/css" rel="stylesheet" />
 <script type="text/javascript" language="Javascript" src="' . cVSFFolder . '/javascript/jsFunctions.js"></script>
 <script type="text/javascript" language="Javascript" src="' . cVSFFolder . '/components/jQuery/jQuery.js"></script>
 <script type="text/javascript" language="Javascript" src="' . cVSFFolder . '/components/jQuery/jQueryUI.js"></script>
 <link rel="stylesheet" href="' . cVSFFolder . '/components/jQuery/css/redmond/jQueryUI.css" />

 <script type="text/javascript" language="Javascript" src="../include/jsFinancials.js"></script>
 
 <link rel="stylesheet" href="' . cVSFFolder . '/components/moodalbox/css/moodalbox.css" type="text/css" media="screen" />
 ' . (isset($objAJAX) ? $objAJAX->printJavascript(cVSFFolder . '/classes/') : '') . '
 </head>
<body style="background-image:url(); background-color:#ffffff;" topmargin="0" leftmargin="0">');

include('../include/errors.php');

?>