<?php
include('../../controlpanel/include/includes.php');

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "createstore")
		$varError = $objStore->CreateStore($objGeneral->fnGet("id"), $objGeneral->fnGet("code"), $objGeneral->fnGet("txtStoreUserName"), $objGeneral->fnGet("txtPassword"), $objGeneral->fnGet("txtEmailAddress"), $objGeneral->fnGet("affiliateid"));
}

if ($objStore->StoreVerify()) $objGeneral->fnRedirect('../../controlpanel/home/');

include('../../controlpanel/include/top.php');
print($objStore->ShowStoreCreateAccount($objGeneral->fnGet("id"), $objGeneral->fnGet("code")));
include('../../controlpanel/include/bottom.php'); ?>