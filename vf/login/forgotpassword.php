<?php
include('../include/includes.php');

include('../../system/library/clsControlPanel.php');
$objControlPanel = new clsControlPanel();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "forgotpassword")
		$varError = $objControlPanel->ForgotPassword($objGeneral->fnGet("emailaddress"));
	else if ($varAction == "resetpassword")
		$varError = $objControlPanel->ResetPassword($objGeneral->fnGet("emailaddress"), $objGeneral->fnGet("store"), $objGeneral->fnGet("code"), $objGeneral->fnGet("newpassword"), $objGeneral->fnGet("confirmnewpassword"));
}

if ($objStore->StoreVerify()) $objGeneral->fnRedirect('../../controlpanel/home/');

include('../../controlpanel/include/top.php');

if ($varAction == "reset")
	print($objControlPanel->ShowResetPassword($objGeneral->fnGet("emailaddress"), $objGeneral->fnGet("store"), $objGeneral->fnGet("code")));
else 
	print($objControlPanel->ShowForgotPassword());

include('../../controlpanel/include/bottom.php');
?>