<?php
include('../include/includes.php');

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "login")
		$varError = $objEmployee->EmployeeLogin($objGeneral->fnGet("username"), $objGeneral->fnGet("password"), $objGeneral->fnGet("rememberme"));
	if ($varAction == "logout")
		$varError = $objEmployee->EmployeeLogout();
}

// Verify Employee Account
if ($objEmployee->EmployeeVerify()) $objGeneral->fnRedirect('../home/');

include('../include/top.php');
print($objEmployee->ShowEmployeeLogin());
include('../include/bottom.php'); ?>