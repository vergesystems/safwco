<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization_DataBackup.php');
$objDataBackup = new clsDataBackup();

$varAction = $objGeneral->fnGet("action");
if ($varAction != "")
{
    if ($varAction == "DataBackup")
        $varError = $objDataBackup->DataBackup();
	if ($varAction == "DownloadBackup")
        $varError = $objDataBackup->DownloadBackup($objGeneral->fnGet("filename"));
    else if ($varAction == "DeleteBackup")
		$varError = $objDataBackup->DeleteBackup($objGeneral->fnGet("filename"));
}

include('../include/top2.php'); 

$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objDataBackup->ShowAllBackups());

include('../include/bottom2.php'); 
/*
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objOrganization = new clsOrganization();

include('../include/top.php');

include(cVSFFolder . '/classes/clsDHTMLSuite.php');
$objDHTMLSuite = new clsDHTMLSuite();

$aTabs[0][0] = 'Data Backup';
$aTabs[0][1] = '../organization/databackup_showall.php';

print($objDHTMLSuite->TabBar($aTabs, $objOrganization->ShowOrganizationMenu()));
include('../include/bottom.php'); 
*/
?>