<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');

include('../../system/library/clsOrganization_DataBackup.php');
$objDataBackup = new clsDataBackup();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

    if ($varAction == "DataBackup")
        $varError = $objDataBackup->DataBackup();
	if ($varAction == "DownloadBackup")
        $varError = $objDataBackup->DownloadBackup($objGeneral->fnGet("filename"));
    else if ($varAction == "DeleteBackup")
		$varError = $objDataBackup->DeleteBackup($objGeneral->fnGet("filename"));
}

include('../include/top2.php'); 
print($objDataBackup->ShowAllBackups());
include('../include/bottom2.php'); 
?>