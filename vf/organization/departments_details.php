<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objDepartment = new clsDepartments();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateDepartment")
		$varError = $objDepartment->UpdateDepartment(
											$objGeneral->fnGet("id"),
											$objGeneral->fnGet("selDepartmentParentId"),
        									$objGeneral->fnGet("txtDepartmentName"),
        									$objGeneral->fnGet("txtDepartmentCode"),
        									$objGeneral->fnGet("selDepartmentHead"),
        									$objGeneral->fnGet("txtDescription"),
        									$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewDepartment")
        $varError = $objDepartment->AddNewDepartment(
											$objGeneral->fnGet("selDepartmentParentId"),
        									$objGeneral->fnGet("txtDepartmentName"),
        									$objGeneral->fnGet("txtDepartmentCode"),
        									$objGeneral->fnGet("selDepartmentHead"),
        									$objGeneral->fnGet("txtDescription"),
        									$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteDepartment")
		$varError = $objDepartment->DeleteDepartment($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objDepartment->DepartmentDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>