<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objDepartment = new clsDepartments();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteDepartment")
		$varError = $objDepartment->DeleteDepartment($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objDepartment->ShowAllDepartments($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>