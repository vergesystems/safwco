<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');
$sAction = $objGeneral->fnGet("action");

$objEmployeeType = new clsEmployees_Types();
include('../../system/library/clsOrganization.php');
$objDepartment = new clsDepartments();

$varAction = $objGeneral->fnGet("action");
if ($varAction != "")
{
	if ($varAction == "UpdateEmployee")
		$varError = $objEmployee->UpdateEmployee(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("selEmployeeType"),
        									$objGeneral->fnGet("selStation"),
											$objGeneral->fnGet("selDepartment"),
        									$objGeneral->fnGet("txtUserName"),
        									$objGeneral->fnGet("txtPassword"),
        									$objGeneral->fnGet("selStationAccess"),
											$objGeneral->fnGet("selChartOfAccounts"),
											$objGeneral->fnGet("selDonorProjects"),
        									$objGeneral->fnGet("selStatus"),
        									$objGeneral->fnGet("txtFirstName"),
        									$objGeneral->fnGet("txtLastName"),
        									$objGeneral->fnGet("txtFatherName"),
        									$objGeneral->fnGet("txtNICNumber"),
        									$objGeneral->fnGet("txtDesignation"),
        									$objGeneral->fnGet("txtEmailAddress"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtMobileNumber"),
        									$objGeneral->fnGet("txtNotes"),
											$objGeneral->fnGet("chkApplyRestriction"),
											$objGeneral->fnGet("txtRestrictionAmount"));
    else if ($varAction == "AddNewEmployee")
        $varError = $objEmployee->AddNewEmployee(
        									$objGeneral->fnGet("selEmployeeType"),
        									$objGeneral->fnGet("selStation"),
											$objGeneral->fnGet("selDepartment"),
        									$objGeneral->fnGet("txtUserName"),
        									$objGeneral->fnGet("txtPassword"),
        									$objGeneral->fnGet("selStationAccess"),
											$objGeneral->fnGet("selChartOfAccounts"),
											$objGeneral->fnGet("selDonorProjects"),
        									$objGeneral->fnGet("selStatus"),
        									$objGeneral->fnGet("txtFirstName"),
        									$objGeneral->fnGet("txtLastName"),
        									$objGeneral->fnGet("txtFatherName"),
        									$objGeneral->fnGet("txtNICNumber"),
        									$objGeneral->fnGet("txtDesignation"),
        									$objGeneral->fnGet("txtEmailAddress"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtMobileNumber"),
        									$objGeneral->fnGet("txtNotes"),
											$objGeneral->fnGet("chkApplyRestriction"),
											$objGeneral->fnGet("txtRestrictionAmount"));
	else if($varAction == "DeleteEmployee")
		$varError = $objEmployee->DeleteEmployee($objGeneral->fnGet("id"));
		
	else if ($varAction == "AddNewEmployeeType")
        $varError = $objEmployeeType->AddNewEmployeeType($objGeneral->fnGet("txtEmployeeTypeName"), $objGeneral->fnGet("selEmployeeTypeParent"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateEmployeeType")
		$varError = $objEmployeeType->UpdateEmployeeType($objGeneral->fnGet("id"), $objGeneral->fnGet("txtEmployeeTypeName"), $objGeneral->fnGet("selEmployeeTypeParent"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteEmployeeType")
		$varError = $objEmployeeType->DeleteEmployeeType($objGeneral->fnGet("id"));	
		
	else if($varAction == "UpdateDepartment")
		$varError = $objDepartment->UpdateDepartment(
											$objGeneral->fnGet("id"),
											$objGeneral->fnGet("selDepartmentParentId"),
        									$objGeneral->fnGet("txtDepartmentName"),
        									$objGeneral->fnGet("txtDepartmentCode"),
        									$objGeneral->fnGet("selDepartmentHead"),
        									$objGeneral->fnGet("txtDescription"),
        									$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewDepartment")
        $varError = $objDepartment->AddNewDepartment(
											$objGeneral->fnGet("selDepartmentParentId"),
        									$objGeneral->fnGet("txtDepartmentName"),
        									$objGeneral->fnGet("txtDepartmentCode"),
        									$objGeneral->fnGet("selDepartmentHead"),
        									$objGeneral->fnGet("txtDescription"),
        									$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteDepartment")
		$varError = $objDepartment->DeleteDepartment($objGeneral->fnGet("id"));	
}
include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");

if ($sPageType == "")
	print($objEmployee->ShowAllEmployees($objGeneral->fnGet("p"), $objGeneral->fnGet("station"), $objGeneral->fnGet("employeetype")));
else if ($sPageType == "details")
	print($objEmployee->ShowEmployeeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
else if ($sPageType == "employeesledger")
	print($objEmployee->ShowAllEmployeesLedger($objGeneral->fnGet("id"), $objGeneral->fnGet("p")));	
	
else if ($sPageType == "employeetypes")
	print($objEmployeeType->ShowAllEmployeeTypes($objGeneral->fnGet("p")));	
else if ($sPageType == "employeetypes_details")	
	print($objEmployeeType->EmployeeTypeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));	

else if ($sPageType == "departments")
	print($objDepartment->ShowAllDepartments($objGeneral->fnGet("p")));	
else if ($sPageType == "departments_details")	
	print($objDepartment->DepartmentDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');
?>