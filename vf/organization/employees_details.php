<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateEmployee")
		$varError = $objEmployee->UpdateEmployee(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("selEmployeeType"),
        									$objGeneral->fnGet("selStation"),
											$objGeneral->fnGet("selDepartment"),
        									$objGeneral->fnGet("txtUserName"),
        									$objGeneral->fnGet("txtPassword"),
        									$objGeneral->fnGet("selEmployeeReportTo"),
											$objGeneral->fnGet("selChartOfAccounts"),
											$objGeneral->fnGet("selDonorProjects"),
        									$objGeneral->fnGet("selStatus"),
        									$objGeneral->fnGet("txtFirstName"),
        									$objGeneral->fnGet("txtLastName"),
        									$objGeneral->fnGet("txtFatherName"),
        									$objGeneral->fnGet("txtNICNumber"),
        									$objGeneral->fnGet("txtDesignation"),
        									$objGeneral->fnGet("txtEmailAddress"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtMobileNumber"),
        									$objGeneral->fnGet("txtNotes"),
											$objGeneral->fnGet("chkApplyRestriction"),
											$objGeneral->fnGet("txtRestrictionAmount"));
    else if ($varAction == "AddNewEmployee")
        $varError = $objEmployee->AddNewEmployee(
        									$objGeneral->fnGet("selEmployeeType"),
        									$objGeneral->fnGet("selStation"),
											$objGeneral->fnGet("selDepartment"),
        									$objGeneral->fnGet("txtUserName"),
        									$objGeneral->fnGet("txtPassword"),
        									$objGeneral->fnGet("selEmployeeReportTo"),
											$objGeneral->fnGet("selChartOfAccounts"),
											$objGeneral->fnGet("selDonorProjects"),
        									$objGeneral->fnGet("selStatus"),
        									$objGeneral->fnGet("txtFirstName"),
        									$objGeneral->fnGet("txtLastName"),
        									$objGeneral->fnGet("txtFatherName"),
        									$objGeneral->fnGet("txtNICNumber"),
        									$objGeneral->fnGet("txtDesignation"),
        									$objGeneral->fnGet("txtEmailAddress"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtMobileNumber"),
        									$objGeneral->fnGet("txtNotes"),
											$objGeneral->fnGet("chkApplyRestriction"),
											$objGeneral->fnGet("txtRestrictionAmount"));
	if ($varAction == "DeleteEmployee")
		$varError = $objEmployee->DeleteEmployee($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objEmployee->ShowEmployeeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>