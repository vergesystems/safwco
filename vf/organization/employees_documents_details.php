<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

$objEmployeeDocuments = new clsEmployeeDocuments();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "AddNewEmployeeDocument")
        $varError = $objEmployeeDocuments->AddNewEmployeeDocument($objGeneral->fnGet("id"), $objGeneral->fnGet("txtDocumentTitle"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateEmployeeDocument")
		$varError = $objEmployeeDocuments->UpdateEmployeeDocument($objGeneral->fnGet("id"), $objGeneral->fnGet("documentid"), $objGeneral->fnGet("txtDocumentTitle"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteEmployeeDocument")
		$varError = $objEmployeeDocuments->DeleteEmployeeDocument($objGeneral->fnGet("id"), $objGeneral->fnGet("documentid"));

}

include('../include/top2.php');
print($objEmployeeDocuments->EmployeeDocumentDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("documentid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>