<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

$objEmployeeDocuments = new clsEmployeeDocuments();

print($objEmployeeDocuments->EmployeeDocumentDownload($objGeneral->fnGet("id"), $objGeneral->fnGet("documentid")));
?>