<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

$objEmployeeDocuments = new clsEmployeeDocuments();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteEmployeeDocument")
		$varError = $objEmployeeDocuments->DeleteEmployeeDocument($objGeneral->fnGet("id"), $objGeneral->fnGet("documentid"));
}

include('../include/top2.php');
print($objEmployeeDocuments->ShowAllEmployeeDocuments($objGeneral->fnGet("id"), $objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>