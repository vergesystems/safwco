<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

$objEmployeeType = new clsEmployees_Types();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "AddNewEmployeeType")
        $varError = $objEmployeeType->AddNewEmployeeType($objGeneral->fnGet("txtEmployeeTypeName"), $objGeneral->fnGet("selEmployeeTypeParent"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "UpdateEmployeeType")
		$varError = $objEmployeeType->UpdateEmployeeType($objGeneral->fnGet("id"), $objGeneral->fnGet("txtEmployeeTypeName"), $objGeneral->fnGet("selEmployeeTypeParent"), $objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteEmployeeType")
		$varError = $objEmployeeType->DeleteEmployeeType($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objEmployeeType->EmployeeTypeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>