<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

$objEmployeeType = new clsEmployees_Types();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
		
	if ($varAction == "SetEmployeeTypeRoles")
		$varError = $objEmployeeType->SetEmployeeTypeRoles($objGeneral->fnGet("id"));
	else if ($varAction == "CopyRoles_EmployeeType")
		$varError = $objEmployee->CopyRoles("EmployeeType", "EmployeeType", $objGeneral->fnGet("id"), $objGeneral->fnGet("selCopyRoles_EmployeeType"));
	else if ($varAction == "CopyRoles_Employee")
		$varError = $objEmployee->CopyRoles("EmployeeType", "Employee", $objGeneral->fnGet("id"), $objGeneral->fnGet("selCopyRoles_Employee"));
}

include('../include/top2.php');
print($objEmployeeType->ShowEmployeeTypeRoles($objGeneral->fnGet("id")));
include('../include/bottom2.php');
?>