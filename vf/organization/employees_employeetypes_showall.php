<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

$objEmployeeType = new clsEmployees_Types();


if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteEmployeeType")
		$varError = $objEmployeeType->DeleteEmployeeType($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objEmployeeType->ShowAllEmployeeTypes($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>