<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');
    
if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "SetEmployeeRoles")
		$varError = $objEmployee->SetEmployeeRoles($objGeneral->fnGet("id"));
	else if ($varAction == "CopyRoles_EmployeeType")
		$varError = $objEmployee->CopyRoles("Employee", "EmployeeType", $objGeneral->fnGet("id"), $objGeneral->fnGet("selCopyRoles_EmployeeType"));
	else if ($varAction == "CopyRoles_Employee")
		$varError = $objEmployee->CopyRoles("Employee", "Employee", $objGeneral->fnGet("id"), $objGeneral->fnGet("selCopyRoles_Employee"));	
}

include('../include/top2.php');
print($objEmployee->ShowEmployeeRoles($objGeneral->fnGet("id")));
include('../include/bottom2.php');
?>