<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteEmployee")
		$varError = $objEmployee->DeleteEmployee($objGeneral->fnGet("id"));
}

include('../include/top2.php');

print($objEmployee->ShowAllEmployees($objGeneral->fnGet("p"), $objGeneral->fnGet("station"), $objGeneral->fnGet("employeetype")));
include('../include/bottom2.php');

?>