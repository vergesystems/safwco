<?php
include('../include/includes.php');
include(cVSFFolder . '/classes/clsGoogleMaps.php');

$gm = new EasyGoogleMap(cGoogleMapsAPIKey);
$gm->SetMarkerIconStyle('STAR');

$gm->SetMapControl('LARGE_PAN_ZOOM');
$gm->SetMapWidth(400);
$gm->SetMapHeight(350);
$gm->mMapType = TRUE;

$gm->SetMapZoom(5);

$sCity = $objGeneral->fnGet("city");
$sState = $objGeneral->fnGet("state");
$sCountry = $objGeneral->fnGet("country");
$sPhoneNumber = $objGeneral->fnGet("phone");
$sAddressType = $objGeneral->fnGet("stationname");

$sLatitude = $objGeneral->fnGet("latitude");
$sLongitude = $objGeneral->fnGet("longitude");

$aData[0][0] = $sCity . ', ' . $sState . ', ' . $Country;
$aData[0][1] = $sAddressType;
$aData[0][2] = $sAddress . $sCity . ', ' . $sState . ', ' . $sCountry . '<br />Phone: ' . $sPhoneNumber;

if (($sLatitude != "") && ($sLongitude != ""))
	$aData[0][0] = $sLatitude . ', ' . $sLongitude;

for ($i=0; $i < count($aData); $i++)
{
	$gm->SetAddress($aData[$i][0]);
	$gm->SetInfoWindowText('<span style="font-size:10px; font-family:Tahoma, Arial; font-weight:bold;">' . $aData[$i][1] . '</span><span style="font-size:10px; font-family:Tahoma, Arial;"><br />' . $aData[$i][2] . '</span>');
}
//$map->addMarkerByAddress($aData[$i][0], $aData[$i][1], $aData[$i][1] . '<br />' . $aData[$i][2], $aData[$i][1]);

print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
' . $gm->GmapsKey() . '
</head>
<body topmargin="0" leftmargin="0">
' . $gm->MapHolder() . '
' . $gm->InitJs() . '
' . $gm->UnloadMap() . '
</body>
</html>');
//echo $gm->GetSideClick();
?>