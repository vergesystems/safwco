<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objOrganization = new clsOrganization();

include('../include/top.php');
print($objOrganization->ShowOrganizationPages($objGeneral->fnGet("page")));
include('../include/bottom.php'); 

?>