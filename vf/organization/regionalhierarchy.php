<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objRegions = new clsRegions();
$objRegionTypes = new clsRegions_Types();

$varAction = $objGeneral->fnGet("action");
if ($varAction != "")
{
	if ($varAction == "AddNewRegion")
	{
        $varError = $objRegions->AddNewRegion(
        										$objGeneral->fnGet("txtRegionName"), 
        										$objGeneral->fnGet("txtRegionCode"), 
        										$objGeneral->fnGet("selRegionTypeId"),
        										$objGeneral->fnGet("selRegionParentId"),
        										$objGeneral->fnGet("txtRegionDescription"),
        										$objGeneral->fnGet("txtNotes"),
        										$objGeneral->fnGet("selStatus"));
	}
	else if ($varAction == "UpdateRegion")
	{
		$varError = $objRegions->UpdateRegion(
												$objGeneral->fnGet("id"),
        										$objGeneral->fnGet("txtRegionName"), 
												$objGeneral->fnGet("txtRegionCode"),
        										$objGeneral->fnGet("selRegionTypeId"),
        										$objGeneral->fnGet("selRegionParentId"),
        										$objGeneral->fnGet("txtRegionDescription"),
        										$objGeneral->fnGet("txtNotes"),
        										$objGeneral->fnGet("selStatus"));
	}
	else if ($varAction == "DeleteRegion")
		$varError = $objRegions->DeleteRegion($objGeneral->fnGet("id"));
		
	else if ($varAction == "AddNewRegionType")
        $varError = $objRegionTypes->AddNewRegionType($objGeneral->fnGet("txtRegionTypeName"), $objGeneral->fnGet("selRegionTypeParentId"));
	else if ($varAction == "UpdateRegionType")
		$varError = $objRegionTypes->UpdateRegionType($objGeneral->fnGet("id"), $objGeneral->fnGet("txtRegionTypeName"), $objGeneral->fnGet("selRegionTypeParentId"));
	else if ($varAction == "DeleteRegionType")
		$varError = $objRegionTypes->DeleteRegionType($objGeneral->fnGet("id"));	
}

include('../include/top2.php'); 

$sPageType = $objGeneral->fnGet("pagetype");

if ($sPageType == "")
	print($objRegions->ShowAllRegions($objGeneral->fnGet("p"), $objGeneral->fnGet("stationtype")));
else if ($sPageType == "regiondetails")
	print($objRegions->RegionDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
else if ($sPageType == "regiontypes")
	print($objRegionTypes->ShowAllRegionTypes($objGeneral->fnGet("p")));
else if ($sPageType == "regiontypes_details")	
	print($objRegionTypes->RegionTypeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
		
include('../include/bottom2.php'); 
?>