<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objRegions = new clsRegions();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "AddNewRegion")
	{
        $varError = $objRegions->AddNewRegion(
        										$objGeneral->fnGet("txtRegionName"), 
        										$objGeneral->fnGet("txtRegionCode"), 
        										$objGeneral->fnGet("selRegionTypeId"),
        										$objGeneral->fnGet("selRegionParentId"),
        										$objGeneral->fnGet("txtRegionDescription"),
        										$objGeneral->fnGet("txtNotes"),
        										$objGeneral->fnGet("selStatus"));
	}
	else if ($varAction == "UpdateRegion")
	{
		$varError = $objRegions->UpdateRegion(
												$objGeneral->fnGet("id"),
        										$objGeneral->fnGet("txtRegionName"), 
												$objGeneral->fnGet("txtRegionCode"),         										
        										$objGeneral->fnGet("selRegionTypeId"),
        										$objGeneral->fnGet("selRegionParentId"),
        										$objGeneral->fnGet("txtRegionDescription"),
        										$objGeneral->fnGet("txtNotes"),
        										$objGeneral->fnGet("selStatus"));
	}
	else if ($varAction == "DeleteRegion")
		$varError = $objRegions->DeleteRegion($objGeneral->fnGet("id"));
	
}

include('../include/top2.php');
print($objRegions->RegionDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>