<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objRegions = new clsRegions();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteRegion")
		$varError = $objRegions->DeleteRegion($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objRegions->ShowAllRegions($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>