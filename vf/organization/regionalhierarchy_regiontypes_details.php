<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objRegionTypes = new clsRegions_Types();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "AddNewRegionType")
        $varError = $objRegionTypes->AddNewRegionType($objGeneral->fnGet("txtRegionTypeName"), $objGeneral->fnGet("selRegionTypeParentId"));
	else if ($varAction == "UpdateRegionType")
		$varError = $objRegionTypes->UpdateRegionType($objGeneral->fnGet("id"), $objGeneral->fnGet("txtRegionTypeName"), $objGeneral->fnGet("selRegionTypeParentId"));
	else if ($varAction == "DeleteRegionType")
		$varError = $objRegionTypes->DeleteRegionType($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objRegionTypes->RegionTypeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>