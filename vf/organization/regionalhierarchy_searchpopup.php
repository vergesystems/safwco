<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	print('<script type="text/javascript" language="JavaScript">alert(\'Your session has ended, Please Login Again!\');window.close();</script>');

include('../../system/library/clsOrganization.php');
$objRegions = new clsRegions();

include('../include/top2.php');

print('
<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
 <tr>
  <td>
   <table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
    <tr>
     <td>
      ' . $objRegions->ShowSearchRegionPopUp($objGeneral->fnGet("type")) . '
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br />
');

include('../include/bottom2.php'); ?>