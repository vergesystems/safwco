<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objOrganization = new clsOrganization();

$sAction = $objGeneral->fnGet("action");
if ($sAction != "")
{
	if ($sAction == "SaveSettings")
        $varError = $objOrganization->SaveSettings();
}

include('../include/top2.php'); 
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objOrganization->ShowAllSystemSettings());
	
include('../include/bottom2.php');
?>