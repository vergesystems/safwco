<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objOrganization = new clsOrganization();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

    if ($varAction == "SaveSettings")
        $varError = $objOrganization->SaveSettings();
}

include('../include/top2.php'); 
print($objOrganization->ShowAllSystemSettings());
include('../include/bottom2.php'); 
?>