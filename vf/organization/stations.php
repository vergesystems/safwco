<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objStation = new clsStations();

$sAction = $objGeneral->fnGet("action");

if ($sAction != "")
{
    if ($sAction == "AddNewStation") 
    {
		$varError = $objStation->AddNewStation(
        									$objGeneral->fnGet("txtStationName"),
        									$objGeneral->fnGet("selStationType"),
        									$objGeneral->fnGet("selStationParentId"),
											$objGeneral->fnGet("selStationTimeZone"),
											$objGeneral->fnGet("selStationCurrency"),
											$objGeneral->fnGet("txtStationCurrencySign"),
        									$objGeneral->fnGet("txtStationCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtFaxNumber"),
        									$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtWebsite"),
											$objGeneral->fnGet("txtLatitude"),
											$objGeneral->fnGet("txtLongitude"),
        									$objGeneral->fnGet("txtNotes"));
	}
	else if ($sAction == "UpdateStation")
	{
		$varError = $objStation->UpdateStation(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("txtStationName"),
        									$objGeneral->fnGet("selStationType"),
        									$objGeneral->fnGet("selStationParentId"),
											$objGeneral->fnGet("selStationTimeZone"),
											$objGeneral->fnGet("selStationCurrency"),
											$objGeneral->fnGet("txtStationCurrencySign"),
        									$objGeneral->fnGet("txtStationCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
											$objGeneral->fnGet("txtFaxNumber"),
        									$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtWebsite"),
											$objGeneral->fnGet("txtLatitude"),
											$objGeneral->fnGet("txtLongitude"),
        									$objGeneral->fnGet("txtNotes"));        									
	}
	else if ($sAction == "DeleteStation")
		$varError = $objStation->DeleteStation($objGeneral->fnGet("id"));
}

include('../include/top2.php'); 

$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objStation->ShowAllStations($objGeneral->fnGet("p"), $objGeneral->fnGet("stationtype")));
else if ($sPageType == "details")
	print($objStation->StationDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));

include('../include/bottom2.php'); 
?>