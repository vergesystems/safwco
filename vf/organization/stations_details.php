<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objStation = new clsStations();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateStation")
		$varError = $objStation->UpdateStation(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("txtStationName"),
        									$objGeneral->fnGet("selStationTypeId"),
        									$objGeneral->fnGet("selStationParentId"),
        									$objGeneral->fnGet("selRegion"),
        									$objGeneral->fnGet("txtStationCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
											$objGeneral->fnGet("txtFaxNumber"),
        									$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtWebsite"),
											$objGeneral->fnGet("txtLatitude"),
											$objGeneral->fnGet("txtLongitude"),
        									$objGeneral->fnGet("txtNotes"),
        									$objGeneral->fnGet("selStatus"));        									
    else if ($varAction == "AddNewStation")
        $varError = $objStation->AddNewStation(	
        									$objGeneral->fnGet("txtStationName"),
        									$objGeneral->fnGet("selStationTypeId"),
        									$objGeneral->fnGet("selStationParentId"),
        									$objGeneral->fnGet("selRegion"),
        									$objGeneral->fnGet("txtStationCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtFaxNumber"),
        									$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtWebsite"),
											$objGeneral->fnGet("txtLatitude"),
											$objGeneral->fnGet("txtLongitude"),
        									$objGeneral->fnGet("txtNotes"),
        									$objGeneral->fnGet("selStatus"));
	else if ($varAction == "DeleteStation")
		$varError = $objStation->DeleteStation($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objStation->StationDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>