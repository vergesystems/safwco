<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objStation = new clsStations();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteStation")
		$varError = $objStation->DeleteStation($objGeneral->fnGet("id"));
}

include('../include/top2.php'); 
print($objStation->ShowAllStations($objGeneral->fnGet("p"), $objGeneral->fnGet("stationtype")));
include('../include/bottom2.php'); 
?>