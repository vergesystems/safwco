<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objStationTypes = new clsStationTypes();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "AddNewStationType")
        $varError = $objStationTypes->AddNewStationType($objGeneral->fnGet("txtStationTypeName"));
	else if ($varAction == "UpdateStationType")
		$varError = $objStationTypes->UpdateStationType($objGeneral->fnGet("id"), $objGeneral->fnGet("txtStationTypeName"));
	else if ($varAction == "DeleteStationType")
		$varError = $objStationTypes->DeleteStationType($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objStationTypes->StationTypeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>