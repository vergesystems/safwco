<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/clsOrganization.php');
$objStationTypes = new clsStationTypes();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteStationType")
		$varError = $objStationTypes->DeleteStationType($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objStationTypes->ShowAllStationTypes($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>