<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

global $objEmployee;

include('../include/top2.php'); 

$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objSystemLog->ShowAllSystemLogs($objGeneral->fnGet("p")));
else if ($sPageType == "details")
	print($objSystemLog->ShowSystemLog($objGeneral->fnGet("date")));
include('../include/bottom2.php'); 
?>