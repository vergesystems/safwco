<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objAccountReports = new clsFMS_Reports_AccountReports();

$sReportName = $objGeneral->fnGet("reportname");
if ($sReportName == '') $sReportName = "ChartOfAccounts";

include('../include/top2.php');
print($objAccountReports->ShowAccountReports($sReportName));
include('../include/bottom2.php');
?>