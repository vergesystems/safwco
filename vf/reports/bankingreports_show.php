<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objBankingReports = new clsFMS_Reports_BankingReports();

$sReportName = $objGeneral->fnGet("reportname");
if ($sReportName == '') $sReportName = "BankLedger";

include('../include/top2.php');
print($objBankingReports->ShowBankingReports($sReportName));
include('../include/bottom2.php');
?>