<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objReports = new clsFMS_Reports();

include('../include/top.php');

include(cVSFFolder . '/classes/clsDHTMLSuite.php');
$objDHTMLSuite = new clsDHTMLSuite();

$aTabs[0][0] = 'Budget Reports';
$aTabs[0][1] = '../reports/budgetreports_show.php';

print($objDHTMLSuite->TabBar($aTabs, $objReports->ShowFMSReportsMenu()));
include('../include/bottom.php'); ?>