<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objBudgetReports = new clsFMS_Reports_BudgetReports();

$sReportName = $objGeneral->fnGet("reportname");
if ($sReportName == '') $sReportName = "BudgetList";

include('../include/top2.php');
print($objBudgetReports->ShowBudgetReports($sReportName));
include('../include/bottom2.php');
?>