<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objCustomerReports = new clsFMS_Reports_CustomerReports();

$sReportName = $objGeneral->fnGet("reportname");
if ($sReportName == '') $sReportName = "Customers";

include('../include/top2.php');
print($objCustomerReports->ShowCustomerReports($sReportName));
include('../include/bottom2.php');

?>