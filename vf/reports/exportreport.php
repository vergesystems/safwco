<?php
header("Expires: 0");
@set_time_limit("99999999999");		// Set Unlimited Timeout for the Script

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objReports = new clsFMS_Reports();

$sReportString = $objReports->ShowReport($objGeneral->fnGet("report"), $objGeneral->fnGet("reporttype"), $objGeneral->fnGet("exportxls"), $aExcelData);

if ($objGeneral->fnGet("action") == "ExportReport")
	$objReports->ExportReport($sReportString, $objGeneral->fnGet("exporttype"), $aExcelData);

include('../include/top2.php');
print($objReports->ShowExportReport());
include('../include/bottom2.php');
?>