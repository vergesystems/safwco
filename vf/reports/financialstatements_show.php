<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objFinancialStatements = new clsFMS_Reports_FinancialStatements();

$sReportName = $objGeneral->fnGet("reportname");
if ($sReportName == '') $sReportName = "IncomeStatement";

include('../include/top2.php');
print($objFinancialStatements->ShowFinancialStatements($sReportName));
include('../include/bottom2.php');
?>