<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');
	
include('../../system/library/fms/clsFMS_Reports.php');
$objGraphs = new clsFMS_Reports_Graphs();

$sReportName = $objGeneral->fnGet("reportname");
if ($sReportName == '') $sReportName = "IncomeVsExpenditureGraph";

include('../include/top2.php');
print($objGraphs->ShowGraphs($sReportName));
include('../include/bottom2.php');
?>