<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objReports = new clsFMS_Reports();

include('../include/top.php');
print($objReports->ShowReportsPages($objGeneral->fnGet("page")));
include('../include/bottom.php');

?>