<?php
header("Expires: 0");
@set_time_limit("99999999999");		// Set Unlimited Timeout for the Script

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objReports = new clsFMS_Reports();

$sAction = $objGeneral->fnGet("action");

$sReportString = $objReports->ShowReport($objGeneral->fnGet("report"), $objGeneral->fnGet("reporttype"), $objGeneral->fnGet("exportxls"), $aExcelData, $sAction);

if ($sAction == "EmailReport")
	$sEmailResponse = $objReports->EmailReport($sReportString, $objGeneral->fnGet("EmailDetails_YourName"), $objGeneral->fnGet("EmailDetails_YourEmailAddress"), $objGeneral->fnGet("EmailDetails_RecipientName"), $objGeneral->fnGet("EmailDetails_RecipientEmailAddress"), $objGeneral->fnGet("EmailDetails_Message"));
else if ($sAction == "ExportReport")
	$objReports->ExportReport($sReportString, $objGeneral->fnGet("exporttype"), $aExcelData);

include('../include/top2.php');

// Stylesheet for Reports Printing
print($objReports->ReportStyleSheet());
if ($sEmailResponse != "") print('<script type="text/javascript" language="JavaScript">alert("' . $sEmailResponse . '");</script>');
print($sReportString);

include('../include/bottom2.php');
?>