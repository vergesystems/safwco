<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Reports.php');
$objVendorReports = new clsFMS_Reports_VendorReports();

$sReportName = $objGeneral->fnGet("reportname");
if ($sReportName == '') $sReportName = "Vendors";

include('../include/top2.php');
print($objVendorReports->ShowVendorReports($sReportName));
include('../include/bottom2.php');
?>