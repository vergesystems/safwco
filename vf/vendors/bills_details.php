<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Vendors.php');
$objBill = new clsVendors_Bills();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateBill")
		$varError = $objBill->UpdateBill(											
											$objGeneral->fnGet("id"), 
											$objGeneral->fnGet("selPurchaseOrder"), 
											$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("hdnBillItems"), 
											$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewBill")
        $varError = $objBill->AddNewBill(   
											$objGeneral->fnGet("selPurchaseOrder"), 
											$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("hdnBillItems"),  
											$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteBill")
		$varError = $objBill->DeleteBill($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objBill->BillDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>