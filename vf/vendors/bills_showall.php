<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Vendors.php');
$objBill = new clsVendors_Bills();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteBill")
		$varError = $objBill->DeleteBill($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objBill->ShowAllBills($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>