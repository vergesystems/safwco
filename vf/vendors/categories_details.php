<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_Products.php');
$objCategories = new clsVendors_Products_Categories();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateCategory")
		$varError = $objCategories->UpdateCategory(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("txtCategoryName"),
        									$objGeneral->fnGet("txtCategoryCode"),
        								   	$objGeneral->fnGet("txtDescription"),
        								   	$objGeneral->fnGet("txtNotes"),
        								   	$objGeneral->fnGet("selCategoryParentId"));
    else if ($varAction == "AddNewCategory")
        $varError = $objCategories->AddNewCategory(
        									$objGeneral->fnGet("txtCategoryName"),
        									$objGeneral->fnGet("txtCategoryCode"),
        									$objGeneral->fnGet("txtDescription"),
        									$objGeneral->fnGet("txtNotes"),
    	   									$objGeneral->fnGet("selCategoryParentId"));
    if ($varAction == "DeleteCategory")
		$varError = $objCategories->DeleteCategory(
										$objGeneral->fnGet("id"));
    	   						
	

}

include('../include/top2.php');
print($objCategories->CategoryDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>
