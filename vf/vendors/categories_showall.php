<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_Products.php');
$objCategories = new clsVendors_Products_Categories();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteCategory")
		$varError = $objCategories->DeleteCategory($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objCategories->ShowAllCategories($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>