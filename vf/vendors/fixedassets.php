<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');
	
include('../../system/library/fms/clsFMS_Vendors_FixedAssets.php');
$objFixedAssets = new clsVendors_FixedAssets();
$objCategories = new clsVendors_FixedAssets_Categories();

include('../../system/library/fms/clsFMS_Vendors_Manufacturers.php');
$objManufacturers = new clsManufacturers();

$varAction = $objGeneral->fnGet("action");

if ($varAction != "")
{	
	if ($varAction == "UpdateFixedAsset")
		$varError = $objFixedAssets->UpdateFixedAsset(
											$objGeneral->fnGet("id"),
											$objGeneral->fnGet("selCategory"),
        									$objGeneral->fnGet("selVendor"),
                                            $objGeneral->fnGet("selManufacturer"),
        									$objGeneral->fnGet("txtFixedAssetName"),
        									$objGeneral->fnGet("txtFixedAssetStockNumber"),
        									$objGeneral->fnGet("selStation"),
        									$objGeneral->fnGet("selDonorProject"),
        									$objGeneral->fnGet("selCurrentStation"),

        									$objGeneral->fnGet("txtDateOfPurchase"),
											$objGeneral->fnGet("selFixedAssetStatus"),
        									$objGeneral->fnGet("txtFixedAssetCost"),
        									$objGeneral->fnGet("txtFixedAssetWeight"),

        									$objGeneral->fnGet("txtFixedAssetRecoveryPeriod"),
        									$objGeneral->fnGet("txtFixedAssetSalvageValue"),
        									$objGeneral->fnGet("radFixedAssetDepreciationMethod"),
        									$objGeneral->fnGet("txtDepreciationValue"),

        								   	$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("chkFixedAssetConsumable"),
        								   	$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewFixedAsset")
        $varError = $objFixedAssets->AddNewFixedAsset(
        									$objGeneral->fnGet("selCategory"),
        									$objGeneral->fnGet("selVendor"),
                                            $objGeneral->fnGet("selManufacturer"),
        									$objGeneral->fnGet("txtFixedAssetName"),
        									$objGeneral->fnGet("txtFixedAssetStockNumber"),
        									$objGeneral->fnGet("selStation"),
        									$objGeneral->fnGet("selDonorProject"),
        									$objGeneral->fnGet("selCurrentStation"),
											
        									$objGeneral->fnGet("txtDateOfPurchase"),
											$objGeneral->fnGet("selFixedAssetStatus"),
        									$objGeneral->fnGet("txtFixedAssetCost"),
        									$objGeneral->fnGet("txtFixedAssetWeight"),

        									$objGeneral->fnGet("txtFixedAssetRecoveryPeriod"),
        									$objGeneral->fnGet("txtFixedAssetSalvageValue"),
        									$objGeneral->fnGet("radFixedAssetDepreciationMethod"),
        									$objGeneral->fnGet("txtDepreciationValue"),
        									$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("chkFixedAssetConsumable"),
        									$objGeneral->fnGet("txtNotes"));

   else if ($varAction == "ReIssueFixedAssetCode")
		$varError = $objFixedAssets->ReIssueFixedAssetCode($objGeneral->fnGet("id"));
   else if ($varAction == "DeleteFixedAsset")
		$varError = $objFixedAssets->DeleteFixedAsset($objGeneral->fnGet("id"));
		
	else if($varAction == "UpdateCategory")
		$varError = $objCategories->UpdateCategory(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("txtCategoryName"),
        									$objGeneral->fnGet("txtCategoryCode"),
        								   	$objGeneral->fnGet("txtDescription"),
        								   	$objGeneral->fnGet("txtNotes"),
        								   	$objGeneral->fnGet("selCategoryParentId"));
    else if ($varAction == "AddNewCategory")
        $varError = $objCategories->AddNewCategory(
        									$objGeneral->fnGet("txtCategoryName"),
        									$objGeneral->fnGet("txtCategoryCode"),
        									$objGeneral->fnGet("txtDescription"),
        									$objGeneral->fnGet("txtNotes"),
    	   									$objGeneral->fnGet("selCategoryParentId"));
    if ($varAction == "DeleteCategory")
		$varError = $objCategories->DeleteCategory($objGeneral->fnGet("id"));	
		
	else if($varAction == "UpdateManufacturer")
		$varError = $objManufacturers->UpdateManufacturer(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("txtManufacturerName"),
        									$objGeneral->fnGet("txtManufacturerCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtNotes"),
        								   	$objGeneral->fnGet("txtDescription"));
    else if ($varAction == "AddNewManufacturer")
        $varError = $objManufacturers->AddNewManufacturer(
        									$objGeneral->fnGet("txtManufacturerName"),
        									$objGeneral->fnGet("txtManufacturerCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtNotes"),   	
        									$objGeneral->fnGet("txtDescription"));
	else if ($varAction == "DeleteManufacturer")
		$varError = $objManufacturers->DeleteManufacturer($objGeneral->fnGet("id"));	

}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");

if ($sPageType == "")
	print($objFixedAssets->ShowAllFixedAssets($objGeneral->fnGet("p"), $objGeneral->fnGet("cat"), $objGeneral->fnGet("vendor"), $objGeneral->fnGet("manufacturer"), $objGeneral->fnGet("status")));
else if($sPageType == "details")
	print($objFixedAssets->FixedAssetDetails($objGeneral->fnGet("id"),$objGeneral->fnGet("categoryid"), $objGeneral->fnGet("vendorid"), $objGeneral->fnGet("action2")));
	
else if($sPageType == "categories")
	print($objCategories->ShowAllCategories($objGeneral->fnGet("p")));
else if($sPageType == "categories_details")
	print($objCategories->CategoryDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
	
else if($sPageType == "manufacturers")
	print($objManufacturers->ShowAllManufacturers($objGeneral->fnGet("p")));
else if($sPageType == "manufacturers_details")
	print($objManufacturers->ManufacturerDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');

?>