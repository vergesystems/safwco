<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');
	
include('../../system/library/fms/clsFMS_Vendors.php');
$objVendors = new clsVendors();

include('../include/top.php');
print($objVendors->ShowVendorsPages($objGeneral->fnGet("page")));
include('../include/bottom.php'); 
?>