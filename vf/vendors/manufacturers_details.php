<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_Manufacturers.php');
$objManufacturers = new clsManufacturers();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateManufacturer")
		$varError = $objManufacturers->UpdateManufacturer(
											$objGeneral->fnGet("id"),
        									$objGeneral->fnGet("txtManufacturerName"),
        									$objGeneral->fnGet("txtManufacturerCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtNotes"),
        								   	$objGeneral->fnGet("txtDescription"));
    else if ($varAction == "AddNewManufacturer")
        $varError = $objManufacturers->AddNewManufacturer(
        									$objGeneral->fnGet("txtManufacturerName"),
        									$objGeneral->fnGet("txtManufacturerCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
        									$objGeneral->fnGet("txtNotes"),   	
        									$objGeneral->fnGet("txtDescription"));
	else if ($varAction == "DeleteManufacturer")
		$varError = $objManufacturers->DeleteManufacturer($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objManufacturers->ManufacturerDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');

?>