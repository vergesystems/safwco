<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_Manufacturers.php');
$objManufacturers = new clsManufacturers();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteManufacturer")
		$varError = $objManufacturers->DeleteManufacturer($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objManufacturers->ShowAllManufacturers($objGeneral->fnGet("p")));
include('../include/bottom2.php');

?>