<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
	$objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors.php');
$objVendors = new clsVendors();

include('../include/top.php');

include(cVSFFolder . '/classes/clsDHTMLSuite.php');
$objDHTMLSuite = new clsDHTMLSuite();

$aTabs[0][0] = 'Pay Bills';
$aTabs[0][1] = '../vendors/paybills_showall.php';

print($objDHTMLSuite->TabBar($aTabs, $objVendors->ShowvendorsMenu()));
include('../include/bottom.php'); ?>