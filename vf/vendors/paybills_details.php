<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Vendors_PayBills.php');
$objPayBill = new clsPayBills();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdatePayBill")
		$varError = $objPayBill->UpdatePayBill(											
												$objGeneral->fnGet("id"), 
												$objGeneral->fnGet("selVendor"), 
												$objGeneral->fnGet("txtDiscount"), 
												$objGeneral->fnGet("txtPaidAmount"), 
												$objGeneral->fnGet("txtPaidDate"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewPayBill")
        $varError = $objPayBill->AddNewPayBill(        									
												$objGeneral->fnGet("selVendor"), 
												$objGeneral->fnGet("txtDiscount"), 
												$objGeneral->fnGet("txtPaidAmount"), 
												$objGeneral->fnGet("txtPaidDate"), 
												$objGeneral->fnGet("txtDescription"), 
												$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeletePayBill")
		$varError = $objPayBill->DeletePayBill($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objPayBill->PayBillDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>