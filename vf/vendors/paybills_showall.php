<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Vendors_PayBills.php');
$objPayBill = new clsPayBills();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeletePayBill")
		$varError = $objPayBill->DeletePayBill($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objPayBill->ShowAllPayBills($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>