<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_Products.php');
$objProducts = new clsVendors_Products();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateProduct")
		$varError = $objProducts->UpdateProduct(
											$objGeneral->fnGet("id"),
											$objGeneral->fnGet("selCategory"),        									
        									$objGeneral->fnGet("selVendor"),
                                            $objGeneral->fnGet("selManufacturer"),
        									$objGeneral->fnGet("txtProductName"),
        									$objGeneral->fnGet("txtProductStockNumber"),
        									$objGeneral->fnGet("selStation"),
        									$objGeneral->fnGet("selDonorProject"),
        									$objGeneral->fnGet("selCurrentStation"),

        									$objGeneral->fnGet("txtDateOfPurchase"),
											$objGeneral->fnGet("selProductStatus"),
        									$objGeneral->fnGet("txtProductCost"),
        									$objGeneral->fnGet("txtProductWeight"),

        									$objGeneral->fnGet("txtProductRecoveryPeriod"),
        									$objGeneral->fnGet("txtProductSalvageValue"),
        									$objGeneral->fnGet("radProductDepreciationMethod"),
        									$objGeneral->fnGet("txtDepreciationValue"),

        								   	$objGeneral->fnGet("txtDescription"),
        								   	$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewProduct")
        $varError = $objProducts->AddNewProduct(
        									$objGeneral->fnGet("selCategory"),
        									$objGeneral->fnGet("selVendor"),
                                            $objGeneral->fnGet("selManufacturer"),
        									$objGeneral->fnGet("txtProductName"),
        									$objGeneral->fnGet("txtProductStockNumber"),
        									$objGeneral->fnGet("selStation"),
        									$objGeneral->fnGet("selDonorProject"),
        									$objGeneral->fnGet("selCurrentStation"),

        									$objGeneral->fnGet("txtDateOfPurchase"),
											$objGeneral->fnGet("selProductStatus"),
        									$objGeneral->fnGet("txtProductCost"),
        									$objGeneral->fnGet("txtProductWeight"),

        									$objGeneral->fnGet("txtProductRecoveryPeriod"),
        									$objGeneral->fnGet("txtProductSalvageValue"),
        									$objGeneral->fnGet("radProductDepreciationMethod"),
        									$objGeneral->fnGet("txtDepreciationValue"),

        									$objGeneral->fnGet("txtDescription"),
        									$objGeneral->fnGet("txtNotes"));

   else if ($varAction == "ReIssueProductCode")
		$varError = $objProducts->ReIssueProductCode($objGeneral->fnGet("id"));
   else if ($varAction == "DeleteProduct")
		$varError = $objProducts->DeleteProduct($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objProducts->ProductDetails($objGeneral->fnGet("id"),$objGeneral->fnGet("categoryid"), $objGeneral->fnGet("vendorid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');

?>