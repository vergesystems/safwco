<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Vendors_Products.php');
$objProducts = new clsVendors_Products();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteProduct")
		$varError = $objProducts->DeleteProduct($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objProducts->ShowAllProducts($objGeneral->fnGet("p"), $objGeneral->fnGet("cat"), $objGeneral->fnGet("vendor"), $objGeneral->fnGet("manufacturer"), $objGeneral->fnGet("status")));
include('../include/bottom2.php');
?>