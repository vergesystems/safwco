<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Vendors.php');
$objPurchaseOrder = new clsVendors_PurchaseOrders();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdatePurchaseOrder")
		$varError = $objPurchaseOrder->UpdatePurchaseOrder(											
															$objGeneral->fnGet("id"), 
															$objGeneral->fnGet("selQuotation"),
															$objGeneral->fnGet("txtPurchaseOrderNumber"),
															$objGeneral->fnGet("txtDeliveryDate"),
															$objGeneral->fnGet("txtDescription"),
															$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewPurchaseOrder")
        $varError = $objPurchaseOrder->AddNewPurchaseOrder(
															$objGeneral->fnGet("selQuotation"), 
															$objGeneral->fnGet("txtPurchaseOrderNumber"),
															$objGeneral->fnGet("txtDeliveryDate"),
															$objGeneral->fnGet("txtDescription"), 
															$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeletePurchaseOrder")
		$varError = $objPurchaseOrder->DeletePurchaseOrder($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objPurchaseOrder->PurchaseOrderDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>