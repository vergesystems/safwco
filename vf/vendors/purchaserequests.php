<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_PurchaseRequests.php');
$objPurchaseRequest = new clsVendors_PurchaseRequests();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");
	
	if ($varAction == "UpdatePurchaseRequest")
		$varError = $objPurchaseRequest->UpdatePurchaseRequest(
											$objGeneral->fnGet("id"),
                                            $objGeneral->fnGet("selEmployee"),
											$objGeneral->fnGet("txtPurchaseRequestNumber"),
											$objGeneral->fnGet("selPurchaseRequestStatus"),
        									$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("OrderTotalAmount"),
        									$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewPurchaseRequest")
        $varError = $objPurchaseRequest->AddNewPurchaseRequest(
											$objGeneral->fnGet("selEmployee"),
											$objGeneral->fnGet("txtPurchaseRequestNumber"),
											$objGeneral->fnGet("selPurchaseRequestStatus"),
        									$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("OrderTotalAmount"),
        									$objGeneral->fnGet("txtNotes"));
   else if ($varAction == "DeletePurchaseRequest")
		$varError = $objPurchaseRequest->DeletePurchaseRequest($objGeneral->fnGet("id"));
}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objPurchaseRequest->ShowAllPurchaseRequests($objGeneral->fnGet("p"), $objGeneral->fnGet("id"), $objGeneral->fnGet("status")));
else if ($sPageType == "details")	
	print($objPurchaseRequest->PurchaseRequestDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
	
include('../include/bottom2.php');
?>