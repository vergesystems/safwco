<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_PurchaseRequests.php');
$objPurchaseRequest = new clsVendors_PurchaseRequests();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdatePurchaseRequest")
		$varError = $objPurchaseRequest->UpdatePurchaseRequest(
											$objGeneral->fnGet("id"),
                                            $objGeneral->fnGet("selEmployee"),
											$objGeneral->fnGet("txtPurchaseRequestNumber"),
											$objGeneral->fnGet("selPurchaseRequestStatus"),
        									$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("OrderTotalAmount"),
        									$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewPurchaseRequest")
        $varError = $objPurchaseRequest->AddNewPurchaseRequest(
											$objGeneral->fnGet("selEmployee"),
											$objGeneral->fnGet("txtPurchaseRequestNumber"),
											$objGeneral->fnGet("selPurchaseRequestStatus"),
        									$objGeneral->fnGet("txtDescription"),
											$objGeneral->fnGet("OrderTotalAmount"),
        									$objGeneral->fnGet("txtNotes"));
   else if ($varAction == "DeletePurchaseRequest")
		$varError = $objPurchaseRequest->DeletePurchaseRequest($objGeneral->fnGet("id"));
	/*
	else if ($varAction == "ForwardPurchaseRequest")
		$varError = $objPurchaseRequest->Forward($objGeneral->fnGet("purchaserequestid"), $objGeneral->fnGet("selForwardTo"), $objGeneral->fnGet("txtComments"), 0, $objGeneral->fnGet("hdnSource"));
	else if ($varAction == "RejectPurchaseRequest")
		$varError = $objPurchaseRequest->Reject($objGeneral->fnGet("purchaserequestid"), $objGeneral->fnGet("txtComments"));
	else if ($varAction == "ApprovePurchaseRequest")
		$varError = $objPurchaseRequest->Approve($objGeneral->fnGet("purchaserequestid"), $objGeneral->fnGet("txtComments"));
	else if ($varAction == "SendBackPurchaseRequest")
		$varError = $objPurchaseRequest->SendBack($objGeneral->fnGet("purchaserequestid"), $objGeneral->fnGet("hdnSource"), $objGeneral->fnGet("txtComments"));
	*/
}

include('../include/top2.php');
print($objPurchaseRequest->PurchaseRequestDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');

?>