<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_PurchaseRequests.php');
$objPurchaseRequest = new clsVendors_PurchaseRequests();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeletePurchaseRequest")
		$varError = $objPurchaseRequest->DeletePurchaseRequest($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objPurchaseRequest->ShowAllPurchaseRequests($objGeneral->fnGet("p"), $objGeneral->fnGet("id"), $objGeneral->fnGet("status")));
include('../include/bottom2.php');
?>