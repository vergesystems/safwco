<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Vendors_Quotations.php');
$objQuotation = new clsQuotations();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateQuotation")
		$varError = $objQuotation->UpdateQuotation(											
													$objGeneral->fnGet("quotationid"),
													$objGeneral->fnGet("selPurchaseRequest"),
													$objGeneral->fnGet("selVendor"),
													$objGeneral->fnGet("txtQuotationDate"),
													$objGeneral->fnGet("txtValidityDate"),
													$objGeneral->fnGet("txtQuotationNumber"),
													$objGeneral->fnGet("txtQuotationTitle"),
													$objGeneral->fnGet("txtDescription"),
													$objGeneral->fnGet("OrderTotalAmount"),
													$objGeneral->fnGet("selQuotationtStatus"),
													$objGeneral->fnGet("txtNotes")
													);
    else if ($varAction == "AddNewQuotation")
		$varError = $objQuotation->AddNewQuotation(
													$objGeneral->fnGet("selPurchaseRequest"),
													$objGeneral->fnGet("selVendor"),
													$objGeneral->fnGet("txtQuotationDate"),
													$objGeneral->fnGet("txtValidityDate"),
													$objGeneral->fnGet("txtQuotationNumber"),
													$objGeneral->fnGet("txtQuotationTitle"),
													$objGeneral->fnGet("txtDescription"),
													$objGeneral->fnGet("OrderTotalAmount"),
													$objGeneral->fnGet("selQuotationtStatus"),
													$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteQuotation")
		$varError = $objQuotation->DeleteQuotation($objGeneral->fnGet("quotationid"));

}

include('../include/top2.php');
print($objQuotation->QuotationDetails($objGeneral->fnGet("quotationid"), $objGeneral->fnGet("purchaserequestid"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>