<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Employee
    $objGeneral->fnRedirect('../login/?error=2002');


include('../../system/library/fms/clsFMS_Vendors_Vendors.php');
$objVendors = new clsVendors_Vendors();
$objVendorType = new clsVendors_Types();

$varAction = $objGeneral->fnGet("action");

if ($varAction != "")
{
	if ($varAction == "UpdateVendor")
		$varError = $objVendors->UpdateVendor(
											$objGeneral->fnGet("id"),
											$objGeneral->fnGet("selVendorType"),
        									$objGeneral->fnGet("txtVendorName"),
        									$objGeneral->fnGet("txtVendorCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
											$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtNTNNumber"),
											$objGeneral->fnGet("txtCNICNumber"),
        									$objGeneral->fnGet("txtOfficeManager"),
        									$objGeneral->fnGet("txtAccountRepresentative"),
        									$objGeneral->fnGet("txtSpecialNote"),
        									$objGeneral->fnGet("txtNotes"),   		
        								   	$objGeneral->fnGet("txtVendorDescription"),
        								   	$objGeneral->fnGet("selStatus"));
    else if ($varAction == "AddNewVendor")
        $varError = $objVendors->AddNewVendor(
        									$objGeneral->fnGet("selVendorType"),
        									$objGeneral->fnGet("txtVendorName"),
        									$objGeneral->fnGet("txtVendorCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
											$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtNTNNumber"),
											$objGeneral->fnGet("txtCNICNumber"), 
        									$objGeneral->fnGet("txtOfficeManager"),
        									$objGeneral->fnGet("txtAccountRepresentative"),
        									$objGeneral->fnGet("txtSpecialNote"),
        									$objGeneral->fnGet("txtNotes"),   		
        									$objGeneral->fnGet("txtVendorDescription"),
        									$objGeneral->fnGet("selStatus"));
	else if ($varAction == "DeleteVendor")
		$varError = $objVendors->DeleteVendor($objGeneral->fnGet("id"));
		
	else if($varAction == "UpdateVendorType")
		$varError = $objVendorType->UpdateVendorType(											
														$objGeneral->fnGet("id"), 
														$objGeneral->fnGet("txtVendorTypeName"), 
														$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewVendorType")
        $varError = $objVendorType->AddNewVendorType(        									
														$objGeneral->fnGet("txtVendorTypeName"), 
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteVendorType")
		$varError = $objVendorType->DeleteVendorType($objGeneral->fnGet("id"));	

}

include('../include/top2.php');
$sPageType = $objGeneral->fnGet("pagetype");
if ($sPageType == "")
	print($objVendors->ShowAllVendors($objGeneral->fnGet("p")));
else if($sPageType == "details")
	print($objVendors->VendorDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
else if($sPageType == "vendortypes")
	print($objVendorType->ShowAllVendorTypes($objGeneral->fnGet("p")));	
else if($sPageType == "vendortypes_details")
	print($objVendorType->VendorTypeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
		
include('../include/bottom2.php');

?>