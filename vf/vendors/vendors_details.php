<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_Vendors.php');
$objVendors = new clsVendors_Vendors();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateVendor")
		$varError = $objVendors->UpdateVendor(
											$objGeneral->fnGet("id"),
											$objGeneral->fnGet("selVendorType"),
        									$objGeneral->fnGet("txtVendorName"),
        									$objGeneral->fnGet("txtVendorCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
											$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtNTNNumber"),
											$objGeneral->fnGet("txtCNICNumber"),
        									$objGeneral->fnGet("txtOfficeManager"),
        									$objGeneral->fnGet("txtAccountRepresentative"),
        									$objGeneral->fnGet("txtSpecialNote"),
        									$objGeneral->fnGet("txtNotes"),   		
        								   	$objGeneral->fnGet("txtVendorDescription"),
        								   	$objGeneral->fnGet("selStatus"));
    else if ($varAction == "AddNewVendor")
        $varError = $objVendors->AddNewVendor(
        									$objGeneral->fnGet("selVendorType"),
        									$objGeneral->fnGet("txtVendorName"),
        									$objGeneral->fnGet("txtVendorCode"),
        									$objGeneral->fnGet("txtAddress"),
        									$objGeneral->fnGet("txtCity"),
        									$objGeneral->fnGet("txtState"),
        									$objGeneral->fnGet("txtZipCode"),
        									$objGeneral->fnGet("selCountry"),
        									$objGeneral->fnGet("txtPhoneNumber"),
											$objGeneral->fnGet("txtEmailAddress"),
											$objGeneral->fnGet("txtNTNNumber"),
											$objGeneral->fnGet("txtCNICNumber"), 
        									$objGeneral->fnGet("txtOfficeManager"),
        									$objGeneral->fnGet("txtAccountRepresentative"),
        									$objGeneral->fnGet("txtSpecialNote"),
        									$objGeneral->fnGet("txtNotes"),   		
        									$objGeneral->fnGet("txtVendorDescription"),
        									$objGeneral->fnGet("selStatus"));
	else if ($varAction == "DeleteVendor")
		$varError = $objVendors->DeleteVendor($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objVendors->VendorDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');

?>