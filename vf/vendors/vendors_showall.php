<?php include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_Vendors.php');
$objVendors = new clsVendors_Vendors();
if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteVendor")
		$varError = $objVendors->DeleteVendor($objGeneral->fnGet("id"));
}

include('../include/top2.php'); 
print($objVendors->ShowAllVendors($objGeneral->fnGet("p")));
include('../include/bottom2.php');
 
?>