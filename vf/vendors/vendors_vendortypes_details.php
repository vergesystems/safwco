<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Vendors_Vendors.php');
$objVendorType = new clsVendors_Types();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ''))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "UpdateVendorType")
		$varError = $objVendorType->UpdateVendorType(											
														$objGeneral->fnGet("id"), 
														$objGeneral->fnGet("txtVendorTypeName"), 
														$objGeneral->fnGet("txtNotes"));
    else if ($varAction == "AddNewVendorType")
        $varError = $objVendorType->AddNewVendorType(        									
														$objGeneral->fnGet("txtVendorTypeName"), 
														$objGeneral->fnGet("txtNotes"));
	else if ($varAction == "DeleteVendorType")
		$varError = $objVendorType->DeleteVendorType($objGeneral->fnGet("id"));

}

include('../include/top2.php');
print($objVendorType->VendorTypeDetails($objGeneral->fnGet("id"), $objGeneral->fnGet("action2")));
include('../include/bottom2.php');
?>