<?php

include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
        $objEmployees->SessionExpired();

include('../../system/library/fms/clsFMS_Vendors_Vendors.php');
$objVendorType = new clsVendors_Types();

if (($_SERVER["CONTENT_LENGTH"] > 0) || ($objGeneral->fnGet("action") != ""))
{
	$varAction = $objGeneral->fnGet("action");

	if ($varAction == "DeleteVendorType")
		$varError = $objVendorType->DeleteVendorType($objGeneral->fnGet("id"));
}

include('../include/top2.php');
print($objVendorType->ShowAllVendorTypes($objGeneral->fnGet("p")));
include('../include/bottom2.php');
?>