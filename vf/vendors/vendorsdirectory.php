<?php
include('../include/includes.php');

if (!$objEmployee->EmployeeVerify())		// Verify the Store
    $objGeneral->fnRedirect('../login/?error=2002');

include('../../system/library/fms/clsFMS_Vendors_Vendors.php');
$objVendor = new clsVendors_Vendors();

$sVendorsDirectory = $objVendor->ShowVendorsDirectory($objGeneral->fnGet("id"), $objGeneral->fnGet("action"), $objGeneral->fnGet("view"));

include('../include/top2.php');
    
print($sVendorsDirectory);

include('../include/bottom2.php'); ?>